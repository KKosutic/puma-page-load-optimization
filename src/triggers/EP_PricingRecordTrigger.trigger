/* 
  @Author <Jyotsna Yadav>
   @name <EP_PricingRecordTrigger >
   @CreateDate <06/07/2016>
   @Description <Trigger on EP_Pricing_Engine__c > 
   @Version <1.0>
*/
trigger EP_PricingRecordTrigger on EP_Pricing_Engine__c (after update) {
  if(trigger.isAfter){
      if(trigger.isUpdate){
        EP_PricingRecordTriggerHandler.doAfterUpdate( trigger.oldMap,trigger.newMap);
      }              
    }
}