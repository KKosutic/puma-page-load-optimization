trigger EP_CRM_opportunityTrigger on Opportunity (before insert,before update,after insert,after update) {
    try{
        if(trigger.isBefore && trigger.isInsert && !EP_CRM_opportunityTriggerHandler.isBeforeInsertRecursiveTrigger){
            EP_CRM_opportunityTriggerHandler.beforeInsertAndUpdateOpportunity(trigger.new);
        }
        
        if(trigger.isBefore && trigger.isUpdate && !EP_CRM_opportunityTriggerHandler.isBeforeUpdatetRecursiveTrigger){
            EP_CRM_opportunityTriggerHandler.beforeInsertAndUpdateOpportunity(trigger.new);
        }
        
        if(trigger.isAfter && trigger.isInsert && !EP_CRM_opportunityTriggerHandler.isAfterInsertRecursiveTrigger){
        EP_CRM_opportunityTriggerHandler.afterInsertOpportunity(trigger.new);
        }
        if(trigger.isAfter && trigger.isUpdate && !EP_CRM_opportunityTriggerHandler.isAfterUpdateRecursiveTrigger){
            EP_CRM_opportunityTriggerHandler.afterUpdateOpportunity(trigger.new);
        }
    }
    catch(exception ex){
            ex.getmessage();
        }
    
}