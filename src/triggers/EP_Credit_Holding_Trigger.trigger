trigger EP_Credit_Holding_Trigger on EP_Credit_Holding__c (before update,after update, before insert, after insert) {
    if(trigger.isBefore && trigger.isUpdate){
        EP_CreditHoldingDomainObject holdingDomainObj = new EP_CreditHoldingDomainObject(trigger.new,trigger.oldMap);
        holdingDomainObj.doActionBeforeUpdate();
    }
    
    
    if(trigger.isBefore  && trigger.isInsert){
        EP_CreditHoldingDomainObject holdingDomainObj = new EP_CreditHoldingDomainObject(trigger.new,null);
        holdingDomainObj.doActionBeforeInsert();
    }
    
    if(trigger.isAfter  && trigger.isInsert){
        EP_CreditHoldingDomainObject holdingDomainObj = new EP_CreditHoldingDomainObject(trigger.new,null);
        holdingDomainObj.doActionAfterInsert();
    }
}