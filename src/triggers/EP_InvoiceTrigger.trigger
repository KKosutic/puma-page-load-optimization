/*
   @Author <Vinay Jaiswal>
   @name <EP_InvoiceTrigger>
   @Description <This triggers that handles for EP_Invoice__c object>
   @Version <1.0>
*/
trigger EP_InvoiceTrigger on EP_Invoice__c (after insert, after update, before insert, before update) {
      if(trigger.isBefore){
        if(trigger.isInsert){
            EP_InvoiceTriggerHandler.doBeforeInsert(trigger.new);
        }
    }
}