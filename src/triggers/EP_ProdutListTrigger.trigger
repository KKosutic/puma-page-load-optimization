/* 
   @Author <Accenture>
   @name <EP_ProdutListTrigger>
   @CreateDate <19/09/2016>
   @Description <This is product list trigger> 
   @Version <1.0>
*/
trigger EP_ProdutListTrigger on Pricebook2 (before insert, before update) {
    
    if(trigger.isInsert){
      if(trigger.isBefore){
        EP_ProductListTriggerHandler.doBeforeInsert(trigger.New);
      }
    }
    
    if(trigger.isUpdate){
      if(trigger.isBefore && !EP_ProductListTriggerHandler.isExecuteBeforeUpdate){
        EP_ProductListTriggerHandler.doBeforeUpdate(trigger.New, trigger.oldMap);
      }
    }
    
}