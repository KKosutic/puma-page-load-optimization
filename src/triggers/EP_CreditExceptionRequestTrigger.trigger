/**
 * @author <Accenture>
 * @name <EP_CreditExceptionRequestTrigger>
 * @createDate <08/06/2016>
 * @description <This trigger handles events on EP_Credit_Exception_Request object> 
 * @version <1.0>
 */
trigger EP_CreditExceptionRequestTrigger on EP_Credit_Exception_Request__c (after insert, after update, before update){
    //45435 Perform Credit Exception Review --Start
         if(trigger.isInsert && trigger.isAfter){
             EP_CreditExceptionTriggerHandler.doAfterInsert(Trigger.newMap);
         }
         if(trigger.isUpdate && trigger.isAfter){
                EP_CreditExceptionTriggerHandler.doAfterUpdate(Trigger.newMap,Trigger.oldMap);
         }
    //45435 Perform Credit Exception Review --End     
}