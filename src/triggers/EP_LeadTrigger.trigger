/* 
  @Author <Ashok Arora>
  @name <EP_AccountTrigger>
  @CreateDate <10/11/2015>
  @Description <This is LEAD Trigger>
  @Version <1.0>
 
*/
trigger EP_LeadTrigger on Lead (before update,after update, before insert) {
	if(trigger.isBefore){
		if(trigger.isInsert){
			EP_LeadTriggerHandler.doBeforeInsert(trigger.new); 
		}
		if(trigger.isUpdate
			&& !EP_LeadTriggerHandler.isExecuteBeforeUpdate){
			EP_LeadTriggerHandler.doBeforeUpdate(trigger.old
												,trigger.new
												,trigger.oldMap
												,trigger.newMap);
		}
	}
	if(trigger.isAfter){
		if(trigger.isUpdate
			&& !EP_LeadTriggerHandler.isExecuteAfterUpdate){
			EP_LeadTriggerHandler.doAfterUpdate(trigger.old
												,trigger.new
												,trigger.oldMap
												,trigger.newMap);
		}
	}
}