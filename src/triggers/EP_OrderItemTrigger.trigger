/* 
  @Author <Accenture>
  @name <EP_OrderItemTrigger>
  @CreateDate <21/12/2015>
  @Description <This is OrderItemTrigger>
  @Version <1.0>
 */
trigger EP_OrderItemTrigger on OrderItem (before insert,after insert, before update,after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            EP_OrderItemTriggerHandler.doBeforeInsert(trigger.new);
        }
        //Added before update Jyotsna 1.178
        if(trigger.isUpdate){
            EP_OrderItemTriggerHandler.doBeforeUpdate(trigger.oldMap,trigger.newMap);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            EP_OrderItemTriggerHandler.doAfterInsert(trigger.oldMap, trigger.newMap);
        }
     }
}