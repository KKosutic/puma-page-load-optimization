trigger EP_CRM_AccountTrigger on Account (after insert,after update) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_AccountTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_AccountTriggerHandler.afterInsertAccount(trigger.new);
        EP_CRM_AccountTriggerHandler.isInsertRecursiveTrigger= true;
    }
    
    if(trigger.isAfter && trigger.isUpdate && !EP_CRM_AccountTriggerHandler.isUpdateRecursiveTrigger){
         EP_CRM_AccountTriggerHandler.afterUpdateAccount(trigger.new);
         EP_CRM_AccountTriggerHandler.isUpdateRecursiveTrigger = true;
    }

}