<!-- 
 * 	@Author <Vikas Malik>
 * 	@Name <EP_ChangeRequest>
 * 	@CreateDate <04/01/2016>
 * 	@Description <This is the Page on EP_ChangeRequestController>
 * 	@Version <1.0>
-->
<apex:page controller="EP_ChangeRequestController" tabstyle="EP_ChangeRequest__c" id="page">
    <c:EP_PerformanceMonitor />

    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/overlay.css')}" id="overlycss" />
    <apex:stylesheet value="{!$Resource.EP_ChangeRequestCSS}" id="pagecss"/>
    
    <apex:includeScript value="{!URLFOR($Resource.EP_jQuery, 'lib/jquery-1.11.3.min.js')}" id="jquery"/>
    <apex:includeScript value="{!URLFOR($Resource.EP_ChangeRequestJS,'EP_ChangeRequestJS.js')}" id="pagejs"/>
    
    <apex:sectionHeader title="{!$Label.EP_Change_Request_Section_Header_Title}" subtitle="{!newObjInstance['Name']}" id="sectHeader"/>
    <apex:form id="crForm">
        <apex:actionFunction action="{!submit}" name="submitForm"   oncomplete="showHideSubmitButton();" 
        		rerender="msg" status="statusOverlay" id="actFunSubmit"/><!-- rerender="tabId,msg" status="showDiv"-->
        <apex:pageMessages id="msg"/>   
        <!-- Overlay -->
        <apex:actionStatus id="statusOverlay" startStyleClass="overlay"></apex:actionStatus>
        <apex:pageBlock title="{!$Label.EP_Change_Request_Header_Message}" id="crpageBlock" rendered="{!NOT(isError)}" mode="edit">
            <apex:actionstatus id="showDiv" onstart="javascript:showDiv();" onstop="javascript:hideDiv();"/>
            <apex:pageBlockSection title="{!$Label.EP_Change_Request_Page_Block_Section_Title}" columns="1" id="reqLineSection" collapsible="false">
                    <apex:pageBlockTable value="{!allFieldValues}" var="reqline" id="tabId">
                        <apex:column headerValue="{!$Label.EP_Change_Request_Field_Column}" id="fieldNameHeader"> <!-- value="{!reqline.crlobj.EP_Source_Field_Label__c}"-->
                            <apex:outputPanel layout="block" styleClass="hideField" id="blk">
                                 <apex:pageBlockSection columns="1" collapsible="false" showHeader="false" id="sect">
                                          <apex:outputField value="{!recobj[reqline.crlobj.EP_Source_Field_API_Name__c]}" 
                                          		style="border-bottom: 0px none;text-align: left;font-family: Arial,Helvetica,sans-serif;font-size: 110%;" 
                                          id="dynamicField"/>
                                 </apex:pageBlockSection>
                            </apex:outputPanel>
                        </apex:column>
                        <apex:column headerValue="{!$Label.EP_Change_Request_Current_Field_Value}" id="originalVal" rendered="{!$CurrentPage.parameters.op!='NEW'}">
                            <apex:outputField value="{!recobj[reqline.crlobj.EP_Source_Field_API_Name__c]}" id="orgValInput"/>
                        </apex:column>
                        <apex:column headerValue="{!$Label.EP_Change_Request_New_Field_Value}" id="newVal">
                            <apex:inputField value="{!newObjInstance[reqline.crlobj.EP_Source_Field_API_Name__c]}" id="newValInput" 
                                         required="{!reqline.isfieldRequired}"
                                         rendered="{!NOT(reqLine.isRestrictedPickist)}"
                                         styleClass="Field-{!reqline.fieldType}"
                                         onchange="copyNewValue('{!reqline.fieldType}','{!$Component.newValuehidden}'
                                         ,this, '{!reqline.crlobj.EP_Original_Value__c}', '{!$Component.newVal}')"/>  
                                         
                            
                                       
                        <apex:outputPanel rendered="{!(reqline.fieldType == refType)}" id="panel1">
                            <a href="javascript:clearLookup('{!reqline.crlobj.EP_Original_Value__c}','{!$Component.newValInput}', '{!$Component.changeCHeck}', '{!$Component.imgID}')">Clear</a>    
                        </apex:outputPanel>
                        <apex:outputpanel layout="block" styleclass="{!IF(reqline.isfieldRequired, 'requiredInput','')}" 
                        	rendered="{!(reqLine.isRestrictedPickist)}" id="panel2">
                            <apex:outputpanel layout="block" styleclass="{!IF(reqline.isfieldRequired, 'requiredBlock','')}" id="restrictPickPanel"></apex:outputpanel>
                            <apex:selectList value="{!newObjInstance[reqline.crlobj.EP_Source_Field_API_Name__c]}"  
                                        required="{!reqline.isfieldRequired}"
                                        rendered="{!(reqLine.isRestrictedPickist)}"
                                        size="1"
                                        onchange="copyNewValue('{!reqline.fieldType}','{!$Component.newValuehidden}'
                                        ,this, '{!reqline.crlobj.EP_Original_Value__c}', '{!$Component.newVal}')"
                                        id="restrictedList">
                                         
                                <apex:selectOptions value="{!reqline.restrictedPicklist}" id="restPick"/>
                            </apex:selectList>
                        </apex:outputpanel>
                        <apex:image value="/img/icon/custom51_100/pencil16.png" 
                            style="display:none" id="imgID" styleclass="pencilImg "/>
                        <apex:inputCheckbox value="{!reqline.isChanged}" style="display:none" id="changeCHeck" styleclass="changeTrack"/>
                        <apex:inputHidden value="{!reqline.crlobj.EP_New_Value__c}" id="newValuehidden"/>     
                        <apex:inputHidden value="{!reqline.crlobj.EP_Reference_Value__c}" id="newRefValuehidden"/>
                        <apex:inputHidden value="{!reqline.crlobj.EP_Original_Value__c}" id="newOrigRefValuehidden"/>
                      
                        </apex:column>
                    </apex:pageBlockTable>
                </apex:pageBlockSection>
            <apex:pageBlockButtons location="bottom" id="commButton">
                    <span id="btnsect">
                        <apex:commandButton value="{!$Label.EP_Save_Caption}" style="overflow: visible; line-height: 120%; min-width: 80px;" onclick="if (confirmSubmit('{!$Label.EP_Change_Request_Form_Submit_Message}')){submitForm(); } return false;" id="submitbtn"/>
                    </span>
                    <apex:commandButton value="{!$Label.EP_Cancel_Label}" id="cancel" style="overflow: visible; line-height: 120%; min-width: 80px;" action="{!cancel}" immediate="true" />
                    </apex:pageBlockButtons>
        </apex:pageBlock>
        <div id="pnlLoading"
           style="background: none repeat scroll 0 0 #FFFFFF; height: 100%; width: 100%; opacity: 0.7; filter: alpha(opacity = 70); 
           position: absolute; text-align: center; z-index: 200; padding-top: 50px; top:0; left:0; display:none;">
           <div style="position:fixed; top: 50%; left: 50%; margin-top: -45px; margin-left: -45px;">
               <apex:image value="/img/loading.gif" id="ldngimg"/>
           </div>
        </div>
    </apex:form>
    <script>
        $(document).ready(function(){
                showHideSubmitButton();
        });   
        
    </script>
</apex:page>