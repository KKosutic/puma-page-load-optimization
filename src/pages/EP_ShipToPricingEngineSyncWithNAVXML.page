<apex:page standardController="Account" extensions="EP_ShipToPricingEnginePayloadCtrlExtn" action="{!checkPageAccess}" contentType="application/xml" showChat="false" showHeader="false" sidebar="false" standardStylesheets="false">
    <customers>
        <customer>
            <seqId>{!custSeqId}</seqId>
            <identifier>
                <custId>{!Account.Parent.EP_Account_Id__c}</custId>
            </identifier>
            <custCatgry>{!Account.Parent.EP_Customer_Category__c}</custCatgry>
            <name>{!Account.Parent.Name}</name>
            <name2>{!Account.Parent.EP_Account_Name_2__c}</name2>
            <address>{!Account.Parent.BillingStreet}</address>
            <city>{!Account.Parent.BillingCity}</city>
            <phone>{!Account.Parent.Phone}</phone>
            <mobilePhone>{!Account.Parent.EP_Mobile_Phone__c}</mobilePhone>
            <currencyId>{!Account.Parent.CurrencyIsoCode}</currencyId>
            <langCode>{!Account.Parent.EP_Language_Code__c}</langCode>
            <cntryCode>{!Account.Parent.EP_Country_Code__c}</cntryCode>
            <billToCustId>{!Account.Parent.EP_BillToCustomerNr__c}</billToCustId>
            <fax>{!Account.Parent.Fax}</fax>
            <companyTaxId>{!Account.Parent.EP_Company_Tax_Number__c}</companyTaxId>
            <postCode>{!Account.Parent.BillingPostalCode}</postCode>
            <county>{!Account.Parent.BillingState}</county>
            <email>{!Account.Parent.EP_Email__c}</email>
            <website>{!Account.Parent.Website}</website>
            <pmtMthdId>{!Account.Parent.EP_Payment_Method_Code__c}</pmtMthdId>
            <reqPmtCrdLmt>{!Account.Parent.EP_Recommended_Credit_Limit__c}</reqPmtCrdLmt>
            <!-- <reqPmtTrm>{!Account.Parent.EP_Requested_Payment_Terms__c}</reqPmtTrm>
            <pckgdRqstdPymntTerm>{!Account.Parent.EP_Requested_Packaged_Payment_Term__c}</pckgdRqstdPymntTerm>-->
            <reqPmtTrm>{!paymentTermCode}</reqPmtTrm>
            <pckgdRqstdPymntTerm>{!packagedPaymentTermCode}</pckgdRqstdPymntTerm>
            <billBasis>{!Account.Parent.EP_Billing_Basis__c}</billBasis>
            <billMthd>{!Account.Parent.EP_Billing_Method__c}</billMthd>
            <billFrqncy>{!Account.Parent.EP_Billing_Frequency__c}</billFrqncy>
            <billConsolBasis>{!Account.Parent.EP_Invoice_Consolidation_basis__c}</billConsolBasis>
            <blocked>{!Account.Parent.EP_Block_Status__c}</blocked>
            <isElgblForRebate>{!Account.Parent.EP_Rebate_Eligibity__c}</isElgblForRebate>
            <dlvryType>{!Account.Parent.EP_Delivery_Type__c}</dlvryType>
            <manualInvcingAllowed>{!Account.Parent.EP_ManualInvoicingAllowed_NAV__c}</manualInvcingAllowed>
            <salesPersonCode>{!Account.Parent.EP_Cstmr_Owner_Alias__c}</salesPersonCode>
            <isVatExempted>{!Account.Parent.EP_VAT_Exempted__c}</isVatExempted>
            <isAdditionalTaxExempted>{!Account.Parent.EP_Company_Is_Tax_Exempt__c}</isAdditionalTaxExempted>
            <deferInvoice>{!Account.Parent.EP_Sys_Defer_Invoice__c}</deferInvoice>
            <!--<deferUpperLmt>{!Account.EP_Defer_Upper_Limit__c}</deferUpperLmt>-->
            <deferUpperLmt>{!deferUpperLimit}</deferUpperLmt> 
            <allowRlsOnPymntProof>{!Account.Parent.EP_Allow_release_on_proof_of_payment__c}</allowRlsOnPymntProof>
            <acntStmntFrqncy>
                <daily>{!Account.Parent.EP_Sys_Account_Statement_Periodicity_D__c}</daily>
                <weekly>{!Account.Parent.EP_Sys_Account_Statement_Periodicity_W__c}</weekly>
                <fortnightly>{!Account.Parent.EP_Sys_Account_Statement_Periodicity_F__c}</fortnightly>
                <monthly>{!Account.Parent.EP_Sys_Account_Statement_Periodicity_M__c}</monthly>
            </acntStmntFrqncy>
            <stmntTypAll>{!Account.Parent.EP_Sys_All_Items_Account_Statements__c}</stmntTypAll>
            <stmntTypOpen>{!Account.Parent.EP_Sys_Open_Items_Account_Statements__c}</stmntTypOpen>
            <combinedInvoicing>{!Account.Parent.EP_Combined_Invoicing__c}</combinedInvoicing>
            <priceConsolBasis>{!Account.Parent.EP_Price_Consolidation_Basis__c}</priceConsolBasis>
            <businessSegment>{!Account.Parent.EP_Business_Segment_Code__c}</businessSegment>
            <businessChannel>{!Account.Parent.EP_Business_Channel_Code__c}</businessChannel>
            <exciseDuty>{!Account.Parent.EP_Excise_duty__c}</exciseDuty>
            <displayOrdrsInPrgrs>{!Account.Parent.EP_Show_Orders_in_Progress__c}</displayOrdrsInPrgrs>
            <isPONrVsibl>{!Account.Parent.EP_Is_Customer_Reference_Visible__c}</isPONrVsibl>
            <isPOReq>{!Account.Parent.EP_Is_Customer_Reference_Mandatory__c}</isPOReq>
            <poReqBy>{!Account.Parent.EP_Customer_s_PO_number_required_by__c}</poReqBy>
            <poNr>{!Account.Parent.EP_Customer_PO_Number__c}</poNr>
            <poValidFrmDate>
                <!--TR DateTime 59843 Start-->
                <!--<apex:outputText value="{0,date,yyyy-MM-dd HH:mm:ss}">-->              
                <apex:outputText value="{0,date,yyyy-MM-dd}">
                <!--TR DateTime 59843 End-->
                    <apex:param value="{!Account.Parent.EP_Valid_From_date__c}" />
                </apex:outputText>
            </poValidFrmDate>
            <poValidToDate>
                <!--TR DateTime 59843 Start-->
                <!--<apex:outputText value="{0,date,yyyy-MM-dd HH:mm:ss}">-->              
                <apex:outputText value="{0,date,yyyy-MM-dd}">
                <!--TR DateTime 59843 End-->
                    <apex:param value="{!Account.Parent.EP_Valid_To_date__c}" />
                </apex:outputText>
            </poValidToDate>
            <custActivationDate>
                <!--TR DateTime 59843 Start-->
                <!--<apex:outputText value="{0,date,yyyy-MM-dd HH:mm:ss}">-->              
                <apex:outputText value="{0,date,yyyy-MM-dd}">
                <!--TR DateTime 59843 End-->
                    <apex:param value="{!Account.Parent.EP_Customer_Activation_Date__c}" />
                </apex:outputText>
            </custActivationDate>
            <status>{!Account.Parent.EP_NewStatus__c}</status>
            <shipToAddresses>
                <shipToAddress>
                    <seqId>{!shipToSeqId}</seqId>
                    <identifier>
                        <shipToId>{!Account.EP_Account_Id__c}</shipToId>
                        <!--<custId>{!shipTo.shipToAddress.EP_Account_Id__c}</custId>-->
                        <custId>{!Account.Parent.EP_Account_Id__c}</custId>
                    </identifier>
                    <name>{!Account.Name}</name>
                    <name2>{!Account.EP_Account_Name_2__c}</name2>
                    <cntryCode>{!Account.EP_Country_Code__c}</cntryCode>
                    <address>{!Account.ShippingStreet}</address>
                    <postCode>{!Account.ShippingPostalCode}</postCode>
                    <city>{!Account.ShippingCity}</city>
                    <phone>{!Account.Phone}</phone>
                    <fax>{!Account.Fax}</fax>
                    <email>{!Account.EP_Email__c}</email>
                    <county>{!Account.ShippingState}</county>
                    <isVMI>{!Account.EP_Is_VMI_ShipTo__c}</isVMI>
                    <shipToType>{!Account.EP_Ship_To_Type__c}</shipToType>
                    <dlvryType>{!Account.EP_Delivery_Type__c}</dlvryType>
                    <transportManagmnt>{!Account.EP_Transportation_Management__c}</transportManagmnt>
                    <isElgblForRebate>{!Account.EP_Rebate_Eligibity__c}</isElgblForRebate>
                    <duty>{!Account.EP_Duty__c}</duty>
                    <isPOReq>{!Account.EP_Is_Customer_Reference_Mandatory__c}</isPOReq>
                    <poReqBy>{!Account.EP_Customer_s_PO_number_required_by__c}</poReqBy>
                    <poNr>{!Account.EP_Customer_PO_Number__c}</poNr>
                    <poValidFrmDate>
                        <!--TR DateTime 59843 Start-->
                        <!--<apex:outputText value="{0,date,yyyy-MM-dd HH:mm:ss}">-->              
                        <apex:outputText value="{0,date,yyyy-MM-dd}">
                        <!--TR DateTime 59843 End-->
                            <apex:param value="{!Account.EP_Valid_From_date__c}" />
                        </apex:outputText>
                    </poValidFrmDate>
                    <poValidToDate>
                        <!--TR DateTime 59843 Start-->
                        <!--<apex:outputText value="{0,date,yyyy-MM-dd HH:mm:ss}">-->              
                        <apex:outputText value="{0,date,yyyy-MM-dd}">
                        <!--TR DateTime 59843 End-->
                            <apex:param value="{!Account.EP_Valid_To_date__c}" />
                        </apex:outputText>
                    </poValidToDate>
                </shipToAddress>
            </shipToAddresses>
            <clientId>{!Account.EP_Puma_Company_Code__c}</clientId>
        </customer>
    </customers>
</apex:page>