///* 
//  @Author <Spiros Markantonatos>
//   @name <EP_SelectTankDipSitePageExtensionClass_Test>
//   @CreateDate <06/11/2014>
//   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
//   @Version <1.0> 
//*/
//@isTest
public class EP_SelectTankDipSitePageExtClass_Test {
//    private static user currentrunningUser;
//    private static List<Account> accList; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    private static EP_Meter__c ExistingMeter; 
//    private Static Product2 currentProduct;
//    /**************************************************************************
//    *@Description : This method is used to create Data.                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();   
//         accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//         TankInstance= EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//         Account acc =accList.get(0);
//         acc.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         update acc;
//         currentProduct=EP_TestDataUtility.createTestRecordsForProduct();
//         ExistingMeter=EP_TestDataUtility.createTestRecordsForMeter(String.valueof(accList.get(0).id),currentProduct.id);
//    }
//    
//    
//    /**************************************************************************
//    *@Description : This method is used to .                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void Inforet(){ 
//         init();   
//         Test.startTest();
//           system.runas(currentrunningUser){     
//             PageReference pageRef = Page.EP_SelectTankDipSitePage;
//             Test.setCurrentPage(pageRef);//Applying page context here 
//             ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//             EP_SelectTankDipSitePageExtensionClass controller = new EP_SelectTankDipSitePageExtensionClass();
//             controller.retrieveSiteInformation(); 
//             system.assertEquals(controller.shipTos.size(),1);        
//             controller.cancel();
//          }
//         Test.stopTest();  
//    }    
//    
//    /**************************************************************************
//    *@Description : This method is used to .                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void Inforet2(){ 
//        init();   
//        
//        Test.startTest();    
//             system.runas(currentrunningUser) {
//                // Add placeholder tank dips
//                // Create test tank dips
//                DateTime dtNow = System.Now();
//                DateTime dtPlaceholderReadingDateTime = EP_PortalLibClass.returnLocationDateTimeFromInteger(dtNow.Year(), dtNow.Month(), dtNow.Day(), dtNow.Hour(), dtNow.Minute(), 0);
//                
//                accList.get(0).EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                accList.get(0).EP_Ship_To_UTC_Timezone__c = 'UTC+00:00';
//                update accList;
//                
//                List<EP_Tank_Dip__c> testTankDips = new List<EP_Tank_Dip__c>();
//                testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                            EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime));
//                testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                            EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime.addDays(-1)));
//                testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                            EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime.addDays(-2)));
//                testTankDips = EP_TestDataUtility.createTestEP_Tank_Dip(testTankDips);
//            
//                EP_Tank_Dip__c currentDip = EP_TestDataUtility.createTestRecordsForTankDip(TankInstance.id);
//                upsert currentDip ;
//                accList[0].EP_Status__c = EP_Common_Constant.INACTIVE_SHIP_TO_STATUS;
//                update accList[0];
//                accList[0].EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                update accList[0];
//            }
//        Test.stopTest();  
//    }
//        
}