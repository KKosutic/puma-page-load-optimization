/**
 * String builder mockup class.
 *<br><br>
 * Idea is to have an object which behaves similar to string builder in other programming languages.
 * Though, it does not support operations over character sequences, it is recommended to use this class whenever
 * one needs to concatinate huge number of strings (or any concatination in any kind of loops).
 *<br><br>
 * Example:
 * <code>
 *  StringBuilder bldr = new StringBuilder();
 *  System.assertEquals(
 *   'abc',
 *   bldr.append('a')
 *    .append('b')
 *    .append('c')
 *    .toString()
 *  );
 *<br>
 *  StringBuilder bldr2 = new StringBuilder();
 *  System.assertEquals(
 *   'abcdef',
 *   bldr2.append(bldr)
 *    .append('d')
 *    .append('e')
 *    .append('f')
 *    .toString()
 *  );
 *
 * bld2.clear();
 * System.assertEquals(
 *  '',
 *  bldr2.toString()
 * )
 * </code>
 */
global class StringBuilder { 
  /**
   * Defauult delimiter for String.join method (currently it is empty string)
   */
  global static final String DEFAULT_DELIMITER = '';

  /**
   * Buffer containing string elements which will be concatinated in toString() methods.
   * Order in this list will determine final result in a way that it will sequentialy concatinate each element.
   * Elements are concatinated using String.join internal SF apex method.
   *
   * @see DEFAULT_DELIMITER
   * @see toString()
   */
  @TestVisible
  protected List<String> buffer;

  /**
   * Final string length if using plain toString method.
   *<br><br>
   * <b>Warning:</b>
   * If using specific delimiter in toString() method invocation, final string length value might be different than this.
   */
  @TestVisible
  global Integer length { get; private set; }

  /**
   * Default constructor.
   *
   * Might be used as:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder().append('a');
   *  System.assertEquals(
   *   'a',
   *   bldr.toString()
   *  );
   * </code>
   */
  global StringBuilder() {
    this.buffer = new List<String>();
    this.length = 0;
  }

  /**
   * Constructor with initial value.
   *
   * Might be used as:
   *<br>
   * <code>
   *  System.assertEquals(
   *   'a',
   *   new StringBuilder('a').toString()
   *  );
   * </code>
   *
   * @param initialValue Initial string value for builder.
   */
  global StringBuilder(String initialValue) {
    this();
    this.append(initialValue);
  }

  /**
   * Appends string <code>s</code> to be concatinated with {@link DEFAULT_DELIMITER}.
   * <br><br>
   * <b>Important:</b>
   *<br>
   * The final result is determined by the order of appended strings or builders.
   *<br><br>
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append('a')
   *   .append('b');
   *
   *  System.assertEquals(
   *   'ab',
   *   bldr.toString()
   *  );
   * </code>
   *
   * @param s String to be appended
   *
   * @return This StringBuilder instance
   */
  global StringBuilder append(String s) {
    if (s != null) {
      this.buffer.add(s);
      this.length += s.length();
    }

    return this;
  }

  /**
   * Appends whole string ment to be built with <code>strBldr</code> in optimized way (does not create any aditional strings).
   * <br><br>
   * <b>Important:</b>
   *<br>
   * The final result is determined by the order of appended strings or builders.
   *<br><br>
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append('a')
   *   .append('b');
   *
   *  StringBuilder bldr2 = new StringBuilder();
   *  bldr2
   *   .append('a')
   *   .append(bldr);
   *  System.assertEquals(
   *   'aab',
   *   bldr2.toString()
   *  );
   * </code>
   *
   * @param strBldr Anothers StringBuilder final result to be appended
   *
   * @return This StringBuilder instance
   */
  global StringBuilder append(StringBuilder strBldr) {
    if (strBldr != null) {
      this.buffer.addAll(strBldr.buffer);
      this.length += strBldr.length;
    }

    return this;
  }

  /**
   * Appends each of String element in given set by default order. (if want to perserve order use append with List<String> argument)
   * <br><br>
   * <b>Important:</b>
   *<br>
   * The final result is determined by the order of appended strings or builders.
   *<br><br>
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append(new Set<String> { 'a', 'b', 'c' });
   *
   *  System.assertEquals(
   *   'abc',
   *   bldr.toString()
   *  );
   * </code>
   *
   * @param stringSet Set of strings you wish to add as factors to string builder
   *
   * @return This StringBuilder instance
   */
  global StringBuilder append(Set<String> stringSet) {
    if (stringSet != null) {
      this.buffer.addAll(stringSet);
      for (String s : stringSet) {
        if (s != null) {
          this.length += s.length();
        }
      }
    }

    return this;
  }

  /**
   * Appends each of String element in given list by its order.
   * <br><br>
   * <b>Important:</b>
   *<br>
   * Order of appended strings or builders determens final result.
   *<br><br>
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append(new List<String> { 'a', 'b', 'c' });
   *
   *  System.assertEquals(
   *   'abc',
   *   bldr.toString()
   *  );
   * </code>
   *
   * @param stringList List of strings you wish to add as factors to string builder
   *
   * @return This StringBuilder instance
   */
  global StringBuilder append(List<String> stringList) {
    if (stringList != null) {
      this.buffer.addAll(stringList);
      for (String s : stringList) {
        if (s != null) {
          this.length += s.length();
        }
      }
    }

    return this;
  }

  /**
   * Clears current string ment to be built.
   *
   * <br><br>
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append('a')
   *   .append('b')
   *   .clear();
   *
   *  System.assertEquals(
   *   '',
   *   bldr.toString()
   *  );
   * </code>
   */
  global void clear() {
    this.buffer.clear();
    this.length = 0;
  }

  /**
   * Builds final string result concatinating elements with {@link DEFAULT_DELIMITER}
   *
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append('a')
   *   .append('b');
   *
   *  System.assertEquals(
   *   'ab',
   *   bldr.toString()
   *  );
   * </code>
   */
  global override String toString() {
    return toString(DEFAULT_DELIMITER);
  }

  /**
   * Builds final string result concatinating elements with given delimiter.
   *
   * Usage:
   *<br>
   * <code>
   *  StringBuilder bldr = new StringBuilder();
   *  bldr
   *   .append('a')
   *   .append('b');
   *
   *  System.assertEquals(
   *   'a\tb',
   *   bldr.toString('\t')
   *  );
   * </code>
   *
   * @param delimiter String value for String.join SF Apex method.
   */
  global String toString(String delimiter) {
    return String.join(this.buffer, delimiter);
  }
}