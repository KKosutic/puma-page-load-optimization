/* This class is worked upon iterative hence please don't review this class as the code is not up to the expections
    @Author <Accenture>
    @name <EP_OrderModificationPageController>
    @Description <This is class act as a controller when we try to modify order page>
    @Version <1.0>
*/
public without sharing class EP_OrderModificationPageController {
    //Domain, Service and Mapper variables
    public EP_OrderPageContext ctx {get;set;}
    public EP_OrderControllerHelper hlp;
    public EP_OrderModificationControllerHelper modfiyHlp;
    public EP_OrderLinesAndSummaryHelper hlp3and4;
    EP_RunMapper runMapper;
    
    //PRIVATE CLASS VARIABLES
    private static final String CLASSNAME = 'EP_OrderModificationPageController';
    private Map<Id,Double> itemIdTotalPriceMap;
    public Date deldate {get;set;}
    private set<Id> orderItemDeleteSet;
    private map<Id,list<OrderItem>> childItemsMap;
    public String productNotAvailableMsg{get;set;}
    //Set 
    public Set<Id> existingOrderProductSet;
    
    //public getter setters
    //public String ctx.ctx.strSelectedDate {get;set;}
    //public String strCustomerCurrencyCode {get;set;}
    //public Boolean isCountryNonMixing {get;set;}
    
    private EP_Stock_Holding_Location__c selectedPickupDetail;

    public EP_OrderModificationPageController( Apexpages.StandardController stdController) {
        Order orderObj = (Order)stdController.getRecord();   
        init(orderObj);
        system.debug('Order is ' + orderObj);
    }  
    @TestVisible
    private void init(Order orderObj){
        ctx = new EP_OrderPageContext(orderObj.Id);
        ctx.transportAccount = new List<SelectOption>();
        ctx.isPriceCalculated = true;
        hlp = new EP_OrderControllerHelper(ctx);
        hlp3and4 = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.isModification = true;
        modfiyHlp = new EP_OrderModificationControllerHelper(ctx);
        existingOrderProductSet = new Set<Id>();
    }
    
    public Map<String,csord__Order__c> orgPckgdOrderMap{
        get{
            return EP_PortalOrderUtil.orgPckgdOrderMap(ctx.newOrderRecord.AccountId__c);
        }
        set;        
    }

    public Boolean isDeliveryOrder {
        get {
            return ctx.isDeliveryOrder;
        }
    }

    public Boolean showExportedCheckBox{get;set;}
    public Boolean isErrorInPage {
        get {
            Boolean hasError = FALSE;
            if (ApexPages.getMessages() != NULL) {
                hasError = ApexPages.hasMessages(ApexPages.severity.ERROR);
            }
            return hasError;
        }
    }
    public Boolean isPrepaymentCustomer {
        get {
            return (ctx.newOrderRecord.EP_Payment_Term__c != NULL && ctx.newOrderRecord.EP_Payment_Term__c.equalsIgnoreCase(EP_Common_Constant.PREPAYMENT));}
    }
    
    public Boolean showOrderConfirmationTab {
        get {
            return (ctx.strSelectedOrderID != NULL);
        }
    }
    
    public Boolean showAddNewLineButton {
        get {
            return (ctx.isOprtnlTankAvail && hlp3and4.getListShipToTanks().size() > 1) || (!ctx.isOprtnlTankAvail && hlp3and4.getProductOptions().size() > 1);
        }
        set; 
    }
    
    public Boolean showNextButton {
        get {
            Boolean proceed = ((ctx.intOrderCreationStepIndex > 0 && ctx.intOrderCreationStepIndex < 4) ||( ctx.intOrderCreationStepIndex ==4 && (ctx.isConsignmentOrder || ctx.isDummy)) || ( ctx.intOrderCreationStepIndex ==4 && ctx.isOrderEntryValid && ctx.isPriceCalculated && !ctx.isPricingRequired) ) ;
            return (proceed && !ctx.hasAccountSetupError && !isErrorInPage);
        }set;
    }
    
    public Boolean showSpinButton{
        get {
            Boolean proceed = (ctx.intOrderCreationStepIndex == 4 && ctx.isPriceCalculated && ctx.isPricingRequired && !(ctx.isConsignmentOrder || ctx.isDummy) && ctx.isOrderEntryValid && ctx.listofOrderWrapper.size() >0);
            return (proceed && !ctx.hasAccountSetupError && !isErrorInPage);
        }
        set;
    }    
    public Boolean showBackButton {
        get {
            if( isDeliveryOrder ){
                return (ctx.intOrderCreationStepIndex > 2 && ctx.intOrderCreationStepIndex < 6 );
            }
            else{
                return (ctx.intOrderCreationStepIndex > 3 && ctx.intOrderCreationStepIndex < 6 );
            }
        }
        set;
    }
    public Boolean showCalculateButton  {
        get {
            
            return (ctx.intOrderCreationStepIndex == 4  && ctx.isOrderEntryValid &&!ctx.isPriceCalculated && !(ctx.isConsignmentOrder || ctx.isDummy) && ctx.listofOrderWrapper.size() > 0);
        }
        set;
    }
    public Boolean showCancelButton {
        get {
            return ((ctx.intOrderCreationStepIndex == 4 && ctx.isPriceCalculated && !ctx.isPricingRequired)||  (ctx.intOrderCreationStepIndex != 4));
        }
        set;
    }
    
   
    public Double totalOrderCost {
        get {
            Double dblTotalCost = 0;
            if( !ctx.isPricingRequired ){
                dblTotalCost = modfiyHlp.gettotalOrderCost();
            }else{
                dblTotalCost = ctx.totalCostPE;        
            }
            return dblTotalCost;
        }
    }
    
    public String userCurrencyFormat {
        get {
            return EP_GeneralUtility.getCurrencyFormat();
        }
    }
    
    public Boolean supplyLocTransportEditable{
        get{
            return (EP_Common_Constant.CSC_PROFILES.contains(ctx.profileName) || EP_Common_Constant.BSM_GM_PROFILES.contains(ctx.profileName) || EP_Common_Constant.ADMIN_PROFILES.contains(ctx.profileName)  );
        }set;
    }    
    
    public List < EP_OrderPageContext.OrderSummaryWrapper > orderSummaryItems {
        get {
            return hlp.getOrderSummaryItems();
        }
    }
      
    
    /*
Guard Conditions for Cancelling/Modifying the order
Method to determine whether the order is editable or not
*/
    public Boolean isOrderEditable(csord__Order__c ord)
    {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','isOrderEditable');
        return ctx.isOrderEditable();
    }
    /*
This method is used to load details of selected sellTo/billTo Account
*/
    public void loadAccountOrderDetails() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','loadAccountOrderDetails');
        try {
            if (String.isBlank(ctx.strSelectedOrderID)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, EP_Common_Constant.ORDERIDNOTPROVIDED)); 
            }           
            else{
                ctx.loadAccountOrderDetails();
                if(!isDeliveryOrder){       
                    next();     
                }
            }   
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        }
    }

    
    
    
    /* 
*Load exsisting order Items
*/
    public void loadExistingOrderItems()
    {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','loadExistingOrderItems');
        try{
            ctx.loadExistingOrderItems();
            // modfiyHlp.calculateTotalPrice(ctx.childItemsMap);Defect 57495
            //updateOrderItemLines();
        }
        catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.LOAD_EXISTING_ORDER_ITEMS, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /*
This method is used to take one step back in the wizard.
*/
    public void back() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','back');
        try {
            ctx.intOrderCreationStepIndex--;
            //Changes for #60147
            ctx.pricingPollerCount = 0;
            //Changes for #60147 End
            ctx.hasAccountSetupError = FALSE;   
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        }
    }
    
   
    @TestVisible
    private void showMessageSupplyLocationMissing(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationPageController','showMessageSupplyLocationMissing');
        ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.INFO, Label.EP_No_SupplyLocation_At_SellTo);
        ApexPages.addMessage(msg);
    }
    /*
This method is used to go one step ahead.
*/
    public void next() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','next');
        try {
            if (ctx.intOrderCreationStepIndex == 2){
                modfiyHlp.loadStep2();
                if(ctx.hasAccountSetupError){
                    showNextButton = FALSE;
                } 
            }
            if (ctx.intOrderCreationStepIndex == 3){
                modfiyHlp.loadStep3();
            }
            if (ctx.intOrderCreationStepIndex == 4){
                modfiyHlp.loadStep4();
            }
            ctx.intOrderCreationStepIndex++;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            System.debug('******'+e.getMessage()+'**********'+e.getStackTraceString());
        }
    }
     
    /*
/*
Method to set Run on order
*/    
    public void setRun() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','setRun');
        ctx.setRun();
    }     
    
    /*
This method is used remove order line item.
*/
    public void removeOrderLineItem() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','removeOrderLineItem');
        List<OrderItem> itemToDel = new List<OrderItem>();
        try {
            hlp.removeOrderLineItem();
            ctx.isPriceCalculated = false;
            modfiyHlp.updateOrderItemLines();

        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        }
    }
      
    
    /*
This method is used to add new order line item.
*/
    public void addNewLine() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','addNewLine');
        try {
            hlp3and4.addNewLine();
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        }
    }
    
    /*
This method is used to cancel the order
Gautam : need to ask Functionality
*/
    public PageReference cancel() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','cancel');
        SavePoint sp = Database.setSavepoint();
        Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        PageReference ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
        try {
             //if(ctx.isPricingRequired){ Defect #58835
                modfiyHlp.doCancel();
            //} Defect #58835
            if (ctx.strSelectedAccountID != NULL) {
                ref = new PageReference(EP_Common_Constant.SLASH + ctx.strSelectedAccountID);
            }
        } catch (Exception e) {
            database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        }
        ref = new PageReference(EP_Common_Constant.SLASH + ctx.strSelectedOrderID);
        return ref;
    }
    /*
This method is used to cancel the order when available funds is not available
*/
    public PageReference CancelOrder() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','doCancelOrder');
        pagereference ref;
        try {
            EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doCancelOrder');
            ref = hlp.setOrderStatustoCancelled();
        } catch (Exception ex) {
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
                EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.CANCEL_ORD_MTD, CLASSNAME, ApexPages.Severity.ERROR);
            }
        return ref;
        
    }
    
    /*
This method is used to dispaly page containing overdue invoice details.
*/
    public PageReference openInvoicesOverduePage() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','openInvoicesOverduePage');
        PageReference pageRef;
        try {
            pageRef = new PageReference(EP_Common_Constant.PO_INVOICE_OVERDUE_PAGE + ctx.orderAccount.Id);
            pageRef.setRedirect(true);
        } catch (Exception e) {
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.OPEN_INVOICE_ORDERDUE, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return pageRef;
    }
    
    /*
    This method is used to submit the order.
    */
    
    public PageReference submit() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','submit');
        PageReference ref = NULL;
        List < csord__Order_Line_Item__c > aOrderItems = new List < csord__Order_Line_Item__c > ();
        SavePoint sp = Database.setSavepoint();
        List < Database.UpsertResult > results = new List < Database.UpsertResult > ();
        list<Database.DeleteResult> delResult = new list<Database.DeleteResult>();
        try {
        	//Defect:-57628--Start
            ctx.orderService.doSubmitActions(); 
            //Defect:-57628--End
            results.add(Database.upsert(ctx.newOrderRecord, TRUE));
            ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);  
            system.debug('----'+ctx.intOrderCreationStepIndex);
            aOrderItems = ctx.getOrderLineItemsFromOrderWrapper();
            if (!aOrderItems.isEmpty() && !ctx.isPricingRequired) {
                results = Database.upsert(aOrderItems, FALSE); 
            }            
            
            //State machine is called to get the appropriate status 
            EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_EDIT);           
            ctx.orderService.setOrderStatus(orderEvent);
            ctx.newOrderRecord.Status__c = ctx.orderDomainObj.getStatus();
            ref = hlp.updateOrder(orderEvent);
            ctx.orderService.doPostStatusChangeActions();
             
        } 
        catch (Exception ex){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + EP_Common_Constant.ERR_OCCURED));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.SUBMIT_MTD, CLASSNAME, ApexPages.Severity.ERROR);
            return null; 
        }
        return ref;
    }
    
    /* 
This Method is Used to save order with draft status
*/
    public PageReference saveOrderAsDraft(){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','saveOrderAsDraft');
        return hlp.saveOrderAsDraft();
        
    }
    /*
This method check availability of products at new Supply Location
*/
    public void isProductAvailable(){
        productNotAvailableMsg = EP_Common_Constant.BLANK;
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','blnIsProductAvailable');
        try {
            modfiyHlp.loadExistingProductSet();
            Set<Id> availProdAtSuppLoc = hlp3and4.getAvailableProducts(ctx.strSelectedPickupLocationID);
            ctx.prodNotAvailAtSuppLoc = !(availProdAtSuppLoc.containsAll(ctx.existingOrderProductSet));
            if(ctx.prodNotAvailAtSuppLoc)
                productNotAvailableMsg = Label.EP_ProductNotAvail_At_New_SL;
               
        }
        catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.BLN_IS_PROD_AVAIL, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /*
Get Pricing
*/
    public void getPricing(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','getPricing');
        hlp.getPricing(); 
    }
    
    
    /*
Need Pricing
*/
    public void setPricingVariable(){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','setPricingVariable');
        ctx.isPriceCalculated = false;
    }
    
    /*
Update Supplier Contracts
*/
    public void doUpdateSupplierContracts() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doUpdateSupplierContracts');
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doUpdateSupplierContracts');
        hlp.doUpdateSupplierContracts();
    }
    /*
Calculate Price
*/
    public void calculatePrice(){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','calculatePrice');
        try{
            PageReference ref = NULL;
            modfiyHlp.updateOrderItemLines();
            hlp.setOrderDetails();
            hlp.setRelatedBulkOrderDetailsonPackagedOrder();
            update ctx.newOrderRecord;
            List < csord__Order_Line_Item__c > listOfOrderItems = new List < csord__Order_Line_Item__c > ();
            listOfOrderItems = ctx.getOrderItemsIdListFromWrapper();
            if (!listOfOrderItems.isEmpty()) {
                Database.upsert(listOfOrderItems, true);
            }
            ctx.orderService.calculatePrice(); 
            ctx.isPriceCalculated = true;
            ctx.isPricingRequired = true;
        }
        catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.CALCULATE_PRICE, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /*
    Display Product name and safe fill level on selection of tank
    */
    public void displayProductNameAndSafeFillLevel() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','displayProductNameAndSafeFillLevel');
        try{
            modfiyHlp.displayProductNameAndSafeFillLevel();
        } catch (Exception e) {
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA,EP_Common_Constant.DISPLAY_PROD_NAME, CLASSNAME, ApexPages.Severity.ERROR) ;            
        }    
    }

    public void updateOrderItemLines(){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','displayProductNameAndSafeFillLevel');
        try{
            modfiyHlp.updateOrderItemLines();
        }catch (Exception e) {
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA,EP_Common_Constant.DISPLAY_PROD_NAME, CLASSNAME, ApexPages.Severity.ERROR) ;            
        }    
    }
}