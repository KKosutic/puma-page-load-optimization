/**
 * @author <Pooja Dhiman>
 * @name <EP_InvoiceTriggerHandler>
 * @createDate <05/02/2016>
 * @description <This class handles requests from Invoice trigger> 
 * @version <1.0>
 */
public with sharing class EP_InvoiceTriggerHandler {

    private static final string CLASS_NAME = 'EP_InvoiceTriggerHandler';
    private static final string doBeforeInsert = 'doBeforeInsert';
    private static final string doAftereUpdate = 'doAftereUpdate';
    
    /*
        This method handles before insert requests from Invoice trigger
    */
    public static void doBeforeInsert(List<EP_Invoice__c> lNewInvoice){
      try{
       EP_InvoiceTriggerHelper.validateInvoice(lNewInvoice);
       }
       catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doBeforeInsert, 
                    CLASS_NAME,apexPages.severity.ERROR);
      }
    }
    
}