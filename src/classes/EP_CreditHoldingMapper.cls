/****************************************************************
@Author      Accenture                                          *
@name        EP_CreditHoldingMapper                             *
@CreateDate  1/11/2017                                          *
@Description contains soql for Credit Holding Object            *
@Version     1.0                                                *
****************************************************************/
public with sharing class EP_CreditHoldingMapper {
    
    private List<EP_Credit_Holding__c> CeditHoldingList;
    public static map<id,EP_Credit_Holding__c> mapIdCreditHolding;
    public static map<id,list<account>> mapIdCDnAccounts;
    public static map<id,list<EP_Holding_Association__c>> mapIDnHoldingAssociation;
    
    public EP_CreditHoldingMapper() {
         
    }

    public EP_CreditHoldingMapper(EP_CreditHoldingDomainObject creditHoldingDomainObject) {
        this.CeditHoldingList = creditHoldingDomainObject.getCreditHolding();  
    }
    
    /****************************************************************
    * @author       Accenture                                       *
    * @name         retriveCreditHolding                     *
    * @description  method to get credit Holding Record             *
    * @param        Credit Holding Id                               *
    * @return       EP_Credit_Holding__c                            *
    ****************************************************************/
    public  EP_Credit_Holding__c retriveCreditHolding(id creditHoldingId){
        EP_Credit_Holding__c objCreditHoldingRecord;
        if(mapIdCreditHolding == null ){
            mapIdCreditHolding = new map<id,EP_Credit_Holding__c>();
            for(EP_Credit_Holding__c objCreditHolding : [select id,EP_Seq_id__c,EP_holding_number__c,EP_enterprise_id__c,EP_company__c,EP_company__r.EP_Company_Code__c,name,
                                                        EP_Holding_Name_2__c,EP_address_line_1__c,EP_address_line_2__c,EP_city__c,EP_phone__c,EP_mobile_phone__c,EP_country_code__c,
                                                        EP_fax__c,EP_postal_code__c,EP_state__c,EP_email__c,EP_Status__c
                                                        from EP_Credit_Holding__c where id =: CeditHoldingList ]){
                
                mapIdCreditHolding.put(objCreditHolding.id, objCreditHolding);                 
            }

        }
        if(mapIdCreditHolding.containskey(creditHoldingId)){
            objCreditHoldingRecord =mapIdCreditHolding.get(creditHoldingId);
        }
        
        return objCreditHoldingRecord;
    } 

    /****************************************************************
    * @author       Accenture                                       *
    * @name         getHoldingObject                                *
    * @description  method to get credit Holding Record             *
    * @param        Credit Holding Id                               *
    * @return       EP_Credit_Holding__c                            *
    ****************************************************************/
    public static EP_Credit_Holding__c getHoldingObject(id creditHoldingId){
        
        return([select id,EP_Seq_id__c,EP_enterprise_id__c,EP_holding_number__c,EP_company__c,EP_company__r.EP_Company_Code__c,name,
                        EP_Holding_Name_2__c,EP_address_line_1__c,EP_address_line_2__c,EP_city__c,EP_phone__c,EP_mobile_phone__c,EP_country_code__c,
                        EP_fax__c,EP_postal_code__c,EP_state__c,EP_email__c,EP_Status__c
                        from EP_Credit_Holding__c where id =: creditHoldingId ]);
    } 
    
    /****************************************************************
    * @author       Accenture                                       *
    * @name         retriveCreditHoldingAccounts                    *
    * @description  method to get all Account associated to 
                    credit Holding Record                           *
    * @param        Credit Holding Id                               *
    * @return       list<account>                            *
    ****************************************************************/
    public  list<account> retriveCreditHoldingAccounts(id creditHoldingId){
        list<account> lstAccount;
        if(mapIdCDnAccounts == null ){
            mapIdCDnAccounts = new map<id,list<account>>();
            for(EP_Credit_Holding__c objCreditHolding : [select id,(select id,EP_Status__c from Accounts__r) 
                    from EP_Credit_Holding__c where id =: CeditHoldingList ]){
                
                if(!mapIdCDnAccounts.containskey(objCreditHolding.id)){
                    mapIdCDnAccounts.put(objCreditHolding.id, new list<account>());                 
                }
                mapIdCDnAccounts.get(objCreditHolding.id).addAll(objCreditHolding.Accounts__r);
            }
        }
        if(mapIdCDnAccounts.containskey(creditHoldingId)){
            lstAccount =mapIdCDnAccounts.get(creditHoldingId);
        }
        
        return lstAccount;
    } 
    
    /****************************************************************
    * @author       Accenture                                       *
    * @name         isHoldingAssociationExists                      *
    * @description  method to check whether Holding association
                    record exists against an credit holding record  *
    * @param        Credit Holding Id                               *
    * @return       boolean                          *
    ****************************************************************/
    public   boolean isHoldingAssociationExists(id creditHoldingId){
        boolean isHoldingExists =false;
        if(mapIDnHoldingAssociation == null ){
            mapIDnHoldingAssociation = new map<id,list<EP_Holding_Association__c>>();
            for(EP_Holding_Association__c objCreHoldAsso : [select id,EP_Holding_Name__c,EP_Date_of_Dissociation__c,EP_Date_of_Association__c from EP_Holding_Association__c where EP_Holding_Name__c =: CeditHoldingList ]){
                
                if(!mapIDnHoldingAssociation.containskey(objCreHoldAsso.EP_Holding_Name__c)){
                    mapIDnHoldingAssociation.put(objCreHoldAsso.EP_Holding_Name__c, new list<EP_Holding_Association__c>());                 
                }
                mapIDnHoldingAssociation.get(objCreHoldAsso.EP_Holding_Name__c).add(objCreHoldAsso);
            }
        }
        if(mapIDnHoldingAssociation.containskey(creditHoldingId)){
            isHoldingExists = mapIDnHoldingAssociation.get(creditHoldingId).size()>0? true: false;
        }
        
        return isHoldingExists;
    }
}