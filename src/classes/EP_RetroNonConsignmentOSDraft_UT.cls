@isTest
public  class EP_RetroNonConsignmentOSDraft_UT {
  @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_Order_State_Mapping__C> lOrderStateMapping = Test.loadData(EP_Order_State_Mapping__C.sObjectType, 'EP_Order_State_Mapping');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setOrderDomainObject_test() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordObj = EP_TestDataUtility.getSalesOrderDomainObject();
        Test.startTest();
        localObj.setOrderDomainObject(ordObj);
        Test.stopTest();  
        System.assertEquals(ordObj,localObj.order);       
    }

    static testMethod void doTransition_test() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getRetroVMINonConsignmentOrderPositiveScenario();
        //getRetroVMINonConsignmentOrderPositiveScenario();
        localObj.setOrderDomainObject(ordDomain);
        EP_OrderEvent currentEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(ordDomain,currentEvent);
        System.debug('@@ status : '+ordDomain.getOrder().Status__c);
        Test.startTest();
        localObj.doTransition();
        Test.stopTest();  
        //No Assertion as the method only delegates to super class method ,hence adding a dummy assert.
        system.Assert(true);   
    }

    static testMethod void doOnEntry_ExpectedLoadingDateTest() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomainObject(ordDomain);
        csord__Order__c ord = ordDomain.getOrder();
        ord.EP_Loading_Date__c =  System.today();       
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();   
        System.assertNotEquals(null,ord.EP_Expected_Loading_Date__c);
    }

    static testMethod void doOnEntry_ActualDeliveryDateTimeTest() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomainObject(ordDomain);
        csord__Order__c ord = ordDomain.getOrder();
        ord.EP_Expected_Delivery_Date__c  =  System.today();       
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();   
        System.assertNotEquals(null,ord.EP_Actual_Delivery_Date_Time__c );
    }

    static testMethod void doOnEntry_EffectiveDateTest() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomainObject(ordDomain);
        csord__Order__c ord = ordDomain.getOrder();
        ord.EP_Order_Date__c =  System.today();       
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();   
        System.assertNotEquals(null,ord.EffectiveDate__c);
    }

    static testMethod void doOnExit_Test() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        csord__Order__c ord = ordDomain.getOrder();
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();  
        //No Assertion as the method has no processing logic,hence adding a dummy assert.
        system.Assert(true);      
    }
    static testMethod void isInboundTransitionPossible_Test() {
        EP_RetroNonConsignmentOSDraft localObj = new EP_RetroNonConsignmentOSDraft();       
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        csord__Order__c ord = ordDomain.getOrder();
        Test.startTest();
        boolean result =localObj.isInboundTransitionPossible();
        Test.stopTest(); 
        System.assert(result);         
    }
    static testMethod void getTextValue_Test() {
        Test.startTest();
        String result =EP_RetroNonConsignmentOSDraft.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Draft,result);   
    }
 }