/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTAnyToAccepted>
*  @CreateDate <03/02/2017>
*  @Description <Handles order status change from any status to Accepted>
*  @Version <1.0>
*/

public class EP_OSTAnyToAccepted extends EP_OrderStateTransition{
    
    static string classname = 'EP_OSTAnyToAccepted';

    public EP_OSTAnyToAccepted(){
        finalState =EP_OrderConstant.OrderState_Accepted;
        //system.debug('In Constructor: EP_OSTAnyToAccepted');
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTAnyToAccepted','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTAnyToAccepted','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTAnyToAccepted');
        return super.isRegisteredForEvent();       
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTAnyToAccepted','isGuardCondition');
            //system.debug('In isGuardCondition of EP_OSTAnyToAccepted');
            this.order.getOrder().EP_Sync_with_NAV__c = true;
            return true;               
        }
        
    }