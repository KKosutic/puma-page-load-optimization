/*
   @Author          Accenture
   @Name            EP_ATRController_UT - #59186
   @CreateDate      12/07/2017
   @Description     This Test class for EP_ATRController Class
   @Version         1.0
*/
@isTest
private class EP_ATRController_UT {
	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    	
    }

    
	@isTest
	private static void enableATRNegative_Test() {
		Test.startTest();
		EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_OUTBOUND_COMMUNICATIONS);  
		communicationSettings.Disable__c = true;
		update communicationSettings;
		String ValidMsg = 'ATR Can not be Enabled Since OUTBOUND COMMUNICATIONS is disabled for Org. Please uncheck "Disable Outbound Communications" in EP_CS_Communication_Settings__c custom setting';
		EP_ATRController atrController = new EP_ATRController();
		atrController.enableATR();
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean isValidMsg = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(ValidMsg)) isValidMsg = true;
		}
		Test.stopTest();
		system.assert(isValidMsg);
	}

	@isTest
	private static void enableATRPositive_Test() {
		Test.startTest();
		String ValidMsg = 'ATR is Enabled';
		EP_ATRController atrController = new EP_ATRController();
		atrController.enableATR();
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean isValidMsg = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(ValidMsg)) isValidMsg = true;
		}
		Test.stopTest();
		system.assert(isValidMsg);
	}


	@isTest
	private static void disableATRPositive_Test() {
		Test.startTest();
		String ValidMsg = 'ATR is Disabled';
		EP_ATRController atrController = new EP_ATRController();
		atrController.disableATR();
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean isValidMsg = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(ValidMsg)) isValidMsg = true;
		}
		Test.stopTest();
		system.assert(isValidMsg);
	}

	@isTest
	private static void updateATRSettingPositive_Test() {
		Test.startTest();
		EP_ATRController atrController = new EP_ATRController();
		atrController.updateATRSetting(true);
		EP_CS_Communication_Settings__c comSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
		Test.stopTest();
		System.assertEquals(comSettings.Disable__c,true);
	}

	@isTest
	private static void updateATRSettingNegative_Test() {
		Test.startTest();
		EP_ATRController atrController = new EP_ATRController();
		atrController.updateATRSetting(false);
		EP_CS_Communication_Settings__c comSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
		Test.stopTest();
		System.assertEquals(comSettings.Disable__c,false);
	}
	
	@isTest
	private static void runAutomaticTechnicalRetryBatchPositive_Test() {
		Test.startTest();
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		EP_ATRController atrController = new EP_ATRController();
		atrController.runAutomaticTechnicalRetryBatch();
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,2.0);
	}

	@isTest
	private static void runAutomaticTechnicalRetryBatchNegative_Test() {
		Test.startTest();
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,'SYNC'); 
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		EP_ATRController atrController = new EP_ATRController();
		atrController.runAutomaticTechnicalRetryBatch();
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,1.0);
	}
	
	@isTest
	private static void addPageMessgage_Test(){
		Test.startTest();
		String msgText = 'Test Message';
		EP_ATRController atrController = new EP_ATRController();
		atrController.addPageMessgage(msgText);
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean isValidMsg = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(msgText)) isValidMsg = true;
		}
		Test.stopTest();
		system.assert(isValidMsg);
	}
	
}