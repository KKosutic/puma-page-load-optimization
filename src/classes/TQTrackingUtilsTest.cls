/**
 *  Test Class for the TQTrackingUtils
 */
@isTest
private class TQTrackingUtilsTest {

    private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();
/*********************************************************************************************
     @Author <>
     @name <testGettFieldSetFields>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/    
    static testMethod void testGettFieldSetFields() {

        System.runAs(sysAdmUser){
            TQTrackingUtils.FieldSetSelectionCriteria params = new TQTrackingUtils.FieldSetSelectionCriteria();
            params.objectAPIName = 'Account';
            params.fieldSetName = 'FieldSet';
            params.enforceIsAccessible = false;
            params.enforceIsCreateable = false;
            params.enforceIsUpdateable = false;
            params.enforceIsRequired = false;

            Map<String, Schema.SObjectType> describe = TQTrackingUtils.getGlobalDescribe();

            System.assert(!describe.isEmpty());

            Schema.Describesobjectresult result = TQTrackingUtils.getObjectDescribe('Account');
            System.assert(result != null);

            result = TQTrackingUtils.getObjectDescribe('NonValidObject');
            System.assertEquals(null, result);
            
            List<String> res = TQTrackingUtils.getFieldSetFields(TQTrackingUtils.getGlobalDescribe(), params);
            
            System.assertEquals(0, res.size());

            User myUser = TQTrackingUtils.getCurrentUser();
            System.assertEquals(UserInfo.getUserId(), myUser.Id);

            List<String> splitList = TQTrackingUtils.splitList('Account,Contact');
            System.assertEquals(2, splitList.size());
        }   
    }

}