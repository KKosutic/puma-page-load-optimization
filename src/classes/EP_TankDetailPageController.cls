/* 
   @author <Breajesh Tiwary>
   @name <EP_TankDetailPageController>
   @createDate <22/08/2016>
   @description <This is extension for EP_Tank_Product_Page> 
   @Novasuite Fix - Required Documentation
   @version <1.1>
    - Fixed view state problem when too many products are loaded
*/
public without sharing class EP_TankDetailPageController {
    private EP_Tank__c tank;
    public String productList{get;set;}
    public String tankId{get;set;}
    public String tankLabel{get;set;}
    public List<SelectOption> ProductValues {get;set;}
    public EP_Tank__c tnk;
    private static final String Str_Lkid = '_lkid';
    private static final string TANK_DETAIL_PAGE_CTRL = 'EP_TankDetailPageController';
    private static final string UPDATE_UOM = 'updateUOM';
    private static final string FETCH_PROD_VALUES = 'fetchProductValues';
    public Boolean blnShowSaveButton {
        get {
            if (blnShowSaveButton == NULL)
            {
                blnShowSaveButton = TRUE;
            }
            return blnShowSaveButton;
        }
        set;
    }
    
    Id shipId;
    Account shipTo;
    Id companyId;
    Id priceBookId;
    Boolean isChanged;
    Map<Id, String> mProductIDUOM;
    
    /*
        Constructor
    */
    public EP_TankDetailPageController(ApexPages.StandardController stdController) {
        this.tank = (EP_Tank__c)stdController.getRecord();
        ProductValues = new List<SelectOption>();
        mProductIDUOM = new map<Id, String>();
        isChanged = true;
    }
    
    /*
        Getter method for productList
    */
    public void fetchProductValues(){
        
        //Fix for Defect 30023 - Start
        try{
            List<SelectOption> pList = new List<SelectOption>();
            tankId = ApexPages.CurrentPage().getParameters().get(EP_Common_Constant.ID);
            mProductIDUOM = new map<Id, String>();
            Integer queryRows;
            set<Id> priceBookIds = new set<Id>();
            
            // Get ship to ID
            shipId = (Id)ApexPages.currentPage().getParameters().get(label.EP_Ship_To_Field_Id_for_Tank_Page+Str_Lkid);
            
            // If editing existing tank then update the Ship-To ID
            if(tank.id == NULL && tank.EP_Ship_To__c != NULL){
                shipId = tank.EP_Ship_To__c;
            }
            
            // If tank ID is blank and Ship-To ID is not blank      
            if(String.isBlank(tankId) && String.isNotBlank(shipId)){
                // then query the parent account pricebook
                shipTo = [SELECT Id,
                        ParentId,
                        Parent.EP_Puma_Company__c
                        FROM Account 
                        WHERE Id =:shipId
                        LIMIT :EP_COMMON_CONSTANT.ONE  
                    ];
                // Company ID
                companyId = shipTo.Parent.EP_Puma_Company__c;
                 /* #L4_45352_Start*/
                // Pricebook Ids
                priceBookIds = getPriceBookIds(shipTo.ParentId);
                if(priceBookIds.isEmpty()){
                    ApexPages.addmessage(new ApexPages.message( ApexPages.severity.WARNING,label.EP_No_PriceBook_Associated_Customer));
                    blnShowSaveButton = FALSE;
                }
                 /* #L4_45352_END*/
            }
            // Else if tank is edited
            else if(String.isNotBlank(tankId)){
                // Get the Ship-To of the tank and the Parent Account of the Ship-To
                tnk = [SELECT id
                            ,Name
                            ,EP_Product__r.EP_Unit_of_Measure__c
                            ,EP_Ship_To__c
                            ,EP_Ship_To__r.ParentId
                            ,EP_Ship_To__r.Parent.EP_PriceBook__c
                            ,EP_Ship_To__r.Parent.EP_Puma_Company__c 
                       FROM EP_Tank__c WHERE id = :tankId LIMIT :EP_COMMON_CONSTANT.ONE];
                  /* #L4_45352_Start*/
                // get Pricebook Ids
                priceBookIds = getPriceBookIds(tnk.EP_Ship_To__r.ParentId);
                 /* #L4_45352_END*/
                // Company ID
                companyId = tnk.EP_Ship_To__r.Parent.EP_Puma_Company__c;
            }
            else{}   
            queryRows = EP_Common_Util.getQueryLimit();
            // If company ID and pricebook ID are not NULL
            if(companyId != NULL && !priceBookIds.isEmpty()) {             
                // Then get the procucts in the pricebook through the PriceBookEntry object 
                for(PriceBookEntry pb : [SELECT Product2Id
                                                ,Product2.Name
                                                ,Product2.ProductCode,Product2.EP_Unit_of_Measure__c
                                        FROM PriceBookEntry  /* #L4_45352_Start*/
                                        WHERE PriceBook2Id IN : priceBookIds
                                         /* #L4_45352_END*/
                                          AND Product2.EP_Company_Lookup__c = :companyId 
                                          AND IsActive = TRUE
                                          AND Product2.EP_Product_Sold_As__c = :EP_Common_Constant.BULK_ORDER_PRODUCT_CAT
                                          AND Product2.EP_Is_System_Product__c = FALSE
                                         ORDER BY Product2.Name ASC LIMIT :queryRows ]) {
                    SelectOption sOpt;
                    // Create select list of products
                    // Added this logic to avoid displaying the same product twice
                    if (!mProductIDUOM.containsKey(pb.Product2Id)){
                        sOpt = new SelectOption(
                                        pb.Product2Id,pb.Product2.Name + 
                                            EP_Common_Constant.LEFT_BRACKET + pb.Product2.ProductCode + EP_Common_Constant.RIGHT_BRACKET  +
                                                EP_Common_Constant.LEFT_BRACKET + pb.Product2.EP_Unit_of_Measure__c + EP_Common_Constant.RIGHT_BRACKET);                       
                        pList.add(sOpt); 
                    }
                    // Map prodcut ID(key) and UoM (value)
                    mProductIDUOM.put(pb.Product2Id, pb.Product2.EP_Unit_of_Measure__c);                 
                }  
                
                // If there is no product that matches the above criteria, then display a warning message to the user
                if (pList.isEmpty()){
                    ApexPages.addmessage(
                        new ApexPages.message(
                            ApexPages.severity.WARNING, 
                                Label.EP_No_Available_Tank_Products));
                    blnShowSaveButton = FALSE;
                }     
            }
            /* #L4_45352_Start*/
            // If creating a new tank
            /*if(tnk == null){
                if(pList.isEmpty()){
                    // If no product is available then leave blank
                    tank.EP_UNIT_OF_MEASURE__C = EP_Common_CONSTANT.BLANK;
                }
                else{
                    // Else use th UoM of the first item from the product list
                    tank.EP_UNIT_OF_MEASURE__C = mProductIDUOM.values()[0];
                }
                 
            }*/
            /* #L4_45352_END*/
            ProductValues = pList;
            
            //Fix for Defect 30023 - End
            }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                FETCH_PROD_VALUES, TANK_DETAIL_PAGE_CTRL, ApexPages.Severity.ERROR);
        } 
    }
    /* #L4_45352_Start*/
    //need to move query account mapper class
    /**
    * @author       Accenture
    * @name         getPriceBookIds
    * @date         16/11/2017
    * @description  This method returns set of priceBook Ids related to SellTo Account Product Option object.
    * @param        Sell To Account Id.
    * @return       set of PriceBook Ids
    */  
    @TestVisible
    private set<Id> getPriceBookIds(Id SellToAccId){
        set<Id> priceBookIds = new set<Id>();
        list<EP_Product_Option__c> lstProductOption = new list<EP_Product_Option__c>();
        Account SellToAcc = new Account();
        SellToAcc = EP_AccountMapper.getSellToAccountWithRelatedProdOption(SellToAccId);
        // Pricebook ID                 
        lstProductOption = SellToAcc.Product_Options__r;
        if(lstProductOption != null && !lstProductOption.isEmpty()){
            priceBookIds = returnPriceBookIds(lstProductOption);
            
        }
        return priceBookIds;
    }
    /**
    * @author       Accenture
    * @name         returnPriceBookIds
    * @date         16/11/2017
    * @description  This method returns set of priceBook Ids related to SellTo Account Product Option object.
    * @param        List of Product Option Records
    * @return       set of PriceBook Ids
    */ 
    @TestVisible
    private set<id> returnPriceBookIds(list<EP_Product_Option__c> lstProductOption){
        set<Id> priceBookIds = new set<Id>();
        for(EP_Product_Option__c ProdOption : lstProductOption){
            if(String.isNotBlank(ProdOption.Price_List__c)){
                priceBookIds.add(ProdOption.Price_List__c);
            }   
        }
        return priceBookIds;
    }
   
    /*
     @Description: This method is used to fetch unit of measure for tank.
    */
    public void updateUOM(){
        // Get the selected product UoM
        /*try{
            tank.EP_UNIT_OF_MEASURE__C = mProductIDUOM.containsKey(tank.EP_Product__c)?mProductIDUOM.get(tank.EP_Product__c):EP_COMMON_CONSTANT.BLANK;
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                UPDATE_UOM, TANK_DETAIL_PAGE_CTRL, ApexPages.Severity.ERROR);
        } 
		*/
    }
    
    /* #L4_45352_END*/
    
}