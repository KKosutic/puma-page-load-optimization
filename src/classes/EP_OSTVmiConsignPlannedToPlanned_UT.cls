@isTest
public class EP_OSTVmiConsignPlannedToPlanned_UT
{
	Static String ORDER_EVENT = 'Planned To Planned';

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    /*Dont have negative scenario as isTransitionPossible always return true*/
	static testMethod void isTransitionPossible_positive_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getVmiConsignmentOrderStatePlannedDomainObjectPositiveScenario();
		EP_OrderEvent oe = new EP_OrderEvent(ORDER_EVENT);
		EP_OSTVmiConsignPlannedToPlanned ost = new EP_OSTVmiConsignPlannedToPlanned();
		ost.setOrderContext(obj,oe);
		Test.startTest();
		boolean result = ost.isTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}	
	/*Dont have negative scenario as isRegisteredForEvent always return true*/
	static testMethod void isRegisteredForEvent_positive_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
		EP_OrderEvent oe = new EP_OrderEvent(ORDER_EVENT);
		EP_OSTVmiConsignPlannedToPlanned ost = new EP_OSTVmiConsignPlannedToPlanned();
		ost.setOrderContext(obj,oe);
		Test.startTest();
		boolean result = ost.isRegisteredForEvent();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void isGuardCondition_positive_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
		EP_OrderEvent oe = new EP_OrderEvent(ORDER_EVENT);
		EP_OSTVmiConsignPlannedToPlanned ost = new EP_OSTVmiConsignPlannedToPlanned();
		ost.setOrderContext(obj,oe);
		Test.startTest();
		boolean result = ost.isGuardCondition();
		Test.stopTest();
		System.AssertEquals(true,result);
		System.AssertEquals(EP_Common_Constant.Blank, obj.getOrder().EP_Order_credit_status__c);
	}
	static testMethod void isGuardCondition_creditCheck_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
		obj.getOrder().EP_Sync_with_NAV__c = true;
		Database.update(obj.getOrder());
		EP_OrderEvent oe = new EP_OrderEvent(ORDER_EVENT);
		EP_OSTVmiConsignPlannedToPlanned ost = new EP_OSTVmiConsignPlannedToPlanned();
		ost.setOrderContext(obj,oe);
		Test.startTest();
		boolean result = ost.isGuardCondition();
		Test.stopTest();
		System.AssertEquals(true,result);
		System.AssertEquals(EP_Common_constant.Credit_Okay, obj.getOrder().EP_Order_credit_status__c);
	}
}