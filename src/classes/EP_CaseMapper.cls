/* 
@Author      Accenture
@name        EP_CaseMapper
@CreateDate  04/1/2018
@Description his class contains all SOQLs related to Case Object 
@Version     1.0
*/
public class EP_CaseMapper{ 

    /** This method returns caseobject Records
    *  @name            getRecordsByIds
    *  @param           id
    *  @return          Case
    *  @throws          NA
    */
    public static Case getRecordsByIds(id RecordId) {
        Case objCase ;
        list<Case> lstCase = [select id ,EP_FE_Sub_Type__c,type,Accountid,BusinessHoursid,Account.EP_Puma_Company__c   from Case where id=:RecordId];
        if(!lstCase .isEmpty()){
            objCase = lstCase [0];
        }
        return objCase ;
    }
}