@isTest
public class EP_PortalOrderPageController_UT {

    //For state machine to give proper status
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

    static testMethod void init_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.init();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.ctx );
        System.AssertNotEquals(Null,localObj.hlp );
        System.AssertEquals(Null,localObj.hlpstep3And4 );
        System.AssertEquals(EP_TestDataUtility.getSellTo().Id,localObj.ctx.strSelectedAccountID);
    }
    static testMethod void getPricing_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
         PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, orderObj.AccountId__c);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Integer InitailCount = localObj.ctx.pricingPollerCount;
        Test.startTest();
        localObj.getPricing();
        Test.stopTest();
        System.Assert(InitailCount<localObj.ctx.pricingPollerCount);
    }
    static testMethod void getSlots_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.getSlots();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.isPriceCalculated);
        System.AssertEquals(false,localObj.showCalculateButton);
    }
    static testMethod void setAccountOrderDetails_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.setAccountOrderDetails();
        Test.stopTest();
        System.AssertEquals(localObj.ctx.orderAccount.CurrencyIsoCode,localObj.ctx.newOrderRecord.CurrencyIsoCode );
    }
    static testMethod void setShipToTank_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        Account shipto = EP_TestDataUtility.getShipTo();
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, shipto.ParentId);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.setShipToTank();
        Test.stopTest();
        System.AssertEquals(true,localObj.ctx.mapShipToTanks.size() > 0);
    }
    static testMethod void doDeleteActions_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, orderObj.AccountId__c);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        localObj.ctx.newOrderRecord = orderObj;
        localObj.ctx.orderDomainObj = new EP_OrderDomainObject(orderObj);
        List<OrderItem> lstOrderItems = new List<OrderItem>();
        lstOrderItems = [Select Id FROM OrderItem WHERE OrderId=:orderObj.Id];
        Integer beforeExecutionNumberOfItems = lstOrderItems.size();
        Test.startTest();
        localObj.doDeleteActions(orderObj.Id);
        Test.stopTest();
        lstOrderItems = [Select Id FROM OrderItem WHERE OrderId=:orderObj.Id];
        Integer afterExecutionNumberOfItems = lstOrderItems.size();
        System.Assert(beforeExecutionNumberOfItems>afterExecutionNumberOfItems);
    }
    static testMethod void doActionBack_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doActionNext();
        PageReference result = localObj.doActionNext();
        localObj.doActionBack();
        Test.stopTest();
        System.AssertNotEquals(1,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void doActionNext_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        PageReference result = localObj.doActionNext();
        Test.stopTest();
        System.AssertNotEquals(0,localObj.ctx.intOrderCreationStepIndex);
    }
    
    static testMethod void doActionNext_Step1_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doActionNext();
        PageReference result = localObj.doActionNext();
        Test.stopTest();
        System.AssertNotEquals(2,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void doActionNext_Step2_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doActionNext();
        localObj.doActionNext();
        PageReference result = localObj.doActionNext();
        Test.stopTest();
        System.AssertNotEquals(3,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void doActionNext_Step3_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doActionNext();
        localObj.doActionNext();
        localObj.doActionNext();
        PageReference result = localObj.doActionNext();
        Test.stopTest();
        System.AssertNotEquals(4,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void doActionNext_Step4_test() {
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, EP_TestDataUtility.getSellTo().Id);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doActionNext();
        localObj.doActionNext();
        localObj.doActionNext();
        localObj.doActionNext();
        PageReference result = localObj.doActionNext();
        Test.stopTest();
        System.AssertNotEquals(5,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void setRun_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.setRun();
        Test.stopTest();
        
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - setRun() Method is calling EP_OrderPageContext class Methods
    }
    static testMethod void removeOrderLineItem_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.removeOrderLineItem();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.isPriceCalculated);
    }
    static testMethod void addNewLine_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.addNewLine();
        Test.stopTest();
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - setRun() Method is calling EP_OrderLinesAndSummaryHelper class Methods
    }
    static testMethod void doCancel_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, orderObj.AccountId__c);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        PageReference result = localObj.doCancel();
        Test.stopTest();
        System.AssertNotEquals(Null,result); 
        System.AssertNotEquals(EP_Common_Constant.HOME_PAGE_URL,result.getUrl());
    }
    static testMethod void doCancelOrder_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, orderObj.AccountId__c);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        PageReference result = localObj.doCancelOrder();
        Test.stopTest();
        System.AssertNotEquals(Null,result);
    }
    static testMethod void doSubmit_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, orderObj.AccountId__c);
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        PageReference result = localObj.doSubmit();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.ctx.newOrderRecord.Status__c);
    }
    static testMethod void saveOrderAsDraft_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        PageReference result = localObj.saveOrderAsDraft();
        Test.stopTest();
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - saveOrderAsDraft() Method is calling Helper class Methods
    }
    static testMethod void doUpdateSupplierContracts_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doUpdateSupplierContracts();
        Test.stopTest();
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - doUpdateSupplierContracts() Method is calling Helper class Methods
    }
    static testMethod void setDataForConsumptionOrder_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.setDataForConsumptionOrder();
        Test.stopTest();
        
        //System.AssertEquals(true,<asset conditions>);
        
		//Can't Assert - setDataForConsumptionOrder() Method is calling EP_OrderPageContext class Methods
    }
    static testMethod void doCalculatePrice_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.doCalculatePrice();
        Test.stopTest();
        
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - doCalculatePrice() Method is calling Helper class Methods
    }
    static testMethod void validateOperationalTanks_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.validateOperationalTanks();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.ctx.mapShipToTanks);
    }
    static testMethod void needPricing_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.needPricing();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.isPriceCalculated);
    }
    static testMethod void displayProductNameAndSafeFillLevel_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.displayProductNameAndSafeFillLevel();
        Test.stopTest();
        //System.AssertEquals(true,<asset conditions>);
        
        //Can't Assert - displayProductNameAndSafeFillLevel() Method is calling EP_OrderPageContext class Methods
    }
    static testMethod void updateOrderItemLines_test() {
        EP_PortalOrderPageController localObj = new EP_PortalOrderPageController();
        Test.startTest();
        localObj.updateOrderItemLines();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.isPriceCalculated);
    }
}