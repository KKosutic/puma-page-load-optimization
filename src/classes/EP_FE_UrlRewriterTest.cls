/*   
     @Author <Accenture>
     @name <EP_FE_UrlRewriterTest.cls>     
     @Description <This class is the Test Class for EP_FE_UrlRewriter>   
     @Version <1.1> 
*/
@isTest
private class EP_FE_UrlRewriterTest {
/*********************************************************************************************
     @Author <>
     @name <CreateURL>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
   static TestMethod void CreateURL(){

    EP_FE_UrlRewriter obj = new EP_FE_UrlRewriter();
    
    PageReference pageRef = new PageReference('http://cs81.salesforce.com/ep_fe_dictionary');
    
     obj.mapRequestUrl(pageRef);
  
  }
/*********************************************************************************************
     @Author <>
     @name <TestURL>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/  
     static TestMethod void TestURL(){

    EP_FE_UrlRewriter obj = new EP_FE_UrlRewriter();
    
    PageReference pageRef = new PageReference('http://cs81.salesforce.com/');
    
     obj.mapRequestUrl(pageRef);
  
  }
/*********************************************************************************************
     @Author <>
     @name <CreateTestURL>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/  
  static TestMethod void CreateTestURL(){
  	test.starttest();
      EP_FE_UrlRewriter obj = new EP_FE_UrlRewriter(); 
      List<PageReference> pageRefList = new List<PageReference>();
      List<PageReference> pageRefListReturn = new List<PageReference>();
      pageRefListReturn = obj.generateUrlFor(pageRefList );
      test.stoptest();
  
   }
}