@isTest
public class EP_ProfileMapper_UT{

    static testMethod void getRecordsByIds_test() {
        EP_ProfileMapper localObj = new EP_ProfileMapper();
        Profile profileObj = [SELECT Id FROM Profile LIMIT 1];
        Set<id> setProfile = new Set<Id>{profileObj.id };
        Test.startTest();
        list<Profile> result = localObj.getRecordsByIds(setProfile );
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getProfilesByName_test() {
        EP_ProfileMapper localObj = new EP_ProfileMapper();
        Profile profileObj = [SELECT Id ,Name FROM Profile LIMIT 1];
        List<String> setProfile = new List<String>{profileObj.Name};
        Test.startTest();
        list<Profile> result = localObj.getProfilesByName(setProfile);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getCurrentUserProfile_test() {
        EP_ProfileMapper localObj = new EP_ProfileMapper();
        Profile profileObj = [SELECT id , Name FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        Profile result = new Profile();
        Id currentUserProfileId;
        User userObj = EP_TestDataUtility.createUser(profileObj.id);
        Insert userObj;
        Test.startTest();
        
        System.runAs(userObj){
            result = localObj.getCurrentUserProfile();   
            currentUserProfileId = userinfo.getProfileId();
        }       
        Test.stopTest();
        System.AssertEquals(profileObj.id,result.id);

    }


}