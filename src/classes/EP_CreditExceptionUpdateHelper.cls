/* 
   * @Author <Accenture>
   * @name <EP_CreditExceptionUpdateHelper>
   * @CreateDate <03/14/2017>
   * @Description <This is a class to Transform JSON string data into SOBject(Credit Exception and Order) for Credit Exception Status Update from WINDMS Rest Service Request> 
   * @Version <1.0>
*/ 
public with sharing class EP_CreditExceptionUpdateHelper {
    @testVisible private set<string> creditExceptionNameSet = new set<string>();
    @testVisible private map<string, EP_Credit_Exception_Request__c > creditExceptionNameMap = new map<string, EP_Credit_Exception_Request__c >();
    
    /**
    * @Author       Accenture
    * @Name         createDataSets
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createDataSets(list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionList) {
        EP_GeneralUtility.Log('Private','EP_CreditExceptionUpdateHelper','createDataSets');
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper : creditExceptionList){
            creditExceptionNameSet.add(creditExceptionWrapper.identifier.exceptionNo);
        }
    }

    /**
    * @Author       Accenture
    * @Name         createDataMaps
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createDataMaps(list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionList) {
        EP_GeneralUtility.Log('Private','EP_CreditExceptionUpdateHelper','createDataMaps');
        createDataSets(creditExceptionList);
        setCreditExceptions(creditExceptionNameSet);
    }   
    
   /**
    * @Description: This method will process Credit Exceptions recolrds to update the Status from Inbound message
    * @Date :03/14/2017
    * @Name :setCreditExceptionAttributes
    * @param: NA
    * @return: NA
    */
    public void setCreditExceptionAttributes(list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExcStatusList) {
        EP_GeneralUtility.Log('Public','EP_CreditExceptionUpdateHelper','setCreditExceptionAttributes');
        createDataMaps(creditExcStatusList);
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper : creditExcStatusList){
            if(!isRecordsExists(creditExceptionWrapper)){
                continue;
            }
            if(!isValidApprovalStatus(creditExceptionWrapper)) {
                continue;
            }
            if(!isValidOrder(creditExceptionWrapper)) {
                continue;
            }
            processValidRecords(creditExceptionWrapper);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         processValidRecords
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void processValidRecords(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper) {
        creditExceptionWrapper.isProcess = true;
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','processValidRecords');
        EP_Credit_Exception_Request__c creditExceptionObject = creditExceptionNameMap.get(creditExceptionWrapper.identifier.exceptionNo);
        creditExceptionWrapper.creditExcpObj.Id = creditExceptionObject.Id;
    }

    

    /**
    * @Author       Accenture
    * @Name         isRequestApproved
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private boolean isRequestApproved(string status) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','isRequestApproved');
        boolean isRequestApproved = false;
        if(EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED.equalsIgnoreCase(status)) {
                isRequestApproved = true;
        }
        return isRequestApproved;
    }
    /**
    * @Description: This method will process order record and set order Status if Credit Exception Status is Rejected.
    * @Date :03/14/2017
    * @Name :setRejectedStatus
    * @param :creditExceptionWrapper - Class instance of CreditExceptionStatusType
    * @return: NA
    */
    @TestVisible
    private void setRejectedStatus(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','setRejectedStatus');
        if(EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED.equalsIgnoreCase(creditExceptionWrapper.creditExcpObj.EP_Status__c)){
            if(!string.isNotBlank(creditExceptionWrapper.creditExcpObj.EP_Reason__c)){
                setfailureReason(creditExceptionWrapper, 'ERROR - Reason Can not be Blank');
            }
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setfailureReason
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setfailureReason(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper, string failureReason) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','setfailureReason');
        creditExceptionWrapper.isProcess = false;
        creditExceptionWrapper.failureReason = failureReason;
    }

    /**
    * @Description: This method will check if Credit exception exists in SFDC or not.
    * @Date :03/14/2017
    * @Name :isRecordsExists
    * @param :creditExceptionWrapper - Class instance of CreditExceptionStatusType
    * @param :creditExceptionNameMap - Map of Credit exception Name and Credit Exception object
    * @return: boolean
    */
    @TestVisible
    private boolean isRecordsExists(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','isRecordsExists');
        if(!creditExceptionNameMap.containsKey(creditExceptionWrapper.identifier.exceptionNo)) {
            setfailureReason(creditExceptionWrapper, 'ERROR - Provided Credit Exception does not exist in SFDC');
            return false;
        }
        return true;
    }
    
    /**
    * @Description: This method will check if Credit exception status is not blank in JSON message.
    * @Date :03/14/2017
    * @Name :isValidApprovalStatus
    * @param :creditExceptionWrapper - Class instance of CreditExceptionStatusType
    * @return: boolean
    */
    @TestVisible
    private boolean isValidApprovalStatus(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','isValidApprovalStatus');
        if(string.isBlank(creditExceptionWrapper.approvalStatus)) {
            setfailureReason(creditExceptionWrapper, 'ERROR - Approval Status is Blank');
            return false;
        }
        return true;
    }
    
    /**
    * @Description: This method will check if order records for given Credit exception is exists in SFDC or not.
    * @Date :03/14/2017
    * @Name :isValidOrder
    * @param :creditExceptionWrapper - Class instance of CreditExceptionStatusType
    * @param :creditExceptionNameMap - Map of Credit exception Name and Credit Exception object
    * @return: NA
    */
    @TestVisible
    private boolean isValidOrder(EP_CreditExceptionUpdateStub.CreditExceptionStatusType creditExceptionWrapper) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','isValidOrder');
        EP_Credit_Exception_Request__c crdExpRec = creditExceptionNameMap.get(creditExceptionWrapper.identifier.exceptionNo);
        if(crdExpRec.EP_CS_Order__c == null) {
            setfailureReason(creditExceptionWrapper, 'ERROR - Parent Order does not exist');
            return false;
        }
        return true;
    }
    
    
    /**
    * @Description: This method will return credit Exceptions records
    * @Date :03/14/2017
    * @Name :setCreditExceptions
    * @param :creditExceptionName - set of credit excpetion Name coming in JSON message
    * @return: map<string, EP_Credit_Exception_Request__c >
    */
    @TestVisible
    private void setCreditExceptions(set<string> creditExceptionNameSet) {
        EP_GeneralUtility.Log('private','EP_CreditExceptionUpdateHelper','setCreditExceptions');
        EP_CreditExceptionRequestMapper creditExcepMapper = new EP_CreditExceptionRequestMapper(); 
        for(EP_Credit_Exception_Request__c crdExpRec : creditExcepMapper.getRecords(creditExceptionNameSet)) {
            creditExceptionNameMap.put(crdExpRec.Name,crdExpRec);
        }
    }
}