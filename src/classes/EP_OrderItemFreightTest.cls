/**
 * @author <Jyotsna>EP_OrderItemFreightTest
 * @name <EP_OrderItemFreightTest>
 * @description <This class is a test class of functionality of Frieght Addition on Delivery type orders> 
 * @version <1.0>
 */
@isTest
private class EP_OrderItemFreightTest {

    private static EP_Freight_Matrix__c freightMatrix;
    private static EP_Freight_Price__c freightPrice;
    private static Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
    private static Id orderRecTypeIdVMI = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    private static Account accObj, accObj1, shipTo;
    private static OrderItem oOrderItems;
    private static Account vmiShipToAccount,sellToAccount; 
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.SELL_TO);
    private static List<PricebookEntry> priceBookEntryToInsert;
    private static  Id orderRecTypeIdNonVMI = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
    private static EP_Stock_Holding_Location__c stockLoc;
    private static PricebookEntry prodPricebookEntry;
    private static Account transporter1;
     /**
         * @author Jyotsna Yadav
         * @description : Test method to check the message displayed to suer if freight is not configured 
         * @This method covers test case number 10437
     **/
   private  static  testmethod void freightNotCreated(){
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();       
        Test.startTest();
        System.runAs(cscUser) {
         try{
           
        // create pricebook
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
        insert customPB;
        
        // insert product
        List<Product2> prodList = new List<Product2>();
        Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        prodList.add(productDieselObj);
        prodList.add(productPetrolObj);
        Database.insert(prodList);
        
        
        // insert pricebook entry for Diesel product
        List<PricebookEntry> pbEntryList = new List<PricebookEntry>();
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productDieselObj.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(standardPrice);
        
        PricebookEntry pbEntryDiesel = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productDieselObj.Id,
        UnitPrice = 12000, IsActive = true);
        
        pbEntryList.add(pbEntryDiesel);
        
                
        //Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice2 = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productPetrolObj.Id,
        UnitPrice = 10000, IsActive = true);
        
        pbEntryList.add(standardPrice2);

        PricebookEntry pbEntryPetrol = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productPetrolObj.Id,
        UnitPrice = 12000, IsActive = true);
        pbEntryList.add(pbEntryPetrol);
        
         insert pbEntryList;
         freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        insert freightMatrix;
        
               
        // create bill to Account
        Account billToAccount = EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update billToAccount;     
        // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.id;
        insert sellToAccount;
        
        // create non vmi ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        insert nonVmiShipToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        // insert tanks in order to mark vmi account as Active
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);        
        insert tanksToBeInsertedList;
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVmiShipToAccount;
        
        
          
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId;
        update nonVmiShipToAccount;
        
                
        // create Storage Location type Account
        List<Account> storageLocAcc = new List<Account>();
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccSecondary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccAlternate = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAcc.add(storageLocAccPrimary);
        storageLocAcc.add(storageLocAccSecondary);
        storageLocAcc.add(storageLocAccAlternate);
        Database.insert(storageLocAcc,false);
        
        //create stock holding location
        List<EP_Stock_Holding_Location__c> stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
        EP_Stock_Holding_Location__c primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Primary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccPrimary.id);
        EP_Stock_Holding_Location__c secondaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Secondary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccSecondary.id);
        EP_Stock_Holding_Location__c alternateStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Alternate',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccAlternate.id);
        stockholdingLocs.add(primaryStockHolding);
        stockholdingLocs.add(secondaryStockHolding);
        stockholdingLocs.add(alternateStockHolding);
        insert stockholdingLocs;
        
        
        //create Inventories
        List<EP_Inventory__c> inventories = new List<EP_Inventory__c>();
        EP_Inventory__c primaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccPrimary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c secondaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccSecondary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c alterNateInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccAlternate.id,EP_Product__c=productDieselObj.id);
        inventories.add(primaryInventory);
        inventories.add(secondaryInventory);
        inventories.add(alterNateInventory);
        Database.insert(inventories,false);
        System.debug('inventories are '+inventories);
        
        
        // Page Navigations
        
        // Load the order and account details
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);//Applying page context here  
        ApexPages.currentPage().getParameters().put('id', sellToAccount.Id);
        EP_PortalOrderPageController portalOrderController =new EP_PortalOrderPageController();
        
        //portalOrderController.loadAccountOrderDetails();
        
        // Set order type to "Delivery"
        //portalOrderController.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
        //portalOrderController.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
        
        // Move to step #2
        //portalOrderController.next();
        
        // Ensure that step index has moved
        //System.assertEquals(2, portalOrderController.intOrderCreationStepIndex);
        
        // Select Ship-To
        //portalOrderController.strSelectedShipToId = nonVMIShipToAccount.Id;
        
        // Move to step #4 (step #3 is skipped for Delivery orders)
        //portalOrderController.next();        
         Test.stopTest();
        system.debug('success inventory');
        //Assert the message when freight is not configured
        system.assertEquals(Label.EP_Freight_Error_Message_for_Internal_User,ApexPages.getMessages()[0].getDetail());
        }catch(Exception ex){
            //system.debug('============'+ApexPages.getMessages()[1]);
        }
         
        }
       
        
     } 
      /**
         * @author Jyotsna Yadav
         * @description : Test method to check the the freight added for the given parameters
         * @This method covers test case number 10615
     **/
     private  static  testmethod void freightCreated(){
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();       
        Test.startTest();
        System.runAs(cscUser) {
         try{
        // create pricebook
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
        insert customPB;
        
        // insert product
        List<Product2> prodList = new List<Product2>();
        Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  taxProd= new product2(Name = 'TaxProduct', CurrencyIsoCode = 'GBP', isActive = TRUE);                           
        Product2  freightProd = new product2(Name = 'Freight Price', CurrencyIsoCode = 'GBP', isActive = TRUE);                           
        prodList.add(productDieselObj);
        prodList.add(productPetrolObj);
        prodList.add(taxProd);
        prodList.add(freightProd);
        Database.insert(prodList);
        
        
        // insert pricebook entry for Diesel product
        List<PricebookEntry> pbEntryList = new List<PricebookEntry>();
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry taxPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = taxProd.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(taxPrice);
        
        PricebookEntry freightPriceEntry = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = freightProd.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(freightPriceEntry);
        
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productDieselObj.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(standardPrice);
        
        PricebookEntry pbEntryDiesel = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productDieselObj.Id,
        UnitPrice = 12000, IsActive = true);
        
        pbEntryList.add(pbEntryDiesel);
        
                
        //Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice2 = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productPetrolObj.Id,
        UnitPrice = 10000, IsActive = true);
        
        pbEntryList.add(standardPrice2);

        PricebookEntry pbEntryPetrol = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productPetrolObj.Id,
        UnitPrice = 12000, IsActive = true);
        pbEntryList.add(pbEntryPetrol);
        
         insert pbEntryList;
         freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        insert freightMatrix;
        
        //create freight price
        freightPrice = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        insert freightPrice;
        
        EP_Freight_Price__c freightPrice1 = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        freightPrice1.EP_Max_Distance__c = 200;
        freightPrice1.EP_Max_Volume__c = 50;
        freightPrice1.EP_Min_Distance__c = 100;
        freightPrice1.EP_Min_Volume__c = 20;
        freightPrice1.EP_Freight_Price__c = 100.54;
        insert freightPrice1;
         
        
        // create bill to Account
        Account billToAccount = EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update billToAccount;     
        // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.id;
        insert sellToAccount;
        
        // create non vmi ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId ;
        nonVmiShipToAccount.CurrencyIsoCode = 'GBP';
        insert nonVmiShipToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        // insert tanks in order to mark vmi account as Active
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);        
        insert tanksToBeInsertedList;
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVmiShipToAccount;
        
        
          
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId;
        update nonVmiShipToAccount;
        
                
        // create Storage Location type Account
        List<Account> storageLocAcc = new List<Account>();
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccSecondary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccAlternate = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAcc.add(storageLocAccPrimary);
        storageLocAcc.add(storageLocAccSecondary);
        storageLocAcc.add(storageLocAccAlternate);
        Database.insert(storageLocAcc,false);
        
        //create stock holding location
        List<EP_Stock_Holding_Location__c> stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
        EP_Stock_Holding_Location__c primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Primary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccPrimary.id,EP_Distance__c=7);
        EP_Stock_Holding_Location__c secondaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Secondary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccSecondary.id,EP_Distance__c=12);
        EP_Stock_Holding_Location__c alternateStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Alternate',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccAlternate.id,EP_Distance__c=20);
        stockholdingLocs.add(primaryStockHolding);
        stockholdingLocs.add(secondaryStockHolding);
        stockholdingLocs.add(alternateStockHolding);
        Database.insert(stockholdingLocs,false);
        
        
        //create Inventories
        List<EP_Inventory__c> inventories = new List<EP_Inventory__c>();
        EP_Inventory__c primaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccPrimary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c secondaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccSecondary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c alterNateInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccAlternate.id,EP_Product__c=productDieselObj.id);
        inventories.add(primaryInventory);
        inventories.add(secondaryInventory);
        inventories.add(alterNateInventory);
        Database.insert(inventories,false);
        System.debug('inventories are '+inventories);
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);//Applying page context here  
        ApexPages.currentPage().getParameters().put('id', sellToAccount.Id);
        EP_PortalOrderPageController portalOrder =new EP_PortalOrderPageController();
        EP_PortalOrderPageController portalOrderController =new EP_PortalOrderPageController();
        
        //portalOrderController.loadAccountOrderDetails();;
        
        // Set order type to "Delivery"
        //ortalOrderController.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
        //portalOrderController.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
        
        // Move to step #2
        //portalOrderController.next();
        
        // Ensure that step index has moved
        //System.assertEquals(2, portalOrderController.intOrderCreationStepIndex);
        
        // Select Ship-To
        //portalOrderController.strSelectedShipToId = nonVMIShipToAccount.Id;
        
        // Move to step #4 (step #3 is skipped for Delivery orders)
        //portalOrderController.next();        
        
         Test.stopTest();
        system.debug('success inventory');
        List<ORderItem> oList = [Select id,pricebookentry.product2.name,unitprice from orderitem where pricebookentryId=:freightPriceEntry.Id ];
        //assert the price added as freight
        system.assertEquals(20.22,oList[0].unitprice);
        }catch(Exception ex){
            system.debug('============'+ApexPages.getMessages());
        }
         
        }
       
        
     } 
     
           /**
         * @author Jyotsna Yadav
         * @description : Test method to check the the freight added for the given parameters
         * @This method covers test case number 10774
     **/
     private  static  testmethod void freightValidationForExRack(){
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();       
        Test.startTest();
        System.runAs(cscUser) {
         try{
        // create pricebook
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
        insert customPB;
        
        // insert product
        List<Product2> prodList = new List<Product2>();
        Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  taxProd= new product2(Name = 'TaxProduct', CurrencyIsoCode = 'GBP', isActive = TRUE);                           
        Product2  freightProd = new product2(Name = 'Freight Price', CurrencyIsoCode = 'GBP', isActive = TRUE);                           
        prodList.add(productDieselObj);
        prodList.add(productPetrolObj);
        prodList.add(taxProd);
        prodList.add(freightProd);
        Database.insert(prodList);
        
        
        // insert pricebook entry for Diesel product
        List<PricebookEntry> pbEntryList = new List<PricebookEntry>();
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry taxPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = taxProd.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(taxPrice);
        
        PricebookEntry freightPriceEntry = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = freightProd.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(freightPriceEntry);
        
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productDieselObj.Id,
        UnitPrice = 10000, IsActive = true);
        pbEntryList.add(standardPrice);
        
        PricebookEntry pbEntryDiesel = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productDieselObj.Id,
        UnitPrice = 12000, IsActive = true);
        
        pbEntryList.add(pbEntryDiesel);
        
                
        //Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice2 = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productPetrolObj.Id,
        UnitPrice = 10000, IsActive = true);
        
        pbEntryList.add(standardPrice2);

        PricebookEntry pbEntryPetrol = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = productPetrolObj.Id,
        UnitPrice = 12000, IsActive = true);
        pbEntryList.add(pbEntryPetrol);
        
         insert pbEntryList;
         freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        insert freightMatrix;
        
        //create freight price
        freightPrice = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        insert freightPrice;
        
        EP_Freight_Price__c freightPrice1 = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        freightPrice1.EP_Max_Distance__c = 200;
        freightPrice1.EP_Max_Volume__c = 50;
        freightPrice1.EP_Min_Distance__c = 100;
        freightPrice1.EP_Min_Volume__c = 20;
        freightPrice1.EP_Freight_Price__c = 100.54;
        insert freightPrice1;
         
        
        // create bill to Account
        Account billToAccount = EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update billToAccount;     
        // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.id;
        insert sellToAccount;
        
        // create non vmi ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId ;
        nonVmiShipToAccount.CurrencyIsoCode = 'GBP';
        insert nonVmiShipToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        // insert tanks in order to mark vmi account as Active
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);        
        insert tanksToBeInsertedList;
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVmiShipToAccount;
        
        
          
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId;
        update nonVmiShipToAccount;
        
                
        // create Storage Location type Account
        List<Account> storageLocAcc = new List<Account>();
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccSecondary = EP_TestDataUtility.createStockHoldingLocation();
        Account storageLocAccAlternate = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAcc.add(storageLocAccPrimary);
        storageLocAcc.add(storageLocAccSecondary);
        storageLocAcc.add(storageLocAccAlternate);
        Database.insert(storageLocAcc,false);
        
        //create stock holding location
        List<EP_Stock_Holding_Location__c> stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
        EP_Stock_Holding_Location__c primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Primary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccPrimary.id,EP_Distance__c=7);
        EP_Stock_Holding_Location__c secondaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Secondary',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccSecondary.id,EP_Distance__c=12);
        EP_Stock_Holding_Location__c alternateStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Alternate',EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccAlternate.id,EP_Distance__c=20);
        stockholdingLocs.add(primaryStockHolding);
        stockholdingLocs.add(secondaryStockHolding);
        stockholdingLocs.add(alternateStockHolding);
        Database.insert(stockholdingLocs,false);
        
        
        //create Inventories
        List<EP_Inventory__c> inventories = new List<EP_Inventory__c>();
        EP_Inventory__c primaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccPrimary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c secondaryInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccSecondary.id,EP_Product__c=productDieselObj.id);
        EP_Inventory__c alterNateInventory = new EP_Inventory__c(EP_Storage_Location__c=storageLocAccAlternate.id,EP_Product__c=productDieselObj.id);
        inventories.add(primaryInventory);
        inventories.add(secondaryInventory);
        inventories.add(alterNateInventory);
        Database.insert(inventories,false);
        System.debug('inventories are '+inventories);
        PageReference pageRef = Page.EP_PortalOrderPage;
        Test.setCurrentPage(pageRef);//Applying page context here  
        ApexPages.currentPage().getParameters().put('id', sellToAccount.Id);
        EP_PortalOrderPageController portalOrder =new EP_PortalOrderPageController();
        EP_PortalOrderPageController portalOrderController =new EP_PortalOrderPageController();
        
        //portalOrderController.loadAccountOrderDetails();;
        
        // Set order type to "Delivery"
        //portalOrderController.strSelectedDeliveryType = 'Ex-Rack';//EP_Common_Constant.DELIVERY;
        //portalOrderController.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
        
        // Move to step #2
        //portalOrderController.next();
        
        // Ensure that step index has moved
        //System.assertEquals(2, portalOrderController.intOrderCreationStepIndex);
        
        // Select Ship-To
        //portalOrderController.strSelectedShipToId = nonVMIShipToAccount.Id;
        
        // Move to step #4 (step #3 is skipped for Delivery orders)
        //portalOrderController.next();        
        
         Test.stopTest();
        system.debug('success inventory');
        List<ORderItem> oList = [Select id,pricebookentry.product2.name,unitprice from orderitem where pricebookentryId=:freightPriceEntry.Id ];
        //assert the price added as freight
        system.assertEquals(0,oList.size());
        }catch(Exception ex){
            system.debug('============'+ApexPages.getMessages());
        }
         
        }   

  }
  
   private static void createTestData() {
        // TO DO: implement unit test
     
        // Inserting a bill- to account
        accObj1= EP_TestDataUtility.createBillToAccount();
        Database.insert(accObj1,true);
        accObj1.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update accObj1;
        accObj1.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update accObj1;
    
    // Inserting a EP_Freight_Matrix__c record      
        freightMatrix = createFrightMatrix('Test');
        insert freightMatrix;
        
    //Create a sell-to account record 
        EP_Payment_Term__c paymentTerm = new EP_Payment_Term__c();
        paymentTerm = EP_TestDataUtility.createPaymentTerm();
        paymentTerm.Name = EP_Common_Constant.CREDIT_PAYMENT_TERM;
        Database.insert(paymentTerm); 
        //create sell to
        accObj = EP_TestDataUtility.createSellToAccount(accObj1.Id,freightMatrix.id);
        insert accObj;
        accObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update accObj;  
        system.assertEquals('02-Basic Data Setup',accObj.EP_Status__c );
        accObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update accObj; 
        shipTo = EP_TestDataUtility.createShipToAccount(accObj.Id,shpToRecordTypeId );
        insert shipTo;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update shipTo;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update shipTo;
        
                
        transporter1 = EP_TestDataUtility.createTestVendor('Transporter','40001');
        transporter1.EP_Provider_Managed_Transport_Services__c = 'N/A';
        //transporter1.EP_Provider_Managed_Transport_Services__c = 'TRUE';
        insert transporter1;
        
        stockLoc = EP_TestDataUtility.createStockLocation(shipTo.id,true);
        stockLoc.EP_Distance__c = 50;
        stockLoc.EP_Transporter__c = transporter1.id;
        stockLoc.EP_Use_Managed_Transport_Services__c= 'FALSE';
        insert stockLoc;

      
        Order orderObj = createOrder(accObj.Id, orderRecTypeIdVMI, Test.getStandardPricebookId());
        insert orderObj;
        
        //update orderObj;
     
        Product2 productObj = createProduct('Test');
        Product2 taxProductObj = createProduct(EP_Common_Constant.TAX_PRODUCT_NAME);
        Product2 frieghtMatrixProductObj = createProduct(EP_Common_Constant.FREIGHT_PRICE_NAME);
        List<Product2> prodToInsert = new List<Product2>{productObj,taxProductObj,frieghtMatrixProductObj};
        insert prodToInsert;
        
        freightPrice = createFrightPrice(freightMatrix,prodToInsert[0].Id);
        insert freightPrice;
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[0].id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[1].id limit 1];
        PricebookEntry frieghtMatrixPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[2].id limit 1];
        priceBookEntryToInsert = new List<PricebookEntry>{pricebookEntryObj,taxPricebookEntryObj,frieghtMatrixPricebookEntryObj};
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        orderObj.EP_Delivery_Type__c = 'Delivery';
        orderObj.EP_ShipTo__c = shipTo.Id;
        orderObj.Stock_Holding_Location__c = stockLoc.Id;
        
        update orderObj;
        /*
        PricebookEntry pricebookEntryObj = createPricebookEntry(prodToInsert[0].Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(prodToInsert[1].Id, Test.getStandardPricebookId());
        PricebookEntry frieghtMatrixPricebookEntryObj = createPricebookEntry(prodToInsert[2].Id, Test.getStandardPricebookId());
        priceBookEntryToInsert = new List<PricebookEntry>{pricebookEntryObj,taxPricebookEntryObj,frieghtMatrixPricebookEntryObj};
        insert priceBookEntryToInsert;*/
         //Test Class Fix End
        system.debug(')))))))'+priceBookEntryToInsert);
            
       
        
    }
    
    
     /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test Order record
     * @params Id,Id,Id
     * @return Order
     */
    private static Order createOrder(Id acctId, Id recTypeId, Id pricebookId){
        Order obj = new Order();
        obj.AccountId = acctId;
        obj.RecordTypeId = recTypeId;
        obj.Status = 'Draft';
        obj.CurrencyIsoCode = 'USD';
        obj.PriceBook2Id = pricebookId;
        obj.EffectiveDate = system.today();
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test Product2 record
     * @params String
     * @return Product2
     */
    private static Product2 createProduct(String name){
        Product2 obj = new product2();
        obj.Name = name;
        obj.CurrencyIsoCode = 'USD';
        obj.IsActive = true;
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test PricebookEntry record
     * @params Id,Id
     * @return PricebookEntry
     */
    private static PricebookEntry createPricebookEntry(Id productId, Id pricebookId){
        PricebookEntry obj = new PricebookEntry();
        obj.Pricebook2Id = pricebookId;
        obj.Product2Id = productId;
        obj.UnitPrice = 1;
        obj.IsActive = true;
        obj.EP_VAT_GST__c = 14;
        obj.EP_Is_Sell_To_Assigned__c=True;
        obj.EP_Additional_Taxes__c = 5.0;
        obj.CurrencyIsoCode = 'USD';
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test OrderItem record
     * @params Id, Id
     * @return OrderItem
     */
    private static OrderItem createOrderItem(Id pricebookEntryId, Id orderId){
        OrderItem obj = new OrderItem();
        obj.PricebookEntryId = pricebookEntryId;
        obj.UnitPrice = 25.00;
        obj.Quantity = 1;
        obj.OrderId = orderId;
        return obj;
    }
    
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test FrightMatrix record
     * @params Id, Id
     * @return OrderItem
     */
     
    private static EP_Freight_Matrix__c createFrightMatrix(String Name){
        EP_Freight_Matrix__c oFreightMatrix = new EP_Freight_Matrix__c();
        oFreightMatrix.Name = Name; 
        return oFreightMatrix;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test FrightPrice record
     * @params Id, Id
     * @return OrderItem
     */
    
    private static EP_Freight_Price__c createFrightPrice(EP_Freight_Matrix__c oFm,Id prodId){
        EP_Freight_Price__c oFreightPrice = new EP_Freight_Price__c();
        //oFreightPrice.Name = 'Test';
        oFreightPrice.EP_Freight_Matrix__c = oFm.id;
        oFreightPrice.EP_Max_Distance__c = 100;
        oFreightPrice.EP_Min_Distance__c = 10;
        oFreightPrice.EP_Max_Volume__c = 1000;
        oFreightPrice.EP_Min_Volume__c = 1;
        oFreightPrice.EP_Distance_UOM__c = 'Km';
        oFreightPrice.EP_Volume_UOM__c = 'LT';
        oFreightPrice.EP_Freight_Price__c = 28.6;   
        oFreightPrice.EP_Product__c = prodId;
        oFreightPrice.CurrencyIsoCode = 'USD';
        return oFreightPrice;
    }
    
    private static testmethod void testInternal(){      
       
        
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();       
        Test.startTest();
        System.runAs(cscUser) {
        // create pricebook
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook Test', isActive=true);
        insert customPB;
        
        // insert product
        List<Product2> prodList = new List<Product2>();
        Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'USD', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'USD', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
        Product2  taxProd= new product2(Name = 'TaxProduct', CurrencyIsoCode = 'USD', isActive = TRUE);                           
        Product2  freightProd = new product2(Name = 'Freight Price', CurrencyIsoCode = 'USD', isActive = TRUE);                           
        prodList.add(productDieselObj);
        prodList.add(productPetrolObj);
        prodList.add(taxProd);
        prodList.add(freightProd);
        Database.insert(prodList);
  
         freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        insert freightMatrix;
        
        //create freight price
        freightPrice = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        insert freightPrice;
        
        EP_Freight_Price__c freightPrice1 = EP_TestDataUtility.createFreight(freightMatrix.Id,productDieselObj.Id);
        freightPrice1.EP_Max_Distance__c = 200;
        freightPrice1.EP_Max_Volume__c = 50;
        freightPrice1.EP_Min_Distance__c = 100;
        freightPrice1.EP_Min_Volume__c = 20;
        freightPrice1.EP_Freight_Price__c = 100.54;
        insert freightPrice1;
         
        
     }  
    }
    
    private static testmethod void testInternal1(){
        createTestData();
         Test.startTest();
         /*shipTo = EP_TestDataUtility.createShipToAccount(null,null);
         shipTo.BillingCity = 'test';
        insert shipTo;*/
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(shipTo.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);
        
        insert tanksToBeInsertedList;
        freightMatrix = createFrightMatrix('Test');
        insert freightMatrix;
        /*shipTo.EP_Status__c  = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update shipTo;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;*/
        EP_Freight_Matrix__c freightMatrix1 = createFrightMatrix('Test');
        insert freightMatrix1 ;

        
    }  
}