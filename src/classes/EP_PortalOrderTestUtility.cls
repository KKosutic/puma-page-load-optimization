/* 
  @Author <Accenture>
   @name <EP_PortalOrderTestUtility>
   @CreateDate <06/10/2015>
   @Description <This test class is used to test methods of portal Order>
   @Version <1.0>
 
*/
public class EP_PortalOrderTestUtility{
    private static Account sellToAccount;
    private static Account billToAccount;
    private static Account nonVmiShipToAccount;
    private static Account nonVmiShipToAccount2;
    private static Account storageShipToAcc;
    private static Account storageShipToAcc2;
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static Account storageLocAccPrimary;
    private static Account storageLocAccSecondary;
    private static Account storageLocAccAlternate;
    private static EP_Stock_Holding_Location__c primaryStockHolding;
    private static EP_Stock_Holding_Location__c secondaryStockHolding;
    private static EP_Stock_Holding_Location__c alternateStockHolding;
    private static EP_Inventory__c primaryInventory;
    private static EP_Inventory__c secondaryInventory;
    private static EP_Inventory__c alterNateInventory;
    private static String str;
    private static String newStr;
    private static EP_Tank__c tankInstance; 
    private static Id strNonVMIRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, 'Non-VMI Ship To');
    private static Id strStorageShipToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, 'Storage Ship-To');
    private static Profile cscAgent = [Select id from Profile Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE Limit 1];
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    private static  PricebookEntry pbEntryPetrol;
    private static  PricebookEntry pbEntryDiesel;
    private static Product2 productPetrolObj;
    private static Product2 productDieselObj;
    private static Product2 taxProduct;
    private static Product2 freightProduct;
    private static Profile stockAccountantProfile = [Select id from Profile Where Name =: EP_Common_Constant.EP_Stock_Accountant_R1 Limit : EP_Common_Constant.ONE];
    private static  User stockAcctUser = EP_TestDataUtility.createUser(stockAccountantProfile.id);
    private static Profile stockControllerProfile = [Select id from Profile Where Name =: EP_Common_Constant.EP_Stock_Controller_R1 Limit : EP_Common_Constant.ONE];
    private static  User stockControllerUser = EP_TestDataUtility.createUser(stockAccountantProfile.id);
    private static EP_Region__c reg;
    /*
    Creating Test Data
    */
    public static void createTestData(){
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('India','IN', 'Asia');
            contry1.EP_Mixing_Allowed__c = True;
            database.insert(contry1);
            
            Company__c compObj = new Company__c();
            compObj = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
            database.insert(compObj);
            
            reg = EP_TestDataUtility.createRegion('East',contry1.id );
            database.insert(reg);
            
            //create PriceBook  
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book2', isActive=true);
            database.insert(customPB);
            
             Pricebook2 customPB2 = new Pricebook2(Name='Standard Price Book1', isActive=true);
            database.insert(customPB2);
            
            //create Product        
            productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Blended__c = false);                           
            database.insert(productPetrolObj);
            
            productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Blended__c = false);                           
            database.insert(productDieselObj);
            
            taxProduct = new product2(Name = 'TaxProduct', CurrencyIsoCode = 'GBP', isActive = TRUE,Eligible_for_Rebate__c = true,EP_Blended__c = false);
            database.insert(taxProduct);
            
            FreightProduct = new product2(Name = 'FreightPrice', CurrencyIsoCode = 'GBP', isActive = TRUE,Eligible_for_Rebate__c = true,EP_Blended__c = false);
            database.insert(FreightProduct); 
            
            Id pricebookId = Test.getStandardPricebookId();
            Id pricebookId2 = Test.getStandardPricebookId();
            
            pbEntryPetrol = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = productPetrolObj.Id,
            UnitPrice = 19, IsActive = false, CurrencyIsoCode = EP_Common_Constant.GBP);
            database.insert(pbEntryPetrol); 
            
            PricebookEntry taxEntry = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = taxProduct.Id,
            UnitPrice = 1, IsActive = false, CurrencyIsoCode = EP_Common_Constant.GBP);
            database.insert(taxEntry);
            
            PricebookEntry freightEntry = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = freightProduct.Id,
            UnitPrice = 1, IsActive = false, CurrencyIsoCode = EP_Common_Constant.GBP);
            database.insert(freightEntry);
            
            pbEntryDiesel = new PricebookEntry(
            Pricebook2Id = customPB2.Id, Product2Id = productDieselObj.Id,
            UnitPrice = 1, IsActive = false, CurrencyIsoCode = EP_Common_Constant.GBP);
            database.insert(pbEntryDiesel); 
            
            // create freight matrix      
            EP_Freight_Matrix__c FreightMat = EP_TestDataUtility.createFreightMatrix();
            database.insert(FreightMat);
            
            // create freight price
            EP_Freight_Price__c  FreightPrice = EP_TestDataUtility.createFreight(FreightMat.id,productPetrolObj.id); 
            FreightPrice.EP_Max_Distance__c = 1000;
            FreightPrice.EP_Min_Distance__c =1;
            FreightPrice.EP_Max_Volume__c = 1000; 
            FreightPrice.EP_Min_Volume__c = 1;     
            database.insert(FreightPrice);

             // create prepayment term
            EP_Payment_Term__c prepaymentTerm = EP_TestDataUtility.createPaymentTerm();
            prepaymentTerm.name = EP_Common_Constant.PREPAYMENT;
            database.insert(prepaymentTerm);

            sellToAccount =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
            sellToAccount.EP_Country__c=  contry1.id;
            sellToAccount.EP_Freight_Matrix__c= FreightMat.id;
            sellToAccount.EP_PriceBook__c  = customPB.Id;
            /* CAM 2.7 start */
            sellToAccount.EP_AvailableFunds__c=100000;
            /* CAM 2.7 end */
            sellToAccount.EP_Is_Dummy__c = false;
            sellToAccount.EP_Billing_Basis__c = 'Ordered';
            sellToAccount.EP_Email__c = 'asd@as.com';
            sellToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
            sellToAccount.EP_Cntry_Region__c = reg.id;
            database.insert(sellToAccount);
           System.assertEquals(sellToAccount.EP_Status__c,EP_Common_Constant.STATUS_PROSPECT);
            
            List<Account> storageLocAcc = new List<Account>();
            storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
            database.insert(storageLocAccPrimary);
            storageLocAccSecondary = EP_TestDataUtility.createStockHoldingLocation();
            database.insert(storageLocAccSecondary);
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
            database.update(sellToAccount);
            
            nonVmiShipToAccount =  EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strNonVMIRecordTypeId);
           nonVmiShipToAccount.EP_Pumps__c= 10;
           nonVmiShipToAccount.EP_Freight_Matrix__c= FreightMat.id;
           nonVmiShipToAccount.EP_PriceBook__c = customPB.Id;
           nonVmiShipToAccount.EP_Ship_To_Type__c =  EP_Common_Constant.NON_CONSIGNMENT;  //--c
           nonVmiShipToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
           nonVmiShipToAccount.EP_Transportation_Management__c = EP_Common_Constant.TRANSPORT_OWN;
           nonVmiShipToAccount.EP_Country__c = contry1.id;
           nonVmiShipToAccount.EP_Puma_Company__c = compObj.id;
           nonVmiShipToAccount.Phone = '98789798';
           nonVmiShipToAccount.EP_Email__c = 'a@A.com';
           database.insert(nonVmiShipToAccount);
           
           //create stock holding location
            List<EP_Stock_Holding_Location__c> stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
            primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = EP_Common_Constant.PRIMARY_LOCATION_NAME,EP_Sell_To__c=sellToAccount.id,Stock_Holding_Location__c=storageLocAccPrimary.id);
            stockholdingLocs.add(primaryStockHolding);
            secondaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = EP_Common_Constant.Secondary_Test,EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccSecondary.id);
            stockholdingLocs.add(secondaryStockHolding);
            Database.insert(stockholdingLocs,false);
           
           EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,productPetrolObj.Id);
           database.insert(tankObj); 
           nonVmiShipToAccount.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
           database.update(nonVmiShipToAccount);
           nonVmiShipToAccount.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
           database.update(nonVmiShipToAccount);
    }
}