/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Controller                            *
* @Created Date 26/12/2017                                      *
* @description  Class to show LookUp Data                       *
****************************************************************/
public with sharing class EP_LookUp_Controller {
    public EP_LookUp_Context ctx{get;set;}
    public EP_LookUp_Helper helperObj;
    public SObject objVariable;
    public EP_LookUp_Controller controller;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Controller                            *
* @description  Contructor                                      *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_LookUp_Controller(){
        try{
            ctx = new EP_LookUp_Context(objVariable);
            ctx.targetId = ApexPages.currentPage().getParameters().get(EP_Common_Constant.FIELDID);
            ctx.objectVariable = ApexPages.currentPage().getParameters().get(EP_Common_Constant.OBJECTNAME);
            ctx.IdVariable = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            ctx.inputHiddenId = ApexPages.currentPage().getParameters().get(EP_Common_Constant.INPUTHIDDENID);
            ctx.searchText = ApexPages.currentPage().getParameters().get(EP_Common_Constant.SEARCHTEXT);
            helperObj = new EP_LookUp_Helper(ctx);
        }Catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         hideClearSearchButton                           *
* @description  to hide button on page                          *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void hideClearSearchButton(){
        EP_GeneralUtility.Log('publi','EP_LookUp_Controller','hideClearSearchButton');
        try{
            helperObj.hideClearSearchButton();
        }Catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
        }
    } 

/****************************************************************
* @author       Accenture                                       *
* @name         goButton                                        *
* @description  to get filtered record                          *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void goButton(){
        EP_GeneralUtility.Log('public','EP_LookUp_Controller','goButton');
        try{
            helperObj.lookUpRecords();
        }Catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
        }
    }
}