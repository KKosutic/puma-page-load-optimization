/****************************************************************
@Author      Accenture                                          *
@name        EP_HoldingAssociationMapper                        *
@CreateDate  1/11/2017                                          *
@Description contains soql for Holding Association Object       *
@Version     1.0                                                *
****************************************************************/
public with sharing class EP_HoldingAssociationMapper {
	public EP_HoldingAssociationMapper() {
		
	}

/************************************************************
* @Author       Accenture
* @Name         getHoldingRecord
* @Date         11/02/2017
* @Description  This method will return holging association
* @Param        Account Id
* @return       Holding Association 
************************************************************/
    public EP_Holding_Association__c getHoldingAsscRecord(id accId){
       EP_Holding_Association__c holdAccObj =  [select id, EP_Date_of_Association__c,EP_Date_of_Dissociation__c,EP_Holding_Name__c,EP_Sell_To__c 
                                                    from EP_Holding_Association__c where EP_Sell_To__c =: accId order by EP_Date_of_Association__c desc limit 1];
       return holdAccObj;
    }
    
/************************************************************
* @Author       Accenture
* @Name         getAllHoldingAsscRecord
* @Date         11/09/2017
* @Description  This method will return holging association 
				recods based on filter conditions
* @Param        string - whrer clouse
* @return       list of Holding Association records
************************************************************/
    public static list<EP_Holding_Association__c> getAllHoldingAsscRecord(string filterCondition){
       string qureyString =  'select id,EP_Holding_Name__c,EP_Holding_Name__r.Name ,EP_Holding_Name__r.EP_holding_number__c ,EP_Date_of_Dissociation__c,EP_Date_of_Association__c ,'+
       						'EP_Sell_To__c, EP_Sell_To__r.EP_Account_Id__c,EP_Sell_To__r.name from EP_Holding_Association__c';
       qureyString = qureyString+ filterCondition;
       return Database.query(qureyString);
    }
    
}