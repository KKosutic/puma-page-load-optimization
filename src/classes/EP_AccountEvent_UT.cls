@isTest
public class EP_AccountEvent_UT{
	
static final string EVENT_NAME = EP_AccountConstant.NEW_RECORD;
static testMethod void getEventName_test() {
	String eventname = EVENT_NAME;
	EP_AccountEvent localObj = new EP_AccountEvent(eventname);
	Test.startTest();
		String result = localObj.getEventName();
	Test.stopTest();
	system.AssertEquals(EVENT_NAME,result);
}
static testMethod void setEventName_test() {
	String eventname = EVENT_NAME;
	EP_AccountEvent localObj = new EP_AccountEvent(eventname);
	String value = '01-ProspectTo01-Prospect';
	Test.startTest();
		localObj.setEventName(value);
	Test.stopTest();
	system.AssertEquals(value,localObj.getEventName());
}
static testMethod void getEventMessage_test() {
	String eventname = EVENT_NAME;
	EP_AccountEvent localObj = new EP_AccountEvent(eventname);
	string eventMessage = 'Test event message';
	localObj.setEventMessage(eventMessage);
	Test.startTest();
		String result = localObj.getEventMessage();
	Test.stopTest();
	System.AssertEquals(eventMessage,result);
}
static testMethod void setEventMessage_test() {
	String eventname = EVENT_NAME;
	EP_AccountEvent localObj = new EP_AccountEvent(eventname);
	string eventMessage = 'Test event message';	
	Test.startTest();
		localObj.setEventMessage(eventMessage);
	Test.stopTest();
	System.AssertEquals(eventMessage,localObj.getEventMessage());
}
static testMethod void addEventMessage_test() {
	String eventname = EVENT_NAME;
	EP_AccountEvent localObj = new EP_AccountEvent(eventname);
	string value = 'Test event message';	
	Test.startTest();
		localObj.addEventMessage(value);
	Test.stopTest();
	System.Assert(localObj.eventMessages.size() > 0);
}
}