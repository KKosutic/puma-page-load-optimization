@isTest
public class EP_PricingResponseWS_UT
{
	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
	static testMethod void pricingResponse_test() {
		EP_PricingResponseStub stub = EP_TestDataUtility.createPricingResponseStub();
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/PricingResponse/';  
		req.httpMethod = EP_Common_Constant.POST;
		req.requestBody = Blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response= res;
   		EP_PricingResponseWS.processResuest();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    } 
}