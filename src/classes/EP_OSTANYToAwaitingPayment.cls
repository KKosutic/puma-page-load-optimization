/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToAwaitingPayment>
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to awaiting payment>
*  @Version <1.0>
*/

public class EP_OSTANYToAwaitingPayment extends EP_OrderStateTransition{
    public EP_OSTANYToAwaitingPayment(){
        finalState = EP_OrderConstant.OrderState_Awaiting_Payment;
        //system.debug('In Constructor: EP_OSTANYToAwaitingPayment');
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingPayment','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingPayment','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTANYToAwaitingPayment');
        return super.isRegisteredForEvent();      
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingPayment','isGuardCondition');

        return true;
    }

}