/**
 * This class contains unit tests for validating the behavior of Apex  classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_OrderSyncWithWindms_Test {
    private static Account sellTo,billTo,nonVMIship,storageLoc,vendor1;
  private static EP_Freight_Matrix__c f1 = new EP_Freight_Matrix__c();
  private static EP_Stock_Holding_Location__c supplyOption;
  private static EP_Country__c country1 = new EP_Country__c();
  private static String COUNTRY_NAME = 'Australia';
  private static String COUNTRY_CODE = 'AU';
  private static String COUNTRY_REGION = 'Australia';
  private static String REGION_NAME = 'North-Australia';
  private static EP_Tank__c tankObj;
  private static Contract contract1 = new Contract();
  private static string VENDOR_ACCOUNT_RT = 'Vendor';
  private static final string VENDORTYPE = 'Transporter';
  private static final String NAV_VENDOR_ID_1 = '00001';
  private static EP_Route__c route1;
  private static  EP_Run__c run1;
  private static EP_Route_Allocation__c routeAllocation;
  private static EP_Supply_Location_Transporter__c SLT;
  private static PricebookEntry prodPricebookEntry;
  private static Product2 productObj1;
  private static EP_Stock_holding_location__c supplyOption1;
  private static EP_Order_Configuration__c orderConfig;
  private static EP_Inventory__c primaryInventory;
     
    
    private static final Id orderNONVMIRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get( EP_COMMON_CONSTANT.NONVMI_ORDER_RECORD_TYPE_NAME).getRecordTypeId();
    /*
    Update Account status
    */
    private static void updateAccountStatus(){
    
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;      
    }
    /*
    Order Sync With Windms On Accepted Status
    */
    static testMethod void OrderSyncWithWindmsOnAcceptedStatus() {
        
        
        
        Id pricebookId = Test.getStandardPricebookId(); 
        ////////// Create Product //////////
        productObj1 = new product2(Name = 'P1', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                    
        Database.insert(productObj1);
        /*PricebookEntry standardPrice1 = new PricebookEntry(
                                        Pricebook2Id = pricebookId, Product2Id = productObj1.Id,
                                        UnitPrice = 10000, IsActive = true);
        insert standardPrice1;*/
        
        // Create a custom price book //////
        Pricebook2 customPB1 = new Pricebook2(Name='PriceBook A', isActive=true);
        insert customPB1; 
        
        /////////Insert freightMatrix //////
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix, false);
        
        ///////////Insert Company record //////
        Company__c company = EP_TestDataUtility.createCompany('Test');
        database.insert(company, false);
        EP_Payment_Term__c payTerm = EP_TestDataUtility.createPaymentTerm();
        payTerm.Name = '31 Days from Invoice';
        insert payTerm;
        payTerm.EP_Payment_Term_Code__c = 'PP';
        update payTerm;
        ///////// BILL TO /////////
        Account bill1 = EP_TestDataUtility.createBillToAccount();
        insert bill1;
        updateAccountStatus();
        bill1.EP_Status__c = EP_COMMON_CONSTANT.STATUS_BASIC_DATA_SETUP;
        update bill1;
        bill1.EP_Status__c = EP_COMMON_CONSTANT.STATUS_ACTIVE;
        update bill1;
        
        
        
        
        
        /////// Insert Sell To Accounts /////////
        Account sell1 = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
        sell1.EP_PriceBook__c = pricebookId;
        sell1.EP_Puma_Company__c = company.Id;
        sell1.AccountNumber = '12345';
        insert sell1;
        
        //////////Create StockHolding locationn account 
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAccPrimary.EP_Nav_Stock_Location_Id__c = '155454';
        insert storageLocAccPrimary;
        
        Account venderAccount = new Account();
        venderAccount.RecordTypeId = [select Id from recordType where sObjectType = 'Account' and developername = 'EP_Vendor' limit 1][0].Id;
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        venderAccount.EP_Source_Entity_ID__c = 'TestVenderId';
        /* TFS fix 45559,45560,45567,45568 end*/
        venderAccount.EP_Puma_Company__c = company.Id;
        venderAccount.BillingCity = 'testCity';
        venderAccount.Name = 'testVender';
        insert venderAccount;

        EP_Stock_Holding_Location__c primaryStockHolding1 = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Primary',
        EP_Sell_To__c=sell1.id,
        EP_Transporter__c = venderAccount.Id,
        Stock_Holding_Location__c=storageLocAccPrimary.id);
        insert primaryStockHolding1;
        
        ////////Create Ship to records//////////
        Id recordtypeid = EP_Common_Util.fetchRecordTypeId(EP_COMMON_CONSTANT.ACCOUNT_OBJ, EP_COMMON_CONSTANT.NON_VMI_SHIP_TO);
        Account ship1 = EP_TestDataUtility.createShipToAccount(sell1.id,recordtypeid);
        ship1.name = 'NVMI1';
        ship1.EP_PriceBook__c = customPB1.id;
        insert ship1;
        
        
        //////////Create StockHolding locationn record 
        EP_Stock_Holding_Location__c primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Location_Type__c = 'Primary',EP_Transporter__c = venderAccount.Id,
        EP_Ship_To__c=ship1.id,Stock_Holding_Location__c=storageLocAccPrimary.id);
        insert primaryStockHolding;
        
        
        Order sellToOrder = createOrder(sell1.Id, orderNONVMIRecordTypeId, Test.getStandardPricebookId());
        //Database.SaveResult dsr = database.insert(sellToOrder,false);
        //System.assert(true,dsr.isSuccess());
        insert sellToOrder;
 
        Id recordtypeidVMI = EP_Common_Util.fetchRecordTypeId(EP_COMMON_CONSTANT.ACCOUNT_OBJ,EP_COMMON_CONSTANT.VMI_SHIP_TO);
        Account ship2 = EP_TestDataUtility.createShipToAccount(sell1.id,recordtypeidVMI);
        ship2.name = 'VMI1';
        ship2.EP_Ship_To_Type__c =EP_COMMON_CONSTANT.CONSIGNMENT;
        ship2.EP_PriceBook__c = customPB1.id;
        insert ship2;
        Test.startTest();
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_Order_Fieldset_Sfdc_Nav_Intg');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Order sellToOrderNew = createOrder(sell1.Id, orderNONVMIRecordTypeId, Test.getStandardPricebookId());
        sellToOrderNew.EP_ShipTo__c = ship1.id;
        sellToOrderNew.EP_Sell_To__c = sell1.Id;
        sellToOrderNew.EP_WinDMS_Trip_Reference_Number__c = '9879';
        sellToOrderNew.EP_WinDMS_Order_Version_Number__c='121234';
        sellToOrderNew.EP_WinDMS_Order_Number__c='123432';
        sellToOrderNew.EP_Requested_Delivery_Date__c = system.today()+5; // Defect 44398
        sellToOrderNew.Stock_Holding_Location__c = primaryStockHolding.Id;
        sellToOrderNew.EP_Delivery_Type__C = EP_COMMON_CONSTANT.ORDER_DELIVERY_TYPE_IS_DELIVERY;
        //insert sellToOrderNew; 
        
        /* code Added for Order insertion */
        
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
            //EP_OrderTriggerHandler.isExecuted = True;
            //EP_OrderUpdateHandler.AfterUpdateOrder = False;
            //EP_OrderTriggerHandler.isExecuteOnDraftStatus = False;
            createTestData();
            
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
            Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            
            PageReference pageRef = Page.EP_PortalOrderPage;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', sellTo.Id);
            /*EP_PortalOrderPageController portalOrder =new EP_PortalOrderPageController();
            portalOrder.loadAccountOrderDetails();
            portalOrder.newOrderRecord.EP_Order_Category__c = 'Sales Order';
            portalOrder.newOrderRecord.EP_Order_Epoch__c = 'Current';
            portalOrder.newOrderRecord.EP_Order_Product_Category__c = 'Bulk';
            portalOrder.strSelectedDeliveryType = 'Delivery';
            portalOrder.next();
            portalOrder.strSelectedShipToID= nonVMIship.Id;
            portalOrder.strSelectedProductID = productObj1.id;
            portalOrder.strSelectedPricebookID= prodPricebookEntry.id;
            portalOrder.newOrderRecord.EP_ShipTo__c = nonVMIship.Id;
            system.assertNotEquals(portalOrder.strSelectedPricebookID,NULL);
            system.assertNotEquals(portalOrder.newOrderRecord.CurrencyIsoCode,NULL);
            system.assertNotEquals(portalOrder.newOrderRecord.EP_Order_Product_Category__c,NULL);
           /* PriceBookEntry priceBookEntries = [SELECT ID, Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Product_Sold_As__c, Product2.EP_Unit_Of_Measure__c,
                            Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c
                            FROM PriceBookEntry WHERE PriceBook2Id =: portalOrder.strSelectedPricebookID
                            AND Product2.IsActive = TRUE
                            AND CurrencyIsoCode =: portalOrder.newOrderRecord.CurrencyIsoCode 
                            AND Product2.EP_Product_Sold_As__c =: portalOrder.newOrderRecord.EP_Order_Product_Category__c];            
            system.assertEquals(priceBookEntries,NULL);*/
			
            /*system.assertEquals(portalOrder.strSelectedShipToID,nonVMIship.Id);
            
            
            portalOrder.next();
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            //system.assertEquals(pageMessages ,NULL);
            
            portalOrder.next();
            
           // system.assert(portalOrder.blnAccountSetupError);
            portalOrder.strSelectedPaymentTerm = 'Credit';
            portalOrder.newOrderRecord.Stock_Holding_Location__c = supplyOption1.Id;
            //portalOrder.strTransporterId = SLT.Id;
            portalOrder.strSelectedDate = String.valueOf(System.Now()+7);
           // portalOrder.aOrderWrapper[0].oliTanksID = tankObj.Id;
            portalOrder.aOrderWrapper[0].oliQuantity = 20;
            
            
            portalOrder.next();
            //portalOrder.submit();*/
        }
        /* code Added for order insertion ends */
        
        Set<Id> oId = new Set<Id>();
        oId.add(sellToOrder.Id);
        oId.add(sellToOrderNew.Id);      
        
        OrderItem orderItem = new OrderItem();
       // orderItem.PricebookEntryId = standardPrice1.Id;
        orderItem.UnitPrice = 1;
        orderItem.Quantity = 1;
        orderItem.orderId = sellToOrder.Id;
        orderItem.EP_Eligible_for_Rebate__c  = true;
        orderItem.EP_Is_Standard__c = true;
        orderItem.EP_Is_Freight_Price__c = true;
        Database.insert (orderItem,false);
        
        OrderItem orderItemNew = new OrderItem();
       // orderItemNew.PricebookEntryId = standardPrice1.Id;
        orderItemNew.UnitPrice = 1;
        orderItemNew.Quantity = 1;
        orderItemNew.orderId = sellToOrderNew.Id;
        orderItemNew.EP_Eligible_for_Rebate__c  = true;
        orderItemNew.EP_Is_Standard__c = true;
        orderItemNew.EP_Quantity_UOM__c = 'LT';
        Database.insert (orderItemNew,false);
        
        set<Id> objectids = new set<Id>();
        objectids.add(sellToOrderNew.Id);
        objectids.add(orderItemNew.Id );
        system.debug('--objectids--'+objectids);
        List<sObject> Fieldset_Sfdc_Windms_Intg_list = Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.sObjectType, 'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
        //system.debug( 'orde=='+ [select id, OrderNumber,status, EP_Puma_Company_Code__c, Account.AccountNumber, EP_Ship_To_ID__c, EP_Requested_Delivery_Date__c, EP_NAV_Stock_Holding_Location_ID__c, Stock_Holding_Location__c, EP_Delivery_Type__c from order where Id =: sellToOrderNew.Id]);
        
        /*sellToOrderNew.Status = EP_COMMON_CONSTANT.SUBMITTED;
        update sellToOrderNew;
        sellToOrderNew.Status = EP_COMMON_CONSTANT.ORDER_ACCEPTED_STATUS;
        update sellToOrderNew;
        system.debug('sellToOrderNew='+sellToOrderNew+'orderItemNew='+orderItemNew);*/
        
        Test.stoptest(); 
        list <EP_IntegrationRecord__c> IntRecList =  [select id from EP_IntegrationRecord__c where EP_Object_ID__c IN: objectids ];
       // system.assertEquals(2,IntRecList.size() );
        
    }
    /*
    Create Order
    */
    private static Order createOrder(Id acctId, Id recTypeId, Id pricebookId){
        Order obj = new Order();
        obj.AccountId = acctId;
        obj.Status= EP_COMMON_CONSTANT.ORDER_DRAFT_STATUS;
        obj.RecordTypeId = recTypeId;
        obj.CurrencyIsoCode = 'GBP';
        obj.PriceBook2Id = pricebookId;
        obj.EffectiveDate = system.today();
        obj.EP_WinDMS_Trip_Reference_Number__c ='123';
        obj.EP_WinDMS_Order_Version_Number__c='12123';
        obj.EP_WinDMS_Order_Number__c='12343';
        return obj;
    }
    /*
    Create Test data
    */
     public static void createTestData(){
     id orderConfigCountryRangeRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                                                        EP_Common_Constant.COUNTRY_RANGE);
      EP_Region__c region;
      EP_Payment_Method__c paymentMethod = EP_TestDataUtility.createPaymentMethod('Cash Payment'
                                                                                        ,'CP');
        insert paymentMethod;
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;
        
        Company__c comp = EP_TestDataUtility.createCompany('AUN');
        comp.EP_Disallow_DD_for_PP_customers__c = true;
        database.insert(comp);
        
        country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country1);
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country1.Id);  
        Database.insert(region);
        
        Id pricebookId = Test.getStandardPricebookId(); 
        ////////// Create Product //////////
        productObj1 = new product2(Name = 'P1', CurrencyIsoCode = 'AUD', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Unit_of_Measure__c='LT',EP_Company_Lookup__c=comp.Id);  
        productObj1.EP_Product_Sold_As__c = 'Bulk';                 
        Database.insert(productObj1);
        prodPricebookEntry = [SELECT Id,CurrencyIsoCode,Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Unit_Of_Measure__c,
                        Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c,Product2.EP_Product_Sold_As__c FROM PricebookEntry WHERE Product2.id =: productObj1.id LIMIT : EP_Common_Constant.ONE];
        //system.assertEquals(NULL,prodPricebookEntry);
        // Create a custom price book //////
        Pricebook2 customPB1 = new Pricebook2(Name='PriceBook B', isActive=true,EP_Company__c=comp.ID);
        insert customPB1; 
      //Bill-to
      billTo = EP_TestDataUtility.createBillToAccount();
       billTo.EP_Country__c = country1.id;
      billTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
      billTo.EP_PriceBook__c = pricebookId;
      insert billTo;
      system.debug('--recordtype--'+billTo.recordtypeid);
      billTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(billTo,true);
        billTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(billTo,true);
      f1 = EP_TestDataUtility.createFreightMatrix();
      Database.insert(f1,true);
      sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,f1.Id);
      sellTo.EP_Country__c = country1.id;
      sellTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
        sellTo.EP_Puma_Company__c = comp.id;
        sellTo.EP_Payment_Method__c = paymentMethod.id;
        sellTo.EP_Requested_Payment_Terms__c = 'Credit';
        sellTo.EP_Recommended_Credit_Limit__c = 100;
        sellTo.EP_Bill_To_Account__c = billTo.Id;
        //sellTo.EP_Alternative_Payment_Method__c = 'Paylink';
        sellTo.EP_Excise_duty__c = 'Excise Free';
        sellTo.EP_PriceBook__c = pricebookId;
        Database.saveResult sv = Database.insert(sellTo);
        
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellTo;
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellTo;
        System.debug('sellTo###'+sellTo);
         // create country range order configuration record
        orderConfig =  new EP_Order_Configuration__c();
        orderConfig.RecordTypeId = orderConfigCountryRangeRecordType;
        orderConfig.EP_Country__c = country1.id;
        orderConfig.EP_Max_Quantity__c = 200;
        orderConfig.EP_Min_Quantity__c = 1;
        orderConfig.EP_Product__c = productObj1.id;
        orderConfig.EP_Volume_UOM__c = 'LT';
        Database.insert(orderConfig);
        
        
        Id recordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        nonVMIship = EP_TestDataUtility.createShipToAccount(sellTo.id,recordtypeid);
        nonVMIship.name = 'NVMI1';
        nonVMIship.EP_Puma_Company__c = comp.id;
        nonVMIship.EP_Country__c = country1.id;
        nonVMIship.EP_PriceBook__c = pricebookId;
        nonVMIship.EP_Ship_To_Type__c = 'Consignment';
        nonVMIship.EP_Duty__c = 'Excise Free';
        insert nonVMIship;
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVMIship);
        
        tankObj= new EP_Tank__c();
        tankObj.EP_Ship_To__c =nonVMIship.Id;
        tankObj.EP_Safe_Fill_Level__c=60;
        tankObj.EP_Deadstock__c=20;
        tankObj.EP_Tank_Status__c='New';
        tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry';
        tankObj.EP_Capacity__c=80;                
        tankObj.EP_Tank_Code__c = '111';
        tankObj.EP_Product__c = productObj1.Id;
        Database.insert(tankObj,true);
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        Database.update(tankObj);
        
        system.assertEquals(tankObj.EP_Tank_Code__c,'111');
        
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVMIship;
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL;
        Database.update(tankObj);
        
      
        
        
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT : EP_Common_Constant.ONE];
        
        storageLoc = EP_TestDataUtility.createStorageLocAccount(country1.Id,bhrs.Id);
        insert storageLoc;
        storageLoc.EP_Country__c = country1.id;
        storageLoc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update storageLoc;
        storageLoc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update storageLoc;
        
        // Adding Run and Route
        route1 =  new EP_Route__c(EP_Status__c = 'Active');
        route1.EP_Storage_Location__c = storageLoc.Id;
        Database.insert(route1);
        run1 =  new EP_Run__c(EP_Route__c = route1.Id);
        run1.EP_Run_End_Date__c = System.Today() + 60;
        run1.EP_Run_Start_Date__c = System.Today();
        Database.Insert(run1);
        routeAllocation = new EP_Route_Allocation__c();
        routeAllocation.Delivery_Window_End_Date__c = 10;
        routeAllocation.EP_Delivery_Window_Start_Date__c = 5;
        routeAllocation.EP_Route__c = route1.Id;
        routeAllocation.EP_Ship_To__c = nonVMIship.Id;
        Database.Insert(routeAllocation); 
        
       /* vendor1 = createTestVendor(VENDORTYPE, NAV_VENDOR_ID_1,comp);
        Database.insert(vendor1,true);
        vendor1.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(vendor1,true);
        vendor1.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(vendor1,true); */
        
        Id supplyLocSellToRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
        supplyOption1 = EP_TestDataUtility.createSellToStockLocation(sellTo.id,true,storageLoc.Id,supplyLocSellToRecordtypeid); //Add one SHL
        supplyOption1.EP_Trip_Duration__c = 1;
        //supplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption1.EP_Is_Pickup_Enabled__c = True;
        Database.insert(supplyOption1,false);
        
       Id supplyLocDeliveryRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        EP_Stock_holding_location__c supplyOption2 = EP_TestDataUtility.createShipToStockLocation(nonVMIship.id,true,storageLoc.Id,supplyLocDeliveryRecordtypeid ); //Add one SHL
        supplyOption2.EP_Trip_Duration__c = 1;
        supplyOption2.EP_Ship_To__c = nonVMIship.Id;
        //supplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption2.EP_Is_Pickup_Enabled__c = True;
        //supplyOption2.EP_Transporter__c = vendor1.id;
        Database.insert(supplyOption2,false);
        
       // primaryInventory = createInventory(storageLoc,productObj1);
        //insert primaryInventory;
        
        //contract1 = EP_TestDataUtility.createContract(storageLoc.Id,comp.Id,paymentTerm.Id,vendor1);
        //Database.insert(contract1,false);
        //contract1.Status = EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE;
        //Database.update(contract1,false);
        
        //SLT = createSupplyLocationTransporter(vendor1.Id, supplyOption2.Id, true );
        //Database.Insert(SLT);
    }
}