/*
    *  @Author <Aravindhan Ramalingam>
    *  @Name <EP_OSTANYToAwaitingInventoryReview>
    *  @CreateDate <03/02/2017>
    *  @Description <Handles order status change from any status to awaiting inventory review>
    *  @Version <1.0>
    */

    public class EP_OSTANYToAwaitingInventoryReview extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToAwaitingInventoryReview';

    public EP_OSTANYToAwaitingInventoryReview(){
        finalState = EP_OrderConstant.OrderState_Awaiting_Inventory_Review;
        //system.debug('In Constructor: EP_OSTANYToAwaitingInventoryReview');
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingInventoryReview','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingInventoryReview','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTANYToAwaitingInventoryReview');
        return super.isRegisteredForEvent();         
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToAwaitingInventoryReview','isGuardCondition');
        //system.debug('In isGuardCondition of EP_OSTANYToAwaitingInventoryReview');
        
        if (this.order.isInventoryLow() && !this.order.isRetrospective()){
            system.debug('recommendedFinalState is inventory review' );
            this.orderEvent.setEventMessage(EP_OrderConstant.INVENTORY_ISSUE);
            //this.finalState=EP_OrderConstant.OrderState_Awaiting_Inventory_Review;
            return true;
        }
        return false;               
    }
}