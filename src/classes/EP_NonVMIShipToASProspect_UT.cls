@isTest
public class EP_NonVMIShipToASProspect_UT
{
    static final string EVENT_NAME = '01-ProspectTo01-Prospect';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    static testMethod void doOnEntry_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASProspectDomainObject().getAccount();
        EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.Assert(true);
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    }
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest(); //No assert required
        System.Assert(true);
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())

    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe); 
        Test.startTest();
        String message;
        try{            
            Boolean result = localObj.doTransition();
        }catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
    }
    static testMethod void isInboundTransitionPossible_test() {
        EP_NonVMIShipToASProspect localObj = new EP_NonVMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/

}