/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_OrderEpoch
  * @CreateDate  : 31/01/2017
  * @Description : This is Parent for all the  Order Epoch Type 
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public virtual class EP_OrderEpoch{

    public EP_OrderEpoch() {
        
    }
    
    
    /** This method is returns NonVMI ShipTos for Current Order
     *  @date      15/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
     public virtual List<Account> getShipTos(List<Account> lstAccount,EP_ProductSoldAs productSoldAsType) {
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','getShipTos');

        return lstAccount;
    }
    
    
    /** This method validates the Loading Date on Order
     *  @date      15/02/2017
     *  @name      isValidLoadingDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
     public virtual Boolean isValidLoadingDate(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','isValidLoadingDate');
        return false;
    }
    
    
    /** This method validates the Expected Date on Order
     *  @date      15/02/2017
     *  @name      isValidExpectedDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
     public virtual Boolean isValidExpectedDate(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','isValidExpectedDate');
        return false;
    }
    
    
    /** This method validates the Start Date on Order
     *  @date      15/02/2017
     *  @name      isValidOrderDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
     public virtual Boolean isValidOrderDate(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','isValidOrderDate');
        return false;
    }
    
    
    /** This method returns the Id of order record type
     *  @date      15/02/2017
     *  @name      findRecordType
     *  @param     EP_VendorManagement vmType,EP_ConsignmentType consignmentType
     *  @return    Id
     *  @throws    NA
     */
     public virtual Id findRecordType(EP_VendorManagement vmType,EP_ConsignmentType consignmentType) {
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','findRecordType');
        return null;
    }
    
    
    /** This method is used to set the Requested Date Time field on Order
     *  @date      15/02/2017
     *  @name      setRequestedDateTime
     *  @param     Order orderObj,String selectedDate,String availableSlots
     *  @return    Order 
     *  @throws    NA
     */
     public virtual csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','setRequestedDateTime');
        return orderObj;
    }

     /** This method sets retrospective orders
     *  @date      03/03/2017
     *  @name      setRetroOrderDetails
     *  @param     Order orderObj
     *  @return    NA
     *  @throws    NA
     */
     public virtual void setRetroOrderDetails(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_OrderEpoch','setRetroOrderDetails');
    }
}