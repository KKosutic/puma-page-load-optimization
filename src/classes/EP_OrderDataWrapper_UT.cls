@isTest
public class EP_OrderDataWrapper_UT{
    static testMethod void constructorInvokeTest (){
        EP_OrderDataWrapper  localObj = new EP_OrderDataWrapper();
        System.assertequals(localObj.orderType, EP_Common_Constant.SALES_ORDER);
        System.assertequals(localObj.vmiType, EP_Common_Constant.NONVMI);
        System.assertequals(localObj.deliveryType, EP_Common_Constant.DELIVERY);
        System.assertequals(localObj.productSoldAsType, EP_Common_Constant.PRODUCT_BULK);
    }
}