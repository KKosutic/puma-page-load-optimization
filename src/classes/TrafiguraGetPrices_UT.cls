@isTest
public class TrafiguraGetPrices_UT {

    @isTest
    static void execute_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
        
        String jsonOrder = '{"lineItems":[{"companyCode":"666"}]}';
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('BasketId', newOrder.csord__Identification__c);
        inputMap.put('OrderJSON', jsonOrder);
        inputMap.put('AccId', newAccount.Id);
        
        Test.startTest();
            TrafiguraGetPrices trafigura = new TrafiguraGetPrices();
            trafigura.execute(inputMap);
        Test.stopTest();
    }
    
    @isTest
    static void test_test(){
    
        Test.startTest();
            TrafiguraGetPrices.test();
        Test.stopTest();
    }
}