/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToLoaded >
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to Loaded>
*  @Version <1.0>
*/

public class EP_OSTANYToLoaded extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToLoaded';
    public EP_OSTANYToLoaded(){
        finalState = EP_OrderConstant.OrderState_Loaded;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToLoaded','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToLoaded','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToLoaded','isGuardCondition');
        if (!String.isblank(this.order.getOrder().EP_WinDMS_Status__c) 
            && this.order.getOrder().EP_WinDMS_Status__c.equalsIgnoreCase(EP_Common_Constant.loaded)) {
            this.order.getOrder().EP_ETA_AT_Loading__c = this.order.getOrder().EP_Estimated_Delivery_Date_Time__c;
            setETAonOrder();
            system.debug('finalState is ' + this.finalState );
            orderEvent.setEventMessage(this.order.getOrder().EP_WinDMS_Status__c);
            return true;
        }
        //Defect #57297 START
        if(this.order.isPackaged()){
        	return true;
        }
        //Defect #57297 END
        return false;               
    }

    private void setETAonOrder(){
        EP_GeneralUtility.Log('private','EP_OSTANYToLoaded','setETAonOrder');
        Account selltoAcc = this.order.getSellToAccountDomain().getAccount();
        // Calculating ETA Range, calls General utility method.
        if(EP_GeneralUtility.isDateTimeValid(string.valueOf(this.order.getOrder().EP_ETA_AT_Loading__c))){
            this.order.getOrder().EP_Estimated_Time_Range__c = EP_GeneralUtility.getETARangeOnOrder(string.valueOf(this.order.getOrder().EP_ETA_AT_Loading__c), 
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_Start_Hours__c),
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_End_Hours__c),
             this.order.getOrder().Status__c);
        }
    }
}