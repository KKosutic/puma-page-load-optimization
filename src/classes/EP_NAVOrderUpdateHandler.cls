/* 
    @Author <Accenture>
    @name : EP_NAVOrderUpdateHandler>
    @CreateDate <03/06/2017>
    @description :This is the handler class to Update Orders Status Rest Service Request from NAV> 
    @Version <1.0>
*/ 
public with sharing class EP_NAVOrderUpdateHandler extends EP_InboundHandler { 
    private static boolean processingFailed = false;
    private static final String CLASS_NAME = 'EP_NAVOrderUpdateHandler';
    private static final String METHOD_NAME = 'processRequest';
    private static final String CS_INBOUND_MESSAGE = 'NAV_TO_SFDC_ORDER_STATUS_UPDATE';
    @testVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> (); 
    private static EP_NAVOrderUpdateHelper orderUpdateHelper = new EP_NAVOrderUpdateHelper();
    /**  
     * @name : processRequest  
     * @description :This method process the JSON Input string and returns jsonResponse. This method will be called from global class 'EP_OrderStatusUpdateWS'.   
     * @param String -It takes JSON requestBody.
     * @return string - It returns json Response.
     */
    public override string processRequest(String requestBody){
        EP_GeneralUtility.Log('Public','EP_NAVOrderUpdateHandler','processRequest');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        string failureReason;
        try {
            EP_NAVOrderUpdateStub stub = (EP_NAVOrderUpdateStub) System.JSON.deserialize(requestBody, EP_NAVOrderUpdateStub.class);
            headerCommon = stub.MSG.HeaderCommon;
            list<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = stub.MSG.Payload.any0.orderStatus.orders.order;
            system.debug('**orderWrapperList' + orderWrapperList);
            orderUpdateHelper = new EP_NAVOrderUpdateHelper();
            orderUpdateHelper.setOrderAttributes(orderWrapperList);
            updateOrders(orderWrapperList);
        } catch (exception exp ) {
            failureReason = exp.getMessage();
            EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_NAVOrderUpdateHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            createResponse(null, exp.getTypeName(), exp.getMessage());
        }
        return EP_AcknowledgementHandler.createAcknowledgement('NAV_TO_SFDC_ORDER_STATUS_UPDATE', processingFailed, failureReason, headerCommon, ackResponseList);
    }
        
    /** 
     * @name : updateOrderStatus 
     * @description :This method updates the status of Order.    
     * @param EP_NAVOrderUpdateHandler. It is Wrapper class instance holds all the values.
     * @return list<EP_NAVOrderUpdateHandler.orderWrapper>.It returns the list of orders with updated values.
     */
     @TestVisible
     private static void updateOrders(list<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHandler','updateOrderStatus');
        list<csord__Order__c> ordersToUpdateList = new list<csord__Order__c>();
        
        for(EP_NAVOrderUpdateStub.orderWrapper orderWrapper : orderWrapperList){
            System.Debug('Status-->'+orderWrapper.status);           
            if(string.isNotEmpty(orderWrapper.errorDescription)) {
                createResponse(orderWrapper.seqId,orderWrapper.errorCode, orderWrapper.errorDescription);
            } else { 
               ordersToUpdateList.add(orderWrapper.sfOrder);                              
            }    
        }
        doUpdate(ordersToUpdateList);
    }
    
     /**
     * @name : doUpdate
     * @description :This method performs the DML action - Update. List of orders   
     * @param list<order> List of orders are updated
              EP_NAVOrderUpdateHandlerTransform - It holds list orders with updated values and used to update the certain felds of orders if those orders are not inserted.
     * @return void 
     */
     @TestVisible
    private static void doUpdate(list<csord__Order__c> ordersToUpdateList) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHandler','doUpdate');
        Database.SaveResult[] ordersUpdateResult = Database.update(ordersToUpdateList, false);
        set<id> successOrderIdSet = new set<id>();
        for (Integer counter = 0; counter < ordersUpdateResult.size(); counter++) {
            if (!ordersUpdateResult[counter].isSuccess()) {
                processUpsertErrors(ordersUpdateResult[counter].getErrors(), ordersToUpdateList[counter].EP_SeqId__c);
            } else {
                successOrderIdSet.add(ordersUpdateResult[counter].id);
                createResponse(ordersToUpdateList[counter].EP_SeqId__c, '', '');
            }
        }
        //orderUpdateHelper.doPostActions(successOrderIdSet);
    }
    
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            system.debug('**Error Message**' + err.getMessage());
            createResponse(seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHandler','createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.ORDER_STRING, seqId, errorCode, errorDescription));
    }
}