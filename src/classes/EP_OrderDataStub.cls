public class  EP_OrderDataStub {

       public List<LineItem> lineItems;
       public String deliveryType; // = CS.getAttributeValue('Order_Type_0');
       public String VMIOrder; // = CS.getAttributeValue('VMI_Order_0');
       public String blanketOrderType; // = CS.getAttributeValue('Blanket_Order_Type_0');
       public String requestDate; // = CS.getAttributeValue('Requested_Date_0');
       public String orderEpoch; // = CS.getAttributeValue('Order_Epoch_0');
       public String truckAndTransport; // = CS.getAttributeValue('Truck_and_Transport_0');
       public String rro; // = CS.getAttributeValue('Reason_for_Restrospective_Orders__RO__0');
       public String truckId; // = CS.getAttributeValue('Truck_ID_0');
       public String orderType; // = CS.getAttributeValue('Order_Type_2_0');
       public String pickupLocation; // = CS.getAttributeValue('Pickup_Location_0');
       public String shipTo; // = CS.getAttributeValue('Delivery_0');
       public string accountNumber;
       public string quantity;
       public string run;
       public String supplyLocation; // = CS.getAttributeValue('Supply_Location_0');
       public String preferedDeliverySlot; // = CS.getAttributeValue('Preferred_Delivery_Slot_0');
       public String captiveORderReferenceNumber; // = CS.getAttributeValue('Captive_Order_Reference_Number_0');
       public String paymentTerm; // = CS.getAttributeValue('Payment_Term_0');
       public String supplierId; // = CS.getAttributeValue('Supplier_ID_0');
       public String overridePaymenTerm; // = CS.getAttributeValue('Override_Payment_Term_0');
       public String contractId; // = CS.getAttributeValue('Contract_ID_0');
       public String customPoNumber; // = CS.getAttributeValue('Customer_Reference_Number_0');
       public String bolNumber; // = CS.getAttributeValue('BOL_Number_0');
       public Id basketId; // = params.basketId;
       public Id configurationId; // = '{!editingConfigSObject.id}';
       public String currencyCode;
       public String transporterId;
    

	public class LineItem {
        public String lineItemId;
        public String quantity;
        public String companyCode;
        public String productCode;

        public LineItem() {
        	
        }

        public LineItem(String id, String quan, String companyCd, String productCd) {
            this.lineItemId = id;
            this.quantity = quan;
            this.companyCode = companyCd;
            this.productCode = productCd;
        }
    }
}