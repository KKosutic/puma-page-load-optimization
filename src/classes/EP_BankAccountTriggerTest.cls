/* 
  @Author <Ashok Arora>
  @name <EP_BankAccountTriggerTest>
  @CreateDate <30/10/2015>
  @Description <This is the test class BankAccount Trigger>
  @Version <1.0>
 
*/
@isTest
private class EP_BankAccountTriggerTest {
    private static final string VALIDATE_BILLTOTEST = 'validateBillToTest';
    private static final string CLASSNAME = 'EP_BankAccountTriggerTest';
    private static final String POST= 'POST'; 
    /*
        This method test for Bill to same as Sell to
    */
    static testMethod void validateBillToTest() {
        Integer count;
       
        EP_Freight_Matrix__c freightMatrix;
        List<Company__c> lCompany = new List<Company__c>();
        List<Account>lCustomerAccounts = new list<Account>();
        List<Account>lBillTo = new list<Account>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>(); 
        
        List<EP_Bank_Account__c>lBankAccounts;
        
        for(count = 0; count < 50; count++){
            lCompany.add(EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA + count +
                                            (String.valueOf(DateTime.now().millisecond()))));
        }
        Database.insert(lCompany);
        //CREATE BILL TO
        for(count = 0; count < 50; count ++){
            lBillTo.add(EP_TestDataUtility.createBillToAccount());
        }
        try{
            Database.insert(lBillTo);
            
        }catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA,
                                     VALIDATE_BILLTOTEST , CLASSNAME, ApexPages.Severity.ERROR);
        }
        for(Account billTo : lBillTo){
            billto.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        }
        database.update(lBillTo);
        for(Account billTo : lBillTo){
            billto.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        }
        database.update(lBillTo);   
        //CREATE FRIEGHT MATRIX
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        //CREATE CUSTOMER
        for(count = 0; count < 50; count ++){
            lCustomerAccounts.add(EP_TestDataUtility.createSellToAccount(lBillTo[count].id
                                                                            ,freightMatrix.id));
        }
        database.insert(lCustomerAccounts);
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                lBankAccounts = new list<EP_Bank_Account__c>();
                //CREATE BANK ACCOUNTS 
                for(count = 0; count < 50; count++){
                    lBankAccounts.add(EP_TestDataUtility.createBankAccount(lCustomerAccounts[0].id));
                }
                lSaveResult = Database.insert(lBankAccounts,false);
            }
        Test.stopTest();
        
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(saveResult.isSuccess());
            //ASSERT VALIDATION ERROR
            //System.assert(saveResult.getErrors()[0].getMessage().contains(System.Label.EP_BillTo_SellTo_Err_Msg));
            
        }
        
    }
    
    
    /*
        
        Create Bank Account as SM User When Bill to Same As Sell to
    */
   static testMethod void CreateBankAccSMTest() {
        Integer count;
       
        EP_Freight_Matrix__c freightMatrix;
        List<Company__c> lCompany = new List<Company__c>();
        List<Account>lCustomerAccounts = new list<Account>();
        List<Account>lBillTo = new list<Account>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>(); 
        
        List<EP_Bank_Account__c>lBankAccounts;
        set<Id>sBanks = new set<Id>();
        
      
        database.insert(lCustomerAccounts);
        Profile SM_PROFILE = [Select id from Profile
                                Where Name = :EP_Common_Constant.SM_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
        User smUser = EP_TestDataUtility.createUser(SM_PROFILE.id);
        Test.startTest();
            System.runAs(smUser){
                for(count = 0; count < 200; count++){
                    lCompany.add(EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA + count +
                                                    (String.valueOf(DateTime.now().millisecond()))));
                }
                Database.insert(lCompany);
                
                //CREATE FRIEGHT MATRIX
                freightMatrix = EP_TestDataUtility.createFreightMatrix();
                database.insert(freightMatrix);
                //CREATE CUSTOMER
                for(count = 0; count < 200; count ++){
                    lCustomerAccounts.add(EP_TestDataUtility.createSellToAccount(null,freightMatrix.id));
                }
                lBankAccounts = new list<EP_Bank_Account__c>();
                //CREATE BANK ACCOUNTS 
                for(count = 0; count < 200; count++){
                    lBankAccounts.add(EP_TestDataUtility.createBankAccount(lCustomerAccounts[0].id));
                }
                lSaveResult = Database.insert(lBankAccounts,false);
            }
        Test.stopTest();
        
        for(DataBase.SaveResult saveResult : lSaveResult){
            /*ASSERT BANK ACCOUNT NOT CREATED SUCCESSFULLY*/ 
            System.assert(!saveResult.isSuccess());
            sBanks.add(saveResult.getId());
            
            
        }
        list<EP_Action__c>lBankCSCReview = new list<EP_Action__c>([Select Id 
                                                                    From EP_Action__c 
                                                                    Where EP_Bank_Account__c IN:sBanks
                                                                      And RecordType.DeveloperName =: EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV]);
        System.assertEquals(0,lBankCSCReview.size());
        
    }
    
    /*
        
        Create Bank Account as TM User When Bill to Same As Sell to
    */
   static testMethod void CreateBankAccTMTest() {
        Integer count;
       
        EP_Freight_Matrix__c freightMatrix;
        List<Company__c> lCompany = new List<Company__c>();
        List<Account>lCustomerAccounts = new list<Account>();
        List<Account>lBillTo = new list<Account>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>(); 
        DataBase.DeleteResult[] lBankDeleteResult = new list<DataBase.DeleteResult>();
        set<Id>sBanks = new set<Id>();
        List<EP_Bank_Account__c>lBankAccounts;
        
      
        database.insert(lCustomerAccounts);
        Profile TM_PROFILE = [Select id from Profile
                                Where Name = :EP_Common_Constant.TM_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
        User tmUser = EP_TestDataUtility.createUser(TM_PROFILE.id);
        insert tmuser;
        System.runAs(tmUser){
        Test.startTest();
                EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
                EP_BankAccountTriggerHandler.isExecuteAfterUpdate = true;
                EP_IntegrationUtil.isCallout = true;
                for(count = 0; count < 200; count++){
                    lCompany.add(EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA + count +
                                                    (String.valueOf(DateTime.now().millisecond()))));
                }
                Database.insert(lCompany);          
                //CREATE FRIEGHT MATRIX
                freightMatrix = EP_TestDataUtility.createFreightMatrix();
                database.insert(freightMatrix);
                //CREATE CUSTOMER
                for(count = 0; count < 200; count ++){
                    lCustomerAccounts.add(EP_TestDataUtility.createSellToAccount(null
                                                                                    ,freightMatrix.id));
                }
                lBankAccounts = new list<EP_Bank_Account__c>();
                //CREATE BANK ACCOUNTS 
                for(count = 0; count < 200; count++){
                    lBankAccounts.add(EP_TestDataUtility.createBankAccount(lCustomerAccounts[0].id));
                }
                lSaveResult = Database.insert(lBankAccounts,false);
                lBankAccounts = new list<EP_Bank_Account__c>([Select id from EP_Bank_Account__c]);          
        Test.stopTest();
        }
        for(DataBase.SaveResult saveResult : lSaveResult){
            /*ASSERT BANK ACCOUNT CREATED SUCCESSFULLY*/
            System.assert(!saveResult.isSuccess());
            sBanks.add(saveResult.getId()); 
        } 
        list<EP_Action__c>lBankCSCReview = new list<EP_Action__c>([Select Id 
                                                                    From EP_Action__c 
                                                                    Where EP_Bank_Account__c IN:sBanks
                                                                      And RecordType.DeveloperName =: EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV]);       
        System.assertEquals(0,lBankCSCReview.size());
    }
    
    /*
        
        Create Bank Account as CSC User When Bill to Same As Sell to
    */
   static testMethod void CreateBankAccCSCTest() {
        Integer count;
       
        EP_Freight_Matrix__c freightMatrix;
        List<Company__c> lCompany = new List<Company__c>();
        List<Account>lCustomerAccounts = new list<Account>();
        List<Account>lBillTo = new list<Account>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>(); 
        set<Id>sBanks = new set<Id>();
        List<EP_Bank_Account__c>lBankAccounts;
        
      
        database.insert(lCustomerAccounts);
        Profile CSC_PROFILE = [Select id from Profile
                                Where Name = :EP_Common_Constant.CSC_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
        User cscUser  = EP_TestDataUtility.createUser(CSC_PROFILE.id);
        Test.startTest();
            System.runAs(cscUser){
                for(count = 0; count < 200; count++){
                    lCompany.add(EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA + count +
                                                        (String.valueOf(DateTime.now().millisecond()))));
                }
                Database.insert(lCompany);
                
                //CREATE FRIEGHT MATRIX
                freightMatrix = EP_TestDataUtility.createFreightMatrix();
                database.insert(freightMatrix);
                //CREATE CUSTOMER
                for(count = 0; count < 200; count ++){
                    lCustomerAccounts.add(EP_TestDataUtility.createSellToAccount(null
                                                                                    ,freightMatrix.id));
                }
                lBankAccounts = new list<EP_Bank_Account__c>();
                //CREATE BANK ACCOUNTS 
                for(count = 0; count < 200; count++){
                    lBankAccounts.add(EP_TestDataUtility.createBankAccount(lCustomerAccounts[0].id));
                }
                lSaveResult = Database.insert(lBankAccounts,false);
            }
        Test.stopTest();
        
        for(DataBase.SaveResult saveResult : lSaveResult){
            /*ASSERT BANK ACCOUNT CREATED SUCCESSFULLY*/
            System.assert(!saveResult.isSuccess());
            //system.assertEquals(null,saveResult.getErrors());
            sBanks.add(saveResult.getId()); 
        } 
        list<EP_Action__c>lBankCSCReview = new list<EP_Action__c>([Select Id 
                                                                    From EP_Action__c 
                                                                    Where EP_Bank_Account__c IN:sBanks
                                                                      And RecordType.DeveloperName =: EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV]);       
        System.assertEquals(0,lBankCSCReview.size());
    }
    
    
    /*  This method is to check the required field on selecting bank status Field as 
        Actice and Inactive.     
        User Story - US006 
        Test Script- 7039, 7036
    */
    static testMethod void validateFActiveANdInactiveAccout() {
        
        Integer count =0;
        EP_Freight_Matrix__c freightMatrix;
        List<Company__c> lCompany = new List<Company__c>();
        List<Account>lCustomerAccounts = new list<Account>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>(); 
        List<EP_Bank_Account__c> bankAcountToUpdate = new List<EP_Bank_Account__c>();
        List<EP_Bank_Account__c>lBankAccounts = new list<EP_Bank_Account__c>();
        DataBase.SaveResult[] lupdateResult = new list<DataBase.SaveResult>(); 
        for(count = 0; count < 200; count++){
            lCompany.add(EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA + count + 
            (String.valueOf(DateTime.now().millisecond()))));
        }
        database.insert(lCompany);
        
        //CREATE FRIEGHT MATRIX
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
            
        for(count = 0; count < 200; count ++){
            lCustomerAccounts.add(EP_TestDataUtility.createSellToAccount(null, freightMatrix.id));
        }
        database.insert(lCustomerAccounts);
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
        System.runAs(adminUser){
            lBankAccounts = new list<EP_Bank_Account__c>();
            //CREATE BANK ACCOUNTS 
            for(count = 0; count < 200; count++){
                EP_Bank_Account__c bankAcc = EP_TestDataUtility.createBankAccount(lCustomerAccounts[0].id);
                if(count>100){
                    // Set Status as 'Active'
                    bankAcc.EP_Bank_Account_Status__c = EP_Common_Constant.BANK_STATUS_ACTIVE;
                }else{
                    //Set  Status as '05-Inactive'
                    bankAcc.EP_Bank_Account_Status__c = EP_Common_Constant.BANK_STATUS_INACTIVE;
                }   
                lBankAccounts.add(bankAcc);
            }
            lSaveResult = Database.insert(lBankAccounts,false);
        }
        Test.stopTest();
        Integer cnt = 0;
        for(DataBase.SaveResult saveResult : lSaveResult){
            //ASSERT VALIDATION ERROR
           // System.assertEquals(null, saveResult.getErrors());
            System.assertEquals(true, saveResult.isSuccess());
            if(saveResult.isSuccess()){
                bankAcountToUpdate.add(lBankAccounts[cnt]);
            }   
            cnt++;
        }
        lupdateResult = database.update(bankAcountToUpdate, false);
        
        for(DataBase.SaveResult saveResult : lupdateResult){
            //ASSERT VALIDATION ERROR
            System.assertEquals(true, saveResult.isSuccess());
        }
    }
    /*
     * Automation Test script for 17F
     */
    static testMethod void automationScriptFor17F(){
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        Test.startTest();
        //RUN AS CSC USER
        System.runAs(cscUser){              
            Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            Database.saveResult sv = database.insert(customer);
            system.assert(sv.isSuccess());
            EP_Bank_Account__c bankAcc = EP_TestDataUtility.createBankAccount(customer.id);
            bankAcc.EP_Bank_Account_Status__c = EP_Common_Constant.BANK_STATUS_INACTIVE;
            sv = Database.insert(bankAcc);
            system.assert(sv.isSuccess());
            
            bankAcc.EP_Is_Default_Bank_Account__c = true;
            Try{
                sv = Database.update(bankAcc);
                system.assert(!sv.isSuccess());
            }catch(Exception e){
                system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            }
            bankAcc.EP_Bank_Account_Status__c = EP_Common_Constant.BANK_STATUS_ACTIVE; 
            bankAcc.EP_Is_Default_Bank_Account__c = true;
            system.assert(sv.isSuccess());
            bankAcc.EP_Is_Default_Bank_Account__c = false;
            system.assert(sv.isSuccess());
        }
        test.stopTest();
    } 
    /*
     * Automation Test script for 17F
     */
    static testMethod void test17FUnitTesting(){
        list<EP_Bank_Account__c> lNewAccounts = new list<EP_Bank_Account__c>();
        string fieldToName;
        map<id,EP_Bank_Account__c> mOldAccounts = new map<id,EP_Bank_Account__c>();
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        Test.startTest();
        //RUN AS CSC USER
        System.runAs(cscUser){
            //EP_BankAccountTriggerHelper.allowModificationOnBankAccount(lNewAccounts,mOldAccounts);
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
            Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            Database.saveResult sv = database.insert(customer);
            system.assert(sv.isSuccess());
            EP_Bank_Account__c bankAcc = EP_TestDataUtility.createBankAccount(customer.id);
            DataBase.saveResult bankAccSv = database.insert(bankAcc,false);
            bankAcc.EP_Phone__c = '435678';
            bankAccSv = database.update(bankAcc,false);
        Test.stopTest(); 
            system.assert(bankAccSv.isSuccess());  
            }                                               
    }
 
    /*
     * validation on isDefault checkbox dependent on the bank status field 
     */
    static testMethod void testActiveInactiveIsDefault(){
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        Test.startTest();
        //RUN AS CSC USER
        System.runAs(cscUser){
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
            Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            Database.saveResult sv = database.insert(customer);
            system.assert(sv.isSuccess());
            EP_Bank_Account__c bankAcc = EP_TestDataUtility.createBankAccount(customer.id);
            //bankAcc.EP_Is_Default_Bank_Account__c = true;
            bankAcc.EP_Bank_Account_Status__c = 'Inactive';
            sv = Database.insert(bankAcc, false);
            System.assert(sv.isSuccess());
        }   
    }

    static testMethod void testBankAccAfterAccActiveTest(){
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        Test.startTest();
        System.runAs(cscUser){  
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
            Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            Database.saveResult sv = database.insert(customer);
            system.assert(sv.isSuccess());
            
            EP_Bank_Account__c bankAcc2 = EP_TestDataUtility.createBankAccount(customer.id);
            //bankAcc2.EP_Bank_Account_Status__c = 'Inactive';
            sv = Database.insert(bankAcc2, false);
            System.assert(sv.isSuccess());
            EP_Bank_Account__c bcc = [select id,EP_Bank_Account_Status__c from EP_Bank_Account__c where id =:bankAcc2.id];
            system.assertEquals('Inactive',bankAcc2.EP_Bank_Account_Status__c);
            
            customer.EP_Status__C = EP_Common_Constant.STATUS_SET_UP;
            Database.update(customer,false);
            customer.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(customer,false);
            EP_Bank_Account__c bankAcc1 = EP_TestDataUtility.createBankAccount(customer.id);
            bankAcc1.EP_Bank_Account_Status__c = 'Inactive';
            sv = Database.insert(bankAcc1, false);
            System.assert(!sv.isSuccess());//Cannot add bank account after account is synced(05-Active)
        } 
        Test.stopTest();   
    }
    
    static testMethod void testBankAccResponseTest(){
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        String ACKNOWLEDGMENT = 'Acknowledgment';
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile integrationAgent = [Select id from Profile
                                Where Name ='EP - Interface User' 
                                limit :EP_Common_Constant.ONE];
        User integrationUser = EP_TestDataUtility.createUser(integrationAgent.id);          
        Test.startTest();
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
            Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
            Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            customer.EP_Puma_Company__c = company.id;
            Database.saveResult sv = database.insert(customer);
            system.assert(sv.isSuccess());
            Account acc = [select accountNumber from Account where id=: customer.id limit:EP_Common_Constant.ONE];
            system.debug('ed5tg7yh8'+acc.accountNumber);
            EP_Bank_Account__c bankAcc2 = EP_TestDataUtility.createBankAccount(customer.id);
            sv = Database.insert(bankAcc2, false);
            System.assert(sv.isSuccess());
            EP_Bank_Account__c bcc = [select id,Name from EP_Bank_Account__c where id =:bankAcc2.id];
            customer.EP_Status__C = EP_Common_Constant.STATUS_SET_UP;
            Database.update(customer,false);
            //customer.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            //Database.update(customer,false);
            
            set<String> bankAccIDSet = new set<String>{'00214875-56565', '00214875-BAI-145638'};
            set<String> accountnoSet = new set<String>{acc.accountNumber};
            Account a = [Select id, Name, AccountNumber , 
                                         (Select id, Name, EP_NAV_Bank_Account_Code__c 
                                            from  Bank_Accounts__r 
                                           where EP_NAV_Bank_Account_Code__c in :bankAccIDSet)
                                    From Account 
                                   Where AccountNumber in: accountnoSet 
                                         And EP_Puma_Company_Code__c =:EP_Common_Constant.EPUMA];
            system.debug('tryuiom'+a);
            /*String jsonString = '{"MSG":{"HeaderCommon":{"MsgID":"test","InterfaceType":"NAV","SourceGroupCompany":"",'+
                                '"DestinationGroupCompany":"","SourceCompany":"'+EP_Common_Constant.EPUMA+'","DestinationCompany":"","CorrelationID":"",'+
                                '"DestinationAddress":"","SourceResponseAddress":"","SourceUpdateStatusAddress":"",'+
                                '"DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"","EmailNotification":"",'+
                                '"ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true",'+
                                '"ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"",'+
                                '"UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true",'+
                                '"UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true",'+
                                '"CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Cust. Bank Acc. Out. Staging",'+
                                '"CommunicationType":"Async"},'+
                                '"Payload":{"any":{"custBanks":'+
                                '{"custBank":[{"seqId":"test1","identifier":{"custId":"'+acc.accountNumber+'","bankAccId":"'+bcc.Name+'"},'+
                                '"name":"Test Bank 27-07-2016","name2":"name2","bankBranchId":"12133","bankAccountNo":"3333344444222",'+
                                '"isActiveStatus":"Yes","clientId":"'+EP_Common_Constant.EPUMA+'"},'+
                                '{"seqId":"test1677","identifier":{"custId":"'+acc.accountNumber+'","bankAccId":"56565"},'+
                                '"name":"Test Ban","name2":"name2","bankBranchId":"12133","bankAccountNo":"33344444222",'+
                                '"isActiveStatus":"Yes","clientId":"'+EP_Common_Constant.EPUMA+'"}]}}},"StatusPayload":"StatusPayload"}}';    
            RestRequest req = new RestRequest();
            RestResponse response = new RestResponse();
            req.httpMethod = POST;
            req.requestBody = Blob.valueOf(jsonString);
            RestContext.request = req;
            RestContext.response = response;
            EP_BankAccountNAVToSFDCWS.upsertbankAccounts();
            map<id,EP_Bank_Account__c> bankMap = new map<id,EP_Bank_Account__c>([select id,Name,EP_Bank_Account_Status__c from EP_Bank_Account__c where EP_Account__c =:customer.id]);
            system.assertEquals('Active',bankMap.get(bankAcc2.id).EP_Bank_Account_Status__c);
            */
    }
}