@isTest
public class EP_Account_VendorManagement_UT {
    static testMethod void updateVMIShipToUTCTimeZoneOffset_test() {
        EP_Account_VendorManagement localObj = new EP_Account_VendorManagement();
        Account account = EP_TestDataUtility.getVMIShipTo();
        Test.startTest();
        localObj.updateVMIShipToUTCTimeZoneOffset(account);
        Test.stopTest();
        //No asserts as it is a super class and does not have any implementation hence adding dummy assert
        system.assert(true);
    }
    
   static testMethod void insertPlaceHolderTankDips_test() {
        EP_Account_VendorManagement localObj = new EP_Account_VendorManagement();
        Account account = EP_TestDataUtility.getVMIShipTo();
        Test.startTest();
        localObj.insertPlaceHolderTankDips(account);
        Test.stopTest();
        //No asserts as it is a super class and does not have any implementation hence adding dummy assert
        system.assert(true);
    } 
    
    static testMethod void deletePlaceHolderTankDips_test() {
        EP_Account_VendorManagement localObj = new EP_Account_VendorManagement();   
        Account account = EP_TestDataUtility.getVMIShipTo();
        Test.startTest();
        localObj.deletePlaceHolderTankDips(account);
        Test.stopTest();    
        //No asserts as it is a super class and does not have any implementation hence adding dummy assert
        system.assert(true);
    }
     
}