@isTest
public class EP_CustomerUpdateWS_UT
{
	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
	static testMethod void updateCustomer_test() {
		EP_CustomerUpdateStub stub = EP_TestDataUtility.createInboundCustomerMessageWithBanks();
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/cstmrUpdate';  //Request URL
		req.httpMethod = EP_Common_Constant.POST;//HTTP Request Type
		req.requestBody = Blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response= res;
   		EP_CustomerUpdateWS.updateCustomer();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    }
}