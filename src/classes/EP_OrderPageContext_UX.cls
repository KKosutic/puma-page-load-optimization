public with sharing class EP_OrderPageContext_UX {
    
    public csord__Order__c newOrderRecord {get;set;}   
    public Account orderAccount {get;set;} 
    public Account invoicedAccount;
    //public EP_ProfileMapper profileMapper;
    public EP_OrderMapper orderMapper;
    public EP_OrderDomainObject orderDomainObj;
    public EP_OrderService orderService;
    public EP_AccountDomainObject accountDomainObj;
    public EP_AccountService accountService; //only used once

    public String strSelectedAccountID {get;set;}
    public String strSelectedOrderID;
    public String strSelectedDeliveryType {get;set;}
    public String strSelectedShipToID {get;set;}
    public String strSelectedPickupLocationID {get;set;}
    public String strSelectedPaymentTerm {get;set;}
    public String strSelectedDate {get;set;}
    public string startDate{get;set;}
    public String orderDateStr{get;set;}
    public String loadingDateStr{get;set;}
    public String deliveredDateStr{get;set;}
    public String strSelectedRoute {get;set;}
    public String strSelectedRun{get;set;}
    public String strRelatedBulkOrder{get;set;}
    public String availableSlots { get; set; }
    public String profileName {get;set;}

    public Boolean isModification = false;
    public Boolean hasAccountSetupError {get;set;}
    public Boolean isOprtnlTankAvail {get;set;}
    public Boolean isSlottingEnabled{get;set;}
    public Boolean isRoutePresent;
    public Boolean isPriceCalculated;
    public Boolean isPricingRecieved{get;set;}
    public Boolean isPricingAvailable{get;set;}
    public Boolean hasOverdueInvoices {get;set;}
    public Boolean hasCreditIssues {get;set;}
    public Boolean isConsignmentOrder {get;set;}
    public Boolean showAddProducts {get;set;}
    public Boolean showSubmitButton {get;set;}
    public Boolean showCalculatePriceButton {get;set;}
    public Boolean startPolling{get;set;}
    public Boolean hasinvalidorderlines;

    public Double totalCostPE; 
    public Integer intOliIndex{ get;set;} // used in doUpdateSupplierContracts

    public List<Account> listOfShipTos;
    public List<String> availableDatesList{get;set;}

    public List<SelectOption> listOfSupplyLocationOptions;
    public List<SelectOption> transportAccount{get;set;}
    public List<SelectOption> availableRoutes{get;set;}
    public List<SelectOption> availableRuns{get;set;}
    public List<SelectOption> supplierOptions {get;set;}

    public List<OrderWrapper> listofOrderWrapper {get;set;}
    public List<OrderWrapper> listofNewOrderWrapper {get;set;}
    public List<csord__Order_Line_Item__c> oldOrderItems;

    public map<Integer,OrderWrapper> orderWrapperMap {get;set;}


    public Map<ID, EP_Tank__c> mapShipToOperationalTanks;
    public Map<ID, EP_Tank__c> mapShipToTanks;
    public Map<Id,String> runMap;
    public Map<String,List<SelectOption>> timeSlots{get;set;}
    public Map < Id, Account > mapOfShipTo;
    public Map < String, EP_Stock_Holding_Location__c > mapOfSupplyLocation;
    public Map<String,csord__Order__c> bulkRelatedOrderMap; 
    public Map < String, Decimal > productPriceMap;
    public Map < String, String > productNameMap;
    public Map < String, String > productPriceBookEntryMap;
    public Map < String, String > PriceBookEntryproductMap;
    public Map < Id, String > productInventoryMap;
    public Map < String, String > productUnitOfMeasureMap;
    public Map < String, String > productRangeMap;
    public Map < String, List < String >> availableProductQuantitiesMap;
    public map<String,PricebookEntry> pricebookEntryMap;
    public Map<Id,List<Contract>> supplierContractMap;

    public Set<Id> shipToOperationalTankIds = new Set<Id>();

    //TODO
    public String strSelectedPricebookID {get;set;}
    public Integer intAccountProductsFound;
    public Integer numberOfProductsInOrders;
    public Integer numberOfAvailableProducts;

    public List<String> setProductIDs;
    
    public EP_OrderPageContext_UX(String strSelectedID){

        orderMapper = new EP_OrderMapper();

        //used in modification scenario
        oldOrderItems = new List<csord__Order_Line_Item__c>();

        if(strSelectedID.substring(0,3).equals('001')){
            accountDomainObj = new EP_AccountDomainObject(strSelectedID);
            accountService = new EP_AccountService(accountDomainObj);
            orderAccount = accountDomainObj.localAccount;
            strSelectedAccountID = orderAccount.Id;
            newOrderRecord = new csord__Order__c(EP_Order_Epoch__c = EP_Common_Constant.EPOC_CURRENT, EP_Order_Category__c = EP_Common_Constant.SALES,EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK, EP_Delivery_Type__c = EP_Common_Constant.DELIVERY);  
            newOrderRecord.AccountId__c = strSelectedAccountID;
            if (orderAccount.EP_Is_Dummy__c) {
                newOrderRecord.EP_Order_Category__c = EP_Common_Constant.ORD_CAT_TRANSFER_ORD;
            }
            newOrderRecord.EP_Delivery_Type__c = orderAccount.EP_Delivery_Type__c;
            orderDomainObj = new EP_OrderDomainObject(newOrderRecord);
        }else{
            orderDomainObj = new EP_OrderDomainObject(strSelectedID);
            newOrderRecord  = orderDomainObj.getOrder();  
            accountDomainObj = new EP_AccountDomainObject(newOrderRecord.AccountId__c);
            accountService = new EP_AccountService(accountDomainObj);
            orderAccount = accountDomainObj.localAccount;
            strSelectedOrderID = newOrderRecord.Id;
            strSelectedAccountID = orderAccount.Id;
            isModification =true;
            oldOrderItems = orderDomainObj.getOrderItems();
        }

        profileName = new EP_ProfileMapper().getCurrentUserProfile().Name;
        initializeObjects(orderDomainObj);
        bulkRelatedOrderMap = EP_PortalOrderUtil.relatedBulkOrderMap(strSelectedID);
        orderWrapperMap = new map<Integer, OrderWrapper>();
        listofOrderWrapper = orderWrapperMap.values();
    }

    // methods will be also used in the controller for reinitizalization
    public void initializeObjects(EP_OrderDomainObject orderDomainObj){
        
        //Initialize the Order object for the page
        orderService = new EP_OrderService(orderDomainObj);
        hasAccountSetupError = false;
        availableDatesList = new List<String>();
        mapShipToOperationalTanks = new Map<ID, EP_Tank__c>();
        mapShipToTanks = new Map<ID, EP_Tank__c>();
        runMap = new Map<Id,String>();

        productPriceMap = new Map < String, Decimal > ();
        productNameMap = new Map < String, String > ();
        productPriceBookEntryMap = new Map < String, String > ();
        priceBookEntryproductMap = new Map < String, String > ();
        productInventoryMap = new Map < Id, String > ();
        productUnitOfMeasureMap = new Map < String, String > ();
        productRangeMap = new Map < String, String > (); // This map is used handle the product restrictions
        availableProductQuantitiesMap = new Map < String, List < String >> ();

        pricebookEntryMap = new map<String,PricebookEntry>();
        
        setProductIDs = new List < String > ();

        isRoutePresent = false;
        intAccountProductsFound = 0;
        numberOfProductsInOrders = 0;
        numberOfAvailableProducts = 0;
        totalCostPE = 0;
        isPriceCalculated = false;
        isPricingAvailable = false;
        hasOverdueInvoices = false;
        hasCreditIssues = false;
        isConsignmentOrder = false;
        isOprtnlTankAvail = false;
        showCalculatePriceButton = false;
        showSubmitButton = false;
        startPolling = false;
        hasinvalidorderlines = false;

        listofNewOrderWrapper = new List<OrderWrapper>();
        orderWrapperMap = new map<Integer,OrderWrapper>();

        EP_AccountDomainobject invoicingCustomer = new EP_AccountDomainObject(accountService.getBillTo());
        if(!invoicingCustomer.localaccount.EP_IsActive__c){
            hasAccountSetupError = true;
            String strErrorMsg = (Label.EP_Blocked_Order_Error).replace(EP_Common_Constant.GENERIC_MSG_REPLACE_STRING,invoicingCustomer.localAccount.EP_Account_Type__c);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,strErrorMsg));
        }
        else{
            invoicedAccount = invoicingCustomer.localaccount;
            hasAccountSetupError = false;
        }     
       // strSelectedDeliveryType = orderAccount.EP_Delivery_Type__c;
        newOrderRecord.CurrencyIsoCode = orderAccount.CurrencyIsoCode;
        newOrderRecord.EP_Credit_Status__c =  EP_Common_Constant.OK;
        newOrderRecord.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);         
    }
    
    public boolean isDummy{
        get{
            return orderAccount.EP_Is_Dummy__c;
        }
    }
    
    public boolean isDeliveryOrder{
        get{
            // Consumption order has Consumption as delivery type, but behaviour is same as Delivery
            return (EP_Common_Constant.DELIVERY.equalsIgnoreCase(newOrderRecord.EP_Delivery_Type__c) 
                || EP_Common_Constant.CONSUMPTION.equalsIgnoreCase(newOrderRecord.EP_Delivery_Type__c));
        }
    }

    public boolean isExrackOrder{
        get{
            return EP_Common_Constant.EX_RACK.equalsIgnoreCase(newOrderRecord.EP_Delivery_Type__c);
        }
    }

    public boolean isConsumptionOrder{
        get{
            return EP_Common_Constant.CONSUMPTION.equalsIgnoreCase(orderDomainObj.getType());
        }
    }

    public boolean isRoOrder{
       get{
            return (orderDomainObj.isRetrospective());
       }
       set;
    }

    public boolean isTransfer{
       get{
            return (orderDomainObj.isTransfer());
       }
       set;
    }

    public boolean isConsignment{
        get{
            return orderDomainObj.isConsignment();
        }
    }

    public boolean isBulkOrder{
        get{
            return (orderDomainObj.isbulk());
        }
        set;
    }

    public boolean isPackagedOrder{
        get{
            return (orderDomainObj.isPackaged());
        }
        set;
    }

    public Boolean isPrepaymentCustomer {
        get {
            return (EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(strSelectedPaymentTerm));
        }
    }
    
    public boolean isErrorInPage {
        get {
            Boolean hasError = FALSE;
            if (ApexPages.getMessages() != NULL) {
                hasError = ApexPages.hasMessages(ApexPages.severity.ERROR);
            } // End page error NULL check
            return hasError;
        }
    }

    public Double totalOrderCost {
        get {
            Double dblTotalCost = 0;
            if(newOrderRecord.Id != NULL ) {
                dblTotalCost = totalCostPE;        
            }
            return dblTotalCost;
        }
    }

    public List <SelectOption> listDeliveryTypeSelectOptions {
        get {
            listDeliveryTypeSelectOptions = new List < SelectOption > ();
            for (String deliveryType : orderDomainObj.getDeliveryTypes()) {
                listDeliveryTypeSelectOptions.add(new SelectOption(deliveryType, deliveryType));
            }  
            return listDeliveryTypeSelectOptions;
        }
        set;
    }

    public List<SelectOption> getTransporterPricingList(){
        EP_GeneralUtility.Log('Public','CLASSNAME','getTransporterPricingList');
        List<SelectOption> listTransportPricingOptions = new List<SelectOption>();
        for(Account transportPricingAccount : accountDomainObj.getTransporterPricingList()) {
            listTransportPricingOptions.add(EP_Common_Util.createOption(transportPricingAccount.id,transportPricingAccount.name)); 
        }
        return listTransportPricingOptions;
    }

    public void setRun(){
        EP_GeneralUtility.Log('Public','CLASSNAME','setRun');
        EP_RunMapper runMapper = new EP_RunMapper();
        List<SelectOption> runList = new List<SelectOption>();
        runList.add(new SelectOption(EP_Common_Constant.BLANK,EP_Common_Constant.PICKLIST_NONE));
        //Defect Fix Start #57583
        for(EP_Run__c objRun : runMapper.getActiveRunsByRoute(strSelectedRoute)) {
            runList.add(EP_Common_Util.createOption(objRun.id,objRun.name));
            runMap.put(objRun.id,objRun.EP_Run_Number__c);
        }
        
        if(strSelectedRoute != newOrderRecord.EP_Route__c) {
            if(strSelectedRoute == null)
                strSelectedRun = null;
        }
        //Defect Fix End #57583
        availableRuns = runList;
    }

    @TestVisible
    private void validateCustomer(){
        EP_GeneralUtility.Log('Private','CLASSNAME','validateCustomer');
        if(!accountDomainObj.isValidCustomer()){
            hasAccountSetupError = TRUE;
            if(accountDomainObj.isSellToBlocked){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Sell_To_Blocked_Order_Error));
            }
            else if(accountDomainObj.isBillToBlocked){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Bill_To_Blocked_Order_Error));
            }
        }   
    }

    public List<SelectOption> getlistOfShipToOptions(){
        
        List <SelectOption> listOfShipToOptions = new List <SelectOption>();
        for (Account shipToAcc : listOfShipTos) {
            listOfShipToOptions.add(EP_Common_Util.createOption(shipToAcc.ID, shipToAcc.Name));
        }
        if (listOfShipTos.isEmpty() && isDeliveryOrder) {
            hasAccountSetupError = true;
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Info, Label.EP_No_SupplyLocation_At_SellTo);
            ApexPages.addMessage(msg);
            return listOfShipToOptions;
        }    
        setMapOfShipToAccounts();
        return listOfShipToOptions;
    
    }

    public void setMapOfShipToAccounts(){
        mapOfShipTo = new map<Id,Account>();
        for (Account shipToAcc : listOfShipTos) {
                mapOfShipTo.put(shipToAcc.Id, shipToAcc);
        }
    }

    public List<SelectOption> listPaymentTermsOptions {        
        get {
            listPaymentTermsOptions = new List <SelectOption> ();
            for(String strPaymentTerm : orderService.getApplicablePaymentTerms(invoicedAccount)) {
                listPaymentTermsOptions.add(new SelectOption(strPaymentTerm, strPaymentTerm));
            }            
            return listPaymentTermsOptions;
        }
        set;
    }

    public List<SelectOption> getSupplyLocPricing(){
        EP_GeneralUtility.Log('Public','CLASSNAME','getSupplyLocPricing');
        List<SelectOption> listSupplyLocPricingOptions = new List<SelectOption>();                   
        for(Account supplylocPricingObj : accountDomainObj.getsupplyLocationPricingList()) {  
            listSupplyLocPricingOptions.add(EP_Common_Util.createOption(supplylocPricingObj.id,supplylocPricingObj.name));                   
        }
        return listSupplyLocPricingOptions;
    
    }

    public boolean showCustomerPoField{
        get{
            return orderDomainObj.showCustomerPOAllowed();
        }
    }

    public void setshiptodetailsonOrder(){
        EP_GeneralUtility.Log('Public','CLASSNAME','setshiptodetailsonOrder');
        system.debug('******* strSelectedShipToID is ' + strSelectedShipToID);
        system.debug('******* getType is ' + orderDomainObj.getType());
        system.debug('******* mapOfShipTo' + mapOfShipTo);
        if (isDeliveryOrder || isDummy) {
            if(!isModification){
                newOrderRecord.EP_ShipTo__c = strSelectedShipToID;
                newOrderRecord.EP_ShipTo__r = mapOfShipTo.get(strSelectedShipToID);  
            }
            mapShipToTanks = orderService.getTanks( strSelectedShipToID );
            mapShipToOperationalTanks = EP_PortalOrderUtil.getOperationalTanks( mapShipToTanks.values() );

        }
    }

    public void setDataForConsumptionOrder(){
        EP_GeneralUtility.Log('Public','CLASSNAME','setDataForConsumptionOrder');
        if(!EP_Common_Constant.CONSUMPTION_ORDER.equalsIgnoreCase(newOrderRecord.EP_Order_Category__c)) {
            return;
        }
        strSelectedDeliveryType = EP_Common_Constant.CONSUMPTION;
        newOrderRecord.EP_Delivery_Type__c = EP_Common_Constant.CONSUMPTION;
        newOrderRecord.EP_Order_Epoch__c = EP_Common_Constant.EPOC_RETROSPECTIVE;
    }

    public void setOrderDetailsForModification(){
        EP_GeneralUtility.Log('public','CLASSNAME','setPageVariablesforModificationController');
        strSelectedDate = convertDateStringFormat(String.valueOf(newOrderRecord.EP_Requested_Delivery_Date__c));
        orderDateStr = convertDateStringFormat(String.valueOf(newOrderRecord.EP_Order_Date__c));
        loadingDateStr = convertDateStringFormat(String.valueOf(newOrderRecord.EP_Loading_Date__c));
        deliveredDateStr = convertDateStringFormat(String.valueOf(newOrderRecord.EP_Expected_Delivery_Date__c));

        strSelectedPickupLocationID = newOrderRecord.Stock_Holding_Location__c;
        strSelectedShipToID = newOrderRecord.EP_ShipTo__c;
        //Defect Fix Start #57583
        strSelectedRoute = newOrderRecord.EP_Route__c;
        //Defect Fix End #57583
        strSelectedRun = newOrderRecord.EP_Run__c;
        strSelecteddeliveryType = newOrderRecord.EP_Delivery_Type__c; 
        //strCustomerCurrencyCode = orderAccount.CurrencyIsoCode;
        if(newOrderRecord.EP_Related_Bulk_Order__c != NULL){
            strRelatedBulkOrder = newOrderRecord.EP_Related_Bulk_Order__r.OrderNumber__c;
            newOrderRecord.EP_Bulk_Order_Number__c = strRelatedBulkOrder;
        }    
        
    }

    private String convertDateStringFormat(String dateStr){
        String formattedStr;
        if (String.isNotBlank(dateStr)) {
            String strDay = dateStr.subString(8, 10);
            String strMonth = dateStr.subString(5, 7);
            String strYear = dateStr.subString(0, 4);
            formattedStr = strDay +EP_Common_Constant.STRING_HYPHEN+strMonth+EP_Common_Constant.STRING_HYPHEN+strYear;
        }
        return formattedStr;
    }
    
    public String userCurrencyFormat {
        get {
           return EP_GeneralUtility.getNumericDisplayFormat();
        }
    }
    
    public without sharing class OrderWrapper {

        public csord__Order_Line_Item__c orderLineItem {get;set;}
        public List < csord__Order_Line_Item__c > listChildOrderLineItems {get;set;}
        public List < SelectOption > oliAvailableProducts {get;set;}
        public List < SelectOption > oliAvailableQuantities {get;set;}
        public List <SelectOption> oliAvailableTanks {get;set;}
        public List < SelectOption > contractOptions{get;set;}

        public Integer oliIndex {get;set;}
        public Integer oliQuantity {get;set;}
        //when rerendering must resolve to string type is occuring
        public String oliQuantityStr {get;set;}
        public Integer oliTankSafeFillLvl {get;set;}

        public Double oliTotalPrice {get;set;}
        public Double oliUnitPrice {get;set;}
        public Double oliBasePrice {get;set;}
        public Double oliCost {get;set;}
        public double oliTaxPercent {get;set;}
        public double oliTaxAmount {get;set;}
        public Decimal oliOtherCosts {get;set;}
        
        public Boolean oliIsValid {get;set;}
        public Boolean oliIsAdditionalItem {get;set;}        
        public String oliPricebookEntryID {get;set;}
        public String oliProductName {get;set;}
        public String oliProductError {get;set;}
        public String oliQuantityRange {get;set;}
        public String oliQuantityError {get;set;}
        public String oliProductUoM {get;set;}
        public String oliTanksID {get;set;}
        public String oliTanksName {get;set;}
        public String oliTankProductName {get;set;}
        public String supplierId{get;set;}            
        public String contractId{get;set;}            
        public String oliCurrencyCode {get;set;}
        public String oliDescription {get;set;}

        public Boolean isNew {get;set;}//for modification

        public OrderWrapper() {
            EP_GeneralUtility.Log('Public','OrderWrapper','OrderWrapper');
            orderLineItem = new csord__Order_Line_Item__c();
            orderLineItem.EP_Ambient_Loaded_Quantity__c = null;
            orderLineItem.EP_Standard_Loaded_Quantity__c = null;
            orderLineItem.EP_Ambient_Delivered_Quantity__c = null;
            orderLineItem.EP_Standard_Delivered_Quantity__c = null;
            orderLineItem.EP_BOL_Number__c = null;

            listChildOrderLineItems = new List < csord__Order_Line_Item__c > ();
            oliQuantityStr = '';
            oliIsValid = false;
            isNew = true;
        }
        
        
    }
}