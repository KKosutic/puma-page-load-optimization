@isTest
/*********************************************************************************************
            @Author <>
            @name <TQAppControllerTest >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/ 
private class TQAppControllerTest {
/*********************************************************************************************
            @Author <>
            @name <SampleOperations>
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/    
    @isTest static void SampleOperations() {
        
        TQAppController controller = new TQAppController();

        String networkId = controller.getNetworkId();  

        User u = [SELECT ID FROM USER WHERE UserType = 'Guest' and isActive=true LIMIT 1];
        User u2 = [SELECT ID FROM USER WHERE UserType != 'Guest' and isActive=true LIMIT 1];

        PageReference pr = new PageReference('ACNONE_test');
        PageReference pr2 = new PageReference('ACNONE_login');
        Test.setCurrentPage(pr);

        System.runAs(u) {
            PageReference temp = controller.initialAction();
            System.assertEquals(pr2.getUrl(), temp.getUrl());
            TQAppController.login('Testuser','authPassword','www.google.com');
        }

        Test.setCurrentPage(pr2);

        System.runAs(u2) {
            
            PageReference temp2 = controller.initialAction();
            System.assertEquals(pr.getUrl(), temp2.getUrl());
            
        }

        controller.authUsername = 'Testuser';
        controller.authPassword = 'authPassword';
        TQAppController.login('jonna.muralikrishna@accenture.com','authPassword','www.google.com');
        TQAppController.changePassword('authPassword');
        TQAppController.forgotPassword('jonna.muralikrishna@accenture.com');
        TQAppController.changePasswordWithVerification('test','test','test');        
        controller.adrumAppKey = '';
        String adrTest = controller.adrumAppKey ;
        controller.loginStatus = 'Test';
        
        System.assertEquals(networkId, Network.getNetworkId());
        
    }
    
}