/* ================================================
 * @Class Name : EP_CRM_EventTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_EventTrigger apex trigger.
 * @created date: 08/07/2016
 ================================================*/
@isTest(seealldata=false)
private class EP_CRM_EventTriggerTest{
     /* This method is used to create all test data and test before insert event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testEventInsertUpdateTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            
            Database.SaveResult saveUser = Database.insert(u);
            System.runAs(u){
                
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            
            Database.SaveResult saveAcc = Database.insert(acc);
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            
            Database.SaveResult saveOpp = Database.insert(opp);
            
            Event tempEvent = new Event ();
            tempEvent.OwnerId = UserInfo.getUserId();
            tempEvent.Subject='Donni';
            tempEvent.whatId = opp.Id;
            tempEvent.DurationInMinutes = 10;
            tempEvent.ActivityDateTime = system.now();
            
            Database.SaveResult saveEvent = Database.insert(tempEvent);
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            
            //update Event........
            tempEvent.Subject = 'test subject';
            
            Database.SaveResult updateEvent = Database.update(tempEvent);
             Test.startTest();
             //Check System assert---------
            Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
                
            test.stopTest();
            }
        }
        catch(exception ex){}
    }
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkEventDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
           
            Database.saveResult saveUser = Database.insert(u);
            System.runAs(u){
                
                 // Insert Account
                Account acc=new Account();
                acc.name='Test Accenture';
                acc.BillingCity = 'test City';
                acc.BillingStreet  = 'test street';
                acc.BillingState = 'test state';
                acc.BillingPostalCode = '201301';
                acc.BillingCountry = 'test';
               
                Database.SaveResult saveAcc = Database.insert(acc);
                //Insert Opportunity
                Opportunity opp=new Opportunity();
                opp.name='Test';
                opp.Stagename='Closed Won';
                opp.Account=acc;
                opp.EP_CRM_Credit_Approval_Received__c = true;
                opp.CloseDate=Date.Today() + 5;
                
                Database.SaveResult saveopp = Database.insert(opp);
                //insert Event..............
                list<event> lstEvent = new list<Event>();
                Event tempEvent = new Event ();
                tempEvent.OwnerId = UserInfo.getUserId();
                tempEvent.Subject='Donni';
                tempEvent.whatId = opp.Id;
                tempEvent.DurationInMinutes = 10;
                tempEvent.ActivityDateTime = system.now();
                
                Event tempEvent1 = new Event ();
                tempEvent1.OwnerId = UserInfo.getUserId();
                tempEvent1.Subject='Donni';
                tempEvent1.whatId = opp.Id;
                tempEvent1.DurationInMinutes = 10;
                tempEvent1.ActivityDateTime = system.now();
                
                lstEvent.add(tempEvent);
                lstEvent.add(tempEvent1);
                
                Database.SaveResult[] savelstEvent = Database.insert(lstEvent);
                //delete Event........
                
                Database.DeleteResult deletetempEvent = Database.delete(tempEvent);
                
                Test.startTest();
                //Check System assert---------
                Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
                
                //validate through system Assert 
                System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
                test.stopTest();
            }
        }
        catch(exception ex){}
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleEventDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
               
                // Insert Account
                Account acc=new Account();
                acc.name='Test Accenture';
                acc.BillingCity = 'test City';
                acc.BillingStreet  = 'test street';
                acc.BillingState = 'test state';
                acc.BillingPostalCode = '201301';
                acc.BillingCountry = 'test';
                Database.SaveResult saveacc = Database.insert(acc);
                
                //Insert Opportunity
                Opportunity opp=new Opportunity();
                opp.name='Test';
                opp.Stagename='Closed Won';
                opp.Account=acc;
                opp.EP_CRM_Credit_Approval_Received__c = true;
                opp.CloseDate=Date.Today() + 5;
                
                Database.SaveResult saveopp = Database.insert(opp);
                //insert Event..............
               
                Event tempEvent = new Event ();
                tempEvent.OwnerId = UserInfo.getUserId();
                tempEvent.Subject='Donni';
                tempEvent.whatId = opp.Id;
                tempEvent.DurationInMinutes = 10;
                tempEvent.ActivityDateTime = system.now();
                
                Database.SaveResult savetempEvent = Database.insert(tempEvent);
                
                //delete Event........
                delete tempEvent;
                Database.SaveResult deletetempEvent = Database.insert(tempEvent);
                
                Test.startTest();
                //Check System assert---------
                Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
                
                //validate through system Assert 
                System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);
                test.stopTest();
            }
        }
        catch(exception ex){}
    }
}