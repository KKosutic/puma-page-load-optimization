/***L4-45352 start****/
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_ProductListExtensionContext>                    *
*  @Description <Context class to declare and set variables>   *
*  @Version <1.0>                                              *
****************************************************************/
public with sharing class EP_ProductListExtensionContext {
    Public List<PricebookEntry> ListProd{get;set;}
    Public string PriceBookId{get;set;}
    Public string PriceBookdelId{get;set;}
    Public Boolean ShowErrorForTank{get;set;}
    Public EP_PriceBookEntryMapper PriceBookEntryMapper;
    Public Integer RowNum{get;set;}
    
/****************************************************************
* @author       Accenture                                       *
* @name         EP_ProductListExtensionContext                  *
* @description  constructor of class                            *
* @param        PriceBook2                                      *
* @return       NA                                              *
****************************************************************/
    public EP_ProductListExtensionContext(PriceBook2 PriceBook) {
        try{
            init();
        }catch(exception ex){
            system.debug('Error = '+ex.getMessage()+EP_Common_Constant.ATLINE+ex.getLineNumber());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + EP_Common_Constant.ATLINE +ex.getLineNumber())); 
        }
    }
/****************************************************************
* @author       Accenture                                       *
* @name         init                                            *
* @description  method to initialize variables                  *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/
    public void init(){
        ListProd = new List<PricebookEntry>();
        PriceBookEntryMapper = new EP_PriceBookEntryMapper();
        PriceBookdelId = '';
        RowNum = 0;
    }
}
/***L4-45352 end****/