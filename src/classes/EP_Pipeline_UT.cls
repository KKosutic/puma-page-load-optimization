@isTest
public class EP_Pipeline_UT
{

static testMethod void updateTransportService_test() {
    EP_Pipeline localObj = new EP_Pipeline();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getConsumptionOrder(); // check
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.updateTransportService(objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals('NA',objOrder.EP_Use_Managed_Transport_Services__c);
    // **** TILL HERE ~@~ *****
}
static testMethod void isOrderEntryValid_PositiveScenariotest() {
    EP_Pipeline localObj = new EP_Pipeline();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount =EP_Common_Constant.ONE;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isOrderEntryValid_NegativeScenariotest() {
    EP_Pipeline localObj = new EP_Pipeline();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount =0;//EP_Common_Constant.ZERO;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(false,result);
}
}