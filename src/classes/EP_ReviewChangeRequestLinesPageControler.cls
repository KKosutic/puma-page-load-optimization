/* 
  @Author <Spiros Markantonatos>
  @name <EP_ReviewChangeRequestLinesPageControler>
  @CreateDate <07/09/2016>
  @Description <This is controller class is used to allow users to mass-approve Change Request lines>
  @Version <1.0>
*/
public with sharing class EP_ReviewChangeRequestLinesPageControler {
    
    public EP_ChangeRequest__c changeRequest {get;set;}
    public EP_Action__c action {get;set;}
    public List<EP_ChangeRequestLine__c> lChangeRequestLines {get;set;}   
    public String strPageSubTitle {get;set;}
    public String strSelectedActionID {get;set;}
    
    private static final String OPEN_CHANGE_REQUEST_LINE_STATUS = 'Open';
    private static final String COMPLETED_CHANGE_REQUEST_STATUS = 'Completed';
    private static final String APPROVED_CHANGE_REQUEST_LINE_STATUS = 'Approved';
    private static final String REJECTED_CHANGE_REQUEST_LINE_STATUS = 'Rejected';
    private static final String CLASS_NAME = 'EP_ReviewChangeRequestLinesPageExt';
    private static final String UPDATE_METHOD = 'updateChangeRequestLines';
    private static final String UPDATE_ACTION_OWNER_METHOD = 'assignActionToRunningUser';
    private static final String REJECT_METHOD = 'rejectAllChangeRequestLines';
    private static final String APPROVE_METHOD = 'approveAllChangeRequestLines';
    private static final String ASSIGN_METHOD = 'assignActionToRunningUser';
    private static final String PRODUCT_LIST_CR_RT = 'EP_Product_List_Change_Request_Line';
    private static final String GENERIC_CR_RT = 'EP_Generic_Change_Request_Line';
    
    private static final string STR_QUERY1 = 'EP_Action__c = \'';
    private static final string STR_QUERY2 = '\' ';
    private static final string STR_QUERY3 = 'AND EP_Request_Status__c = :OPEN_CHANGE_REQUEST_LINE_STATUS ';
    private static final string STR_QUERY4 = 'AND EP_Is_User_Allowed_to_Review__c = TRUE';
    private static final string STR_QUERY5 = 'EP_Change_Request__c = \'';
    private static final string STR_QUERY6 = '\' ';
    private static final string STR_QUERY7 = 'SELECT ID, Name, EP_Source_Field_Label__c, EP_Original_Value__c, EP_New_Value__c, EP_Review_Step__c,';
    private static final string STR_QUERY8 = 'EP_Action__c, EP_Request_Status__c, EP_Reason_For_Rejection__c, EP_Action__r.OwnerId, EP_Action__r.Owner.Name, ';
    private static final string STR_QUERY9 = 'EP_Product_Name__c, EP_Product_Name__r.Name, EP_Product_Name__r.ProductCode, EP_Request_Type__c, EP_Street__c, EP_City__c, EP_State__c, EP_Country__c, ';
    private static final string STR_QUERY10 = 'EP_PostalCode__c, RecordTypeId ';
    private static final string STR_QUERY11 = 'FROM EP_ChangeRequestLine__c ';
    private static final string STR_QUERY12 = 'WHERE ';
    private static final string STR_QUERY13 = '/';
    private static final string STR_BLANK = '';
    
    public String genericChangeRequestLineRecordType {
        get {   
            if (genericChangeRequestLineRecordType == NULL)
            {
                genericChangeRequestLineRecordType = 
                    EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.CHANGE_REQUEST_LINE_OBJ, GENERIC_CR_RT);
            }
            return genericChangeRequestLineRecordType;
        }
        set;
    }
    
    public String productChangeRequestLineRecordType {
        get {   
            if (productChangeRequestLineRecordType == NULL)
            {
                productChangeRequestLineRecordType = 
                    EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.CHANGE_REQUEST_LINE_OBJ, PRODUCT_LIST_CR_RT);
            }
            return productChangeRequestLineRecordType;
        }
        set;
    }
    
        /*
    * @ Description: Constructor
    */
    /** Constructor**/
    public EP_ReviewChangeRequestLinesPageControler() {
        // Get the URL Parameter
        if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID) != NULL)
        {
            String strParentRecordID = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            
            strParentRecordID = String.escapeSingleQuotes(strParentRecordID);
            
            // First try to retrieve the change request (when the user is navigating to the page from the CR detail page)
            List<EP_ChangeRequest__c> lChangeRequests = [SELECT ID, Name FROM EP_ChangeRequest__c WHERE ID = :strParentRecordID Limit : EP_COMMON_CONSTANT.ONE];
            if (!lChangeRequests.isEmpty())
            {
                changeRequest = lChangeRequests[0];
                strPageSubTitle = changeRequest.Name;
            }
            
            // Then try to retrieve the change request (when the user is navigating to the page from the Action detail page)
            List<EP_Action__c> lActions = [SELECT ID, Name FROM EP_Action__c WHERE ID = :strParentRecordID Limit : EP_COMMON_CONSTANT.ONE];
            if (!lActions.isEmpty())
            {
                action = lActions[0];
                strPageSubTitle = action.Name;
            }
        }
        
        lChangeRequestLines = new List<EP_ChangeRequestLine__c>();
    }
    
    // Methods
    
    /*
    * @ Description: This method is used to allow the controller to retrieve all the relevant change request line records
    */
    public void retrieveAllChangeRequestLines() {
        try{
            if (changeRequest != NULL || action != NULL)
            {
                String strSOQL = STR_QUERY7 ;
                strSOQL += STR_QUERY8 ;
                strSOQL += STR_QUERY9 ;
                strSOQL += STR_QUERY10 ;
                strSOQL += STR_QUERY11 ;
                strSOQL += STR_QUERY12 ;
                
                String strWhereClause = STR_BLANK ;
                
                if (changeRequest != NULL)
                    strWhereClause = STR_QUERY5  + changeRequest.Id + STR_QUERY6 ;
                
                if (action != NULL)
                {
                    strWhereClause = STR_QUERY1  + action.Id + STR_QUERY2 ;
                }
                
                strWhereClause += STR_QUERY3 ;
                strWhereClause += STR_QUERY4 ;
                
                strSOQL += strWhereClause;
                
                lChangeRequestLines = Database.query(strSOQL);
                
            }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
        }
    }
    
    /*
    * @ Description: This method is used to allow the user to action to him directly from the mass update page
    */
    public PageReference assignActionToRunningUser() {
        try{
            if (strSelectedActionID != NULL)
            {
                // Update the list of change request lines so that the user can update them through the mass update screen
                for (EP_ChangeRequestLine__c crl : lChangeRequestLines)
                {
                    crl.EP_Action__r.OwnerId = UserInfo.getUserId();
                }
                EP_Action__c updateActionRecord = new EP_Action__c(ID = strSelectedActionID, OwnerId = UserInfo.getUserId());
                
                try {
                    Database.update(updateActionRecord);
                } catch(Exception ex){
                    EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, CLASS_NAME, 
                                                                        UPDATE_ACTION_OWNER_METHOD, ApexPages.Severity.ERROR);
                }
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, ASSIGN_METHOD, CLASS_NAME,apexPages.severity.ERROR);
        }
        return NULL;
    }
    
    /*
    * @ Description: This method is used to allow the user to navigate back to the Change Request record 
    */
    public PageReference cancel() {
        PageReference ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
        
        try{
            if (changeRequest != NULL)
            {
                if (changeRequest.Id != NULL)
                {
                    ref = new PageReference(STR_QUERY13  + changeRequest.Id);
                }
            }
            
            if (action != NULL)
            {
                if (action.Id != NULL)
                {
                    ref = new PageReference(STR_QUERY13  + action.Id);
                }
            }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
        }
        return ref;
    }
    
    /*
    * @ Description: This method is used to mass update all the change requests
    */
    public PageReference updateChangeRequestLines() {
        
        if (!lChangeRequestLines.isEmpty())
        {
            Savepoint sp = Database.setSavepoint();
            try {
                Database.update(lChangeRequestLines);
                /*
                Boolean blnCloseChangeRequest = TRUE;
                
                // Check that there are no open change request lines
                for (EP_ChangeRequestLine__c crl : lChangeRequestLines)
                {
                    if (crl.EP_Request_Status__c == OPEN_CHANGE_REQUEST_LINE_STATUS)
                    {
                        blnCloseChangeRequest = FALSE;
                        break;
                    }
                }
                
                if (blnCloseChangeRequest)
                {
                    changeRequest.EP_Request_Status__c = COMPLETED_CHANGE_REQUEST_STATUS;
                    Database.update(changeRequest);
                }
                */
                return cancel();
                
            } catch(Exception ex){
                Database.rollback(sp);
                EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, CLASS_NAME, 
                                                                    UPDATE_METHOD, ApexPages.Severity.ERROR);
            }
        }
        
        return NULL;
    }
    
    /*
    * @ Description: This method is used to mass approve all change requests
    */
    public PageReference approveAllChangeRequestLines() {
        try{
            for (EP_ChangeRequestLine__c crl : lChangeRequestLines)
            {
                crl.EP_Request_Status__c = APPROVED_CHANGE_REQUEST_LINE_STATUS;
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, APPROVE_METHOD, CLASS_NAME,apexPages.severity.ERROR);
        }
        return updateChangeRequestLines();
    }
    
    /*
    * @ Description: This method is used to mass reject all change requests
    */
    public PageReference rejectAllChangeRequestLines() {
        try{
            for (EP_ChangeRequestLine__c crl : lChangeRequestLines)
            {
                crl.EP_Request_Status__c = REJECTED_CHANGE_REQUEST_LINE_STATUS;
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, REJECT_METHOD, CLASS_NAME,apexPages.severity.ERROR);
        }
        return updateChangeRequestLines();
    }
    
}