/*
 *  @Author <Accenture>
 *  @Name <EP_StorageShipToASBasicDataSetup >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  02-Basic Data Setup Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageShipToASBasicDataSetup extends EP_AccountState{
     
    public EP_StorageShipToASBasicDataSetup() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASBasicDataSetup','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASBasicDataSetup','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASBasicDataSetup','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASBasicDataSetup','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASBasicDataSetup','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}