@isTest
public class EP_TankDipExportHandler_UT
{
    static String MESSAGE_ID = 'SFD-GBL-CPE-24012017-16:00:20-024242';
    @testSetup static void init() {
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Pricing_Engine_CS__c.SobjectType,'EP_Pricing_Engine_CS');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_Integration_StatusUpdateTestData');
    	//Test Class Fix Start
    	EP_TestDataUtility.createCommunicationSettingsCS();
    	//Test Class Fix End
    }
    
    static testMethod void processRequest_WithoutParameterPositivetest() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        tankDipRecords[0].RecordTypeId = EP_testDataUtility.RECORDTYPEID_ACTUAL;
        tankDipRecords[0].EP_Ambient_Quantity__c =200;
        tankDipRecords[0].EP_Tank_Dip_Exported__c = false;
        tankDipRecords[0].EP_Reading_Date_Time__c = System.Today() - 1;
        update  tankDipRecords;
        List<Product2> products = [SELECT Id, ProductCode from Product2];
        for(integer i =0;i<products.size();i++){
            products[i].ProductCode=string.valueOf(i+1500);
        }
        update products;
        EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
        tankDipRecords = tankDipMapper.getTankDipsToExport();
        EP_TankDipExportHandler localObj = new EP_TankDipExportHandler();
        Test.startTest();
        String result = localObj.processRequest();
        Test.stopTest();
        System.AssertNotEquals(EP_Common_Constant.TANK_DIP_FAILURE_RESPONSE,result);
    }
    
    static testMethod void processRequest_WithoutParameterNegativeetest() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        tankDipRecords[0].RecordTypeId = EP_testDataUtility.RECORDTYPEID_ACTUAL;
        tankDipRecords[0].EP_Ambient_Quantity__c =200;
        tankDipRecords[0].EP_Tank_Dip_Exported__c = false;
        tankDipRecords[0].EP_Reading_Date_Time__c = System.Today() - 1;
        update  tankDipRecords;
        
        EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
        tankDipRecords = tankDipMapper.getTankDipsToExport();
        EP_TankDipExportHandler localObj = new EP_TankDipExportHandler();
        Test.startTest();
        String result = localObj.processRequest();
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.TANK_DIP_FAILURE_RESPONSE,result);
    }
    
    static testMethod void processRequest_WithParamterPositivetest() {
        EP_TankDipExportHandler localObj = new EP_TankDipExportHandler();
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        tankDipRecords[0].RecordTypeId = EP_testDataUtility.RECORDTYPEID_ACTUAL;
        tankDipRecords[0].EP_Ambient_Quantity__c =200;
        tankDipRecords[0].EP_Tank_Dip_Exported__c = true;
        tankDipRecords[0].EP_Reading_Date_Time__c = System.Today() - 1;
        update tankDipRecords;
        
        EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
        acknowledgement.MSG.HeaderCommon.CorrelationID=MESSAGE_ID;
        EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
        record.EP_Message_ID__c = MESSAGE_ID;
        record.EP_Object_Type__c ='Tank Dip';
        record.EP_Object_ID__c=tankDipRecords[0].Id;
        record.EP_Status__c='SENT';
        record.EP_Source__c='Source';
        record.EP_Target__c='Target';
        Database.insert(record);
        
        String jsonRequest = JSON.serialize(acknowledgement);
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.SUCCESS,result); 
    }
    
    static testMethod void processRequest_WithParamterNegativetest() {
        EP_TankDipExportHandler localObj = new EP_TankDipExportHandler();
        String jsonRequest;
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.EXCEPTION_Response,result);  
    }

    static testMethod void updateTankDipRecords_test() {    
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        Test.startTest();
        EP_TankDipExportHandler.updateTankDipRecords(tankDipRecords);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.SENT_STATUS,tankDipRecords[0].EP_Integration_Status__c); 
    }

    static testMethod void createIntegrationRecords_test() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        Boolean transactionFailed = false;
        String failureReason;
        String messageId;
        Test.startTest();
        EP_TankDipExportHandler.createIntegrationRecords(tankDipRecords,transactionFailed,failureReason,messageId);
        Test.stopTest();
        List<EP_IntegrationRecord__c> intRec = [Select Id from EP_IntegrationRecord__c];
        system.assertEquals(0,intRec.size());
    }

    static testMethod void getObjectNameMap_test() {
        Test.startTest();
        Map<string,string> result = EP_TankDipExportHandler.getObjectNameMap();
        Test.stopTest();
        System.AssertNotEquals(0,result.size());
    }
    static testMethod void updateIntegrationRecords_test() {
        Account acc = EP_TestDataUtility.getSellTo();
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        EP_IntegrationRecord__c intRecordNew = EP_TestDataUtility.createIntegrationRec(acc.Id,EP_Common_Constant.ACCOUNTS,DateTime.now(),EP_Common_Constant.AAF_COMPANY,EP_Common_Constant.SENT_STATUS);
        Database.insert(intRecordNew); 
        String correlationId = intRecordNew.EP_Message_ID__c;
        EP_AcknowledgementStub.dataset dataset = new EP_AcknowledgementStub.dataset();
        Set<string> setTankDipIds = new Set<String>{tankDipRecords[0].id};
        Test.startTest();
        EP_TankDipExportHandler.updateIntegrationRecords(correlationId,dataset,setTankDipIds);
        Test.stopTest();
        intRecordNew = [Select Id, EP_Status__c from EP_IntegrationRecord__c];
        System.AssertEquals(EP_Common_constant.SYNC_STATUS,intRecordNew.EP_Status__c);
        
    }
    
    static testMethod void updateIntegrationRecords_ErrorSync_test() {
        Account acc = EP_TestDataUtility.getSellTo();
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        EP_IntegrationRecord__c intRecordNew = EP_TestDataUtility.createIntegrationRec(acc.Id,EP_Common_Constant.ACCOUNTS,DateTime.now(),EP_Common_Constant.AAF_COMPANY,EP_Common_Constant.SENT_STATUS);
        Database.insert(intRecordNew); 
        String correlationId = intRecordNew.EP_Message_ID__c;
        EP_AcknowledgementStub.dataset dataset = new EP_AcknowledgementStub.dataset();
        dataset.errorDescription = 'Error';
        Set<string> setTankDipIds = new Set<String>{tankDipRecords[0].id};
        Test.startTest();
        EP_TankDipExportHandler.updateIntegrationRecords(correlationId,dataset,setTankDipIds);
        Test.stopTest();
        intRecordNew = [Select Id, EP_Status__c from EP_IntegrationRecord__c];
        System.AssertEquals(EP_Common_constant.ERROR_SYNC_STATUS.toupperCase(),intRecordNew.EP_Status__c.toupperCase());        
    }
    
    static testMethod void updateTankDipRecords1_test() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        Set<String> setTankDipIds = new Set<String>{tankDipRecords[0].id};
        EP_AcknowledgementStub.dataset dataset = new EP_AcknowledgementStub.dataset();
        Test.startTest();
        EP_TankDipExportHandler.updateTankDipRecords(setTankDipIds, dataset);
        Test.stopTest();
        tankDipRecords = [Select EP_Integration_Status__c  from EP_Tank_Dip__c];
        System.AssertEquals(EP_Common_constant.SYNC_STATUS,tankDipRecords[0].EP_Integration_Status__c);
    }
    
    static testMethod void updateTankDipRecords_ErrorSync_test() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        Set<String> setTankDipIds = new Set<String>{tankDipRecords[0].id};
        EP_AcknowledgementStub.dataset dataset = new EP_AcknowledgementStub.dataset();
        dataset.errorDescription = 'Error';
        Test.startTest();
        EP_TankDipExportHandler.updateTankDipRecords(setTankDipIds, dataset);
        Test.stopTest();
        tankDipRecords = [Select EP_Integration_Status__c  from EP_Tank_Dip__c];
        System.AssertEquals(EP_Common_constant.ERROR_SYNC_STATUS.toupperCase(),tankDipRecords[0].EP_Integration_Status__c.toupperCase());
    }
}