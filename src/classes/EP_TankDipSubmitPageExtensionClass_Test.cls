///* 
//  @Author <Spiros Markantonatos>
//   @name <EP_TankDipPageExtensionClass_Test>
//   @CreateDate <06/11/2014>
//   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
//   @Version <1.0>
// 
//*/
//@isTest
public class EP_TankDipSubmitPageExtensionClass_Test{
//    private static user currentrunningUser;
//    private static List<Account> accList; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    private static EP_Tank__c TankInstance2; 
//    private static Account currentAccount;
//    
//    /**************************************************************************
//    *@Description : This method is used to create Data.                       *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init() {
//        // Create running user
//        currentrunningUser = EP_TestDataUtility.createTestRecordsForUser();   
//        // Create test account
//        accList = EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//        // Create 2 test tanks
//        TankInstance = EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//        TankInstance.EP_Tank_Status__c = label.EP_Tank_Stopped_Status_Value;
//        update TankInstance;
//        TankInstance.EP_Tank_Status__c = label.EP_Operation_Tank_Status_Label;
//        update TankInstance;
//        TankInstance2 = EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//        TankInstance.EP_Tank_Status__c = label.EP_Tank_Stopped_Status_Value;
//        update TankInstance;
//        TankInstance.EP_Tank_Status__c = label.EP_Operation_Tank_Status_Label;
//        update TankInstance;
//        
//        // Create test tank dips
//        List<EP_Tank_Dip__c> testTankDips = new List<EP_Tank_Dip__c>();
//        testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.id));
//        testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance2.id));
//        tankDipList = EP_TestDataUtility.createTestEP_Tank_Dip(testTankDips);
//        
//        for(EP_Tank_Dip__c dip : tankDipList){
//         dip.EP_Is_Placeholder_Tank_Dip__c = FALSE;
//        } // End for
//        
//        update tankDipList;
//        
//        for(EP_Tank_Dip__c dip:tankDipList){
//         dip.EP_Is_Placeholder_Tank_Dip__c = TRUE;
//        } // End for
//        
//        update tankDipList;
//    
//    }
//    
//    /**************************************************************************
//    *@Description : This method is used to .                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void Inforet(){ 
//     init();   
//     Test.startTest();
//       system.runas(currentrunningUser){     
//
//          Account acc = accList.get(0);
//          acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//          update acc;
//          PageReference pageRef = Page.EP_TankDipSubmitPage;
//          Test.setCurrentPage(pageRef);//Applying page context here  
//          ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//          ApexPages.currentPage().getParameters().put('Date', '20181015051719');        
//          EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//          controller.strSelectedTanksIDs = TankInstance.id;
//          controller.saveStockRecords();
//          boolean blnShipToHasMissingOlderTankDips= controller.blnShipToHasMissingOlderTankDips;
//          boolean blnShipToIsMissingTankDipsForToday= controller.blnShipToIsMissingTankDipsForToday;
//          boolean blnRedirectToAccountPage = controller.blnRedirectToAccountPage; 
//          List<EP_TankDipSubmitPageControllerClass.tankDipRecordClass> displayedTankDipLines = controller.displayedTankDipLines;
//          controller.strSelectedTanksIDs=';' + TankInstance.id + ';';
//          controller.saveStockRecords();  
//          controller.back(); 
//      }    
//     Test.stopTest();  
//
//    }   
//    
//    /*********************************************************************************
//    *@Description : This method is used to test account redirection and invalid date *
//    *@Params      : none                                                             *
//    *@Return      : void                                                             *    
//    **********************************************************************************/        
//    private static testMethod void Inforet2() { 
//        init();   
//        Test.startTest();
//            system.runas(currentrunningUser) {     
//            
//                Account acc = accList.get(0);
//                acc.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//                update acc;
//                PageReference pageRef = Page.EP_TankDipSubmitPage;
//                Test.setCurrentPage(pageRef); //Applying page context here  
//                ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//                ApexPages.currentPage().getParameters().put('date', '2018101505a719'); // Date is not valid (invalid character)
//                ApexPages.currentPage().getParameters().put('ret', 'account');
//                
//                EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//                controller.strSelectedTanksIDs = TankInstance.id;
//                controller.toggleSiteInputControls();
//                controller.strSelectedTanksIDs = ';'+TankInstance.id+';';
//                
//                System.assertEquals(TRUE, controller.blnRedirectToAccountPage);
//                System.assertEquals(TRUE, controller.blnInvalidDate);
//                
//                controller.next();
//                controller.saveStockRecords();
//                controller.cancel();
//            }
//        Test.stopTest();  
//    }
//    
//    /*********************************************************************************************
//    *@Description : This method is used to test data calculation based on user and site timezone *
//    *@Params      : none                                                                         *
//    *@Return      : void                                                                         *    
//    **********************************************************************************************/        
//    private static testMethod void testDateCalculation() { 
//        init();   
//        Test.startTest();
//            // Set user timezone < 0
//            currentrunningUser.TimeZoneSidKey = 'America/Los_Angeles';
//            Database.update(currentrunningUser);
//            
//            // Set site timezone < 0
//            system.runas(currentrunningUser) {     
//                Account acc = accList.get(0);
//                acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                acc.EP_Ship_To_UTC_Timezone__c = 'UTC−09:30';
//                update acc;
//                PageReference pageRef = Page.EP_TankDipSubmitPage;
//                Test.setCurrentPage(pageRef); //Applying page context here  
//                ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//                ApexPages.currentPage().getParameters().put('date', '20181015050719');
//                ApexPages.currentPage().getParameters().put('ret', 'account');
//                
//                EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//            }
//            
//            // Set user timezone > 0
//            currentrunningUser.TimeZoneSidKey = 'Australia/Brisbane';
//            Database.update(currentrunningUser);
//            
//            // Set site timezone < 0
//            system.runas(currentrunningUser) {     
//                Account acc = accList.get(0);
//                acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                acc.EP_Ship_To_UTC_Timezone__c = 'UTC−09:30';
//                update acc;
//                PageReference pageRef = Page.EP_TankDipSubmitPage;
//                Test.setCurrentPage(pageRef); //Applying page context here  
//                ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//                ApexPages.currentPage().getParameters().put('date', '20181015050719');
//                ApexPages.currentPage().getParameters().put('ret', 'account');
//                
//                EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//            }
//            
//            // Set user timezone > 0
//            
//            // Set site timezone > 0
//            system.runas(currentrunningUser) {     
//                Account acc = accList.get(0);
//                acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                acc.EP_Ship_To_UTC_Timezone__c = 'UTC+09:00';
//                update acc;
//                PageReference pageRef = Page.EP_TankDipSubmitPage;
//                Test.setCurrentPage(pageRef); //Applying page context here  
//                ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//                ApexPages.currentPage().getParameters().put('date', '20181015050719');
//                ApexPages.currentPage().getParameters().put('ret', 'account');
//                
//                EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//            }
//        Test.stopTest();  
//    }
//    
//    /**********************************************************************************************
//    *@Description : This method is used to test missing tank dip notification displayed on screen *
//    *@Params      : none                                                                          *
//    *@Return      : void                                                                          *    
//    ***********************************************************************************************/        
//    private static testMethod void testMissingDipNotification() { 
//        init(); 
//        // Set user timezone > 0
//        currentrunningUser.TimeZoneSidKey = 'Australia/Brisbane';
//        Database.update(currentrunningUser);
//        
//        Test.startTest();
//            system.runas(currentrunningUser) {    
//                // Add placeholder tank dips
//                // Create test tank dips
//                DateTime dtPlaceholderReadingDateTime = EP_PortalLibClass.returnLocationDateTimeFromInteger(2016, 1, 1, 1, 0, 0);
//                // Add UTC/Site different
//                dtPlaceholderReadingDateTime.addHours(10);
//                
//                List<EP_Tank_Dip__c> testTankDips = new List<EP_Tank_Dip__c>();
//                testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                                EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime ));
//                testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance2.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                                EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime));
//                testTankDips = EP_TestDataUtility.createTestEP_Tank_Dip(testTankDips);
//                
//                Account acc = accList.get(0);
//                acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//                acc.EP_Ship_To_UTC_Timezone__c = 'UTC-10:00';
//                update acc;
//                PageReference pageRef = Page.EP_TankDipSubmitPage;
//                Test.setCurrentPage(pageRef); //Applying page context here  
//                ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//                ApexPages.currentPage().getParameters().put('date', '20160101010000');
//                ApexPages.currentPage().getParameters().put('ret', 'account');
//                
//                EP_TankDipSubmitPageControllerClass controller = new EP_TankDipSubmitPageControllerClass();
//                
//                // Set reading time and update the wrapper
//                controller.intUserSelectedHour = 10;
//                controller.intUserSelectedMinute = 0;
//                
//                controller.updateTankDipReadingTime();
//                
//                System.assertEquals(10, controller.newTankDipRecords[0].intHour);
//                System.assertEquals(0, controller.newTankDipRecords[0].intMinute);
//                
//                // Set tank dips
//                for (EP_TankDipSubmitPageControllerClass.tankDipRecordClass td : controller.newTankDipRecords) {
//                    td.tankDip.EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime;
//                    td.tankDip.EP_Ambient_Quantity__c = 210;
//                    td.strTankID = TankInstance.Id;
//                    td.blnAddTankDip = TRUE;
//                } // End for
//                
//                controller.back();
//                System.assertEquals('3', controller.strStepIndex);
//                controller.strSelectedTanksIDs = ';' + TankInstance.Id + ';';
//                controller.next();
//                
//                controller.saveStockRecords();
//                 
//                controller.strStepIndex = '5';
//                PageReference ref = controller.next();
//                System.assertNotEquals(-1, ref.getURL().indexOf(acc.Id));
//                
//                controller.back();
//                System.assertEquals('3', controller.strStepIndex);
//                
//            }
//        Test.stopTest();  
//    }
}