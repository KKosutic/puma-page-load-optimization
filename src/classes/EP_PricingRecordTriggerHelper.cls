/* 
  @Author <Accenture>
   @name <EP_PricingRecordTriggerHelper >
   @CreateDate <06/07/2016>
   @Description <This is helper class of PricingRecord Trigger> 
   @Version <1.0>
*/
public with sharing class EP_PricingRecordTriggerHelper {

    private static final string CLASS_NAME = 'EP_PricingRecordTriggerHelper';
    private static final string sendResponseToLS = 'sendResponseToLS';
    private static final string updateLatestRecord = 'updateLatestRecord';
    
    /* 
      @Description <This method is used to send response to LS> 
    */
    public static void sendResponseToLS(Map<Id,EP_Pricing_Engine__c> oldPricingRecords,Map<Id,EP_Pricing_Engine__c> newPricingRecords){
        Set<Id> orderIdsToSyncSet = new Set<Id>();
        Set<Id> orderItemSyncSet = new Set<Id>();
        Integer nRows = EP_Common_Util.getQueryLimit();
        try{
            for( EP_Pricing_Engine__c pe : newPricingRecords.values() ){
                orderIdsToSyncSet.add( pe.EP_Order__c );    
            }  
            //Defect 47425 Start
            for( OrderItem oItem : [  Select id from OrderItem where EP_Is_Standard__c = True and OrderId IN : orderIdsToSyncSet and order.RecordType.Name = :EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME and order.EP_Order_Epoch__c !=: EP_Common_Constant.EPOC_RETROSPECTIVE Limit:nRows] ) { 
            //Defect 47425 End       
                orderItemSyncSet.add( oItem.Id );
            } 
            if( !orderItemSyncSet.isEmpty() ){
                 EP_Message_Id_Generator__c oMsgId = new EP_Message_Id_Generator__c();
                 insert oMsgId;
                 String msgName = [ Select name from EP_Message_Id_Generator__c where id =: oMsgId.Id Limit: EP_Common_Constant.ONE].Name;
                //EP_VMIOrderCreationHandler.updateOrdersStatus( orderIdsToSyncSet,msgName ,orderItemSyncSet);
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, sendResponseToLS, CLASS_NAME,apexPages.severity.ERROR);
        }
    }
    
   }