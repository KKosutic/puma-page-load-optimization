/* ================================================
 * @Class Name : EP_CRM_AccountTriggerHandler 
 * @author : Kamendra Singh
 * @Purpose: This is Account trigger handler class and used to update account related opportunites.
 * @created date: 28/07/2016
 ================================================*/
public with sharing class EP_CRM_AccountTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;
    public static Boolean isUpdateRecursiveTrigger = False;
    
    /* This method is execute after account record insertion.
    * @param : Account record list
    * @return: NA
    */  
    public static void afterInsertAccount(list<Account> TriggerNew){
        try{
            updateOpportunity(TriggerNew);
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    /* This method is execute after account record updation.
    * @param : Account record list
    * @return: NA
    */ 
    public static void afterUpdateAccount(list<Account> TriggerNew){
       try{
            updateOpportunity(TriggerNew);
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    /* This method is used to update account related opportunity.
    * @param : Account record list
    * @return: NA
    */
    private static void updateOpportunity(List<Account> TriggerNew){
        try{
            map<id,list<Opportunity>> mapAccIdTolistOpportunity = new map<id,list<Opportunity>>();
            list<Opportunity> opportunitylist = new list<opportunity>();
           
            for(Account objAcc:[select id,(Select id,AccountId,Payment_Term__c,EP_CRM_Payment_Method__c,Billing_Method__c,Delivery_Type__c from opportunities where StageName != 'Closed Won' and StageName != 'Closed Lost') from Account  where Id IN : TriggerNew limit 50000]){
                mapAccIdTolistOpportunity.put(objAcc.Id,new list<Opportunity>());
                if(mapAccIdTolistOpportunity.containskey(objAcc.Id)){
                    mapAccIdTolistOpportunity.get(objAcc.Id).addall(objAcc.opportunities);
                }
            }
            for(Account objAcc : TriggerNew){               
                if(mapAccIdTolistOpportunity.get(objAcc.Id) != null ){
                    for(Opportunity objOpport : mapAccIdTolistOpportunity.get(objAcc.Id)){                      
                        objOpport.Payment_Term__c = objAcc.EP_Payment_Term_Lookup__c;
                        objOpport.EP_CRM_Payment_Method__c = objAcc.EP_Payment_Method__c;
                        objOpport.Billing_Method__c = objAcc.EP_Billing_Method__c;
                        objOpport.Delivery_Type__c = objAcc.EP_Delivery_Type__c;
                        opportunitylist.add(objOpport);
                    }
                }
            }
            
            if(opportunitylist != null && !opportunitylist.isEmpty()){
                database.SaveResult[] updateOppSaveResult = Database.update(opportunitylist);
            }
        }
        catch(exception ex){           
            ex.getmessage();
        }
    }

}