/*
@Author <Accenture>
@name <EP_AccountType>
@CreateDate <02/24/2017>
@Description <Virtual class for all Account Types>
@Novasuite fixes - Required Documentation
@Version <1.0>
*/
public virtual class EP_AccountType {
    //Bulkification Implementation -- Start
    public EP_AccountDomainObject accountDomainObj;
    //Bulkification Implementation -- End
    /**  
    *  @Author      Accenture
    *  @ name       constructor of class EP_AccountType
    *  @param       EP_AccountDomainObject
    *  @Return      Null
    */
    public EP_AccountType(EP_AccountDomainObject accountDomainObj){
        this.accountDomainObj = accountDomainObj;
    } 

    public EP_AccountType(){
    }

    /**  
    *  @Author      Accenture
    *  @ name       setCustomerActivationDate
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setCustomerActivationDate(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setCustomerActivationDate');

    }
    /**  
    *  @Author      Accenture
    *  @ name       validateOverdueInvoice
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean validateOverdueInvoice(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','validateOverdueInvoice');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       insertPlaceHolderTankDips
    *  @param       null
    *  @Return      Null
    */
    public virtual void insertPlaceHolderTankDips(){
        EP_GeneralUtility.Log('Public','EP_AccountType','insertPlaceHolderTankDips');

    }
    /**  
    *  @Author      Accenture
    *  @ name       setCountry
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setCountry(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setCountry');

    }
    /**  
    *  @Author      Accenture
    *  @ name       isBankAccountRequired
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean isBankAccountRequired(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isBankAccountRequired');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasPackagedProduct
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean hasPackagedProduct(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasPackagedProduct');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isPackagedPaymentTermProvided
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean isPackagedPaymentTermProvided(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isPackagedPaymentTermProvided');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isProductListLinkedWithDifferentSellTo
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isProductListLinkedWithDifferentSellTo(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isProductListLinkedWithDifferentSellTo');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasSameCompanyOnProducts
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean hasSameCompanyOnProducts(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasSameCompanyOnProducts');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasSameCompanyOnProductList
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean hasSameCompanyOnProductList(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasSameCompanyOnProductList');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       syncCustomerToPricingEngine
    *  @param       Account
    *  @Return      Null
    */
    public virtual void syncCustomerToPricingEngine (Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','syncCustomerToPricingEngine');

    }
    /**  
    *  @Author      Accenture
    *  @ name       isSupplyOptionAttached
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isSupplyOptionAttached(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isSupplyOptionAttached');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasCompletedReviews
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean hasCompletedReviews(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasCompletedReviews');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       getBillTo
    *  @param       Account
    *  @Return      Account
    */
    public virtual Account getBillTo(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getBillTo');
        return null;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isProductListAttached
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isProductListAttached(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isProductListAttached');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       getSupplyLocationList
    *  @param       Id
    *  @Return      List<EP_Stock_Holding_Location__c>
    */
    public virtual List<EP_Stock_Holding_Location__c> getSupplyLocationList(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_AccountType','getSupplyLocationList');
        return new List<EP_Stock_Holding_Location__c>();
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasSetUpShipTo
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean hasSetUpShipTo(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasSetUpShipTo');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isDirectDebitPayment
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isDirectDebitPayment(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isDirectDebitPayment');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isKYCReviewRejected
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isKYCReviewRejected(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isKYCReviewRejected');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       setPackagePaymentTermValue
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setPackagePaymentTermValue(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setPackagePaymentTermValue');

    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerEditRequestToWinDMS
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setCountryLookup(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setCountryLookup');

    }
    /**  
    *  @Author      Accenture
    *  @ name       doBeforeInsertHandle
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doBeforeInsertHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doBeforeInsertHandle');

    }
    /**  
    *  @Author      Accenture
    *  @ name       doBeforeUpdateHandle
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doBeforeUpdateHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doBeforeUpdateHandle');

    }
    /**  
    *  @Author      Accenture
    *  @ name       doBeforeUpdateHandle
    *  @param       Account,Account
    *  @Return      Null
    */
    public virtual void doBeforeUpdateHandle(Account newAccount, Account oldAccount){
        EP_GeneralUtility.Log('Public','EP_AccountType','doBeforeUpdateHandle');

    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerCreateRequest
    *  @param       Account
    *  @Return      Null
    */
    public virtual void sendCustomerCreateRequest(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerCreateRequest');

    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerEditRequest
    *  @param       Account
    *  @Return      Null
    */
    public virtual void sendCustomerEditRequest(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerEditRequest');

    }
    /**  
    *  @Author      Accenture
    *  @ name       doAfterInsertHandle
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doAfterInsertHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doAfterInsertHandle');

    }
    /**  
    *  @Author      Accenture
    *  @ name       doAfterUpdateHandle
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doAfterUpdateHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doAfterUpdateHandle');

    }
    //#L4-45526 Start
    /**  
    *  @Author      Accenture
    *  @ name       doBeforeDeleteHandle
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doBeforeDeleteHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doBeforeDeleteHandle');

    }
    //#L4-45526 End
    
    /**  
    *  @Author      Accenture
    *  @ name       doAfterUpdateHandle
    *  @param       Account,Account
    *  @Return      Null
    */
    public virtual void doAfterUpdateHandle(Account newAccount, Account oldAccount){
        EP_GeneralUtility.Log('Public','EP_AccountType','doAfterUpdateHandle');

    }
    /**  
    *  @Author      Accenture
    *  @ name       getSupplyOptions
    *  @param       Account
    *  @Return      List<EP_Stock_Holding_Location__c>
    */
    public virtual List<EP_Stock_Holding_Location__c> getSupplyOptions(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getSupplyOptions');
        return null;
    }
    /**  
    *  @Author      Accenture
    *  @ name       getPriceBook
    *  @param       Account
    *  @Return      Pricebook2
    */
    public virtual Pricebook2 getPriceBook(Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','getPriceBook');
        return new PriceBook2(); 
    }
    //not used
    /**  
    *  @Author      Accenture
    *  @ name       getPriceBookId
    *  @param       Id
    *  @Return      Id
    */
    public virtual Id getPriceBookId(Id accountId){
        EP_GeneralUtility.Log('Public','EP_AccountType','getPriceBookId');
        return null;
    }
    /**  
    *  @Author      Accenture
    *  @ name       setCompanyFromParent
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setCompanyFromParent (Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','setCompanyFromParent');

    }
    
    /*****Defect Fix- 43911 START********/
    /**  
    *  @Author      Accenture
    *  @ name       setOwnerFromParent
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setOwnerFromParent (Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','setOwnerFromParent');
    }
    /*****Defect Fix- 43911 END********/ 
    /**  
    *  @Author      Accenture
    *  @ name       doActiveAccount
    *  @param       Account
    *  @Return      Null
    */
    public virtual void doActiveAccount(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doActiveAccount');

    }
    /**  
    *  @Author      Accenture
    *  @ name       isShipToCSCReviewCompleted
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean isShipToCSCReviewCompleted(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isShipToCSCReviewCompleted');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasCompletedReviewActions
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean hasCompletedReviewActions(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasCompletedReviewActions');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasCompletedReviewActionsOnTanks
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean hasCompletedReviewActionsOnTanks(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasCompletedReviewActionsOnTanks');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isTankCSCReviewCompleted
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean isTankCSCReviewCompleted(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isTankCSCReviewCompleted');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isPrimarySupplyOptionAttached
    *  @param       Account
    *  @Return      Boolean
    */
    public virtual boolean isPrimarySupplyOptionAttached(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','isPrimarySupplyOptionAttached');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       insertPlaceHolderTankDips
    *  @param       Account
    *  @Return      Null
    */
    public virtual void insertPlaceHolderTankDips(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','insertPlaceHolderTankDips');

    }
    /**  
    *  @Author      Accenture
    *  @ name       deletePlaceHolderTankDips
    *  @param       Account
    *  @Return      Null
    */
    public virtual void deletePlaceHolderTankDips(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','deletePlaceHolderTankDips');

    }
    /**  
    *  @Author      Accenture
    *  @ name       hasOpenOrders
    *  @param       Account
    *  @Return      boolean 
    */
    public virtual boolean hasOpenOrders(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasOpenOrders');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasOpenOrderItems
    *  @param       Account
    *  @Return      boolean 
    */
    public virtual boolean hasOpenOrderItems(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','hasOpenOrderItems');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       getSellTo
    *  @param       Account
    *  @Return      Account
    */
    public virtual Account getSellTo(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getSellTo');
        return null;
    }
    /**  
    *  @Author      Accenture
    *  @ name       hasBillTo
    *  @param       Account
    *  @Return      Boolean 
    */
    public virtual Boolean hasBillTo(Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','hasBillTo');
        return false;
    }
    
    /*public virtual void doActionInsertManualSharingRecords(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doActionInsertManualSharingRecords');
        EP_AccountShareUtility.insertManualSharingRecords(account.Id);      
    }*/
    /**  
    *  @Author      Accenture
    *  @ name       isShipToActive
    *  @param       Account
    *  @Return      Boolean 
    */
    public virtual boolean isShipToActive(Account account){  
        EP_GeneralUtility.Log('Public','EP_AccountType','isShipToActive');
        return true;
    }
    /**  
    *  @Author      Accenture
    *  @ name       setSentNAVWINDMFlag
    *  @param       Account
    *  @Return      Null
    */
    public virtual void setSentNAVWINDMFlag(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setSentNAVWINDMFlag');         
    }
    /**  
    *  @Author      Accenture
    *  @ name       getCustomerPONumber
    *  @param       Account
    *  @Return      string
    */
    public virtual string getCustomerPONumber(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getCustomerPONumber'); 
        string customerPoNumber = '';
        return customerPoNumber;    
    }   
    /**  
    *  @Author      Accenture
    *  @ name       getPricebookId
    *  @param       Account
    *  @Return      Id
    */
    public virtual Id getPricebookId(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getPricebookId');
        return null;
    }
    /**  
    *  @Author      Accenture
    *  @ name       getPricebookEntry
    *  @param       Account
    *  @Return      Map<string,PricebookEntry>
    */
    public virtual Map<string,PricebookEntry> getPricebookEntry(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','getPricebookEntry');
        return new Map<string,PricebookEntry>();
    }
    /**  
    *  @Author      Accenture
    *  @ name       getPricebookEntry
    *  @param       Account,string
    *  @Return      Map<string,PricebookEntry>
    */
    public virtual Map<string,PricebookEntry> getPricebookEntry(Account account,string key){
        EP_GeneralUtility.Log('Public','EP_AccountType','getPricebookEntry');
        return new Map<string,PricebookEntry>();
    }
    
    /*public virtual void setCurrency(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','setCurrency');
    }*/
    /**DEFECT 43409 START**/
    public virtual boolean isCurrencySameOnProducts(Account account){
         EP_GeneralUtility.Log('Public','EP_AccountType','isCurrencySameOnProducts');
         return false;
    }
    /**DEFECT 43409 END**/
    
     // #routeSprintWork starts
    /**
    * @author       Accenture
    * @name         isRouteAllocationAvailable
    * @date         08/05/2017
    * @description  This method returns true if there is a record of ship to and selected default route
    * @param        NA
    * @return       Boolean
    */
    public virtual boolean isRouteAllocationAvailable(Account acc){ // #routeSprintWork
         EP_GeneralUtility.Log('Public','EP_AccountType','isRouteAllocationAvailable');
         return true;
    }
    // #routeSprintWork ends
    
    //L4_45352_Start
    /**
    * @author       Accenture
    * @name         isPickupEnabledSupplyOptionAttached
    * @date         11/14/2017
    * @description  This method returns true if pickup enabled supply option is attached on sell To
    * @param        NA
    * @return       Boolean
    */
    public virtual Boolean isPickupEnabledSupplyOptionAttached(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','isPickupEnabledSupplyOptionAttached'); 
        return true;
    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerCreateRequestToNAV
    *  @param       Account
    *  @Return      Null
    */
    public virtual void sendCustomerCreateRequestToNAV(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerCreateRequestToNAV'); 
    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerCreateRequestToWinDMS
    *  @param       Account
    *  @Return      Null
    */
    public virtual void sendCustomerCreateRequestToWinDMS(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerCreateRequestToWinDMS'); 
    }
    /**  
    *  @Author      Accenture
    *  @ name       isRecommendedCreditLimitRequired
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isRecommendedCreditLimitRequired(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','isRecommendedCreditLimitRequired');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isSellToSynced
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isSellToSynced(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','isSellToSynced');
        return false;
    }
    /**  
    *  @Author      Accenture
    *  @ name       isSellToActive
    *  @param       Account
    *  @Return      boolean
    */
    public virtual boolean isSellToActive(Account acc){
            EP_GeneralUtility.Log('Public','EP_AccountType','isSellToActive');
        return false;    
    }
    //L4_45352_END
    /**  
    *  @Author      Accenture
    *  @ name       updateConsignmentLocationReviewStatus
    *  @param       Account
    *  @Return      Null
    */
    public virtual void updateConsignmentLocationReviewStatus(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','updateConsignmentLocationReviewStatus');
    }

    // Changes made for CUSTOMER MODIFICATION L4 Start
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerEditRequestToNAV
    *  @param       Account
    *  @Return      Null
    */
    public virtual void sendCustomerEditRequestToNAV(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerCreateRequestToNAV'); 
    }
    /**  
    *  @Author      Accenture
    *  @ name       sendCustomerEditRequestToWinDMS
    *  @param       Account
    *  @Return      Null
    */ 
    public virtual void sendCustomerEditRequestToWinDMS(Account acc){
        EP_GeneralUtility.Log('Public','EP_AccountType','sendCustomerCreateRequestToWinDMS'); 
    }
    // Changes made for CUSTOMER MODIFICATION L4 End
    
    //L4#67333 Code Changes Start
     /**  
    *  @Author      Accenture
    *  @ name       hasHoldingMaster
    *  @param       Account
    *  @Return      Boolean 
    */
    public virtual Boolean hasHoldingMaster(Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','hasHoldingMaster');
        return false;
    }
    
    /*  @Author      Accenture
    *  @ name       setFinanceInfo
    *  @param       Account
    *  @Return      Boolean 
    */
    public virtual void setFinanceInfo(Account account) {
        EP_GeneralUtility.Log('Public','EP_AccountType','setFinanceInfo');
        
    }
    
    //L4#67333 Code Changes End
}