public class EP_AccountTypeStateMachineFactory {

    @TestVisible static EP_AccountDomainObject accountDomain;
    @TestVisible static EP_AccountEvent accountEvent;

    @TestVisible static final String PUMA_SYNTAX = 'EP_';
    @TestVisible static final String STATEMACHINE = 'SM';

    public EP_AccountTypeStateMachineFactory(EP_AccountDomainObject currentAccount){
        accountDomain = currentAccount;
    }
    
    public EP_AccountTypeStateMachineFactory(){

    }

    public static EP_AccountStateMachine getAccountStateMachine(){
        EP_GeneralUtility.Log('Public','EP_AccountTypeStateMachineFactory','getAccountStateMachine');
        return getAccountStateMachineInstance(accountDomain);
    }

    public static EP_AccountStateMachine getAccountStateMachine(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_AccountTypeStateMachineFactory','getAccountStateMachine');
        return getAccountStateMachineInstance(currentAccount);
    }


    public static EP_AccountStateMachine getAccountStateMachineInstance(EP_AccountDomainObject currentAccount){
        accountDomain = currentAccount;
        string stateMachineType = getStateMachineType();
        EP_AccountStateMachine ast;
        try{
            Type t = Type.forName(stateMachineType);
            ast = (EP_AccountStateMachine)t.newInstance();
            ast.setAccountDomainObject(accountDomain);            
        }catch(Exception e){
            throw new AccountStateMachineException('Account State Machine not available for ' +stateMachineType);
        }        
        return ast; 
    }
    
    
    @TestVisible 
    private static string getStateMachineType(){
        EP_GeneralUtility.Log('Private','EP_AccountTypeStateMachineFactory','getStateMachineType');
        String stateMachineType;
        string accountType = accountDomain.localAccount.EP_Account_Type__c;
		if(EP_AccountConstant.SHIP_TO.equalsIgnoreCase(accountType)){      
            stateMachineType  = String.format('{0}{1}{2}',new String[]{PUMA_SYNTAX,accountDomain.localAccount.EP_Entity_Type__c,STATEMACHINE});
        }else{
        	stateMachineType  = String.format('{0}{1}{2}',new String[]{PUMA_SYNTAX,accountType,STATEMACHINE});
        }
        return stateMachineType; 
    }
}