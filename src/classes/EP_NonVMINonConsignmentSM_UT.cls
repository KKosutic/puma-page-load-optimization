@isTest
public class EP_NonVMINonConsignmentSM_UT
{
static final string EVENT_NAME = 'Planned to cancelled';
static testMethod void getOrderState_test() {
	//Get Custom setting data from static resource
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
	csord__Order__c consumptionOrder = EP_TestDataUtility.getConsignmentOrderPlannedStatusPositiveScenario();
	EP_OrderDomainObject orderDomainObj = new EP_OrderDomainObject(consumptionOrder);
	EP_NonVMINonConsignmentSM localObj = new EP_NonVMINonConsignmentSM();
	localObj.setOrderDomainObject(orderDomainObj);
	EP_OrderEvent currentEvent = new EP_OrderEvent(EVENT_NAME);
	Test.startTest();
	EP_OrderState result = localObj.getOrderState(currentEvent);
	Test.stopTest();
	System.AssertEquals(result.order.getOrder().id, consumptionOrder.id);
}
}