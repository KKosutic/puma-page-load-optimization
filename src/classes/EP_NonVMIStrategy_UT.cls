@isTest
public class EP_NonVMIStrategy_UT
{

    static testMethod void doUpdatePaymentTermAndMethod_test() {
        EP_NonVMIStrategy localObj = new EP_NonVMIStrategy();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        Test.startTest();
        localObj.doUpdatePaymentTermAndMethod(orderObj);
        Test.stopTest();
        Account orderAccountObj = [SELECT Id ,EP_Email__c, BillingCountry,BillingCity,EP_Billing_Basis__c,EP_Price_Consolidation_Basis__c FROM Account WHERE Id=:orderObj.AccountId__c];
        System.AssertEquals(orderObj.EP_Email__c ,orderAccountObj.EP_Email__c);
        //System.AssertEquals(orderObj.BillingCountry__c, orderAccountObj.BillingCountry__c);
    }
    
   
    static testMethod void findRecordType_test() {
        EP_NonVMIStrategy localObj = new EP_NonVMIStrategy();
        Test.startTest();
        Id devRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        Id result = localObj.findRecordType();
        Test.stopTest();
        System.AssertEquals(devRecordTypeId,result);
    }
}