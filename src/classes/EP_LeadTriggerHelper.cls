/**
 * @author <Ashok Arora>
 * @name <EP_LeadTriggerHelper>
 * @createDate <10/11/2015>
 * @description <This class handles requests from LeadTriggerhandler>
 * @Version <1.0>
 */
public with sharing class EP_LeadTriggerHelper {
	 
	private static final String MAP_PAYMENT_METHOD_AND_TERM = 'mapPaymentMethodAndTerm';
	private static final String UPDATE_ACCEPTED_RECORD_TYPE = 'updateAcceptedRecordType';
	private static final String CREATE_CASE = 'createCase';
	private static final String POUPLATE_LEAD_DATA = 'populateLeadData';
	private static final String UPDATE_CASE_PRODUCT_INTERESTED_IN = 'updateCaseProductInterestedIn';
	private static final String VALIDATE_LEAD_ADDRESS = 'validateLeadAddress';
	
	/**
	 * @author <Ashok Arora>
	 * @description <Create case Record>
	 * @name <createCase>
	 * @date <10/11/2015>
	 * @param List<Lead>
	 * @return void
	 */
	public static void createCase(List<Lead> listOfLeads){
		List<AssignmentRule> listOfAssignmentRule;
		Database.DMLOptions dmlOpts;
		List<Lead> listOfLeadsWOCase = new List<Lead>();
		try{
			List<Case >listOfCase = new List<Case>();
			RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.CASE_OBJ_REC
							,EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
			RecordType biRecordType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.LEAD_OBJ,Label.EP_CRM_LEAD_RECTYPE_BI);
			RecordType biOnHoldRecordType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.LEAD_OBJ,Label.EP_CRM_LEAD_RECTYPE_BI_ONHOLD);
			for(Lead lead : listOfLeads){
				/**
				 * @Author <Kamendra Singh
				 * @Description <add filter on Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).
				 * @CreateDate <08/06/2016>
				 */
				if(String.isBlank(lead.Case__c) && lead.RecordTypeId != biRecordType.Id && lead.RecordTypeId != biOnHoldRecordType.Id){
					listOfCase.add(initialiseCaseValues(lead,recType.id));
					 listOfLeadsWOCase.add(lead);
				}
			}
			listOfAssignmentRule = EP_Common_Util.getAssignmentRules(EP_Common_Constant.CASE_OBJ); 
			//Creating the DMLOptions for "Assign using active assignment rules" checkbox
			dmlOpts = new Database.DMLOptions();
			if (listOfAssignmentRule != null && !listOfAssignmentRule.isEmpty()){
				dmlOpts.assignmentRuleHeader.assignmentRuleId= listOfAssignmentRule[0].id;
			}
			dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
			DataBase.insert(listOfCase,dmlOpts);
			for(Integer i = 0; i <  listOfLeadsWOCase.size(); i++ ){
				 listOfLeadsWOCase[i].Case__c = listOfCase[i].id;
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, CREATE_CASE, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <Case initialization values>
	 * @name <initialiseCaseValues>
	 * @date <10/11/2015>
	 * @param Lead, Id
	 * @return Case
	 */
	private static Case initialiseCaseValues(Lead lead, Id recordTypeId){
		return new Case(Origin = lead.LeadSource//EP_Common_Constant.CASE_ORIGIN_WEB
		,Status = EP_Common_Constant.CASE_STATUS_NEW_APPLICATION
		,SuppliedEmail = lead.Email
		,Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM
		,RecordTypeId = recordTypeId
		,EP_Sys_Products_Interested_In__c = lead.EP_Products_Interested_In__c
		,EP_Country_Code__c = lead.EP_Dlvry_Cntry_Code__c
		,EP_Region_Name__c = lead.EP_Region_Name__c);
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method updates lead record type once status changes to accepted>
	 * @name <updateAcceptedRecordType>
	 * @date <10/11/2015>
	 * @param List<Lead>
	 * @return void
	 */
	public static void updateAcceptedRecordType(List<Lead> listOfLeads){
		Id leadNewRecordTypeId;
		Id leadAcceptedRecordTypeId;
		try{
			leadNewRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.LEAD_OBJ, EP_Common_Constant.LEAD_NEW_TYPE);
			leadAcceptedRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.LEAD_OBJ, EP_Common_Constant.LEAD_ACCEPTED_TYPE);
			//IF LEAD STATUS IS ACCEPTED AND RECORDTYPE IS NEW THEN UPDATE RECORD TYPE TO ACCEPTED
			for(Lead lead : listOfLeads){
				if(EP_Common_Constant.LEAD_STATUS_ACCEPTED.equalsIgnoreCase(lead.Status) && lead.RecordTypeId == leadNewRecordTypeId){
					lead.RecordTypeId = leadAcceptedRecordTypeId;//UPDATE RECORD TYPE TO ACCEPTED
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, UPDATE_ACCEPTED_RECORD_TYPE, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method maps requested payment method and payment term on connverted Account from lead>
	 * @name <mapPaymentMethodAndTerm>
	 * @date <10/11/2015>
	 * @param Map<Id,Lead>
	 * @return void
	 */
	public static void mapPaymentMethodAndTerm(Map<Id,Lead> mapOfNewLeadIdWithLeads){
		MAP<Id,Id> mapOfConvertedLeadIdwithLeadIDs = new Map<Id,Id>();
		Set<String> setOfPaymentMethodNames = new Set<String>();
		List<Account> listOfAccount = new List<Account>();
		Map<String,Id> mapOfPaymentMethodNameWithPaymentMethodIDs = new Map<String,Id>();
		Integer noOfRows;
		EP_Payment_Term__c paymentTerm;
		try{
			//GET THE CONVERTED ACCOUNTS IF LEAD IS CONVERTED
			for(Lead lead : mapOfNewLeadIdWithLeads.values()){
				/**
				 * @Author <Kamendra Singh
				 * @Description <add filter on Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).
				 * @CreateDate <08/06/2016>
				 */
				if(lead.isConverted && String.isNotBlank(lead.ConvertedAccountId)){
					mapOfConvertedLeadIdwithLeadIDs.put(lead.ConvertedAccountId,lead.id);
					setOfPaymentMethodNames.add(lead.EP_Requested_Payment_Method__c);
				}
			}
			noOfRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
			//GET RELATED PAYMENT METHODS
			if(!setOfPaymentMethodNames.isEmpty()){
				for(EP_Payment_Method__c paymentMethod: [Select Name, Id From EP_Payment_Method__c Where Name IN:setOfPaymentMethodNames Limit :noOfRows]){
					mapOfPaymentMethodNameWithPaymentMethodIDs.put(paymentMethod.Name,paymentMethod.Id);
				}
			}
			//UPDATE PAYMENT METHOD AND PAYMENT TERM ON CONVERTED ACCOUNTS
			if(!mapOfConvertedLeadIdwithLeadIDs.isEmpty()){
				paymentTerm = [select id from EP_Payment_Term__c where Name =:System.label.EP_PrePayment limit :EP_Common_Constant.ONE];
				noOfRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
				for(Account accountObject : [Select Id, EP_Requested_Payment_Method__c,EP_Requested_Payment_Terms__c, EP_Package_Payment_Term__c
				,currencyISOCode From Account Where Id IN : mapOfConvertedLeadIdwithLeadIDs.keyset() limit :noOfRows]){
					//MAP PRE-PAYMENT TERM
					accountObject.EP_Payment_Term_Lookup__c = paymentTerm.Id;
					accountObject.EP_Package_Payment_Term__c = paymentTerm.Id;
					//MAP PAYMENT METHOD
					if(mapOfPaymentMethodNameWithPaymentMethodIDs.containsKey(accountObject.EP_Requested_Payment_Method__c)){
						accountObject.EP_Payment_Method__c = mapOfPaymentMethodNameWithPaymentMethodIDs.get(accountObject.EP_Requested_Payment_Method__c);
					}
					//MAP DELIVERY\PICKUP COUNTRY
					accountObject.EP_Delivery_Pickup_Country__c = mapOfNewLeadIdWithLeads.get(mapOfConvertedLeadIdwithLeadIDs.get(accountObject.Id)).EP_Delivery_Country__c;
					//MAP REGION
					accountObject.EP_Cntry_Region__c= mapOfNewLeadIdWithLeads.get(mapOfConvertedLeadIdwithLeadIDs.get(accountObject.Id)).EP_Region__c;
					//ADD ACCOUNTS TO UPDATE
					listOfAccount.add(accountObject);
				}
				if(!listOfAccount.isEmpty()){
					update listOfAccount;
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, MAP_PAYMENT_METHOD_AND_TERM, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
		
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <To Set Lead data from country object>
	 * @name <populateLeadData>
	 * @date <10/11/2015>
	 * @param List<Lead>
	 * @return void
	 */
	public static void populateLeadData(List<Lead> listOfLeads){
		try{
			Integer noOfRows;
			Set<Id> countryIds = new Set<Id>();
			for(Lead le : listOfLeads){
				if(le.EP_CountryLookUp__c != null){
					countryIds.add(le.EP_CountryLookUp__c);
				}
			}
			noOfRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
			Map<Id, EP_Country__c> countryMap = new Map<Id, EP_Country__c>([SELECT id,CurrencyIsoCode, Name From EP_Country__c WHERE Id IN : countryIds limit :noOfRows]);
			for(Lead le : listOfLeads){
				if(le.EP_CountryLookUp__c != null && countryMap.containsKey(le.EP_CountryLookUp__c)){
					le.CurrencyIsoCode = countryMap.get(le.EP_CountryLookUp__c).CurrencyIsoCode;
					le.Country = countryMap.get(le.EP_CountryLookUp__c).Name;
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, POUPLATE_LEAD_DATA, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method is updating case's product interested in? field if we are changing Product interasted in? field on lead>
	 * @name <updateCaseProductInterestedIn>
	 * @date <10/11/2015>
	 * @param Map<Id, Lead>, Map<Id, Lead>
	 * @return void
	 */
	public static void updateCaseProductInterestedIn(Map<Id,Lead> mapOfOldLeadIdWithLeads, Map<Id,Lead>mapOfNewLeadIdWithLeads){
		List<Case>listOfCase = new List<Case>();
		Case caseRecord;
		try{
			for(Id lead : mapOfNewLeadIdWithLeads.keySet()){
				if((mapOfNewLeadIdWithLeads.get(lead).EP_Products_Interested_In__c
							!= mapOfOldLeadIdWithLeads.get(lead).EP_Products_Interested_In__c
							|| mapOfNewLeadIdWithLeads.get(lead).EP_Region_Name__c
							!= mapOfOldLeadIdWithLeads.get(lead).EP_Region_Name__c
							|| mapOfNewLeadIdWithLeads.get(lead).EP_Dlvry_Cntry_Code__c
							!= mapOfOldLeadIdWithLeads.get(lead).EP_Dlvry_Cntry_Code__c  
							)
						&& String.isNotBlank(mapOfNewLeadIdWithLeads.get(lead).case__c)){
					caseRecord = (Case)Case.SObjectType.newSObject(mapOfNewLeadIdWithLeads.get(lead).case__c);
					caseRecord.EP_Sys_Products_Interested_In__c = mapOfNewLeadIdWithLeads.get(lead).EP_Products_Interested_In__c;
					caseRecord.EP_Region_Name__c = mapOfNewLeadIdWithLeads.get(lead).EP_Region_Name__c;
					caseRecord.EP_Country_Code__c = mapOfNewLeadIdWithLeads.get(lead).EP_Dlvry_Cntry_Code__c;
					listOfCase.add(caseRecord);
				}
			}
			if(!listOfCase.isEmpty()){
				Database.update(listOfCase);
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, UPDATE_CASE_PRODUCT_INTERESTED_IN, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method is used to update 'Valid Address' field on Lead object>
	 * @name <validateLeadAddress>
	 * @date <10/11/2015>
	 * @param Map<Id, Lead>, Map<Id, Lead>
	 * @return void
	 */
	public static void validateLeadAddress(Map<Id,Lead> mapOfOldLeadIdWithLeads ,Map<Id,Lead> mapOfNewLeadIdWithLeads){
		try{
			for(Id accountId : mapOfNewLeadIdWithLeads.keySet()){
				if(mapOfNewLeadIdWithLeads.get(accountId).Street != mapOfOldLeadIdWithLeads.get(accountId).Street
					|| mapOfNewLeadIdWithLeads.get(accountId).City != mapOfOldLeadIdWithLeads.get(accountId).City
					|| mapOfNewLeadIdWithLeads.get(accountId).State != mapOfOldLeadIdWithLeads.get(accountId).State
					|| mapOfNewLeadIdWithLeads.get(accountId).Country != mapOfOldLeadIdWithLeads.get(accountId).Country
					|| mapOfNewLeadIdWithLeads.get(accountId).PostalCode != mapOfOldLeadIdWithLeads.get(accountId).PostalCode){
					mapOfNewLeadIdWithLeads.get(accountId).EP_Is_Address_Valid__c = EP_Common_Util.isValidAddress;
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, VALIDATE_LEAD_ADDRESS, EP_LeadTriggerHelper.Class.getName(), ApexPages.Severity.ERROR);
		}
	}

}