/* 
	@Author <Vikas Malik>
	@name <EP_CreateCustomerEmailHandler>
	@CreateDate <06/10/2015>
	@Description <This class will handle the incoming email sent by customer and create a Case and Lead record>
	@Version <1.0> 
*/
global without sharing class EP_CreateCustomerEmailHandler implements Messaging.InboundEmailHandler{
	private static final string CLASSNAME = 'EP_CreateCustomerEmailHandler'; 
	private static final string HANDLE_INBOUNDEMAIL_MTD = 'handleInboundEmail';
	
	/**
	* @author <Vikas Malik>
	* @name <handleInboundEmail>
	* @date <06/10/2015>
	* @description <This method will handle the incoming email and result in generating of the Case and Lead Record>
	* @version <1.0>
	* @param Messaging.InboundEmail, Messaging.InboundEnvelope
	* @return Messaging.InboundEmailResult
	*/
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		Savepoint sp = Database.setSavepoint();
		try{
			Case caseObject = new Case();
			caseObject.Origin = EP_Common_Constant.CASE_ORIGIN_EMAIL;
			caseObject.Status = EP_Common_Constant.CASE_STATUS_NEW_APPLICATION;
			caseObject.Subject = email.Subject;
			caseObject.SuppliedEmail  = email.fromAddress;
			caseObject.SuppliedName = email.fromName;
			caseObject.Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM;
			caseObject.Description = email.plainTextBody;
			RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName
			(EP_Common_Constant.CASE_OBJ_REC,EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
			if (recType != null){
				caseObject.RecordTypeId = recType.Id;
			}
			
			List<AssignmentRule> listOfAssignmentRules = new List<AssignmentRule>();
			listOfAssignmentRules = EP_Common_Util.getAssignmentRules(EP_Common_Constant.CASE_OBJ);
			//Creating the DMLOptions for "Assign using active assignment rules" checkbox
			Database.DMLOptions dmlOpts = new Database.DMLOptions();
			if (listOfAssignmentRules != null && !listOfAssignmentRules.isEmpty()){
				dmlOpts.assignmentRuleHeader.assignmentRuleId= listOfAssignmentRules[0].id;
			}
			dmlOpts.EmailHeader.triggerAutoResponseEmail = true;                                
			caseObject.setOptions(dmlOpts);
			Database.insert(caseObject);
			
			Lead leadObject = new Lead();
			//system.debug('emailFromName++++'+email.fromName);
			//leadObject.FirstName = email.
			leadObject.LastName = email.fromName;
			leadObject.Email = email.fromAddress;
			leadObject.Company = email.fromName;            
			leadObject.Case__c = caseObject.Id;
			leadObject.LeadSource = EP_Common_Constant.LEAD_ORIGIN_EMAIL;
			
			listOfAssignmentRules = EP_Common_Util.getAssignmentRules(EP_Common_Constant.LEAD_OBJ);
			//Creating the DMLOptions for "Assign using active assignment rules" checkbox
			dmlOpts = new Database.DMLOptions();
			if (listOfAssignmentRules != null && !listOfAssignmentRules.isEmpty()){
				dmlOpts.assignmentRuleHeader.assignmentRuleId= listOfAssignmentRules[0].id;
			}
			leadObject.setOptions(dmlOpts);
			Database.insert(leadObject);            
			result.success = true;
		}catch (Exception handledException){
			EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA , HANDLE_INBOUNDEMAIL_MTD, CLASSNAME, ApexPages.Severity.ERROR);
			result.success = false;
			result.Message = Label.EP_Incoming_Email_Error_Message;
			Database.rollBack(sp);          
		}
		return result;
	}
}