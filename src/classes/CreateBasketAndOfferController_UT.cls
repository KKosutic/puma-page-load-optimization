@isTest(SeeAllData=true)
private class CreateBasketAndOfferController_UT
{
	@isTest
	static void createBasket_test() {

		//Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		Account acc = createAccount();
        //insert acc;

		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		CreateBasketAndOfferController controller = new CreateBasketAndOfferController(sc);

		controller.createBasket();

		String basketName = 'Order for ' + acc.Name;
		cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c from cscfga__Product_Basket__c where name= :basketName];

		System.assertEquals(basketName, createdBasket.name);
		System.assertEquals(acc.id, createdBasket.csbb__Account__c);

		CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
		String iFrame = '<iframe id="mle-iframe" src="'+csOrderSetting.MLE_Org_URL__c+'?batchSize='+csOrderSetting.MLE_BatchSize__c+'&id=' + createdBasket.id + '&productDefinitionId=' 
		+csOrderSetting.OrderLineItemProductDefinitionLineId__c +'&showHeader='+csOrderSetting.MLE_ShowHeader__c+'&sideBar='+csOrderSetting.MLE_SideBar_Selected__c+'&embedded='
		+csOrderSetting.MLE_Embedded__c+'&cssoverride='+csOrderSetting.CSS_Resource_Path__c+'&scriptplugin='+csOrderSetting.Javascript_Resource_Path__c
        +'" width="'+csOrderSetting.MLE_IFRAME_WIDTH__c+'" height="'+csOrderSetting.MLE_HEIGHT__c+'" frameBorder="'+csOrderSetting.MLE_FrameBorder__c+'"></iframe>';

		// verify connectConfigurations() is executed properly
 		//System.assertEquals(createdBasket.id, controller.attribute1.cscfga__Value__c);
		//System.assertEquals(iFrame, controller.attribute7.cscfga__Value__c);
	}
	
	private static Account createAccount(){
	    Account acc = new Account();
        
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccountyy';
        acc.EP_Status__c = '06-Blocked';
       
       
        /*acc.BillingCity = 'testEPAccounttestCity';
        acc.BillingStreet = 'testEPAccounttestStreet';
        acc.BillingState = 'testEPAccounttestState';
        acc.BillingCountry = 'testEPAccounttestCountry';
        acc.BiLlingPostalCode = '89123456';
        acc.ShippingCity = 'testEPAccountShipCity';
        acc.ShippingStreet = 'testEPAccountShipStreet';
        acc.ShippingState = 'testEPAccountShipState';
        acc.EP_Eligible_for_Rebate__c =True;
        acc.ShippingCountry = 'testEPAccountShipCountry';
        acc.ShippingPostalCode = '78123465';  */                   
        insert acc;
        return acc;
	}    

	@isTest
	static void editBasket_test() {
    
		//Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
        //insert acc;
        Account acc = createAccount();

		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		CreateBasketAndOfferController controller = new CreateBasketAndOfferController(sc);

		controller.editBasket();

		String basketName = 'Order for ' + acc.Name;
		cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c from cscfga__Product_Basket__c where name= :basketName];

		System.assertEquals(basketName, createdBasket.name);
		System.assertEquals(acc.id, createdBasket.csbb__Account__c);

	}
}