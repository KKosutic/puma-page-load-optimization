@isTest
public class EP_StorageLocationASRejected_UT
{

    static final string EVENT_NAME = '08-RejectedTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
static testMethod void setAccountDomainObject_test() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.setAccountDomainObject(obj);
    Test.stopTest();     
    //assert not needed
    system.assert(true);
    
}
static testMethod void doOnEntry_test() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.doOnEntry();
    Test.stopTest();
    //assert not needed
    system.assert(true);
}
static testMethod void doOnExit_test() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.doOnExit();
    Test.stopTest();
    //assert not needed
    system.assert(true);
}
static testMethod void doTransition_PositiveScenariotest() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObjectPositiveScenario();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    Boolean result = localObj.doTransition();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void doTransition_NegativeScenariotest() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObjectNegativeScenario();
    EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    List<Exception> excList = new List<Exception>();
    Test.startTest();
    try{
        Boolean result = localObj.doTransition();
        }
    catch(Exception e){
        excList.add(e);
    }
    Test.stopTest();
    System.Assert(excList.size()>0);
}
static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
    EP_StorageLocationASRejected localObj = new EP_StorageLocationASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASRejectedDomainObjectPositiveScenario();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    Boolean result = localObj.isInboundTransitionPossible();
    Test.stopTest();
    System.AssertEquals(true,result);
}

}