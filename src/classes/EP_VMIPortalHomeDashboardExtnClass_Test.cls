///* 
//    @Author <Abhay Arora>
//    @name <EP_VMIPortalHomeDashboardExtensionClass>
//    @CreateDate <16/10/2015>
//    @Description <This class is used by the users to select the site that they want to submit a tank dip for>
//    @Version <1.0>
// 
//*/
//@isTest
public class EP_VMIPortalHomeDashboardExtnClass_Test {
//    
//    private static List<Account> accList; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    
//    /**************************************************************************
//    *@Description : This method is used to create Data.                       *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init() {
//        
//        Product2 productObj = EP_TestDataUtility.createTestRecordsForProduct();
//        accList = EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//        TankInstance = EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//        TankInstance.EP_Product__c = productObj.Id;
//        Database.update(TankInstance);
//        tankDipList = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id)});
//    
//    }
//    
//    /**************************************************************************
//    *@Description : This method is used to .                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void Inforet() { 
//        init();
//        
//        // Add placeholder tank dips
//        // Create test tank dips
//        DateTime dtNow = System.Now();
//        DateTime dtPlaceholderReadingDateTime = EP_PortalLibClass.returnLocationDateTimeFromInteger(dtNow.Year(), dtNow.Month(), dtNow.Day(), dtNow.Hour(), dtNow.Minute(), 0);
//        
//        accList.get(0).EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
//        accList.get(0).EP_Ship_To_UTC_Timezone__c = 'UTC+00:00';
//        update accList;
//        
//        List<EP_Tank_Dip__c> testTankDips = new List<EP_Tank_Dip__c>();
//        testTankDips.add(new EP_Tank_Dip__c(EP_Tank__c = TankInstance.Id, EP_Is_Placeholder_Tank_Dip__c = TRUE, 
//                                                                        EP_Reading_Date_Time__c = dtPlaceholderReadingDateTime));
//        testTankDips = EP_TestDataUtility.createTestEP_Tank_Dip(testTankDips);
//        
//        Test.startTest();
//        
//            PageReference pageRef = Page.EP_VMIPortalHomeDashboardPage;
//            Test.setCurrentPage(pageRef); //Applying page context here     
//            
//            EP_VMIPortalHomeDashboardExtensionClass ctrl = new EP_VMIPortalHomeDashboardExtensionClass();
//            
//            ctrl.selectedSiteID = accList.get(0).Id;
//            List<EP_VMIPortalHomeDashboardExtensionClass.SiteTableData> userSiteData = ctrl.userSiteData; 
//            List<SelectOption> userSiteOptions = ctrl.userSiteOptions;
//            ctrl.getBarData();
//            
//        Test.stopTest();
//    }
//    
}