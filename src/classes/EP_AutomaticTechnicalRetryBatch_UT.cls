/*
   @Author          Accenture
   @Name            EP_AutomaticTechnicalRetryBatch_UT - #59186
   @CreateDate      02/07/2017
   @Description     This Test class for EP_AutomaticTechnicalRetryBatch Class
   @Version         1.0
*/
@isTest
private class EP_AutomaticTechnicalRetryBatch_UT {

	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    	
    }

    
	@isTest
	private static void AutomaticTechnicalRetryBatchPositive_Test() {
		
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		EP_AutomaticTechnicalRetryBatch aTRBatch = new EP_AutomaticTechnicalRetryBatch();
		
		Test.startTest();
		Id jobId = Database.executeBatch(aTRBatch);
		Test.stopTest();
		
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,2.0);
		
	}
	

	@isTest
	private static void AutomaticTechnicalRetryBatchNegative_Test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,'SYNC'); 
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		EP_AutomaticTechnicalRetryBatch aTRBatch = new EP_AutomaticTechnicalRetryBatch();
		Test.startTest();
		Id jobId = Database.executeBatch(aTRBatch);
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,1.0);
	}

	
	@isTest
	private static void isATRDisabledPositive_Test() {
		Test.startTest();
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
		communicationSettings.Disable__c = true;
		update communicationSettings;
		Test.stopTest();
		System.assertEquals(batchClass.isATRDisabled(),true);
	}

	@isTest
	private static void isATRDisabledNegative_Test() {
		Test.startTest();
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		Test.stopTest();
		System.assertEquals(batchClass.isATRDisabled(),false);
	}
	
	@isTest
	private static void sendOutboundMessagePositive_Test() {
		
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);  
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
		EP_AutomaticTechnicalRetryBatch.sendOutboundMessage(intRecord);
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,2.0);
		
	}

	
	@isTest
	private static void sendOutboundMessageNegative_Test() {
		Test.startTest();   
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 	
		EP_AutomaticTechnicalRetryBatch.sendOutboundMessage(Null);
		list<EP_Exception_Log__c> lstLog = [SELECT Id FROM EP_Exception_Log__c ];
		Test.stopTest();
		System.assert(lstLog.size()>0);
	}

	@isTest
	private static void scheduleNextJobPositive_Test() {
		Test.startTest();
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
    	
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		batchClass.scheduleNextJob();
		Test.stopTest();
		System.assertEquals(EP_AutomaticTechnicalRetryBatch.isCronJobScheduled(EP_Common_Constant.ATR_SEARCH_STRING),true);
		List<CronTrigger> lstCron = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name Like: EP_Common_Constant.ATR_SEARCH_STRING];
		System.assertEquals(lstCron.size(),1);
		
	}

	@isTest
	private static void scheduleNextJobNegative_Test() {
		Test.startTest();
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		batchClass.scheduleNextJob();
		batchClass.scheduleNextJob();
		List<CronTrigger> lstCron = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name Like: EP_Common_Constant.ATR_SEARCH_STRING];
		Test.stopTest();
		System.assertEquals(lstCron.size(),1);		
	}
	
	@isTest
	private static void isCronJobScheduledNegative_Test() {
		Test.startTest();
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		EP_AutomaticTechnicalRetryBatch.deleteCronJobs(new Set<String>{EP_Common_Constant.COMPLETE_STRING,EP_Common_Constant.DELETED_STRING,EP_Common_Constant.WAITING_STRING});
		Test.stopTest();
		System.assertEquals(EP_AutomaticTechnicalRetryBatch.isCronJobScheduled(EP_Common_Constant.ATR_SEARCH_STRING),false);
	}

	@isTest
	private static void isCronJobScheduledPositive_Test() {
		Test.startTest();
		EP_AutomaticTechnicalRetryBatch batchClass = new EP_AutomaticTechnicalRetryBatch();
		EP_AutomaticTechnicalRetryBatch.deleteCronJobs(new Set<String>{EP_Common_Constant.COMPLETE_STRING,EP_Common_Constant.DELETED_STRING,EP_Common_Constant.WAITING_STRING});
		batchClass.scheduleNextJob();
		Test.stopTest();
		System.assertEquals(EP_AutomaticTechnicalRetryBatch.isCronJobScheduled(EP_Common_Constant.ATR_SEARCH_STRING),true);
	}
}