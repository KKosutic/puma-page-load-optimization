/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationSetupToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 04-Account Set-up to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationSetupToActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationSetupToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToActive',' isGuardCondition');        
        return true;
    }

    public override void doOnExit(){

    }
}