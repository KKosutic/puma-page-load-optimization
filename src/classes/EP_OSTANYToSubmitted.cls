/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToSubmitted>
*  @CreateDate <03/02/2017>
*  @Description <handles order status change from any status to submitted>
*  @Version <1.0>
*/

public class EP_OSTANYToSubmitted extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToSubmitted';
    public EP_OSTANYToSubmitted(){
        finalState = EP_OrderConstant.OrderState_Submitted;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToSubmitted','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToSubmitted','isRegisteredForEvent');
        return super.isRegisteredForEvent();      
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToSubmitted','isGuardCondition');
        return true;               
    }
        
}