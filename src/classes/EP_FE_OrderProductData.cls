/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_OrderProductData >
     @CreateDate <>
     @Description < Wrapper to hold all the order items pricing info >  
     @Version <1.0>
*********************************************************************************************/
global with sharing class EP_FE_OrderProductData {

    public List<EP_FE_OrderItem> orderItems;

    // Discount
    public Decimal discountPercentage;
    public Decimal discountAmount;

    // Total
    public Decimal subtotal;
    public Decimal totalAmount;
    public Decimal totalFreight;
    public Decimal totalTax;   
    /*
    *   Constructor
    */ 
    public EP_FE_OrderProductData(){
        this.orderItems = new List<EP_FE_OrderItem>();          
    }
    
    /*
    *   Order Item info
    */
    global with sharing class EP_FE_OrderItem {
        public Id productId {get; set;}
        public String productName {get; set;}
        public String productUnitOfMeasure {get; set;}
        public Decimal price {get; set;}
        public Decimal quantity {get; set;}
        public Decimal pricePerUnit {get; set;}
        public Decimal freightPrice {get; set;}
        public Decimal discount {get; set;}
        public Decimal totalPrice {get; set;}
        public List<EP_FE_TaxItem> taxItems;
        public Id tankId {get; set;}
        public DateTime LastModifiedDate {get; set;}
        
        /*
        *   Constructor
        */
        public EP_FE_OrderItem(){
            taxItems = new List<EP_FE_TaxItem>();
        }
        
        /*
        *   Constructor
        */
        public EP_FE_OrderItem(csord__Order_Line_Item__c orderLineItem) {
            this(orderLineItem, true);
        }   
            
        /*
        *   Constructor
        */        
        public EP_FE_OrderItem(csord__Order_Line_Item__c orderLineItem, Boolean pricesAreVisible) {
            taxItems = new List<EP_FE_TaxItem>();
            this.productId = orderLineItem.EP_Product__r.Id;
            this.productName = orderLineItem.EP_Product__r.Name;
            this.productUnitOfMeasure = orderLineItem.EP_Product__r.EP_Unit_of_Measure__c;
            this.quantity = orderLineItem.Quantity__c;
            this.tankId = orderLineItem.EP_Tank__c;
            this.LastModifiedDate = orderLineItem.LastModifiedDate;
            if (pricesAreVisible) {
                this.pricePerUnit = orderLineItem.ListPrice__c;
                this.price  = orderLineItem.EP_Total_Price__c;
            }
        }
    }

    /*
    *   Tax Item info
    */
    global with sharing class EP_FE_TaxItem {
        public String name;
        public Decimal amount;
        
        /*
        *   Constructor
        */
        public EP_FE_TaxItem(String name, Decimal amount) {
            this.name = name;
            this.amount = amount;
        }
    }

}