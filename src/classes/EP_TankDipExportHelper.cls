/* 
   @Author 			Accenture
   @name 			EP_TankDipExportHelper
   @CreateDate 		03/10/2017
   @Description		Transformer class for tank inbound interface used to transform the data to the wrapper instance
   @Version 		1.0
*/
public class EP_TankDipExportHelper {
    
    /**
	* @author 			Accenture
	* @name				transformTankDipMsg
	* @date 			03/13/2017
	* @description 		Method used to transform the tanks in database to the wrapper tanks.
	* @param 			List<EP_Tank_Dip__c>
	* @return 			EP_TankDipExportStub
	*/
    public static EP_TankDipExportStub setTankDipAttributes(List<EP_Tank_Dip__c> tankDipRecords){
        EP_GeneralUtility.Log('Public','EP_TankDipExportHelper','transformTankDipMsg');
        EP_TankDipExportStub tankDipMessage = new EP_TankDipExportStub();
        EP_TankDipExportStub.tankDip ltankDipLine= new EP_TankDipExportStub.tankDip();
        for(EP_Tank_Dip__c tankDip : tankDipRecords){
            ltankDipLine= new EP_TankDipExportStub.tankDip();
            ltankDipLine.seqId = tankDip.Id;
            //ltankDipLine.clientId = tankDip.EP_Tank__r.EP_Ship_To__r.EP_Account_Company_Name__c;
            ltankDipLine.clientId = tankDip.EP_Tank__r.EP_Ship_To__r.EP_Puma_Company_Code__c;
            ltankDipLine.identifier.custId = tankDip.EP_Tank__r.EP_Ship_To__r.Parent.AccountNumber;
            ltankDipLine.identifier.shiptoId = tankDip.EP_Tank__r.EP_Ship_To__r.AccountNumber; 
            ltankDipLine.identifier.tankNr= tankDip.EP_Tank_Nr__c;
            ltankDipLine.ItemId = String.isNotBlank(tankDip.EP_Tank__r.EP_Product__r.ProductCode) ? Integer.ValueOf(tankDip.EP_Tank__r.EP_Product__r.ProductCode) : null;
            ltankDipLine.qty = (tankDip.EP_Ambient_Quantity__c != NULL ? Integer.ValueOf(tankDip.EP_Ambient_Quantity__c): 0);
            ltankDipLine.uom= tankDip.EP_Unit_Of_Measure__c;
            //ltankDipLine.readingTimesStmpLcl= tankDip.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c != NULL ? EP_GeneralUtility.returnLocalDateTime(tankDip.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c) : null;
            ltankDipLine.readingTimesStmpLcl= tankDip.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c != NULL ? EP_DateTimeUtility.formatDateAsString(tankDip.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c) : null;
            /** TR DateTime 59862 Start **/ 
            //ltankDipLine.ModifyOn= EP_GeneralUtility.returnLocalDateTime(tankDip.LastModifiedDate);
            ltankDipLine.ModifyOn= EP_DateTimeUtility.formatDateAsString(tankDip.LastModifiedDate);
            /** TR DateTime 59862 End **/ 
            ltankDipLine.ModifyBy= (tankDip.LastModifiedBy.Alias.length() > 10) ? tankDip.LastModifiedBy.Alias.left(10): tankDip.LastModifiedBy.Alias;
            tankDipMessage.MSG.payload.any0.tankDips.tankDip.add(ltankDipLine);
        }
        return  tankDipMessage;
    }
}