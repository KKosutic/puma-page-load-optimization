@isTest
public with sharing class EP_Logging_Service_Test{
       
    public static TestMethod void runException(){
        
        EP_Exception_Log__c log = new EP_Exception_Log__c();
        log.EP_Exception_Details__c = 'System handling Error';
        log.EP_Handled__c = true; //API changed
        log.EP_Class_Name__c = 'TestClass';
        log.EP_Severity__c = 'ERROR';
        Test.StartTest();
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'threshold';
        setting.Value__c = 3;
        insert setting;  
        EP_LoggingService.logUnhandledException(log,'ERROR');      
        Test.StopTest(); 
    }
    
    public static TestMethod void checkserviceExcep(){
        //Changed by Sandeep
        User u = EP_TestDataUtility.SYSTEM_ADMIN_USER;
        insert u;
        List<EP_Exception_Log__c> expList = new List<EP_Exception_Log__c>();
        List<EP_Error_Handler_CS__c> settingList = new List<EP_Error_Handler_CS__c>();
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
        insert setting;  
        EP_Error_Handler_CS__c setting1 = new EP_Error_Handler_CS__c();
        setting1.Name = 'ERROR';
        setting1.severity__c = 'ERROR';
        setting1.value__c = 5;
        insert setting1;
        settingList.add(setting);
        settingList.add(setting1);
        try{
            integer i = 1/0;
            }
        catch(Exception ex){
            Test.StartTest();
            EP_LoggingService.logServiceException(ex,'org','app','method','class','ERROR',u.id,'WS_APP','null','null');
            Test.StopTest();
            expList = [Select id, EP_Method_Name__c from EP_Exception_Log__c where EP_Method_Name__c =: 'method'];
            }   
            System.debug(expList.size());
            System.AssertEquals(expList[0].EP_Method_Name__c ,'method'); 
    }
    
    public static TestMethod void checkHandledExp(){
        List<EP_Exception_Log__c> expList1 = new List<EP_Exception_Log__c>();
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
        insert setting;
        try{
            integer i = 1/0;
            }
        catch(Exception ex){
            Test.StartTest();
            EP_LoggingService.logHandledException(ex,'application','methodName','className',ApexPages.Severity.FATAL);
            Test.StopTest();
            expList1 = [Select id, EP_Method_Name__c from EP_Exception_Log__c where EP_Method_Name__c =: 'methodName'];
            }   
            System.debug(expList1.size());
            System.AssertEquals(expList1[0].EP_Method_Name__c ,'methodName'); 
    }   
    
    public static TestMethod void checkListAsyncExcp(){
    List<EP_Error_Handler_CS__c> setList = new List<EP_Error_Handler_CS__c>();
    List<String> sevList = new List<String>{'FATAL','ERROR','Threshold'};
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
    setList.add(Setting);
    EP_Error_Handler_CS__c setting2 = new EP_Error_Handler_CS__c();
        setting2.Name = 'ERROR';
        setting2.Severity__c = 'ERROR';
        setting2.Value__c = 3;
    setList.add(setting2);
    EP_Error_Handler_CS__c setting3 = new EP_Error_Handler_CS__c();
        setting3.Name = 'FATAL';
        setting3.Severity__c = 'FATAL';
        setting3.Value__c = 3;
        
    setList.add(setting3);
    insert setList;    
    List<Account> accList = new List<Account>();
    Account accObj;
    Account accObj1; 
    EP_Tank_Dip__c tankDip; 
    EP_Tank__c TankInstance;
    EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
    accObj=customHierarchy.lShipToAccounts[0];
    TankInstance=customHierarchy.lTanks[0]; 
    accObj.recordType = [Select id,name from RecordType where sObjectType='Account' and RecordType.developerName= 'EP_VMI_SHIP_TO' limit 1];
    accObj.EP_Status__c = 'Active';
    accObj.Phone = null;
    accObj1=customHierarchy.lShipToAccounts[1];
    accObj1.EP_Email__c= '123';
    accList.add(accObj);
    accList.add(accObj1);
    List<Exception> exList = new List<Exception>();
    
    try{
    database.insert(accList);
    }
    catch(Exception e){
    
        exList.add(e);
       // EP_LoggingService.logListAsynchronousException(exList,'org','app','class','method',sevList,'Gautam');
        }
        
    }
        
   }