/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @Created Date 28/12/2017                                      *
* @description  Virtual Class to insert Event                   *
****************************************************************/
public virtual class EP_Event_Subscription {
    public Database.SaveResult saveResultobj;

    public subscriptionEvent subscriptionEventObj;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_Event_Subscription() {
        subscriptionEventObj = new subscriptionEvent();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         subscribeEvent                                  *
* @description  method to insert event                          *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public virtual void subscribeEvent(){
        EP_GeneralUtility.Log('public','EP_Event_Subscription','subscribeEvent');
        List<EP_Notification_Account_Settings__c> settingList = new List<EP_Notification_Account_Settings__c>();
        if(subscriptionEventObj.eventType !=null && subscriptionEventObj.sellToId !=null)
            settingList = EP_Account_Notification_SettingsMapper.getNotificationIdByCode(subscriptionEventObj.eventType,subscriptionEventObj.sellToId);
        if(!settingList.isEmpty())
            publishEvent(settingList[0].Id);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         publishEvent                                    *
* @description  method to publish event                         *
* @param        String                                          *
* @return       void                                            *
****************************************************************/
    public void publishEvent(string accNotificationId){
        EP_GeneralUtility.Log('public','EP_Event_Subscription','publishEvent');
        EP_Notification_Framework__e eventObj = new EP_Notification_Framework__e();
        eventObj.EP_Attachment_Id__c = subscriptionEventObj.attachmentId;
        eventObj.EP_Account_Notification_Id__c = accNotificationId;
        eventObj.EP_What_Id__c = subscriptionEventObj.whatId;
        saveResultobj = EventBus.publish(eventObj);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @Created Date 28/12/2017                                      *
* @description  Inner   Class to insert Event                   *
****************************************************************/
    public class subscriptionEvent{
        public string attachmentId{get;set;}
        public string eventType{get;set;}
        public string sellToId{get;set;} 
        public string whatId{get;set;}
    }
}