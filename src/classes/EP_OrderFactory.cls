/**
 * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
 * @name        : EP_OrderFactory
 * @CreateDate  : 31/01/2017
 * @Description : This class contains code to set the context of the order
 * @Version     : <1.0>
 * @reference   : N/A
 */
 public with sharing class EP_OrderFactory {

 	private EP_OrderDomainObject orderDomainObj;
 	private csord__Order__c orderObj;
 	private final String strClassPrefix = 'EP_';
 	private final String strOrderType   = 'Order';
 	private final String strStrategy    = 'Strategy';
 	private final String strProducts    = 'Products';
 	public EP_OrderFactory(EP_OrderDomainObject ordDomainObj) {
 		this.orderDomainObj = ordDomainObj;
 		this.orderObj = orderDomainObj.getOrder();
 	}

	/** This method is used to get Order Type
	*  @date      05/02/2017
	*  @name      getType
	*  @param     NA
	*  @return    EP_Ordertype
	*  @throws    NA
	*/  
	public EP_Ordertype getType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getType');
		EP_Ordertype  oType;
		System.debug('*** order type factory - ' + strClassPrefix+orderDomainObj.getType()+strOrderType);
		oType = (EP_Ordertype)Type.forName(strClassPrefix+orderDomainObj.getType()+strOrderType).newInstance();
		return oType;
	}

	/** This method is used to get Order VMI  Type
	*  @date      16/02/2017
	*  @name      getVMIType
	*  @param     NA
	*  @return    EP_VendorManagement
	*  @throws    NA
	*/  
	public EP_VendorManagement getVMIType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getVMIType');
		EP_VendorManagement  vmiType;
		System.debug('*****:-'+strClassPrefix+orderDomainObj.getVMIType()+strStrategy);
		vmiType = (EP_VendorManagement)Type.forName(strClassPrefix+orderDomainObj.getVMIType()+strStrategy).newInstance();
		System.debug('*****:-'+strClassPrefix+orderDomainObj.getVMIType()+strStrategy);
		return vmiType;
	}


	/** This method is used to get OrderEpoch Type
	*  @date      17/02/2017
	*  @name      getEpochType
	*  @param     NA
	*  @return    EP_OrderEpoch
	*  @throws    NA
	*/  
	public EP_OrderEpoch getEpochType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getEpochType');
		EP_OrderEpoch  orderEpochType;
		orderEpochType = (EP_OrderEpoch)Type.forName(strClassPrefix+orderDomainObj.getEpochType()).newInstance();
		return orderEpochType;
	}

	/** This method is used to get Order Consignment Type
	*  @date      17/02/2017
	*  @name      getConsignmentType
	*  @param     NA
	*  @return    EP_ConsignmentType
	*  @throws    NA
	*/  
	public EP_ConsignmentType getConsignmentType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getConsignmentType');
		EP_ConsignmentType  consignType;
		consignType = (EP_ConsignmentType)Type.forName(strClassPrefix+orderDomainObj.getConsignmentType()).newInstance();
		return consignType;
	}

	/** This method is used to get Order Product Sold As Type
	*  @date      17/02/2017
	*  @name      getProductSoldAsType
	*  @param     NA
	*  @return    EP_ProductSoldAs
	*  @throws    NA
	*/  
	public EP_ProductSoldAs getProductSoldAsType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getProductSoldAsType');
		EP_ProductSoldAs  psaType;
		psaType = (EP_ProductSoldAs)Type.forName(strClassPrefix+orderDomainObj.getProductSoldAsType()+strProducts).newInstance();
		return psaType;
	}

	/** This method is used to get Order Product Sold As Type
	*  @date      17/02/2017
	*  @name      getDeliveryType
	*  @param     NA
	*  @return    EP_DeliveryType
	*  @throws    NA
	*/  
	public EP_DeliveryType getDeliveryType() {
		EP_GeneralUtility.Log('Public','EP_OrderFactory','getDeliveryType');
		EP_DeliveryType  delType;
		delType = (EP_DeliveryType)Type.forName(strClassPrefix+orderDomainObj.getDeliveryType()).newInstance();
		return delType;
	}
}