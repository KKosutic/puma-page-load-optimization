/* 
  @Author <Accenture>
   @name <EP_VMIOrderUpdateWS>
   @CreateDate <30/12/2015>
   @Description <This is apex RESTful WebService for Order Updates from WINDMS> 
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/OrderUpdate/*')
global without sharing class EP_WINDMSOrderUpdateWS{
    /*
        This is the method that handles HttpPost request for VMI Order Creations
    */
    @HttpPost
    global static void processRequest(){
        EP_GeneralUtility.Log('global','EP_WINDMSOrderUpdateWS','processRequest');
        RestRequest request = RestContext.request;
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        String requestBody = request.requestBody.toString();
        EP_IntegrationService service = new EP_IntegrationService();
        string response = service.handleRequest(EP_Common_Constant.WINDMS_ORDER_UPDATE,requestBody); 
        RestContext.response.responseBody = Blob.valueOf(response);
    }
}