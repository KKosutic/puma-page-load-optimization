@isTest
private class EP_RunMapper_UT{
   static testMethod void getActiveRunsByRoute_test() {
         
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        Id routeId = runRecords[0].EP_Route__c; 
        Test.startTest();
            list<EP_Run__c> result = localObj.getActiveRunsByRoute(routeId);
        Test.stopTest();
        System.assertEquals(true,result.size()>0);
    }
     static testMethod void getRunList_UT(){
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        //Id routeId = runRecords[0].id;                    
        Test.startTest();
            list<EP_Run__c> result = localObj.getRunList(runRecords);
        Test.stopTest(); 
        System.assertEquals(true,result.size()>0);
    }
    static testMethod void getRunsByRoute_UT(){
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        Id routeId = runRecords[0].EP_Route__c;         
        Test.startTest();
            list<EP_Run__c> result = localObj.getRunsByRoute(routeId);
        Test.stopTest(); 
        System.assertEquals(true,result.size()>0);
    }
    static testMethod void getRunsByRunId_UT(){
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
        set<Id> runIdSet =new set<Id>();   
        for(EP_Run__c run: runRecords ){
            runIdSet.add(run.id);
        }                             
        Test.startTest();
            list<EP_Run__c> result = localObj.getRunsByRunId(runIdSet);
        Test.stopTest(); 
        System.assertEquals(true,result.size()>0);
    }
    static testMethod void getAssociatedOrdersWithRun_UT(){
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();        
        Order ord = [SELECT Id,Status,EP_Run__c FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Run__c = runRecords[0].id;
        update ord;
        Map<Id,EP_Run__c> oldRunMap = New Map<Id,EP_Run__c> (runRecords);           
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2;            
        Test.startTest();
           Set <Id> result = localObj.getAssociatedOrdersWithRun(oldRunMap.values());
        Test.stopTest(); 
        System.assertEquals(true,result.size()>0);
    }
}