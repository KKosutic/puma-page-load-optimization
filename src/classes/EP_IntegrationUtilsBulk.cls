/**
  * @author       Accenture
  * @name         EP_IntegrationUtilsBulk
  * @date         14/09/2017
  * @description  Integration Utility Bulkified Methods
  */
public with sharing class EP_IntegrationUtilsBulk {
	public EP_IntegrationUtilsBulk() {
		
	}
	private static final String COUNTRY_FIELD = 'EP_Country__c';
    private static final String SHIP_TO = 'EP_ShipTo__c';
    private static final String SHIP_TOONTANK = 'EP_Ship_To__c';
    private static final String TANK_ID = 'EP_Tank__c';
    private static final String NAME = 'Name';
    private static final String ACCOUNT_ID = 'AccountId';
    private static final String ORDER_NO = 'OrderNumber';
    private static final String ORDER_ID = 'OrderId';
    private static final String ORDER_ITEM_NO = 'OrderItemNumber';
    
    private static final String QUERY_ORDER_ITEM_NO = 'Select OrderItemNumber,OrderId,Order.Account.EP_Country__c from ';
    private static final String QUERY_ACC_ID_FRM_ORDER = 'Select AccountId from Order';
    private static final String QUERY_ORDER_NO = 'Select OrderNumber,EP_ShipTo__c,AccountId,Account.EP_Country__c from '; 
    private static final String QUERY_TANK = 'Select Name,EP_Tank__c,EP_Tank__r.EP_Ship_To__r.EP_Country__c from ';
    private static final String QUERY_CNTRY_FRM_ACC = 'Select Name,EP_Country__c from Account';
    private static final String QUERY_COUNTRY = 'Select Name,EP_Country__c from ';
    private static final String QUERY_SHIP_TO = 'Select Name, EP_Ship_To__c,EP_Ship_To__r.EP_Country__c from ';
    private static final String QUERY_NAME = 'Select Name from ';
    private static final String WHERE_ID = ' Where Id=';
    private static final String ESCAPE_SEQ = '\'';
    private static final string SEND_OK_MTHD = 'sendOk';
    private static final String CLASS_NAME = 'EP_IntegrationUtilsBulk';
    private static Map<String,EP_Integration_Status_Update__c> integrationCS = EP_Integration_Status_Update__c.getAll();
    private static final String SF = 'SF';
    private static final String HYPHEN_STRING = '-';
    private static map<String,String> objId_UniqueSeqIdMap = new map<String,String>();

    
  
   /**
    * @author       Accenture
    * @name         createBulkIntegrationRecordOutbound
    * @date         14/08/2017
    * @description  Method to create integration records in Bulk
    * @param        Set<Id> setIds,String transacionId,String objType,String msgId,String company,DateTime dtSent,String status, String target, String errorDescription
    * @return       List<EP_IntegrationRecord__c>
    */  
    private static final String WHERE_ID_BIND = ' Where Id=:';
    
    public static List<EP_IntegrationRecord__c> createIntegrationRecordOutbound(Set<Id> setIds,String transacionId,String objType,
                                                                     String msgId,String company,DateTime dtSent,
                                                                     String status, String target, String errorDescription){
        EP_GeneralUtility.Log('Public','EP_IntegrationUtilsBulk','createIntegrationRecordOutbound');
        Schema.SObjectType objName = Schema.getGlobalDescribe().get(integrationCS.get(objType).EP_API_Name__c);
        Map<Id,SObject> mapIdObjRecord = getSObjectRecords(objType,objName,setIds);
       
        List<EP_IntegrationRecord__c> lstIntRecord = new List<EP_IntegrationRecord__c>();
        
        try{

            for (Id objId : setIds) {
                EP_IntegrationRecord__c intRecord = new EP_IntegrationRecord__c();
                intRecord.EP_Object_ID__c = objId;
                intRecord.EP_Object_Type__c = ObjType;
                intRecord.EP_Transaction_ID__c = transacionId;
                intRecord.EP_Message_ID__c = msgId;
                intRecord.EP_Company__c = company;
                intRecord.EP_DT_SENT__c = dtSent; 
                intRecord.EP_Status__c = status.toUpperCase();   
                intRecord.EP_Target__c = target;
                //change for unique seqId
                intRecord.EP_Error_Description__c = String.isNOtBlank(errorDescription)&&errorDescription.length()>=255?errorDescription.substring(0,254):errorDescription;
                intRecord.EP_isLatest__c = TRUE;
                intRecord.EP_Source__c = SF;
                intRecord.EP_SeqId__c = reCreateSeqId( msgId,intRecord.EP_Object_ID__c);
                //change for unique seqId
                if( objId_UniqueSeqIdMap.containsKey( ObjId ) ) {
                    intRecord.EP_SeqId__c = objId_UniqueSeqIdMap.get( ObjId );
                }

                if (intRecord.EP_Object_Type__c != NULL && integrationCS != NULL  && integrationCS.containsKey(intRecord.EP_Object_Type__c) 
                    && mapIdObjRecord.containsKey(objId)){

                    intRecord = updateNameAndCountryOnIntegrationRec(intRecord,mapIdObjRecord,objId);
                }

                if (status.equalsIgnoreCase(EP_Common_Constant.ERROR_SENT_STATUS)) {
                    intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.MIDDLEWARE_TRANS_ERROR;
                } else if (status.equalsIgnoreCase(EP_Common_Constant.ERROR_SYNC_STATUS)) {
                    intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.TARGET_TRANS_ERROR;
                } else if ((status.equalsIgnoreCase(EP_Common_Constant.ERROR_RECEIVED_STATUS)) || (status.toUpperCase().equalsIgnoreCase(EP_Common_Constant.ERROR_ACKNOWLEDGED_STATUS))) {
                    intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.FUCTIONAL_ERROR;
                }

                lstIntRecord.add(intRecord);
            }
        }
        catch(Exception ex){
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, 'createIntegrationRecordOutbound', CLASS_NAME,apexPages.severity.ERROR);
        }
        return lstIntRecord;                     
    }

    /**
    * @author       Accenture
    * @name         getSObjectRecords
    * @date         14/08/2017
    * @description  method to get sObject Records
    * @param        String objType,Set<Id> setIds
    * @return       Map<Id,SObject>
    */ 
    @TestVisible
    private static Map<Id,SObject> getSObjectRecords(String objType,Schema.SObjectType objName,Set<Id> setIds) {
        
        Map<Id,SObject> mapIdObjRecord = new Map<Id,SObject>();

        if (objType.equals(EP_Common_Constant.ACCOUNTS)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_COUNTRY+objName+WHERE_ID_BIND+'setIds'));
        }
        else if (objType.equals(EP_Common_Constant.STOCKLOCATIONS)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_SHIP_TO+objName+WHERE_ID_BIND+'setIds'));
        }
        else if (objType.equals(EP_Common_Constant.DIPS)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_TANK+objName+WHERE_ID_BIND+'setIds'));
        }
        else if (objType.equals(EP_Common_Constant.ORDERITEMS)) {
           mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_ORDER_ITEM_NO+objName+WHERE_ID_BIND+'setIds'));   
        } 
        else if (objType.equals(EP_Common_Constant.ORDERS_INT)) {
           mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_ORDER_NO+objName+WHERE_ID_BIND+'setIds'));
        }
        else if (objType.equals(EP_Common_Constant.BANKACCOUNTS)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_COUNTRY+objName+WHERE_ID_BIND+'setIds'));
        } 
        else if (objType.equals(EP_Common_Constant.CREDITEXCEPTIONS)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_NAME+objName+WHERE_ID_BIND+'setIds'));
        } 
        else if (objType.equals(EP_Common_Constant.TANKS_INT)) {
            mapIdObjRecord = new Map<Id,SObject>(Database.query(QUERY_SHIP_TO+objName+WHERE_ID_BIND+'setIds'));
        }

        return mapIdObjRecord;
    }


     /**
    * @author       Accenture
    * @name         updateNameAndCountryOnIntegrationRec
    * @date         14/08/2017
    * @description  method to update country and Name on Integration record
    * @param        EP_IntegrationRecord__c intRecord,Map<Id,SObject> mapIdObjRecord,Id objI
    * @return       EP_IntegrationRecord__c
    */ 
    private static final String SHIPTO_REL = 'EP_Ship_To__r';
    private static final String TANK_REL = 'EP_Tank__r';
    @TestVisible
    private static EP_IntegrationRecord__c  updateNameAndCountryOnIntegrationRec(EP_IntegrationRecord__c intRecord,Map<Id,SObject> mapIdObjRecord,Id objId) {

        try {
            if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.ACCOUNTS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).get(COUNTRY_FIELD));
            }
            else if(intRecord.EP_Object_Type__c.equals(EP_Common_Constant.STOCKLOCATIONS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).getSObject(SHIPTO_REL).get(COUNTRY_FIELD));
            }
            else if(intRecord.EP_Object_Type__c.equals(EP_Common_Constant.DIPS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).getSObject(TANK_REL).getSObject(SHIPTO_REL).get(COUNTRY_FIELD));
            }
            else if(intRecord.EP_Object_Type__c.equals(EP_Common_Constant.ORDERITEMS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(ORDER_ITEM_NO));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).getSObject(EP_Common_Constant.ORDER).getSObject(EP_Common_Constant.ACCOUNT_OBJ).get(COUNTRY_FIELD));
            }
            else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.ORDERS_INT)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(ORDER_NO));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).getSObject(EP_Common_Constant.ACCOUNT_OBJ).get(COUNTRY_FIELD));
            }
            else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.BANKACCOUNTS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).get(COUNTRY_FIELD));
            } 
            else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.CREDITEXCEPTIONS)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
            } 
            else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.TANKS_INT)) {
                intRecord.EP_Object_Record_Name__c = String.ValueOf(mapIdObjRecord.get(objId).get(NAME));
                intRecord.EP_Country__c = String.ValueOf(mapIdObjRecord.get(objId).getSObject(SHIPTO_REL).get(COUNTRY_FIELD));
            }
        }
        catch(Exception ex){
            System.debug('******'+ex.getMessage()+'******'+ex.getStackTraceString());
           EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, 'updateNameAndCountryOnIntegrationRec', 
            CLASS_NAME,apexPages.severity.ERROR);
       }
        

        return intRecord;
    }

    /*
     * Description: To re create seqId from SF RecordId to <SFID-DATETIME-MESSAGEAUTONUMBER>
     * @Author: Jai Singh
     * @param messageId: generated mesageId
     * @param existingSeqId: SF RecordId
     */
     @TestVisible
     public static String reCreateSeqId( String mesageId, String existingSeqId) {
        
        EP_GeneralUtility.Log('Public','EP_IntegrationUtil','reCreateSeqId');
        String newSeqId = existingSeqId;
        try{
            if( String.isNotBlank( mesageId ) && mesageId.length() == 36 && String.isNotBlank( existingSeqId ) && existingSeqId.length() == 18 )
            {   /**NOVASUITE CHANGES BY ASHOK-START**/
                newSeqId = mesageId.substring(12);
                newSeqId = newSeqId.remove(EP_COMMON_CONSTANT.TIME_SEPARATOR_SIGN);
                newSeqId = newSeqId.replaceFirst(EP_COMMON_CONSTANT.STRING_HYPHEN,EP_COMMON_CONSTANT.BLANK);
                newSeqId = existingSeqId+EP_COMMON_CONSTANT.STRING_HYPHEN+newSeqId;
                /**NOVASUITE CHANGES BY ASHOK-END**/
            }
            if( String.isNotBlank( existingSeqId ) && existingSeqId.length() >= 18 )
            {
                String existingSeqIdTemp = existingSeqId.substring( 0, 18 );
                objId_UniqueSeqIdMap.put( existingSeqIdTemp, newSeqId );
            }
            system.debug('newSeqId='+newSeqId);
        }
        catch(Exception ex){
           EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, 'reCreateSeqId', 
            CLASS_NAME,apexPages.severity.ERROR);
       }
       return newSeqId;
   }
}