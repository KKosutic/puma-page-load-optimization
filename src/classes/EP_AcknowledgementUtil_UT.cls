@isTest
public class EP_AcknowledgementUtil_UT
{
    static String NAME  = 'Data Set NAme';
    static String SEQID = 'ASDE133k';
    static String ERROR_CODE = '500';
    static String ERROR_DESCRIPTION = 'NULL POINTER';
    @testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    static testMethod void createDataSet_test() { 
        Test.startTest();
        EP_AcknowledgementStub.dataSet result = EP_AcknowledgementUtil.createDataSet(NAME, SEQID, ERROR_CODE, ERROR_DESCRIPTION);
        Test.stopTest();
        System.assertEquals(NAME, result.name);
        System.assertEquals(SEQID, result.seqId);
        System.assertEquals(ERROR_CODE, result.errorCode);
        System.assertEquals(ERROR_DESCRIPTION, result.errorDescription);
    }
    static testMethod void doDML_test(){
        List<Account> customersToUpdate = new List<Account>{EP_TestDataUtility.getSellTo()};
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        Boolean result = EP_AcknowledgementUtil.doDML(customersToUpdate, ackDatasets);
        Test.stopTest();
        System.assertEquals(true, !result);
    }
    static testMethod void doDML1_test(){
        Account acc = EP_TestDataUtility.getSellTo();
        List<EP_Bank_Account__c> banksToUpsert = new List<EP_Bank_Account__c>{EP_TestDataUtility.createBankAccount(acc.id)};
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        Boolean result = EP_AcknowledgementUtil.doDML(banksToUpsert, ackDatasets);
        Test.stopTest();
        System.assertEquals(true, !result);      
    }
}