public with sharing class EP_OrderDataWrapper {
    
    
    public String orderType;//Sales Order,Transfer Order, Consumption Order
    public String vmiType; // VMI/NonVMI
    public String deliveryType; // Ex-Rack,Delivery,Consumption
    public String productSoldAsType; // Bul, Packaged
    public String epochType; //Current,Retrospective
    public String consignmentType;//Consignment,NonConsignment
    public Boolean isBillToRequired;//True,False
    public String scenarioType;//Positive,Negative
    public String status;
    public String reasonForChange;
    public boolean isInvoiceRequired ;
        
    public EP_OrderDataWrapper(){
        orderType = EP_Common_Constant.SALES_ORDER;
        vmiType = EP_Common_Constant.NONVMI;
        deliveryType = EP_Common_Constant.DELIVERY;
        productSoldAsType = EP_Common_Constant.PRODUCT_BULK;
        epochType = EP_Common_Constant.EPOC_CURRENT;
        consignmentType = EP_Common_Constant.CONSIGNMENT;
        isBillToRequired = False;
        scenarioType = EP_Common_Constant.POSITIVE;
        status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        reasonForChange = EP_Common_Constant.BLANK;
        isInvoiceRequired = false;
    }

}