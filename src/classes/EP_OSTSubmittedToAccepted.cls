/*
    *  @Author <Aravindhan Ramalingam>
    *  @Name <EP_OSTSubmittedToAccepted>
    *  @CreateDate <03/02/2017>
    *  @Description <Handles status change from any status to accepted>
    *  @Version <1.0>
    */

    public class EP_OSTSubmittedToAccepted extends EP_OrderStateTransition{
        
        public EP_OSTSubmittedToAccepted(){
            finalState = EP_OrderConstant.OrderState_Accepted;
        }
        
        public override boolean isTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OSTSubmittedToAccepted','isTransitionPossible');
            return super.isTransitionPossible();
        }
        
        public override boolean isRegisteredForEvent(){
            EP_GeneralUtility.Log('Public','EP_OSTSubmittedToAccepted','isRegisteredForEvent');
            return super.isRegisteredForEvent();       
        }

        public override boolean isGuardCondition(){
            EP_GeneralUtility.Log('Public','EP_OSTSubmittedToAccepted','isGuardCondition');
            if(EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(this.order.getOrder().EP_Payment_Term__c)){
                return false;
            }
            this.order.getOrder().EP_Sync_with_NAV__c = true;
            return true;               
        }
    }