@isTest
public class EP_VMIShipToASInactive_UT
{
    static final string EVENT_NAME = '07-InactiveTo07-Inactive';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void setAccountDomainObject_test() {
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    
    //Delegates to other methods. Adding dummy assert     
    static testMethod void doOnEntry_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        Account newAccount = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario().getAccount();
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true); 
    }  

    //Method has no implementation, hence adding dummy assert   
    static testMethod void doOnExit_test() {
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();      
        System.assertEquals(true, true); 
    }    
    static testMethod void doTransition_PositiveScenariotest() {
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        string message;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }Catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
    } 
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VMIShipToASInactive localObj = new EP_VMIShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
        
    }    
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/
}