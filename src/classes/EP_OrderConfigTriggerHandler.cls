/**
 * @author <Accenture>
 * @name <EP_OrderConfigTriggerHandler>
 * @createDate <21/01/2016>
 * @description <This class handles requests from Order Configuration Trigger> 
 * @version <1.0>
 */
 public with sharing class EP_OrderConfigTriggerHandler {
 	
 	/**
	* @author <Accenture>
	* @description <This method handles before insert requests from Order Configuration Trigger>
	* @name <doBeforeInsert>
	* @date <21/01/2016>
	* @param List<EP_Order_Configuration__c>
	* @return void
	*/
   public static void doBeforeInsert(List<EP_Order_Configuration__c> listNewOrderConfig){
        EP_OrderConfigTriggerHelper.validateOrderRange(listNewOrderConfig);
    }
  }