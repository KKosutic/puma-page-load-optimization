/* 
    @Author <Ram Rai>
    @name <EP_FE_Customer_SupportInfo>
    @CreateDate <11/07/2016>
    @Description <This is API class use to return localSupportNo>  
    @Version <1.0>
*/

@RestResource(urlMapping='/FE/V1/support/customerinfo/*')  
/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_Customer_SupportInfo>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/        
global with sharing class EP_FE_Customer_SupportInfo{
    
    @HttpGet
/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_Customer_SupportInfoResponse >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/  
    global static EP_FE_Customer_SupportInfoResponse returnLocalSupportNumber() {
    
        EP_FE_Customer_SupportInfoResponse objCstSup = new EP_FE_Customer_SupportInfoResponse();
        objCstSup.address = EP_PortalOrderHelper.returnLocalSupportAddress();
        objCstSup.numbers = EP_PortalOrderHelper.returnLocalSupportNumbers();
       
        return objCstSup;        
    }    
}