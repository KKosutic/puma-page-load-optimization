/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_ExRack
  * @CreateDate  : 05/02/2017
  * @Description : This class executes logic for Delivery
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public with sharing class EP_ExRack extends EP_DeliveryType  {
    
    private static final String exRackInventoryCheck_Method = 'exRackInventoryCheck';
    private static final String deliveryInventoryCheck_Method = 'deliveryInventoryCheck';
    private static final String ClassName = 'EP_ExRack';
    
    public EP_ExRack() {
      
    }

    public override void updateTransportService(csord__Order__c objOrder) {
      EP_GeneralUtility.Log('Public','EP_ExRack','updateTransportService');
      
      super.updateTransportService(objOrder);
    }


    /** This method is used to get price book entries
      *  @date      05/02/2017
      *  @name      findPriceBookEntries
      *  @param     Id pricebookId,Id storageLocId,csord__Order__c objOrder
      *  @return    List<PriceBookEntry>
      *  @throws    NA
      */ 
      public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_ExRack','findPriceBookEntries');

        Set<Id> availableProductIds = super.getAvailableProducts(objOrder.EP_Stock_Holding_Location__c,objOrder);

        List<PriceBookEntry> lstPriceBookEntry = new List<PriceBookEntry>(); 

        if (!availableProductIds.isEmpty()) {

           lstPriceBookEntry = new EP_PriceBookEntryMapper().getCsPriceBookEntriesforExRack(availableProductIds,pricebookId,objOrder);
        }
        
        return lstPriceBookEntry;
      }
      
      
    /** This method is used set the visbility of Customer PO field on portal order page
      *  @date      05/02/2017
      *  @name      showCustomerPO
      *  @param     csord__Order__c orderObject
      *  @return    Boolean
      *  @throws    NA
      */ 
      public override Boolean showCustomerPO(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_ExRack','showCustomerPO');
        return orderObject.csord__Account__r.EP_Is_Customer_Reference_Visible__c;
      }

      /** returns record recordype for the Order
      *  @date      22/02/2017
      *  @name      findRecordType
      *  @param     NA
      *  @return    Id
      *  @throws    NA
      */
      public override Id findRecordType(EP_OrderEpoch epochType,EP_VendorManagement vmType,EP_ConsignmentType consignmentType) {
          EP_GeneralUtility.Log('Public','EP_ExRack','findRecordType');
          return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
      }
    }