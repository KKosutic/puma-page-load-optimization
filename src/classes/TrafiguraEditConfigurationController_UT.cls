@isTest
public class TrafiguraEditConfigurationController_UT {
    
    @isTest
    static void openEditOrderPage_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
            PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderPage_cancelCheckDoneTrue_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        newOrder.Cancellation_Check_Done__c = true;
		insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
            PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderPageWithId_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
        
        Test.startTest();
            String pr = TrafiguraEditConfigurationController.openEditOrderPageWithId(newOrder.Id);
        Test.stopTest();
        
        System.assertEquals('/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c), pr, 'Wrong string value returned.');
    }
}