/* 
  @Author <Ashok Arora>
  @name <EP_StockHoldingLocationTest>
  @CreateDate <13/11/2015>
  @Description <This is the test class for Stock Holding Location Trigger>
  @Version <1.0>
 
*/
@isTest
private class EP_StockHoldingLocationTest {
    public static final String COMPANY_CODE = 'AU';
    private static Account testShipToAccount;
    private static Account testSellToAccount;
    private static Company__c testCompany;


    // Const
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    /*
        This method tests the vaidation for one primary stock holding location on ship to account 
        while inserting more than one primary stock holding location
    */
    static testMethod void onePrimaryLocationInsertTest() {
        list<EP_Stock_Holding_Location__c> lsstockloc = new list<EP_Stock_Holding_Location__c>();
        System.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            Test.startTest();
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);

            EP_Country__c testCountry =
                EP_TestDataUtility.createCountryRecord(
                    COUNTRY_NAME,
                    COUNTRY_CODE,
                    COUNTRY_REGION);
            Database.insert(testCountry);

            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);
            Database.insert(testRegion);
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy =
                EP_TestDataUtility.createCustomerHierarchyForNAV(1);    testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];

            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours
                WHERE IsActive = TRUE
                AND IsDefault = TRUE
                LIMIT: EP_Common_Constant.ONE
            ];

            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);

            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);
            
            // Adding Run and Route            
            EP_Route__c route1 =  new EP_Route__c(EP_Status__c = 'Active');
            route1.EP_Storage_Location__c = testStorageLocation.Id;
            Database.insert(route1);
            
            EP_Route_Allocation__c routeAllocation = new EP_Route_Allocation__c();
            routeAllocation.Delivery_Window_End_Date__c = 10;
            routeAllocation.EP_Delivery_Window_Start_Date__c = 5;
            routeAllocation.EP_Route__c = route1.Id;
            routeAllocation.EP_Ship_To__c = testShipToAccount.Id;
            Database.Insert(routeAllocation);
            
            // Create a supply location

            Id strSupplyLocationSellToRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    EP_Stock_Holding_Location__c testSupplyOption1 =
                EP_TestDataUtility.createSellToStockLocation(
                    testSellToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            testSupplyOption1.EP_Default_Route__c = route1.Id;
            testSupplyOption1.EP_Location_Type__c = 'Primary';
            Database.insert(testSupplyOption1);
            lsstockloc.add(testSupplyOption1);
      Test.stopTest();

            Id strSupplyLocationDeliveryRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 =
                EP_TestDataUtility.createShipToStockLocation(
                    testShipToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationDeliveryRecordTypeId);
            /*testSupplyOption2.EP_Trip_Duration__c = 1;
            testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Location_Type__c = 'Primary';*/
            Database.insert(testSupplyOption2);
            

            //update the supply option
            Database.update(testSupplyOption2);
            
            lsstockloc.add(testSupplyOption2);
            EP_StorageLocationTriggerHelper.checkShipToRouteAllocation(lsstockloc);
            //delete the supply option
            Database.delete(testSupplyOption2);
        }
    }
    
    /*
        This method tests the vaidation for one secondary stock holding location on ship to account
    */
    static testMethod void oneSecondaryLocationInsertTest() {
        list<EP_Stock_Holding_Location__c> lsstockloc = new list<EP_Stock_Holding_Location__c>();
        System.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            Test.startTest();
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);

            EP_Country__c testCountry =
                EP_TestDataUtility.createCountryRecord(
                    COUNTRY_NAME,
                    COUNTRY_CODE,
                    COUNTRY_REGION);
            Database.insert(testCountry);

            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);
            Database.insert(testRegion);
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy =
                EP_TestDataUtility.createCustomerHierarchyForNAV(1);    testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];

            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours
                WHERE IsActive = TRUE
                AND IsDefault = TRUE
                LIMIT: EP_Common_Constant.ONE
            ];

            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);

            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);
            
            // Adding Run and Route            
            EP_Route__c route1 =  new EP_Route__c(EP_Status__c = 'Active');
            route1.EP_Storage_Location__c = testStorageLocation.Id;
            Database.insert(route1);
            
            EP_Route_Allocation__c routeAllocation = new EP_Route_Allocation__c();
            routeAllocation.Delivery_Window_End_Date__c = 10;
            routeAllocation.EP_Delivery_Window_Start_Date__c = 5;
            routeAllocation.EP_Route__c = route1.Id;
            routeAllocation.EP_Ship_To__c = testShipToAccount.Id;
            Database.Insert(routeAllocation);
            
            // Create a supply location

            Id strSupplyLocationSellToRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    EP_Stock_Holding_Location__c testSupplyOption1 =
                EP_TestDataUtility.createSellToStockLocation(
                    testSellToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            testSupplyOption1.EP_Default_Route__c = route1.Id;
            testSupplyOption1.EP_Location_Type__c = 'Primary';
            Database.insert(testSupplyOption1);
            lsstockloc.add(testSupplyOption1);
      Test.stopTest();

            Id strSupplyLocationDeliveryRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 =
                EP_TestDataUtility.createShipToStockLocation(
                    testShipToAccount.Id,
                    false,
                    testStorageLocation.Id,
                    strSupplyLocationDeliveryRecordTypeId);
            testSupplyOption2.EP_Trip_Duration__c = 1;
            /*testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
           
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Location_Type__c = 'Secondary';*/
            Database.insert(testSupplyOption2);
            

            //update the supply option
            Database.update(testSupplyOption2);
            
            lsstockloc.add(testSupplyOption2);
            EP_StorageLocationTriggerHelper.checkShipToRouteAllocation(lsstockloc);
            //delete the supply option
            Database.delete(testSupplyOption2);
        }
    }
    
}