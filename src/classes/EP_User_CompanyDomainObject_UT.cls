@isTest
private class EP_User_CompanyDomainObject_UT {
	
	static testMethod void doActionAfterInsert_test(){
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyService service = new EP_UserCompanyService(userCompDomain);
		EP_UserCompanyMapper UserCompanyMapper = new EP_UserCompanyMapper(userCompDomain);
		Test.startTest();
        userCompDomain.doActionAfterInsert();
		Test.stopTest();
		//Method is delegating to other method hence a dummy assert is added
        system.assert(true);
	}
	
	static testMethod void doActionAfterDelete_test(){
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyService service = new EP_UserCompanyService(userCompDomain);
		EP_UserCompanyMapper UserCompanyMapper = new EP_UserCompanyMapper(userCompDomain);
		Test.startTest();
        userCompDomain.doActionAfterDelete();
		Test.stopTest();
		//Method is delegating to other method hence a dummy assert is added
        system.assert(true);
	}

	static testMethod void getUserCompanyObject_test(){
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyService service = new EP_UserCompanyService(userCompDomain);
		EP_UserCompanyMapper UserCompanyMapper = new EP_UserCompanyMapper(userCompDomain);
		Test.startTest();
        userCompDomain.getUserCompanyObject();
		Test.stopTest();
		//Method is delegating to other method hence a dummy assert is added
        system.assert(true);
	}
}