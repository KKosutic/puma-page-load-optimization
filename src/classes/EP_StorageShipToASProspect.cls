/*
 *  @Author <Accenture>
 *  @Name <EP_StorageShipToASProspect >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  01-Prospect Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageShipToASProspect extends EP_AccountState{
     
    public EP_StorageShipToASProspect() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASProspect','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASProspect','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASProspect','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASProspect','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASProspect','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }
}