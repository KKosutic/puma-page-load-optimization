@isTest
public class EP_ShipToPricingEngPayldCtrlExtn_UT
{
	static testMethod void checkPageAccess_positivetest() {
		Account acc = EP_TestDataUtility.getShipToPositiveScenario();
		Account accObject = [select id,Parent.EP_Requested_Payment_Terms__c,EP_Defer_Upper_Limit__c,Parent.EP_Requested_Packaged_Payment_Term__c from Account where Id=:acc.Id];
		ApexPages.StandardController sc = new ApexPages.StandardController(accObject);
		
		PageReference pageRef = Page.EP_ShipToPricingEngineSyncWithNAVXML;
        Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE, '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, String.valueOf(acc.Id));
		EP_ShipToPricingEnginePayloadCtrlExtn localObj = new EP_ShipToPricingEnginePayloadCtrlExtn(sc);
		Test.startTest();
		PageReference result = localObj.checkPageAccess();
		Integer deferUpperLimit = localObj.deferUpperLimit;
		Test.stopTest();
		System.AssertEquals(true,result == NULL);
	}

	static testMethod void checkPageAccess_negativetest() {
		Account acc = EP_TestDataUtility.getShipToPositiveScenario();
		Account accObject = [select id,Parent.EP_Requested_Payment_Terms__c,Parent.EP_Requested_Packaged_Payment_Term__c from Account where Id=:acc.Id];
		ApexPages.StandardController sc = new ApexPages.StandardController(accObject);
		EP_ShipToPricingEnginePayloadCtrlExtn localObj = new EP_ShipToPricingEnginePayloadCtrlExtn(sc);
		Test.startTest();
		PageReference result = localObj.checkPageAccess();
		Test.stopTest();
		System.AssertEquals(true,result != NULL);
		System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
	}
}