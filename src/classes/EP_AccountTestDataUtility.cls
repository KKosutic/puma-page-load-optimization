/**
   @Author Rahul Jain
   @name <EP_AccountTestDataUtility>
   @CreateDate 12/27/2106
   @Description This is a test class which will be use as helper to create unit test data for Account Object 
   @Version <1.0>
   @reference <Referenced program names>
*/
  
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
*/
@isTest
public class EP_AccountTestDataUtility {
    private static final string CURR_USD = 'USD';
    /**  Test data creation for NonVmiShip to recordtype.
       *  @name < createNonVMIshipToAccountWithBasicDataSetup >
       *  @param List of accounts and doUpdate boolean variable.
       *  @return List of Accounts..
       *  @throws NA
    */
    public static list<Account> createNonVMIshipToAccountWithBasicDataSetup(list<Account> sellToAccountList,boolean doUpdate){
        //create ship to
        list<Account> nonVMIshipToList = new list<Account>(); 
        list<Account> nonVMIshipToUpdateList = new list<Account>();
        list<EP_Stock_holding_location__c> shlist = new list<EP_Stock_holding_location__c>();
        Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( 'Australia','AU' ,'Australia' );
        Database.insert(country,false);
        ID shipToToSHLRTID= EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.SUPPLY_LOCATION_OBJ, EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        //Get Multiple storage locations of sellTo and any of one to be pass in createShipToStockLocation method
        map<id, list<EP_Stock_Holding_Location__c>> sellTo_StorageLocListMap = getSellToStorageLocations(sellToAccountList);
        Account nonVMIshipTo = new Account();
        for(Account sellToAccount : sellToAccountList){
            nonVMIshipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,shpToRecordTypeId);
            nonVMIshipTo.EP_Country__c = country.id;
            nonVMIshipTo.EP_Transportation_Management__c = 'Puma Planned';
            nonVMIshipTo.EP_Eligible_for_Rebate__c = true;
            nonVMIshipTo.EP_Ship_To_Type__c = EP_Common_Constant.CONSIGNMENT;
            nonVMIshipToList.add(nonVMIshipTo);
        }
        Database.insert(nonVMIshipToList);
        //CreateVendorId()
        list<Account> vendorList = createTestVendors(1,'Transporter', true);
        for(Account nonVmiObj : nonVMIshipToList) {
            Id storageLocationID = null;
            if(!sellTo_StorageLocListMap.get(nonVmiObj.parentId).isEmpty()){
                storageLocationID = sellTo_StorageLocListMap.get(nonVmiObj.ParentId)[0].Stock_Holding_Location__c;
            }
            shlist.add(EP_TestDataUtility.createShipToStockLocation(nonVmiObj.id,true,storageLocationID,vendorList[0].id,shipToToSHLRTID)); //Add one SHL
            nonVmiObj.EP_Status__c  = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            nonVMIshipToUpdateList.add(nonVmiObj);
        }
        Database.insert(shlist);
        if(doUpdate) {
            Database.update(nonVMIshipToUpdateList);
        }
        return nonVMIshipToUpdateList;
    }
    
    /**  This method creates test data for VMIship to recordtype.
       *  @name <createVMIshipToAccountWithBasicDataSetup>
       *  @param List of Accounts and boolean doUpdate flag.
       *  @return updated Accounts list.
       *  @throws NA
    */
    public static list<Account> createVMIshipToAccountWithBasicDataSetup(list<Account> sellToAccountList, boolean doUpdate) {
        //list<Account> vMIshipToList = new list<Account>();
        list<Account> vMIshipToUpdateList = new list<Account>();
        //list<EP_Stock_holding_location__c> shlist = new list<EP_Stock_holding_location__c>();
         list<Account> vMIshipToList = populateVMIShipToObject(sellToAccountList, true);
        
        map<id,list<id>> sellTo_ProductIdMap = getProductsforSellToAccounts(sellToAccountList);
        //create tank
        list<EP_Tank__c> tankList = new list<EP_Tank__c>();
        for(Account vmiShipTo : vMIshipToList) {
            if(!sellTo_ProductIdMap.keyset().isEmpty()){
                tankList.add(EP_TestDataUtility.createTestEP_Tank(vmiShipTo.id,sellTo_ProductIdMap.get(vmiShipTo.parentId)[0]));
            }
            vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            vMIshipToUpdateList.add(vmiShipTo);
        }
        database.insert(tankList);
       //Update ship to stats to basic data setup
        if(doUpdate){
            Database.update(vMIshipToUpdateList);
        }
        return vMIshipToUpdateList;
    }
    
    /**
       * Creates account with status as Set up and NonVmiShip to RecordType.
       *  @name < createNonVMIshipToAccountWithSetupStatus >
       *  @param List of accounts to be created and doUpdate.
       *  @return Updated Account list.
       *  @throws NA
    */
    public static list<Account> createNonVMIshipToAccountWithSetupStatus(list<Account> sellToAccount, boolean doUpdate) {
        list<Account> nonVMIshipToList = new list<Account>();
        list<Account> nonVMIshipToAccountList = createNonVMIshipToAccountWithBasicDataSetup(sellToAccount,doUpdate);
        for(Account nonVmiShipTo : nonVMIshipToAccountList) {
            nonVmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            nonVMIshipToList.add(nonVmiShipTo);
        }
        if(doUpdate) {
            database.update(nonVMIshipToList);
        }
        return nonVMIshipToList;
    }
    
    /**
       * Creates account with status as Set up and recordtype as VMI Ship To.
       *  @name < createVMIshipToAccountWithSetupStatus >
       *  @param List of accounts to be created and doUpdate.
       *  @return updated Account list.
       *  @throws NA
    */
    public static list<Account> createVMIshipToAccountWithSetupStatus(list<Account> sellToAccount, boolean doUpdate) {
        list<Account> vMIshipToList = new list<Account>();
        list<Account> vMIshipToAccountList = createVMIshipToAccountWithBasicDataSetup(sellToAccount,doUpdate);
        for(Account vmiShipTo : vMIshipToAccountList) {
            vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            vMIshipToList.add(vmiShipTo);
        }
        if(doUpdate) {
            database.update(vMIshipToList);
        }
        return vMIshipToList;
    }
    
    /**
       * Creates accounts with status as Set Up and Sell to Record Types.
       *  @name < createSellToAccountWithActiveStatus >
       *  @param Number of accounts to be created and doUpdate.
       *  @return Updated Account list.
       *  @throws NA
    */
    public static List<Account> createSellToAccountWithSetupStatus(integer cont, boolean doUpdate) {
        List<Account> sellToAccountList = new List<Account>();
        List<Account> sellToAccountListBasicDataSetup = new List<Account>();
        List<Account> sellToAccountListSetup = new List<Account>();
        list<Pricebook2> customPBList  = createPriceBookForAccounts(cont);
        //EP_Freight_Matrix__c freightMatrix = [select Id from EP_Freight_Matrix__c limit 1];
        //CREATE FREIGHT MATRIX
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        Database.insert(freightMatrix);
        //EP_Country__c country = EP_TestDataUtility.createCountryRecord( 'Australia','AU' ,'Australia' );
        //Database.insert(country,false);
        //BusinessHours bhrs = [Select Name from BusinessHours where IsActive =true AND IsDefault = true limit 1];
        //Account storageLoc = EP_TestDataUtility.CreateStorageLocAccount(country.id,bhrs.id);
        //database.insert(storageLoc);
        List<Account> storageLoc = createStorageLocationAccount(1,true);
        Account sellToObj = new account();
        for(Integer i = 0; i < cont; i++) {
            sellToObj  = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
            sellToObj.EP_PriceBook__c = customPBList[i].id;
            sellToObj.EP_Puma_Company__c = customPBList[i].EP_Company__c;
           // sellToObj.CurrencyIsoCode = CURR_USD; //Added by bhushan 13-1-2017
            sellToAccountList.add(sellToObj);
        }
        //Insert Account with Prospect Status
        database.insert(sellToAccountList);
        
        //create Stock Holding Location and associate with Account
        //Account storageLocation  = [select Id from Account where RecordTypeId =:EP_TestDataUtility.STORAGELOCATION_RT limit 1];
        ID sellToSHLRTID = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Stock_Holding_Location__c', 'Ex_Rack_Supply_Location');
        list<EP_Stock_holding_location__c> shlist = new list<EP_Stock_holding_location__c>();
        EP_Stock_holding_location__c shlObj;
        for(Integer i = 0; i < sellToAccountList.size(); i++) {
            shlObj = EP_TestDataUtility.createSellToStockLocation(sellToAccountList[i].id,false, storageLoc[0].id,sellToSHLRTID);
            //shlObj.EP_Use_Managed_Transport_Services__c = 'N/A';
            shlist.add(shlObj);
        }
        database.insert(shlist);
        //Update Status with Basic Data Setup
        for(Account accObj : sellToAccountList){
            accObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            sellToAccountListBasicDataSetup.add(accObj);
        }
        //update Account for Basic Data Setup Status
        database.update(sellToAccountListBasicDataSetup);
        
        if(doUpdate) {
            //Update With Account Setup Status
            for(Account accObj : sellToAccountListBasicDataSetup){
                accObj.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
                sellToAccountListSetup.add(accObj);
            }
            database.update(sellToAccountListSetup);
        }
        return sellToAccountListSetup;
    }
    
    /**
       * Creates account with status as Active and Sell to RecordType .
       *  @name < createSellToAccountWithActiveStatus >
       *  @param Number of accounts to be created and doUpdate.
       *  @return Inserted Account list.
       *  @throws NA
    */
    public static List<Account> createSellToAccountWithActiveStatus(integer cont, boolean doUpdate) {
        list<Account> sellToSetUpList = createSellToAccountWithSetupStatus(cont, doUpdate);
        list<Account> sellToAccountList = new list<Account> ();
        for(Account sellToObj : sellToSetUpList) {
            sellToObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            sellToAccountList.add(sellToObj);
        }
        if(doUpdate) {
            database.update(sellToAccountList);
        }
        return sellToAccountList;
    }
    
    /**
       * Creates an account with status as prospect and with proper billing details.
       *  @name <createBillToAccountWithProspectStatus>
       *  @param Number of accounts to be created and doInsert.
       *  @return Inserted Account list.
       *  @throws NA
    */
    public static List<Account> createBillToAccountWithProspectStatus(integer cont, boolean doInsert) {
        list<Account> billToAccountList = new list<Account>();
        final String COMPANY_CODE = 'AUN2_BillTO';
        //Company__c company = EP_TestDataUtility.createCompany( COMPANY_CODE );
        //Database.upsert(company);
        Company__c company = new Company__c();
        list<Company__c> cmpObjList = [select id from Company__c where EP_Company_Code__c =: COMPANY_CODE limit 1];
        if(cmpObjList.isEmpty()) {
            company = EP_TestDataUtility.createCompany(COMPANY_CODE); 
            database.insert(company);  
        } else {
            company = cmpObjList[0];
        }
        
        Account billToAccount = new Account();
        EP_Common_Util.isValidAddress = true;
        Id billToRT = EP_TestDataUtility.BILLTO_RT;
        for(integer i = 0; i < cont; i++) {
            billToAccount = new Account();
            billToAccount.Name = 'BillAccount'+i+system.now().millisecond()+Math.round(Math.random()*1000000);
            billToAccount.EP_Is_Valid_Address__c = true;
            billToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
            billToAccount.EP_Billing_Basis__c = 'Ordered';
            billToAccount.EP_Billing_Method__c  = 'Per Order';
            billToAccount.EP_Billing_Frequency__c  = 'Daily';
            billToAccount.recordtypeid = billToRT; 
            billToAccount.EP_Ship_To_UTC_Timezone__c = 'UTC+11:00';
            billToAccount.Phone  = '8348548545';
            billToAccount.EP_Mobile_Phone__c = '999999999';
            billToAccount.EP_EMail__c = 'a@b.c';
            billToAccount.BillingCity = 'testCity';
            billToAccount.BillingStreet = 'testStreet';
            billToAccount.BillingState = 'testState';
            billToAccount.BillingCountry = 'testCountry';
            billToAccount.BiLlingPostalCode = '123456';
            billToAccount.ShippingCity = 'ShipCity';
            billToAccount.ShippingStreet = 'ShipStreet';
            billToAccount.ShippingState = 'ShipState';
            billToAccount.ShippingCountry = 'ShipCountry';
            billToAccount.ShippingPostalCode = '123465';
            billToAccount.EP_Puma_Company__c = company.Id;
            //billToAccount.CurrencyIsoCode = CURR_USD; //Added by bhushan 13-1-2017
            billToAccountList.add(billToAccount);
        }
        if(doInsert) {
            database.insert(billToAccountList);
        }
        return billToAccountList;
    }
    
    /**
       *  Helper method to Create Accounts with Basic Data. All method should call this method to create account with in this class.
       *  @name < createPriceBookForAccounts >
       *  @param  Number of pricebooks inserted.
       *  @return  list of pricebooks inserted.
       *  @throws NA
    */
    public static List<Pricebook2> createPriceBookForAccounts(integer cont) {
        list<Pricebook2> customPBList = new list<Pricebook2> ();
        list<PricebookEntry> customPBEntryList = new list<PricebookEntry> ();
        //create product and Price Book
        Company__c cmpObj = new Company__c();
        list<Company__c> cmpObjList = [select id from Company__c where EP_Company_Code__c = 'AUN1_Test' limit 1];
        if(cmpObjList.isEmpty()) {
            cmpObj = EP_TestDataUtility.createCompany('AUN1_Test'); 
            database.insert(cmpObj);  
        } else {
            cmpObj = cmpObjList[0];
        }
        
        product2 productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = TRUE,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,
                                                EP_Company_Lookup__c = cmpObj.id);
        database.insert(productPetrolObj);      
        //Insert Custom PriceBook and Product
        Pricebook2 customPB = new Pricebook2();
        for(Integer i = 0; i < cont; i++) {
            customPB = new Pricebook2(Name=EP_Common_Constant.STANDARD_PRICE_BOOK_NAME+(i+system.now().millisecond()+Math.round(Math.random()*1000000)), isActive=true,EP_Company__c = cmpObj.Id);
            customPBList.add(customPB);
        }
        database.insert(customPBList); 
        PricebookEntry pbEntryPetrol = new PricebookEntry();             
        for(Pricebook2 customPBObj : customPBList ) {
            pbEntryPetrol = new PricebookEntry(
            Pricebook2Id = customPBObj.Id,EP_Is_Sell_To_Assigned__c = true, Product2Id = productPetrolObj.Id,
            UnitPrice = 12000, IsActive = true);
            customPBEntryList.add(pbEntryPetrol);
        }
        database.insert(customPBEntryList); 
        
        return customPBList;
    }
    
    /**
       *  Helper method to Create Accounts with Basic Data. All method should call this method to create account with in this class.
       *  @name <createAccountsWithBasicData>
       *  @param Number of accounts which need to be create
       *  @return  list of inserted accounts
       *  @throws NA
    */
    public static List<Account> createAccountsWithBasicData(integer cont, Boolean doInsert) {
        List<Account> accountList = new List<Account>();
        Company__c companyObj = [select id from Company__c limit 1];
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        Database.insert(freightMatrix);
        Account accObj = new account();
        for(Integer i = 0; i < cont; i++) {
            accObj = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            accObj.Name = 'Test Account__#'+i ;
            accObj.EP_Puma_Company__c = companyObj.Id;
            accountList.add(accObj);
        }
        if(doInsert) {
            database.insert(accountList);
        }
        return accountList;
    }
    
    /**  This method gets the list of sellto locations for the accounts list.
       *  @name <getProductsforSellToAccounts>
       *  @param list of accounts.
       *  @return Map of account and Pricebookid.
       *  @throws NA
    */   
    public static map<id,list<id>> getProductsforSellToAccounts(list<Account> sellToAccounts) {
        //List<Account> accountList = new List<Account>();
        set<Id> priceBookIdSet = new set<id>();
        map<id,list<id>> sellTo_ProductIdMap = new map<id,list<id>>();
        map<id,list<id>> priceBook_PBookEntryMap = new map<id,list<id>>();
        map<id,id> pricebook_sellToMap = new map<id,id>();
        for(Account accObj : [select id, EP_PriceBook__c from Account where Id In : sellToAccounts]) {
            priceBookIdSet.add(accObj.EP_PriceBook__c);
            pricebook_sellToMap.put(accObj.id,accObj.EP_PriceBook__c);
        }
        list<Id> tempPriceBookIdSet = new list<id>();
        for(PricebookEntry pricebookEnt : [select id,Product2Id,Pricebook2Id from PricebookEntry where Pricebook2Id In : priceBookIdSet]) {
            tempPriceBookIdSet = new list<id>();
            if(priceBook_PBookEntryMap.containsKey(pricebookEnt.Pricebook2Id)) {
                tempPriceBookIdSet = sellTo_ProductIdMap.get(pricebookEnt.Pricebook2Id);
            }
            tempPriceBookIdSet.add(pricebookEnt.Product2Id);
            priceBook_PBookEntryMap.put(pricebookEnt.Pricebook2Id,tempPriceBookIdSet);
        }
        for(Id accId : pricebook_sellToMap.keySet()){
            if(priceBook_PBookEntryMap.containsKey(pricebook_sellToMap.get(accId))){
                sellTo_ProductIdMap.put(accId, priceBook_PBookEntryMap.get(pricebook_sellToMap.get(accId)));
            }
        }
        return sellTo_ProductIdMap;
    }
    
    /**  This method gets the list of sellto locations for the accounts list passed.
       *  @name <getSellToStorageLocations>
       *  @param List of accounts.
       *  @return Map of accountid and its associated stock holding location details.
       *  @throws NA
    */   
    public static map<id, list<EP_Stock_Holding_Location__c>> getSellToStorageLocations(List<Account> sellToAccountList) {
        map<id, list<EP_Stock_Holding_Location__c>> sellTo_StorageLocListMap = new map<id, list<EP_Stock_Holding_Location__c>>();
        for(Account accObj : [select id,(select id,Name,Stock_Holding_Location__c from Stock_Holding_Locations2__r) from Account where Id In : sellToAccountList] ) {
            sellTo_StorageLocListMap.put(accObj.id, accObj.Stock_Holding_Locations2__r);
        }
        return sellTo_StorageLocListMap;
    }
    
    /**  This method creates accounts of recordtype vendors.
       *  @name <createTestVendors>
       *  @param <Integer cont, String vendor_Type,boolean doInsert >
       *  @return accList
       *  @throws NA
    */   
    public static List<Account> createTestVendors(Integer cont, String vendor_Type,boolean doInsert ) {
        List<Account> accList = new List<Account>();
        final String COMPANY_CODE = 'AUN2';
        final string VENDOR_ACCOUNT_RT = 'Vendor';
        Account vendorAcc;
        
        Company__c company = EP_TestDataUtility.createCompany( COMPANY_CODE );
        Database.upsert(company);
        
        Id vendorRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(VENDOR_ACCOUNT_RT).getRecordTypeId();
        for(Integer i = 0; i < cont; i++) {
            vendorAcc = new Account( 
                                Name = 'Transporter '+i,
                                EP_Status__c = '05-Active', 
                                RecordTypeId = vendorRT, 
                                /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
                                EP_VendorType__c = vendor_Type,
                                EP_Source_Entity_ID__c =  ' NAVID_'+i+Math.round(Math.random()*1000),
                                /* TFS fix 45559,45560,45567,45568 end*/
                                EP_Puma_Company__c = company.Id,
                                BillingStreet = '123 st',
                                BillingCity = 'Gurgaon',
                                BillingState = 'Haryana',
                                BillingPostalCode = '123456',
                                BillingCountry = 'India',
                                Phone = '12345'
                            );
            accList.add(vendorAcc);
        }
        
        if(doInsert) {
            database.upsert(accList);
        }
        return accList;
    }
    
    /**Deprecated method Use getSObjectIdSet instead of getAccountIdSet **/
    /**  This method is use to create a set of Account Ids by a List of Account Object
       *  @name <getAccountIdSet>
       *  @param List of accounts.
       *  @return Set of Account Id.
       *  @throws NA
    */   
    public static Set<Id> getAccountIdSet(list<Account> accountList){
        set<Id> accountIdSet = new set<id>();
        for(Account accObj : accountList) {
            accountIdSet.add(accObj.id);
        }
        return accountIdSet;
    }
    
    /**  This method is use to create a set of Ids by a List of sObject Object
     *  @name <getSObjectIdSet>
     *  @param List of accounts.
     *  @return Set of Account Id.
     *  @throws NA
     */   
    public static Set<Id> getSObjectIdSet(list<SObject> sObjectList){
        set<Id> objIdSet = new set<id>();
        for(SObject sObj : sObjectList) {
            objIdSet.add(sObj.id);
        }
        return objIdSet;
    }
  
    /**  This method is use to create a multiple opportunities with Prospecting Stage for this given Account Id 
       *  @name <createOpportunitiesForAccount>
       *  @param Id of the account for which opportunity needs to be created.
       *  @param noOfOpps - Count the Number of opportunties which needs to created.
       *  @return doInsert - determine if Insert opration need to perform in this method for opportunties .
       *  @throws NA
    */ 
    public static list<Opportunity> createOpportunitiesForAccount(Id accountId, integer noOfOpps, boolean doInsert){
        Opportunity objOpp = new Opportunity();
        list<Opportunity> oppsList = new list<Opportunity>();
        for(integer i = 0; i < noOfOpps; i++) {
            //Insert Opportunity
            objOpp = new Opportunity();
            objOpp.name = 'Test'+i+accountId;
            objOpp.Stagename = 'Prospecting';
            objOpp.Accountid  = accountId;
            objOpp.EP_CRM_Credit_Approval_Received__c  =  true;
            objOpp.CloseDate = Date.Today() + 5;
            objOpp.Description  =  'Test Description';
            objOpp.Probability  =  5;
            objOpp.LeadSource  =  'Phone';
            objOpp.EP_CRM_Region__c  = 'AU';
            objOpp.EP_CRM_State__c  = 'ACT';
            oppsList.add(objOpp);
        }
        if(doInsert) {
            Database.insert(oppsList);
        }
        return oppsList;
    }
    
    /**  This method is use to create a Actions with ACT_NEW_STATUS Status for accounts 
       *  @name <createIncompleteCSRReviewAction>
       *  @param accList - List of Account Object for which Actions needs to be created.
       *  @param recordTypeName - Record Type Name of Action.
       *  @return doInsert - determine if Insert opration need to perform in this method for Action.
       *  @throws NA
    */ 
    public static list<EP_Action__c> createIncompleteCSRReviewAction(list<Account> accList, String recordTypeName, boolean doInsert) {
        Id accCSCreviewRTId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId(); 
        List<EP_Action__c> accActionList = new List<EP_Action__c>();
        EP_Action__c accAction;
        for(Account accObj :  accList) {
            accAction = new EP_Action__c ( EP_Action_Name__c = recordTypeName, EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS, RecordTypeId = accCSCreviewRTId ,EP_Account__c = accObj.Id);
            accActionList.add(accAction);
        }
        if(doInsert) {
            Database.insert(accActionList);
        }
        return accActionList;
    }
    /**  This method is use to create a Account of Storage Location record type
       *  @name <createStorageLocationAccount>
       *  @param accList - List of Account Object 
       *  @param recordTypeName - Record Type Name of Storage Location.
       *  @return list of Account
       *  @throws NA
    */ 
    public static List<Account> createStorageLocationAccount(Integer cont, Boolean doInsert){
        List<Account> aStorageLocList = new List<Account>();
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( 'Australia','AU' ,'Australia' );
        Database.insert(country,false);
        BusinessHours bhrs = [Select Name from BusinessHours where IsActive =true AND IsDefault = true limit 1]; 
        //Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);
        //Database.insert(testStorageLocation);       
        for(Integer i=0 ;i<cont; i++){
            aStorageLocList.add(EP_TestDataUtility.CreateStorageLocAccount(country.id,bhrs.id));
        }
        if(doInsert){
             database.insert(aStorageLocList);
        }
       return aStorageLocList;
    }
    
    /**  This method will populate VMI ship to account. 
       *  @name   <populateVMIShipToObject>
       *  @param  List of Accounts and boolean doInsert flag.
       *  @return Accounts list.
       *  @throws NA
    */
    public static List<Account> populateVMIShipToObject(list<Account> sellToAccounts, Boolean doInsert ) {
        list<Account> vMIshipToList = new list<Account>();
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( 'Australia','AU' ,'Australia' );
        Database.insert(country,false);
        Account vmiShipToAccount = new Account();
        for(Account sellToAccount : sellToAccounts){
            vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
            vmiShipToAccount.EP_Transportation_Management__c = 'Puma Planned';
            vmiShipToAccount.EP_Eligible_for_Rebate__c = false;
            vmiShipToAccount.EP_Ship_To_Type__c = EP_Common_Constant.NON_CONSIGNMENT;
            vmiShipToAccount.EP_Country__c = country.id;
            vmiShipToAccount.EP_Ship_To_UTC_Offset__c = 5.50;
            vmiShipToAccount.EP_Ship_To_UTC_Timezone__c = 'UTC+11:00';
            vMIshipToList.add(vmiShipToAccount);
        }
        if(doInsert){
            database.insert(vMIshipToList);
        }
        return vMIshipToList;
    }
    
    /**  This method will create test data account with status as Basic Data Setup.
       *  @name <createBillToAccountWithBasicDataSetup>
       *  @param List of Accounts and boolean doUpdate flag.
       *  @return updated Accounts list.
       *  @throws NA
    */
    public static List<Account> createBillToAccountWithBasicDataSetup(integer cont, boolean doUpdate) {
        List<Account> billToList = new list<Account>();
        for(Account billToObj : createBillToAccountWithProspectStatus(cont, true) ) {
            billToObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            billToList.add(billToObj);
        }
        
        if(doUpdate){
            database.update(billToList);
        }
        return billToList;
    }
    
    /** This method will create bill to account with Status as Active 
       *  @name <createBillToAccountWithActive>
       *  @param List of Accounts and boolean doUpdate flag.
       *  @return updated Accounts list.
       *  @throws NA
    */
    public static List<Account> createBillToAccountWithActive(integer cont, boolean doUpdate) {
        List<Account> billToList = new list<Account>();
        for(Account billToObj : createBillToAccountWithBasicDataSetup(cont, true) ) {
            billToObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            billToList.add(billToObj);
        }
        
        if(doUpdate){
            database.update(billToList);
        }
        return billToList;
    }
}