/**
  @Author <Pravindra Khatri>
   @name <EP_validationon_Tankdiptest>
   @CreateDate <09/12/2015>
   @Description <This test class is used to cover the validation rule "EP_VAL005_Newer_Tank_Dip_For_Same_Day">
   @Version <1.0>
  
*/
@isTest
private class EP_validationon_Tankdiptest {
    /*
    This method is used to insert user with profile "Puma CSC Manager_R1"
    */
    static testMethod void EP_profiletest1(){
            Profile profile1 = [Select Id from Profile where name = 'Puma CSC Manager_R1'];
            User portalAccountOwner1 =  new User(Alias = 'batman',ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',
                                            Email='bruce.wayne@wayneenterprises.com',EmailEncodingKey='UTF-8',Firstname='Bruce',Lastname='Wayne',LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago');
            Database.insert(portalAccountOwner1);

        System.runAs ( portalAccountOwner1 ) {
            
            EP_Payment_Term__c pay =  new EP_Payment_Term__c();
            pay.name = 'Prepayment';
            insert pay;
            
            recordType rcc = [Select id,name from RecordType where sObjectType='Account' and developerName= 'EP_VMI_SHIP_TO' limit 1];

            
            account accobj = new account();
                accObj.EP_Ship_To_UTC_Timezone__c = 'UTC+11:00';
                accobj.EP_Tank_Dips_Schedule_Time__c='11:00';
                accobj.EP_Tank_Dip_Entry_Mode__c = 'Portal dip entry';
                accobj.recordtypeid = rcc.id;
                accobj.name = 'account2';
                accobj.EP_Status__c = '03-Active';
                accObj.BillingCity = 'testCity';
                accObj.BillingStreet = 'testStreet';
                accObj.BillingState = 'testState';
                accObj.BillingCountry = 'testCountry';
                accObj.BiLlingPostalCode = '123456';
                accObj.ShippingCity = 'ShipCity';
                accObj.ShippingStreet = 'ShipStreet';
                accObj.ShippingState = 'ShipState';
                accObj.ShippingCountry = 'ShipCountry';
                accObj.ShippingPostalCode = '123465';
                database.insert(accobj,false);

            EP_Tank__c tankObj = new EP_Tank__c (EP_Ship_To__c=accobj.id,EP_Deadstock__c=20,EP_Tank_Status__c='Operational',EP_Safe_Fill_Level__c=50,CurrencyIsoCode='AUD');
            database.insert(tankobj,false);
            
            String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Actual').getRecordTypeId();
            EP_Tank_Dip__c tankDipObj = new EP_Tank_Dip__c(EP_Tank__c=tankObj.id,EP_Tank_Dip_Reading_Date_Time_Error__c = true, RecordTypeId = recType,
                                    EP_Unit_Of_Measure__c='LT',EP_Ambient_Quantity__c=40,
                                    EP_Reading_Date_Time__c=System.today());
            database.insert(tankDipObj,false) ;

                                    
        }
    
    }
    
    /*
    This method is used to insert user with profile "Puma CSC Agent_R1"
    */
    static testMethod void EP_profiletest2(){
        Profile profile2 = [Select Id from Profile where name = 'Puma CSC Agent_R1'];
        User portalAccountOwner1 = new User(Alias = 'batman',ProfileId = profile2.Id,Username = System.now().millisecond() + 'test2@test.com',
                                            Email='bruce.wayne@wayneenterprises.com',EmailEncodingKey='UTF-8',Firstname='Bruce',Lastname='Wayne',LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago');
        Database.insert(portalAccountOwner1);

        System.runAs ( portalAccountOwner1 ) {
            //Create account record
            EP_Payment_Term__c pay =  new EP_Payment_Term__c();
            pay.name = 'Prepayment';
            insert pay;
            
            recordType rcc = [Select id,name from RecordType where sObjectType='Account' and developerName= 'EP_VMI_SHIP_TO' limit 1];

            
            account accobj = new account();
            accObj.EP_Ship_To_UTC_Timezone__c = 'UTC+11:00';
               accobj.EP_Tank_Dips_Schedule_Time__c='11:00';
               accobj.EP_Tank_Dip_Entry_Mode__c = 'Portal dip entry';
                accobj.name = 'account2';
                accObj.BillingCity = 'testCity';
                accObj.BillingStreet = 'testStreet';
                accObj.BillingState = 'testState';
                accObj.BillingCountry = 'testCountry';
                accobj.EP_Status__c = '03-Active';
                accObj.BiLlingPostalCode = '123456';
                accObj.ShippingCity = 'ShipCity';
                accObj.ShippingStreet = 'ShipStreet';
                accObj.ShippingState = 'ShipState';
                accObj.ShippingCountry = 'ShipCountry';
                accObj.ShippingPostalCode = '123465';
                database.insert(accobj,false);

            EP_Tank__c tankObj = new EP_Tank__c (EP_Ship_To__c=accobj.id,EP_Deadstock__c=20,EP_Tank_Status__c='Operational',EP_Safe_Fill_Level__c=50,CurrencyIsoCode='AUD');
            database.insert(tankobj,false);
            String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Actual').getRecordTypeId();
            EP_Tank_Dip__c tankDipObj = new EP_Tank_Dip__c(EP_Tank__c=tankObj.id,EP_Tank_Dip_Reading_Date_Time_Error__c = true, RecordTypeId = recType,
                                    EP_Unit_Of_Measure__c='LT',EP_Ambient_Quantity__c=40,
                                    EP_Reading_Date_Time__c=System.today());
            database.insert(tankDipObj,false) ;


                                        
    }
   
}
/*
Initailize method
*/    
private static void init(){
     Account accObj; 
     List<EP_Tank_Dip__c> tankListDips; 
     EP_Tank__c TankInstance; 
   
     EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(1);
     accObj=customHierarchy.lShipToAccounts[0];
     TankInstance=customHierarchy.lTanks[0];
     tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
    }
    /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testDatePageRedirect(){ 
     init();   
     EP_Tank__c TankInstance; 
     Account accObj; 
     List<EP_Tank_Dip__c> tankListDips; 
     String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
     Test.startTest();
     EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(1);
     accObj=customHierarchy.lShipToAccounts[0];
     TankInstance=customHierarchy.lTanks[0];
     tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
     Account acc =[Select id from account where id=:accObj.id];
     acc.EP_Status__c ='Active';
     database.update(acc,false);
     System.debug('test class list size'+tankListDips.size());
     tankListDips[0].EP_Reading_Date_Time__c=System.today()-4;//Updating tank date as today minus 4days
     tankListDips[0].RecordTypeId = recType;
     database.update(tankListDips[0],false);    
  
}

}