/** 
   * @Author <Accenture>
   * @name <EP_NAVOrderNewHandler>
   * @CreateDate <02/8/2017>
   * @Description <This is the handler class to create/update Orders and orderLine Items for Rest service from NAV > 
   * @Version <1.0>
*/
public with sharing class EP_NAVOrderNewHandler extends EP_InboundHandler {
    @testVisible private static List <EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    private static final String METHOD_NAME = 'processRequest';
    private static final String CLASS_NAME = 'EP_NAVOrderNewHandler';
    private static final String CS_INBOUND_MESSAGE = 'NAV_TO_SFDC_ORDER_SYNC';
    private static boolean processingFailed = false;
    private static string failureReason;
    
    /**
    * @Description : This method will be called from global web service class 'EP_OrderCreationUpdationWithNav' to handled the rest service request.
    * @Date :02/9/2017
    * @Name :processRequest
    * @param: requestBody : JSON String
    * @return: jsonResponse : Response Body
    */
    public override String processRequest(String requestBody){
        EP_GeneralUtility.Log('Public','EP_NAVOrderNewHandler','processRequest');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        try {
            EP_NAVOrderNewStub stub = (EP_NAVOrderNewStub)  System.JSON.deserialize(requestBody, EP_NAVOrderNewStub.class);
            system.debug('pep ' + stub);
            headerCommon = stub.MSG.HeaderCommon;
            EP_NAVOrderNewStub.orderWrapper orderWrapper = stub.MSG.Payload.Any0.Order;         
            EP_NAVOrderNewHelper orderNewHelper = new EP_NAVOrderNewHelper();
            system.debug('pep ' + orderWrapper);
            orderNewHelper.setOrderAttributes(orderWrapper);
            system.debug('**orderWrapper-setOrderAttributes**' + orderWrapper);
            processOrder(orderWrapper);
            
            orderNewHelper.setOrderItemAttributes(orderWrapper);
            system.debug('**setOrderItemAttributes**' + orderWrapper);
            processOrderLineItems(orderWrapper);

            orderNewHelper.setInvoiceItemsAttributes(orderWrapper);
            system.debug('**setInvoiceItemsAttributes**' + orderWrapper);
            processInvoiceLineItems(orderWrapper);
            
            //Send Request for Order Sync to NAV
            orderNewHelper.sendOrderSyncRequest(orderWrapper.sfOrder.id);
        } catch (exception ex ) {
            processingFailed = true;
            failureReason = ex.getMessage();
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), 
                                                        EP_Common_constant.EPUMA, METHOD_NAME, CLASS_NAME, 
                                                        EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, 
                                                        EP_Common_Constant.BLANK);
            ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.ORDER_STRING, null, ex.getTypeName(), ex.getMessage()) );
        }

        string jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(CS_INBOUND_MESSAGE, processingFailed, failureReason, headerCommon, ackResponseList);
        return jsonResponse;
    }
    
    /**
    * @Description: This class will update or insert the order and its order items which is coming in JSON string
    * @Date :02/9/2017
    * @Name :manageOrder
    * @param: orderMessage - A instance of EP_NAVOrderNewStub class
    * @return: NA
    */
    @TestVisible
    private static void processOrder(EP_NAVOrderNewStub.orderWrapper orderWrapper){
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHandler','manageOrder');
        list<order> orderList = new list<order>();
        system.debug('orderinsert ' + orderWrapper.sfOrder);
        Database.Upsert(orderWrapper.sfOrder,true);
        createResponse(EP_Common_Constant.ORDER_STRING, orderWrapper.sfOrder.EP_SeqId__c, null , null);
    }
    /**
    * @Description: This method will be used to process the order items coming in JSON string
    * @Date :02/9/2017
    * @Name :processOrderLineItems
    * @param: orderMapperObj - A instance of EP_NAVOrderNewStubTransform class
    * @return: NA
    */
    @TestVisible
    private static void processOrderLineItems(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHandler','processOrderLineItems');
        list<csord__Order_Line_Item__c> orderItemsToBeProcess  = new list<csord__Order_Line_Item__c>();
        
        for(EP_NAVOrderNewStub.OrderLine ordLine : orderWrapper.orderLines.OrderLine){
            system.debug('**ordLine' + ordLine);
            orderItemsToBeProcess.add(ordLine.orderLineItem);
            //invoiceItemList.addAll(getInvoiceItems(ordLine));
        }        
        upsertOrderLineItems(orderItemsToBeProcess);
        
        //return invoiceItemList;
    }
    @TestVisible
    private static list<csord__Order_Line_Item__c> getInvoiceItems(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        list<csord__Order_Line_Item__c> invoiceItemList = new list<csord__Order_Line_Item__c>();
        for(EP_NAVOrderNewStub.OrderLine ordLine : orderWrapper.orderLines.OrderLine){
            //Defect -58835--Start
            if(ordLine.lineComponents.invoiceComponents != null) {
                for(EP_NAVOrderNewStub.InvoiceComponent ordItem: ordLine.lineComponents.invoiceComponents.invoiceComponent) {
                    invoiceItemList.add(ordItem.invoiceItem);
                }
            }
            //Defect -58835--End
         }
        return invoiceItemList;
    }
    /**
    * @Description: This method upserts processed OrderLineItems and process the DML errors if upsert fails
    * @Date :02/9/2017
    * @Name :upsertOrderLineItems
    * @param: orderItemsToBeProcess : list of order items
    * @return: map<String, map<String, String>> - Map of orderItem SeqId with errors details
    */
    @TestVisible
    private static void upsertOrderLineItems(list<csord__Order_Line_Item__c> orderItemsToBeProcess) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHandler','upsertOrderLineItems');
        if(!orderItemsToBeProcess.isEmpty()) {         
            list<Database.UpsertResult> ordersItemsUpsertResult = Database.upsert(orderItemsToBeProcess,false);
            for(integer counter = 0 ; counter < ordersItemsUpsertResult.size(); counter++){           
                if(!ordersItemsUpsertResult[counter].isSuccess()){
                    processUpsertErrors(EP_Common_Constant.ORDERLINE_STRING, ordersItemsUpsertResult[counter].getErrors(), orderItemsToBeProcess[counter].EP_SeqId__c);
                } else {
                    createResponse(EP_Common_Constant.ORDERLINE_STRING, orderItemsToBeProcess[counter].EP_SeqId__c,null , null);
                }
            }
        }
    }

    /**
    * @Description: Method to do upsert invoice item and process the failer results if any
    * @Date :02/9/2017
    * @Name :processRequest
    * @param: list-  List of order item to do upsert
    * @return: map<String, map<String, String>> return the map of parant of invoice item Id which is a order Item id and its error code and description 
    */
    @TestVisible
    private static void processInvoiceLineItems(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHandler','upsertInvoiceLineItems');
        list<csord__Order_Line_Item__c> orderItemsToBeProcess = getInvoiceItems(orderWrapper);
        if(!orderItemsToBeProcess.isEmpty()) {           
            list<Database.UpsertResult> ordersItemsUpsertResult = Database.upsert(orderItemsToBeProcess,false);
            for(integer counter = 0 ; counter < ordersItemsUpsertResult.size(); counter++){        
                if(!ordersItemsUpsertResult[counter].isSuccess()){
                    processUpsertErrors(EP_Common_Constant.ORDERLINE_STRING, ordersItemsUpsertResult[counter].getErrors(), orderItemsToBeProcess[counter].EP_Parent_Order_Line_Item__c);
                }
            }
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string ResponseType, string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private',CLASS_NAME,'createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(ResponseType, seqId, errorCode, errorDescription));
    }
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(string ResponseType , list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            createResponse(ResponseType, seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
}