/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationProspectToBasicData>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 01-Prospect to 02-BasicDataSetup>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationProspectToBasicData extends EP_AccountStateTransition {

    public EP_ASTStorageLocationProspectToBasicData () {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationProspectToBasicData',' isGuardCondition');        
        return true;
    }
}