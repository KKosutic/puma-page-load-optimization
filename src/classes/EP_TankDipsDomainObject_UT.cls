@isTest
public class EP_TankDipsDomainObject_UT
{
    static testMethod void insertPlaceHolderTankDips_test(){
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectPositiveScenario();
        EP_TankDipsDomainObject localObj = new EP_TankDipsDomainObject();
        Test.startTest();
        localObj.insertPlaceHolderTankDips(obj.localAccount);
        Test.stopTest();
        EP_Tank__c tank = [SELECT Id,EP_Tank_Status__c,EP_Last_Place_Dip_Reading_Date_Time__c,EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c FROM EP_Tank__c LIMIT 1];
        List<EP_Tank_Dip__c> tankDips = [SELECT Id FROM EP_Tank_Dip__c];   
        System.assert(tankDips.size() > 0);
    }
    static testMethod void deletePlaceHolderTankDips_test(){
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectPositiveScenario();
        EP_TankDipsDomainObject localObj = new EP_TankDipsDomainObject();
        Test.startTest();
        localObj.deletePlaceHolderTankDips(obj.getAccount());
        Test.stopTest();
        List<EP_Tank__c > listTanks = [SELECT Id ,EP_Ship_To__c FROM EP_Tank__c];
        System.assertNotEquals(0,listTanks.size());
    }
}