/**
 * @author <Sandeep Kumar>
 * @name <EP_ActionTriggerHelperTest>
 * @createDate <06/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_ActionTriggerHelperTest {
    
    /*********************************************************************************************
    *@Description : This method valids restrictActionOwnerChange method of EP_ActionTriggerHelper class
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testRestrictActionOwnerChange1(){ 
        Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
        insert acc;
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        List<User> userList = [Select id from user where isActive = true and profileId in :profiles limit 2 ];
        EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = acc.Id;
        action.ownerId = userList[0].Id;
        //action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS.toUpperCase();
        insert action;
        
        //action.EP_Status__c = EP_Common_Constant.ACT_APPROVED_STATUS.toUpperCase();
        action.ownerId = userList[1].Id;
        try {
            update action;
        } catch (Exception e) {
            system.assertEquals(true, e.getMessage().contains(Label.EP_Cmpltd_RVw_Asgn_Err+EP_Common_Constant.SPACE));
        }
        
        
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;
        EP_Action__c action2 = new EP_Action__c();
        action2.EP_Account__c = acc.Id;
        action2.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS.toUpperCase();
        List<QueueSobject> queueObjects = [Select QueueId from QueueSobject where SobjectType = 'Action' limit 1];
        if(queueObjects.size() > 0) {
            List<Group> queList = [Select Id from Group where type = 'Queue' and id = :queueObjects[0].Id limit 1];
            action2.EP_System_Queue__c = queList[0].Id;
        }       
        
        action2.ownerId = userList[0].Id;        
        insert action2;
        EP_Action__c insertedAction = [Select Id, OwnerId from EP_Action__c 
                                     where id =: action2.Id];
        action2.ownerId = userList[1].Id;
        
        update action2;      
        EP_Action__c updatedAction = [Select Id, OwnerId from EP_Action__c 
                                    where id =: action2.Id];
        system.assertNotEquals(insertedAction.OwnerId, updatedAction.OwnerId);
        
    }
        
}