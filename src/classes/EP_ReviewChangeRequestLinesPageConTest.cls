/* 
  @Author <Spiros Markantonatos>
  @name <EP_ReviewChangeRequestLinesPageConTest>
  @CreateDate <07/09/2016>
  @Description <This is the test class for the EP_ReviewChangeRequestLinesPageExt controler>
  @Version <1.0>
*/
@isTest
public with sharing class EP_ReviewChangeRequestLinesPageConTest {
    
    private static Account testNonVmiShipToAccount;
    private static Account testSellToAccount;
    private static Product2 testProduct;
    private static Contact testContact;
    private static EP_Tank__C testTank;
    private static EP_ChangeRequest__c testChangeRequest;
    private static EP_ChangeRequestLine__c testChangeRequestLine1;
    private static EP_ChangeRequestLine__c testChangeRequestLine2;
    private static EP_Action__c testAction;
    private static User testCSCUser;
    private static User testCSC2User;
    
    private static final String OPEN_CHANGE_REQUEST_STATUS = 'Open';
    private static final String OPEN_CHANGE_REQUEST_LINE_STATUS = 'Open';
    private static final String COMPLETED_CHANGE_REQUEST_STATUS = 'Completed';
    private static final String APPROVED_CHANGE_REQUEST_LINE_STATUS = 'Approved';
    private static final String REJECTED_CHANGE_REQUEST_LINE_STATUS = 'Rejected';
    
    private static void createTestCRData() {
        
        // Create test CSC user
        Profile testCSCProfile = [SELECT ID FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE];
        testCSCUser = EP_TestDataUtility.createUser(testCSCProfile.Id);
        Database.insert(testCSCUser);
        
        testCSC2User = EP_TestDataUtility.createUser(testCSCProfile.Id);
        Database.insert(testCSC2User);
        
        EP_ExceptionEmailTo__c exEm = new EP_ExceptionEmailTo__c(SetupOwnerId=UserInfo.getOrganizationId(), Email__c = 'test@xyz.com');
        Database.insert(exEm);
        
        System.RunAs(testCSCUser) {
            testSellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            Database.insert(testSellToAccount, false);
            testContact = EP_TestDataUtility.createTestRecordsForContact(testSellToAccount);
               
            Account testBillToAccount = EP_TestDataUtility.createBillToAccount();
            Database.insert(testBillToAccount);
            
            testSellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
            testSellToAccount.EP_Bill_To_Account__c = testBillToAccount.Id;   
            Database.update(testSellToAccount, FALSE); 
            
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
            testNonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(testSellToAccount.Id, strRecordTypeId);
            testNonVmiShipToAccount.EP_Delivery_Type__c = 'Ex-Rack';
            testNonVmiShipToAccount.EP_Pumps__c= 10;
            Database.insert(testNonVmiShipToAccount);
            
            testAction = new EP_Action__c();
            testAction.EP_Account__c = testSellToAccount.Id;
            testAction.EP_Action_Name__c = 'CSC Review';
            testAction.EP_Status__c = '01-New';
            Database.insert(testAction);
            
            // Create test CR record
            testChangeRequest = new EP_ChangeRequest__c();
            testChangeRequest.EP_Account__c = testSellToAccount.Id;
            testChangeRequest.EP_Object_Type__c = 'Account';
            testChangeRequest.EP_Operation_Type__c = 'UPDATE';
            testChangeRequest.EP_Request_Status__c = OPEN_CHANGE_REQUEST_STATUS;
            testChangeRequest.EP_Request_Date__c = System.Now();
            testChangeRequest.EP_Requestor__c = UserInfo.getUserId();
            Database.insert(testChangeRequest);
            
            // Create test CR line records
            testChangeRequestLine1 = new EP_ChangeRequestLine__c();
            testChangeRequestLine1.EP_Change_Request__c = testChangeRequest.Id;
            testChangeRequestLine1.EP_New_Value__c = 'NEW VALUE 1';
            testChangeRequestLine1.EP_Original_Value__c = 'OLD VALUE 1';
            testChangeRequestLine1.EP_Review_Step__c = 'CSC Review';
            testChangeRequestLine1.EP_Request_Status__c = OPEN_CHANGE_REQUEST_LINE_STATUS;
            testChangeRequestLine1.EP_Action__c = testAction.Id;
            Database.insert(testChangeRequestLine1);
            
            testChangeRequestLine2 = new EP_ChangeRequestLine__c();
            testChangeRequestLine2.EP_Change_Request__c = testChangeRequest.Id;
            testChangeRequestLine2.EP_New_Value__c = 'NEW VALUE 2';
            testChangeRequestLine2.EP_Original_Value__c = 'OLD VALUE 2';
            testChangeRequestLine2.EP_Review_Step__c = 'CSC Review';
            testChangeRequestLine2.EP_Request_Status__c = OPEN_CHANGE_REQUEST_LINE_STATUS;
            testChangeRequestLine2.EP_Action__c = testAction.Id;
            Database.insert(testChangeRequestLine2);
        }
    }
    
    static testMethod void testMassRejectRequestUpdate() {
        createTestCRData();
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                
                PageReference pageRef = Page.EP_ReviewChangeRequestLinesPage;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id', String.valueOf(testChangeRequest.Id));
                
                EP_ReviewChangeRequestLinesPageControler con = new EP_ReviewChangeRequestLinesPageControler();
                
                System.assertNotEquals(NULL, con.genericChangeRequestLineRecordType);
                System.assertNotEquals(NULL, con.productChangeRequestLineRecordType);
                
                // Retrieve all CR lines
                con.retrieveAllChangeRequestLines();
                
                // Test that the controller has retrieved 2 CR Lines
                System.assertEquals(2, con.lChangeRequestLines.size());
                
                // The cancel method should return a URL pointing to the 
                System.assertEquals(TRUE, con.cancel().getURL().contains(testChangeRequest.Id));
                
                // Try to reject one CR line without specifying the reason for rejection
                con.lChangeRequestLines[0].EP_Request_Status__c = REJECTED_CHANGE_REQUEST_LINE_STATUS;
                PageReference ref = con.updateChangeRequestLines();
                System.assertEquals(NULL, ref);
                
                con.lChangeRequestLines[0].EP_Reason_For_Rejection__c = 'TEST 1';
                con.lChangeRequestLines[1].EP_Reason_For_Rejection__c = 'TEST 2';
                
                ref = con.rejectAllChangeRequestLines();
                
                // The CR lines should be rejected this time
                System.assertEquals(TRUE, con.cancel().getURL().contains(testChangeRequest.Id));
                
                // The CR record should be flagged as "Completed"
                // Remove this part of the logic as we the method updating the CR must be changed
                //testChangeRequest = [SELECT ID,EP_Request_Status__c FROM EP_ChangeRequest__c WHERE ID = :testChangeRequest.Id];
                //System.assertEquals(COMPLETED_CHANGE_REQUEST_STATUS, testChangeRequest.EP_Request_Status__c);
            }
        Test.stopTest();
    }
    
    static testMethod void testMassAcceptRequestUpdate() {
        createTestCRData();
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                PageReference pageRef = Page.EP_ReviewChangeRequestLinesPage;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id', String.valueOf(testChangeRequest.Id));
                
                EP_ReviewChangeRequestLinesPageControler con = new EP_ReviewChangeRequestLinesPageControler();
                
                // Retrieve all CR lines
                con.retrieveAllChangeRequestLines();
                
                System.assertEquals(2, con.lChangeRequestLines.size());
                
                // Approve only 1 CR Line
                con.lChangeRequestLines[0].EP_Request_Status__c = APPROVED_CHANGE_REQUEST_LINE_STATUS;
                PageReference ref = con.updateChangeRequestLines();
                
                // The approval should go through
                System.assertEquals(TRUE, con.cancel().getURL().contains(testChangeRequest.Id));
                
                // But the CR record must remain open as there is at list one CR line open
                testChangeRequest = [SELECT ID,EP_Request_Status__c FROM EP_ChangeRequest__c WHERE ID = :testChangeRequest.Id];
                System.assertEquals(OPEN_CHANGE_REQUEST_STATUS, testChangeRequest.EP_Request_Status__c);
                
                
                // Then approve all remaining CR lines
                con.retrieveAllChangeRequestLines();
                
                // There should be 1 open line remaining
                System.assertEquals(1, con.lChangeRequestLines.size());
                
                ref = con.approveAllChangeRequestLines();
                
                // The CR lines should be rejected this time
                System.assertEquals(TRUE, con.cancel().getURL().contains(testChangeRequest.Id));
                
                // The CR record should be flagged as "Completed"
                // Remove this part of the logic as we the method updating the CR must be changed
                //testChangeRequest = [SELECT ID,EP_Request_Status__c FROM EP_ChangeRequest__c WHERE ID = :testChangeRequest.Id];
                //System.assertEquals(COMPLETED_CHANGE_REQUEST_STATUS, testChangeRequest.EP_Request_Status__c);
            }
        Test.stopTest();
    }
    
    static testMethod void testActionOwnershipChange() {
        
        createTestCRData();
        
        // Create test system admin user
        Profile testSystemAdminProfile = [SELECT ID FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        User testSystemAdminUser = EP_TestDataUtility.createUser(testSystemAdminProfile .Id);
        Database.insert(testSystemAdminUser);
        
        Test.startTest();
            System.RunAs(testSystemAdminUser) {
                // Change action ownership
                testAction.OwnerId = testCSC2User.Id;
                Database.update(testAction);
                
                PageReference pageRef = Page.EP_ReviewChangeRequestLinesPage;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id', String.valueOf(testAction.Id));
                
                EP_ReviewChangeRequestLinesPageControler con = new EP_ReviewChangeRequestLinesPageControler();
                
                // Retrieve all CR lines
                con.retrieveAllChangeRequestLines();
                
                // Change the action ownership
                con.strSelectedActionID = testAction.Id;
                con.assignActionToRunningUser();
                testAction = [SELECT Id, OwnerId FROM EP_Action__c WHERE Id = :testAction.Id];
                System.assertEquals(UserInfo.getUserId(), testAction.OwnerId);
            }
        Test.stopTest();
        
    }
    
     static testMethod void testMassActionReview() {
        createTestCRData();
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                PageReference pageRef = Page.EP_ReviewChangeRequestLinesPage;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id', String.valueOf(testAction.Id));
                
                EP_ReviewChangeRequestLinesPageControler con = new EP_ReviewChangeRequestLinesPageControler();
                
                // Retrieve all CR lines
                con.retrieveAllChangeRequestLines();
                
                // Test that the controller has retrieved 2 CR Lines assigned to this action
                System.assertEquals(2, con.lChangeRequestLines.size());
                
                // The cancel method should return a URL pointing to the 
                System.assertEquals(TRUE, con.cancel().getURL().contains(testAction.Id));
            }
        Test.stopTest();
    }
    
}