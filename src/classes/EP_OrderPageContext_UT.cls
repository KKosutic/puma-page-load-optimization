@isTest
public class EP_OrderPageContext_UT
{
    static testMethod void isRoOrder_PositiveTest() {
        String strid = EP_TestDataUtility.getRetrospectivePositiveScenario().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        System.AssertEquals(True,localObj.isRoOrder);
    }
    static testMethod void isRoOrderNegativeTest() {
        String strid = EP_TestDataUtility.getCurrentOrderNegativeTestScenario().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        System.AssertEquals(false,localObj.isRoOrder);
    }
    static testMethod void getSupplyLocPricing_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        LIST<SelectOption> result = localObj.getSupplyLocPricing();
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getTransporterPricingList_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        LIST<SelectOption> result = localObj.getTransporterPricingList();
        Test.stopTest();
        System.Assert(result.size()> 0);
    }
    
    static testMethod void showDeliveryTypeSelector_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.intOrderCreationStepIndex  = 1;        
        Test.startTest();
        Boolean result = localObj.showDeliveryTypeSelector;        
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void showSupplyLocationSelector_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.showSupplyLocationSelector = true;
        Test.startTest();
        Boolean result = localObj.showSupplyLocationSelector;
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    static testMethod void isPrepaymentCustomer_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
        Test.startTest();
        Boolean result = localObj.isPrepaymentCustomer;
        Test.stopTest();
        System.AssertEquals(True, result);
    }
    
    static testMethod void isExRack_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.strSelecteddeliveryType = EP_Common_Constant.EX_RACK;
        Test.startTest();
        Boolean result = localObj.isExRack;
        Test.stopTest();
        System.AssertEquals(True,result);
    }
    
    static testMethod void isBulkOrder_PositiveTest() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.strSelecteddeliveryType = EP_Common_Constant.EX_RACK;
        Test.startTest();
        Boolean result = localObj.isBulkOrder;
        Test.stopTest();
        System.AssertEquals(True,result);
    }
    static testMethod void isBulkOrder_NegativeTest() {
        String strid = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.strSelecteddeliveryType = EP_Common_Constant.EX_RACK;
        Test.startTest();
        Boolean result = localObj.isBulkOrder;
        Test.stopTest();
        System.AssertEquals(True,result);
    }
    
    static testMethod void showShipToSelector_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.showShipToSelector = true;
        Test.startTest();
        Boolean result = localObj.showShipToSelector;
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    static testMethod void showOrderEntryLocationSelector_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.showOrderEntryLocationSelector = true;
        Test.startTest();
        Boolean result = localObj.showOrderEntryLocationSelector;
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isDeliveryOrder_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.showOrderEntryLocationSelector = true;
        Test.startTest();
        Boolean result = localObj.isDeliveryOrder;
        Test.stopTest();
        System.AssertEquals(true, result);
    }
    
    static testMethod void showOrderSummarySelector_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.showOrderSummarySelector = true;
        Test.startTest();
        Boolean result = localObj.showOrderSummarySelector;
        Test.stopTest();
        System.AssertEquals(result, false);
    }
    static testMethod void showSubmitButton_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.showSubmitButton;
        Test.stopTest();
        System.AssertEquals(result, false);
    }
    static testMethod void isOrderEntryValid_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid;
        System.AssertEquals(false,result);
        Test.stopTest();
    }
    static testMethod void userCurrencyFormat_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        String result = localObj.userCurrencyFormat;
        Test.stopTest();
        System.AssertNotEquals(result, 'mm/dd/yyyy');
    }
    
    static testMethod void isPrepaymentOrde_PositiveTest() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isPrepaymentOrder;
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isPrepaymentOrde_NegativeTest() {
        String strid = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isPrepaymentOrder;
        Test.stopTest();
        System.AssertEquals(false,localObj.isPrepaymentOrder);
    }
    
    static testMethod void totalOrderCost_PositiveTest() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Double result = localObj.totalOrderCost;
        Test.stopTest();
        System.AssertEquals(0,result);
    }
    static testMethod void totalOrderCost_NegativeTest() {
        String strid = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Double result = localObj.totalOrderCost;
        Test.stopTest();
        System.AssertEquals(false, result > 0 );
    }
    
    static testMethod void isErrorInPage_PositiveTest() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isErrorInPage;
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isErrorInPage_NegativeTest() {
        String strid = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isErrorInPage;
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    static testMethod void listDeliveryTypeSelectOptions_Test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Integer result = localObj.listDeliveryTypeSelectOptions.size();
        System.AssertEquals(true,result > 0);
        Test.stopTest();
    }    
    static testMethod void listPaymentTermsOptions_Test() {
        String strid = EP_TestDataUtility.getOrderStateInvoicedDomainObject().getOrder().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);        
        Test.startTest();
        localObj.onLoadActionForNewOrder();
        Test.stopTest();
        System.AssertEquals(true,localObj.listPaymentTermsOptions.size() > 0);
    }
    static testMethod void onLoadActionForNewOrder_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        localObj.onLoadActionForNewOrder();
        Test.stopTest();
        System.AssertEquals(true,localObj.isAccountFound );
        System.AssertEquals(localObj.orderAccount.CurrencyIsoCode,localObj.newOrderRecord.CurrencyIsoCode );
    }
    static testMethod void setWizardIndexonBack_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getConsumptionOrder();
        String strid = orderObj.AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.intOrderCreationStepIndex = 4;
        localObj.newOrderRecord.id = orderObj.Id;
        Test.startTest();
        localObj.setWizardIndexonBack();
        Test.stopTest();
        List<OrderItem> listOrderItem = [SELECT Id , OrderId  FROM OrderItem WHERE OrderId =: strid];
        System.AssertEquals(true,listOrderItem.size() == 0);
    }
    static testMethod void isShowExportedCheckBoxAllowed_PositiveScenariotest() {
        csord__Order__c orderObj =  EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        String strid = orderObj.AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        localObj.newOrderRecord.EP_Use_Managed_Transport_Services__c = EP_Common_Constant.STRING_TRUE;
        localObj.newOrderRecord.EP_3rd_Party_Managed_Exported__c = True;

        localObj.profileName = EP_Common_Constant.CSC_MANAGER_PROFILE;
        Test.startTest();
        Boolean result = localObj.isShowExportedCheckBoxAllowed();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isShowExportedCheckBoxAllowed_NegativeScenariotest() {
        String strid = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isShowExportedCheckBoxAllowed();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void setRun_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        localObj.setRun();
        Test.stopTest();
        System.AssertEquals(true,localObj.availableRuns.size() > 0);
        System.AssertEquals(true,localObj.runMap.size() == 0);
    }
    static testMethod void setshiptodetailsonOrder_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getConsumptionOrder();
        EP_OrderPageContext localObj = new EP_OrderPageContext(orderObj.AccountId__c);
        localObj.strSelecteddeliveryType = EP_Common_Constant.DELIVERY;
        localObj.strSelectedShipToID = orderObj.EP_ShipTo__c;
        system.debug('orderObj.EP_ShipTo__c--'+orderObj.EP_ShipTo__c);
        EP_AccountDomainObject accountdomainObj = new EP_AccountDomainObject(orderObj.EP_ShipTo__c);
        Account shipToAccountObj = accountdomainObj.getAccount();
        system.debug('shipToAccountObj--'+shipToAccountObj);
        localObj.mapOfShipTo = new Map <Id,Account>();
        localObj.mapOfShipTo.put(shipToAccountObj.id,shipToAccountObj);       
        Test.startTest();
            localObj.setshiptodetailsonOrder();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.newOrderRecord.EP_ShipTo__c);
        System.AssertNotEquals(Null,localObj.mapShipToTanks);
    }
    static testMethod void setDataForConsumptionOrder_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        localObj.setDataForConsumptionOrder();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.orderService );
    }
    
    static testMethod void settankdetailsonOrder_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [SELECT Id, EP_Product__C,EP_Product__r.Name,EP_Safe_Fill_Level__c,EP_Tank_Code__c,EP_Tank_Alias__c FROM EP_Tank__c LIMIT 1];
        orderWrapperObj.oliTanksID = tankObj.id;
        localObj.mapShipToOperationalTanks.put(tankObj.Id,tankObj);
        localObj.PriceBookEntryproductMap.put(tankObj.EP_Product__c,tankObj.EP_Product__c);
        Test.startTest();
        localObj.settankdetailsonOrder(orderWrapperObj);
        Test.stopTest();
        System.AssertEquals(null,orderWrapperObj.oliQuantity);
        System.AssertEquals(tankObj.EP_Product__c,orderWrapperObj.oliPricebookEntryID);
        System.AssertEquals(tankObj.Id,orderWrapperObj.orderLineItem.EP_Tank__c);
    }
    
    static testMethod void setOperationTankdetails_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        EP_Tank__c tankObj = [SELECT Id, EP_Product__C,EP_Product__r.Name,EP_Safe_Fill_Level__c,EP_Tank_Code__c,EP_Tank_Alias__c FROM EP_Tank__c LIMIT 1];
        tankobj.EP_Tank_Code__c = 'TANK-007';
        update tankObj;
        System.debug('@@ tankObj : '+ tankObj);
        localObj.shipToOperationalTankIds.add(tankObj.Id);     
        localObj.mapShipToOperationalTanks.put(tankObj.Id,tankObj);          
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        orderWrapperObj.oliTanksID = tankObj.Id;
        Test.startTest();
        localObj.setOperationTankdetails(orderWrapperObj);
        Test.stopTest();        
        System.AssertEquals(null,orderWrapperObj.orderLineItem.EP_Ambient_Loaded_Quantity__c  );
        System.AssertEquals(null,orderWrapperObj.orderLineItem.EP_Standard_Loaded_Quantity__c  );
        System.AssertEquals(null,orderWrapperObj.orderLineItem.EP_Ambient_Delivered_Quantity__c  );
        System.AssertEquals(null,orderWrapperObj.orderLineItem.EP_Standard_Delivered_Quantity__c  );
        System.AssertEquals(tankObj.EP_Product__r.Name,orderWrapperObj.oliTankProductName ); 
        System.AssertEquals(tankObj.EP_Safe_Fill_Level__c,orderWrapperObj.oliTankSafeFillLvl); 
        System.AssertNotEquals(null,orderWrapperObj.oliTanksName);         
    }
    
    static testMethod void setProductNameandTankDetails_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        csord__Order_Line_Item__c orderItemObj = [SELECT Id, EP_Unit_of_Measure__c, EP_Prior_Quantity__c, EP_Total_Price__c, PriceBookEntryID__c,
                                                    //Product2.EP_Unit_of_Measure__c,
                                                    Quantity__c,
                                                    //PriceBookEntry.Product2.Name,
                                                    EP_Tank__r.id,EP_Tank_Code__c,EP_Tank__r.EP_Tank_Alias__c
                                                    //, PriceBookEntry.Product2.EP_Unit_Of_Measure__c
                                                    FROM csord__Order_Line_Item__c WHERE OrderId__c =: strid];
        orderItemObj.EP_Is_Standard__c = true;
        localObj.newOrderRecord.csord__Order_Line_Items__r.add(orderItemObj);
        localObj.itemIdTotalPriceMap = new Map<id,Double>();
    	localObj.itemIdTotalPriceMap.put(localObj.newOrderRecord.csord__Order_Line_Items__r[0].id,111.00);
        localObj.listofOrderWrapper.add(localObj.setExistingOrderLineItemsInWrapper(orderItemObj));
        Test.startTest();
        localObj.loadExistingOrderItems();
        localObj.setProductNameandTankDetails();
        Test.stopTest();
        System.AssertEquals(false,localObj.isPriceCalculated  );
        System.AssertNotEquals('',localObj.listofOrderWrapper[0].oliProductName );
    }
    
    static testMethod void isOrderEditable_PositiveScenariotest() {
        csord__Order__c orderObj = EP_TestDataUtility.getConsumptionOrder();
        //String strid = orderObj.AccountId;
        EP_OrderPageContext localObj = new EP_OrderPageContext(orderObj.Id);
        orderObj.Status__c = EP_Common_Constant.planned;
        //localObj.newOrderRecord = orderObj;
        Test.startTest();
        Boolean result = localObj.isOrderEditable();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderEditable_NegativeScenariotest() {
        csord__Order__c orderObj =  EP_TestDataUtility.getConsumptionOrder();
        orderObj.Status__c = EP_OrderConstant.OrderState_Delivered;
        update orderObj;
        String strid = orderObj.AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        Boolean result = localObj.isOrderEditable();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void loadExistingOrderItems_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        localObj.loadExistingOrderItems();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.childItemsMap );
    }
    
    static testMethod void setExistingOrderLineItemsInWrapper_test() {
        String strid = EP_TestDataUtility.getSalesOrder().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        EP_OrderItemMapper orderItemMapperObj = new EP_OrderItemMapper();
        List<csord__Order_Line_Item__c> orderItemList = [SELECT Id, EP_Unit_of_Measure__c, EP_Prior_Quantity__c, EP_Total_Price__c, PriceBookEntryID__c,
                                                    //Product2.EP_Unit_of_Measure__c,
                                                    Quantity__c,
                                                    //PriceBookEntry.Product2.Name,
                                                    EP_Tank__r.id,EP_Tank_Code__c,EP_Tank__r.EP_Tank_Alias__c
                                                    //, PriceBookEntry.Product2.EP_Unit_Of_Measure__c
                                                    FROM csord__Order_Line_Item__c WHERE OrderId__c =: strid];
        localObj.newOrderRecord.csord__Order_Line_Items__r.add(orderItemList[0]);
        localObj.itemIdTotalPriceMap = new Map<id,Double>();
    	localObj.itemIdTotalPriceMap.put(localObj.newOrderRecord.csord__Order_Line_Items__r[0].id,111.00);
        System.debug('@@ orderItemList : '+orderItemList);
        Test.startTest();
        EP_OrderPageContext.OrderWrapper result = localObj.setExistingOrderLineItemsInWrapper(orderItemList[0]);
        Test.stopTest();
        System.AssertEquals(orderItemList[0],result.orderLineItem);
    }
    
    static testMethod void getOrderLineItemsFromOrderWrapper_test() {
        String strid = EP_TestDataUtility.getSalesOrder().Id;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        csord__Order_Line_Item__c orderItemObj = [SELECT Id, EP_Unit_of_Measure__c, EP_Prior_Quantity__c, EP_Total_Price__c, PriceBookEntryID__c,
                                                    //Product2.EP_Unit_of_Measure__c,
                                                    Quantity__c,
                                                    //PriceBookEntry.Product2.Name,
                                                    EP_Tank__r.id,EP_Tank_Code__c,EP_Tank__r.EP_Tank_Alias__c
                                                    //, PriceBookEntry.Product2.EP_Unit_Of_Measure__c
                                                    FROM csord__Order_Line_Item__c WHERE OrderId__c =: strid];
        System.debug('@@ orderItemObj : '+orderItemObj );
        orderItemObj.EP_Is_Standard__c = true;
        localObj.newOrderRecord.csord__Order_Line_Items__r.add(orderItemObj);
        localObj.itemIdTotalPriceMap = new Map<id,Double>();
    	localObj.itemIdTotalPriceMap.put(localObj.newOrderRecord.csord__Order_Line_Items__r[0].id,111.00);
        localObj.listofOrderWrapper.add(localObj.setExistingOrderLineItemsInWrapper(orderItemObj));
        System.debug('@@ orderWrapperList : '+localObj.listofOrderWrapper );
        LIST<csord__Order_Line_Item__c> result = localObj.getOrderLineItemsFromOrderWrapper();
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    
    static testMethod void setPageVariablesforModificationController_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getConsumptionOrder();
        orderObj.EP_Requested_Delivery_Date__c = System.Today()+10;
        String strid = orderObj.AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);  
        localObj.newOrderRecord = orderObj;    
        Test.startTest();
        localObj.setPageVariablesforModificationController();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.showCustomerPoField );
        System.AssertNotEquals(Null,localObj.strSelectedDate);
        System.AssertNotEquals(Null,localObj.strSelectedPickupLocationID);
        System.AssertEquals(orderObj.EP_ShipTo__c,localObj.strSelectedShipToID);
        System.AssertEquals(orderObj.EP_Run__c,localObj.strSelectedRun);
        System.AssertEquals(orderObj.EP_Delivery_Type__c,localObj.strSelecteddeliveryType);
    }
    
    static testMethod void validateCustomer_test() {
        String strid = EP_TestDataUtility.getConsumptionOrder().AccountId__c;
        EP_OrderPageContext localObj = new EP_OrderPageContext(strid);
        Test.startTest();
        localObj.validateCustomer();
        Test.stopTest();
        System.AssertEquals(false,localObj.hasAccountSetupError);
    }
    
    static testMethod void getOrderItemsIdListFromWrapper_test() {
        //String strid = EP_TestDataUtility.getSalesOrder().AccountId;
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        EP_OrderPageContext localObj = new EP_OrderPageContext(ord.id);
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        localObj.itemIdTotalPriceMap = new Map<id,Double>();
    	localObj.itemIdTotalPriceMap.put(orderlineitem.id,111.00);
        Test.startTest();
        localObj.loadExistingOrderItems();
        LIST<csord__Order_Line_Item__c> result = localObj.getOrderItemsIdListFromWrapper();
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
}