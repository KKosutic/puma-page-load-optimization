/* 
   @Author <Accenture>
   @name <EP_OrderUpdateMsgFromWINDMS>
   @CreateDate <03/07/2017>
   @Description <These are the stub classes to map JSON string for orders Update Rest Service Request from WINDMS> 
   @Version <1.0>
   */
public class EP_NAVOrderUpdateStub {
    /* Class for MSG */
    public MSG MSG;
    
    /* MSG stub class*/ 
    public class MSG {
        public HeaderCommon HeaderCommon {get;set;}
        public Payload Payload {get;set;}
        public String StatusPayload {get;set;}
    }
    
    /* HeaderCommon stub class*/    
    public class HeaderCommon extends EP_MessageHeader {}
    
    /* Payload stub class*/
    public class Payload {
        public any0 any0 {get;set;}
    }
    
    /* Any0 stub class*/
    public class any0 {
        public OrderStatus orderStatus {get;set;}
    }
    
    /* OrderWrapper stub class*/
    public class OrderWrapper {
        //Added New variable
        public csord__Order__c sfOrder {get;set;}
        public string errorCode {get;set;}
        public string errorDescription {get;set;}
        public String versionNr {get;set;}
        //END
        public string seqId {get;set;}
        public Identifier identifier {get;set;}
        public string reasoncode {get;set;}
        public string description {get;set;}
        public String status {get;set;}
    }
    
    /* OrderStatus stub class*/
    public class OrderStatus {
        public OrderList orders {get;set;}
    }

    public class OrderList {
        public List<OrderWrapper> order {get;set;}
    }

    /* Identifier stub class*/
    public class Identifier {
        public string orderId {get;set;}
        public String entrprsId {get;set;}
    }
}