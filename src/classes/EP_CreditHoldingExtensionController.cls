/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_CreditHoldingExtensionController>                 *
*  @CreateDate <3/11/2017>                                     *
*  @Description <Controller class to associate/dissociate 
					customer>                                  *
*  @Version <1.0>                                              *
****************************************************************/
public with sharing class EP_CreditHoldingExtensionController {
	private EP_Credit_Holding__c holdingObj;
	public EP_CreditHoldingExtensionContext ctx{get;set;}
	private EP_CreditHoldingExtensionHelper holdingHelper;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_CreditHoldingExtensionController             *
* @description  Standard constructor of class                   *
* @param        Standard Controller                             *
* @return       NA                                              *
****************************************************************/
	public EP_CreditHoldingExtensionController(ApexPages.StandardController controller) {
		try{
			String currentPageUrl = ApexPages.currentPage().getURL();
			String holdingId = ApexPages.currentPage().getParameters().get('id');
			holdingObj = (EP_Credit_Holding__c) controller.getRecord();
			system.debug('holdingObj = '+holdingObj);
			ctx = new EP_CreditHoldingExtensionContext(holdingObj);
			ctx.returnURL = ApexPages.currentPage().getParameters().get('retURL');
			holdingHelper = new EP_CreditHoldingExtensionHelper(ctx);
			
		}catch(exception ex){
			system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage())); 
		}
	}
	
	public EP_CreditHoldingExtensionController() {
		
	}

/****************************************************************
* @author       Accenture                                       *
* @name         search                                          *
* @description  method to get all credit customer               *
* @param        NA 					                            *
* @return       pageReference                                   *
****************************************************************/
	public pageReference search(){
		system.debug('ctx.searchCriteria = '+ctx.searchCriteria);
		system.debug('ctx.searchName = '+ctx.searchName);
		return(holdingHelper.searchCreditCustomer());
	}

/****************************************************************
* @author       Accenture                                       *
* @name         save                                            *
* @description  method to do DML on account and holding object  *
* @param        NA 					                            *
* @return       pagereference                                   *
****************************************************************/
	public pageReference save(){
		try{
			return(holdingHelper.save());
		}catch(exception ex){
			system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
			if(!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage())); 
			return null;
		}
	}

/****************************************************************
* @author       Accenture                                       *
* @name         cancel                                          *
* @description  method to return to holding record              *
* @param        NA 					                            *
* @return       pagereference                                   *
****************************************************************/
	public pageReference cancel(){
		try{
			return(holdingHelper.cancel());
		}catch(exception ex){
			system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
			if(!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage())); 
			return null;
		}
	}
}