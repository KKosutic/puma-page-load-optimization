/*
 *  @Author <Accenture>
 *  @Name <EP_AccountEventObject>
 *  @CreateDate <05/02/2017>
 *  @Description <Wrapper class to store Account events>
 *  @Version <1.0>
 */ 
 
 public class EP_AccountEvent {

    String eventName;
    String eventMessage;
    public boolean isError = false;
    @TestVisible List<string> eventMessages = new List<string>();
    
    public EP_AccountEvent(String eventName){
        this.eventName = eventName;
    }
    
    public String getEventName(){
        EP_GeneralUtility.Log('Public','EP_AccountEvent','getEventName');
        return eventName;
    }

    public void setEventName(String value){
        EP_GeneralUtility.Log('Public','EP_AccountEvent','setEventName');
        eventName = value;
        
    }

    public String getEventMessage(){
        EP_GeneralUtility.Log('Public','EP_AccountEvent','getEventMessage');
        return eventMessage;
    }

    public void setEventMessage(String value){
        EP_GeneralUtility.Log('Public','EP_AccountEvent','setEventMessage');
        eventMessage = value;
    } 
    
    public void addEventMessage(String value){
    	EP_GeneralUtility.Log('Public','EP_AccountEvent','addEventMessage');
    	eventMessages.add(value);
    }
}