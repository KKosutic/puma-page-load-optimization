/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateCancelled.cls>     
     @Description <Order State for Cancelled status>   
     @Version <1.1> 
     */

     public class EP_OrderStateCancelled extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCancelled','doTransition');
            return super.doTransition();
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCancelled','doOnEntry');
            
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCancelled','doOnExit');
            //Defect Fix Start #57588 - Do not send Outbound SYNC Request to WINDMS if EP_Use_Managed_Transport_Services__c is true 
            if(this.order.getOrder().EP_Sync_With_LS__c && !EP_Common_Constant.STRING_TRUE.equalsIgnoreCase(this.order.getOrder().EP_Use_Managed_Transport_Services__c) ) {
                this.orderService.doSyncStatusWithWindms();
            }
            //Defect Fix End #57588
            
            if(this.order.getOrder().EP_Sync_with_NAV__c){
                this.orderService.doSyncStatusWithNav();    
            }

            if(EP_Common_Constant.CREDIT_BLOCK.equalsIgnoreCase(this.order.getOrder().EP_Order_Credit_Status__c)){
                this.orderService.doSyncCreditCheckWithWINDMS();
            }
        }
        
        public override boolean isInboundTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCancelled','isInboundTransitionPossible');
            //Override this with all possible guard conditions that permits the state transition possible
            //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
            return super.isInboundTransitionPossible();
        }
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Cancelled;
        }
    }