/**
@Author <Pravindra Khatri>
@name <EP_Mandate_Tank_Dip_Entry_For_VMITest>
@CreateDate <09/12/2015>
@Description <This test class is used to cover basic validations on Account status field, ship to type, tank status>
@Version <1.0>
*/
@isTest
public class EP_Mandate_Tank_Dip_Entry_For_VMITest{
    private static Account accObj; 
    //method for single dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks
    private static testMethod void validateDateTimeTankDip(){
            //CREATE CSC USER
            Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
            User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            Test.startTest();
        System.runAs(cscUser) {
                // Inserting an account
                accObj=createTestAccount(strRecordTypeId);
                accObj.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
                //insert accObj;           
                EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c='1'); 
                tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
                insert tankObj;
                
                EP_Tank__c tankObj1= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c='2'); 
                tankObj1.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry';   
                insert tankObj1;
            //Changing the Ship-To account to 'Basic Data Setup'
                accObj.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                accObj.EP_Is_Valid_Address__c = true;
                EP_Common_Util.isValidAddress = true;
                update accObj;
                
                tankObj.EP_Tank_Status__c='Operational';
               // update tankObj;
                tankObj1.EP_Tank_Status__c='Operational';
               // update tankObj1;
            
            try{ 
                accObj.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj,false);
            
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
                database.insert(tankList,false);
                tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList[0],false);
                Test.stopTest();
                
            }
            catch(Exception e){
                EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
                // system.debug(e.getstacktracestring()+' reason: '+e.getMessage());
                System.Assert(e.getMessage().contains('You cannot enter a single dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks'));
            }
        }
    }
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.                                  *
    *@Params      : strRecordTypeId                                                      *
    *@Return      : Account                                                      *    
    **************************************************************************/
    private static Account createTestAccount(Id strRecordTypeId) {
        accObj = new Account(Name='ZISCHKE FUEL ARATULA'
                         ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                         ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Non-Consignment'
                         ,ShippingCity = 'testCity'
                         ,ShippingStreet = 'testStreet'
                         ,ShippingState = 'testState'
                         ,ShippingCountry = 'testCountry'
                         ,ShippingPostalCode = '123456'
                            ,Phone='12345678'
                            ,EP_Email__c='test123@test.com'
                            ,EP_Is_Valid_Address__c = true
                            );
        database.insert(accobj,false);                 
        return accObj;
    }
     
    //method for multiple dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank     
    private static testMethod void validateDateTimeTankDip1(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
            // Inserting an account
            accObj = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Consignment'
                             //,EP_Status__c = 'active'
                             ,ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,Phone='12345678'
                             ,EP_Email__c='test123@test.com'
                             ,EP_Is_Valid_Address__c = true);
             database.insert(accobj,false);                 
            
            EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500);
            tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
            insert tankObj;
            //Changing the Ship-To account to 'Basic Data Setup'
                accObj.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                update accObj;
               // tankObj.EP_Tank_Status__c='Operational';
               // update tankObj;
           // tankObj.EP_Tank_Status__c= 'Operational';
            //database.insert(tankObj,false);
                
            //try{ 
                accObj.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
                
                for(EP_Tank_Dip__c tp: tankList){
                    tp.EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                }
                // tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList,false);
                Test.stopTest();
          //  }
           // catch(Exception e){
               // EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip1', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
               // System.Assert(e.getMessage().contains('You cannot enter a multiple dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank'));
           // }
        }
    }
 
    //method for multiple dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank          
    private static testMethod void validateDateTimeTankDip2(){
            Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
            User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            Test.startTest();
            System.runAs(cscUser) {
            // Inserting an account
            accObj = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Non-Consignment'
                             //,EP_Status__c = 'active'
                             ,ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,Phone='12345678'
                             ,EP_Email__c='test123@test.com'
                             ,EP_Is_Valid_Address__c = true); 
             database.insert(accobj,false);               
                             
            EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500);
            tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
            insert tankObj;
            //Changing the Ship-To account to 'Basic Data Setup'
            accObj.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update accObj;
            tankObj.EP_Tank_Status__c='Operational';
           // update tankObj;    
           // try{ 
                accObj.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankobj.id)},tankobj);
                
                for(EP_Tank_Dip__c tp: tankList){
                    tp.EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                }
                // tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList,false);
                Test.stopTest();
          //  }
          //  catch(Exception e){
           //     EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip2', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
           //     System.Assert(e.getMessage().contains('You cannot enter a multiple dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank'));
           // }
        }
    }

    //method for multiple dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks     
    private static testMethod void validateDateTimeTankDip3(){
                Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];

            User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            Test.startTest();
            System.runAs(cscUser) {
            // Inserting an account
            accObj = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Consignment',
                              //EP_Status__c = 'active'
                             ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                               ,Phone='12345678'
                             ,EP_Email__c='test123@test.com'
                             ,EP_Is_Valid_Address__c = true); 
            database.insert(accobj,false); 
                             
            list<EP_Tank__c> tank =  new list<EP_Tank__c>();
            for(integer i=0;i<5;i++){
            EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c= 'Cd' + i); 
            tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry';             
           // tankObj.EP_Tank_Status__c= 'Operational';
            tank.add(tankobj);
            } 
            insert tank;  
            accObj.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update accObj;
                tank =  new list<EP_Tank__c>();
            for(EP_Tank__c tk: [Select id, EP_Tank_Status__c from EP_Tank__c where Ep_Ship_To__c =:accObj.id ]){
                system.assertEquals(EP_Common_Constant.BASIC_DATA_SETUP, tk.EP_Tank_Status__c);
                tk.EP_Tank_Status__c= EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                tank.add(tk);
            }
                //update tank;

            
            
                
           // try{ 
                accObj.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tank[0].id)},tank[0]);
                
                for(EP_Tank_Dip__c tp: tankList){
                    tp.EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                }
                database.update(tankList,false);
                Test.stopTest();
           // }
           /* catch(Exception e){
                EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip3', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
                System.Assert(e.getMessage().contains('You cannot enter a multiple dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks'));
            }*/
        }
    }

    //method for multiple dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks     
    private static testMethod void validateDateTimeTankDip4(){
                Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];

            User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            Test.startTest();
            System.runAs(cscUser) {
            // Inserting an account
            accObj = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Non-Consignment',
                                 //EP_Status__c = 'active'
                             ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,Phone='12345678'
                             ,EP_Email__c='test123@test.com'
                             ,EP_Is_Valid_Address__c = true); 
                             
            database.insert(accobj,false);                 
                             
            list<EP_Tank__c> tank =  new list<EP_Tank__c>();
            for(integer i=0;i<5;i++){
            EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c= 'Cd' + i); 
             tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry';            
           // tankObj.EP_Tank_Status__c= 'Operational';
            tank.add(tankobj);
            } 
            insert tank;  
            accObj.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update accObj;
                tank =  new list<EP_Tank__c>();
            for(EP_Tank__c tk: [Select id, EP_Tank_Status__c from EP_Tank__c where Ep_Ship_To__c =:accObj.id ]){
                system.assertEquals(EP_Common_Constant.BASIC_DATA_SETUP, tk.EP_Tank_Status__c);
                tk.EP_Tank_Status__c= 'Operational';
                tank.add(tk);
            }
               // update tank;

                
          //  try{ 
                accObj.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tank[0].id)},tank[0]);
                
                for(EP_Tank_Dip__c tp: tankList){
                    tp.EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                }

                database.update(tankList,false);
                Test.stopTest();
           // }
           /** catch(Exception e){
                EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip4', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
                System.Assert(e.getMessage().contains('You cannot enter a multiple dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks'));
            }**/
        }
    }

        
    private static testMethod void validateDateTimeTankDip5(){
                    
            Account accObj1; 
            //method for single dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank
            
                //CREATE CSC USER
                Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];

                User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
                Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
                Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
                Test.startTest();
                System.runAs(cscUser) {
                    // Inserting an account
                    accObj1=createTestAccount(strRecordTypeId);
                    database.insert(accObj1,false);
                    
                    EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj1.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500); 
                    tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
                    insert tankObj;
            //Changing the Ship-To account to 'Basic Data Setup'
                    accObj1.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                    accObj.EP_Is_Valid_Address__c = true;
                    EP_Common_Util.isValidAddress = true;
                    update accObj1;
                   // tankObj.EP_Tank_Status__c='Operational';
                   // update tankObj;
                
           // try{ 
                accObj1.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj1,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
                tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList[0],false);
                Test.stopTest();
           // }
           // catch(Exception e){
           //     EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip5', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
            //    System.Assert(e.getMessage().contains('You cannot enter a single dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank'));
            //}
        }
    }
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.                                  *
    *@Params      : strRecordTypeId                                                      *
    *@Return      : Account                                                      *    
    **************************************************************************/
    private static Account createTestAccount1(Id strRecordTypeId) {
          account  accObj1 = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Consignment'
                             ,ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,EP_Is_Valid_Address__c = true);
          database.insert(accobj1,false);                   
                             return accObj1;
    }
    private static testMethod void validateDateTimeTankDip6(){
                    
        Account accObj1; 
        //method for single dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank

         
                //CREATE CSC USER
                Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];

                User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
                Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
                Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
                Test.startTest();
                System.runAs(cscUser) {
                    // Inserting an account
                    accObj1=createTestAccount(strRecordTypeId);
                    database.insert(accObj1,false);
                    
                    EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj1.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500); 
                    tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
                    insert tankObj;
            //Changing the Ship-To account to 'Basic Data Setup'
                    accObj1.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                    accObj.EP_Is_Valid_Address__c = true;
                    EP_Common_Util.isValidAddress = true;
                    update accObj1;
                  //  tankObj.EP_Tank_Status__c='Operational';
                  //  update tankObj;
                    
                
           // try{ 
                accObj1.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj1,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
                tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList[0],false);
                Test.stopTest();
          //  }
           // catch(Exception e){
            //    EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip6', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
            //    System.Assert(e.getMessage().contains('You cannot enter a single dip entry for VMI Non-Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a single tank'));
            //}
        }
    }
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.                                  *
    *@Params      : strRecordTypeId                                                      *
    *@Return      : Account                                                      *    
    **************************************************************************/
    private static Account createTestAccount2(Id strRecordTypeId) {
         account   accObj1 = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Non-Consignment'
                             ,ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,EP_Is_Valid_Address__c = true);
          database.insert(accobj1,false);                   
                             return accObj1;
    }
             
    private static testMethod void validateDateTimeTankDip7(){
            
            
        Account accObj1; 
        //method for single dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks

            
                //CREATE CSC USER
                Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];

                User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
                Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
                Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
                Test.startTest();
                System.runAs(cscUser) {
                    // Inserting an account
                    accObj1=createTestAccount(strRecordTypeId);
                    database.insert(accObj1,false);
                    
                    EP_Tank__c tankObj= new EP_Tank__c (EP_Ship_To__c=accObj1.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c='1'); 
                    tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
                    insert tankObj;                    
                    EP_Tank__c tankObj1= new EP_Tank__c (EP_Ship_To__c=accObj1.id,EP_Product__c=prod.id,EP_Safe_Fill_Level__c=2500,EP_Tank_Code__c='2'); 
                    tankObj1.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry'; 
                    insert tankObj1;
                    //Changing the Ship-To account to 'Basic Data Setup'
                    accObj1.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP; 
                    accObj.EP_Is_Valid_Address__c = true;
                    EP_Common_Util.isValidAddress = true;
                    update accObj1;
                   // tankObj.EP_Tank_Status__c= 'Operational';
                   // update tankObj;
                   // tankObj1.EP_Tank_Status__c= 'Operational';
                    //update tankObj1;
                    
                
           // try{ 
                accObj1.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
                database.update(accObj1,false);
                
                List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
                tankList[0].EP_Reading_Date_Time__c=DateTime.Now().AddDays(1);
                database.update(tankList[0],false);
                Test.stopTest();
           // }
           // catch(Exception e){
            //    EP_LoggingService.logHandledException(e, 'ePuma', 'validateDateTimeTankDip7', 'EP_Mandate_Tank_Dip_Entry_For_VMITest', ApexPages.Severity.ERROR);
            //    System.Assert(e.getMessage().contains('You cannot enter a single dip entry for VMI Consignment credit Customer with Unblocked Customer Account, Ship-To and Tank status for a multiple tanks'));
            //}
        }
    }
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.                                  *
    *@Params      : strRecordTypeId                                                      *
    *@Return      : Account                                                      *    
    **************************************************************************/
    private static Account createTestAccount3(Id strRecordTypeId) {
        account accObj1 = new Account(Name='ZISCHKE FUEL ARATULA'
                             ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                             ,RecordTypeId= strRecordTypeId,EP_Ship_To_Type__c  = 'VMI Consignment',EP_Tank_Dips_Schedule_Time__c ='11:00',EP_Tank_Dip_Entry_Mode__c ='Portal dip entry'
                             ,ShippingCity = 'testCity'
                             ,ShippingStreet = 'testStreet'
                             ,ShippingState = 'testState'
                             ,ShippingCountry = 'testCountry'
                             ,ShippingPostalCode = '123456'
                             ,EP_Is_Valid_Address__c = true);
        database.insert(accobj1,false);                 
        return accObj1;
    }        
}