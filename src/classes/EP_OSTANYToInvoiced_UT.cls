@isTest
public class EP_OSTANYToInvoiced_UT
{

    static final String event='Delivered To Invoiced';
    static final String invalidEvent='Invoiced To Submitted';

    @testSetup static void init() {
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
    }

    static testMethod void isTransitionPossible_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(event);
        EP_OSTANYToInvoiced ost = new EP_OSTANYToInvoiced();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(event);
        EP_OSTANYToInvoiced ost = new EP_OSTANYToInvoiced();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }   
    
    static testMethod void isGuardCondition_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(event);
        EP_OSTANYToInvoiced ost = new EP_OSTANYToInvoiced();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

}