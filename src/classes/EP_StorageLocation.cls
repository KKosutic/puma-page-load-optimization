/* 
@Author      Accenture
@name        EP_StorageLocation
@CreateDate  02/16/2017
@Description This is strategy class for storage location and executes logic specific to storage location Account
@Version     1.1
*/
public class EP_StorageLocation extends EP_AccountType{
	
	//#L4-45526 Start
    /**  
    *  @Author      Accenture
    *  @ name       doBeforeDeleteHandle
    *  @param       Account
    *  @Return      Null
    */
    public override void doBeforeDeleteHandle(Account account){
        EP_GeneralUtility.Log('Public','EP_AccountType','doBeforeDeleteHandle');
        if(String.isNotBlank(account.EP_Status__c) && EP_Common_Constant.STATUS_ACTIVE.equalsignorecase(account.EP_Status__c)){
        	account.adderror(System.label.EP_Active_StockLocation_cant_delete);
        }
    }
    //#L4-45526 End
}