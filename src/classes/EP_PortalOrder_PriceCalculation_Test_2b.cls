/**
 * @author <Kamal Garg>
 * @name <EP_PortalOrder_PriceCalculation2b_Test >
 * @createDate <28/12/2015>
 * @description <Test class for testing automation tests for E2E 024_002b> 
 * @version <1.0>
 */
@isTest
private class EP_PortalOrder_PriceCalculation_Test_2b {

    private static Account billToAccount;
    private static Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1]; 
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    static testMethod void testOrderPrice() {
        User sysAdminUser = EP_TestDataUtility.createRunAsUser();
        System.runAs(sysAdminUser){
                billToAccount = EP_TestDataUtility.createBillToAccount();
                database.insert(billToAccount) ;
                billToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                database.update(billToAccount);
                billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                database.update(billToAccount);
                Account sellToAccount = EP_TestDataUtility.createSellToAccount(billToAccount.id, null);
                sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
                sellToAccount.EP_VAT_Exempted__c = false;
                database.insert(sellToAccount);
                
                sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                database.update(sellToAccount);
                
                sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                database.update(sellToAccount);
             
                Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
                
                Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
                database.insert(orderObj);
                
                List<Product2> productsToBeInserted = new List<Product2>();
                Product2 productObj = createProduct('Diesel');
                Product2 taxProductObj = createProduct('TaxProduct');
                productsToBeInserted.add(productObj);
                productsToBeInserted.add(taxProductObj);
                Database.insert(productsToBeInserted, false);
                
                List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
                PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
                PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
                pricebookEntryToBeInserted.add(pricebookEntryObj);
                pricebookEntryToBeInserted.add(taxPricebookEntryObj);
                Database.insert(pricebookEntryToBeInserted,false);
                
                OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
                database.insert(orderItemObj,false);
                // verify the results
               // List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
              //  system.assertEquals(orderItemsList.size(), 2);
                Decimal price = 0.0;
                for(OrderItem obj : [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
                    price += obj.UnitPrice*obj.Quantity;
                }
                Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
                system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    static testMethod void testOrderPrice1() {
        User sysAdminUser = EP_TestDataUtility.createRunAsUser();
        System.runAs(sysAdminUser){
                Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
                sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
                sellToAccount.EP_VAT_Exempted__c = true;
                database.insert(sellToAccount,false);
                
                sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                database.update(sellToAccount,false);
                
                Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
                
                Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
                database.insert(orderObj,false);
                
                List<Product2> productsToBeInserted = new List<Product2>();
                Product2 productObj = createProduct('Diesel');
                Product2 taxProductObj = createProduct('TaxProduct');
                productsToBeInserted.add(productObj);
                productsToBeInserted.add(taxProductObj);
                Database.insert(productsToBeInserted, false);
                
                List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
                PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
                PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
                pricebookEntryToBeInserted.add(pricebookEntryObj);
                pricebookEntryToBeInserted.add(taxPricebookEntryObj);
                Database.insert(pricebookEntryToBeInserted,false);
                
                OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
                database.insert(orderItemObj,false);
                // verify the results
                //List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
                //system.assertEquals(orderItemsList.size(), 2);
                Decimal price = 0.0;
                for(OrderItem obj :[SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
                    if(obj.EP_Is_Taxes__c == true){
                        system.assertEquals(obj.UnitPrice, 5.0);
                    }
                    price += obj.UnitPrice*obj.Quantity;
                }
                Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
                system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    static testMethod void testOrderPrice2() {
        User sysAdminUser = EP_TestDataUtility.createRunAsUser();
        System.runAs(sysAdminUser){
                Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
                sellToAccount.EP_Company_Is_Tax_Exempt__c = true;
                sellToAccount.EP_VAT_Exempted__c = false;
                database.insert(sellToAccount,false);
                
                sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                database.update(sellToAccount,false);
                
                Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
                
                Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
                database.insert(orderObj,false);
                
                List<Product2> productsToBeInserted = new List<Product2>();
                Product2 productObj = createProduct('Diesel');
                Product2 taxProductObj = createProduct('TaxProduct');
                productsToBeInserted.add(productObj);
                productsToBeInserted.add(taxProductObj);
                Database.insert(productsToBeInserted, false);
                
                List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
                PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
               
                PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
                pricebookEntryToBeInserted.add(pricebookEntryObj);
                pricebookEntryToBeInserted.add(taxPricebookEntryObj);
                Database.insert(pricebookEntryToBeInserted);
                
                OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
                database.insert(orderItemObj,false);
                // verify the results
                //List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
                //system.assertEquals(orderItemsList.size(), 2);
                Decimal price = 0.0;
                for(OrderItem obj : [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
                    if(obj.EP_Is_Taxes__c == true){
                        system.assertEquals(obj.UnitPrice, 0.14);
                    }
                    price += obj.UnitPrice*obj.Quantity;
                }
                Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
                system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     * @This method covers test case number 10982
     */
    static testMethod void testOrderPriceCsc() {
        
        System.runAs(cscUser){
        
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount.EP_VAT_Exempted__c = false;
        database.insert(sellToAccount,false);
   
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        database.update(sellToAccount,false);
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        database.insert(orderObj,false);
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted,false);
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        database.insert(orderItemObj,false);
        // verify the results
        //List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
        //system.assertEquals(orderItemsList.size(), 2);
        Decimal price = 0.0;
        for(OrderItem obj : [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     * @This method covers test case number 10979
     */
    static testMethod void testOrderPrice1Csc() {
        
        System.runAs(cscUser){
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount.EP_VAT_Exempted__c = true;
        database.insert(sellToAccount,false);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        database.update(sellToAccount,false);
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        database.insert(orderObj,false);
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        database.insert(orderItemObj,false);
        // verify the results
        //List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
        //system.assertEquals(orderItemsList.size(), 2);
        Decimal price = 0.0;
        for(OrderItem obj : [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 5.0);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     * @This method covers test case number 10981
     */
    static testMethod void testOrderPrice2Csc() {
        System.runAs(cscUser){
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount.EP_Company_Is_Tax_Exempt__c = true;
        sellToAccount.EP_VAT_Exempted__c = false;
        database.insert(sellToAccount,false);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        database.update(sellToAccount,false);
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        database.insert(orderObj,false);
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted,false);
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        database.insert(orderItemObj,false);
        // verify the results
        //List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 1];
       // system.assertEquals(orderItemsList.size(), 2);
        Decimal price = 0.0;
        for(OrderItem obj : [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem LIMIT 2]){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 0.14);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id LIMIT 1];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test Order record
     * @params Id,Id,Id
     * @return Order
     */
    private static Order createOrder(Id acctId, Id recTypeId, Id pricebookId){
        Order obj = new Order();
        obj.AccountId = acctId;
        obj.RecordTypeId = recTypeId;
        obj.Status = 'Draft';
        obj.CurrencyIsoCode = 'USD';
        obj.PriceBook2Id = pricebookId;
        obj.EffectiveDate = system.today();
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test Product2 record
     * @params String
     * @return Product2
     */
    private static Product2 createProduct(String name){
        Product2 obj = new product2();
        obj.Name = name;
        obj.CurrencyIsoCode = 'USD';
        obj.IsActive = true;
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test PricebookEntry record
     * @params Id,Id
     * @return PricebookEntry
     */
    private static PricebookEntry createPricebookEntry(Id productId, Id pricebookId){
        PricebookEntry obj = new PricebookEntry();
        obj.Pricebook2Id = pricebookId;
        obj.Product2Id = productId;
        obj.EP_Is_Sell_To_Assigned__c=True;
        obj.UnitPrice = 1;
        obj.IsActive = true;
        obj.EP_VAT_GST__c = 14;
        obj.EP_Additional_Taxes__c = 5.0;
        obj.CurrencyIsoCode = 'USD';
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test OrderItem record
     * @params Id, Id
     * @return OrderItem
     */
    private static OrderItem createOrderItem(Id pricebookEntryId, Id orderId){
        OrderItem obj = new OrderItem();
        obj.PricebookEntryId = pricebookEntryId;
        obj.UnitPrice = 1;
        obj.Quantity = 1;
        obj.OrderId = orderId;
        obj.EP_Is_Freight_Price__c = false;
        obj.EP_Is_Taxes__c = false;
        
        return obj;
    }
}