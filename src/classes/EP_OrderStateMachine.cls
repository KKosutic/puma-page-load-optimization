/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderStateMachine>
 *  @CreateDate <03/02/2017>
 *  @Description <Order StateMachine provides order state based on order record type and order ship to account's type>
 *  @Version <1.0>
 */

 public virtual class EP_OrderStateMachine {
    public static EP_OrderDomainObject order;
    public static EP_OrderEvent orderEvent;
    
    public EP_OrderStateMachine(){
        
    }

    public EP_OrderState getOrderState(){
        EP_GeneralUtility.Log('Public','EP_OrderStateMachine','getOrderState');
        return getOrderStateInstance(order,orderEvent);
    }

    public virtual EP_OrderState getOrderState(EP_OrderEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_OrderStateMachine','getOrderState');
        orderEvent = currentEvent;
        return getOrderStateInstance(order,currentEvent);
    } 
    
    public static EP_OrderState getOrderState(EP_OrderDomainObject currentorder,EP_OrderEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_OrderStateMachine','getOrderState');
        order = currentorder;
        orderEvent = currentEvent;
        return getOrderStateInstance(currentorder,currentEvent);
    }
    
    @TestVisible
    private static EP_OrderState getOrderStateInstance(EP_OrderDomainObject localOrder, EP_OrderEvent currentEvent){
        EP_GeneralUtility.Log('Private','EP_OrderStateMachine','getOrderStateInstance');
        String localStatus=localOrder.getStatus();
        String orderCategory = localOrder.getOrderTypeClassification();
        system.debug('local Status ' + localStatus);
        EP_Order_State_Mapping__c orderstatename;
        try{
            orderstatename = [select Classnames__c from EP_Order_State_Mapping__c where Order_Type__c=:orderCategory and Order_Status__c=:localStatus limit 1];
            system.debug('Order State name ' +orderstatename.Classnames__c);
        }catch(Exception e){
            system.debug('exception is '+ e);
            // Order Status - {0} is not valid for {1}
            String exceptionMsg = Label.EP_OrderStateException;
            throw new EP_OrderStateMachineException(String.format(exceptionMsg, new String[]{localStatus, orderCategory}));
        }
        EP_OrderState os;
        try{
            Type t = Type.forName(orderstatename.Classnames__c);
            os = (EP_OrderState)t.newInstance();
            os.setOrderContext(localOrder,currentEvent);
            }catch(Exception e){
                system.debug('exception is '+ e);
                // custom label is -- Order State Machine not available for
                throw new EP_OrderStateMachineException(Label.EP_NoStateMachineAvailable +orderstatename.Classnames__c);
            }
            return os;
        }
        
        public void setOrderDomainObject(EP_OrderDomainObject orderdomainobject){
            EP_GeneralUtility.Log('Public','EP_OrderStateMachine','setOrderDomainObject');
            order = orderdomainobject;
        }
    }