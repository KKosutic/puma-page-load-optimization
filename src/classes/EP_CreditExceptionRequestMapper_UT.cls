@isTest
private class EP_CreditExceptionRequestMapper_UT {
	@isTest
	static void getRecord_TestPositive() {
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditExceptionRequest();
		EP_CreditExceptionRequestMapper creditExceptionMapper = new EP_CreditExceptionRequestMapper();
		EP_Credit_Exception_Request__c creditExceptionQueried = creditExceptionMapper.getRecord(creditException.Id);
		system.assertEquals(creditException.Id,creditExceptionQueried.Id);
	}

	@isTest
	static void getRecord_TestNegative() {
		EP_Credit_Exception_Request__c creditExceptionQueried;
		EP_CreditExceptionRequestMapper creditExceptionMapper = new EP_CreditExceptionRequestMapper();
		try {
			creditExceptionQueried = creditExceptionMapper.getRecord(null);
			
		}
		catch(Exception Ex) {
			System.debug('****'+ex.getMessage());
		}
		system.assertEquals(null,creditExceptionQueried);
	}


	@isTest
	static void getRecords_TestPositive() {
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditExceptionRequest();
		EP_CreditExceptionRequestMapper creditExceptionMapper = new EP_CreditExceptionRequestMapper();
		List<EP_Credit_Exception_Request__c> lstcreditExceptionQueried = creditExceptionMapper.getRecords(new Set<String>{creditException.Name});
		system.assertEquals(1,lstcreditExceptionQueried.size());
	}

	@isTest
	static void getRecords_TestNegative() {
		List<EP_Credit_Exception_Request__c> lstcreditExceptionQueried = new List<EP_Credit_Exception_Request__c>();
		EP_CreditExceptionRequestMapper creditExceptionMapper = new EP_CreditExceptionRequestMapper();
		try {
			lstcreditExceptionQueried = creditExceptionMapper.getRecords(null);
			
		}
		catch(Exception Ex) {
			System.debug('****'+ex.getMessage());
		}
		system.assertEquals(0,lstcreditExceptionQueried.size());
	}
}