/**
 * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
 * @name        : EP_Consignment
 * @CreateDate  : 05/02/2017
 * @Description : This class executes logic for Consignment
 * @Version     : <1.0>
 * @reference   : N/A
 */
 public class EP_Consignment extends EP_ConsignmentType{

  public EP_Consignment() {
  }

    /**  to fetch the record type of order
     *  @date      05/02/2017
     *  @name      findRecordType
     *  @param     EP_VendorManagement vmType
     *  @return    Id 
     *  @throws    NA
     */
     public override Id findRecordType(EP_VendorManagement vmType) {
      EP_GeneralUtility.Log('Public','EP_Consignment','findRecordType');
      return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.CONSIGNMENT_ORDER_RECORD_TYPE_NAME);
    }
    
    
    /** This method is used for credit check & since it is a Consignment type so it will return only False
   *  @date      05/02/2017
   *  @name      hasCreditIssue
   *  @param     Order objOrder
   *  @return    Boolean
   *  @throws    NA
   */  
   public override boolean hasCreditIssue(csord__Order__c objOrder) {
    EP_GeneralUtility.Log('Public','EP_Consignment','hasCreditIssue');
    return False;
  }
}