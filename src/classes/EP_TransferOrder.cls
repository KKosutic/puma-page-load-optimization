public class EP_TransferOrder extends EP_OrderType {

  public EP_TransferOrder(){
  }

  public EP_TransferOrder(EP_OrderDomainObject orderDomainObject){
    loadStrategies(orderDomainObject);
  }
  
     /** This method is returns Accounts for TransferOrder
     *  @date      15/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
     public override List<Account> getShipTos(Id accountId) {
      EP_GeneralUtility.Log('Public','EP_TransferOrder','getShipTos');

      EP_AccountMapper accountMapper = new EP_AccountMapper();
      return accountMapper.getShipToStorageLocations(accountId);  
    }
    
    
    /** returns record recordype for the Order
      *  @date      22/02/2017
      *  @name      findRecordType
      *  @param     NA
      *  @return    Id
      *  @throws    NA
      */
      public override Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_TransferOrder','findRecordType');

        return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.TRANSFER_ORDER_RECORD_TYPE_NAME);
      }

    /** This method is returns the applicable delivery type for the transfer order.
     *  @date      26/02/2017
     *  @name      getApplicableDeliveryTypes
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */ 
     public override List<String> getApplicableDeliveryTypes(){
      EP_GeneralUtility.Log('Public','EP_TransferOrder','getApplicableDeliveryTypes');
      List<String> listOfPaymentTerms = new List<String>();
      listOfPaymentTerms.add(EP_Common_Constant.DELIVERY);
      return listOfPaymentTerms;
    }
    
    
    /** This method returns Pricbook Entries for the Order
      *  @date      24/02/2017
      *  @name      findPriceBookEntries
      *  @param     NA
      *  @return    NA
      *  @throws    NA
      */
      public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_TransferOrder','findPriceBookEntries');
        
        return deliverytype.findPriceBookEntries(pricebookId,objOrder);
      }
      
    /** checks if Order Date is Valid or not
      *  @date      25/02/2017
      *  @name      isValidOrderDate
      *  @param     NA
      *  @return    NA
      *  @throws    NA
      */
      public override csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
        EP_GeneralUtility.Log('Public','EP_TransferOrder','setRequestedDateTime');

        return epochType.setRequestedDateTime(orderObj,selectedDate,availableSlots);
      }
      
      
    /** return delivery Types for the Order
      *  @date      28/02/2017
      *  @name      getDeliveryTypes
      *  @param     NA
      *  @return    NA
      *  @throws    NA
      */
      public override List<String> getDeliveryTypes() {
        EP_GeneralUtility.Log('Public','EP_TransferOrder','getDeliveryTypes');
        
        List<String> listDeliveryTypes = new List<String>();
        listDeliveryTypes.add(EP_Common_Constant.DELIVERY);
        return listDeliveryTypes;
      } 

     /** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public override Boolean isOrderEntryValid(integer numOfTransportAccount){
      EP_GeneralUtility.Log('Public','EP_TransferOrder','isOrderEntryValid');
      return deliverytype.isOrderEntryValid(numOfTransportAccount);
    }


    /** returns routes of ship to and location
     *  @date      22/02/2017
     *  @name      getAllRoutesOfShipToAndLocation
     *  @param     Id shipToId,Id stockholdinglocationId
     *  @return    List<EP_Route__c>
     *  @throws    NA
     */  
     public override List<EP_Route__c> getAllRoutesOfShipToAndLocation(Id shipToId,Id stockholdinglocationId) {
      EP_GeneralUtility.Log('Public','EP_TransferOrder','getAllRoutesOfShipToAndLocation');
      System.debug('************shipToId:---'+shipToId+'****stockholdinglocationId*'+stockholdinglocationId);
      return deliverytype.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId); 
    }

     /** checks Order Credit 
     *  @date      05/02/2017
     *  @name      hasCreditIssue
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public override boolean hasCreditIssue() {
        EP_GeneralUtility.Log('Public','EP_TransferOrder','hasCreditIssue');
        return false;
    }
    
    //Defect #57769 Start
    /** This method is used to populate "Pipeline" in delivery type when default trannsporter is "No Transporter"
    *  @date      08/05/2017
    *  @name      checkForNoTransporter
    *  @param     String transporterName
    *  @return    Order
    *  @throws    NA
    */ 
    public override csord__Order__c checkForNoTransporter(String transporterName) {
        EP_GeneralUtility.Log('Public','EP_SalesOrder','checkForNoTransporter');
        return deliverytype.checkForNoTransporter(orderObj,transporterName);
    }
    //Defect #57769 End
    
    //Defect Start - #57852
    public override Boolean isValidExpectedDate(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_TransferOrder','isValidExpectedDate');
        return epochType.isValidExpectedDate(orderObj);
    }
    public override Boolean isValidOrderDate(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_TransferOrder','isValidOrderDate');
        return epochType.isValidOrderDate(orderObj);
    }
    
    public override Boolean isValidLoadingDate(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_TransferOrder','isValidLoadingDate');
        return epochType.isValidLoadingDate(orderObj);
    }
    //Defect End - #57852
  }