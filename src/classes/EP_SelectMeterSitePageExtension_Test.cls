///* 
//  @Author <Spiros Markantonatos>
//   @name <EP_SelectMeterSitePageExtensionClass>
//   @CreateDate <06/11/2014>
//   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
//   @Version <1.0>
//*/
//
//@isTest
public class EP_SelectMeterSitePageExtension_Test{
//    private static user currentrunningUser;
//    private static List<Account> accList; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    private static EP_Meter__c ExistingMeter; 
//    private Static Product2 currentProduct;
//    /**************************************************************************
//    *@Description : This method is used to create Data.                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();   
//         accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//         TankInstance= EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//         Account acc =accList.get(0);
//         acc.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         update acc;
//         currentProduct=EP_TestDataUtility.createTestRecordsForProduct();
//         ExistingMeter=EP_TestDataUtility.createTestRecordsForMeter(String.valueof(accList.get(0).id),currentProduct.id);
//    }
//    
//    /**************************************************************************
//    *@Description : This method is used to test sites available to user       *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void testUserSiteData(){ 
//     init();   
//       Test.startTest();
//         system.runas(currentrunningUser){     
//             ApexPages.currentPage().getParameters().put('id', accList[0].Id);
//             EP_SelectMeterSitePageExtensionClass controller = new EP_SelectMeterSitePageExtensionClass();
//             controller.retrieveUserSites();
//             controller.cancel();
//             system.assertEquals(controller.lstAvailableSites.size(),1);
//         }   
//                   
//       Test.stopTest();  
//     
//   
//    }    
//    
//        
}