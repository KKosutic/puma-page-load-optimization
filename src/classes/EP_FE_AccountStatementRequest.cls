/* 
   @Author <Nicola Tassini>
   @name <EP_FE_AccountStatementRequest>
   @CreateDate <30/06/2016>
   @Description <Request for the Account Statement API>  
   @Version <1.0>
*/
global with sharing class EP_FE_AccountStatementRequest {

	public Date dateFrom {get; set;} 
	public Date dateTo {get; set;} 
	public String type {get; set;}
	public String billToId {get; set;} 

	//public EP_FE_AccountStatementRequest() {}

}