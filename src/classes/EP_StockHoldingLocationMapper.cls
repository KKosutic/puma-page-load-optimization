/**
   @Author          Shakti Mehtani
   @name            EP_StockHoldingLocationMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_Stock_Holding_Location__c Object
   @Version         1.0
   @Nova suite fixes -- hard coding and commenting done
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    12/29/2016          Shakti Mehtani          1) Added method getDefaultRecordsByIds()
    10/01/2017          Swapnil Gorane          Added Method -> getSupplyOptionsRecordsAsPriLocation(set<id> , String )
                                                                getSupplyOptionsRecordsFromOrderList(Set<Id> )
                                                                getSupplyOptionsRecordswithTrnpDetails(Set<Id> )
                                                                getStockHoldingLocationWithDefaultRoute(Set<Id> )
    02/23/2017          Sandeep Kumar           Added Method -> getActiveStockHoldingLocationByShipToId(String shipToId) 
                                                                getActiveStockHoldingLocationBySellToId(String sellToId)
    *************************************************************************************************************
    */
    public with sharing class EP_StockHoldingLocationMapper {
    public static final String EXPRESSION_ZERO = 'expr0';//Novasuite Fix
    /**
    *  Constructor. 
    *  @name            EP_StockHoldingLocationMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */   
    public EP_StockHoldingLocationMapper() {

    }

    //Bulkification Implementation -- Start
    Map<Id,Integer> accountIdCountMap = new Map<Id,Integer>();
    /* *  
    *  @date            12/28/2016
    *  @name            EP_StockHoldingLocationMapper
    *  @param           Set<Id>
    *  @throws          NA
    */
    public EP_StockHoldingLocationMapper(Set<Id> accountIds) {
        accountIdCountMap = getCountOfPrimaryStockHoldingLocationsBulk(accountIds);
    }
    //Bulkification Implementation -- End
        
   /* *  This method Returns Count of sellTo supplyoptions under a sellTo Record
    *  @author          Amit Singh
    *  @date            12/28/2016
    *  @name            getSellToSupplyOptionCount
    *  @param           Account 
    *  @return          Integer
    *  @throws          NA
    */
    public Integer getSellToSupplyOptionCount(Account account){
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getSellToSupplyOptionCount');
        return [Select count() From EP_Stock_Holding_Location__c 
        Where EP_Sell_To__c =:account.Id];
    }
    
    /* *  This method Returns Records By Ids.
    *  @author          Shakti Mehtani
    *  @date            12/28/2016
    *  @name            getRecordsByIds
    *  @param           set<id>, Integer 
    *  @return          list<EP_Stock_Holding_Location__c> 
    *  @throws          NA
    */
    public list<EP_Stock_Holding_Location__c> getRecordsByIds(set<id> idSet, Integer limitVal) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getRecordsByIds');
        list<EP_Stock_Holding_Location__c> shlList = new list<EP_Stock_Holding_Location__c>();

        for(list<EP_Stock_Holding_Location__c> stockHoldingLocObjList: [    SELECT 
            ID
            , Stock_Holding_Location__r.Name
            , Stock_Holding_Location__r.EP_Slotting_Enabled__c
            , EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c
            , Stock_Holding_Location__r.EP_Business_Hours__c
            , Stock_Holding_Location__r.EP_Country__c
            , Stock_Holding_Location__r.EP_Scheduling_Hours__c
            , EP_Trip_Duration__c
            , EP_Transporter__r.Name
            , EP_Transporter__r.Id
            FROM EP_Stock_Holding_Location__c
            WHERE id IN :idSet
                                                            //limit :limitVal
                                                            ]) {
            shlList.addAll(stockHoldingLocObjList);
        }
        return shlList;
    }
    
    /* *  This method Returns Records By Ids.
    *  @author          Aravindhan Ramalingam
    *  @date            14/02/2017
    *  @name            getRecordById
    *  @param           Id as String 
    *  @return          EP_Stock_Holding_Location__c
    *  @throws          NA
    */
    public EP_Stock_Holding_Location__c getRecordsById(String stockHoldingId) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getRecordsById');


        EP_Stock_Holding_Location__c stockHoldingLocRecord = [SELECT 
        ID
        , Stock_Holding_Location__r.Name
        , Stock_Holding_Location__r.EP_Slotting_Enabled__c
        , EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c
        , Stock_Holding_Location__r.EP_Business_Hours__c
        , Stock_Holding_Location__r.EP_Country__c
        , Stock_Holding_Location__r.EP_Scheduling_Hours__c
        , EP_Trip_Duration__c
        , EP_Transporter__r.Id
        , EP_Transporter__r.Name
        FROM EP_Stock_Holding_Location__c
        WHERE id = :stockHoldingId
        ];
        return stockHoldingLocRecord;
    }
   
    /** This method returns the count of Primary Stock Holding Locations for ship To accounts.
    *  @name             getCountofPrimaryLocations
    *  @param            set<id>, Integer
    *  @return           list<AggregateResult>
    *  @throws           NA
    */ 
    public list<AggregateResult> getCountofPrimaryLocations(set<id> idSet, Integer limitVal) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getCountofPrimaryLocations');
        list<AggregateResult> aggResList = new list<AggregateResult>();
        for(list<AggregateResult> aggrResultList: [SELECT 
            EP_Ship_To__c
            , count(Id) noOfPrimLocations
            FROM  EP_Stock_Holding_Location__c
            WHERE EP_Location_Type__c = :EP_Common_Constant.PRIMARY
            AND EP_Ship_To__c IN :idSet        
            GROUP BY EP_Ship_To__c 
            ]){
            aggResList.addAll(aggrResultList);  
        }                                      
        return aggResList;
    }     

    /********************** Removed Limit *****************************/

        /* *  Returns Records By Ids.
    *  @author          Shakti Mehtani
    *  @date            12/28/2016
    *  @name            getRecordsByIds
    *  @param           set<id>
    *  @return          list<EP_Stock_Holding_Location__c>
    *  @throws          NA
    */
    public list<EP_Stock_Holding_Location__c> getRecordsByIds(set<id> idSet) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getRecordsByIds');
        list<EP_Stock_Holding_Location__c> shlList = new list<EP_Stock_Holding_Location__c>();
        for(list<EP_Stock_Holding_Location__c> stockHoldingLocObjList: [    SELECT 
            ID
            , Stock_Holding_Location__r.Name
            , Stock_Holding_Location__r.EP_Slotting_Enabled__c
            , EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c
            , Stock_Holding_Location__r.EP_Business_Hours__c
            , Stock_Holding_Location__r.EP_Country__c
            , Stock_Holding_Location__r.EP_Scheduling_Hours__c
            , EP_Trip_Duration__c
            FROM EP_Stock_Holding_Location__c
            WHERE id IN :idSet
            ]) { 
            shlList.addAll(stockHoldingLocObjList);
        }                        
        return shlList;
    }    
    
    /** This method returns the count of Primary Stock Holding Locations for ship To accounts.
    *  @name             getCountofPrimaryLocations
    *  @param            set<id>
    *  @return           list<AggregateResult>
    *  @throws           NA
    */ 
    public list<AggregateResult> getCountofPrimaryLocations(set<id> idSet) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getCountofPrimaryLocations');
        list<AggregateResult> aggResList = new list<AggregateResult>();
        for(list<AggregateResult> aggrResultList: [SELECT 
            EP_Ship_To__c
            , count(Id) noOfPrimLocations
            FROM  EP_Stock_Holding_Location__c
            WHERE EP_Location_Type__c = :EP_Common_Constant.PRIMARY
            AND EP_Ship_To__c IN :idSet        
            GROUP BY EP_Ship_To__c 
            ]){
            aggResList.addAll(aggrResultList);   
        }                                      
        return aggResList;
    }
    
     /** This method is used to get Stock Holding Location for ShipIds provided
    *  @author          Kalpesh Thakur
    *  @date            02/02/2017
    *  @name            getStockHoldingLocationByIds
    *  @param           Set<Id> setShipToIds
    *  @return          List<EP_Stock_Holding_Location__c> 
    *  @throws          NA
    */
    public List<EP_Stock_Holding_Location__c> getStockHoldingLocationByIds(Set<Id> setShipToIds ) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getStockHoldingLocationByIds');
        
        List<EP_Stock_Holding_Location__c> lstStockHoldLoc = [SELECT Id,EP_Ship_To__c,
        Stock_Holding_Location__c,
        Stock_Holding_Location__r.Name,
        Stock_Holding_Location__r.EP_Country__c
        FROM EP_Stock_Holding_Location__c 
        WHERE EP_Ship_To__c IN : setShipToIds
        AND Stock_Holding_Location__r.EP_Status__c =: EP_Common_Constant.STATUS_ACTIVE]; 
        //AND EP_Location_Type__c =: EP_Common_Constant.primaryLoc];
        return lstStockHoldLoc;
    }

    /** This method is used to get Active Stock Holding Location for sellTo provided
    *  @author          Kalpesh Thakur
    *  @date            02/23/2017
    *  @name            getStockHoldingLocationById
    *  @param           String, Integer
    *  @return          List<EP_Stock_Holding_Location__c> 
    *  @throws          NA
    */
    public List<EP_Stock_Holding_Location__c> getActiveStockHoldingLocationBySellToId(String sellToId) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getActiveStockHoldingLocationBySellToId');
        System.debug('*****sellToId'+sellToId);
        List<EP_Stock_Holding_Location__c> lstStockHoldLoc = [SELECT ID, Name, EP_Ship_To__c,Stock_Holding_Location__r.Name,EP_Stock_Holding_Location_Order__c,Stock_Holding_Location__r.EP_Status__c,Stock_Holding_Location__c,Stock_Holding_Location__r.EP_Country__c
        FROM EP_Stock_Holding_Location__c
        WHERE EP_Sell_To__c = :sellToId
        AND Stock_Holding_Location__r.EP_Status__c =: EP_Common_Constant.STATUS_ACTIVE
        AND EP_Is_Pickup_Enabled__c = TRUE 
        ORDER BY EP_Stock_Holding_Location_Order__c ASC ];

        return lstStockHoldLoc;
    }

    //Bulkification Implementation -- Start
    
    /** This method is used to get Active Stock Holding Location for sellTo provided
    *  @author          Accenture
    *  @date            03/02/2017
    *  @name            getCountOfPrimaryStockHoldingLocations
    *  @param           Account
    *  @return          integer 
    *  @throws          NA
    */
    public integer getCountOfPrimaryStockHoldingLocations(Account account){
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getCountOfPrimaryStockHoldingLocations');
        if (accountIdCountMap.containsKey(account.Id)) {

            return accountIdCountMap.get(account.Id);
        }

        return [Select count() From EP_Stock_Holding_Location__c Where EP_Location_Type__c =: EP_Common_Constant.PRIMARY AND  EP_Ship_To__c =:account.Id ];
    }
    

    /** This method is used to get Active Stock Holding Location for sellTo provided
    *  @author          Accenture
    *  @date            09/29/2017
    *  @name            getCountOfPrimaryStockHoldingLocationsBulk
    *  @param           Set<Id> accountIds
    *  @return          map<Id,Integer> 
    *  @throws          NA
    */
    public map<Id,Integer> getCountOfPrimaryStockHoldingLocationsBulk(Set<Id> accountIds){
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getCountOfPrimaryStockHoldingLocationsBulk');

        map<Id,Integer> mapAccountIdCount = new Map<Id,Integer>();
        for (AggregateResult groupedResult :[SELECT EP_Ship_To__c,count(Id) FROM EP_Stock_Holding_Location__c WHERE EP_Location_Type__c =: EP_Common_Constant.PRIMARY AND  EP_Ship_To__c =:accountIds GROUP BY EP_Ship_To__c]){
            mapAccountIdCount.put(String.valueOf(groupedResult.get(EP_Common_Constant.TANK_SHIP_TO_API_NAME)),Integer.valueOf(groupedResult.get(EXPRESSION_ZERO)));
        }
         return mapAccountIdCount;
    }
    //Bulkification Implementation -- End

    /*Added by Rahul Jain*/
    public list<EP_Stock_Holding_Location__c> getSHLByNavStockLocationId(set<string> stockLocationIdSet, 
        string companyCode ) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getSHLByNavStockLocationId');
        List<EP_Stock_Holding_Location__c> stockHolLocationObjList = new List<EP_Stock_Holding_Location__c>();
        for(list<EP_Stock_Holding_Location__c> stockLocation : [Select Id
            ,Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c
            ,Stock_Holding_Location__r.EP_Puma_Company_Code__c
            ,Stock_Holding_Location__c
            ,EP_Ship_To_Id__c
            From EP_Stock_Holding_Location__c   
            Where
            Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c IN :stockLocationIdSet 
            AND Stock_Holding_Location__r.EP_Puma_Company_Code__c =: companyCode
            ]){
            stockHolLocationObjList.addAll(stockLocation);  
        }
        return stockHolLocationObjList;
    }
    /*Added by Rahul Jain*/
    public list<EP_Stock_Holding_Location__c> getStockLocationByIdForShipTo(string StockLocId, string ShipToId ) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getStockLocationByIdForShipTo');
        List<EP_Stock_Holding_Location__c> stockHolLocationObjList = new List<EP_Stock_Holding_Location__c>();
        for(list<EP_Stock_Holding_Location__c> stockLocation : [Select id 
            from 
            EP_Stock_Holding_Location__c 
            where 
            EP_Stock_Location_Id__c =: StockLocId 
            AND EP_Ship_To__c =: ShipToId ]) {
            stockHolLocationObjList.addAll(stockLocation);  
        }
        return stockHolLocationObjList;
    }
    /**
    * @Author       Accenture
    * @Name         getStockHoldingLocation
    * @Param        Id     
    * @return       EP_Stock_Holding_Location__c
    */
    public EP_Stock_Holding_Location__c getStockHoldingLocation(Id stockLocationId) {

        return [SELECT Id,Stock_Holding_Location__r.Id FROM EP_Stock_Holding_Location__c WHERE Id=: stockLocationId];
    }
    /**
    * @Author       Accenture
    * @Name         getStockHoldingLocations
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    public map<String,EP_Stock_Holding_Location__c> getStockHoldingLocations(set<string> sStockLocationId, string companyCode) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewStub','getStockHoldingLocations');
        map<String,EP_Stock_Holding_Location__c> storageStockLocationMap = new map <String,EP_Stock_Holding_Location__c>();
        for(EP_Stock_Holding_Location__c stockLocation : getSHLByNavStockLocationId(sStockLocationId, companyCode)){
            storageStockLocationMap.put(stockLocation.Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c,stockLocation);  
        }
        return storageStockLocationMap;
    }
    /**
    * @Author       Accenture
    * @Name         getTransportDetails
    * @Date         03/24/2017
    * @Description  
    * @Param        Id
    * @return       EP_Stock_Holding_Location__c
    */
    public EP_Stock_Holding_Location__c getTransportDetails(Id stockLocationId) {

        return [SELECT EP_Transporter__c , EP_Use_Managed_Transport_Services__c FROM EP_Stock_Holding_Location__c  WHERE Id =:stockLocationId];
    }
        
        //L4_45352_Start
    /** This method is used to get Pickup enabled Stock Holding Location for sellTo provided
    *  @author          Bhushan Adhikari
    *  @date            11/14/2017
    *  @name            getPickupEnabledLocationBySellToId
    *  @param           String
    *  @return          List<EP_Stock_Holding_Location__c> 
    *  @throws          NA
    */
    public List<EP_Stock_Holding_Location__c> getPickupEnabledLocationBySellToId(String sellToId) {
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getPickupEnabledLocationBySellToId');
        List<EP_Stock_Holding_Location__c> lstStockHoldLoc = [SELECT ID, Name,EP_Sell_To__c, EP_Ship_To__c,Stock_Holding_Location__r.Name,
                                                              EP_Stock_Holding_Location_Order__c,Stock_Holding_Location__r.EP_Status__c,
                                                              Stock_Holding_Location__c,Stock_Holding_Location__r.EP_Country__c
        FROM EP_Stock_Holding_Location__c
        WHERE EP_Sell_To__c = :sellToId
        AND Stock_Holding_Location__r.EP_Status__c =: EP_Common_Constant.STATUS_ACTIVE
        AND EP_Is_Pickup_Enabled__c = TRUE 
        ORDER BY EP_Stock_Holding_Location_Order__c ASC ];

        return lstStockHoldLoc;
    }
    
    //L4_45352_START
     /** This method is used to get Primary Syorage locations of ship to
    *  @author          Accenture
    *  @date            11/14/2017
    *  @name            getPrimaryStockLoactionsForShipTos
    *  @param           List<Account>
    *  @return          List<EP_Stock_Holding_Location__c> 
    *  @throws          NA
    */
    public list<EP_Stock_Holding_Location__c> getPrimaryStockLoactionsForShipTos(List<Account> shipToAccount){
        EP_GeneralUtility.Log('Public','EP_StockHoldingLocationMapper','getPrimaryStockLoactionsForShipTos');
        List<EP_Stock_Holding_Location__c> lstStockHoldLoc = [SELECT ID, Name, EP_Ship_To__c,Stock_Holding_Location__c,EP_Ship_To__r.ParentId,EP_Location_Type__c
                                                                FROM EP_Stock_Holding_Location__c
                                                                WHERE EP_Location_Type__c = :EP_Common_Constant.PRIMARY
                                                                    AND EP_Ship_To__c IN :shipToAccount];
        return lstStockHoldLoc;
    }
    
    //L4_45352_END
}