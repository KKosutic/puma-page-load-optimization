/* 
   @Author <Amit Singh>
   @name <EP_ProductListTriggerHandler>
   @CreateDate <19/09/2016>
   @Description <This is helper class of EP_ProdutListTrigger Trigger> 
   @Version <1.0>
*/
public with sharing class EP_ProductListTriggerHandler {

    public static boolean isExecuteBeforeUpdate = False;
    private static final String CLASS_NAME = 'EP_ProductListTriggerHandler';
    private static final String doBeforeInsert = 'doBeforeInsert';
    private static final String doBeforeUpdate = 'doBeforeUpdate';
    
    /**
	 * @author <Amit Singh>
	 * @description <This method handles before insert requests from ProductList trigger>
	 * @name <doBeforeInsert>
	 * @date <19/09/2016>
	 * @param List<PriceBook2>
	 * @return void
	 */
    public static void doBeforeInsert(List<PriceBook2> listOfPriceBook){
        try{
        	//Validate Pricebook name- It should be unique
            EP_ProductListTriggerHelper.checkForUniqueName(listOfPriceBook, null);
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doBeforeInsert, CLASS_NAME,apexPages.severity.ERROR);
        }
    }
    
    /**
	 * @author <Amit Singh>
	 * @description <This method handles before update requests from ProductList trigger>
	 * @name <doBeforeUpdate>
	 * @date <19/09/2016>
	 * @param List<PriceBook2>, Map<Id, PriceBook2>
	 * @return void
	 */
    public static void doBeforeUpdate(List<PriceBook2> listOfPriceBook, 
                                      Map<Id, PriceBook2> mapOfIdOldPricebooks){
        try{
            isExecuteBeforeUpdate = true;
            //Validate Pricebook name- It should be unique
            EP_ProductListTriggerHelper.checkForUniqueName(listOfPriceBook, mapOfIdOldPricebooks);
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doBeforeUpdate, CLASS_NAME,apexPages.severity.ERROR);
        }
    }
}