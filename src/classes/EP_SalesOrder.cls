/**
* @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
* @name        : EP_SalesOrder
* @CreateDate  : 05/02/2017
* @Description : This class is invokes Logic for Sales Order
* @Version     : <1.0>
* @reference   : N/A
*/
public class EP_SalesOrder extends EP_OrderType {

	public EP_SalesOrder(){
	}

	 public EP_SalesOrder(EP_OrderDomainObject orderDomainObject) {
      loadStrategies(orderDomainObject);
    }
    
	/** This perform all doSubmitActions
	*  @date      05/02/2017
	*  @name      doSubmitActions
	*  @param     Order objOrder
	*  @return    NA
	*  @throws    NA
	*/  
	public override void doSubmitActions() {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','doSubmitActions');
		doUpdatePaymentTermAndMethod();
	}


	/** This perform update payment fields
	*  @date      05/02/2017
	*  @name      updatePaymentTermAndMethod
	*  @param     Order objOrder
	*  @return    NA
	*  @throws    NA
	*/
	public override void doUpdatePaymentTermAndMethod() {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','doUpdatePaymentTermAndMethod');
		vmiType.doUpdatePaymentTermAndMethod(orderObj);
	}


	/** This perform check Order Credit
	*  @date      05/02/2017
	*  @name      hasCreditIssue
	*  @param     NA
	*  @return    NAs
	*  @throws    NA
	*/
	public override Boolean hasCreditIssue() {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','hasCreditIssue');
		Boolean hasCreditIssues = consignmentType.hasCreditIssue(orderObj);
		return hasCreditIssues;
	}


	/** This method is returns ShipTos Accounts for Order Epoch Type
	*  @date      16/02/2017
	*  @name      getShipTos
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override List<Account> getShipTos(Id accountId) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','getShipTos');
		List<Account> lstAccount = new List<Account>();
		//filter based on deliverytype
		lstAccount = deliverytype.getShipTos(accountId);
		// filter based on Order Epoch
		lstAccount = epochType.getShipTos(lstAccount,productSoldAsType);
		return lstAccount;
	}


	/** This method getOperationalTanks Based on the Order 
	*  @date      22/02/2017
	*  @name      getOperationalTanks
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override Map<Id, EP_Tank__c> getOperationalTanks(Id accountId) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','getOperationalTanks');
		Map<Id, EP_Tank__c> mapOperationalTanks = new Map<Id, EP_Tank__c>();
		//filter based on deliverytype
		mapOperationalTanks = deliverytype.getOperationalTanks(accountId);
		mapOperationalTanks = productSoldAsType.getTanksForBulkProduct(mapOperationalTanks);
		return mapOperationalTanks;
	}


	/** to validate the Loading Date
	*  @date      22/02/2017
	*  @name      isValidLoadingDate
	*  @param     orderObj
	*  @return    Boolean
	*  @throws    NA
	*/
	public override Boolean isValidLoadingDate(csord__Order__c orderObject){
		EP_GeneralUtility.Log('Public','EP_SalesOrder','isValidLoadingDate');
		 return epochType.isValidLoadingDate(orderObject);
	}


	/** to validate Expected Date
	*  @date      22/02/2017
	*  @name      isValidExpectedDate
	*  @param     orderObj
	*  @return    NA
	*  @throws    NA
	*/
	public override Boolean isValidExpectedDate(csord__Order__c orderObject){
		EP_GeneralUtility.Log('Public','EP_SalesOrder','isValidExpectedDate');
		return epochType.isValidExpectedDate(orderObject);
	} 


	/** returns routes of ship to and location
	*  @date      22/02/2017
	*  @name      getAllRoutesOfShipToAndLocation
	*  @param     Id shipToId,Id stockholdinglocationId
	*  @return    List<EP_Route__c>
	*  @throws    NA
	*/
	public override List<EP_Route__c> getAllRoutesOfShipToAndLocation(Id shipToId,Id stockholdinglocationId) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','getAllRoutesOfShipToAndLocation');
		return deliverytype.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId); 
	}


	/** returns record recordype for the Order
	*  @date      22/02/2017
	*  @name      findRecordType
	*  @param     NA
	*  @return    Id
	*  @throws    NA
	*/
	public override Id findRecordType(){
		EP_GeneralUtility.Log('Public','EP_SalesOrder','findRecordType');
		return deliverytype.findRecordType(epochType,vmiType,consignmentType);
	}


	/** This method returns the applicable type of deliveries for the order.
	*  @date      26/02/2017
	*  @name      getApplicableDeliveryTypes
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/  
	public override List<String> getApplicableDeliveryTypes(){
		EP_GeneralUtility.Log('Public','EP_SalesOrder','getApplicableDeliveryTypes');
		List<String> listOfPaymentTerms = new List<String>();
		listOfPaymentTerms.add(orderObj.EP_Delivery_Type__c);       
		// if active supply location found on delivery type Customer then give option of "Ex-Rack" as well
		if (orderObj.EP_Delivery_Type__c.equalsIgnoreCase(EP_Common_Constant.DELIVERY)) {
			listOfPaymentTerms.add(EP_Common_Constant.EX_RACK);
		}
		// if active supply location found on Ex-Rack Type Customer then give option of "Delivery" as well
		else if ((orderObj.EP_Delivery_Type__c.equalsIgnoreCase(EP_Common_Constant.EX_RACK))) {
			listOfPaymentTerms.add(EP_Common_Constant.DELIVERY);
		}
		return listOfPaymentTerms;
	}


	/** checks if Order Date is Valid or not
	*  @date      25/02/2017
	*  @name      isValidOrderDate
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override Boolean isValidOrderDate(csord__Order__c orderObj) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','isValidOrderDate');
		return epochType.isValidOrderDate(orderObj);
	}


	/** This method returns Pricbook Entries for the Order
	*  @date      24/02/2017
	*  @name      findPriceBookEntries
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c orderObj) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','findPriceBookEntries');
		return deliverytype.findPriceBookEntries(pricebookId,orderObj);
	}
	

	/** checks if Order Date is Valid or not
	*  @date      25/02/2017
	*  @name      isValidOrderDate
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','setRequestedDateTime');
		return epochType.setRequestedDateTime(orderObj,selectedDate,availableSlots);

	}


	/** return delivery Types for the Order
	*  @date      28/02/2017
	*  @name      getDeliveryTypes
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override List<String> getDeliveryTypes() {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','getDeliveryTypes');
		List<String> listDeliveryTypes = new List<String>();
		listDeliveryTypes.add(EP_Common_Constant.EX_RACK);
		listDeliveryTypes.add(EP_Common_Constant.DELIVERY);
		return listDeliveryTypes;
	}


	/** calculates Price for the Order
	*  @date      28/02/2017
	*  @name      calculatePrice
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public override void calculatePrice() {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','calculatePrice');
		System.debug('****consignmentType:--'+consignmentType);
		consignmentType.calculatePrice(orderObj);
	}

	/** This method is used to populate "Pipeline" in delivery type when default trannsporter is "No Transporter"
	*  @date      02/03/2017
	*  @name      checkForNoTransporter
	*  @param     String transporterName
	*  @return    Order
	*  @throws    NA
	*/ 
	public override csord__Order__c checkForNoTransporter(String transporterName) {
		EP_GeneralUtility.Log('Public','EP_SalesOrder','checkForNoTransporter');
		return deliverytype.checkForNoTransporter(orderObj,transporterName);
	}

	/** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public override Boolean isOrderEntryValid(integer numOfTransportAccount){
     	EP_GeneralUtility.Log('Public','EP_SalesOrder','isOrderEntryValid');
     	return deliverytype.isOrderEntryValid(numOfTransportAccount);
     }
 }