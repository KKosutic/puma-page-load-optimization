@isTest
public class EP_AccountSyncCtrlExtn_UT
{
	@testSetup static void init() {
      List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    }
	
    static testMethod void setHeader_test() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        PageReference pageRef = Page.EP_AccountSyncXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.Id);
        EP_AccountSyncCtrlExtn localObj = new EP_AccountSyncCtrlExtn(sc);
        Test.startTest();
        localObj.setHeader();
        Test.stopTest();
        System.AssertEquals(true,localObj.headerObj.MsgID.equalsIgnoreCase(EP_Common_Constant.TEMPSTRINGWITHHYPHEN));
        System.AssertEquals(true,localObj.headerObj.SourceCompany.equalsIgnoreCase(EP_Common_Constant.TEMPSTRINGWITHHYPHEN));
    }
    static testMethod void setPayload_test() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        PageReference pageRef = Page.EP_AccountSyncXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.Id);
        EP_AccountSyncCtrlExtn localObj = new EP_AccountSyncCtrlExtn(sc);
        Test.startTest();
        localObj.setPayload();
        Test.stopTest();
        System.AssertEquals(true,localObj.encodedPayload != null);
    }
    static testMethod void checkPageAccess_test() {//vs: updated
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        EP_AccountSyncCtrlExtn localObj = new EP_AccountSyncCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result != NULL);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
}