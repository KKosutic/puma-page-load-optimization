@isTest
public class EP_VendorSM_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void getAccountState_test() {
        EP_VendorSM localObj = new EP_VendorSM();
        EP_AccountMapper accountMapperObj = new EP_AccountMapper();
        Account vendorObj = accountMapperObj.getAccountRecord(EP_TestDataUtility.getVendor().id);
        EP_AccountDomainObject obj = new EP_AccountDomainObject(vendorObj);
        localObj.setAccountDomainObject(obj);
        localObj.accountEvent = new EP_AccountEvent(EP_AccountConstant.NEW_RECORD);
        Test.startTest();
        EP_AccountState result = localObj.getAccountState();
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }
}