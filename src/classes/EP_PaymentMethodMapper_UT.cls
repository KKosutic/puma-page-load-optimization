@isTest
private class EP_PaymentMethodMapper_UT {

  static testMethod void getDirectDebitCountById_test() {
      EP_PaymentMethodMapper localObj = new EP_PaymentMethodMapper();
      EP_Payment_Method__c paymentMethodObj = EP_TestDataUtility.createPaymentMethod();
      insert paymentMethodObj;
      Set<Id> setIds = new Set<id>{paymentMethodObj.id};
      Test.startTest();
      Integer result = localObj.getDirectDebitCountById(setIds);  
      Test.stopTest();
      System.assertEquals(0,result);
  }

  static testMethod void getRecordsByName_test() {
      EP_PaymentMethodMapper localObj = new EP_PaymentMethodMapper();
      EP_Payment_Method__c paymentMethodObj = EP_TestDataUtility.createPaymentMethod();
      insert paymentMethodObj;
      String paymentMethodNameStr = paymentMethodObj.Name;
      Test.startTest();
      list<EP_Payment_Method__c> result = localObj.getRecordsByName(paymentMethodNameStr);  
      Test.stopTest();
      System.assertEquals(1,result.size());
  }

  static testMethod void getRecordsByPaymentMethodCode_test() {
      EP_PaymentMethodMapper localObj = new EP_PaymentMethodMapper();
      EP_Payment_Method__c paymentMethodObj = EP_TestDataUtility.createPaymentMethod();
      insert paymentMethodObj;
      Set<String> setPaymentMethod = new Set<String>{paymentMethodObj.EP_Payment_Method_Code__c};
      Test.startTest();
      list<EP_Payment_Method__c> result = localObj.getRecordsByPaymentMethodCode(setPaymentMethod);  
      Test.stopTest();
      System.assertEquals(1,result.size());
  }

  static testMethod void getPaymentMethodsMapByCodes_test() {
      EP_PaymentMethodMapper localObj = new EP_PaymentMethodMapper();
      EP_Payment_Method__c paymentMethodObj = EP_TestDataUtility.createPaymentMethod();
      insert paymentMethodObj;
      Set<String> setPaymentMethod = new Set<String>{paymentMethodObj.EP_Payment_Method_Code__c};
      Test.startTest();
      Map<String,Id> result = localObj.getPaymentMethodsMapByCodes(setPaymentMethod);  
      Test.stopTest();
      System.assertEquals(1,result.size());
  }
}