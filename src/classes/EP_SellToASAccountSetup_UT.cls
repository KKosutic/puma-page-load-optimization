@isTest
public class EP_SellToASAccountSetup_UT
{

    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '08-ProspectTo08-Rejected';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        System.assert(localObj.account.getAccount().id != null);
        
    }
    //Delegates to other methods. Adding dummy assert
    static testMethod void doOnEntry_test() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnExit_test() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void doTransition_NegativeScenariotest() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        string errorMsg ;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }catch(Exception e){
            errorMsg = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(errorMsg));
    }
    
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_SellToASAccountSetup localObj = new EP_SellToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    
}