/*
   @Author <Pooja Dhiman>
   @name <EP_PaymentTriggerHelper>
   @Description <This class handles requests from EP_PaymentTriggerHandler class>
   @Version <1.0>
*/
public with sharing class EP_PaymentTriggerHelper {
    
    private static Id paymentRefundRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_REFUND_RECORD_TYPE_NAME);
    private static Id paymentRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_RECORD_TYPE_NAME);
    
    /**
     * This method is used to create Customer Account Statement Item once Payments are created
     */
    public static void createCustomerAccountStatmentItem(List<EP_Customer_Payment__c> lstPayment){
        for(EP_Customer_Payment__c oCustomerPayment : lstPayment){
            if(oCustomerPayment.RecordTypeId == paymentRecordTypeId){
                EP_CustomerPaymentHandler.createCustomerAccountStatementItem(lstPayment);
            }
            else if(oCustomerPayment.RecordTypeId == paymentRefundRecordTypeId){
                EP_CustomerPaymentRefundHandler.createCustomerAccountStatementItem(lstPayment);
            }
        }       
    }
}