/**
@Author <Pravindra Khatri>
@name <EP_Modify_Data_On_TankTest>
@CreateDate <23/12/2015>
@Description <This test class is used to cover the basic validation rules on Tank object>
@Version <1.0>
*/
@isTest
public class EP_Modify_Data_On_TankTest{
    private static testMethod void tank_validation(){
        Profile cscAgent = [Select id from Profile Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Product2 product = new Product2 (EP_Density__c = 10,EP_Fuel_Conversion_Ratio__c =2,EP_Product_Capacity_Colour__c = 'RED',EP_Product_Colour__c = 'BLACK',EP_Short_Name__c = 'PUMA',EP_Unit_of_Measure__c ='LT');
        database.insert(product,false);
        
        System.runAs(cscUser){
            EP_Tank__c tankObj= new EP_Tank__c(EP_Capacity__c = 200,EP_Safe_Fill_Level__c = 100,EP_Product__c = product.id ,EP_Deadstock__c=20,EP_Unit_Of_Measure__c='LT',EP_Tank_Status__c='Stopped',EP_Reason_Blocked__c= 'Credit Failed'); // EP_Reason_Blocked__c API changed
            Database.insert(tankobj,false);
        
            EP_Tank__c tankObj1 = new EP_Tank__c(EP_Capacity__c = 200,EP_Safe_Fill_Level__c = 100,EP_Product__c = product.id,EP_Deadstock__c=20,EP_Unit_Of_Measure__c='LT',EP_Tank_Status__c='Decomissioned');
            Database.insert(tankobj1,false);
        }
    }    
}