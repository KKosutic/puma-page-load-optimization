/* 
   @Author <Vinay Jaiswal>
   @name <EP_CustomerOpenItemsViewController>
   @CreateDate <15/09/2016>
   @Description <This Class will include the Methods which will be used across the code>
   @Version <1.0>
*/
public with sharing class EP_CustomerOpenItemsViewController {
    
    private Boolean isError {get;set;}
    public string sAccountName {get;set;}
    private string sAccountId {get;set;}
    private list<Account> lstAccount {get;set;} 
    public static list<WrapperCustomerOpenStatementItem> lstCSOIWrapper{get; set;}
    public static list<WrapperOpenInvoicesAndCreditMemos> lstOICMWrapper {get; set;}
    public static list<WrapperOpenOrders> lstOpenOrdWrapper {get; set;}
    public static Double totalOpenOrdersAmt{get;set;}
    public static Double dCalculateRemainaingTotal {get;set;}   
    private static list<String> sRecTypeDevName = new list<String>{EP_Common_Constant.CUSTOMER_INVOICE_RECORD_TYPE_NAME, EP_Common_Constant.CREDIT_MEMO_RECORD_TYPE_NAME};
    private static final String OBJ_ID = 'id';
    private static final String EXECUTION_ERROR = 'An Error occurred while executing this request.';
    private static final String TOTALREMAININGBAL = 'TotalRemainingBal';
    
    /**
     * Constructor method
     */
    public EP_CustomerOpenItemsViewController()
    {
        isError = false;
        try
        {
            initialize();
        }
        catch (Exception e)
        {
            isError = true;
            addPageMessages(ApexPages.Severity.Error, EXECUTION_ERROR);
        }
    }
    
    /**
     * initialize method used by constructor
     */
    private void initialize()
    {
        String objId = Apexpages.currentpage().getparameters().get(OBJ_ID);
        if (objId != null && string.isNotBlank(objId))
        {
            lstAccount = [select Id, Name from Account where ID =:objId limit: EP_Common_Constant.ONE ];
            if(!lstAccount.isEmpty())
            {
               sAccountName = lstAccount[0].Name;
               sAccountId = lstAccount[0].Id;
            }                                  
        }
        if(sAccountId <> null && string.isNotBlank(sAccountId))
        {
            getOpenOrders(sAccountId);
            getOpenInvoiceAndCreditMemo(sAccountId);
            getOpenItemSummary(sAccountId);
        }
    }
    
    /**
     * To retrieve Open Orders details.
     */
    private void getOpenOrders(String accId)
    {
        totalOpenOrdersAmt = 0.00;
        Integer nRows = EP_Common_Util.getQueryLimit();
        List<string> orderStatus = new List<String>{EP_Common_Constant.ORDER_STATUS_SUBMITTED, EP_Common_Constant.ORDER_ACCEPTED_STATUS, EP_Common_Constant.loaded, 
                                                    EP_Common_Constant.ORDER_PLANNING_STATUS, EP_Common_Constant.ORDER_PLANNED_STATUS, EP_Common_Constant.delivered,
                                                    EP_Common_Constant.ORDER_AWAITING_PAYMENT_STATUS, EP_Common_Constant.ORDER_AWAITING_CRED_REVIEW_STATUS,
                                                    EP_Common_Constant.ORDER_AWAITING_INVNTRY_REVIEW_STATUS};
        lstOpenOrdWrapper = new list<WrapperOpenOrders >();
        WrapperOpenOrders oWrapOpenOrders;
        for(Order oOrders : [Select OrderNumber, CurrencyIsoCode, TotalAmount from Order where AccountId =: accId 
                                and Status IN : orderStatus  order by OrderNumber Asc Limit : nRows])
        {
            oWrapOpenOrders = new WrapperOpenOrders();
            oWrapOpenOrders.wrapOrderId = oOrders.id;
            oWrapOpenOrders.wrapOrderNumber  = oOrders.OrderNumber;
            oWrapOpenOrders.wrapAccEntryCurr = oOrders.CurrencyIsoCode;
            oWrapOpenOrders.wrapAccEntryAmt = oOrders.TotalAmount;
            totalOpenOrdersAmt += oOrders.TotalAmount;
            lstOpenOrdWrapper.add(oWrapOpenOrders);
        }
    }
    
    /**
     * To retrieve Open Invoices and Credit Memos details.
     */
    private void getOpenInvoiceAndCreditMemo(String accId)
    {
        WrapperOpenInvoicesAndCreditMemos oOICMWrapper;
        list<Id> sCASIItemIds = new list<Id>();
        lstOICMWrapper = new list<WrapperOpenInvoicesAndCreditMemos>();
        Integer numRows = EP_Common_Util.getQueryLimit();
        for(EP_Customer_Account_Statement_Item__c oCASIItem : [Select Id, RecordType.DeveloperName, EP_Statement_Item_Issue_Date__c, CurrencyIsoCode, EP_Debit__c, EP_Credit__c,
                                                    EP_Customer_Invoice__r.EP_Remaining_Balance__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c,
                                                    EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c 
                                                    from EP_Customer_Account_Statement_Item__c where EP_Bill_To__c =: accId 
                                                    and RecordType.DeveloperName IN : sRecTypeDevName 
                                                    order by EP_Statement_Item_Issue_Date__c Asc Limit : numRows] )
        {
            if(EP_Common_Constant.CREDIT_MEMO_RECORD_TYPE_NAME.equals(oCASIItem.RecordType.DeveloperName))
            {
                if(oCASIItem.EP_Customer_Credit_Memo__r <> null 
                    && oCASIItem.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c > 0
                    && EP_Common_Constant.COMPLETED.equals(oCASIItem.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c))
                {
                    sCASIItemIds.add(oCASIItem.Id);
                }
            }
            else if(EP_Common_Constant.CUSTOMER_INVOICE_RECORD_TYPE_NAME.equals(oCASIItem.RecordType.DeveloperName))
            {
                if(oCASIItem.EP_Customer_Invoice__r <> null 
                    && oCASIItem.EP_Customer_Invoice__r.EP_Remaining_Balance__c > 0
                    && EP_Common_Constant.COMPLETED.equals(oCASIItem.EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c))
                {
                    sCASIItemIds.add(oCASIItem.Id);
                }                
            }
            else
            {
            	//NO USE OF THIS BLOCK
            }           
        }
        if(!sCASIItemIds.isEmpty())
        {
            Integer nRows = EP_Common_Util.getQueryLimit();
            dCalculateRemainaingTotal = 0.0;
            map<Id, String> mAccountEntryType = AccountEntryType(sCASIItemIds);
            map<Id, Double> mAccountEntryAmount = AccountEntryAmount(sCASIItemIds);

            for(EP_Customer_Account_Statement_Item__c oCASIItem : [Select Id, RecordType.DeveloperName, EP_Customer_Invoice__r.EP_Remaining_Balance__c,
                                                        EP_Statement_Item_Issue_Date__c, CurrencyIsoCode, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c
                                                        from EP_Customer_Account_Statement_Item__c where EP_Bill_To__c =: accId 
                                                        and RecordType.DeveloperName IN : sRecTypeDevName 
                                                        and Id IN : sCASIItemIds
                                                        order by EP_Statement_Item_Issue_Date__c Asc Limit : nRows])
            {
                oOICMWrapper = new WrapperOpenInvoicesAndCreditMemos();
                oOICMWrapper.wrapAccEntryDocDate = String.valueOf(oCASIItem.EP_Statement_Item_Issue_Date__c);
                oOICMWrapper.wrapAccEntryType = mAccountEntryType.get(oCASIItem.Id);
                oOICMWrapper.wrapAccEntryCurr = oCASIItem.CurrencyIsoCode;
                oOICMWrapper.wrapAccEntryAmt = Double.valueOf(String.valueOF(mAccountEntryAmount.get(oCASIItem.Id))
                                                                            .replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank));
                                                                            
                dCalculateRemainaingTotal  = dCalculateRemainaingTotal + Double.valueOf(String.valueOf(checkNullDecimal(oCASIItem .EP_Customer_Invoice__r.EP_Remaining_Balance__c))
                                                .replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank)) - Double.valueOf(String.valueOf(checkNullDecimal(oCASIItem .EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c))
                                                .replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank));    
                lstOICMWrapper.add(oOICMWrapper); 
            }                                                                                                
        }   
    }
    
    /**
     * To retrieve Open Customer Account Statement Item records.
     */
    private void getOpenItemSummary(String accId)
    {
        lstCSOIWrapper = new list<WrapperCustomerOpenStatementItem>();
        WrapperCustomerOpenStatementItem oCSOIWrapper = null;
        if(lstCSOIWrapper.isEmpty())
        {
            oCSOIWrapper = new WrapperCustomerOpenStatementItem();
            oCSOIWrapper.wrapInvRemainingBal = dCalculateRemainaingTotal;
            oCSOIWrapper.wrapCashAmtOnAcc = getTotalPaymentRemainingBalance(accId) - getTotalPaymentRefundRemainingBalance(accId);
            oCSOIWrapper.wrapAmtOfOpenOrders = totalOpenOrdersAmt;
            oCSOIWrapper.wrapDueAmt = Double.valueOf(String.valueOF(oCSOIWrapper.wrapAmtOfOpenOrders).replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank)) 
                                        - Double.valueOf(String.valueOF(oCSOIWrapper.wrapCashAmtOnAcc).replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank))
                                        + Double.valueOf(String.valueOF(dCalculateRemainaingTotal).replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank));
            lstCSOIWrapper.add(oCSOIWrapper);
        }           
    }
    
    /**
     * To get Total Payment Remaining Balance.
     */
    private Double getTotalPaymentRemainingBalance(String accId)
    {
        Double totalRemainaningBalance = null;
        Integer nRows = EP_Common_Util.getQueryLimit();
        Id paymentRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_RECORD_TYPE_NAME);
        for(AggregateResult oAggregateResult : [select SUM(EP_Payment_Remaining_Balance__c) TotalRemainingBal ,id
                                                                 from EP_Customer_Payment__c 
                                                                 where EP_Bill_To__c =: accId and
                                                                 RecordTypeID =: paymentRecordTypeId GROUP BY id Limit : nRows ])
     	{
            totalRemainaningBalance = checkNullDouble(Double.valueOf(oAggregateResult.get(TOTALREMAININGBAL)));
        }
        return checkNullDouble(totalRemainaningBalance);
    }
    
    /**
     * To get Total Payment Refund Remaining Balance.
     */
    private Double getTotalPaymentRefundRemainingBalance(String accId)
    { 
        Double totalRemainaningBalance = null;
        Integer nRows = EP_Common_Util.getQueryLimit();
        Id paymentRefundRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_REFUND_RECORD_TYPE_NAME);
        for(AggregateResult oAggregateResult : [select SUM(EP_Payment_Remaining_Balance__c) TotalRemainingBal ,id
                                                                 from EP_Customer_Payment__c 
                                                                 where EP_Bill_To__c =: accId and
                                                                 RecordTypeId =: paymentRefundRecordTypeId  GROUP BY id Limit : nRows ])
     	{
            totalRemainaningBalance = Double.valueOf(String.valueOf(checkNullDouble(Double.valueOf(oAggregateResult.get(TOTALREMAININGBAL))))
                                    .replace(EP_Common_Constant.Minus,EP_Common_Constant.Blank));
        }
        return checkNullDouble(totalRemainaningBalance);
    }
    
    /**
     * To get Account Entry Type details.
     */
    private map<Id, String> AccountEntryType(list<Id> lstCASItemIds)
    {
        Integer nRows = EP_Common_Util.getQueryLimit();
        map<Id, String> mRetValue = new map<Id, String>();
        if(!lstCASItemIds.isEmpty())
        {
            for(EP_Customer_Account_Statement_Item__c oCustomerAccountStatement : [Select RecordType.DeveloperName, EP_Customer_Invoice__r.EP_Remaining_Balance__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c, 
                                                                                    EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c
                                                                                    from EP_Customer_Account_Statement_Item__c
                                                                                    where ID IN: lstCASItemIds and RecordType.DeveloperName IN: sRecTypeDevName
                                                                                    limit: nRows] )
            {                                                                     
                if(EP_Common_Constant.CREDIT_MEMO_RECORD_TYPE_NAME.equals(oCustomerAccountStatement.RecordType.DeveloperName))
                {
                    if(oCustomerAccountStatement.EP_Customer_Credit_Memo__r <> null 
                        && oCustomerAccountStatement.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c > 0
                        && EP_Common_Constant.COMPLETED.equals(oCustomerAccountStatement.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c))
                    {
                        mRetValue.put(oCustomerAccountStatement.Id, EP_Common_Constant.Credit);
                    }
                }
                else if(EP_Common_Constant.CUSTOMER_INVOICE_RECORD_TYPE_NAME.equals(oCustomerAccountStatement.RecordType.DeveloperName))
                {
                    if(oCustomerAccountStatement.EP_Customer_Invoice__r <> null 
                        && oCustomerAccountStatement.EP_Customer_Invoice__r.EP_Remaining_Balance__c > 0
                        && EP_Common_Constant.COMPLETED.equals(oCustomerAccountStatement.EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c))
                    {
                        mRetValue.put(oCustomerAccountStatement.Id, EP_Common_Constant.Debit);
                    }                
                }
                else{
            		//NO USE OF THIS BLOCK
           	 	}
            }
        }
        return mRetValue;                                                                      
    }
    
    /**
     * To get Account Entry Amount details.
     */
    private map<Id, Double> AccountEntryAmount(list<Id> lstCASItemIds)
    {
        Integer nRows = EP_Common_Util.getQueryLimit();
        map<Id, Double> mRetValue = new map<Id, Double>();
        if(!lstCASItemIds.isEmpty())
        {
            for(EP_Customer_Account_Statement_Item__c oCustomerAccountStatement : [Select Id, EP_Credit__c, EP_Debit__c, RecordType.DeveloperName, 
                                                                                    EP_Customer_Invoice__r.EP_Remaining_Balance__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c,
                                                                                    EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c, EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c
                                                                                    from EP_Customer_Account_Statement_Item__c
                                                                                    where ID IN: lstCASItemIds and RecordType.DeveloperName IN: sRecTypeDevName 
                                                                                    limit : nRows])
            { 
                 if(EP_Common_Constant.CREDIT_MEMO_RECORD_TYPE_NAME.equals(oCustomerAccountStatement.RecordType.DeveloperName))
                 {
                     if(oCustomerAccountStatement.EP_Customer_Credit_Memo__r <> null 
                        && oCustomerAccountStatement.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c > 0
                        && EP_Common_Constant.COMPLETED.equals(oCustomerAccountStatement.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Pull_Status__c))
                     {
                         mRetValue.put(oCustomerAccountStatement.Id, Double.valueOf(checkNullDecimal(oCustomerAccountStatement.EP_Customer_Credit_Memo__r.EP_Credit_Memo_Remaining_Balance__c)));
                     }
                 }
                 else if(EP_Common_Constant.CUSTOMER_INVOICE_RECORD_TYPE_NAME.equals(oCustomerAccountStatement.RecordType.DeveloperName))
                 {
                     if(oCustomerAccountStatement.EP_Customer_Invoice__r <> null 
                        && oCustomerAccountStatement.EP_Customer_Invoice__r.EP_Remaining_Balance__c > 0
                        && EP_Common_Constant.COMPLETED.equals(oCustomerAccountStatement.EP_Customer_Invoice__r.EP_Invoice_Pull_Status__c))
                     {
                         mRetValue.put(oCustomerAccountStatement.Id, Double.valueOf(checkNullDecimal(oCustomerAccountStatement.EP_Customer_Invoice__r.EP_Remaining_Balance__c)));
                     }                
                 }
                 else
                 {
		        	//NO USE OF THIS BLOCK
		         }                                             
             }
        }   
        return mRetValue;
    } 
    
    /**
     * Null check for decimal
     */
    @TestVisible     
    private Decimal checkNullDecimal(decimal dDecimal){
        if(dDecimal == null){
                dDecimal = 0.0;
        }
        return dDecimal;
    }

    /**
     * Null check for double
     */
    @TestVisible     
    private Double checkNullDouble(double dDouble){
        if(dDouble == null){
                dDouble = 0.0;
        }
        return dDouble;
    }
    
    
    /**
     * Error message
     */         
    private void addPageMessages(ApexPages.Severity sevrty, string message){
        apexpages.Message msg = new Apexpages.Message(sevrty,message);
        Apexpages.addmessage(msg);
    }
    
    /**
     * WrapperCustomerOpenStatementItem class for Customer Account Statement
     */    
    public with sharing class WrapperCustomerOpenStatementItem {
        public double wrapInvRemainingBal {get;set;}
        public double wrapCashAmtOnAcc {get;set;}
        public double wrapAmtOfOpenOrders {get;set;}
        public double wrapDueAmt{get;set;} 
    }
    
    /**
     * WrapperOpenInvoicesAndCreditMemos class for Customer Account Statement 
     */    
    private with sharing class WrapperOpenInvoicesAndCreditMemos {
        public string wrapAccEntryDocDate {get;set;}
        public string wrapAccEntryType {get;set;}
        public string wrapAccEntryCurr {get;set;}
        public double wrapAccEntryAmt{get;set;} 
    }
    
    /**
     * WrapperOpenOrders class for Customer Account Statement 
     */    
    private with sharing class WrapperOpenOrders {
        public string wrapOrderId {get;set;}  
        public string wrapOrderNumber {get;set;}
        public string wrapAccEntryCurr {get;set;}
        public double wrapAccEntryAmt {get;set;}
    }
    
}