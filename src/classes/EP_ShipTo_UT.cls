@isTest
public class EP_ShipTo_UT{
    @testSetup
    public static void init(){
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setCurrency_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.setCurrency(acct);
        Test.stopTest();
        Account parentAccount = accountMapper.getAccountRecord(acct.ParentId);
        System.AssertEquals(parentAccount.CurrencyISOCode,acct.CurrencyISOCode);
        //System.assertEquals(acct.Parent.CurrencyISOCode,acct.CurrencyISOCode); //check
    }

    static testMethod void setCountry_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_CountryMapper countryMapper = new EP_CountryMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.setCountry(acct);
        Test.stopTest();
        EP_Country__c country = countryMapper.getCountryById(acct.EP_Country__c);   
        System.AssertEquals(country.name,acct.ShippingCountry);
    }

    static testMethod void setCompanyFromParent_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.setCompanyFromParent(acct);
        Test.stopTest();
        Account parentaccount = accountMapper.getAccountRecord(acct.ParentId);
        System.AssertEquals(parentaccount.EP_Puma_Company__c,acct.EP_Puma_Company__c);
    }
    
    /*****Defect Fix- 43911 START********/   
    static testMethod void setOwnerFromParent_test() {
        
        User user = EP_TestDataUtility.CSC_USER;
        database.insert(user);
        
        EP_ShipTo localObj = new EP_ShipTo();
        
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Account sellTo = new Account(Id=acct.ParentId, OwnerId=user.Id);
        Database.update(sellTo);
        Test.startTest();
        localObj.setOwnerFromParent(acct);
        Test.stopTest();
        System.AssertEquals(user.Id,acct.OwnerID);
    }
    /*****Defect Fix- 43911 END********/   
    
    static testMethod void getParentCompany_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        Id result = localObj.getParentCompany(acct);
        Test.stopTest();
        Account parentaccount = accountMapper.getAccountRecord(acct.ParentId);
        System.AssertEquals(parentaccount.EP_Puma_Company__c,acct.EP_Puma_Company__c);
    }


    static testMethod void isPrimarySupplyOptionAttached_PositiveScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToPositiveScenario();
        //Bulkification Implementation -- Start
        EP_AccountDomainObject accountDomObject = new EP_AccountDomainObject(acct);
        localObj.accountDomainObj = accountDomObject;
        //Bulkification Implementation -- End
        Test.startTest();
        Boolean result = localObj.isPrimarySupplyOptionAttached(acct);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isPrimarySupplyOptionAttached_NegativeScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToNegativeScenario();
        //Bulkification Implementation -- Start
        EP_AccountDomainObject accountDomObject = new EP_AccountDomainObject(acct);
        localObj.accountDomainObj = accountDomObject;
        //Bulkification Implementation -- End
        Test.startTest();
        Boolean result = localObj.isPrimarySupplyOptionAttached(acct);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void syncCustomerToPricingEngine_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.syncCustomerToPricingEngine(acct);
        Test.stopTest();
        List<EP_IntegrationRecord__c> intRecList = EP_TestDataUtility.getIntegrationRecord(acct); //check
        System.assertEquals(1,intRecList.size());
    }
    static testMethod void getSupplyLocationList_test() { 
        EP_ShipTo localObj = new EP_ShipTo();
        Id accountId = EP_TestDataUtility.getShipToPositiveScenario().id;
        Test.startTest();
        List<EP_Stock_Holding_Location__c> result = localObj.getSupplyLocationList(accountId);
        Test.stopTest();
        System.assertEquals(true, result.size() > 0);
    }
    static testMethod void sendCustomerCreateRequest_test() { 
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        EP_TestDataUtility.setParentasActive(acct);
        Test.startTest();
        localObj.sendCustomerCreateRequest(acct);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }

    static testMethod void setSentNAVWINDMFlag_Test(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.setSentNAVWINDMFlag(acct);
        Test.stopTest(); 
        Account accountObj = new EP_AccountMapper().getAccountRecord(acct.Id);
        system.assertEquals(true, accountObj.EP_SENT_TO_NAV_WINDMS__c);
    }
    
    static testMethod void sendCustomerEditRequest_test() { 
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.sendCustomerEditRequest(acct);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }

    static testMethod void isParentActive_PositiveScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToPositiveScenario();
        EP_TestDataUtility.setParentasActive(acct);
        Test.startTest();
        Boolean result = localObj.isParentActive(acct);
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void isParentActive_NegativeScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToNegativeScenario();
        Test.startTest();
        Boolean result = localObj.isParentActive(acct);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void sendShipToSyncRequestToNAV_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.sendShipToSyncRequestToNAV(acct);
        Test.stopTest();
        List<EP_IntegrationRecord__c> intRecList = EP_TestDataUtility.getIntegrationRecord(acct);
        system.debug('===intRecList==='+intRecList);
        System.assertEquals(1,intRecList.size());
    }

    static testMethod void sendShipToSyncRequestToLOMO_test1() {
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.sendShipToSyncRequestToLOMO(acct,true);
        Test.stopTest();
        List<EP_IntegrationRecord__c> intRecList = EP_TestDataUtility.getIntegrationRecord(acct);
        system.debug('===intRecList==='+intRecList);
        System.assertEquals(1,intRecList.size());
    }
    
    static testMethod void sendShipToSyncRequestToLOMO_test2() {
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.sendShipToSyncRequestToLOMO(acct,false);
        Test.stopTest();
        List<EP_IntegrationRecord__c> intRecList = EP_TestDataUtility.getIntegrationRecord(acct);
        system.debug('===intRecList==='+intRecList);
        System.assertEquals(1,intRecList.size());
    }

    static testMethod void getTankRecordIds_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_TankMapper tankMapper = new EP_TankMapper(); 
        LIST<account> shipToList = new List<Account>();
        shipToList.add(EP_TestDataUtility.getShipTo());
        Set<ID> shipToID = new Set<ID>();
        shipToID.add(shipToList[0].id);
        Test.startTest();
        LIST<id> result = localObj.getTankRecordIds(shipToList);
        Test.stopTest();
        List<EP_Tank__c> tankList = tankMapper.getRecordsByShipToIds(shipToID);
        System.AssertEquals(tankList.size(),result.size());
    }

    static testMethod void getStockHoldingLocationIds_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        LIST<Account> shipToList = new List<Account>();
        shipToList.add(EP_TestDataUtility.getShipTo());
        Test.startTest();
        LIST<id> result = localObj.getStockHoldingLocationIds(shipToList);
        Test.stopTest();
        EP_StockHoldingLocationMapper stkHlgLocMap = new EP_StockHoldingLocationMapper();
        Set<ID> shipToID = new Set<ID>();
        shipToID.add(shipToList[0].id);
        System.AssertEquals(true,stkHlgLocMap.getStockHoldingLocationByIds(shipToID).size() == result.size());  
        System.Assert(result!=null);
    }

    static testMethod void insertPlaceHolderTankDips2_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        EP_AccountMapper accountMapper = new  EP_AccountMapper();
        Account shipTo = accountMapper.getAccountRecord(acct.Id);
        localObj.insertPlaceHolderTankDips(shipTo);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }

    static testMethod void deletePlaceHolderTankDips_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        Test.startTest();
        EP_TankMapper mapper = new EP_TankMapper();
        List<EP_Tank__c> tanks  = mapper.getRecordsByShipToIds(new Set<Id>{acct.Id});
        MAp<Id, EP_Tank_Dip__c> tankDipMap = EP_TestDataUtility.createTestTankDipsRecords(tanks, system.now(), true);
        acct.EP_Status__c = EP_Common_Constant.INACTIVE_SHIP_TO_STATUS;
        localObj.deletePlaceHolderTankDips(acct);
        Test.stopTest();
        List<EP_Tank_Dip__c> tankDipRec = new List<EP_Tank_Dip__c>([SELECT EP_Ambient_Quantity__c,RecordType.id, CreatedDate, EP_Tank__c FROM EP_Tank_Dip__c WHERE EP_Tank_Dip_Entered_Today__c = TRUE]);
        System.assertEquals(0,tankDipRec.size());
    }

    static testMethod void getVendorType_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account shipToAccount = accountMapper.getAccountRecord(acct.Id);
        EP_Account_VendorManagement result = localObj.getVendorType(shipToAccount);
        Test.stopTest();    
        System.assertEquals(true , result != null);
    }

    static testMethod void doBeforeInsertHandle_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.doBeforeInsertHandle(acct);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }

    static testMethod void doBeforeUpdateHandle1_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account oldaccount = EP_TestDataUtility.getShipTo();
        Account newaccount = oldaccount.clone();
        Test.startTest();
        localObj.doBeforeUpdateHandle(oldaccount,newaccount);
        Test.stopTest();    
        // No Assert required, As this method delegates other methods
    }

    static testMethod void doAfterInsertHandle_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        localObj.doAfterInsertHandle(acct);
        Test.stopTest();
        Account shipToAccountReQuery = [SELECT Id,OwnerId,ParentId FROM Account WHERE Id =: acct.Id];
        Id sellToAccountId = shipToAccountReQuery.ParentId;
        Account sellToAccount = [SELECT Id,OwnerId FROM Account WHERE Id =: sellToAccountId];
        Account shipToAccount = [SELECT Id,OwnerId FROM Account WHERE Id =: acct.Id];
        System.assertEquals(sellToAccount.OwnerId,shipToAccount.OwnerId);
    }

    static testMethod void getBillTo_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        Account result = localObj.getBillTo(acct);
        Test.stopTest();
        System.assertEquals(true, result!=null);
        
    }

    static testMethod void getSellTo_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipTo();
        Test.startTest();
        Account result = localObj.getSellTo(acct);
        Test.stopTest();
        System.assertEquals(true,result!=null);
    }

    static testMethod void hasBillTo_PositiveScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToPositiveScenario();
        Test.startTest();
        Boolean result = localObj.hasBillTo(acct);
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void hasBillTo_NegativeScenariotest() {
        EP_ShipTo localObj = new EP_ShipTo();
        Account acct = EP_TestDataUtility.getShipToNegativeScenario();
        Test.startTest();
        Boolean result = localObj.hasBillTo(acct);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void getPriceBookId_test() {
        EP_ShipTo localObj = new EP_ShipTo();
        Id accountId = EP_TestDataUtility.getShipTo().id;
        Account shiptoAccount = EP_TestDataUtility.getShipTo();
        Test.startTest();
        Id result = localObj.getPriceBookId(shiptoAccount);
        Test.stopTest();
        System.assertEquals(true,result != null);
    }

    static testMethod void getCustomerPONumber_test(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account account = EP_TestDataUtility.getShipTo();
        Test.startTest();
        String result = localObj.getCustomerPONumber(account);
        Test.stopTest();
        System.assertEquals(true,result != null);
    }

    static testMethod void getPricebookEntry_test(){
        EP_ShipTo localObj = new EP_ShipTo(); 
        Account account = EP_TestDataUtility.getShipTo();
        String productCode = EP_Common_Constant.PRODUCTCODE;
        Test.startTest();
        Map<String,PricebookEntry> result = localObj.getPricebookEntry(account, productCode);
        Test.stopTest();
        System.assertEquals(true,result != null);

    }
    
    static testmethod void isShipToActive_NegativeSecnarioTest(){
        EP_ShipTo localObj = new EP_ShipTo(); 
        Account account = EP_TestDataUtility.getShipTo();
        Account sellTo = new Account(Id=account.ParentId);
        account.EP_SENT_TO_NAV_WINDMS__c = true;
        database.update(account);
        Test.startTest();
        boolean result = localObj.isShipToActive(sellTo);
        Test.stopTest();
        system.assertEquals(false, result);
    }
    
    static testmethod void isShipToActive_PositiveSecnarioTest(){
        EP_ShipTo localObj = new EP_ShipTo(); 
        Account account = EP_TestDataUtility.getShipTo();
        Account sellTo = new Account(Id=account.ParentId);
        Test.startTest();
        boolean result = localObj.isShipToActive(sellTo);
        Test.stopTest();
        system.assertEquals(true, result);
    }
    
    static testmethod void isRouteAllocationAvailable_Negative_Test(){
        EP_ShipTo localObj = new EP_ShipTo(); 
        Account account = EP_TestDataUtility.getShipTo();
        //Bulkification Implementation -- Start
        EP_AccountDomainObject accountDomObject = new EP_AccountDomainObject(account);
        localObj.accountDomainObj = accountDomObject;
        //Bulkification Implementation -- End
        Test.startTest();
        boolean result = localObj.isRouteAllocationAvailable(account);
        Test.stopTest();
        system.assertEquals(false, result);
    }
    
    static testmethod void isRouteAllocationAvailable_Positive_Test(){
        EP_ShipTo localObj = new EP_ShipTo(); 
        Account account = EP_TestDataUtility.getShipTo();
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id from EP_Action__c];
        EP_Route_Allocation__c routeAllocation = new EP_Route_Allocation__c(EP_Delivery_Window_Start_Date__c = 2,Delivery_Window_End_Date__c=10,EP_Route__c=route.id,EP_Ship_To__c=account.id);
        insert routeAllocation;  
        //Bulkification Implementation -- Start
        EP_AccountDomainObject accountDomObject = new EP_AccountDomainObject(account);
        localObj.accountDomainObj = accountDomObject;
        //Bulkification Implementation -- End     
        Test.startTest();
        boolean result = localObj.isRouteAllocationAvailable(account);
        Test.stopTest();
        system.assertEquals(true, result);   
    }
    
    static testMethod void sendCustomerCreateRequestNAV_test() { 
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        EP_TestDataUtility.setParentasActive(acct);
        Test.startTest();
        localObj.sendCustomerCreateRequestToNAV(acct);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }
        static testMethod void sendCustomerCreateRequestWinDms_test() { 
        EP_ShipTo localObj = new EP_ShipTo();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Account acct = EP_TestDataUtility.getShipTo();
        EP_TestDataUtility.setParentasActive(acct);
        Test.startTest();
        localObj.sendCustomerCreateRequestToWinDms(acct);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }
    static testMethod void isSellToSynced_test(){
        Account acc =  EP_TestDataUtility.getShipTo();
        Account parentSellTo = [select id,EP_Synced_NAV__c,EP_Synced_PE__c,EP_Synced_WINDMS__c,EP_BSM_GM_Review_Completed__c From Account Where Id =: acc.ParentId ];
        parentSellTo.EP_Synced_NAV__c =true;
        parentSellTo.EP_Synced_PE__c = true;
        parentSellTo.EP_Synced_WINDMS__c = true;
        parentSellTo.EP_BSM_GM_Review_Completed__c = true;
        parentSellTo.EP_Status__c = EP_AccountConstant.ACTIVE;
        update parentSellTo;
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc);
        EP_ShipTo localObj = new EP_ShipTo();
        Test.startTest();
        boolean result = localObj.isSellToSynced(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(true, result);
    }
    static testMethod void isSellToSynced_Negativetest(){
        Account acc =  EP_TestDataUtility.getShipTo();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc);
        EP_ShipTo localObj = new EP_ShipTo();
        Test.startTest();
        boolean result = localObj.isSellToSynced(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    static testMethod void isSellToActive_test(){
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        Account sellToaccount = [select id from Account where Id =: acc.ParentId Limit 1];
        sellToaccount.EP_Synced_NAV__c =true;
        sellToaccount.EP_Synced_PE__c = true;
        sellToaccount.EP_Synced_WINDMS__c = true;
        sellToaccount.EP_BSM_GM_Review_Completed__c = true;
        sellToaccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToaccount;
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc.Id);
        EP_ShipTo localObj = new EP_ShipTo();
        Test.startTest();
        boolean result = localObj.isSellToActive(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(true, result);
    }
    static testMethod void isSellToActive_negativetest(){
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc);
        EP_ShipTo localObj = new EP_ShipTo();
        Test.startTest();
        boolean result = localObj.isSellToActive(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(false, result);
    }

    static testMethod void isLocationIdChanged_positiveTest(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account oldAccount =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(oldAccount.id);
        accountDomainObject.localAccount.EP_Location_Id__c = 'FG897897';
        update accountDomainObject.localAccount;
        Account newAccount = new Account(id = accountDomainObject.localAccount.id,EP_Location_Id__c = 'AFG897897');
        Test.startTest();
        Boolean result = localObj.isLocationIdChanged(accountDomainObject.localAccount,newaccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void isLocationIdChanged_negativeTest(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account oldAccount =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(oldAccount.id);
        //oldAccount.EP_Location_Id__c = 'FG897897';
        //update oldAccount;
        Account newAccount = new Account(id = accountDomainObject.localAccount.id);
        Test.startTest();
        Boolean result = localObj.isLocationIdChanged(accountDomainObject.localAccount,newaccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void updateConsignmentLocationReviewStatus_positiveTest(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc.id);
        accountDomainObject.localAccount.EP_Location_Id__c = 'FG897897';
        update accountDomainObject.localAccount;
        EP_Action__c actionObj = EP_TestDataUtility.createCustomerAction('Consignment Location Review',accountDomainObject.localAccount.id);        
        insert actionObj;
        actionObj.OwnerId = system.UserInfo.getUserId();
        update actionObj;
        Test.startTest();
        localObj.updateConsignmentLocationReviewStatus(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(true,actionObj.id!=null);
    }

    static testMethod void updateConsignmentLocationReviewStatus_negativeTest(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc.id);
        accountDomainObject.localAccount.EP_Location_Id__c = 'FG897897';
        update accountDomainObject.localAccount;
        List<EP_Action__c> actionObj = [select id from EP_Action__c 
                                    where recordType.developerName = 'EP_Consignment_Location_Review' AND EP_Account__c =: accountDomainObject.localAccount.id];
        Test.startTest();
        localObj.updateConsignmentLocationReviewStatus(accountDomainObject.localAccount);
        Test.stopTest();
        System.AssertEquals(0,actionObj.size());
    }

    static testMethod void doAfterUpdateHandle_test(){
        EP_ShipTo localObj = new EP_ShipTo();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Account oldaccount = EP_TestDataUtility.getShipTo();
        Account newaccount = oldaccount.clone();
        Test.startTest();
        localObj.doAfterUpdateHandle(oldaccount,newaccount);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }
// Changes made for CUSTOMER MODIFICATION L4 Start
    static testMethod void sendCustomerEditRequestToNAV_test(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc.id);
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_NAV_SHIPTO_EDIT;
        customSettingData.Target_Integration_System__c = 'NAV';
        insert customSettingData;
        Test.startTest();
        localObj.sendCustomerEditRequestToNAV(accountDomainObject.localAccount);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }

    static testMethod void sendCustomerEditRequestToWINDMS_test(){
        EP_ShipTo localObj = new EP_ShipTo();
        Account acc =  EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acc.id);
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_WINDMS_SHIPTO_EDIT;
        customSettingData.Target_Integration_System__c = 'WINDMS';
        insert customSettingData;
        Test.startTest();
        localObj.sendCustomerEditRequestToWINDMS(accountDomainObject.localAccount);
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true);
    }
// Changes made for CUSTOMER MODIFICATION L4 End
}