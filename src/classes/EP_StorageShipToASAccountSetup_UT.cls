@isTest
public class EP_StorageShipToASAccountSetup_UT
{

    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '05-Active01-Prospect';
    
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    static testMethod void doOnEntry_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method only delegates to another class method,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doOnExit_test() {
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //No Assertion as the method only has no processing logic,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void doTransition_NegativeScenariotest() {
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        string errorMessage;
        try{
        Boolean result = localObj.doTransition();
        }Catch(Exception e){
            errorMessage = e.getMessage();
        }
        Test.stopTest();
        System.AssertEquals(false,string.isBlank(errorMessage));
    }
    
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_StorageShipToASAccountSetup localObj = new EP_StorageShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
}