/*
 *  @Author <Accenture>
 *  @Name <EP_AcknowledgementStub>
 *  @CreatedDate <28/02/2017>
 *  @Description <This class will be used to wrap the outbound and inbound acknowledgement>
 *  @Version <1.0>
 */
public  class EP_AcknowledgementStub {
    public MSG MSG;
    public EP_AcknowledgementStub(){
        MSG = new MSG();
    }
    public class MSG {
        public EP_MessageHeader HeaderCommon;
        public Payload Payload;
        public String StatusPayload;    
        
        public MSG(){
            HeaderCommon= new EP_MessageHeader();
            Payload= new Payload();
        }
    }
    
    public class Payload {
        public any0 any0;
        
        public Payload(){
            any0= new any0();
        }
    }
    public class any0 {
        public acknowledgement acknowledgement;
        
        public any0(){   
            acknowledgement = new acknowledgement();
        }
    }
    public class acknowledgement {
        public String ackType;  
        public dataSets dataSets;
        
        public acknowledgement(){
            dataSets = new dataSets();
        }
    }
    public class dataSets {
        public dataset[] dataset;
        
        public dataSets(){
            dataset = new List<dataset>();
        }
    }
    public class dataSet {
        public String name {get;set;}   
        public String seqId {get;set;}  
        public String errorCode {get;set;}  
        public String errorDescription {get;set;}
        public transient String finalStatus {get;set;}
    }
}