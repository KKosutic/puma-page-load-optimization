@isTest
public class EP_CustomSettingMapper_UT
{
	static testMethod void queryAddressFieldCustomMetaDataTypes_test(){
		String objName = 'Account';
		EP_CustomSettingMapper localObj = new EP_CustomSettingMapper();
		Test.startTest();
		List<EP_Address_Fields__mdt> result = localObj.queryAddressFieldCustomMetaDataTypes(objName);
		Test.stopTest();
		System.Assert(result.size()>0);
	}

	static testMethod void getStateTransitions_test(){
		EP_State_Transitions__c transition = new EP_State_Transitions__c();
		transition.Name = '102';
		transition.Transition_Class_Type__c = 'EP_OSTANYToCancelled';
		transition.EntityType__c = EP_Common_Constant.CONSUMPTION_ORDER;
		transition.State_Name__c = EP_Common_Constant.ORDER_PLANNED_STATUS;
		transition.Event__c = 'UserSubmit';
		Insert transition;

		String entityType = EP_Common_Constant.CONSUMPTION_ORDER;
		String stateName = EP_Common_Constant.ORDER_PLANNED_STATUS;
		String event = 'UserSubmit';
		EP_CustomSettingMapper localObj = new EP_CustomSettingMapper();
		Test.startTest();
		List<EP_State_Transitions__c> result = localObj.getStateTransitions(entityType, stateName, event);
		Test.stopTest();
		System.debug('********Transition State********'+result);
		System.Assert(result.size()>0);
	}

	static testMethod void getAddressDataByName_test(){
		String name = 'EP_Shipping_Country';
		EP_CustomSettingMapper localObj = new EP_CustomSettingMapper();
		Test.startTest();
		List<EP_Address_Fields__mdt> result = localObj.getAddressDataByName(name);
		Test.stopTest();
		System.Assert(result.size()>0);
	}
}