/*
*  @Author <Accenture>
*  @Name <EP_Account_VendorManagement>
*  @CreateDate < 05/02/2017 >
*  @Description <Base class for Vendor Management>
*  @Version <1.0>
*/
public virtual class EP_Account_VendorManagement {

	public virtual void updateVMIShipToUTCTimeZoneOffset(Account account){
		EP_GeneralUtility.Log('Public','EP_Account_VendorManagement','updateVMIShipToUTCTimeZoneOffset');

	}

	public virtual void insertPlaceHolderTankDips(Account account){
		EP_GeneralUtility.Log('Public','EP_Account_VendorManagement','insertPlaceHolderTankDips');

	}

	public virtual void deletePlaceHolderTankDips(Account account){
		EP_GeneralUtility.Log('Public','EP_Account_VendorManagement','deletePlaceHolderTankDips');

	}

}