/* 
@Author      Accenture
@name        EP_ProductOptionService
@CreateDate  10/11/2017
@Description Service layer class for Product Option Object
@Version     1.0
*/
public with sharing class EP_ProductOptionService{
    Map<Id,Account> mapAccount;
    Map<Id,list<PriceBookEntry>> mapPriceIdTolstPriceBookEntry;
    EP_ProductOptionDomain EP_ProductOptionDomainObj;
    map<Id,list<EP_Tank__c>> mapIdTolstTanks;
    map<Id,list<EP_Product_Option__c>> mapIdTolstProdOption;
    
    list<EP_Product_Option__c> lstNewProductOption;
    map<Id,EP_Product_Option__c> triggerOldMap;
    
    /**
    * @author       Accenture
    * @name         EP_ProductOptionService
    * @date         EP_ProductOptionService
    * @description  Constructor for setting ProductOptionDomain and initilizing EP_Product_Option__c object instance
    * @param        EP_ProductOptionDomain
    * @return       NA
    */  
    public EP_ProductOptionService(EP_ProductOptionDomain ProductOptionDomain){  
        this.EP_ProductOptionDomainObj = ProductOptionDomain;
        this.lstNewProductOption = ProductOptionDomain.lstNewProductOption;
        this.triggerOldMap = ProductOptionDomain.triggerOldMap;
        this.mapAccount = getAccountIdToAccount();
        this.mapPriceIdTolstPriceBookEntry = getPriceBookEntry();
        this.mapIdTolstTanks = getSellTolstTanks();
        this.mapIdTolstProdOption = getPriceBookTolstProductOption();
    }
    
    /**
    * @author       Accenture
    * @name         doBeforeInsertHandle
    * @date         29/11/2017
    * @description  method to handle logic for Product Option on before insert event
    * @param        NA
    * @return       NA
    */  
    public void doBeforeInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','doBeforeInsertHandle');
        list<PriceBookEntry> lstPriceBookEntry = new list<PriceBookEntry>();// not required 
        
        for(EP_Product_Option__c objProdOption : lstNewProductOption){          
            lstPriceBookEntry.addAll(getRelatedPriceBookEntry(objProdOption));
            if(iscurrencySameOnProductsAndSellTo(objProdOption)){
                objProdOption.addError(Label.EP_Products_associated_with_different_Currency);
            }  
        }
        if(lstPriceBookEntry != null && !lstPriceBookEntry.isEmpty()){
            database.update(lstPriceBookEntry);
        }
    }
    
    /**
    * @author       Accenture
    * @name         doBeforeUpdateHandle
    * @date         29/11/2017
    * @description  method to handle logic for Product Option on before update event
    * @param        NA
    * @return       NA
    */
    public void doBeforeUpdateHandle(){
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','doBeforeUpdateHandle');
        
        for(EP_Product_Option__c objProdOption : lstNewProductOption){
            if(objProdOption.Price_List__c != triggerOldMap.get(objProdOption.Id).Price_List__c){
                Boolean result = isProductAssociatedWithOperationalTank(objProdOption);
                if(result){
                    objProdOption.addError(Label.EP_Cannot_remove_product_list_associated_to_customer);
                }
            }
        }
    }
    
    /**
    * @author       Accenture
    * @name         doBeforeDeleteHandle
    * @date         29/11/2017
    * @description  method to handle logic for Product Option on before delete event
    * @param        NA
    * @return       NA
    */
    public void doBeforeDeleteHandle(){
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','doBeforeDeleteHandle');
        for(EP_Product_Option__c objProdOption : lstNewProductOption){              
            Boolean result = isProductAssociatedWithOperationalTank(objProdOption);
            if(result){
                objProdOption.addError(Label.EP_Cannot_remove_product_list_associated_to_customer);
            }              
        }
    }
    
    /**
    * @author     Accenture
    * @name      getAccountIdToAccount
    * @date      10/11/2017
    * @description   This method is used to get Account.
    * @param     NA
    * @return     Account to Account object map
    */
    @TestVisible private map<Id,Account> getAccountIdToAccount(){
        set<Id> setAccountIds = new set<Id>();
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        for(EP_Product_Option__c objProdOption : lstNewProductOption){
            setAccountIds.add(objProdOption.Sell_To__c);
        }
        EP_AccountMapper objAccountMapper = new EP_AccountMapper();
        mapAccount = new Map<Id,Account>(objAccountMapper.getAccountRecords(setAccountIds)); 
        
        return mapAccount;
    }
    
    /**
    * @author     Accenture
    * @name      getPriceBookEntry
    * @date      10/11/2017
    * @description   This method is used to get Price Book Entry.
    * @param     NA
    * @return    Price Id to list of PriceBookEntry records.
    */
     @TestVisible private map<Id,list<PriceBookEntry>> getPriceBookEntry(){
        set<Id> setPriceBookIds = new set<Id>();
        setPriceBookIds = returnSetPriceBookIds();
        EP_PriceBookEntryMapper objPriceBookEntryMapper = new EP_PriceBookEntryMapper();
    
        return getIdTolstPriceBookEntry(objPriceBookEntryMapper.getRecordsByPricebookIds(setPriceBookIds));
    }
    /**
    * @author     Accenture
    * @name      getIdTolstPriceBookEntry
    * @date      10/11/2017
    * @description   This method is used to get Price Book Entry.
    * @param     NA
    * @return    Price Id to list of PriceBookEntry records.
    */
    @TestVisible private map<Id,list<PriceBookEntry>> getIdTolstPriceBookEntry(list<PriceBookEntry> lstPriceBookEntry){
        Map<Id,list<PriceBookEntry>> mapPriceIdTolstPriceBookEntry = new Map<Id,list<PriceBookEntry>>();
        if(lstPriceBookEntry != null && !lstPriceBookEntry.isEmpty()){
            for(PriceBookEntry objPriceBookEntry : lstPriceBookEntry){
                if(!mapPriceIdTolstPriceBookEntry.containskey(objPriceBookEntry.Pricebook2Id)){
                    mapPriceIdTolstPriceBookEntry.put(objPriceBookEntry.Pricebook2Id,new list<PriceBookEntry>());
                }
                mapPriceIdTolstPriceBookEntry.get(objPriceBookEntry.Pricebook2Id).add(objPriceBookEntry);
            }
        }
        return mapPriceIdTolstPriceBookEntry;
    }
    /**
    * @author     Accenture
    * @name      getSellTolstTanks
    * @date      15/11/2017
    * @description   This method is used to get Ship to related Tanks.
    * @param     NA
    * @return    SellTO Account Id To List of Tanks.
    */
     @TestVisible private map<Id,list<EP_Tank__c>> getSellTolstTanks(){
        map<Id,list<EP_Tank__c>> mapIdTolstTanks = new map<Id,list<EP_Tank__c>>();
        list<Account> lstShipToAccount = new list<Account>();
        set<Id> setAccountIds = new set<Id>();
        
        setAccountIds = returnSetAccIds();  
        lstShipToAccount = getShipToAccount(setAccountIds);
        
        if(lstShipToAccount != null && !lstShipToAccount.isEmpty()){
            for(Account objAcc : lstShipToAccount){
                if(!mapIdTolstTanks.containskey(objAcc.ParentId)){
                    mapIdTolstTanks.put(objAcc.ParentId,new list<EP_Tank__c>());
                }               
                mapIdTolstTanks.get(objAcc.ParentId).addAll(objAcc.Tank__r);
            }
        }
        return mapIdTolstTanks;
    }
    /**
    * @author     Accenture
    * @name      getShipToAccount
    * @date      15/11/2017
    * @description   This method is used to get Ship to related Tanks.
    * @param     Set Of Id
    * @return    list Of sellTO Account object
    */
    @TestVisible private list<Account> getShipToAccount(set<Id> SellToAccIds){
        list<Account> lstShipToAcc = new list<Account>(); 
        EP_AccountMapper AccountMapper = new EP_AccountMapper();
        lstShipToAcc = AccountMapper.getShipToAccountsWithTanks(SellToAccIds);      
        return lstShipToAcc;
    }
    /**
    * @author     Accenture
    * @name      returnSetAccIds
    * @date      15/11/2017
    * @description   This method is used to get set of SellTo Account Ids.
    * @param     NA
    * @return    Set Of SellTo Account Ids.
    */
    @TestVisible private set<Id> returnSetAccIds(){
        set<Id> setAccountIds = new set<Id>();
        for(EP_Product_Option__c objProdOption : lstNewProductOption){
            setAccountIds.add(objProdOption.Sell_To__c);
        }       
        return setAccountIds;
    }
    
    /**
    * @author     Accenture
    * @name      returnSetAccIds
    * @date      18/11/2017
    * @description   This method is used to get set of Price Book Ids.
    * @param     NA
    * @return    Set Of of Price Book Ids.
    */
    @TestVisible private set<Id> returnSetPriceBookIds(){
        set<Id> setPriceBookIds = new set<Id>();
        for(EP_Product_Option__c objProdOption : lstNewProductOption){
            setPriceBookIds.add(objProdOption.Price_List__c);
        }     
        return setPriceBookIds;
    }
    
    /**
    * @author     Accenture
    * @name      getSellTolstTanks
    * @date      18/11/2017
    * @description   This method is used to Price Book related Existing Product Option records.
    * @param     NA
    * @return    SellTO Account Id To List of Tanks.
    */
    @TestVisible private map<Id,list<EP_Product_Option__c>> getPriceBookTolstProductOption(){
        map<Id,list<EP_Product_Option__c>> mapIdTolstProdOption = new map<Id,list<EP_Product_Option__c>>();
        set<Id> setPriceBookIds = new set<Id>();
        setPriceBookIds = returnSetPriceBookIds();
        mapIdTolstProdOption = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(setPriceBookIds);
        return mapIdTolstProdOption;
    }
    
    /**
    * @author       Accenture
    * @name         iscurrencySameOnProductsAndSellTo
    * @date         10/11/2017
    * @description  This method returns true if on Product and Account having same currency.
    * @param        EP_Product_Option__c
    * @return       Boolean
    */  
    @TestVisible private boolean iscurrencySameOnProductsAndSellTo(EP_Product_Option__c productOption){
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','iscurrencySameOnProductsAndSellTo');
        for(PriceBookEntry objPriEnty : mapPriceIdTolstPriceBookEntry.get(productOption.Price_List__c)){
            if(!mapAccount.get(productOption.Sell_To__c).EP_Currency__c.equalsIgnoreCase(objPriEnty.CurrencyIsoCode)){
                return true;
                break;
            }
        }
        return false;
    }
    
    /**
    * @author       Accenture
    * @name         isProductAssociatedWithOperationalTank`
    * @date         15/11/2017
    * @description  This method returns true if Product list associated to operational tank.
    * @param        EP_Product_Option__c
    * @return       Boolean
    */  
    @TestVisible private boolean isProductAssociatedWithOperationalTank(EP_Product_Option__c productOption){
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','isProductAssociatedWithOperationalTank');
        list<EP_Tank__c> lstTank = mapIdTolstTanks.get(productOption.Sell_To__c);
        
        if(lstTank != null && !lstTank.isEmpty()){
            return true;
        }
        return false;
    }
    /**
    * @author       Accenture
    * @name         getRelatedPriceBookEntry
    * @date         15/11/2017
    * @description  This method returns list of priceBook Entry related Product Option related price Book.
    * @param        EP_Product_Option__c
    * @return       list of PriceBookEntry
    */  
    @TestVisible private list<PriceBookEntry> getRelatedPriceBookEntry(EP_Product_Option__c productOption){
        list<PriceBookEntry> lstPriceBookEntry = new list<PriceBookEntry>();
        list<PriceBookEntry> lstUpdatePriceBookEntry = new list<PriceBookEntry>();
        list<EP_Product_Option__c> lstProductOption =  new list<EP_Product_Option__c>();
        EP_GeneralUtility.Log('Public','EP_ProductOptionService','getRelatedPriceBookEntry');
        lstProductOption = mapIdTolstProdOption.get(productOption.Price_List__c);
        lstPriceBookEntry = mapPriceIdTolstPriceBookEntry.get(productOption.Price_List__c);
        if(lstProductOption == null && lstPriceBookEntry != null && !lstPriceBookEntry.isEmpty()){
            for(PriceBookEntry objPriceBookEntry : lstPriceBookEntry){
                if(checkPriceBookEntryStatus(objPriceBookEntry)){
                    objPriceBookEntry.EP_Is_Sell_To_Assigned__c = true;
                    objPriceBookEntry.IsActive = true;
                    lstUpdatePriceBookEntry.add(objPriceBookEntry);
                }               
            }
        }
        return lstUpdatePriceBookEntry;
    }
    /**
    * @author       Accenture
    * @name         checkPriceBookEntryStatus
    * @date         15/11/2017
    * @description  This method returns true id PriceBookEntry is not active
    * @param        PriceBookEntry
    * @return       Boolean
    */  
    @TestVisible private Boolean  checkPriceBookEntryStatus(PriceBookEntry PriceBookEntry){
        if(!PriceBookEntry.EP_Is_Sell_To_Assigned__c || !PriceBookEntry.IsActive){
         return true;
        }
        return false;
    }
}