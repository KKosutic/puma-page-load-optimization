@RestResource(urlMapping = '/FE/V1/Dictionary/*')
/*********************************************************************************************
     @Author <>
     @name <EP_FE_Dictionary >
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_Dictionary {
    
    @HTTPGet
/*********************************************************************************************
     @Author <>
     @name <doGet>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
   webservice static EP_FE_BootstrapResponse doGet() {
        // Response  
        EP_FE_BootstrapResponse response = new EP_FE_BootstrapResponse();
        //Get Request params
        Map<String,Object> params = RestContext.request.params;
        String content = params.containsKey(EP_FE_BootstrapEndpoint.PARAM_KEY_CONTENT) 
            ? String.valueOf(params.get(EP_FE_BootstrapEndpoint.PARAM_KEY_CONTENT))
            : EP_FE_BootstrapEndpoint.PARAM_KEY_ALL;
        Set<String> contentSet = new Set<String>(content.split(','));

        response.dictionary = contentSet.contains(EP_FE_BootstrapEndpoint.PARAM_KEY_ALL) || contentSet.contains(EP_FE_BootstrapEndpoint.DICTIONARY_KEY) ? EP_FE_BootstrapEndpoint.getDictionary(response) : null; // To get Dictionary data 
        
        return response;
    }


}