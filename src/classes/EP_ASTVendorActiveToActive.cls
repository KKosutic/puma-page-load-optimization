public class EP_ASTVendorActiveToActive  extends EP_AccountStateTransition{
    public EP_ASTVendorActiveToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToActive','isGuardCondition');
        return true;
    }
}