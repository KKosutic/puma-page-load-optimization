public with sharing class EP_AccountDataWrapper {
    /** 
        accountType possible values - 
        SellTo/DummySellTo/ShipTo/Vendor
    */
    public string accountType;
    /** Possiblel Values- Consignment/ Non-Consignment **/
    public string consignmentType ;
    public string deliveryType;
    
    /** Possible Values - VMI/ NONVMI **/
    public string vendorType;
    
    public boolean billToRequired ;
    public boolean isKYCRequired ;
    public boolean isTankRequired ;
    public boolean isPortalUser ;
    /*Status Picklist Value*/
    public string status ;
    public boolean addPackagedPoduct ;
    public boolean addSupplyOption;
    public boolean isPaymentTermRequired ;
    public boolean isPaymentMethodRequired; 
    public boolean restrictActionGeneration ;
    /** Positive/ Negative **/
    public string scenarioType ;
    public Id parentId ;
    public ID companyId;
    public Id countryId;
    public Id regionId ;
    
    public EP_AccountDataWrapper(){
        this.billToRequired = false;
        this.addPackagedPoduct = false;
        this.addSupplyOption = false;
        this.isKYCRequired = false;
        this.isTankRequired = true;
        this.deliveryType = EP_Common_Constant.EX_RACK;
        this.isPaymentTermRequired = false;
        this.isPaymentMethodRequired = false;
        this.restrictActionGeneration = false;
        this.isPortalUser = false;
        
        
    }
    
}