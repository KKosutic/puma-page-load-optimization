@isTest
public class EP_NAVOrderUpdateStub_UT {

    @isTest
    static void init_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
        
        Test.startTest();
            EP_NAVOrderUpdateStub updateStub = new EP_NAVOrderUpdateStub();
            EP_NAVOrderUpdateStub.HeaderCommon header = new EP_NAVOrderUpdateStub.HeaderCommon();
            
            EP_NAVOrderUpdateStub.Identifier identifier = new EP_NAVOrderUpdateStub.Identifier();
            identifier.orderId = 'Test';
            identifier.entrprsId = 'Test';
            
            EP_NAVOrderUpdateStub.OrderWrapper wrapper = new EP_NAVOrderUpdateStub.OrderWrapper();
            wrapper.sfOrder = newOrder;
            wrapper.errorCode = 'Test';
            wrapper.errorDescription = 'Test';
            wrapper.versionNr = 'Test';
            wrapper.seqId = 'Test';
            wrapper.identifier = identifier;
            wrapper.reasoncode = 'Test';
            wrapper.description = 'Test';
            wrapper.status = 'Test';
            
            EP_NAVOrderUpdateStub.OrderList ordersList = new EP_NAVOrderUpdateStub.OrderList();
            ordersList.order = new List<EP_NAVOrderUpdateStub.OrderWrapper>{wrapper};
            
            EP_NAVOrderUpdateStub.OrderStatus orderStatus = new EP_NAVOrderUpdateStub.OrderStatus();
            orderStatus.orders = ordersList;
            
            EP_NAVOrderUpdateStub.any0 anyTest = new EP_NAVOrderUpdateStub.any0();
            anyTest.orderStatus = orderStatus;
            
            EP_NAVOrderUpdateStub.Payload payload = new EP_NAVOrderUpdateStub.Payload();
            payload.any0 = anyTest;
            
            EP_NAVOrderUpdateStub.MSG msg = new EP_NAVOrderUpdateStub.MSG();
            msg.StatusPayload = 'Test';
            msg.HeaderCommon = header;
            msg.Payload = payload;
            updateStub.MSG = msg;
        Test.stopTest();
        
        System.assertEquals('Test', updateStub.MSG.StatusPayload, 'Wrong field value');
    }
}