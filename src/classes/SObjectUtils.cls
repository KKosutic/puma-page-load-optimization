global class SObjectUtils { 

    /**
     * Loads objects with id in <code>objectIds</code> set.
     * Please make sure that all of object ids from the argument are of same object type.
     */
    global static List<SObject> loadSObjects(Set<Id> objectIds) {

        if (objectIds == null) {
            return new List<SObject>();
        }

        SObjectType targetType = null;

        for (Id objectId : objectIds) {
            if (objectId == null) {
                continue;
            }

            Schema.SObjectType possibleType = objectId.getSObjectType();
            if (targetType == null) {
                targetType = possibleType;
            } else if (possibleType != targetType) {
                throw new SObjectUtilsException('Please make sure that you send ids of only one type when trying to load objects. If want to load objects of different type, please invoke this method multiple times.');
            }
        }

        if (targetType == null) {
            return new List<SObject>();
        }

        SOQLBuilder bldr = new SOQLBuilder(targetType);
        bldr.addWhereClauseExpression('id in :objectIds');

        return Database.query(bldr.getSOQL());
    }

    /*
     * Loads object from database as select * from SObjectType where id = :objectId
     */
    global static SObject loadSObject(Id objectId) {
        if (objectId == null) {
            return null;
        }

        List<SObject> objectsWithId = SObjectUtils.loadSObjects(new Set<Id> { objectId });

        if (objectsWithId == null || objectsWithId.isEmpty()) {
            return null;
        }
        return objectsWithId[0];
    }

    /**
     * Reads object field value including relationship fetch
     */
    global static Object getSObjectFieldValue(SObject obj, String fieldName) {
       if (String.isBlank(fieldName)) {
            return null;
        }

        List<String> fieldNameFactors = fieldName.split('\\.');
        SObject currentValueHolder = obj;
        for (Integer fieldNameIter = 0; fieldNameIter < fieldNameFactors.size() && currentValueHolder != null; fieldNameIter++) {
            if (fieldNameIter < fieldNameFactors.size() - 1) {
                currentValueHolder = currentValueHolder.getSObject(fieldNameFactors.get(fieldNameIter));
            } else {
                return currentValueHolder.get(fieldNameFactors.get(fieldNameIter));
            }
        }

        return null;
    }

    /**
     * Sorts given list by given field names.
     */
    global static void sortSObjects(List<SObject> listToSort, List<String> sortByFields) {
        List<SObjectSortWrapper> wrappers = new List<SObjectSortWrapper>();

        for (SObject sObj : listToSort) {
            wrappers.add(new SObjectSortWrapper(sObj, sortByFields));
        }
        wrappers.sort();

        for (Integer i = 0; i < wrappers.size(); i++) {
            listToSort[i] = wrappers[i].sObj;
        }
    }

    private class SObjectSortWrapper implements Comparable {
        public SObject sObj;
        public List<String> sortFields;

        public SObjectSortWrapper(SObject sObj, List<String> sortFields) {
            this.sObj = sObj;
            this.sortFields = sortFields;
        }

        public Integer compareTo(Object other) {
            if (!(other instanceof SObjectSortWrapper)) {
                return -1;
            }

            for(String field : this.sortFields) {
                Object myValue = this.sObj.get(field);
                Object otherValue = ((SObjectSortWrapper) other).sObj.get(field);

                Integer compareResult = this.compareTwoValues(myValue, otherValue);
                // not of the same type for some reason
                if (compareResult == null) {
                    return -1;
                } else if (compareResult != 0) {
                    return compareResult;
                }
            }

            return 0;
        }

        private Integer compareTwoValues(Object v1, Object v2) {
            if (v1 == null) {
                return -1;
            }
            if (v2 == null) {
                return 1;
            }

            if (v1 instanceof Comparable) {
                return ((Comparable) v1).compareTo(v2);
            }

            if (v1 instanceof String && v2 instanceof String) {
                return ((String) v1).compareTo((String) v2);
            }

            if (v1 instanceof Decimal && v2 instanceof Decimal) {
                return this.compareDecimals((Decimal) v1, (Decimal) v2);
            }

            if (v1 instanceof Boolean && v2 instanceof Boolean) {
                return this.compareDecimals(v1 == true ? 1 : 0, v2 == true ? 1 : 0);
            }

            if (v1 instanceof Date && v2 instanceof Date) {
                return this.compareDates((Date) v1, (Date) v2);
            }

            if (v1 instanceof DateTime && v2 instanceof DateTime) {
                return this.compareDateTimes((DateTime) v1, (DateTime) v2);
            }

            return null;
        }

        private Integer compareDecimals(Decimal v1, Decimal v2) {
            if (v1 < v2) {
                return -1;
            } else if (v1 > v2) {
                return 1;
            }
            return 0;
        }

        private Integer compareDates(Date v1, Date v2) {
            if (v1 < v2) {
                return -1;
            } else if (v1 > v2) {
                return 1;
            }
            return 0;
        }

        private Integer compareDateTimes(DateTime v1, DateTime v2) {
            if (v1 < v2) {
                return -1;
            } else if (v1 > v2) {
                return 1;
            }
            return 0;
        }
    }

    global class SObjectUtilsException extends Exception {}
}