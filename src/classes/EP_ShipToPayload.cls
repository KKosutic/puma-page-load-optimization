/**
   @Author          Sandeep Kumar
   @name            EP_ShipToPayload
   @CreateDate      01/17/2017
   @Description     This class contains payload methods for shipTo(account) object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON

    -------------------------------------------------------------------------------------------------------------
    *************************************************************************************************************
*/
public with sharing class EP_ShipToPayload {
    String externalSystem;
    EP_PayLoadHelper payLoadHelper;
    EP_PayLoadService payLoadService;
    private Id storageShipToType;


    /**
    *  Constructor
    *  @name EP_ShipToPayload
    *  @param NA
    *  @return N/A
    *  @throws NA
    */ 
    public EP_ShipToPayload() {
        payLoadService = new EP_PayLoadService();
        payLoadHelper = new EP_PayLoadHelper();
    }

    
    /**
    *  This method creates ship to addresses payload for bulk ship to address edit NAV
    *  @name createNAVBulkShipToEditPayLoad
    *  @param set < Id > mShipToIds
    *  @return String
    *  @throws generic Exception
    */ 
    public String createNAVBulkShipToEditPayLoad(set < Id > mShipToIds) {
        String shipToAddressesXML = EP_COMMON_CONSTANT.BLANK;
        String query;
        EP_PayLoadHelper.WrapperFields shipTOWrapperFields;
        try {
            //Line added by Sandeep, dated 2/1/2017
            EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.NAV_CUSTOMER;
            
            //Commented out By Sandeep, dated 2/1/2017
            //externalSystem = EP_Common_Constant.NAV_CUSTOMER;
            //payLoadHelper.createObjectFieldNodeMap(EP_Common_Constant.NAV_CUSTOMER);
            
            shipTOWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.ACCOUNT_OBJ, EP_PayLoadConstants.NAV_SHIP_TO_FIELDS);

            //fetch all shipTo accounts where id is in mShipToIds
            query = EP_Common_Constant.SELECT_STRING +
                shipTOWrapperFields.fieldString +
                EP_PayLoadConstants.FROM_SHIP_TO_EDIT_NAV;

            //CREATE SHIP TO ADDRESSESS HIERARCHY PAYLOAD FOR NAV    
            for (Account shipToAddress: DataBase.query(query)) {
                shipToAddressesXML += payLoadService.createPayload(shipToAddress, shipTOWrapperFields.lFieldAPINames, EP_Common_Constant.SHIP_TO,
                                                payLoadService.createPayload((String) shipToAddress.get(EP_PayLoadConstants.EP_ACCOUNT_COMPANY_NAME),
                                                    EP_Common_Constant.CLIENT_ID, null));
            }

            shipToAddressesXML = payLoadService.createPayload(shipToAddressesXML, EP_Common_Constant.SHIP_TO_ADDRESSES, null);
            
            shipToAddressesXML = shipToAddressesXML.replace(EP_Common_Constant.Identifier, EP_Common_Constant.Identifier.toLowerCase());
        } catch (Exception handledException) {
            throw handledException;
        }
        return shipToAddressesXML;
    }


    /**
    *  This method creates Payload for bulk Ship to Addresses record for NAVISION
    *  @name createNavBulkShipToPayload
    *  @param List < SObject > lSFObjects, List < String > lFieldAPINames
    *  @return string
    *  @throws Generic Exception
    */ 
    public string createNavBulkShipToPayload(List < SObject > lSFObjects, List < String > lFieldAPINames) {
        String shipToAddressesXML = EP_COMMON_CONSTANT.BLANK;
        try {
            for (SObject sfObject: lSFObjects) {
                shipToAddressesXML += payLoadService.createPayload(sfObject, lFieldAPINames, EP_Common_Constant.SHIP_TO, null);
                payLoadHelper.populateChildParentMap((String) sfObject.get(EP_Common_Constant.ID), (String) sfObject.get(EP_PayLoadConstants.PARENT_ID));
            }
            shipToAddressesXML = payLoadService.createPayload(shipToAddressesXML, EP_Common_Constant.SHIP_TO_ADDRESSES, null);
        } catch (Exception handledException) {
            throw handledException;
        }
        return shipToAddressesXML;
    }

    /**
    *  This method creates Payload for Ship to addresses record for LOMOSOFT import
    *  @name createLomoSoftBulkShipToPayLoad
    *  @param map<Id,String> mShipToStatus, String fieldSetName, Boolean isEditPayload
    *  @return String
    *  @throws Generic Exception
    */  
    public String createLomoSoftBulkShipToPayLoad(map<Id,String>mShipToStatus
                                                                 ,String fieldSetName, Boolean isEditPayload){
        //CREATE SHIP TO ADDRESSESS HIERARCHY PAYLOAD FOR LOMOSOFT
        String shipToAddressesXML = EP_Common_Constant.BLANK;
        String lomoSoftTanksXML = EP_Common_Constant.BLANK;
        String lomoSoftSupplyLocationsXML = EP_Common_Constant.BLANK;
        String query;   
        EP_PayLoadHelper.WrapperFields shipTOWrapperFields;
        EP_PayLoadHelper.WrapperFields tankWrapperFields;
        EP_PayLoadHelper.WrapperFields supplyLocationWrapperFields;
        
        //when shipTo is edited
        String accountSetupStatus = EP_COMMON_CONSTANT.STATUS_SET_UP;
        set<Id> sShipToId = new set<Id>();
        
        try{
            //when shipTo is edited
            sShipToId.addAll(mShipToStatus.keySet()); 
            //Line added by Sandeep, dated 2/1/2017
            EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.LS_CUSTOMER;
                  
            //Commented out By Sandeep, dated 2/1/2017
            //externalSystem = EP_Common_Constant.LS_CUSTOMER;
            //payLoadHelper.createObjectFieldNodeMap(EP_Common_Constant.LS_CUSTOMER);
            if(String.isBlank(fieldSetName) && !isEditPayload){
                fieldSetName = EP_Common_Constant.LOMO_SHIP_TO_FIELDS;
                if(!isEditPayload) {
                    storageShipToType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.STORAGE_SHIP_TO);
                }
            }       
            shipTOWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.ACCOUNT_OBJ
                                                        ,fieldSetName);
            tankWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.TANK_OBJ
                                                    ,EP_PayLoadConstants.LOMO_TANK_FIELDS); 
            supplyLocationWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.SUPPLY_LOCATION_OBJ
                                                                ,EP_PayLoadConstants.LOMO_SUPPLY_LOCATION_FIELDS);
            
             query = EP_Common_Constant.SELECT_STRING
                   + shipTOWrapperFields.fieldString
                   + EP_Common_Constant.COMMA_STRING
                   + EP_PayLoadConstants.EP_ACCOUNT_COMPANY_NAME
                   + EP_Common_Constant.COMMA_STRING 
                   + EP_Common_Constant.LEFT_PARENTHESIS
                   + EP_Common_Constant.SELECT_STRING
                   + tankWrapperFields.fieldString
                   + (isEditPayload? EP_PayLoadConstants.FROM_SHIP_TO_EDIT_TANKS : EP_PayLoadConstants.FROM_SHIP_TO_TANKS)
                   + EP_Common_Constant.COMMA_STRING
                   + EP_Common_Constant.LEFT_PARENTHESIS
                   + EP_Common_Constant.SELECT_STRING
                   + supplyLocationWrapperFields.fieldString
                   + (isEditPayload? EP_PayLoadConstants.FROM_SHIP_TO_EDIT_SUPPLY_LOCATIONS : EP_PayLoadConstants.FROM_SHIP_TO_SUPPLY_LOCATIONS);
            
            for(Account shipToAddress : DataBase.query(query)){
                if(shipToAddress.Tank__r.isEmpty()){
                    lomoSoftTanksXML = payLoadHelper.openingTag(EP_Common_Constant.TANKS);
                }else{
                    lomoSoftTanksXML = createLomoSoftBulkSObjectPayLoad(shipToAddress.Tank__r, 
                                          tankWrapperFields.lFieldAPINames, EP_Common_Constant.TANK, 
                                          EP_Common_Constant.TANKS, EP_Common_Constant.TANK_SHIP_TO_API_NAME);
                }
                if(shipToAddress.Stock_Holding_Locations1__r.isEmpty()){
                    lomoSoftSupplyLocationsXML = payLoadHelper.openingTag(EP_Common_Constant.SUPPLY_LOCATIONS);
                }else{
                    lomoSoftSupplyLocationsXML = createLomoSoftBulkSObjectPayLoad(shipToAddress.Stock_Holding_Locations1__r, 
                                                  supplyLocationWrapperFields.lFieldAPINames, EP_Common_Constant.SUPPLY_LOCATION,                                      
                                                   EP_Common_Constant.SUPPLY_LOCATIONS, EP_Common_Constant.TANK_SHIP_TO_API_NAME);
                }
                
                shipToAddressesXML += createLomoSoftShiptoPayLoad(shipToAddress
                                                                        ,isEditPayload? mShipToStatus.get(shipToAddress.id) : ShipToAddress.EP_Status__c
                                                                        ,shipTOWrapperFields.lFieldAPINames
                                                                        ,lomoSoftTanksXML 
                                                                        ,lomoSoftSupplyLocationsXML
                                                                        ,true);
            }                                           
            shipToAddressesXML = payLoadService.createPayload(shipToAddressesXML, EP_Common_Constant.SHIP_TO_ADDRESSES, null);
            shipToAddressesXML = shipToAddressesXML.replace(EP_Common_Constant.Identifier
                                                            ,EP_Common_Constant.Identifier.toLowerCase());
            
        }
        catch(Exception handledException){
            throw handledException;
        }
        
        return shipToAddressesXML;
    }
    
    /**
    *  METHOD FOR LOMOSOFT TANK EDIT PAYLOAD
    *  @name createLomosoftBulkTankEdit
    *  @param map <Id, Id> mUpdateTanksShipTo
    *  @return String
    *  @throws Generic Exception
    */ 
    public String createLomosoftBulkTankEdit(map <Id, Id> mUpdateTanksShipTo) {
        String query;
        String externalSystem;
        String shipToAddressesXML;
        String lomoSoftTanksXML;
        set < Id > sShipToIds = new set < id > ();
        set < Id > sTankIds = new set < id > ();
        EP_PayloadHelper.WrapperFields tankWrapperFields;
        EP_PayloadHelper.WrapperFields shipTOWrapperFields;
        try {
            //Line added by Sandeep, dated 2/1/2017
            EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.LS_CUSTOMER;
            
            //Commented out By Sandeep, dated 2/1/2017
            externalSystem = EP_Common_Constant.LS_CUSTOMER;
            payLoadHelper.createObjectFieldNodeMap(EP_Common_Constant.LS_CUSTOMER);
            shipTOWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.LOMO_SHIP_TO_FIELDS);
            tankWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.TANK_OBJ, EP_PayLoadConstants.LOMO_TANK_FIELDS);

            shipToAddressesXML = EP_Common_Constant.BLANK;

            sShipToIds.addAll(mUpdateTanksShipTo.keySet());
            sTankIds.addAll(mUpdateTanksShipTo.values());

            query = EP_Common_Constant.SELECT_STRING +
                shipTOWrapperFields.fieldString +
                EP_Common_Constant.COMMA_STRING +
                EP_Common_Constant.LEFT_PARENTHESIS +
                EP_Common_Constant.SELECT_STRING +
                tankWrapperFields.fieldString +
                EP_PayLoadConstants.FROM_SHIP_TANKS_EDIT_LS;

            for (Account shipToAddress: DataBase.query(query)) {

                //createObjectRecordMap(ACCOUNTOBJ,shipToAddress.id);
                lomoSoftTanksXML = createLomoSoftBulkSObjectPayLoad(shipToAddress.Tank__r,
                    tankWrapperFields.lFieldAPINames, EP_Common_Constant.TANK,
                    EP_Common_Constant.TANKS, EP_Common_Constant.TANK_SHIP_TO_API_NAME);

                shipToAddressesXML += createLomoSoftShiptoPayLoad(shipToAddress, ShipToAddress.EP_Status__c, shipTOWrapperFields.lFieldAPINames, lomoSoftTanksXML, EP_Common_Constant.BLANK, false);


            }
            shipToAddressesXML = payLoadService.createPayload(shipToAddressesXML, EP_Common_Constant.SHIP_TO_ADDRESSES, null);
            shipToAddressesXML = shipToAddressesXML.replace(EP_Common_Constant.Identifier, EP_Common_Constant.Identifier.toLowerCase());
        } catch (Exception handledException) {
            throw handledException;
        }
        return shipToAddressesXML;

    }

    /**
    *  This method creates Payload for bulk sObject records for LOMOSOFT
    *  @name createLomoSoftBulkSObjectPayLoad
    *  @param List < sObject > lSFObjects, List < String > LSFields, String objSingularName, String objPluralName, String shipToRelshipField
    *  @return string
    *  @throws Generic Exception
    */ 
    private string createLomoSoftBulkSObjectPayLoad(List < sObject > lSFObjects, List < String > LSFields, String objSingularName,
        String objPluralName, String shipToRelshipField) {
        String lomoSoftSObjectXML = EP_Common_Constant.BLANK;
        try {
            //CREATE LOMOSOFT PAYLOAD FOR BULK RECORDS
            for (sObject sObj: lSFObjects) {
                lomoSoftSObjectXML = payLoadService.createPayload(sObj, LSFields, objSingularName, null);
                payLoadHelper.populateChildParentMap((String) sObj.get(EP_Common_Constant.ID), (String) sObj.get(shipToRelshipField));
            }
            lomoSoftSObjectXML = payLoadService.createPayload(lomoSoftSObjectXML, objPluralName, null);
        } catch (Exception handledException) {
            throw handledException;
        }
        return lomoSoftSObjectXML;
    }

    /**       
    *  This method creates ship to payload for edit/create lomosoft
    *  @name updateVMIShipToUTCTimeZoneOffset
    *  @param Account shipToAddress, String status, List < String > lFieldAPINames, String lomoSoftTanksXML, String lomoSoftSupplyLocationsXML, Boolean isEditPayload
    *  @return string
    *  @throws Generic Exception
    */ 
    private string createLomoSoftShiptoPayLoad(Account shipToAddress, String status, List < String > lFieldAPINames, String lomoSoftTanksXML, String lomoSoftSupplyLocationsXML, Boolean isEditPayload) {
        String shipToAddressXML;
        String noStatusNode;
        String yesStatusNode;
        set < Id > sCstmrRecType;
        sCstmrRecType = new set < Id > {
            EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.BILL_TO),
            EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.SELL_TO)
        };
        noStatusNode = payLoadService.createPayload(EP_Common_Constant.STRING_NO_LS, EP_Common_Constant.LS_NODE_STATUS, null);
        yesStatusNode = payLoadService.createPayload(EP_Common_Constant.STRING_YES_LS, EP_Common_Constant.LS_NODE_STATUS, null);

        //CREATE SHIP TO ADDRESSES PAYLOAD FOR LOMOSOFT
        //TO DO - SUPPLY LOCATIONS AND TANKS
        shipToAddressXML = payLoadService.createPayload(shipToAddress, lFieldAPINames, EP_Common_Constant.SHIP_TO, lomoSoftTanksXML +
            lomoSoftSupplyLocationsXML +
            payLoadService.createPayload(shipToAddress.EP_Puma_Company_Code__c, EP_Common_Constant.CLIENT_ID, null));
        Set < String > inactiveStatus = new Set < String > {
            EP_Common_Constant.STATUS_BLOCKED.toUpperCase(),
            EP_Common_Constant.STATUS_INACTIVE.toUpperCase()
        };
        if (isEditPayload) {
            if (shipToAddressXML.contains(noStatusNode) &&
                status.equalsIgnoreCase(EP_Common_Constant.STRING_YES_LS)) {
                shipToAddressXML = shipToAddressXML.replace(noStatusNode, yesStatusNode);
            }
        } else {
            if (shipToAddressXML.contains(noStatusNode) && !inactiveStatus.contains(status.toUpperCase())) {
                shipToAddressXML = shipToAddressXML.replace(noStatusNode, yesStatusNode);
            }
        }

        if ((Id) shipToAddress.RECORDTYPEID == storageShipToType) {
            String shipToId = shipToAddress.EP_Account_Id__c;
            String storageLocId = shipToAddress.EP_Storage_Location__r.EP_Nav_Stock_Location_Id__c;
            shipToAddressXML = shipToAddressXML.replace(EP_PayLoadConstants.SHIPTO_ID_TAG + shipToId + EP_PayLoadConstants.SHIPTO_ID_START_TAG, EP_PayLoadConstants.SHIPTO_ID_TAG + storageLocId + EP_PayLoadConstants.SHIPTO_ID_START_TAG);
        }
        if (sCstmrRecType.contains(shipToAddress.RecordTypeId)) {
            String custId = shipToAddress.EP_Account_Id__c;
            String ParentId = (String) payLoadHelper.getXMLValue(shipToAddress.EP_Parent_Customer_Nr__c);
            if (String.isBlank(ParentId)) {
                shipToAddressXML = shipToAddressXML.replace(EP_PayLoadConstants.CUSID_END_TAG, EP_PayLoadConstants.CUSID_TAG + custId + EP_PayLoadConstants.CUSID_START_TAG);
            } else {
                shipToAddressXML = shipToAddressXML.replace(EP_PayLoadConstants.CUSID_TAG + ParentId + EP_PayLoadConstants.CUSID_START_TAG, EP_PayLoadConstants.CUSID_TAG + custId + EP_PayLoadConstants.CUSID_START_TAG);
            }
        }
        return shipToAddressXML;
    }
}