/* 
  @Author <Ashok Arora>
   @name <EP_TankTriggerHandler_R1>
   @CreateDate <03/11/2015>
   @Description <This class handles requests from EP_TankTrigger> 
   @Novasuite Fixes - Required Documentation
   @Version <1.0>
*/
public with sharing class EP_TankTriggerHandler_R1 {
    public static  Boolean isBeforeUpdate = false;
    public static  Boolean isAfterUpdate = false;
    public static  Boolean executeTankTrigger = true;
    /*
    Before Insert
    */
    public static void doBeforeInsert(List<EP_Tank__c>lNewTanks){
    	if(executeTankTrigger){
	        //VALIDATE TANK CODE UNIQUENESS
	        EP_TankTriggerHelper_R1.validateTankCode(lNewTanks);
	        //Validate selected product
	        EP_TankTriggerHelper_R1.validateProduct(lNewTanks);
	
	        //SET FRREQUNCY ON TANKS FROM RELATED SHIP-TO
	        EP_TankTriggerHelper_R1.setCurrencyOnTank(lNewTanks);
    	}
    }
    /*
    After Update
    */
    public static void doAfterUpdate(Map<Id, EP_Tank__c> mapOldTanks,map<Id,EP_Tank__c>mNewTanks ,List<EP_Tank__c> lOldTanks) {
    	if(executeTankTrigger){
	        isAfterUpdate = true;
	        // 1. Delete placeholder tank dips when a tank is stopped
	        EP_TankTriggerHelper_R1.handlePlaceholderTankDipsBasedOnTankStatus(mapOldTanks, lOldTanks);
	        //Sync edit on tanks with WinDMS
	        EP_TankTriggerHelper_R1.syncTankUpdatesWithWINDMS(mapOldTanks,mNewTanks);
	        //update missing tank dip days field on tank
	        EP_TankTriggerHelper_R1.updateMissingTankDipDays(lOldTanks);
    	}
       
    }
    /*
    Before Update
    */
    public static void doBeforeupdate(List<EP_Tank__c>lOldTanks
                                        ,List<EP_Tank__c>lNewTanks
                                        ,map<id,EP_Tank__c>mOldTanks
                                        ,map<id,EP_Tank__c>mNewTanks){
		if(executeTankTrigger){                                        	
	        isBeforeUpdate = true;
	        //VALIDATE TANK CODE UNIQUENESS
	        EP_TankTriggerHelper_R1.validateTankCode(lNewTanks);
	        //Validate selected product
	        EP_TankTriggerHelper_R1.validateProduct(lNewTanks);
	        //CANNOT CHANGE Tank STATUS TO BASIC DATA UNTIL CSC REVIEW IS COMPLETED
	        EP_TankTriggerHelper_R1.CmpltCscReviewBeforeBasicSetUp(mOldTanks
	                                                            ,mNewTanks);
	        EP_TankTriggerHelper_R1.allowModificationOnTank(lNewTanks,mOldTanks);
	        /**** Defect 44565 Fix START****/
	        //Restrict user to update tank if action is not assigned 
	        EP_TankTriggerHelper_R1.restrictUserToUpdateTankWhenActionNotAssigned(lNewTanks, mOldTanks);
			/**** Defect 44565 Fix END****/
		}

    }
    /*
    L4_45352_Start
    */
    public static void doAfterInsert(List<EP_Tank__c>lNewTanks
                                        ,map<id,EP_Tank__c>mNewTanks){
    
            EP_TankTriggerHelper_R1.syncTankOnInsert(mNewTanks);
    }
    //L4_45352_End
    
}