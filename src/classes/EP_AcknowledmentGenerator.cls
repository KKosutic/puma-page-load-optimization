/**** 
    @Author      <Arpit Sethi - Accenture>
    @name        <EP_AcknowledmentGenerator>
    @CreateDate  <11/02/2016>
    @Description <This class is used to send acknowledment to other systems. 
                  It contins innerclasses which is being used to create Acknowledgement structure> 
    @verion      <1.0>
****/
public without sharing class EP_AcknowledmentGenerator{
    public Cls_MSG MSG;
    private static final String CREATE_ACKNOWLEDGEMENT = 'createacknowledgement';
    private static final string CLASSNAME = 'EP_AcknowledmentGenerator';
    private static final string METHOD_PUT = 'put';
        
    /**** 
        @Author        <Arpit Sethi- Accenture>
        @name          <Cls_MSG>
        @CreateDate    <11/02/2016>
        @Description   <This is main wraper class for Acknowledgement, 
                       It Contains different segment of acknowledgement :
                       Headercomponent- Cls_HeaderCommon
                       acknowledgement - Cls_acknowledgement
                       StatusPayload - status of Request- processd/Faileure >
        @version       <1.0>
    ****/
    public without sharing class Cls_MSG {
        public Cls_HeaderCommon HeaderCommon;
        public Cls_acknowledgement acknowledgement;
        public String StatusPayload;    //StatusPayload
    }
    
    
    /**** 
        @Author        <Arpit Sethi- Accenture>
        @name          <Cls_HeaderCommon>
        @CreateDate    <11/02/2016>
        @Description   <This wrapper class contains all the variables/Fields which are being sent in header segment of Acknowledgement>
        @version       <1.0>
    ****/
    public without sharing class Cls_HeaderCommon {
        public String MsgID;    //MsgID
        public String InterfaceType;    //InterfaceType
        public String SourceGroupCompany;   //SourceGroupCompany
        public String DestinationGroupCompany;  //DestinationGroupCompany
        public String SourceCompany;    //SourceCompany
        public String DestinationCompany;   //DestinationCompany
        public String CorrelationID;    //CorrelationID
        public String DestinationAddress;   //DestinationAddress
        public String SourceResponseAddress;    //SourceResponseAddress
        public String SourceUpdateStatusAddress;    //SourceUpdateStatusAddress
        public String DestinationUpdateStatusAddress;   //DestinationUpdateStatusAddress
        public String MiddlewareUrlForPush; //MiddlewareUrlForPush
        public String EmailNotification;    //EmailNotification
        public String ErrorCode;    //ErrorCode
        public String ErrorDescription; //ErrorDescription
        public String ProcessingErrorDescription;   //ProcessingErrorDescription
        public String ContinueOnError;  //true
        public String ComprehensiveLogging; //true
        public String TransportStatus;  //TransportStatus
        public String ProcessStatus;    //ProcessStatus
        public String UpdateSourceOnReceive;    //true
        public String UpdateSourceOnDelivery;   //true
        public String UpdateSourceAfterProcessing;  //true
        public String UpdateDestinationOnDelivery;  //true
        public String CallDestinationForProcessing; //true
        public String ObjectType;   //ObjectType
        public String ObjectName;   //ObjectName
        public String CommunicationType;    //CommunicationType
        
        /**** 
            @Author        <Arpit Sethi- Accenture>
            @MethodName    <put>
            @CreateDate    <11/02/2016>
            @Description   <this method is being used for adding values in header variables dynamically in code.>
            @Param          1. Striung fieldName -  name of the variable
                            2. string fieldValue - value of the variable
            @ReturnType     Void
        ****/
        public void put(string fieldName, string fieldValue){
            try{
                if(fieldName.equalsIgnoreCase(EP_Common_Constant.MSG_ID)){
                    this.MsgID = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.INTERFACE_TYPE)){
                    this.InterfaceType = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.SOURCE_GROUP_COMPANY)){
                    this.SourceGroupCompany = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.DESTINATION_GROUP_COMPANY)){
                    this.DestinationGroupCompany = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.SOURCE_COMPANY)){
                    this.SourceCompany = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.DESTINATION_COMPANY)){
                    this.DestinationCompany = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.CORR_ID)){
                    this.CorrelationID = fieldValue;    
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.DESTINATION_ADDRESS)){
                    this.DestinationAddress = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.SOURCE_RESP_ADD)){
                    this.SourceResponseAddress = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.SOURCE_UPDATE_STTS_ADD)){
                    this.SourceUpdateStatusAddress = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS)){
                    this.DestinationUpdateStatusAddress = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH)){
                    this.MiddlewareUrlForPush = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.EMAIL_NOTIFICATION)){
                    this.EmailNotification = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.ACK_ERROR_CODE)){
                    this.ErrorCode = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.ERROR_DESCRIPTION)){
                    this.ErrorDescription = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION)){
                    this.ProcessingErrorDescription = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.CONT_ON_ERR_STTS)){
                    this.ContinueOnError = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.COMP_LGNG)){
                    this.ComprehensiveLogging = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.TRANSPORT_STATUS)){
                    this.TransportStatus = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.PROCESS_STATUS)){
                    this.ProcessStatus = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.UPDT_SRC_ON_RCV)){
                    this.UpdateSourceOnReceive = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.UPDT_SRC_ON_DLVRY)){
                    this.UpdateSourceOnDelivery = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG)){
                    this.UpdateSourceAfterProcessing = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.UPDT_DEST_ON_DLVRY)){
                    this.UpdateDestinationOnDelivery = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.CALL_DEST_FOR_PRCSNG)){
                    this.CallDestinationForProcessing = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.OBJECT_TYPE)){
                    this.ObjectType = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.OBJECT_NAME)){
                    this.ObjectName = fieldValue;
                }else if(fieldName.equalsIgnoreCase(EP_Common_Constant.COMMUNICATION_TYPE)){
                    this.CommunicationType = fieldValue;                                            
                }
            }catch(Exception handledException){
                EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, METHOD_PUT, CLASSNAME, ApexPages.Severity.ERROR);
            }
        }
    }
            
    /**** 
        @Author      <Arpit Sethi- Accenture>
        @name        <Cls_acknowledgement>
        @CreateDate  <11/02/2016>
        @Description <This wrapepr class is being used to create dataset in payload. 
                     This data set contains process status, Error Message if any, for all processed records.> 
        @Version     <1.0>
    ****/
    public without sharing class Cls_acknowledgement {
        public Cls_dataSets dataSets;
    }
        
    /**** 
        @Author      <Arpit Sethi- Accenture>
        @name        <Cls_dataSets>
        @CreateDate  <11/02/2016>
        @Description <This inner class is used as a wrapper class containing List of Cls_dataset> 
        @Version     <1.0>
    ****/
    public without sharing class Cls_dataSets {
        public List<Cls_dataset> dataset;
    }
        
    
    /**** 
        @Author      <Arpit Sethi- Accenture>
        @name        <Cls_dataset>
        @CreateDate  <11/02/2016>
        @Description <This class is used to contain single record details which is being proesseed and need to send in acknowledgement.
                      this data set contains the recordName, Id, Error description in case of any Error > 
        @Version     <1.0>
    ****/
    public without sharing class Cls_dataset {
        public String name; //name
        public String seqId;    //seqId
        public String errorCode;    //errorCode
        public String errorDescription; //errorDescription
    }
       
    /**** 
        @Author        <Arpit Sethi- Accenture>
        @MethodName    <createacknowledgement>
        @CreateDate    <15/02/2016>
        @Description   <this method is being used for generating acknowledgement>
        @Param          1. Map<String,String> mapOfHeaderCommonIdDetails -  Map of header component variables
                        2. List<EP_AcknowledmentGenerator.Cls_dataset> datasetRecords -list of dataset - process details of records.
                        3. String statusPayload - transaction status - Failure/ processed
        @ReturnType     String response in JSON format
    ****/
    public static String createAcknowledgement(Map<String,String> mapOfHeaderCommonIdDetails,
        List<EP_AcknowledmentGenerator.Cls_dataset> datasetRecords,String statusPayload){       
        EP_AcknowledmentGenerator ackGenerator = new EP_AcknowledmentGenerator();
        ackGenerator.MSG = new EP_AcknowledmentGenerator.Cls_MSG();
        ackGenerator.MSG.HeaderCommon = new EP_AcknowledmentGenerator.Cls_HeaderCommon();
        ackGenerator.MSG.acknowledgement = new EP_AcknowledmentGenerator.Cls_acknowledgement();
        ackGenerator.MSG.acknowledgement.datasets = new EP_AcknowledmentGenerator.Cls_dataSets();
        ackGenerator.MSG.acknowledgement.datasets.dataset = new List<EP_AcknowledmentGenerator.Cls_dataset>();
        ackGenerator.MSG.StatusPayload = statusPayload;
        
        try{
            ackGenerator.MSG.HeaderCommon = fetchHeaderFields(mapOfHeaderCommonIdDetails);
            for(EP_AcknowledmentGenerator.Cls_dataset datasetRec : datasetRecords){
                ackGenerator.MSG.acknowledgement.datasets.dataset.add(datasetRec);
            }
            
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA,CREATE_ACKNOWLEDGEMENT , CLASSNAME, ApexPages.Severity.ERROR);
        }
        
        String jsonResponse = JSON.serialize(ackGenerator.MSG);
        system.debug('====jsonResponse======'+jsonResponse);        
        return jsonResponse;
    }
        
   /**** 
        @Author        <Amit Singh- Accenture>
        @MethodName    <fieldCheck>
        @CreateDate    <15/11/2016>
        @Description   <this method is being used for assign specific value in specific header variable>
        @Param          1. Map<String,String> headerCommon -  Map of header component variables -Map< Variable Name - Variable Value >
                        2. string fieldToCheck -Name of the variable.
                        3. Cls_HeaderCommon headerCommonObj - headerobject instance
        @ReturnType     void
     ****/
    private static void fieldCheck(Map<String,String> mapOfHeaderCommonIdDetails, 
                                   string fieldToCheck, Cls_HeaderCommon headerCommonObj){
       if(mapOfHeaderCommonIdDetails.containsKey(fieldToCheck) 
          && mapOfHeaderCommonIdDetails.get(fieldToCheck) != NULL ){
              headerCommonObj.put(fieldToCheck,mapOfHeaderCommonIdDetails.get(fieldToCheck));
          }   
    }
    
    /**** 
        @Author        <Amit Singh- Accenture>
        @MethodName    <fetchHeaderFields>
        @CreateDate    <15/11/2016>
        @Description   <this method is being used for assign values in all header variable>
        @Param          1. Map<String,String> headerCommon -  Map of header component variables -Map< Variable Name - Variable Value >
        @ReturnType     Cls_HeaderCommon instance
    ****/
    private static Cls_HeaderCommon fetchHeaderFields(Map<String,String> mapOfHeaderCommonIdDetails){
        Cls_HeaderCommon headerCommonObj = new Cls_HeaderCommon();
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.MSG_ID,headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.INTERFACE_TYPE, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.SOURCE_GROUP_COMPANY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.DESTINATION_GROUP_COMPANY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.SOURCE_COMPANY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.DESTINATION_COMPANY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.CORR_ID, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.DESTINATION_ADDRESS, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.SOURCE_RESP_ADD,headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.SOURCE_UPDATE_STTS_ADD, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.EMAIL_NOTIFICATION, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.ACK_ERROR_CODE, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.ERROR_DESCRIPTION, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.CONT_ON_ERR_STTS,headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.COMP_LGNG, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.TRANSPORT_STATUS, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.PROCESS_STATUS, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.UPDT_SRC_ON_RCV, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.UPDT_SRC_ON_DLVRY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.UPDT_DEST_ON_DLVRY, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.CALL_DEST_FOR_PRCSNG, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.OBJECT_TYPE, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.OBJECT_NAME, headerCommonObj);
        fieldCheck(mapOfHeaderCommonIdDetails, EP_Common_Constant.COMMUNICATION_TYPE, headerCommonObj);
        return headerCommonObj;
    }
}