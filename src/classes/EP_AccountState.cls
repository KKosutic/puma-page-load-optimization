/*
 *  @Author <Accenture>
 *  @Name <EP_AccountState>
 *  @CreateDate <03/02/2017>
 *  @Description <AccountState validates inbound status of Account is valid, find next possible status and return the final recommended status>
 *  @Version <1.0>
 */

 public virtual class EP_AccountState { 
    
    public EP_AccountDomainObject account;
    public EP_AccountEvent accountEvent;
    public List<EP_AccountStateTransition> accountStateTransitions;
    //private static string accountconstant = 'Account'; //To do : Commom constant class
    
    public EP_AccountState(){        
    }
    
    public virtual void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_AccountState','doOnEntry');
    }   

    public virtual void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_AccountState','doOnExit');
    }

    public virtual boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_AccountState','isInboundTransitionPossible');
        return true;
    }


    public virtual boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_AccountState','doTransition');
		EP_AccountStateTransition objAccountStatetransition = new EP_AccountStateTransition();
        //If no transitions defined for this state - return false
        if (accountStateTransitions.isEmpty()){            
            if(EP_AccountConstant.PROSPECT.equalsIgnoreCase(account.getStatus())){
            	throw new AccountStateMachineException(EP_AccountConstant.PROSPECT_TO_BASICDATA_MESSAGE);
        	}else{
        		throw new AccountStateMachineException(String.format('Cannot change status from {0} to {1}',new String[]{account.getStatus(),account.toStatus}));
            }
            return false; 
        }

        //Final State of Account will be
        //IF failure transitions exist - then pick the "highest priority" of failure and set the recommended state from that transition
        //IF no failure transitions - then pick the "lowest priority" of the successful transition and set the recommended state
        for(EP_AccountStateTransition ast:accountStateTransitions){
            if (ast.isTransitionPossible()){
                this.account.setStatus(ast.finalState);    
                objAccountStatetransition  = ast;          
                //ast.doOnExit();
                break;
            }
        }       

        //Finally check if the inbound transition to that state is possible
        EP_AccountState finalState = EP_AccountStateMachine.getAccountState(this.account,this.accountEvent);
        if (!finalState.isInboundTransitionPossible()){
                return false;           
        } 
            
        objAccountStatetransition.doOnExit();
        //finally if everything passes - return true
        return true;
    }
    

    public virtual void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_AccountState','setAccountDomainObject');
        this.account = currentAccount;
    }
    
    
    public virtual void setAccountContext(EP_AccountDomainObject currentAccount,EP_AccountEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_AccountState','setAccountContext');
        this.accountEvent = currentEvent;
        this.account = currentAccount;
        this.accountStateTransitions = ConvertCustomSettingsToObject(currentAccount.getStatus()); 
    }

    @TestVisible
    private List<EP_AccountStateTransition> ConvertCustomSettingsToObject(String stateTextValue){    
    	EP_GeneralUtility.Log('private','EP_AccountState','ConvertCustomSettingsToObject');    
	    string stateValue = stateTextValue;        
	    string entitytype = account.localaccount.EP_Entity_Type__c;   
	    string eventName = accountevent.getEventName();
	    //Step 1: Fetch the definitions from the Custom settings
	    List<EP_State_Transitions__c> objStateTransitionList = getStateTransitions(entitytype,stateValue,eventName);
	    //Step 2:convert Custom setting to object
	    List<EP_AccountStateTransition> localST = convertCustomSetting(objStateTransitionList); 
        return localST;
    }
    
	@testVisible
    private List<EP_State_Transitions__c> getStateTransitions(string entityType, string stateName, string eventname){
    	EP_GeneralUtility.Log('private','EP_AccountState','getStateTransitions');
    	List<EP_State_Transitions__c> objStateTransitionsList =  new  List<EP_State_Transitions__c>();
        //Bulkification Implementation -- Start
    	EP_CustomSettingMapper customSetingMapper = new EP_CustomSettingMapper();
        customSetingMapper.listStateTransitions = new List<EP_State_Transitions__c>();
        //customSetingMapper.listStateTransitions = account.listStateTrans;
        System.debug('****account****:-'+account);
    	account.listStateTrans = new List<EP_State_Transitions__c>();
        objStateTransitionsList = customSetingMapper.getStateTransitionsAccount(entityType, stateName, eventname);  
    	//Bulkification Implementation -- End
        return objStateTransitionsList;      	
    }
        
	@testVisible    
    private List<EP_AccountStateTransition> convertCustomSetting(List<EP_State_Transitions__c> objStateTransition){
    	EP_GeneralUtility.Log('private','EP_AccountState','convertCustomSetting');
    	List<EP_AccountStateTransition> localST = new List<EP_AccountStateTransition>();    	
    	for(EP_State_Transitions__c transition: objStateTransition){
        	try{
                Type t = Type.forName(transition.Transition_Class_Type__c);
                EP_AccountStateTransition AT = (EP_AccountStateTransition)t.newInstance();
                AT.setPriority(transition.Transition_Priority__c.intValue());
                AT.setAccountContext(this.account, this.accountEvent);
                localST.add(AT);
            }catch (Exception e){
                throw new AccountStateMachineException('Account State Transition not available for ' + transition.Transition_Class_Type__c);
            }
        }
        return localST;
    }
}