@isTest
public class EP_OrderStateTransition_UT
{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario(); 
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OrderStateTransition ost = new EP_OrderStateTransition();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OrderStateTransition ost = new EP_OrderStateTransition();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OrderStateTransition ost = new EP_OrderStateTransition();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void setOrderContext_test() {
        EP_OrderStateTransition localObj = new EP_OrderStateTransition();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateTransitionDomainObjectPositiveScenario();
        Test.startTest();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(localObj.order.getOrder().id, obj.getOrder().id);
    }
    
    static testMethod void isGuardCondition_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateTransitionDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OSTAIRToSubmitted ost = new EP_OSTAIRToSubmitted();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void setPriority_test() {
          EP_OrderStateTransition ost = new EP_OrderStateTransition();
          Integer priorityInt = 1;
          Test.startTest();
          ost.setPriority(priorityInt );
          Test.stopTest();
          System.AssertEquals(priorityInt ,ost.getPriority());
    }
    
    static testMethod void getPriority_test() {
          EP_OrderStateTransition ost = new EP_OrderStateTransition ();
          Integer priorityInt = 1;
          ost.setPriority(priorityInt );
          Test.startTest();
          Integer result = ost.getPriority();
          Test.stopTest();
          System.AssertEquals(priorityInt ,result);
    }
 }