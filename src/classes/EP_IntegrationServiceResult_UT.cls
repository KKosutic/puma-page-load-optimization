@isTest
public class EP_IntegrationServiceResult_UT
{
    static testMethod void getRequest_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        HTTPRequest requestObj = new HTTPRequest();
        localObj.setRequest(requestObj);
        Test.startTest();
        HttpRequest result = localObj.getRequest();
        Test.stopTest();
        System.AssertEquals(requestObj,result);
    }
    static testMethod void getResponse_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        HTTPResponse responseObj = new HTTPResponse();
        localObj.setResponse(responseObj );
        Test.startTest();
        HttpResponse result = localObj.getResponse();
        Test.stopTest();
        System.AssertEquals(responseObj ,result);
    }
    static testMethod void getErrors_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        List<Exception> listException = new List<Exception>();
        localObj.setErrors(listException );
        Test.startTest();
        LIST<Exception> result = localObj.getErrors();
        Test.stopTest();
        System.AssertEquals(listException ,result );
    }
    static testMethod void setRequest_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        HttpRequest request = new HttpRequest();
        Test.startTest();
        localObj.setRequest(request);
        Test.stopTest();
        System.AssertEquals(request,localObj.getRequest());
    }
    static testMethod void setResponse_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        HttpResponse response = new HttpResponse();
        Test.startTest();
        localObj.setResponse(response);
        Test.stopTest();
        System.AssertEquals(response,localObj.getResponse());
    }
    static testMethod void setErrors_test() {
        EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        LIST<Exception> errors = new LIST<Exception>();
        Test.startTest();
        localObj.setErrors(errors);
        Test.stopTest();
        System.AssertEquals(errors ,localObj.getErrors());
    }
    //#59186 User Story Start
    static testMethod void createErrorResponse_test() { 
    	EP_IntegrationServiceResult localObj = new EP_IntegrationServiceResult();
        
        Test.startTest();
        	HttpResponse results = localObj.createErrorResponse(EP_Common_Constant.HTTP_ERROR_CODE_503,EP_Common_Constant.COMMUNICATION_DISABLE_MSG_JSON);
        Test.stopTest();
        
        System.AssertEquals(503 ,integer.valueOf(results.getStatusCode()));
    }
    //#59186 User Story End
}