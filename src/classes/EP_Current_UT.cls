@isTest
public class EP_Current_UT
{

/*static testMethod void setETA_test() {
    EP_Current localObj = new EP_Current();     
    Order objOrder = EP_TestDataUtility.getCurrentOrder(); // method check     
    Test.startTest();
    localObj.setETA(objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(objOrder.EP_Estimated_Time_Range__c,EP_Common_Constant.BLANK);
    System.AssertEquals(objOrder.EP_Planned_Delivery_Date_Time__c,Null);
    // **** TILL HERE ~@~ *****
}
static testMethod void setOrderSyncedWithLS_test() {
    EP_Current localObj = new EP_Current();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Id orderId = EP_TestDataUtility.getCurrentOrder().id;
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Boolean isDeleteFor3rdPartyOrder  = True;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.setOrderSyncedWithLS(orderId,isDeleteFor3rdPartyOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    Order orderObj = [Select EP_Sync_With_LS__c,EP_WinDMS_Status__c from Order where id=:orderId];
    System.AssertEquals(true,orderObj.EP_Sync_With_LS__c);
    System.AssertEquals(EP_Common_Constant.WINDMS_STATUS_DELETE_REQUESTED,orderObj.EP_WinDMS_Status__c);
    // **** TILL HERE ~@~ *****
}
static testMethod void setOrderCreditStatus_test() {
    EP_Current localObj = new EP_Current();     
    Boolean creditLimitCheck =True;     
    Boolean overDueCheck =True;     
    Integer inventoryCheck =EP_Common_Constant.ONE;
    Test.startTest();
    String result = localObj.setOrderCreditStatus(creditLimitCheck,overDueCheck,inventoryCheck);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.assertEquals((EP_Common_Constant.Blank + EP_Common_Constant.SEMICOLON + EP_Common_Constant.OVERDUE_INVOICE) ,result );        
    // **** TILL HERE ~@~ *****
}*/

static testMethod void getShipTos_test() {
    EP_Current localObj = new EP_Current ();
    List<Account> lstAccount = [Select RecordType.DeveloperName from Account where Id=:EP_TestDataUtility.getShipTo().Id];
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_ProductSoldAs productSoldAsType= new EP_ProductSoldAs(); // check 
    Test.startTest();
    LIST<Account> result = localObj.getShipTos(lstAccount, productSoldAsType);
    Test.stopTest();
    System.AssertEquals(1,result.size());
     
}
static testMethod void findRecordType_test() {
    EP_Current localObj = new EP_Current();    
    EP_VendorManagement vmType = new EP_VendorManagement();     
    EP_ConsignmentType consignmentType  = new EP_NonConsignment();
     
    Test.startTest();
    Id result = localObj.findRecordType(vmType,consignmentType);
    Test.stopTest();
     
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null, result);
    // **** TILL HERE ~@~ *****
}
static testMethod void setRequestedDateTime_test() {
    EP_Current localObj = new EP_Current();     
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();    
    String selectedDate ='24-03-2017';     
    String availableSlots ='12:13:99';
    Test.startTest();
    csord__Order__c result = localObj.setRequestedDateTime(orderObj,selectedDate,availableSlots);
    Test.stopTest();
    System.AssertEquals(date.newinstance(2017, 03, 24), orderObj.EP_Requested_Delivery_Date__c);
 }
}