public class EP_ASTSellToBlockedToBlocked extends EP_AccountStateTransition {
    
    public EP_ASTSellToBlockedToBlocked() {
        finalstate = EP_AccountConstant.BLOCKED;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToBlocked','isGuardCondition');
        //No guard conditions for Blocked to Blocked
        return true;
    }

}