@isTest
public class EP_OrderState_UT {
    
    public static EP_OrderState orderState;
    public static String EVENT_NAME = 'Submitted To Accepted';
    public static String WINDMS_STATUS = 'Accepted';
    public static String STATUS = 'Submitted';

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnEntry_Test(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnExit(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();  
        System.assertEquals(true, true);     
    }
    
    static testMethod void doTransition_PositiveTest(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
        
    }
    
    static testMethod void doTransition_NegativeTest(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    
    // test setOrderContext method
    static testmethod void setOrderContext_test(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent orderEvent = new EP_OrderEvent(EVENT_NAME);
        localObj.setOrderContext(orderDomain, orderEvent);
        system.assert((localObj.orderEvent != null && localObj.order !=null), true);  
    }
    
    //test ConvertCustomSettingsToObject method
    static testmethod void convertCustomSettingsToObject_test(){
        orderState = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        orderDomain.setWinDMSStatus(WINDMS_STATUS);
        //orderDomain.setStatus('Submi');       
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        orderState.setOrderContext(orderDomain, oe);
        List<EP_OrderStateTransition> listTransition = orderState.ConvertCustomSettingsToObject('Draft'); 
        system.assert(listTransition.size()> 0);
    }

    //test isInboundTransitionPossible method
    static testmethod void isInboundTransitionPossible_test(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
}