/* 
  @Author <Jai Singh>
   @name <EP_VendorSyncNAVtoSFDC>
   @CreateDate <20/04/2016>
   @Description <This is apex RESTful WebService for Vendor(Transporter and Supplier) Sync from NAV to SFDC> 
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/VendorSync/*')
global with sharing class EP_VendorSyncNAVtoSFDC {
    
    /*
        This is the method that handles HttpPost request for product sync
    */
    @HttpPost
    global static void vendorSync(){
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        string resultstr = EP_VendorSyncHandler.createUpdateVendor(requestBody); 
        RestContext.response.responseBody = Blob.valueOf(resultstr); 
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
    }      
}