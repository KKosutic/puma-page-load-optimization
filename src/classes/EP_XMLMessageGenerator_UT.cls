@isTest
public class EP_XMLMessageGenerator_UT
{    
    public static final string MESSAGE_ID ='xyzi-tlxe-rrut-eeuu'; 
    public static final string MESSAGE_TYPE ='TestMessageType'; 
    public static final string VF_PAGE = 'EP_ShipToSyncWithNAVXML';
    public static final string WRONG_VF_PAGE = 'EP_ShipToSyncWithNAVXML2';
    static testMethod void generateXML_test() {
        Id recordId = EP_TestDataUtility.getSalesOrder().id;    
        String messageId = MESSAGE_ID  ;
        String messageType = MESSAGE_TYPE ;
        String vfPageName = VF_PAGE ;
        String companyCode = EP_Common_Constant.SOURCE_COMPANY ;
        Test.startTest();
        String result = EP_XMLMessageGenerator.generateXML(recordId,messageId,messageType,vfPageName,companyCode);
        Test.stopTest();
        System.AssertEquals(true,result !=null);
    }
    
    /* Need to check how can an exception be raised so that exception part can be covered
    static testMethod void generateXML_WithException() {
        Id recordId = EP_TestDataUtility.getSalesOrder().id;    
        String messageId = MESSAGE_ID  ;
        String messageType = MESSAGE_TYPE ;
        String vfPageName = VF_PAGE ;
        String companyCode = EP_Common_Constant.SOURCE_COMPANY ;
        string exMessage='';
        Test.startTest();
        try {
        	String result = EP_XMLMessageGenerator.generateXML(recordId,messageId,messageType,WRONG_VF_PAGE,companyCode);
        } catch(Exception ex){
        	exMessage = ex.getMessage();
        }
        Test.stopTest();
        //System.AssertEquals(string.valueOf(EP_IntegrationException.EP_IntegrationErrorCode.GENERIC_EXCEPTION),exMessage);
    }*/
}