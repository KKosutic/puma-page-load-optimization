@isTest
public class EP_OrderItemMapper_UT
{
    static testMethod void doDelete_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper();
        Id orderId = EP_TestDataUtility.getSalesOrder().Id;
        List<OrderItem> listRecordDelete = [SELECT Id , orderId FROM OrderItem Where orderId =: orderId];
        Test.startTest();
        localObj.doDelete(listRecordDelete);    
        Test.stopTest();
        List<OrderItem> listOrderItem = [SELECT Id , orderId FROM OrderItem Where orderId =: orderId];
        System.AssertEquals(0,listOrderItem.size());
    }
    static testMethod void getOpenOrderItemsOfTransporters_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper();
        Set<Id> setVendorAccountId = new Set<Id>{EP_TestDataUtility.getVendorPositiveScenario().id};
        Set<String> setOrderStatusStr = new Set<String>{EP_Common_Constant.ORDER_DRAFT_STATUS};
        Test.startTest();
        list<OrderItem> result = localObj.getOpenOrderItemsOfTransporters(setVendorAccountId,setOrderStatusStr);    
        Test.stopTest();
        System.Assert(result.size() == 0);
    }
    static testMethod void getNoParentRecsByOrderIds_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        Set<Id> setOrderId = new Set<Id>{EP_TestDataUtility.getSalesOrder().Id};
        Test.startTest();
        list<OrderItem> result = localObj.getNoParentRecsByOrderIds(setOrderId);    
        Test.stopTest();
        //System.AssertEquals(1,result.size());
    }
    static testMethod void getInvoiceItemsByOrderNumber_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        List<Order> orderObjList = [SELECT Id,OrderNumber FROM Order WHERE Id=:EP_TestDataUtility.getSalesOrderNegativeScenario().Id];
        if(orderObjList.size()>0){
            Order orderObj = orderObjList[0];
            Set<String> setOrderNumber = new Set<String>{orderObj.OrderNumber};
            Test.startTest();
            list<OrderItem> result = localObj.getInvoiceItemsByOrderNumber(setOrderNumber); 
            Test.stopTest();
            System.Assert(result.size() == 0);
        }
    }
    static testMethod void getOrderLineWithInvoiceItem_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        Set<Id> setOrderid = new Set<Id>{EP_TestDataUtility.getSalesOrder().Id};
        Test.startTest();
        list<OrderItem> result = localObj.getOrderLineWithInvoiceItem(setOrderid);  
        Test.stopTest();
        //System.Assert(result.size() > 0);
    }
    static testMethod void getOrderItemsByOrderNumber_test() {
        
        
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        List<Order> orderObjList = [SELECT Id,OrderNumber FROM Order WHERE Id=:EP_TestDataUtility.getSalesOrder().Id];
        if(orderObjList.size()>0){
            Order orderObj = orderObjList[0];
            String orderNumber = orderObj.OrderNumber;
            Test.startTest();
            list<OrderItem> result = localObj.getOrderItemsByOrderNumber(orderNumber);  
            Test.stopTest();
            System.Assert(result.size() > 0);
        }
    }
    static testMethod void getOrderLineItemsforOrder_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper();
        Test.startTest();
        list<OrderItem> result = localObj.getOrderLineItemsforOrder(EP_TestDataUtility.getSalesOrder().Id); 
        Test.stopTest();
        //System.Assert(result.size() > 0);
    }
    static testMethod void testCSOrderById() {
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
        csord__Order__c order = new csord__Order__c();
        order.AccountId__c = sellToAcc.id;
        order.Status__c = 'Draft';
        order.EP_Reason_for_Change__c= 'Other';
        order.EP_Other_Reason__c = 'Other';
        order.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        order.EP_Requested_Delivery_Date__c = system.today()+5; 
        //order.PriceBook2Id__c = pricebookId;
        order.csord__Identification__c = basketInsert.Id;
        order.EffectiveDate__c = system.today();
        Insert order;
        set<Id> ids = new set<Id>();
        ids.add(order.id);
        Test.startTest();
        EP_OrderItemMapper t = new EP_OrderItemMapper();
        list<csord__Order_Line_Item__c> olis = t.getCSOrderById(ids);
        Set<String> ordNumberSet = new Set<String>();
        ordNumberSet.add(order.OrderNumber__c);
        List<csord__Order_Line_Item__c> invItems = t.getCSInvoiceItemsByOrderNumber(ordNumberSet);
        list<OrderItem> invItemsList = t.getInvoiceItemsByOrderNumber(ordNumberSet);
        list<csord__Order_Line_Item__c> oliWithInvItems = t.getCsOrderLineWithInvoiceItem(ids);
        list<OrderItem> oliByOrNum = t.getOrderItemsByOrderNumber(order.OrderNumber__c);
        list<csord__Order_Line_Item__c> csOliByOrNum = t.getCSOrderItemsByOrderNumber(order.OrderNumber__c);
        list<csord__Order_Line_Item__c> csOlis = t.getCSOrderLineItemsforOrder(order.id);
        Test.stopTest();
    }
    
}