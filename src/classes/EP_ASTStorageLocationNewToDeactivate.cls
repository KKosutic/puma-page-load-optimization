/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationNewToDeactivate >
*  @CreateDate <20/12/2017>
*  @Description <Handles Storage Ship To Account status change from New to 06-Deactivate>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationNewToDeactivate extends EP_AccountStateTransition {
	
	/**
    * @author           Accenture
    * @name             EP_ASTStorageLocationNewToDeactivate
    * @date             20/12/2017
    * @description      Constructor
    * @param            NA
    * @return           NA 
    */
    public EP_ASTStorageLocationNewToDeactivate () {
        finalState = EP_AccountConstant.DEACTIVATE;
    }
	
	/**
    * @author           Accenture
    * @name             isTransitionPossible
    * @date             20/12/2017
    * @description      Check wether transitionposoble or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToDeactivate ','isTransitionPossible');
        return super.isTransitionPossible();
    }

	/**
    * @author           Accenture
    * @name             isRegisteredForEvent
    * @date             20/12/2017
    * @description      Check wether Registered For Event or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToDeactivate ', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
	
	/**
    * @author           Accenture
    * @name             isGuardCondition
    * @date             20/12/2017
    * @description      Check all validation
    * @param            NA
    * @return           boolean 
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToDeactivate ',' isGuardCondition');        
        return true;
    }
}