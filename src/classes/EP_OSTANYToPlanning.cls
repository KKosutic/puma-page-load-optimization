/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToPlanning>
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to Planning>
*  @Version <1.0>
*/

public class EP_OSTANYToPlanning extends EP_OrderStateTransition{
      static string classname = 'EP_OSTANYToPlanning';
      Order localorder;
      public EP_OSTANYToPlanning(){
        finalState = EP_OrderConstant.OrderState_Planning;
      }
      
      public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanning','isTransitionPossible');
        return super.isTransitionPossible();
      }
      
      public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanning','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
      }

      public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanning','isGuardCondition');
        //Defect Fix Start #57729 - WINDMS Ackowledgement : When triggred from Integration Record trigger Helper, Integration status is not Sync.
        if(EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(this.order.getOrder().EP_Integration_Status__c)
        	|| EP_OrderConstant.ACCEPTEDACKNOWLEDGED.equalsIgnoreCase(orderEvent.getEventName())) {
          this.order.getOrder().EP_Sync_With_LS__c = true;
          this.order.getOrder().EP_Estimated_Time_Range__c = EP_Common_Constant.BLANK;
          this.order.getOrder().EP_Planned_Delivery_Date_Time__c = Null;
          return true;
        }
        //Defect Fix End #57729
        return false;               
      }
  }