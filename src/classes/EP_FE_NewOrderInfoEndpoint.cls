/* 
  @Author <Nicola Tassini>
   @name <EP_FE_NewOrderInfoEndpoint>
   @CreateDate <20/04/2016>
   @Description <This class displays all basic information needed for creating a new order based on the sellToId  >  
   @Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/order/info/*')
global with sharing class EP_FE_NewOrderInfoEndpoint {

    // Parameters keys
    global final static String PARAM_KEY_SELLTO_ID = 'sellToId';
    global final static String PARAM_KEY_ORDER_ID = 'orderId';
    global final static String PARAM_DELIVERY_TYPE = 'deliveryType';
    global final static String PARAM_EDIT_ORDER = 'editOrder';
    global final static String LAST_ORDER = 'last';

    /*
     * API Call
     */
    @HttpGet
    webservice static EP_FE_NewOrderInfoResponse getOrderInfo() {
        // Prepare the output
        EP_FE_NewOrderInfoResponse response = new EP_FE_NewOrderInfoResponse();
        /*
        // Get the parameters
        Map < String, Object > params = RestContext.request.params;
        String sellToIdStr = params.containsKey(PARAM_KEY_SELLTO_ID) ? String.valueOf(params.get(PARAM_KEY_SELLTO_ID)) : null;
        String orderIdStr = params.containsKey(PARAM_KEY_ORDER_ID) ? String.valueOf(params.get(PARAM_KEY_ORDER_ID)) : null;
        String deliveryType = params.containsKey(PARAM_DELIVERY_TYPE) ? String.valueOf(params.get(PARAM_DELIVERY_TYPE)) : null;
        Boolean isOrderEdit = params.containsKey(PARAM_EDIT_ORDER);
        //Integer ordCount = -1;
        Order odr;
        Id orderId = null;
        
        // Check OrderId
        if(!string.isBlank(orderIdStr)){
            if (orderIdStr.equals(LAST_ORDER)){
                odr = getLastCreatedOrder(sellToIdStr);
                if (odr != null){
                    orderId = odr.Id;
                    populateResponseWithOrderData(response, odr, isOrderEdit);
                }
                //else{
                  //  EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_NewOrderInfoResponse.CLASSNAME, 
                    //            EP_FE_NewOrderInfoResponse.METHOD1, EP_FE_NewOrderInfoResponse.SEVERITY, 
                      //          EP_FE_NewOrderInfoResponse.TRANSACTION_ID, EP_FE_NewOrderInfoResponse.DESCRIPTION1),
                        //        response,EP_FE_NewOrderInfoResponse.ERROR_MISSING_SELLTO);
                //}
            } else {
                orderId = Id.valueOf(orderIdStr);
                odr = fetchOrderDetails(orderId);              
                populateResponseWithOrderData(response, odr, isOrderEdit);
            }
            
            if (isOrderEdit && odr != null){
                sellToIdStr = (odr.EP_Sell_To__c != null) ? odr.EP_Sell_To__c : odr.AccountId;
            }
            
            //if ((deliveryType == null || deliveryType == '') && odr != null){
            if (String.isBlank(deliveryType) && odr != null){
                deliveryType = odr.EP_Order_Delivery_Type__c;
            }
        }
        // Validate the input parameters    
          if (sellToIdStr == null || EP_FE_Utils.isUserProfileAccessEquals(EP_FE_Constants.PROFILE_BASIC_ACCESS)) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_NewOrderInfoResponse.CLASSNAME, 
                                EP_FE_NewOrderInfoResponse.METHOD1, EP_FE_NewOrderInfoResponse.SEVERITY, 
                                EP_FE_NewOrderInfoResponse.TRANSACTION_ID, EP_FE_NewOrderInfoResponse.DESCRIPTION1),
                                response,EP_FE_NewOrderInfoResponse.ERROR_MISSING_SELLTO);
          } else {
            // Convert the sell to parameter
              Id sellToId = null; 
              try {
                sellToId = Id.valueOf(sellToIdStr);
              } catch (Exception e) {
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_NewOrderInfoResponse.CLASSNAME, 
                                EP_FE_NewOrderInfoResponse.METHOD1, EP_FE_NewOrderInfoResponse.SEVERITY, 
                                EP_FE_NewOrderInfoResponse.TRANSACTION_ID, EP_FE_NewOrderInfoResponse.DESCRIPTION1),
                                response,EP_FE_NewOrderInfoResponse.ERROR_MISSING_SELLTO);
              }
            // Parameter validated
            if (sellToId != null) {
                // Get the current billto / sellto
                Account sellTo = EP_FE_DataService.getSellTo(sellToId);
                Map<Id, List<EP_FE_NewOrderInfoResponse.EP_FE_OrderItem>> preOrderItemsMap = new Map<Id, List<EP_FE_NewOrderInfoResponse.EP_FE_OrderItem>>();
                if(odr != null){
                  //validate the ReOrder parameters
                  if(isOrderEdit || validateReOrderParameters(sellTo, orderId, response)){
                    // Get OrderLine Item Details
                    preOrderItemsMap = getOrderItems(response, orderId);
                  }
                }    
                if (sellTo != null) {
                    //TODO: check that user can make this delivery type specified in get params
                    response.deliveryType = (deliveryType == null || deliveryType == '') ? sellTo.EP_Delivery_Type__c : deliveryType;
                    response.currencyCode = sellTo.CurrencyISOCode;

                    //Get information about fields visibility
                    fetchFieldsVisivilityData(sellTo, response);


                    // Delivery sell-to: fetch the ship-tos
                    if (EP_Common_Constant.ORDER_DELIVERY_TYPE_IS_DELIVERY.equalsIgnoreCase(response.deliveryType)) {
                        populateDeliveryTypeResponse(response, sellTo, preOrderItemsMap);
                    }

                    // Delivery ex-rack: fetch the terminals
                    else{ 
                        if (EP_Common_Constant.EX_RACK.equalsIgnoreCase(response.deliveryType)) {
                        populateExRackTypeResponse(response, sellTo, preOrderItemsMap);
                        }
                    }
                    // Sell to not found
                    
                    if (isOrderEdit && odr != null) {
                        populateEditingOrderPrices(orderId, response);
                    }
                } else {
                    EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_NewOrderInfoResponse.CLASSNAME, 
                                EP_FE_NewOrderInfoResponse.METHOD1, EP_FE_NewOrderInfoResponse.SEVERITY, 
                                EP_FE_NewOrderInfoResponse.TRANSACTION_ID, EP_FE_NewOrderInfoResponse.DESCRIPTION3),
                                response,EP_FE_NewOrderInfoResponse.ERROR_SELLTO_NOT_FOUND);
                }
                // Sell to id is null
            } else {
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_NewOrderInfoResponse.CLASSNAME, 
                                EP_FE_NewOrderInfoResponse.METHOD1, EP_FE_NewOrderInfoResponse.SEVERITY, 
                                EP_FE_NewOrderInfoResponse.TRANSACTION_ID, EP_FE_NewOrderInfoResponse.DESCRIPTION2),
                                response,EP_FE_NewOrderInfoResponse.ERROR_SELLTOID_NOT_FOUND);
            }
        }*/
        return response;
    }
}