/* 
   @Author 			Accenture
   @name 			EP_InboundHandler
   @CreateDate 		03/10/2017
   @Description		Inbound handler for all inbound interfaces
   @Version 		1.0
*/
public class EP_InboundFactory {
	
	/**
    * @Author       Accenture
    * @Name         getInboundHandler
    * @Date         02/07/2017
    * @Description  This method will be used to get Inbound Handler Class Name from Inbound Message Custom setting
    * @Param        messageType
    * @return       EP_InboundHandler
    */
    public EP_InboundHandler getInboundHandler(string messageType) {
        EP_GeneralUtility.Log('Public','EP_InboundFactory','getInboundHandler');

        EP_InboundHandler  inboundHanlder;
        EP_CS_InboundMessageSetting__c msgSetting = EP_CS_InboundMessageSetting__c.getValues(messageType);
        inboundHanlder = (EP_InboundHandler)Type.forName(msgSetting.Handler_Name__c).newInstance();
        return inboundHanlder;
    }
    
    /**
    * @Author       Accenture
    * @Name         getResponseHandler
    * @Date         02/07/2017
    * @Description  This method will be used to get Response Handler Class Name from Outbound Message Custom setting
    * @Param        messageType
    * @return       EP_InboundHandler
    */
    //#60147 User Story Start
    public EP_InboundHandler getResponseHandler(string messageType) {
        EP_GeneralUtility.Log('Public','EP_InboundFactory','getResponseHandler');
        EP_InboundHandler inboundHanlder;
    	EP_CS_OutboundMessageSetting__c msgSetting = EP_CS_OutboundMessageSetting__c.getValues(messageType);
    	if(string.isNotBlank(msgSetting.Response_Handler_Name__c)) {
    		inboundHanlder = (EP_InboundHandler)Type.forName(msgSetting.Response_Handler_Name__c).newInstance();
    	}
        return inboundHanlder;
    }
    //#60147 User Story End
}