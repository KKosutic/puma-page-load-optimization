/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateDraft.cls>     
     @Description <Order State for draft status, common for all order types. create seperate classes if the logic differs based on order type>   
     @Version <1.1> 
     */

     public class EP_OrderStateDraft extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDraft','doTransition');
            return super.doTransition();
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDraft','doOnEntry');
            
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDraft','doOnExit');
            
        }
        
        public override boolean isInboundTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDraft','isInboundTransitionPossible');
            //Override this with all possible guard conditions that permits the state transition possible
            //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
            return super.isInboundTransitionPossible();
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Draft;
        }
    }