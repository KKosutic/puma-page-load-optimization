@isTest
public class EP_SellToASBlocked_UT{
	
	static final string EVENT_NAME = '06-BlockedTo06-Blocked';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
	
	@testSetup static void init() {
      	List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      	List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
		List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }

	static testMethod void setAccountDomainObject_test() {
		EP_SellToASBlocked localObj = new EP_SellToASBlocked();
		EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBlockedDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.setAccountDomainObject(obj);
		Test.stopTest();
		 System.AssertEquals(obj,localObj.account);
	}
	// 45361 and 45362 start
	static testMethod void doOnEntry_test() {
		EP_SellToASBlocked localObj = new EP_SellToASBlocked();
		Account newAccount = EP_TestDataUtility.getSellToASBlockedDomainObject().getAccount();
		Account oldAccount = newAccount.clone();
		oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
		EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount,oldAccount);
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnEntry();
		Test.stopTest();
		List<EP_IntegrationRecord__c> integrationRecordLst = [select id,EP_Error_Description__c,EP_Status__c,EP_SeqId__c,EP_Object_Type__c,EP_Object_ID__c 
                                                    from EP_IntegrationRecord__c where EP_Object_ID__c =: String.valueOf(obj.getAccount().id)];
        System.assertEquals(true,integrationRecordLst != null);
	}
	// 45361 and 45362 end
	static testMethod void doOnExit_test() {
		EP_SellToASBlocked localObj = new EP_SellToASBlocked();
		EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBlockedDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnExit();
		Test.stopTest();
		//assert not needed
		system.assert(true);
	}
	static testMethod void doTransition_PositiveScenariotest() {
		EP_SellToASBlocked localObj = new EP_SellToASBlocked();
		EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBlockedDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.doTransition();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
		EP_SellToASBlocked localObj = new EP_SellToASBlocked();
		EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBlockedDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.isInboundTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
}