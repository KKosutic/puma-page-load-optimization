/* 
@Author      Accenture
@name        EP_DateTimeUtility
@CreateDate  14/06/2017
@Description This class will be used to perform operations on date and datetime values. This class is created as a part of requirment #59187
@Version     1.0
*/
public class EP_DateTimeUtility {  
	
	public static final string DATEFORMAT = 'DateFormat';  
	public static final string DATETIMEFORMAT = 'DatetimeFormat'; 
    
    private static string getformat(string value){
        EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(value);
        return communicationSettings.value__c;
    }
    
    /**
    * @author       Accenture
    * @name         formatDateAsString
    * @date         14/06/2017
    * @description  Method to convert date in specific format
    * @param        date
    * @return       string
    */ 
    public static string formatDateAsString(date inputDate){      
        String dt = EP_Common_Constant.BLANK;
        if(inputdate != null){
            string dateFormat = getformat(DATEFORMAT);
            dt = Datetime.newInstance(inputDate.year(),inputDate.month(),inputDate.day()).format(dateFormat);
        }
        return dt;
    }
    
    /**
    * @author       Accenture
    * @name         formatDateAsString
    * @date         14/06/2017
    * @description  Method to convert datetime in specific format
    * @param        datetime
    * @return       string
    */  
    public static string formatDateAsString(datetime inputDatetime){        
        String dt = EP_Common_Constant.BLANK;
        if(inputDatetime != null){                  
            string dateTimeFormat = getformat(DATETIMEFORMAT);           
            dt = DateTime.newInstance(inputDatetime.year(),inputDatetime.month(),inputDatetime.day(),inputDatetime.hour(),inputDatetime.minute(),inputDatetime.second()).formatGMT(dateTimeFormat);
        }           
        return dt;
    }
    
    /**
    * @author       Accenture
    * @name         formatDateAsString
    * @date         14/06/2017
    * @description  Method to convert string datetime in specific format
    * @param        string
    * @return       string
    */  
    public static string formatDateAsString(string datetimeStr){
        DateTime dt;
        String dtstring = EP_Common_Constant.BLANK;
        if(!string.isBlank(datetimeStr)){
            dt  = datetime.valueOfGMT(datetimeStr);
            dtstring = formatDateAsString(dt);
        }
        return dtstring;
    }

    /**
    * @author       Accenture
    * @name         convertStringToDate
    * @date         14/06/2017
    * @description  Method to convert string to date
    * @param        string
    * @return       date
    */
    public static date convertStringToDate(string dateStr){
        Date dt;
        if(!string.isBlank(dateStr)){
            dt = Date.valueOf(dateStr);
        }
        return dt;
    }
}