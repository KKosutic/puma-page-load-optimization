/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationInActiveToInActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 07-Inactive to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationInActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationInActiveToInActive () {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToInActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToInActive',' isGuardCondition');        
        return true;
    }
}