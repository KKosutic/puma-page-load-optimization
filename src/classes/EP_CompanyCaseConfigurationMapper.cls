/* 
@Author      Accenture
@name        EP_CompanyCaseConfigurationMapper
@CreateDate  04/1/2018
@Description This class contains all SOQLs related to Company case configuration Object 
@Version     1.0
*/
public with sharing class EP_CompanyCaseConfigurationMapper{

    /** This method returns Company case Config Records
    *  @name            getRecordsByIds
    *  @param           id
    *  @return          Company case configuration entry
    *  @throws          NA
    */
    public static EP_Company_Case_configuration__c FetchRecord(id companyId, string type, string subType) {
        EP_Company_Case_configuration__c objCompanyCasConf ;
        if(string.isNotBlank(companyId) && string.isNotBlank(type) && string.isNotBlank(subType)){
            list<EP_Company_Case_configuration__c > lstCompanyCasConf = [select id,EP_Company__c ,EP_SLA_days__c,EP_SLA_hours__c,EP_Company__r.EP_Business_Hours__c from EP_Company_Case_configuration__c
                                                                          where EP_Company__c=:companyId and EP_Category__c=: type and EP_Sub_Category__c=:subType ];
            objCompanyCasConf= !lstCompanyCasConf .isEmpty() ?lstCompanyCasConf [0] :null;
        }
        return objCompanyCasConf;
    }
}