@isTest
public class EP_OrderLinesAndSummaryHelper_UT
{

    static testMethod void loadStep3_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        PageReference result = localObj.loadStep3();
        Test.stopTest();
        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        System.AssertEquals(ctx.newOrderRecord.EP_Supply_Location_Name__c,selectedPickupDetail.Stock_Holding_Location__r.Name);
    }
    static testMethod void populatesupplierOptions_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.populatesupplierOptions();
        Test.stopTest();
        System.AssertEquals(true, ctx.supplierOptions.size()>0); 
    }
    static testMethod void setOrderRecordType_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.setOrderRecordType();
        Test.stopTest();
        System.AssertEquals(true,ctx.newOrderRecord.RecordTypeId<>Null);
    }
    static testMethod void checkForNoTransporter_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        String transporterName = [Select EP_Transporter__r.Name from EP_Stock_Holding_Location__c where Id=: ord.Stock_Holding_Location__c].EP_Transporter__r.Name;
        Test.startTest();
        csord__Order__c result = localObj.checkForNoTransporter(ord,transporterName);
        Test.stopTest();
        System.AssertEquals('Pipeline',ctx.strSelecteddeliveryType);
    }
    static testMethod void SetPickupDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.SetPickupDetails();
        Test.stopTest();
        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        System.AssertEquals(ctx.newOrderRecord.EP_Supply_Location_Name__c,selectedPickupDetail.Stock_Holding_Location__r.Name);
    }
    static testMethod void setRunAndRoute_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        String shipToId = ord.EP_ShipTo__c;
        String stockholdinglocationId = ord.Stock_Holding_Location__c;
        Test.startTest();
        LIST<SelectOption> result = localObj.setRunAndRoute(shipToId,stockholdinglocationId);
        Test.stopTest();
        System.AssertEquals(true,result.size()>0);
    }
    static testMethod void getAllAvailableDates_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_Stock_Holding_Location__c selectedPickupDetail = [Select Stock_Holding_Location__r.EP_Business_Hours__c,EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c,Stock_Holding_Location__r.EP_Slotting_Enabled__c from EP_Stock_Holding_Location__c 
        														where Id=: ord.Stock_Holding_Location__c];
        Test.startTest();
        localObj.getAllAvailableDates(selectedPickupDetail);
        Test.stopTest();
        // No assertions added as this method is calling another method.
        system.assert(true);
    }
    static testMethod void filterTerminalDateTime_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_Stock_Holding_Location__c selectedPickupDetail = [Select Stock_Holding_Location__r.EP_Business_Hours__c,EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c,Stock_Holding_Location__r.EP_Slotting_Enabled__c from EP_Stock_Holding_Location__c 
        														where Id=: ord.Stock_Holding_Location__c];
        EP_FE_NewOrderInfoResponse feResponse = new EP_FE_NewOrderInfoResponse();
        EP_PortalOrderUtil.getAvailableDates(feResponse,selectedPickupDetail);		
        EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo terminalInfo = feResponse.terminalInfoList[0]; 												
    	LIST<EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime> availableDateTimeList = terminalInfo.availableDateTimeList;
        Test.startTest();
        localObj.filterTerminalDateTime(availableDateTimeList,selectedPickupDetail);
        Test.stopTest();
        System.AssertEquals(true,ctx.availableDatesList.size()>0);
    }
    static testMethod void setTimeframeForOpenDate_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.timeslots = new  Map< String,List<SelectOption> >();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_Stock_Holding_Location__c selectedPickupDetail = [Select Stock_Holding_Location__r.EP_Business_Hours__c,EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c,Stock_Holding_Location__r.EP_Slotting_Enabled__c from EP_Stock_Holding_Location__c 
        														where Id=: ord.Stock_Holding_Location__c];
        EP_FE_NewOrderInfoResponse feResponse = new EP_FE_NewOrderInfoResponse();
        EP_PortalOrderUtil.getAvailableDates(feResponse,selectedPickupDetail);		
        EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo terminalInfo = feResponse.terminalInfoList[0]; 												
    	LIST<EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime> availableDateTimeList = terminalInfo.availableDateTimeList;
    	EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpen = availableDateTimeList[0];
        Test.startTest();
        localObj.setTimeframeForOpenDate(dateOpen);
        Test.stopTest();
        System.AssertEquals(true,ctx.timeSlots.size() > 0);
    }
    static testMethod void prepareProductDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.prepareProductDetails();
        Test.stopTest();
        System.AssertEquals(true,ctx.strLoadedPickupLocationID == ctx.strSelectedPickupLocationID);
        System.AssertEquals(true,ctx.strLoadedDeliveryType == ctx.strSelectedDeliveryType);
    }
    static testMethod void setProductDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.setProductDetails();
        Test.stopTest();
        System.AssertEquals(true,!ctx.productPriceMap.isEmpty());
    }
    static testMethod void getAvailableProducts_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_Stock_Holding_Location__c stockHoldingLocationObj = [Select id,Stock_Holding_Location__c from EP_Stock_Holding_Location__c where Id=: ord.Stock_Holding_Location__c];
        ctx.newOrderRecord = ord;
        ctx.mapOfSupplyLocation = new Map < String, EP_Stock_Holding_Location__c >();
        ctx.mapOfSupplyLocation.put(stockHoldingLocationObj.Id,stockHoldingLocationObj);
        Test.startTest();
        Set<Id> result = localObj.getAvailableProducts(stockHoldingLocationObj.Id);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void setQuantityRestrictions_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_Order_Configuration__c orderConfigObj = EP_TestDataUtility.getOrderConfiguration();
        ctx.productRangeMap.put(orderConfigObj.EP_Product__c+orderConfigObj.EP_Volume_UOM__c, orderConfigObj.EP_Min_Quantity__c + EP_Common_Constant.HYPHEN_STRING + orderConfigObj.EP_Max_Quantity__c);
        Test.startTest();
        localObj.setQuantityRestrictions();
        Test.stopTest();
        System.AssertEquals(true,ctx.productRangeMap.size() > 0);
    }
    static testMethod void addNewLine_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        localObj.addNewLine();
        Test.stopTest();
        System.AssertEquals(true,ctx.listofOrderWrapper.size() > 0);
    }
    static testMethod void getProductOptions_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        ctx.itemIdTotalPriceMap = new Map<id,Double>();
        ctx.itemIdTotalPriceMap.put(orderlineitem.id,111.00);
        ctx.loadExistingOrderItems();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        PriceBookEntry pbEntry = [Select id,product2Id,product2.Name,EP_Unit_Of_Measure__c,unitprice from PriceBookEntry limit 1];
        ctx.PriceBookEntryproductMap.put(pbEntry.Product2Id, pbEntry.Id); 
        ctx.productPriceMap.put(pbEntry.Id,pbEntry.unitprice);
        ctx.productPriceBookEntryMap.put(pbEntry.Id,pbEntry.Product2Id);
        ctx.productUnitOfMeasureMap.put(pbEntry.Product2Id,pbEntry.EP_Unit_Of_Measure__c);
        ctx.productNameMap.put(pbEntry.Id,pbEntry.product2.Name);
        EP_Order_Configuration__c orderConfigObj = EP_TestDataUtility.getOrderConfiguration();
        ctx.productRangeMap.put(EP_PortalOrderUtil.constructProductQuantityLimitKey(pbEntry.product2Id, pbEntry.EP_Unit_Of_Measure__c), orderConfigObj.EP_Min_Quantity__c + EP_Common_Constant.HYPHEN_STRING + orderConfigObj.EP_Max_Quantity__c);
        Test.startTest();
        LIST<SelectOption> result = localObj.getProductOptions();
        Test.stopTest();
        System.AssertEquals(true,result.size() >1 );
    }
    static testMethod void getListShipToTanks_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.setshiptodetailsonOrder();
        Test.startTest();
        LIST<SelectOption> result = localObj.getListShipToTanks();
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void loadStep4_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.strSelectedRoute = ord.EP_Route__c;
        Test.startTest();
        PageReference result = localObj.loadStep4();
        Test.stopTest();
        //Test Class Fix Start
        //System.AssertEquals(true,ctx.newOrderRecord.EP_Route__c <> Null);
        System.AssertEquals(true,result == Null);
    }
    static testMethod void IsValidOrder_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        Boolean result = localObj.IsValidOrder();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void IsValidOrder_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getRetrospectiveNegativeScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.newOrderRecord = ord;
        ctx.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
        ctx.showCustomerPoField = true;
        ctx.isConsumptionOrder = true;
        ctx.isDummy = false;
        Account sellTo = [Select EP_Is_Customer_Reference_Mandatory__c from Account where Id =: ord.AccountId__c];
        sellTo.EP_Is_Customer_Reference_Mandatory__c = true;
        sellTo.EP_Customer_PO_Number__c = '83489';
        sellTo.EP_Valid_From_date__c = System.Today();
        sellTo.EP_Valid_To_date__c = System.Today()+10;
        update sellTo; 
        ctx.orderAccount = sellTo;
        Test.startTest();
        Boolean result = localObj.IsValidOrder();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void updateOrderItemLinesfornewOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        csord__Order_Line_Item__c orderItemObj = ord.csord__Order_Line_Items__r[0];
        orderItemObj.EP_BOL_Number__c = '735745';
        update orderItemObj;
        
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.newOrderRecord = ord;
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        ctx.itemIdTotalPriceMap = new Map<id,Double>();
        ctx.itemIdTotalPriceMap.put(orderlineitem.id,111.00);
        ctx.loadExistingOrderItems();
        Test.startTest();
        localObj.updateOrderItemLinesfornewOrder();
        Test.stopTest();
        System.AssertEquals(true,ctx.listofOrderWrapper.size() >0);
    }
    static testMethod void isValidOrderLineItem_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getRetrospectiveNegativeScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        orderlineitem.EP_BOL_Number__c = EP_Common_Constant.Blank;
        ctx.isConsumptionOrder = false;
        Test.startTest();
        Boolean result = localObj.isValidOrderLineItem(orderlineitem);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isValidOrderLineItem_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        orderlineitem.EP_BOL_Number__c = '735745';
        update orderlineitem;
        Test.startTest();
        Boolean result = localObj.isValidOrderLineItem(orderlineitem);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void ValidateInventory_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        ctx.newOrderRecord = ord;
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        ctx.itemIdTotalPriceMap = new Map<id,Double>();
        ctx.itemIdTotalPriceMap.put(orderlineitem.id,111.00);
        ctx.loadExistingOrderItems();
        LIST<EP_OrderPageContext.OrderWrapper> listofOrderWrapper = ctx.listofOrderWrapper;
        PriceBookEntry pbEntry = [Select id,product2Id,product2.Name,EP_Unit_Of_Measure__c,unitprice from PriceBookEntry limit 1];
        ctx.productPriceBookEntryMap.put(pbEntry.Id,pbEntry.Product2Id);
        listofOrderWrapper[0].oliPricebookEntryID = pbEntry.Id;
        listofOrderWrapper[0].orderLineItem = ord.csord__Order_Line_Items__r[0];
        //OrderItem orderItemObj = new OrderItem();
        ctx.productInventoryMap.put(pbEntry.Product2Id,EP_Common_Constant.GOOD_INV);
        Test.startTest();
        localObj.ValidateInventory(listofOrderWrapper,orderlineitem);
        Test.stopTest();
        //System.AssertEquals(true,orderItemObj.EP_Inventory_Availability__c == EP_Common_Constant.GOOD_INV);
        List<Apexpages.Message> msgs = ApexPages.getMessages();
    	boolean b = false;
    	for(Apexpages.Message msg:msgs){
    	    //if (msg.getDetail().contains('Search requires more characters')) b = true;
    	    system.debug('**MESSAGE** ' + msg);
    	}
    	//system.assert(b);
    }
    static testMethod void setOrderLineitemsForUpdate_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper newOrderWrapperobj = new EP_OrderPageContext.OrderWrapper();
        
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        orderWrapperObj.orderLineItem = ord.csord__Order_Line_Items__r[0];
        orderWrapperObj.oliQuantity = 123;
        LIST<csord__Order_Line_Item__c> listItemsToDelete = new List<csord__Order_Line_Item__c>();
        LIST<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new LIST<EP_OrderPageContext.OrderWrapper>();
        LIST<csord__Order_Line_Item__c> listValidatedOrderLines = new LIST<csord__Order_Line_Item__c>();
        csord__Order_Line_Item__c orderItemObj = orderWrapperObj.orderLineItem;
        Test.startTest();
        localObj.setOrderLineitemsForUpdate(newOrderWrapperobj,orderItemObj,orderWrapperObj,listItemsToDelete,updatedOrderWrapper,listValidatedOrderLines);
        Test.stopTest();
        system.debug('----'+orderItemObj+'----orderWrapperObj'+orderWrapperObj);
        System.AssertEquals(true, orderItemObj.Quantity__c == orderWrapperObj.oliQuantity);
    }
    static testMethod void setProductQuantity_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        PriceBookEntry pbEntry = [Select id,product2Id,unitprice,EP_Unit_Of_Measure__c from PriceBookEntry limit 1];
        ctx.productUnitOfMeasureMap.put(pbEntry.Id,pbEntry.EP_Unit_Of_Measure__c);
        orderWrapperObj.oliPricebookEntryID = pbEntry.Id;
        Test.startTest();
        localObj.setProductQuantity(orderItemObj,orderWrapperObj);
        Test.stopTest();
        System.AssertEquals(true,orderItemObj.EP_Quantity_UOM__c == pbEntry.EP_Unit_Of_Measure__c);
        System.AssertEquals(true,orderWrapperObj.orderLineItem.EP_Quantity_UOM__c == pbEntry.EP_Unit_Of_Measure__c);
        System.AssertEquals(true,orderWrapperObj.oliProductUoM == pbEntry.EP_Unit_Of_Measure__c);
    }
    static testMethod void setOrderLineItemValues_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [Select EP_Capacity__c,EP_Safe_Fill_Level__c,EP_Product__r.Name,EP_Product__c,EP_Tank_Code__c,EP_Tank_Alias__c from EP_Tank__c where EP_Ship_To__c =: ord.EP_ShipTo__c limit 1];
        PriceBookEntry pbEntry = [Select id,product2Id,unitprice from PriceBookEntry where product2Id =: tankObj.EP_Product__c limit 1];
        ctx.PriceBookEntryproductMap.put(pbEntry.Product2Id, pbEntry.Id); 
        ctx.productPriceMap.put(pbEntry.Id,pbEntry.unitprice);
        ctx.mapShipToOperationalTanks.put(tankObj.Id,tankObj);
        orderWrapperObj.oliTanksID = tankObj.Id;
        orderWrapperObj.oliQuantity = Integer.valueOf(tankObj.EP_Capacity__c+10);
        orderWrapperObj.oliIndex = 4;
        Test.startTest();
        localObj.setOrderLineItemValues(orderItemObj,orderWrapperObj);
        Test.stopTest();
        System.AssertEquals(true,orderItemObj.UnitPrice__c == pbEntry.UnitPrice);
        System.AssertEquals(true,orderItemObj.Quantity__c == orderWrapperObj.oliQuantity);
        System.AssertEquals(true,orderItemObj.EP_Is_Standard__c);
        System.AssertEquals(true,orderItemObj.EP_Order_Line_Relationship_Index__c == orderWrapperObj.oliIndex);
        
    }
    static testMethod void setTankDetailsofOrderWrapper_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [Select EP_Capacity__c,EP_Safe_Fill_Level__c,EP_Product__r.Name,EP_Product__c,EP_Tank_Code__c,EP_Tank_Alias__c from EP_Tank__c where EP_Ship_To__c =: ord.EP_ShipTo__c limit 1];
        PriceBookEntry pbEntry = [Select id,product2Id from PriceBookEntry where product2Id =: tankObj.EP_Product__c limit 1];
        ctx.PriceBookEntryproductMap.put(pbEntry.Product2Id, pbEntry.Id); 
        ctx.mapShipToOperationalTanks.put(tankObj.Id,tankObj);
        orderWrapper.oliTanksID = tankObj.Id;
        orderWrapper.oliQuantity = Integer.valueOf(tankObj.EP_Capacity__c+10);
        Test.startTest();
        localObj.setTankDetailsofOrderWrapper(orderWrapper);
        Test.stopTest();
        System.AssertEquals(true,orderWrapper.oliPricebookEntryID == pbEntry.Id);
        System.AssertEquals(true,orderWrapper.orderLineItem.EP_Tank__c == orderWrapper.oliTanksID);
        System.AssertEquals(true,orderWrapper.oliTankProductName == tankObj.EP_Product__r.Name);
        System.AssertEquals(true,orderWrapper.oliTankSafeFillLvl == tankObj.EP_Safe_Fill_Level__c);
        //Test Class Fix Start
        //System.AssertEquals(true,orderWrapper.oliTanksName == tankObj.EP_Tank_Code__c+EP_Common_Constant.SLASH+tankobj.EP_Tank_Alias__c);
    }
    static testMethod void setDeleteListAndUpdatedOrderWrapper_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        csord__Order_Line_Item__c orderlineitem = ord.csord__Order_Line_Items__r[0];
        ctx.itemIdTotalPriceMap = new Map<id,Double>();
        ctx.itemIdTotalPriceMap.put(orderlineitem.id,111.00);
        ctx.loadExistingOrderItems();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderItemObj = ord.csord__Order_Line_Items__r[0];
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [Select EP_Capacity__c,EP_Safe_Fill_Level__c,EP_Product__r.Name,EP_Product__c,EP_Tank_Code__c,EP_Tank_Alias__c from EP_Tank__c where EP_Ship_To__c =: ord.EP_ShipTo__c limit 1];
        PriceBookEntry pbEntry = [Select id,product2Id from PriceBookEntry where product2Id =: tankObj.EP_Product__c limit 1];
        orderWrapperObj.oliPricebookEntryID = pbEntry.Id;
        orderWrapperObj.oliQuantity = 10;
        LIST<csord__Order_Line_Item__c> listValidatedOrderLines = new LIST<csord__Order_Line_Item__c>();
        LIST<csord__Order_Line_Item__c> listItemsToDelete;
        LIST<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new LIST<EP_OrderPageContext.OrderWrapper>();
        Test.startTest();
        localObj.setDeleteListAndUpdatedOrderWrapper(orderWrapper,orderItemObj,orderWrapperObj,listItemsToDelete,updatedOrderWrapper);
        Test.stopTest();
        System.AssertEquals(true,updatedOrderWrapper.size() > 0);
    }
    static testMethod void setOrderWrapperAndValidateOrderLines_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        csord__Order_Line_Item__c orderItemObj = ord.csord__Order_Line_Items__r[0];
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        PriceBookEntry pbEntry = [Select id,product2Id from PriceBookEntry limit 1];
        EP_Tank__c tankObj = [Select EP_Capacity__c from EP_Tank__c limit 1];
        orderWrapperObj.oliPricebookEntryID = pbEntry.Id;
        orderWrapperObj.oliQuantity = 10;
        LIST<csord__Order_Line_Item__c> listValidatedOrderLines = new LIST<csord__Order_Line_Item__c>();
        Test.startTest();
        localObj.setOrderWrapperAndValidateOrderLines(orderItemObj,orderWrapper,orderWrapperObj,listValidatedOrderLines);
        Test.stopTest();
        System.AssertEquals(true,listValidatedOrderLines.size()>0);
    }
    static testMethod void validateEnteredQuantity_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        orderWrapper.oliQuantity = 0;
        Test.startTest();
        localObj.validateEnteredQuantity(orderWrapper);
        Test.stopTest();
        System.AssertEquals(true, orderWrapper.oliQuantityError == EP_Common_Constant.ERROR);
    }
    static testMethod void isQuantiyHigherthanSafefill_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [Select EP_Capacity__c,EP_Safe_Fill_Level__c from EP_Tank__c limit 1];
        ctx.mapShipToOperationalTanks.put(tankObj.Id,tankObj);
        orderWrapper.oliTanksID = tankObj.Id;
        orderWrapper.oliQuantity = Integer.valueOf(tankObj.EP_Safe_Fill_Level__c+10);
        ctx.isOprtnlTankAvail = true;
        Test.startTest();
        Boolean result = localObj.isQuantiyHigherthanSafefill(orderWrapper);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isQuantiyHigherthanSafefill_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        EP_Tank__c tankObj = [Select EP_Capacity__c,EP_Safe_Fill_Level__c from EP_Tank__c limit 1];
        ctx.mapShipToOperationalTanks.put(tankObj.Id,tankObj);
        orderWrapper.oliTanksID = tankObj.Id;
        orderWrapper.oliQuantity = Integer.valueOf(tankObj.EP_Safe_Fill_Level__c-10);
        ctx.isOprtnlTankAvail = true;
        Test.startTest();
        Boolean result = localObj.isQuantiyHigherthanSafefill(orderWrapper);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setQuantityError_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        Test.startTest();
        localObj.setQuantityError(orderWrapper);
        Test.stopTest();
        System.AssertEquals(true,orderWrapper.oliQuantityError == EP_Common_Constant.ERROR);
    }
    static testMethod void loadStep3ForRetro_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        hlp.setShipToDetails();
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        Test.startTest();
        PageReference result = localObj.loadStep3();
        Test.stopTest();
        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        //System.AssertEquals(ctx.newOrderRecord.EP_Supply_Location_Name__c,selectedPickupDetail.Stock_Holding_Location__r.Name);
    }
    static testMethod void SetPickupDetails1_test() {
        csord__Order__c ord = EP_TestDataUtility.getExRackOrder();
        EP_TestDataUtility.createSellToAccountAndItsSupplyLocation();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.loadAccountOrderDetails();
        EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx);
        EP_OrderModificationControllerHelper modhlp = new EP_OrderModificationControllerHelper(ctx);    
        EP_OrderLinesAndSummaryHelper localObj = new EP_OrderLinesAndSummaryHelper(ctx);
        //Test Class Fix Start
        List<EP_Stock_Holding_Location__c> stockLocationsSellToList = [ Select Id,Stock_Holding_Location__c,Stock_Holding_Location__r.Id,EP_Transporter__c from EP_Stock_Holding_Location__c];
        ctx.strSelectedPickupLocationID = stockLocationsSellToList.get(0).id;
        //Test Class Fix end
        Test.startTest();
        localObj.SetPickupDetails();
        Test.stopTest();

        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        System.AssertEquals(ctx.newOrderRecord.EP_Supply_Location_Name__c,selectedPickupDetail.Stock_Holding_Location__r.Name);
    	
    }
}