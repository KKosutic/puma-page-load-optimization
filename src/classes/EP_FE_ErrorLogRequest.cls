/* 
    @Author <Ram Rai>
    @name <EP_FE_ErrorLogRequest>
    @CreateDate <25/04/2016>
    @Description <This is Wrapper Class used in EP_FE_ErrorLogEndpoint>  
    @Version <1.0>
*/
global with sharing class EP_FE_ErrorLogRequest {
    global String description;
    global String errorDetail;
    global String browser;
    global String device;
    global String operatingSystem;
    global String methodName;
    global String className;
    global String transactionId;
    global String Severity;
    global String componentName;
    global String performedAction;
    global String userid;  
    global Boolean wasHandled;
    global String deviceInfo;
}