public with sharing class EP_OrderLinesAndSummaryHelper{
    public EP_OrderPageContext ctx ;
    public static final String CLASSNAME = 'EP_OrderControllerStep3And4Helper';
    public EP_OrderLinesAndSummaryHelper(EP_OrderPageContext ctx){
     this.ctx = ctx;
    } 
    public PageReference loadStep3() {
        EP_GeneralUtility.Log('public',CLASSNAME,'loadStep3');
        ctx.newOrderRecord.Stock_Holding_Location__c = ctx.strSelectedPickupLocationID;
        EP_PortalOrderUtil.setCustomerPoNumber(ctx.newOrderRecord);        
        if(!String.isBlank(ctx.strSelectedPickupLocationID)) {
            SetPickupDetails();
        }
        prepareProductDetails();
        ctx.isConsignmentOrder = ctx.orderDomainObj.isConsignment();
        if(ctx.isRoOrder){
            populateSupplierOptions();
        }
        setOrderRecordType();    
        return null;
    }
    public void populatesupplierOptions(){
        ctx.supplierOptions = new List < SelectOption > ();
        ctx.supplierOptions.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        if(ctx.strSelectedPickupLocationID == NULL) {
            return;
        }
        List<Account> supplierAccount = new EP_AccountMapper().getSuppliersOfSupplyLocation(ctx.strSelectedPickupLocationID );
        for(Account acc : supplierAccount) {
            ctx.supplierOptions.add(EP_Common_Util.createOption(acc.id, acc.name));
        }                     
        ctx.supplierContractMap = EP_PortalOrderUtil.fetchContracts(supplierAccount);
        
    }
    public void setOrderRecordType(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderRecordType');
        ctx.orderDomainObj = new EP_orderDomainObject(ctx.newOrderRecord);   
        ctx.orderService = new EP_orderService(ctx.orderDomainObj);
        Id recordTypeId  =  ctx.orderDomainObj.findRecordType();
        if (recordTypeId != NULL) {
            ctx.newOrderRecord.RecordTypeId = recordTypeId;
        } 
    }
    /*
    This method is used to populate "Pipeline" in delivery type when 
    default trannsporter is "No Transporter"
    */
    public csord__Order__c checkForNoTransporter(csord__Order__c orderObj,String transporterName) {
        EP_GeneralUtility.Log('Public',CLASSNAME,'checkForNoTransporter');
        String delType = orderObj.EP_Delivery_Type__c;
        orderObj = ctx.orderService.checkForNoTransporter(transporterName);
        ctx.deliveryType = orderObj.EP_Delivery_Type__c;
        ctx.strSelecteddeliveryType = orderObj.EP_Delivery_Type__c;
        ctx.strTransporterName = transporterName;//Defect 57621
        return orderObj;
    }  
    @TestVisible  
    private void SetPickupDetails(){
        EP_GeneralUtility.Log('Private',CLASSNAME,'SetPickupDetails');
        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        ctx.isSlottingEnabled = selectedPickupDetail.Stock_Holding_Location__r.EP_Slotting_Enabled__c;
        ctx.newOrderRecord.EP_Stock_Holding_Location__c = selectedPickupDetail.Stock_Holding_Location__c;
        ctx.newOrderRecord.EP_Supply_Location_Name__c =  selectedPickupDetail.Stock_Holding_Location__r.Name; 
        if(!ctx.isExRack){//Defect 57558
            ctx.transportAccount = new List<SelectOption>();
            ctx.transportAccount.add(EP_Common_Util.createOption(selectedPickupDetail.EP_Transporter__r.Id,selectedPickupDetail.EP_Transporter__r.Name));
            checkForNoTransporter(ctx.newOrderRecord,selectedPickupDetail.EP_Transporter__r.Name);                       
            ctx.startDate = EP_PortalOrderUtil.getDeliveryStartDate(selectedPickupDetail);
            ctx.availableRoutes = setRunAndRoute(ctx.strSelectedShipToID,selectedPickupDetail.Stock_Holding_Location__c);
            ctx.newOrderRecord.EP_ShipTo__c = ctx.strSelectedShipToID;
            ctx.newOrderRecord.EP_Price_Type__c = EP_Common_Constant.FUEL_FREIGHT_PRICE;
        }
        else{
            getAllAvailableDates(selectedPickupDetail);
            ctx.newOrderRecord.EP_Price_Type__c = EP_Common_Constant.FUEL_PRICE;//PE  
        }
    }
    /*
    This method is used to create picklist for route and run
    */
    public List<SelectOption> setRunAndRoute(String shipToId, String stockholdinglocationId) {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setRunAndRoute');
        List<SelectOption> listRouteOption = new List<SelectOption>();
        listRouteOption.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK,EP_Common_Constant.PICKLIST_NONE));
        //ctx.orderDomainObj = new EP_orderDomainObject(ctx.newOrderRecord);
        //ctx.orderService = new EP_orderService(ctx.orderDomainObj);
        for(EP_Route__c route : ctx.orderService.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId)) {
           listRouteOption.add(new SelectOption(route.id,route.name));
        }
        ctx.isRoutePresent = listRouteOption.size() > 1;
        return listRouteOption;
    }  
    public void getAllAvailableDates(EP_Stock_Holding_Location__c selectedPickupDetail){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getAllAvailableDates');
        ctx.timeSlots = new Map<String, List<SelectOption>>();
        EP_FE_NewOrderInfoResponse feResponse = new EP_FE_NewOrderInfoResponse();
        EP_PortalOrderUtil.getAvailableDates(feResponse,selectedPickupDetail);
        for( EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo term : feResponse.terminalInfoList){
            filterTerminalDateTime(term.availableDateTimeList,selectedPickupDetail);               
        }
        
    }
    @TestVisible
    private void filterTerminalDateTime(list<EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime> availableDateTimeList,EP_Stock_Holding_Location__c selectedPickupDetail){
        for(EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpen : availableDateTimeList  ){
            ctx.availableDatesList.add(String.valueOf(dateOpen.workingDate));
            if(selectedPickupDetail.Stock_Holding_Location__r.EP_Slotting_Enabled__c){
                ctx.timeSlots.put(null,new List<SelectOption>{EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE)}); // #novaSuiteFixes
                setTimeframeForOpenDate(dateOpen);
            }
        }
        
    }
    @TestVisible
    private void setTimeframeForOpenDate(EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpen ){
        List<String> dates = new List<String>();
        String workingDate = EP_Common_Constant.BLANK;
        List<SelectOption> newList = new List<SelectOption>();
        for(EP_FE_NewOrderInfoResponse.EP_FE_TimePeriod timeSlot : dateOpen.timePeriods  ){
            dates = String.valueOf(dateOpen.workingDate).split(EP_Common_Constant.hyphen);
            workingDate = dates[2]+EP_Common_Constant.hyphen+dates[1]+EP_Common_Constant.hyphen+dates[0];
            if(ctx.timeSlots.containsKey(workingDate)){
                newList = ctx.timeSlots.get(workingDate);
                newList.add(EP_Common_Util.createOption(String.valueOf(timeSlot.endTime), String.valueOf(timeSlot.startTime).substring(0,5)+EP_Common_Constant.HRS+EP_Common_Constant.hyphen+String.valueOf(timeSlot.endTime).substring(0,5)+EP_Common_Constant.HRS));  // #novaSuiteFixes
                ctx.timeSlots.put(workingDate,newList);
            }else{
                ctx.timeSlots.put(workingDate,new List<SelectOption>{EP_Common_Util.createOption(String.valueOf(timeSlot.endTime), String.valueOf(timeSlot.startTime).substring(0,5)+EP_Common_Constant.HRS+EP_Common_Constant.hyphen+String.valueOf(timeSlot.endTime).substring(0,5)+EP_Common_Constant.HRS)});
            }
        }
    }
    @TestVisible
    private void prepareProductDetails() {
        EP_GeneralUtility.Log('public',CLASSNAME,'prepareProductDetails');
        // Store the Ship-To and Pickup Location ID used to produce the data in step 3       
        ctx.strLoadedPickupLocationID = ctx.strSelectedPickupLocationID;
        ctx.strLoadedDeliveryType = ctx.strSelectedDeliveryType;
        setProductDetails();
        setQuantityRestrictions();
        ctx.listofOrderWrapper = new List < EP_OrderPageContext.OrderWrapper > ();
        addNewLine();
    }
    public void setProductDetails() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setProductDetails');
        EP_PriceBookEntryMapper pbEntryMapperObj = new EP_PriceBookEntryMapper();
        List < PriceBookEntry > priceBookEntries;
        ctx.strSelectedPricebookID = NULL;
        ctx.intAccountProductsFound = 0;
        ctx.productPriceMap = new Map < String, Decimal > ();
        ctx.productNameMap = new Map < String, String > ();
        ctx.productPriceBookEntryMap = new Map < String, String > ();
        ctx.productUnitOfMeasureMap = new Map < String, String > ();
        ctx.setProductIDs = new List < String > ();
        ctx.strSelectedPricebookID = ctx.accountDomainObj.getPriceBook();
        Set<Id> availableProductIds = new Set<Id>();
        if(String.isNotBlank(ctx.strSelectedPickupLocationID)&& ctx.isExrack ){
            availableProductIds = getAvailableProducts(ctx.strSelectedPickupLocationID);
        }
        if (ctx.strSelectedPricebookID == NULL) {
            return;
        }
        priceBookEntries = ctx.orderService.findPriceBookEntries(ctx.strSelectedPricebookID,ctx.newOrderRecord);
        if (priceBookEntries.isEmpty()) {
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, Label.EP_No_Product_To_Choose);
            return;
        }
        for (PricebookEntry pbe: priceBookEntries) {
            // Update the memory maps
            ctx.productPriceMap.put(pbe.Id, pbe.UnitPrice);
            ctx.productPriceBookEntryMap.put(pbe.Id, pbe.Product2Id);
            ctx.PriceBookEntryproductMap.put(pbe.Product2Id, pbe.Id); // tank work
            ctx.productUnitOfMeasureMap.put(pbe.Id, pbe.EP_Unit_Of_Measure__c);
            ctx.productNameMap.put(pbe.Id, pbe.Product2.Name);
            ctx.setProductIDs.add(pbe.Product2Id);
            ctx.intAccountProductsFound++;
            // If the PriceBookEntry does not have a UoM then add the product default one
            if (pbe.EP_Unit_Of_Measure__c == NULL) {
                ctx.productUnitOfMeasureMap.put(pbe.Id, pbe.Product2.EP_Unit_Of_Measure__c);
            }
        }
         // End for
        
    }
    /*
    Get Available Products
    */
    public Set<Id> getAvailableProducts(Id supplyLocationId) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'getAvailableProducts');
        Set<Id> productIds = new Set<Id>();
        Id storageLocId = ctx.mapOfSupplyLocation.get(supplyLocationId).Stock_Holding_Location__c;
        for(Ep_Inventory__c inventory : [ Select EP_Product__c,EP_Product__r.EP_Product_Sold_As__c from  Ep_Inventory__c where EP_Storage_Location__c =: storageLocId AND EP_Product__r.EP_Product_Sold_As__c =: ctx.newOrderRecord.EP_Order_Product_Category__c ]) { 
            productIds.add(inventory.EP_Product__c);
        }
        return productIds;
    }
    
    public void setQuantityRestrictions() {
        EP_GeneralUtility.Log('public',CLASSNAME,'setQuantityRestrictions');
        String country;
        if (!ctx.isExrack){ 
            country = ctx.mapOfShipTo.get(ctx.strSelectedShipToID).EP_Country__c; 
        }   
        else{
            country = ctx.mapOfSupplyLocation.get(ctx.strLoadedPickupLocationID).Stock_Holding_Location__r.EP_Country__c;
        }
        List < EP_Order_Configuration__c > listOrderConfigurations = new EP_OrderConfigMapper().getRecordsByIdsCountry(ctx.setProductIDs,country);
        ctx.availableProductQuantitiesMap = new Map < String, List < String >> ();
        String strKey;
        List<String> productQty;
        if (!listOrderConfigurations.isEmpty()) {
            ctx.availableProductQuantitiesMap =  EP_PortalOrderUtil.getavailableProductQuantitiesMap(listOrderConfigurations);
            ctx.productRangeMap = EP_PortalOrderUtil.getproductRangeMap(listOrderConfigurations);
        } 
        else {
            String strOrderConfigurationError = Label.EP_System_Setup_For_Market_Incomplete;
            strOrderConfigurationError = strOrderConfigurationError.replace(EP_Common_Constant.GENERIC_MSG_REPLACE_STRING, ctx.orderAccount.EP_Country__c);
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, strOrderConfigurationError));
            ctx.hasAccountSetupError = true;
        }
    }
    public void addNewLine(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'addNewLine'); 
        if (ctx.listofOrderWrapper.size() > 0) {
            ctx.isPriceCalculated = false;
            updateOrderItemLinesfornewOrder();
            if (ApexPages.hasMessages(ApexPages.severity.ERROR)) {
                return;
            }
        }
        // ****** End *******
        // Create new wrapper item
        EP_OrderPageContext.OrderWrapper orderWrapper = new EP_OrderPageContext.OrderWrapper();
        orderWrapper.oliIndex = ctx.intOrderLineIndex;
        orderWrapper.oliAvailableProducts = getProductOptions();
        orderWrapper.oliAvailableTanks = getListShipToTanks(); // tank work
        ctx.listofOrderWrapper.add(orderWrapper);
        ctx.intOrderLineIndex++;
        system.debug('ctx.listofOrderWrapper****'+ ctx.listofOrderWrapper);
        
    }
    public List<SelectOption> getProductOptions() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'getProductOptions');
        List<SelectOption> listProductOptions = new List <SelectOption> ();
        Map <String, String> selectedProductMap = new Map <String, String> ();
        String strProductName;
        String strUoM;
        String strProductSetupKey;
        Id productID;
        listProductOptions.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE)); // Hardcoding removal
        // Build list of products that are already selected by the user (product and UoM)
        for (EP_OrderPageContext.OrderWrapper orderWrapper: ctx.listofOrderWrapper) {
            selectedProductMap.put(orderWrapper.oliPricebookEntryID + orderWrapper.oliProductUoM, orderWrapper.oliProductUoM);
        } // End for
        // Dynamicaly build the list to ensure that products already selected are not re-selected
        for (Id priceBookEntryId: ctx.productPriceMap.keySet()) {
            strProductName = ctx.productNameMap.get(priceBookEntryId);
            strUoM = ctx.productUnitOfMeasureMap.get(priceBookEntryId);
            productID = ctx.productPriceBookEntryMap.get(priceBookEntryId);
            strProductSetupKey = EP_PortalOrderUtil.constructProductQuantityLimitKey(productID, strUoM);
            // Only include products that have range or allowable quantity setup // defect_anu
            if ((ctx.availableProductQuantitiesMap.containsKey(strProductSetupKey) || ctx.productRangeMap.containsKey(strProductSetupKey)) && !selectedProductMap.containsKey(priceBookEntryId + strUoM)) {
                listProductOptions.add(EP_Common_Util.createOption(priceBookEntryId,strProductName + EP_Common_Constant.LEFT_BRACKET + strUoM + EP_Common_Constant.RIGHT_BRACKET)); // 
            } // End setup check
        } //
        return listProductOptions;
    }
    public List<SelectOption> getListShipToTanks() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'getListShipToTanks');
        List<SelectOption> shipToTanks = new list<SelectOption>();
        set<Id> selectedTankIds = new set<Id>();
        shipToTanks.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        for (EP_OrderPageContext.OrderWrapper o: ctx.listofOrderWrapper) {
            selectedTankIds.add(o.oliTanksID);
        }

        for (Id tankId :  ctx.mapShipToOperationalTanks.keySet()) {
            ctx.shipToOperationalTankIds.add(tankId);
        }
        ctx.shipToOperationalTankIds.removeall(selectedTankIds);
        for(Id tankId : ctx.shipToOperationalTankIds) {
            EP_Tank__c tankobj = ctx.mapShipToOperationalTanks.get(tankId);
            String tankCodeAlias = tankobj.EP_Tank_Code__c;               
            if(tankobj.EP_Tank_Alias__c != NULL){
                tankCodeAlias = tankCodeAlias + EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
            }
            shipToTanks.add(EP_Common_Util.createOption(tankId,tankCodeAlias)); // #novaSuiteFixes
            
        }
    
        return shipToTanks;
    }
    public PageReference loadStep4(){
        EP_GeneralUtility.Log('public',CLASSNAME,'loadStep4');
        ctx.newOrderRecord.EP_Requested_Delivery_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.strSelectedDate);
        ctx.newOrderRecord.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(ctx.availableSlots)).subString(0,8);
        ctx.newOrderRecord.EP_Route__c = ctx.strSelectedRoute;
        ctx.newOrderRecord.EP_Run__c = ctx.strSelectedRun;
        ctx.newOrderRecord.EP_Run_Id__c = String.ValueOf(ctx.runMap.get(ctx.strSelectedRun));
        updateOrderItemLinesfornewOrder();
        return null;
    }
    public boolean IsValidOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'IsValidOrder');
        boolean isvalidOrder = true;
        if(ctx.isPrepaymentCustomer && ctx.showCustomerPoField){
            Boolean showPrepayCustomerPOBlankError = EP_PortalOrderUtil.checkPrepayForPoNumber(ctx.newOrderRecord,ctx.isPrepaymentCustomer,ctx.isConsignmentOrder,ctx.isConsumptionOrder,ctx.isDummy,ctx.orderAccount); // #customerPoWork
            if(showPrepayCustomerPOBlankError) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR,   LABEL.EP_ProvideCustomerPurchaseOrderNumber));
                isvalidOrder = false;
            }
        }
        if(ctx.orderDomainObj.isValidOrderDate()){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Order_Date_Error));
            isvalidOrder = false;
        }
        /*Defect 53419 Start*/
        if (ctx.orderDomainObj.isValidLoadingDate()) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Loading_Date_Error));
            isvalidOrder = false;
        }
        
        if (ctx.orderDomainObj.isValidExpectedDate()) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Delivery_Date_Error));
            isvalidOrder = false;
        }
        /*Defect 53419 End*/
        return isvalidOrder;
    }
    public void updateOrderItemLinesfornewOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'updateOrderItemLinesfornewOrder');
        List<csord__Order_Line_Item__c> listItemsToDelete = new List<csord__Order_Line_Item__c>();
        List<csord__Order_Line_Item__c> listValidatedOrderLines = new List<csord__Order_Line_Item__c>();
        Id productID;     
        csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
        EP_OrderPageContext.OrderWrapper orderWrapperLocal;
        List<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new List<EP_OrderPageContext.OrderWrapper>();
        ctx.newOrderRecord = ctx.orderService.setRequestedDateTime(ctx.newOrderRecord,ctx.strSelectedDate,ctx.availableSlots);
        boolean isValidLineItem = true;
        if(!IsValidOrder()) {
            return;
        }            
        for (EP_OrderPageContext.OrderWrapper orderWrapperObj: ctx.listofOrderWrapper) {
            if(isValidOrderLineItem(orderWrapperObj.orderLineItem)) {
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_BOL_Missing_Error));
                isValidLineItem = false;
                break;
            }
            setOrderLineitemsForUpdate(orderWrapperLocal,orderItemObj,orderWrapperObj,listItemsToDelete,updatedOrderWrapper,listValidatedOrderLines);
            
        }
        if(!isValidLineItem || ctx.isErrorInPage){
            return;
        }
        if(!listItemsToDelete.isEmpty()){
            EP_OrderItemMapper EP_OrderItemMapperobj = new EP_OrderItemMapper();
            EP_OrderItemMapperobj.doCSDelete(listItemsToDelete);
        }
        if(orderWrapperLocal != null){
            orderWrapperLocal.orderLineItem = orderItemObj;
            updatedOrderWrapper.add(orderWrapperLocal);
        }            
        ctx.listofOrderWrapper = updatedOrderWrapper;
        if (!listValidatedOrderLines.isEmpty()) {
            Boolean isAvailable = false;
            ctx.productInventoryMap = EP_PortalOrderUtil.getInventoryMap(listValidatedOrderLines, ctx.newOrderRecord, ctx.strSelectedPickupLocationID);
        }
        if(!ctx.isRoOrder && !ctx.isErrorInPage) {
            ValidateInventory(ctx.listofOrderWrapper,orderItemObj);
        }       
        
    } 
    public boolean isValidOrderLineItem(csord__Order_Line_Item__c orderlineitem){
        EP_GeneralUtility.Log('Public',CLASSNAME,'isValidOrderLineItem');
        return (ctx.isRoOrder && !ctx.isConsumptionOrder && String.isBlank(orderLineItem.EP_BOL_Number__c));
    }
    public void ValidateInventory(List<EP_OrderPageContext.OrderWrapper> listofOrderWrapper,csord__Order_Line_Item__c orderItemObj){
        EP_GeneralUtility.Log('public',CLASSNAME,'ValidateInventory');
        for (EP_OrderPageContext.OrderWrapper orderWrapper: listofOrderWrapper) {
            orderItemObj = orderWrapper.orderLineItem;
            String productId= ctx.productPriceBookEntryMap.get(orderWrapper.oliPricebookEntryID);
            orderItemObj.EP_Inventory_Availability__c = ctx.productInventoryMap.get(productId);
            if(orderItemObj.EP_Inventory_Availability__c == EP_Common_Constant.LOW_INV) {
                apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_Low_Inventory_Error));
            }
            else if(orderItemObj.EP_Inventory_Availability__c == EP_Common_Constant.NO_INV) {
                apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_Inventory_Error));
            }
            else if(orderItemObj.EP_Inventory_Availability__c != EP_Common_Constant.GOOD_INV) {
                apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_Inventory_Not_Available));
                orderItemObj.EP_Inventory_Availability__c = EP_Common_Constant.INV_UNAVAILABLE;
                ctx.isSlottingEnabled = true;
            }
            
        }
    }
    public void setOrderLineitemsForUpdate(EP_OrderPageContext.OrderWrapper newOrderWrapperobj, csord__Order_Line_Item__c orderItemObj, EP_OrderPageContext.OrderWrapper orderWrapperObj, list<csord__Order_Line_Item__c> listItemsToDelete, list<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper, list<csord__Order_Line_Item__c>listValidatedOrderLines)
    {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderLineitemsForUpdate');
        setDeleteListAndUpdatedOrderWrapper(newOrderWrapperobj,orderItemObj,orderWrapperObj,listItemsToDelete,updatedOrderWrapper);
        setTankDetailsofOrderWrapper(orderWrapperObj);
        orderWrapperObj.oliProductName = ctx.productNameMap.get(orderWrapperObj.oliPricebookEntryID);
        orderWrapperObj.oliQuantityRange = EP_Common_Constant.NA;
        orderWrapperObj.oliIndex = ctx.intOrderLineIndex;
        ctx.intOrderLineIndex++;
        setOrderLineItemValues(orderWrapperObj.orderLineItem,orderWrapperObj);
        orderItemObj = orderWrapperObj.orderLineItem;
        orderItemObj.EP_Quantity_UOM__c = EP_Common_Constant.BLANK;
        orderWrapperObj.oliProductUoM = NULL;
        orderWrapperObj.listChildOrderLineItems = new List < csord__Order_Line_Item__c > ();
        orderWrapperObj.oliTotalPrice = NULL;
        orderWrapperObj.oliOtherCosts = NULL;
        setProductQuantity(orderWrapperObj.orderLineItem,orderWrapperObj);
        setOrderWrapperAndValidateOrderLines(orderWrapperObj.orderLineItem,newOrderWrapperobj,orderWrapperObj,listValidatedOrderLines);
        orderItemObj.Quantity__c = orderWrapperObj.oliQuantity;

    }

    
    public void setProductQuantity(csord__Order_Line_Item__c orderItemObj,EP_OrderPageContext.OrderWrapper orderWrapperObj) {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setProductQuantity');
        if (ctx.productUnitOfMeasureMap.containsKey(orderWrapperObj.oliPricebookEntryID)) {
            orderItemObj.EP_Quantity_UOM__c = ctx.productUnitOfMeasureMap.get(orderWrapperObj.oliPricebookEntryID);
            orderWrapperObj.orderLineItem.EP_Quantity_UOM__c = ctx.productUnitOfMeasureMap.get(orderWrapperObj.oliPricebookEntryID);
            orderWrapperObj.oliProductUoM = ctx.productUnitOfMeasureMap.get(orderWrapperObj.oliPricebookEntryID);                                          
        }
    }
    public void setOrderLineItemValues(csord__Order_Line_Item__c orderItemObj,EP_OrderPageContext.OrderWrapper orderWrapperObj) {
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderLineItemValues');
        if( orderItemObj.Id == NULL ) {
            if(orderWrapperObj.oliTanksID != null && ctx.mapShipToOperationalTanks.containsKey(orderWrapperObj.oliTanksID) && ctx.mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Product__C != null) {                          
                orderItemObj.PriceBookEntryId__c = ctx.PriceBookEntryproductMap.get(ctx.mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Product__C);
            }
            else {
                orderItemObj.PriceBookEntryId__c = orderWrapperObj.oliPricebookEntryID;
            }
            orderItemObj.OrderId__c = ctx.newOrderRecord.Id;
        }
         
        
        //System.debug('ctx.productPriceMap:---'+ctx.productPriceMap.get(orderItemObj.PriceBookEntryId));
        orderItemObj.UnitPrice__c = ctx.productPriceMap.get(orderItemObj.PriceBookEntryId__c);
        orderItemObj.EP_Order_Line_Relationship_Index__c = orderWrapperObj.oliIndex;
        orderItemObj.Quantity__c = orderWrapperObj.oliQuantity;
        orderItemObj.EP_Is_Standard__c = true;
    }
    public void setTankDetailsofOrderWrapper(EP_OrderPageContext.OrderWrapper orderWrapperObj) {
        EP_GeneralUtility.Log('public',CLASSNAME,'setTankDetailsofOrderWrapper');
        if(orderWrapperObj.oliTanksID == null){
            return;
        }
        EP_Tank__c tankobj = ctx.mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID);
        
        if(tankobj != null){
            orderWrapperObj.oliPricebookEntryID =  ctx.PriceBookEntryproductMap.get(tankobj.EP_Product__C);   
            orderWrapperObj.orderLineItem.EP_Tank__c = orderWrapperObj.oliTanksID; // tank work
            orderWrapperObj.oliTankProductName = tankobj.EP_Product__r.Name;
            if(tankobj.EP_Safe_Fill_Level__c != null){
                orderWrapperObj.oliTankSafeFillLvl = Integer.valueOf(tankobj.EP_Safe_Fill_Level__c);
            }
            orderWrapperObj.oliTanksName = tankobj.EP_Tank_Code__c;
            if(tankobj.EP_Tank_Alias__c != NULL){
                orderWrapperObj.oliTanksName =  orderWrapperObj.oliTanksName+EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
            }
        }
    }
    public void setDeleteListAndUpdatedOrderWrapper(EP_OrderPageContext.OrderWrapper orderWrapper,csord__Order_Line_Item__c orderItemObj,EP_OrderPageContext.OrderWrapper orderWrapperObj,List<csord__Order_Line_Item__c> listItemsToDelete,List<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'setDeleteListAndUpdatedOrderWrapper');
        if( orderWrapperObj.oliPricebookEntryID != orderWrapperObj.orderLineItem.PriceBookEntryId__c 
        && orderWrapperObj.orderLineItem.PriceBookEntryId__c != null) {
            orderItemObj = new csord__Order_Line_Item__c();
            orderWrapper = new EP_OrderPageContext.OrderWrapper();                    
            if(orderWrapperObj.orderLineItem.Id != NULL) {
                listItemsToDelete.add(orderWrapperObj.orderLineItem);   
            }
            orderWrapperObj.orderLineItem.Id = null;
            orderItemObj = orderWrapperObj.orderLineItem;
            orderItemObj.PriceBookEntryId__c = orderWrapperObj.oliPricebookEntryID;
        }
        else{
            orderItemObj = orderWrapperObj.orderLineItem;
            if(orderWrapperObj.oliQuantity != orderWrapperObj.orderLineItem.Quantity__c){
                ctx.isPriceCalculated = false;
            }           
        }
        updatedOrderWrapper.add(orderWrapperObj);
        
    }
    
    public void setOrderWrapperAndValidateOrderLines(csord__Order_Line_Item__c orderItemObj,EP_OrderPageContext.OrderWrapper orderWrapper,EP_OrderPageContext.OrderWrapper orderWrapperObj,List<csord__Order_Line_Item__c> listValidatedOrderLines) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'setOrderWrapperAndValidateOrderLines');
        if(orderWrapper!=null){
            orderWrapper = orderWrapperObj;
        }
        validateEnteredQuantity(orderWrapperObj);
        if(orderWrapperObj.oliIsValid) {
            listValidatedOrderLines.add(orderItemObj);
        }
        /*if(orderWrapper!=null){
            orderWrapper = orderWrapperObj;
            validateEnteredQuantity(orderWrapper);
            if(orderWrapper.oliIsValid) {
                listValidatedOrderLines.add(orderItemObj);
            }
        }else {                   
            validateEnteredQuantity(orderWrapperObj);
            if(orderWrapperObj.oliIsValid) {
                listValidatedOrderLines.add(orderItemObj);
            }
        }*/
    }
    @TestVisible
     private void validateEnteredQuantity(EP_OrderPageContext.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'validateEnteredQuantity');
        String strSelectedProductID;
        Boolean isValid = TRUE;
        orderWrapper.oliProductError = EP_Common_Constant.BLANK;
        orderWrapper.oliQuantityError = EP_Common_Constant.BLANK;
        // Ensure that the user has selected products for all order line items
        if (string.isblank(orderWrapper.oliPricebookEntryID)) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Select_product));
            orderWrapper.oliProductError = EP_Common_Constant.ERROR;
            isValid = FALSE;
        }
        if (orderWrapper.oliQuantity == NULL) {
            isValid = FALSE;
            setQuantityError(orderWrapper);
            return;
            //orderWrapper.oliQuantity = 0;
        }
        // Ensure that quantity is > 0
        if (orderWrapper.oliQuantity <= 0) {
            setQuantityError(orderWrapper);
            isValid = FALSE;
            // 1. Validation for non mixing countries
            strSelectedProductID = ctx.productPriceBookEntryMap.get(orderWrapper.oliPricebookEntryID);
        }
        
        if(isQuantiyHigherthanSafefill(orderWrapper)){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Warning, LABEL.EP_Qty_entered_higher_than_safe_fill_level);
            ApexPages.addMessage(msg);
            
        }
        orderWrapper.oliIsValid = isValid;
        
    }
    @TestVisible
    private boolean isQuantiyHigherthanSafefill(EP_OrderPageContext.OrderWrapper orderWrapper){
        system.debug('--'+ctx.mapShipToOperationalTanks+'****'+orderWrapper.oliTanksID);
        //System.debug(ctx.isOprtnlTankAvail+'****'+ctx.mapShipToOperationalTanks.containsKey(orderWrapper.oliTanksID)+'********'+(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c != null)+'******'+orderWrapper.oliQuantity+'***'+Integer.valueOf(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c));
        return (ctx.isOprtnlTankAvail && ctx.mapShipToOperationalTanks.containsKey(orderWrapper.oliTanksID) && 
        (ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c != null) &&
        orderWrapper.oliQuantity > Integer.valueOf(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c));
        
    }
    @TestVisible
    private void setQuantityError(EP_OrderPageContext.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'setQuantityError');
        ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Invalid_Quantity));
        orderWrapper.oliQuantityError = EP_Common_Constant.ERROR;
        
    }  
}