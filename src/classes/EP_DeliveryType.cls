/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_DeliveryType
  * @CreateDate  : 05/02/2017
  * @Description : This class is Parent for Delivery  and Ex-Rack Strategies and contains Common Code Of Delivery and Ex-Rack
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public virtual class EP_DeliveryType {

    public Map<id,String> inventoryMap;
    public EP_AccountMapper accountMapper = new EP_AccountMapper();
    public EP_StockHoldingLocationMapper stockHoldingMapper = new EP_StockHoldingLocationMapper();

    /** This method updates Transport Services fields
      *  @date      05/02/2017
      *  @name      checkOrderCreditCheck
      *  @param     Order objOrder
      *  @return    NA
      *  @throws    NA
      */ 
      public virtual void updateTransportService(csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','updateTransportService');
		/**** Defect- 57023 Fix START******/
        /* if (objOrder.EP_Transporter__c != null
            && objOrder.Stock_Holding_Location__c !=null) {
                Account objAccount = accountMapper.getTransporterAccount(objOrder.EP_Transporter__c);     
                EP_Stock_Holding_Location__c objStockHoldLoc = stockHoldingMapper.getTransportDetails(objOrder.Stock_Holding_Location__c);
                 if( String.isNotBlank(objAccount.EP_Provider_Managed_Transport_Services__c ) 
                   && objAccount.EP_Provider_Managed_Transport_Services__c.equalsIgnoreCase( EP_Common_Constant.STRING_TRUE ) 
                   && string.isNotBlank(objStockHoldLoc.EP_Use_Managed_Transport_Services__c ) 
                   && objStockHoldLoc.EP_Use_Managed_Transport_Services__c.equalsIgnoreCase( EP_Common_Constant.STRING_TRUE ))
                   {
                       objOrder.EP_Use_Managed_Transport_Services__c = EP_Common_Constant.STRING_TRUE;
                   }
                else if ( objOrder.EP_Use_Managed_Transport_Services__c == EP_Common_Constant.STRING_TRUE ) {
                    objOrder.EP_Use_Managed_Transport_Services__c = EP_Common_Constant.STRING_FALSE;
                }
            }
           */ 
        
		if(objOrder.Stock_Holding_Location__c != null ) {                      
			EP_Stock_Holding_Location__c objStockHoldLoc = stockHoldingMapper.getTransportDetails(objOrder.Stock_Holding_Location__c);                 
            objOrder.EP_Use_Managed_Transport_Services__c =  objStockHoldLoc.EP_Use_Managed_Transport_Services__c;
        }
        /**** Defect- 57023 Fix End ******/
      }

    /** This method is used for return inventory details
      *  @date      05/02/2017
      *  @name      getInventoryDetails
      *  @param     List<OrderItem> oiList, Order newOrderRecord
      *  @return    Map<Id,String> 
      *  @throws    NA
      */
      public virtual Map<Id,String> getInventoryDetails(List<csord__Order_Line_Item__c> oiList, csord__Order__c newOrderRecord){
        EP_GeneralUtility.Log('Public','EP_DeliveryType','getInventoryDetails');
        return inventoryMap;
      }



    /** This method is shipTos
      *  @date      05/02/2017
      *  @name      getShipTos
      *  @param     Id accountId
      *  @return    List<Account> 
      *  @throws    NA
      */
      public virtual List<Account> getShipTos(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','getShipTos');

        List<Account> lstAccount = new List<Account>();

        return lstAccount;
      }


      EP_InventoryMapper inventorymapper = new EP_InventoryMapper();
  /** This method is to get products avaliable from inventory
    *  @date      05/02/2017
    *  @name      getShipTos
    *  @param     Id accountId
    *  @return    List<Account> 
    *  @throws    NA
    */
    public Set<Id> getAvailableProducts(Id storageLocId,csord__Order__c objOrder) {
      EP_GeneralUtility.Log('Public','EP_DeliveryType','getAvailableProducts');

      Set<Id> productIds = new Set<Id>();

      for(Ep_Inventory__c inventory :  inventorymapper.getInventoryByLocationId(storageLocId,objOrder.EP_Order_Product_Category__c)) {

        productIds.add(inventory.EP_Product__c);
      }
      return productIds;
    }


    /** This method is used to get price book entries
      *  @date      05/02/2017
      *  @name      findPriceBookEntries
      *  @param     Id pricebookId,Id storageLocId,Order objOrder
      *  @return    List<PriceBookEntry>
      *  @throws    NA
      */ 
      public virtual List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','findPriceBookEntries');

        return new List<PriceBookEntry>();
      }


    /** This method is used to get operational tanks on the selected ship to
      *  @date      05/02/2017
      *  @name      getOperationalTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public virtual Map<Id, EP_Tank__c> getOperationalTanks(String accountId) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','getOperationalTanks');
        Map<Id, EP_Tank__c> mapOperationalTanks = new Map<Id, EP_Tank__c>();
        return mapOperationalTanks;      
      }

      /** This method is used to get tanks on the selected ship to
      *  @date      05/02/2017
      *  @name      getTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public virtual Map<Id, EP_Tank__c> getTanks(String accountId) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','getTanks');
        Map<Id, EP_Tank__c> mapTanks = new Map<Id, EP_Tank__c>();
        return mapTanks;      
      }


    /** This method is used to get the Routes based on Ship to and Storage Location
      *  @date      05/02/2017
      *  @name      getAllRoutesOfShipToAndLocation
      *  @param     String shipId, String shlId
      *  @return    List<EP_Route__c>
      *  @throws    NA
      */ 
      public virtual List<EP_Route__c> getAllRoutesOfShipToAndLocation( String shipId, String shlId ){
        EP_GeneralUtility.Log('Public','EP_DeliveryType','getAllRoutesOfShipToAndLocation');
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        
          return routeList;       
      }


     /** This method is used set the visbility of Customer PO field on portal order page
      *  @date      05/02/2017
      *  @name      showCustomerPO
      *  @param     Order orderObject
      *  @return    Boolean
      *  @throws    NA
      */ 
      public virtual Boolean showCustomerPO(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_DeliveryType','showCustomerPO');

        return false;
      }

    /** This method is used to populate "Pipeline" in delivery type when default trannsporter is "No Transporter"
      *  @date      01/03/2017
      *  @name      checkForNoTransporter
      *  @param     String transporterName Order orderObject
      *  @return    Order
      *  @throws    NA
      */ 
      public virtual csord__Order__c checkForNoTransporter(csord__Order__c orderObj,String transporterName) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','checkForNoTransporter');
        if(transporterName != null && transporterName.equalsIgnoreCase(EP_Common_Constant.NO_TRANSPORTER)) {
            orderObj.EP_Delivery_Type__c = EP_Common_Constant.PIPELINE;
        //Defect #57558 Start
        //Defect #57796 Start    
        }else if (orderObj.EP_Delivery_Type__c != EP_Common_Constant.EX_RACK){
        //Defect #57796 End
         	orderObj.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        } 
        //Defect #57558 End
        return orderObj;
      }

  /** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual Boolean isOrderEntryValid(integer numOfTransportAccount){
      EP_GeneralUtility.Log('Public','EP_DeliveryType','isOrderEntryValid');
      return true;
    }

    /** returns record recordype for the Order
    *  @date      22/02/2017
    *  @name      findRecordType
    *  @param     NA
    *  @return    Id
    *  @throws    NA
    */
    public virtual Id findRecordType(EP_OrderEpoch epochType,EP_VendorManagement vmType,EP_ConsignmentType consignmentType) {
        EP_GeneralUtility.Log('Public','EP_DeliveryType','findRecordType');
        return epochType.findRecordType(vmType,consignmentType);
    }
  }