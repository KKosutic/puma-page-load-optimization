@isTest
public class TrafiguraGetProductOptions_UT {

    @isTest
    static void execute_case1_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', newAccount.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeBulk);
        inputMap.put('OrderType', EP_OrderConstant.salesOrderType);
        
        Test.startTest();
            TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
            getOptions.execute(inputMap);
        Test.stopTest();
    }
    
    @isTest
    static void execute_case2_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', newAccount.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeBulk);
        inputMap.put('OrderType', 'Test');
        
        Test.startTest();
            TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
            getOptions.execute(inputMap);
        Test.stopTest();
    }
    
    @isTest
    static void execute_case3_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', newAccount.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeService);
        inputMap.put('OrderType', 'Test');
        
        Test.startTest();
            TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
            getOptions.execute(inputMap);
        Test.stopTest();
    }
    
    @isTest
    static void execute_case4_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', newAccount.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypePackaged);
        inputMap.put('OrderType', EP_OrderConstant.salesOrderType);
        //inputMap.put('LocationId', );
        
        Test.startTest();
            TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
            getOptions.execute(inputMap);
        Test.stopTest();
    }
    
    @isTest
    static void execute_case5_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', newAccount.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypePackaged);
        inputMap.put('OrderType', 'Test');
        
        Test.startTest();
            TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
            getOptions.execute(inputMap);
        Test.stopTest();
    }
}