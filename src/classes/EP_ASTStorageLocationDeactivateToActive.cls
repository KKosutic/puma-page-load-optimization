/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationDeactivateToActive >
*  @CreateDate <20/12/2017>
*  @Description <Handles Storage Ship To Account status change from 06-Deactivate to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationDeactivateToActive extends EP_AccountStateTransition {
	
	/**
    * @author           Accenture
    * @name             EP_ASTStorageLocationDeactivateToActive
    * @date             20/12/2017
    * @description      Constructor
    * @param            NA
    * @return           NA 
    */
    public EP_ASTStorageLocationDeactivateToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }
	
	/**
    * @author           Accenture
    * @name             isTransitionPossible
    * @date             20/12/2017
    * @description      Check wether transitionposoble or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactivateToActive ','isTransitionPossible');
        return super.isTransitionPossible();
    }
	
	/**
    * @author           Accenture
    * @name             isRegisteredForEvent
    * @date             20/12/2017
    * @description      Check wether Registered For Event or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactivateToActive ', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
	
	/**
    * @author           Accenture
    * @name             isGuardCondition
    * @date             20/12/2017
    * @description      Check all validation
    * @param            NA
    * @return           boolean 
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactivateToActive',' isGuardCondition');        
        return true;
    }
}