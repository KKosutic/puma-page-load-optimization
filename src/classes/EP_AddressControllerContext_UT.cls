@isTest
private class EP_AddressControllerContext_UT{
    public static final String CNTRYCODESTR = 'AU';
    public static final String CRSTR = 'CR';
    public static final String ADDRESSSTR = 'address';
    public static final String ISADDRVALID = 'IS ADDRESS VALID';

    static testMethod void checkForRequestReviewAction_test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.googleMapKey = 'TestgoogleMapKey';
        EP_ChangeRequest__c changeRequestObj = new EP_ChangeRequest__c();
        changeRequestObj.EP_Object_Type__c = EP_Common_Constant.ACCOUNT_OBJ;
        changeRequestObj.EP_Record_Type__c = EP_Common_Constant.SELL_TO;
        changeRequestObj.EP_Account__c = accountObj.id;
        changeRequestObj.EP_Request_Date__c = System.today();   
        insert changeRequestObj;
        Id reqID  = changeRequestObj.id;
        Test.startTest();
        localObj.checkForRequestReviewAction(reqID);
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }

    static testMethod void createChangeRequest_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        accountObj.EP_Account_Name_2__c = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.record = accountObj;
        localObj.recordId = accountObj.id;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        Test.startTest();
        localObj.createChangeRequest();
        Test.stopTest();
        List<EP_ChangeRequestLine__c > listChangeRequestLine = [SELECT Id FROM EP_ChangeRequestLine__c];
        Account accObj = [SELECT Id,EP_Account_Name_2__c FROM Account WHERE Id =: accountObj.id];
        System.assertEquals(True, listChangeRequestLine.size() > 0);
        System.assertNotEquals(EP_Common_Constant.ACCOUNT_OBJ, accObj.EP_Account_Name_2__c);
    }
    /**/ 
    static testMethod void createChangeRequest_NegativeTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.record = accountObj;
        accountObj.EP_Is_Customer_Reference_Mandatory__c = True;
        Test.startTest();
        localObj.createChangeRequest();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }

    static testMethod void checkUserAccess_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        Profile profileObj = [SELECT Id FROM Profile WHERE Name =:'Read Only' LIMIT 1]; 
        User userObj = EP_TestDataUtility.createUser(profileObj.id);
        Insert userObj;
        Test.startTest();
        System.runAs(userObj){
            localObj.checkUserAccess();
        }
        Test.stopTest();
        System.assertEquals(True, localObj.showError);
    }

    static testMethod void setAddressFields_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);

        localObj.record = database.query('Select Id from Account where Id=\''+accountObj.Id+'\'');

        localObj.mTypeField.put(EP_Common_Constant.STREET_STR,'BillingStreet');
        localObj.mTypeField.put(EP_Common_Constant.CITY_STR,'BillingCity');
        localObj.mTypeField.put(EP_Common_Constant.POST_CODE,'BillingPostalCode');
        localObj.mTypeField.put(EP_COMMON_CONSTANT.Country.toUpperCase(),'BillingCountry');
        localObj.mTypeField.put(EP_Common_Constant.STATE_STR,'BillingState');
        localObj.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        localObj.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        localObj.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
        localObj.mTypeField.put(ISADDRVALID,'EP_Is_Valid_Address__c');

        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.street1 = '';
        addressWrapperObj.Street2 = '';
        addressWrapperObj.City = accountObj.billingcity;
        addressWrapperObj.zipCode = accountObj.billingpostalCode;
        addressWrapperObj.state = accountObj.billingState;
        addressWrapperObj.Country = accountObj.billingcountry;
        addressWrapperObj.latitude = 1;
        addressWrapperObj.longitude = 2;
        addressWrapperObj.cntryCode = '';
        addressWrapperObj.oldAddress = 'oldAddressBillingAddress';
        addressWrapperObj.addressFieldLabel ='BillingAddress'; 
        addressWrapperObj.addressFieldAPIName = 'BillingAddress';
        addressWrapperObj.countryRecord = [SELECT Id FROM EP_Country__c LIMIT 1];
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        localObj.setAddressFields();
        Test.stopTest();
        System.assertEquals(true,localObj.record.get('EP_Is_Valid_Address__c'));
    }

    static testMethod void setLatitudeLongitude_Test1() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        //localObj.record = new SObject ;
        localObj.mTypeField.put(EP_Common_Constant.STREET_STR,'BillingStreet');
        localObj.mTypeField.put(EP_Common_Constant.CITY_STR,'BillingCity');
        localObj.mTypeField.put(EP_Common_Constant.POST_CODE,'BillingPostalCode');
        localObj.mTypeField.put(EP_COMMON_CONSTANT.Country.toUpperCase(),'BillingCountry');
        localObj.mTypeField.put(EP_Common_Constant.STATE_STR,'BillingState');
        localObj.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        localObj.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        localObj.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
        localObj.mTypeField.put(ISADDRVALID,'Yes');
        localObj.record = accountObj;
        Test.startTest();
        localObj.setLatitudeLongitude();
        Test.stopTest();
        //No Assertion as the method only delegates to other methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void setLatitudeLongitude_Test2() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.mTypeField.put(EP_Common_Constant.STREET_STR,'BillingStreet');
        localObj.mTypeField.put(EP_Common_Constant.CITY_STR,'BillingCity');
        localObj.mTypeField.put(EP_Common_Constant.POST_CODE,'BillingPostalCode');
        localObj.mTypeField.put(EP_COMMON_CONSTANT.Country.toUpperCase(),'BillingCountry');
        localObj.mTypeField.put(EP_Common_Constant.STATE_STR,'BillingState');
        //localObj.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        localObj.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        localObj.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
        localObj.mTypeField.put(ISADDRVALID,'Yes');
        localObj.record = accountObj;
        Test.startTest();
        localObj.setLatitudeLongitude();
        Test.stopTest();
        //No Assertion as the method only delegates to other methods,hence adding a dummy assert.
        system.Assert(true);
    }

    static testMethod void setLATLANGString_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.record = database.query('SELECT Id , EP_Position__c FROM Account WHERE Id=\''+accountObj.Id+'\'');
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.latitude = 1;
        localObj.addressobj = addressWrapperObj;
        localObj.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        Test.startTest();
        localObj.setLATLANGString();
        Test.stopTest();
        System.assertEquals(1.0,localObj.record.get('EP_Position__latitude__s'));
    }

    static testMethod void setLatitude_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.record = database.query('SELECT Id , EP_Position__c,EP_Position__Latitude__s FROM Account WHERE Id=\''+accountObj.Id+'\'');
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.latitude = 1;
        localObj.addressobj = addressWrapperObj;
        localObj.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        Test.startTest();
        localObj.setLatitude();
        Test.stopTest();
        System.assertEquals(1.0,localObj.record.get('BillingLatitude'));
    }

    static testMethod void setLongitude_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.record = database.query('SELECT Id , EP_Position__c FROM Account WHERE Id=\''+accountObj.Id+'\'');
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.longitude = 3.0;
        localObj.addressobj = addressWrapperObj;
        localObj.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
        Test.startTest();
        localObj.setLongitude();
        Test.stopTest();
        System.assertEquals(3.0,localObj.record.get('BillingLongitude'));
    }

    static testMethod void removeNullString_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.street1 = '';
        addressWrapperObj.City = accountObj.billingcity;
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        localObj.removeNullString();
        Test.stopTest();
        System.assertNotEquals(null,localObj.addressobj.City);
        System.assertEquals(EP_Common_Constant.BLANK,localObj.addressobj.Street1);
    }

    static testMethod void getAddressObject_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.address = 'street_number:1,route:testRoute,locality:testlocatilty,administrative_area_level_1:testadministrativearealevel1,Country Code:AU,postal_code:12345678,Country:india,lat:1.0,lng:3.0';
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.oldAddress = 'oldAddressBillingAddress';
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        EP_AddressControllerContext.AddressWrapper result = localObj.getAddressObject();
        Test.stopTest();
        System.assertNotEquals(null,result);
    }

    static testMethod void getAddressAsString_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.street1 = 'test1';
        addressWrapperObj.Street2 = 'test2';
        addressWrapperObj.City = accountObj.billingcity;
        addressWrapperObj.zipCode = accountObj.billingpostalCode;
        addressWrapperObj.state = accountObj.billingState;
        addressWrapperObj.Country = accountObj.billingcountry;
        addressWrapperObj.latitude = 1;
        addressWrapperObj.longitude = 2;
        addressWrapperObj.cntryCode = 'AU';
        addressWrapperObj.oldAddress = 'oldAddressBillingAddress';
        addressWrapperObj.addressFieldLabel ='BillingAddress'; 
        addressWrapperObj.addressFieldAPIName = 'BillingAddress';
        addressWrapperObj.countryRecord = [SELECT Id , Name FROM EP_Country__c LIMIT 1];
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        String result = localObj.getAddressAsString();
        Test.stopTest();
        System.assertNotEquals(null,result);
    }

    static testMethod void isValidCountry_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.country = [SELECT Id , Name FROM EP_Country__c LIMIT 1].Name;
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        boolean  result = localObj.isValidCountry();
        Test.stopTest();
        System.assertEquals(true,result);
    }

    static testMethod void isValidCountry_NegativeTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.country = null;
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        boolean  result = localObj.isValidCountry();
        Test.stopTest();
        System.assertEquals(false,result);
    }

    static testMethod void isAddressFromValidCountry_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        localObj.countryCode = [SELECT Id , Name , EP_Country_Code__c FROM EP_Country__c LIMIT 1].EP_Country_Code__c;
        localObj.enteredCntryCode = localObj.countryCode ;
        localObj.isChangeRequest = false;
        addressWrapperObj.country = null;
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        boolean  result = localObj.isAddressFromValidCountry();
        Test.stopTest();
        System.assertEquals(true,result);
    }

    static testMethod void isAddressFromValidCountry_NegativeTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        localObj.countryCode = [SELECT Id , Name , EP_Country_Code__c FROM EP_Country__c LIMIT 1].EP_Country_Code__c;
        localObj.enteredCntryCode = 'IND' ;
        localObj.isChangeRequest = false;
        addressWrapperObj.country = null;
        localObj.addressobj = addressWrapperObj;
        Test.startTest();
        boolean  result = localObj.isAddressFromValidCountry();
        Test.stopTest();
        System.assertEquals(false,result);
    }

    static testMethod void commitAddressData_ChangeRequiredTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        accountObj.EP_Account_Name_2__c = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.record = accountObj;
        localObj.recordId = accountObj.id;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.isChangeRequest =True;
        Test.startTest();
        pagereference  result = localObj.commitAddressData();
        Test.stopTest();
        System.assertEquals(True,result.getURL().contains(accountObj.id));
    }

    static testMethod void commitAddressData_ChangeNotRequiredTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        accountObj.EP_Account_Name_2__c = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.record = accountObj;
        localObj.recordId = accountObj.id;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.isChangeRequest =false;
        Test.startTest();
        pagereference  result = localObj.commitAddressData();
        Test.stopTest();
        Account accObj = [SELECT Id , EP_Account_Name_2__c FROM Account WHERE Id =: localObj.recordId];
        System.assertEquals(EP_Common_Constant.ACCOUNT_OBJ,accObj.EP_Account_Name_2__c);
        System.assertEquals(True,result.getURL().contains(accountObj.id));
    }


    static testMethod void insertRequestAndRequestLine_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        List<EP_ChangeRequestLine__c> List1changeRequestLine = [SELECT Id FROM EP_ChangeRequestLine__c];
        Test.startTest();
        localObj.insertRequestAndRequestLine();
        Test.stopTest();
        List<EP_ChangeRequestLine__c> List2changeRequestLine = [SELECT Id FROM EP_ChangeRequestLine__c];
        System.assertNotEquals(List1changeRequestLine.size(),List2changeRequestLine.size());
    }

    static testMethod void createRequestInstance_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        List<EP_ChangeRequestLine__c> List1changeRequestLine = [SELECT Id FROM EP_ChangeRequestLine__c];
        Test.startTest();
        EP_ChangeRequest__c  result = localObj.createRequestInstance();
        Test.stopTest();
        List<EP_ChangeRequestLine__c> List2changeRequestLine = [SELECT Id FROM EP_ChangeRequestLine__c];
        System.assertNotEquals(Null,result);
    }

    static testMethod void createRequestLineInstance_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        EP_ChangeRequest__c request = localObj.createRequestInstance();
        DataBase.Insert(request);
        Test.startTest();
        EP_ChangeRequestLine__c result = localObj.createRequestLineInstance(request.id);
        Test.stopTest();
        System.assertNotEquals(Null,result);
    }

    static testMethod void setAddressWrapperObject_Test() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);

        localObj.record = database.query('Select Id from Account where Id=\''+accountObj.Id+'\'');

        localObj.mTypeField.put(EP_Common_Constant.STREET_STR,'BillingStreet');
        localObj.mTypeField.put(EP_Common_Constant.CITY_STR,'BillingCity');
        localObj.mTypeField.put(EP_Common_Constant.POST_CODE,'BillingPostalCode');
        localObj.mTypeField.put(EP_COMMON_CONSTANT.Country.toUpperCase(),'BillingCountry');
        localObj.mTypeField.put(EP_Common_Constant.STATE_STR,'BillingState');
        localObj.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        localObj.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        localObj.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
        localObj.mTypeField.put(ISADDRVALID,'EP_Is_Valid_Address__c');

        EP_AddressControllerContext.AddressWrapper addressWrapperObj = new EP_AddressControllerContext.AddressWrapper();
        addressWrapperObj.street1 = '';
        addressWrapperObj.Street2 = '';
        addressWrapperObj.City = accountObj.billingcity;
        addressWrapperObj.zipCode = accountObj.billingpostalCode;
        addressWrapperObj.state = accountObj.billingState;
        addressWrapperObj.latitude = 1;
        addressWrapperObj.longitude = 2;
        addressWrapperObj.cntryCode = '';
        addressWrapperObj.oldAddress = 'oldAddressBillingAddress';
        addressWrapperObj.countryRecord = [SELECT Id FROM EP_Country__c LIMIT 1];
        localObj.addressobj = addressWrapperObj;
        localObj.addressFieldLbl = 'BillingAddress';
        localObj.addressFieldApi = 'BillingAddress';
        localObj.isCountryReference = true;
        Test.startTest();
        localObj.setAddressWrapperObject();
        Test.stopTest();
        System.AssertEquals('BillingAddress',localObj.addressObj.addressFieldLabel);
        System.AssertEquals(null,localObj.addressObj.Country);
    }

    static testMethod void setContextVlaues_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        EP_Address_Fields__mdt addressFieldsObj = [SELECT Id, EP_Field_Type__c , EP_Field_Label__c , EP_Field_API_Name__c FROM EP_Address_Fields__mdt WHERE EP_Field_Type__c =:'Address CR Field'LIMIT 1];
        Test.startTest();
        localObj.setContextVlaues(addressFieldsObj);
        Test.stopTest();
        System.assertEquals(addressFieldsObj.EP_Field_Label__c, localObj.addressFieldLbl);
    }

    static testMethod void setContextVlaues_IsContryRefTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        EP_Address_Fields__mdt addressFieldsObj = [SELECT Id, EP_Field_Type__c , EP_Field_Label__c , EP_Field_API_Name__c , EP_IsReferenced__c FROM EP_Address_Fields__mdt LIMIT 1];
        Test.startTest();
        localObj.setContextVlaues(addressFieldsObj);
        Test.stopTest();
        System.assertEquals(false, localObj.isCountryReference );
        //EP_IsReferenced__c is always return FALSE
    }

    static testMethod void setContextVlaues_FillmTypeFieldTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        localObj.recordTypeName = EP_Common_Constant.VMI_SHIP_TO_DEV_NAME;
        localObj.ObjectName = EP_Common_Constant.ACCOUNT_OBJ;
        EP_Address_Fields__mdt addressFieldsObj = [SELECT Id, EP_Field_Type__c , EP_Field_Label__c , EP_Field_API_Name__c , EP_IsReferenced__c FROM EP_Address_Fields__mdt LIMIT 1];
        Test.startTest();
        localObj.setContextVlaues(addressFieldsObj);
        Test.stopTest();
        System.assertEquals(true, localObj.mTypeField.size() > 0 );
    }

    static testMethod void isCRAddressField_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        Test.startTest();
        Boolean result = localObj.isCRAddressField('Address CR Field');
        Test.stopTest();
        System.assert(result);
    }
    static testMethod void isCRAddressField_NegativeTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        Test.startTest();
        Boolean result = localObj.isCRAddressField('test');
        Test.stopTest();
        System.assert(!result);
    }

    static testMethod void isCountryReferenced_PositiveTest() {
        Account accountObj = EP_TestDataUtility.getSellTo();
        Map<String, String> mapOfURLParameters = new Map<String, String>();
        mapOfURLParameters.put(EP_Common_Constant.ID,accountObj.id);
        mapOfURLParameters.put(EP_Common_Constant.CNTRY_CODE,CNTRYCODESTR);
        mapOfURLParameters.put(CRSTR,EP_Common_Constant.YES);
        mapOfURLParameters.put(ADDRESSSTR,EP_Common_Constant.SHIP_TO_ADDRESSES);
        EP_AddressControllerContext localObj = new EP_AddressControllerContext(mapOfURLParameters);
        EP_Address_Fields__mdt addressFieldsObj = [SELECT Id, EP_Field_Type__c , EP_Field_Label__c , EP_Field_API_Name__c , EP_IsReferenced__c FROM EP_Address_Fields__mdt WHERE EP_Field_Type__c = :EP_COMMON_CONSTANT.COUNTRY LIMIT 1];
        Test.startTest();
        Boolean result = localObj.isCountryReferenced(addressFieldsObj);
        Test.stopTest();
        System.assert(!result);
        //EP_IsReferenced__c is always return FALSE
    }

}