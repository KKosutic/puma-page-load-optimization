public class EP_TankDipsDomainObject {

    public void insertPlaceHolderTankDips(Account account){
        EP_GeneralUtility.Log('Public','EP_TankDipsDomainObject','insertPlaceHolderTankDips');
        //Logic for insertion of the placeholder dips dips 
        List<EP_Tank_Dip__c> lPlaceholderTankDipsToBeInserted = new List<EP_Tank_Dip__c>();
        List<EP_Tank__c> lOperationalTanks = [SELECT ID, EP_Unit_Of_Measure__c, EP_Last_Place_Dip_Reading_Date_Time__c, 
        EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c 
        FROM EP_Tank__c WHERE EP_Ship_To__c =: account.Id
        AND EP_Tank_Status__c = :EP_Common_Constant.TANK_OPERATIONAL_STATUS
        AND EP_Tank_Dip_Entry_Mode__c = :EP_Common_Constant.SHIP_TO_TANK_DIP_PORTAL_ENTRY_MODE
        ];
        if (!lOperationalTanks.isEmpty()) {
            for(EP_Tank__c currentTank : lOperationalTanks){
                    // Only create a placeholder tank dip if there is not one for today
                    if (Date.valueOf(currentTank.EP_Last_Place_Dip_Reading_Date_Time__c) <> Date.valueOf(currentTank.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c)) {
                        lPlaceholderTankDipsToBeInserted.add(EP_PortalLibClass_R1.createPlaceholderTankDipRecord(currentTank));
                    } 
                } 
            } 
            if( !lPlaceholderTankDipsToBeInserted.isEmpty()) {
                Database.insert(lPlaceholderTankDipsToBeInserted);
            }
        }
        
        public void deletePlaceHolderTankDips(Account account){
            EP_GeneralUtility.Log('Public','EP_TankDipsDomainObject','deletePlaceHolderTankDips');
        //Logic for deletion of placeholder tank dips
        Set<ID> setTankIdsToDelete = new Set<ID>();
        List<EP_Tank_Dip__c> lOfRecordToDelete = new List<EP_Tank_Dip__c>();
        
        Map<Id, EP_Tank__c> mapInactiveShipTos = new Map<Id, EP_Tank__c>([SELECT ID,EP_Ship_To__c FROM EP_Tank__c WHERE EP_Ship_To__c =: account.Id ]);
        if(!mapInactiveShipTos.isEmpty()){
            setTankIdsToDelete = mapInactiveShipTos.keySet();
        }
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
        if(!setTankIdsToDelete.isEmpty()){
            lOfRecordToDelete = [SELECT EP_Ambient_Quantity__c,RecordType.id, CreatedDate, EP_Tank__c 
            FROM EP_Tank_Dip__c
            WHERE EP_Tank__c IN : setTankIdsToDelete 
            AND RecordTypeId =: recType 
            AND EP_Tank_Dip_Entered_Today__c = TRUE
            ];
            if(!lOfRecordToDelete.isEmpty()) {
                Database.delete(lOfRecordToDelete, FALSE);
            } // End deletion list check
        }
    }
}