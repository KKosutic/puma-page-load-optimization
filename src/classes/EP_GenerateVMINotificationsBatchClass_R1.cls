/********************************************************************************************************************
@ File Name...........:: EP_GenerateVMINotificationsBatchClass_R1
@ File Type...........:: Apex Batch Class
@ created By..........:: Accenture
@ Description.........:: This class runs in evry hour.
						 This class is sending notification for missing tank dips to customer and also creating blank 
						 tank dips.
*********************************************************************************************************************
@version..............:: V 1.0
*********************************************************************************************************************/
global class EP_GenerateVMINotificationsBatchClass_R1 implements Database.Batchable<sObject> {
    
    /*
    	Start Method 
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Database.QueryLocator ql = EP_GenerateVMINotfsBatchHelper_R1.retrieveVMIShipToAccountsForCompliance();
        return ql;
    }
    
    /*
    	send notification to customer if record found
    */
    global void execute(Database.BatchableContext BC, List<EP_Tank__c> scope) {
        EP_GenerateVMINotfsBatchHelper_R1.checkShipToAccountsForCompliance(scope);    
    }
    
    /*
    Finish method 
    Write post batch execution operation here
    */
    global void finish(Database.BatchableContext BC){
    
    }

}