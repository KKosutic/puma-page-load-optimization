@isTest
public class EP_PortalOrderHelper_UT
{

static testMethod void validateOverdueInvoice_PositiveScenariotest() {
    csord__Order__c ordObj = EP_TestDataUtility.getSalesOrderWithInvoiceOverdue();
    EP_AccountMapper accMapperObj = new EP_AccountMapper(); 
    Account billToAccount = accMapperObj.getAccountRecordById(ordObj.EP_Bill_To__c);
    String currentRecordId = billToAccount.Id;
    Test.startTest();
    Boolean result = EP_PortalOrderHelper.validateOverdueInvoice(currentRecordId);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void validateOverdueInvoice_NegativeScenariotest(){
    csord__Order__c ordObj = EP_TestDataUtility.getSalesOrderWithInvoiceOverdue();
    EP_AccountMapper accMapperObj = new EP_AccountMapper(); 
    Account billToAccount = accMapperObj.getAccountRecordById(ordObj.EP_Bill_To__c);
    String currentRecordId = billToAccount.Id;
    EP_Invoice__c invoiceObj = [Select Id, EP_Invoice_Due_Date__c, EP_Remaining_Balance__c, EP_Overdue__c FROM EP_Invoice__c][0];
    invoiceObj.EP_Invoice_Due_Date__c = System.today();
    invoiceObj.EP_Remaining_Balance__c = 0;
    Update invoiceObj;
    Test.startTest();
    Boolean result = EP_PortalOrderHelper.validateOverdueInvoice(currentRecordId);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void returnLocalSupportNumbers_test() {
    Test.startTest();
    List<EP_PortalOrderHelper.SupportNumberClass> result = EP_PortalOrderHelper.returnLocalSupportNumbers();
    Test.stopTest();
    System.AssertEquals(result.size(), 0);
}
static testMethod void returnLocalSupportAddress_test() {
    Test.startTest();
    EP_PortalOrderHelper.SupportAddressClass result = EP_PortalOrderHelper.returnLocalSupportAddress();
    Test.stopTest();
    System.Assert(String.isBlank(result.supportStreetName));
}
}