/** 
   @Author          Accenture
   @name            EP_RouteMapper
   @CreateDate      12/29/2016
   @Description     This class contains all SOQLs related to Route Object
   @Version         1.0
   @reference       NA
*/

    public with sharing class EP_RouteMapper {
        
    /**
    * @author       Accenture
    * @name         getRecordsByStorageLocation
    * @date         12/28/2016
    * @description  This method returns Returns Route records by Storagelocation .
    * @param        set<String>, Integer
    * @return        list<EP_Route__c>
    */  
     public list<EP_Route__c> getRecordsByStorageLocation(set<String> stringSet, Integer limitVal) {
        list<EP_Route__c> routeList = new list<EP_Route__c>(); 
        for (List<EP_Route__c> routeObjList: [SELECT id , EP_Storage_Location__c FROM EP_Route__c WHERE EP_Storage_Location__c IN :stringSet]) {
                routeList.addAll(routeObjList);
        }       
        return routeList; 
    }
        
    /**
    * @author       Accenture
    * @name         getRecordsByStorageLocation
    * @date         12/28/2016
    * @description  This method returns Returns Route records by Storage Location Id.
    * @param        set<String>
    * @return       list<EP_Route__c>
    */      
    public list<EP_Route__c> getRecordsByStorageLocation(set<Id> setIds) {
        list<EP_Route__c> routeList = new list<EP_Route__c>(); 
        for (List<EP_Route__c> routeObjList: [SELECT id,Name,EP_Storage_Location__c FROM EP_Route__c WHERE EP_Storage_Location__c IN :setIds]) {
                routeList.addAll(routeObjList); 
        }       
        return routeList; 
    }
    
    /**
    * @author       Accenture
    * @name         getRoutesWithOrdersForNameCodeChange
    * @date         05/05/2017
    * @description  This method returns Route records assoicated with order records(except cancelled order, For Route Name/Code Change)
    * @param        set<Id>
    * @return       Map<Id,List<Order>>
    */      
    public Map<Id,List<Order>> getRoutesWithOrdersForNameCodeChange(set<Id> setIds) {
     Map<Id,List<Order>> routesWithOrdersMap = new Map<Id,List<Order>>();
        for(Order ord : [SELECT Id,Status,EP_Route__c FROM Order WHERE EP_Route__c IN : setIds AND Status != : EP_OrderConstant.CANCELLED_STATUS ]){
        if(routesWithOrdersMap.containsKey(ord.EP_Route__c)) {
            routesWithOrdersMap.get(ord.EP_Route__c).add(ord); 
        }
        else{
            routesWithOrdersMap.put(ord.EP_Route__c,new List<Order>{ord});
        }
     }
     return routesWithOrdersMap;
    }
    
     /**
    * @author       Accenture
    * @name         getAssociatedOrdersWithRoute
    * @date         05/05/2017
    * @description  This method returns Route records assoicated with order records ( For Deletion of Route functionality)
    * @param        set<Id>
    * @return       Map<Id,List<Order>>
    */      
    public Map<Id,List<Order>> getAssociatedOrdersWithRoute(set<Id> setIds) {
      Map<Id,List<Order>> routesWithOrdersMap = new Map<Id,List<Order>>();
          for(Order ord : [SELECT Id,Status,EP_Route__c FROM Order WHERE EP_Route__c IN : setIds AND EP_Run__c != NULL]){
        if(routesWithOrdersMap.containsKey(ord.EP_Route__c)) {
            routesWithOrdersMap.get(ord.EP_Route__c).add(ord); 
        }
        else{
            routesWithOrdersMap.put(ord.EP_Route__c,new List<Order>{ord});
        }
     }
     return routesWithOrdersMap;
    }

    
    /**
    * @author       Accenture
    * @name         getRouteRecordsById
    * @date         05/08/2017
    * @description  This method query and returns those records in Map based on route Ids
    * @param        set<Id>
    * @return       Map<Id, EP_Route__c>
    */   
    public Map<Id, EP_Route__c> getRouteRecordsById(Set<Id> routeIds){
        return new Map<Id, EP_Route__c>([SELECT Id, OwnerId, Name, CurrencyIsoCode, LastModifiedDate, 
                                                LastReferencedDate, EP_Status__c, EP_Storage_Location__c, 
                                                EP_Route_Code__c, EP_Description__c, EP_Route_Name__c, 
                                                EP_Company__c, EP_Max_Instance_Number__c, CreatedDate 
                                           FROM EP_Route__c
                                          Where Id in : routeIds]);
    }

    //Defect 58468--Start
    /**
    * @author       Accenture
    * @name         getRoutesWithAccountForStatusChange
    * @date         05/22/2017
    * @description  This method query and returns Account records in Map based on route Ids
    * @param        set<Id>
    * @return       Map<Id, Account>
    */ 
    public Set<Id> getRoutesWithAccountForStatusChange(Set<Id> routeIds) {

      Set<Id> defaultRouteSet = new Set<Id>();

      for (Account shipToAccount : [SELECT Id,Name,EP_Default_Route__c FROM Account WHERE EP_Default_Route__c=:routeIds]) {

        defaultRouteSet.add(shipToAccount.EP_Default_Route__c);
      }

      return defaultRouteSet;
    }
    //Defect 58468--End

}