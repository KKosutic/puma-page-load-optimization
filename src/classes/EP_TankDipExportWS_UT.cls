@isTest
public class EP_TankDipExportWS_UT
{   
	
	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
     
    static testMethod void retrieveTankDips_test() {
    	Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/TankDips';  
		req.httpMethod = EP_Common_Constant.GET;
		RestContext.request = req;
		RestContext.response= res;
   		EP_TankDipExportWS.retrieveTankDips();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    }
    
    static testMethod void updateTankDips_test() {
		EP_AcknowledgementStub stub = EP_TestDataUtility.createAcknowledgementStub();
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/TankDips';  
		req.httpMethod = EP_Common_Constant.POST;
		req.requestBody = Blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response= res;
   		EP_TankDipExportWS.updateTankDips();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    }  
}