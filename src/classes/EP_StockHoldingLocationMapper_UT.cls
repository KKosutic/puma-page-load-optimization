@isTest
public class EP_StockHoldingLocationMapper_UT
{
    static testMethod void getSellToSupplyOptionCount_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Account account = EP_TestDataUtility.getSellToPositiveScenario();
        Test.startTest();
        Integer result = localObj.getSellToSupplyOptionCount(account);
        Test.stopTest();
        System.AssertNotEquals(0,result);
    }
    static testMethod void getRecordsByIds_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Set<Id> idSet = new Set<Id>();
        list<EP_Stock_Holding_Location__c> listStockHoldingLocation = [SELECT Id FROM EP_Stock_Holding_Location__c];
        for(EP_Stock_Holding_Location__c stockHoldingLocationObj : listStockHoldingLocation){
            idSet.add(stockHoldingLocationObj.id);
        }
        Integer limitVal = 1;
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getRecordsByIds(idSet,limitVal);
        Test.stopTest();
        System.AssertEquals(idSet.size(),result.size());
    }
    static testMethod void getRecordsById1_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Account shipToAccountObject = EP_TestDataUtility.getShipToPositiveScenario();
        String stockHoldingId = [SELECT Id FROM EP_Stock_Holding_Location__c LIMIT 1].id;
        Test.startTest();
        EP_Stock_Holding_Location__c result = localObj.getRecordsById(stockHoldingId);
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }
    
    static testMethod void getCountofPrimaryLocations_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Set<id> idSet = new Set<id>{EP_TestDataUtility.getShipToPositiveScenario().id};
            Integer limitVal = 1;
        Test.startTest();
        LIST<AggregateResult> result = localObj.getCountofPrimaryLocations(idSet,limitVal);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getRecordsByIds_test2() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Set<Id> idSet = new Set<Id>();
        list<EP_Stock_Holding_Location__c> listStockHoldingLocation = [SELECT Id FROM EP_Stock_Holding_Location__c];
        for(EP_Stock_Holding_Location__c stockHoldingLocationObj : listStockHoldingLocation){
            idSet.add(stockHoldingLocationObj.id);
        }
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(idSet.size(),result.size());
    }
    static testMethod void getCountofPrimaryLocations1_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Set<id> idSet = new Set<id>{EP_TestDataUtility.getShipToPositiveScenario().id};
            Test.startTest();
        LIST<AggregateResult> result = localObj.getCountofPrimaryLocations(idSet);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    
    static testMethod void getStockHoldingLocationByIds_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Set<Id> setShipToIds = new Set<id>{EP_TestDataUtility.getShipToPositiveScenario().id};
            Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getStockHoldingLocationByIds(setShipToIds);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getActiveStockHoldingLocationBySellToId_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        String sellToId = EP_TestDataUtility.getSellToPositiveScenario().id;
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getActiveStockHoldingLocationBySellToId(sellToId);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getCountOfPrimaryStockHoldingLocations_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Account account = EP_TestDataUtility.getShipToPositiveScenario();
        Test.startTest();
        Integer result = localObj.getCountOfPrimaryStockHoldingLocations(account);
        Test.stopTest();
        System.AssertEquals(1,result);
    }
    static testMethod void getSHLByNavStockLocationId_test() { 
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        EP_AccountDomainObject accountDomainObj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        EP_Stock_Holding_Location__c stockHoldingLocationObj = [SELECT Id, Stock_Holding_Location__r.EP_Puma_Company_Code__c,Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c 
                                                                FROM EP_Stock_Holding_Location__c];
        Set<string> setStockLocationId = new Set<string>{stockHoldingLocationObj.Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c};
        
        Test.startTest();
        List<EP_Stock_Holding_Location__c> result = localObj.getSHLByNavStockLocationId(setStockLocationId , stockHoldingLocationObj.Stock_Holding_Location__r.EP_Puma_Company_Code__c );
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getStockLocationByIdForShipTo_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        EP_AccountDomainObject shipToAccDomainObj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectPositiveScenario();
        Account shipTo = shipToAccDomainObj.getaccount();
        EP_Stock_Holding_Location__c stockLocObj =[select Id,EP_Stock_Location_Id__c from EP_Stock_Holding_Location__c where EP_Ship_To__c=:shipTo.Id];
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getStockLocationByIdForShipTo(stockLocObj.EP_Stock_Location_Id__c,shipTo.Id);
        Test.stopTest();
        System.AssertEquals(true, result.size() > 0);
    }
    
    static testMethod void getStockHoldingLocations_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        EP_AccountDomainObject accountDomainObj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        EP_Stock_Holding_Location__c stockHoldingLocationObj = [SELECT Id, Stock_Holding_Location__r.EP_Puma_Company_Code__c,Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c 
                                                                FROM EP_Stock_Holding_Location__c];
        Set<string> setStockLocationId = new Set<string>{stockHoldingLocationObj.Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c};
        
        Test.startTest();
        Map<String,EP_Stock_Holding_Location__c> result = localObj.getStockHoldingLocations(setStockLocationId , stockHoldingLocationObj.Stock_Holding_Location__r.EP_Puma_Company_Code__c );
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getStockHoldingLocation_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Id sellToId = EP_TestDataUtility.getSellToPositiveScenario().id;
        EP_Stock_Holding_Location__c obj = [SELECT Id , EP_Sell_To__c FROM EP_Stock_Holding_Location__c WHERE EP_Sell_To__c =:sellToId ];
        Test.startTest();
        EP_Stock_Holding_Location__c  result = localObj.getStockHoldingLocation(obj.Id );
        Test.stopTest();
        System.AssertNotEquals(null , result);
    }
    //L4_45352_Start
    static testMethod void getPickupEnabledLocationBySellToId_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Id sellToId = EP_TestDataUtility.getSellToPositiveScenario().id;
        EP_Stock_Holding_Location__c obj = [SELECT Id , EP_Sell_To__c FROM EP_Stock_Holding_Location__c WHERE EP_Sell_To__c =:sellToId ];
        Test.startTest();
        List<EP_Stock_Holding_Location__c>  result = localObj.getPickupEnabledLocationBySellToId(sellToId);
        Test.stopTest();
        System.Assert(result.size()> 0 );
    }
    static testMethod void getPrimaryStockLoactionsForShipTos_test() {
        EP_StockHoldingLocationMapper localObj = new EP_StockHoldingLocationMapper();
        Account shipToId = EP_TestDataUtility.getShipToPositiveScenario();
        EP_Stock_Holding_Location__c obj = [SELECT ID, Name, EP_Ship_To__c,Stock_Holding_Location__c,EP_Ship_To__r.ParentId,EP_Location_Type__c
                                                                FROM EP_Stock_Holding_Location__c
                                                                WHERE EP_Location_Type__c = :EP_Common_Constant.PRIMARY
                                                                    AND EP_Ship_To__c = :shipToId.id];
        Test.startTest();
        List<EP_Stock_Holding_Location__c>  result = localObj.getPrimaryStockLoactionsForShipTos(new List<Account>{shipToId});
        Test.stopTest();
        System.Assert(result.size()> 0 );
    }
    //L4_45352_end
}