@isTest
private class EP_TankMapper_UT {

    static testMethod void getRecordsByShipToIds_test() {
      EP_TankMapper localObj = new EP_TankMapper();
      set<Id> setShipToIds = new Set<Id>{EP_TestDataUtility.getShipToPositiveScenario().Id};
      Test.startTest();
      list<EP_Tank__c> result = localObj.getRecordsByShipToIds(setShipToIds);
      Test.stopTest();
      System.Assert(result.size() > 0);
    }

    static testMethod void getOperationalRecords_test() {
      EP_TankMapper localObj = new EP_TankMapper();
      set<Id> setShipToIds = new Set<Id>{EP_TestDataUtility.getShipToPositiveScenario().Id};
      system.debug('setShipToIds'+setShipToIds);
      List<EP_Tank__c> tanklist = [Select id,EP_Ship_To__c,ep_tank_status__c from EP_Tank__c];
      system.debug('%%%%tanklist'+tanklist);
      tanklist[0].EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
      tanklist[0].EP_Tank_Dip_Entry_Mode__c = EP_Common_Constant.SHIP_TO_TANK_DIP_PORTAL_ENTRY_MODE;
      update tanklist;
      Test.startTest();
      list<EP_Tank__c> result = localObj.getOperationalRecords(setShipToIds);
      Test.stopTest();
      System.Assert(result.size() > 0);
    }  


    static testMethod void getOperationalRecordsByShipTo_test() {
      Account acc = EP_TestDataUtility.getShipToPositiveScenario();
      EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(acc);
      tank.EP_Tank_Status__c = 'Operational';
      update tank;
      Test.startTest();
      Map<Id,EP_Tank__c> result = EP_TankMapper.getOperationalRecordsByShipTo(acc.Id);
      Test.stopTest();
      System.Assert(result.size() > 0);
    }         

    static testMethod void getRecordsByShipToIdsList_test(){
       Account acc = EP_TestDataUtility.getShipToPositiveScenario();
       List<Account> accList = new List<Account>{acc};
       EP_TankMapper localObj = new EP_TankMapper();
       Test.startTest();
       List<EP_Tank__c> tankList = localObj.getRecordsByShipToIdsList(accList);
       Test.stopTest();
       System.Assert(tankList.size() > 0);
    }

    static testMethod void getRecordsByShipTo_test(){
       Account acc = EP_TestDataUtility.getShipToPositiveScenario();
       Test.startTest();
       Map<Id,EP_Tank__c> tankList = EP_TankMapper.getRecordsByShipTo(acc.id);
       Test.stopTest();
       System.Assert(tankList!=null);
    }
}