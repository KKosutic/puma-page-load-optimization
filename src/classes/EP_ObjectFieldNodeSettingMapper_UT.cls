@isTest
public class EP_ObjectFieldNodeSettingMapper_UT
{
    static testMethod void getRecordsByExternalSystem_Test() {
        EP_ObjectFieldNodeSettingMapper objectFieldNodeSettingMapperObj = new EP_ObjectFieldNodeSettingMapper();
        Test.startTest();
        list<EP_ObjectFieldNodeSetting__mdt> result = objectFieldNodeSettingMapperObj.getRecordsByExternalSystem(EP_Common_Constant.ALL,1);
        Test.stopTest();
        System.AssertNotEquals(result.size(), 0);
    }

    static testMethod void getRecByNodeName_Test() {
        EP_ObjectFieldNodeSettingMapper objectFieldNodeSettingMapperObj = new EP_ObjectFieldNodeSettingMapper();
        Test.startTest();
        list<EP_ObjectFieldNodeSetting__mdt> result = objectFieldNodeSettingMapperObj.getRecByNodeName(1);
        Test.stopTest();
        System.AssertNotEquals(result.size(), 0);
    }    
}