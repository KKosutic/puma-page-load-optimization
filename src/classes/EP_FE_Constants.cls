/* 
@Author <Soumya Raj>
@name <EP_TestSellToResponse>
@CreateDate <12/04/2016>
@Description <List of constants>  
@Version <1.0>
*/
global with sharing class EP_FE_Constants {
    
    //Bootstrap API
    public static final String FE_PROJECT_CODENAME = 'PE_Front_End';
    public static final String FE_DICTIONARY = '[{"EP_FE_Welcome": "You are Welcome!"},{"EP_FE_Logout": "Logout"}]';
    public static final Integer SQL_LIMIT= 50000;
    public static final String ACCOUNT_OBJECT = 'Account';
    public static final String TANK_OBJECT = 'EP_Tank__c';
    public static final String SYSTEM_ADMIN= 'System Administrator';
    public static final String APP_DYNAMICS_KEY = 'AppDynamics_Key';
    
    // ACCOUNT Constants
    public static final String SHIPTO_ACCOUNT_RECORD_TYPE_NAME = 'Non-VMI Ship To';
    public static final String STOCKHOLDING_LOCATION_RECORD_TYPE_NAME = 'Storage Location';
    public static final String STATUS_BASIC_DATA_SETUP = '02-Basic Data Setup';
    public static final String STATUS_KYC_REVIEW = '03-KYC Review';
    public static final String STATUS_ACCOUNT_SET_UP = '04-Account Set-up';
    public static final String STATUS_ACTIVE = '05-Active';
    //public static final String VENDOR_DEV_NAME = 'EP_Vendor';
    public static final String VENDOR_DEV_NAME = 'Vendor';//Prayank
    public static final String VENDOR_TYPE_TRANSPORTER = 'Transporter';
    
    // Order Constants 
    public static final String ORDER_NEW_STATUS = 'New';
    global static final String ORDER_INVOICED_STATUS = 'Invoiced';
    global static final String ORDER_QUOTE_STATUS = 'Quote';
    global static final String ORDER_AWAITING_APPROVAL_STATUS = 'Awaiting Approval';
    global static final String ORDER_PRICE_TYPE_DELIVERY = 'Fuel and Freight Price';
    global static final String ORDER_PRICE_TYPE_EXRACK = 'Fuel Price';
    global static final String ORDER_DELETED_STATUS = 'Deleted';
    global static final String ORDER_TYPE = 'Non-Consignment';
    
    //Order Pricing Contstants
    global static final String ORDER_PRICING_STATUS_PRICED = 'PRICED';
    global static final String ORDER_CHARGE_TYPE_FUEL = 'Fuel Price';
    global static final String ORDER_CHARGE_TYPE_TAX = 'Tax';
    global static final String ORDER_CHARGE_TYPE_FREIGHT = 'Freight';
    global static final String ORDER_CHARGE_TYPE_DISCOUNT = 'Discount';
    global static final String ORDER_RECORD_TYPE_CONSUMPTION = 'EP_Consumption_Orders';
    
    //Pricing engine constants
    global static final String PRICING_ENGINE_TURNED_OFF = 'Pricing_Engine_Turned_off';
    
    // User constants
    global static final String USER_TYPE_STANDARD = 'Standard';
    
    // Business Hours Constant
    public static final String DEFAULT_BUSINESS_HOURS_NAME ='Default';
    
    // Profile configuration Key names
    public static final String CONFIG_PROFILE_NAME = 'Profile Name';
    public static final String PROFILE_BASIC_ACCESS = 'Basic_Access';
    public static final String PROFILE_BASIC_ORDERING = 'Basic_Ordering';
    public static final String PROFILE_MANAGE_ACCESS = 'Manage_Access';
    public static final String PROFILE_FINANCE_ACCESS = 'Finance_Access';
    public static final String PROFILE_SUPER_USER = 'Super_User';
    
    //Temp profiles
    //TODO: remove when done
    //public static final String EP - Retail VMI Site User_R1_FRONTEND
    public static final String PROFILE_SYSTEM_ADMINISTRATOR = 'System Administrator';
    public static final String PROFILE_COMMUNITY_TESTER = 'EP - Retail VMI Site User_R1_FRONTEND';
    
    public static final String CONFIG_FIELDSET = 'Fieldset';
    public static final String CONFIG_BOOTSTRAP = 'Bootstrap';
    public static final String CONFIG_KEY = 'key';
    public static final String CONFIG_FAQTOPIC = 'FAQ Topic';
    
    // Error Log EndPoint
    public static final String ERROR_CLASS_NAME ='EP_FE_ErrorLogEndpoint';
    public static final String ERROR_CLASS_METHOD_NAME ='logError';
    public static final String ERROR_API_NAME ='ErrorLogEndpoint';
    public static final String ERROR_API_EXCEPTION ='Unknown';
    public static final String ERROR_API_EXCEPTION_TYPE ='Unknown';
    
    public static final String ATTACHMENT_BASE_URL = '/servlet/servlet.FileDownload?file=';
    
    public static final String RT_CREDIT_MEMO = 'EP_Credit_Memo';
    public static final String RT_CUSTOMER_INVOICE = 'EP_Customer_Invoice';
    public static final String RT_PAYMENT_IN = 'EP_Payment_In';
    public static final String RT_PAYMENT_OUT = 'EP_Payment_Out';
    
    //Invoice Constants
    
    public static final String SALES_INVOICE = 'EP_Sales_Invoice';
    public static final String PRO_FORMA_INVOICE = 'EP_Pro_Forma_Invoice';
    // Supply Location Constants
    
    public static final String DELIVERY_SUPPLY_LOCATION = 'Delivery_Supply_Location';
    public static final String EXRACK_SUPPLY_LOCATION = 'Ex_Rack_Supply_Location';
    
    public static final String PAYMENT_LABEL = 'Payment';
    
    // Tanks Constants
    public static final String TANK_STATUS_OPERATIONAL = 'Operational';
    
    
    // Pricing Constants
    
    public static final String PRICING_REQUEST_SENT = 'PRICING REQUEST SENT';
    
    //PowerBI constant
    public static final String CONTENTTYPE = 'Content-Type';
    public static final String APPLICATIONJSON  = 'application/json';
    public static final String APPLICATIONENCODED = 'application/x-www-form-urlencoded';
    public static final String AMPERSAND = '&';
    public static final String ACCEPT= 'accept';
    public static final String EQUAL= '=';
    public static final String POST = 'POST';
    public static final String ApplicationName = 'PowerBI';
    public static final String Authorization = 'Authorization';
    public static final String GRANT_TYPE= 'grant_type';
    public static final String PBI_CLIENTID= 'client_id';
    public static final String RESOURCE = 'resource';
    public static final String USERNAME = 'username';
    public static final String PASSWORD = 'password';
    public static final String Groups = 'groups/';
    public static final String Reports = '/reports/';
    public static final String GENERATETOKEN = '/GenerateToken';
    
    //Compute permission
    public static final String PREPAY = 'prepay';
    public static final String POSTPAY = 'postpay';
    
    
}