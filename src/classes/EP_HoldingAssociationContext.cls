/***************************************************************
*  @Author 		Accenture                                      *
*  @Name 		EP_HoldingAssociationContext			       *
*  @CreateDate  9-Nov-2017                                     *
*  @Description This class is use as contex class for 
				EP_HoldingAssociationController 			   *	
*  @Version 	1.0                                            *
****************************************************************/
public with sharing class EP_HoldingAssociationContext {
	public list<EP_HoldingAssociationHelper.HoldingWrapper> lstHoldingWrapper {get;set;}
	public string strHoldingId{get;set;}
	
/****************************************************************
* @author       Accenture                                       *
* @name         getReport                					    *
* @description  This Method is used to retrive reocords
				to show on page                                 *
* @param        NA			                					*
* @return       NA                                   *
****************************************************************/      	
	public void getReport(){
		EP_HoldingAssociationHelper objHelper = new EP_HoldingAssociationHelper();
		lstHoldingWrapper =objHelper.getDetails(strHoldingId);
	}
}