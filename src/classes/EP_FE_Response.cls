/*   
     @Author <Accenture>
     @name <EP_Response.cls>     
     @Description <This class is used as parent response controller class>   
     @Version <1.1> 
*/
global abstract with sharing class EP_FE_Response {

    global static final Integer STATUS_SUCCESS = 0;
    global static final Integer STATUS_ERROR_APEX = -500;
    
    public Integer status {get;set;}
    public List<String> errors {get;set;}

    /*
     *  default constructor
     */
    public EP_FE_Response () {
        status = STATUS_SUCCESS;
        errors = new List<String>();
    }

    /*
     *  Add a single error string
     */
    public void addError(String error) {
        if(errors == null) {
            errors = new List<String>();
        }
        errors.add(error);
    }
}