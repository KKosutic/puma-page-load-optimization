@isTest
public class EP_NonVMIShipToASBlocked_UT
{
    static final string EVENT_NAME = '06-BlockedTo06-Blocked';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    static testMethod void doOnEntry_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObject().getAccount();
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assertEquals(EP_AccountConstant.ACTIVE,oldAccount.EP_Status__c );
    }
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
		system.assert(true);        
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        String message;
        try{            
            Boolean result = localObj.doTransition();
        }catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message)); 
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_NonVMIShipToASBlocked localObj = new EP_NonVMIShipToASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/

}