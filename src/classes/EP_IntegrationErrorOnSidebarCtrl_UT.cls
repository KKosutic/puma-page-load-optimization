/* 
  @Author <Spiros Markantonatos>
   @name <EP_IntegrationErrorOnSidebarCntroller_UT>
   @CreateDate <02/09/2016>
   @Description <This is the test class for EP_IntegrationRecordTrigger> 
   @Version <1.0>
*/
@isTest
public with sharing class EP_IntegrationErrorOnSidebarCtrl_UT {
    
    private static EP_IntegrationRecord__c testMiddlewareIntegrationRecord;
    private static EP_IntegrationRecord__c testTargetIntegrationRecord;
    private static EP_IntegrationRecord__c testFunctionalIntegrationRecord;
    private static EP_Country__c testCountry;
    
    private static final String MIDDLEWARE_TRANSPORT_ERROR_TYPE = 'Middleware Transport Error';
    private static final String TARGET_TRANSPORT_ERROR_TYPE = 'Target Transport Error';
    private static final String FUNCTIONAL_TRANSPORT_ERROR_TYPE = 'Functional Error';
    private static final String OBJECT_ID = '12345';
    
    /*
    * @ Description: This method will create the test integration records and the relevant test country record
    */
    private static void createTestIntegrationRecords() {
        testCountry = EP_TestDataUtility.createCountryRecord('TEST COUNTRY', 'AU', 'NORTH');
        Database.insert(testCountry);
        
        testMiddlewareIntegrationRecord = EP_TestDataUtility.createIntegrationRec(OBJECT_ID + '1', 
                                                                                    'Accounts', 
                                                                                        System.Now().addDays(-2), 
                                                                                            'AAF', 
                                                                                                'ERROR-SENT');
        testMiddlewareIntegrationRecord.EP_Country__c = testCountry.Id;
        testMiddlewareIntegrationRecord.EP_IsLatest__c = TRUE;
        Database.insert(testMiddlewareIntegrationRecord);
        
        testTargetIntegrationRecord = EP_TestDataUtility.createIntegrationRec(OBJECT_ID + '2', 
                                                                                    'Accounts', 
                                                                                        System.Now().addDays(-1), 
                                                                                            'AAF', 
                                                                                                'ERROR-RECEIVED');
        testTargetIntegrationRecord.EP_Country__c = testCountry.Id;
        testTargetIntegrationRecord.EP_IsLatest__c = TRUE;
        Database.insert(testTargetIntegrationRecord);
        
        testFunctionalIntegrationRecord = EP_TestDataUtility.createIntegrationRec(OBJECT_ID + '2', 
                                                                                    'Accounts', 
                                                                                        System.Now(), 
                                                                                            'AAF', 
                                                                                                'ERROR-SYNC');
        testFunctionalIntegrationRecord.EP_Country__c = testCountry.Id;
        testFunctionalIntegrationRecord.EP_IsLatest__c = TRUE;
        Database.insert(testFunctionalIntegrationRecord);
        
    }
    
    /*
    * @ Description: This method will test that the integration error type field is populated correctly based on the status of the integration record
    */
    private static testMethod void checkIntegrationRecordSetup(){
        
        createTestIntegrationRecords();
        
        testMiddlewareIntegrationRecord = [SELECT Id, EP_Integration_Error_Type__c 
                                            FROM EP_IntegrationRecord__c 
                                            WHERE Id = :testMiddlewareIntegrationRecord.Id];
        
        System.assertEquals(MIDDLEWARE_TRANSPORT_ERROR_TYPE, testMiddlewareIntegrationRecord.EP_Integration_Error_Type__c);
        
        testTargetIntegrationRecord = [SELECT Id, EP_Integration_Error_Type__c 
                                            FROM EP_IntegrationRecord__c 
                                            WHERE Id = :testTargetIntegrationRecord.Id];
        
        System.assertEquals(TARGET_TRANSPORT_ERROR_TYPE, testTargetIntegrationRecord.EP_Integration_Error_Type__c);
        
        testFunctionalIntegrationRecord = [SELECT Id, EP_Integration_Error_Type__c 
                                                FROM EP_IntegrationRecord__c 
                                                WHERE Id = :testFunctionalIntegrationRecord.Id];
        
        System.assertEquals(FUNCTIONAL_TRANSPORT_ERROR_TYPE, testFunctionalIntegrationRecord.EP_Integration_Error_Type__c);
    }
    
    /*
    * @ Description: This method will test the sidebar alert controller and ensure that:
            - It returns the correct alerts
            - User can dismiss manually an alert
    */
    private static testMethod void testReturnIntegrationAlerts() {
        createTestIntegrationRecords();
        
        // TO BE REMOVED ONCE THE DEFECT IS FIXED
        testMiddlewareIntegrationRecord.EP_Integration_Error_Type__c = MIDDLEWARE_TRANSPORT_ERROR_TYPE;
        testTargetIntegrationRecord.EP_Integration_Error_Type__c = TARGET_TRANSPORT_ERROR_TYPE;
        testFunctionalIntegrationRecord.EP_Integration_Error_Type__c = FUNCTIONAL_TRANSPORT_ERROR_TYPE;
        Database.update(testMiddlewareIntegrationRecord);
        Database.update(testTargetIntegrationRecord);
        Database.update(testFunctionalIntegrationRecord);
        
        EP_IntegrationErrorOnSidebarController con = new EP_IntegrationErrorOnSidebarController();
        
        // By default the middleware alert should be set to FALSE
        System.assertEquals(FALSE, con.middlewareIntegrationErrorBoolean);
        
        // Load all alerts
        con.loadIntegrationFrameworkErrors();
        
        // There should be 1 Middleware, 1 Functional and 1 Transport alert
        System.assertEquals(TRUE, con.middlewareIntegrationErrorBoolean);
        System.assertEquals(1, con.listFunctionalIntegrationErrors.size());
        System.assertEquals(1, con.listTransportIntegrationErrors.size());
        
        // Check that the system is updating the name of the object
        System.assertEquals('Account', con.listTransportIntegrationErrors[0].objectNameString);
        
        // The system should reset the middleware alert
        testMiddlewareIntegrationRecord = [SELECT ID, EP_Is_Notification_Dismissed__c 
                                            FROM EP_IntegrationRecord__c
                                            WHERE ID = :testMiddlewareIntegrationRecord.Id];
        System.assertEquals(TRUE, testMiddlewareIntegrationRecord.EP_Is_Notification_Dismissed__c);
    
        System.assertNotEquals(NULL, con.openMyErrorListView);
        
        // Dismiss one of the errors
        con.selectedIntegrationRecordString = testTargetIntegrationRecord.Id;
        con.dismissIntegrationError();
        
        // Reload alerts
        con.loadIntegrationFrameworkErrors();
        
        // There should be 1 Functional and 0 Transport alerts
        System.assertEquals(1, con.listFunctionalIntegrationErrors.size());
        System.assertEquals(0, con.listTransportIntegrationErrors.size());
    }
    
}