@isTest
public class EP_OrderMapper_UT {
    
    static testMethod void getOpenOrderOfTransporters_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        
        Set<id> idSet = new Set<id>{newOrder.EP_Transporter__c};
        Set<string> stringSet = new Set<String>{EP_OrderConstant.OrderState_Draft};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getOpenOrderOfTransporters(idSet,stringSet);
        Test.stopTest();
        
        System.Assert(true,result.size()==1);
    }
    static testMethod void getOrderListWithStandardOrderItemsDetails_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        Set<Id> mOrder = new Set<id>{newOrder.id};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            LIST<Order> result = localObj.getOrderListWithStandardOrderItemsDetails(mOrder);
        Test.stopTest();
        
        System.Assert(result.size() > 0);
    }
    static testMethod void getOrderPricing_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Order result = localObj.getOrderPricing(newOrder.Id);
        Test.stopTest();
        
        System.Assert(result <> null);
    }
    static testMethod void getBulkOrdersForAccount_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
        newOrder.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK;
		insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getBulkOrdersForAccount(newAccount.Id);
        Test.stopTest();
        
        System.Assert(result.size() > 0);
    }
    static testMethod void getOrderByAccountAndProductCategory_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
        newOrder.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK;
		insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getOrderByAccountAndProductCategory(newAccount.Id, newOrder.EP_Order_Product_Category__c, new Set<String>{newOrder.Status});
        Test.stopTest();
        
        System.Assert(result.size() == 0);
    }
    static testMethod void getCsOrderSumAmountbyBillTo_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        csord__Order__c OrderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        Test.startTest();
        List<AggregateResult> result = localObj.getCSOrderSumAmountbyBillTo(OrderObj);
        Test.stopTest();
        System.Assert(result.size() == 0);
    }
    static testMethod void getCsOrderWithStandardItems_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        Test.startTest();
        map<string, csord__Order__c> result = localObj.getCSOrderWithStandardItems(new set<string>{orderObj.OrderNumber__c});
        Test.stopTest();
        System.Assert(result.size() == 1);
    }
    
    static testMethod void getCSRecordsByIds_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCSRecordsByIds(new Set<Id>{orderObj.Id});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    static testMethod void getCsRecordsByOrderNumber_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCsRecordsByOrderNumber(new Set<String>{orderObj.OrderNumber__c});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    static testMethod void getCSOrderPricing_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            csord__Order__c result = localObj.getCSOrderPricing(orderObj.Id);
        Test.stopTest();
        
        System.Assert(result != null);
    }
    
    static testMethod void getOrderWithStandardItems_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        Order order = [SELECT Id, OrderNumber FROM Order WHERE Id =: newOrder.Id];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<string, Order> result = localObj.getOrderWithStandardItems(new Set<String>{order.OrderNumber});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    static testMethod void getCsOrderMapByOrderNumber_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<String, csord__Order__c> result = localObj.getCsOrderMapByOrderNumber(new Set<String>{orderObj.OrderNumber__c});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    static testMethod void getOrderPricingwithorderitems_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Order result = localObj.getOrderPricingwithorderitems(newOrder.Id);
        Test.stopTest();
        
        System.Assert(result != null);
    }
    
    static testMethod void getCsOrderByAccountAndProductCategory_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCsOrderByAccountAndProductCategory(newAccount.Id, orderObj.EP_Order_Product_Category__c, new Set<String>{orderObj.Status__c});
        Test.stopTest();
        
        System.Assert(result.size() == 0);
    }
    
    static testMethod void getOrderSumAmountbyBillTo_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<AggregateResult> result = localObj.getOrderSumAmountbyBillTo(newOrder);
        Test.stopTest();
        
        System.Assert(result.size() == 0);
    }
    
    static testMethod void getCsOrderListWithStandardOrderItemsDetails_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        Order newOrder = EP_TestDataUtility.createOrder(newAccount.Id, recordTypeId, null);
		insert newOrder;
        
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCsOrderListWithStandardOrderItemsDetails(new Set<Id>{orderObj.Id});
        Test.stopTest();
        
        System.Assert(result.size() > 0);
    }
}