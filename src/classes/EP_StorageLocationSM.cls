/* 
   @Author      Accenture
   @name        EP_StorageLocationSM
   @CreateDate  03/15/2017
   @Description Class for storage location stateMachine
   @Version     1.1
*/
public class EP_StorageLocationSM extends EP_AccountStateMachine{
	
    public EP_StorageLocationSM() {
	}
	
	public override EP_AccountState getAccountState(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationSM','getAccountState');
        return super.getAccountState();
    }
}