@isTest
private class EP_BankAccountMapper_UT {
    static testMethod void getRecordsByAccountIds_test() {
        EP_BankAccountMapper localObj = new EP_BankAccountMapper();
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        list<Id> listAccount = new list<Id>{sellTo.id};
        insert EP_TestDataUtility.createBankAccount(sellTo.Id);
        Test.startTest();
        list<EP_Bank_Account__c> result = localObj.getRecordsByAccountIds(listAccount);  
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getRecordsByBankAccountCode_test() {
        EP_BankAccountMapper localObj = new EP_BankAccountMapper();
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Bank_Account__c  bankAcct = EP_TestDataUtility.createBankAccount(sellTo.Id);
        EP_BankAccountTriggerHandler.isExecutedByIntegrationUser = true;
       // bankAcct.EP_NAV_Bank_Account_Code__c = sellTo.EP_Composite_Id__c+EP_Common_Constant.STRING_HYPHEN+bankAcct.Name;
       bankAcct.EP_NAV_Bank_Account_Code__c = 'test';
        insert bankAcct;
        System.debug('bankAcct.EP_NAV_Bank_Account_Code__c ###'+bankAcct.EP_NAV_Bank_Account_Code__c+'##bankAcct.id##'+bankAcct.Id );
        EP_Bank_Account__c b = [SELECT Id,EP_NAV_Bank_Account_Code__c FROM EP_Bank_Account__c LIMIT 1];
        system.debug('EP_NAV_Bank_Account_Code__c$$ '+b.EP_NAV_Bank_Account_Code__c );
        Set<String> setOfBankAccountIDs = new Set<String>();
        setOfBankAccountIDs.add(bankAcct.EP_NAV_Bank_Account_Code__c );
        Test.startTest();
        list<EP_Bank_Account__c> result = localObj.getRecordsByBankAccountCode(setOfBankAccountIDs);  
        Test.stopTest();
        System.debug('result###'+result);
        System.Assert(result.size() > 0);
    }
    static testMethod void getBankAccountCount_test() {
        EP_BankAccountMapper localObj = new EP_BankAccountMapper();
        Id sellToId = EP_TestDataUtility.getSellTo().id;
        insert EP_TestDataUtility.createBankAccount(sellToId);
        Test.startTest();
        Integer result = localObj.getBankAccountCount(sellToId);  
        Test.stopTest();
        System.assertEquals(1,result);
    }
}