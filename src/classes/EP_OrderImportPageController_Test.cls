/**
 * @author <Sandeep Kumar>
 * @name <EP_OrderImportPageController>
 * @createDate <10/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_OrderImportPageController_Test {
    
    private static Account testSellToAccount;
    private static Account vmiShipToAccount = new Account();
    private static order order1 = new Order();
    private static Account lCustomerAccounts = new Account();
    private static Account lBillTo = new Account();
    private static Account lStogrageLocation = new Account();
    private static EP_Stock_Holding_Location__c lSupplyLocation = new EP_Stock_Holding_Location__c();
    private static EP_Stock_Holding_Location__c lSupplyLocation1 = new EP_Stock_Holding_Location__c();
    private static final string DELIVERYTYPE_SUPPLY_LOCATION_RT = 'Delivery Supply Location';
    private static final string VENDORTYPE = 'Transporter';
    private static final string VENDORTYPE_SUPPLIER = '3rd Party Stock Supplier';
    
    private static final String NAV_VENDOR_ID_1 = '00001';
    private static final String NAV_VENDOR_ID_2 = '00002';
    private static final String COMPANY_CODE_1 = 'AUN2';
    private static final String COMPANY_CODE_2 = 'EPUMA1';
    private static Product2 prod;
    
    private static Profile sysAdmin = [Select id from Profile
                                    Where Name =: EP_Common_Constant.ADMIN_PROFILE  
                                    limit :EP_Common_Constant.ONE];
    private static User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
    
    
     static void init() {
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        
        
        Integer count;
        EP_Freight_Matrix__c freightMatrix;
        
        //CREATE BILL TO
        lBillTo = EP_TestDataUtility.createBillToAccount();
        Database.insert(lBillTo);
        system.assertNotEquals(null, lBillTo.Id);
         
        //Set Bill To Status as basic data setup
        lBillTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lBillTo);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING BILL TO STATUS TO ACTIVE
        lBillTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(lBillTo);
          
        //CREATE FREIGHT MATRIX
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        Database.insert(freightMatrix);
        
        //CREATE A CUSTOMER ACCOUNTS WITH ACTIVE BILL TO ACCOUNT
        lCustomerAccounts = EP_TestDataUtility.createSellToAccount(lBillTo.id, freightMatrix.id);
        Database.insert(lCustomerAccounts,false);
        system.assertNotEquals(null, lCustomerAccounts.Id);
         
        //Set Sell To Status as basic data setup
        lCustomerAccounts.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lCustomerAccounts);
        
        //create Storage location
        lStogrageLocation = EP_TestDataUtility.createStockHoldingLocation();
        Database.insert(lStogrageLocation,false);
        system.assertNotEquals(null, lStogrageLocation.Id);
         
        //Set Storage location Status as basic data setup
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lStogrageLocation,false);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING Sell TO STATUS TO ACTIVE
        lCustomerAccounts.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
        Database.update(lCustomerAccounts);
        
        //SETTING STORAGE LOCATION STATUS TO ACTIVE
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(lStogrageLocation);
        
        //Create ship to
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(lCustomerAccounts.Id, strRecordTypeId);
        vmiShipToAccount.EP_Email__c = 'test@test.com';
        insert vmiShipToAccount;
        system.assertNotEquals(null, vmiShipToAccount.Id);
         
        //Create Supply option
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        lSupplyLocation = EP_TestDataUtility.createSellToStockLocation( lCustomerAccounts.id , True, lStogrageLocation.Id, sellToSHLRTID);
      //  lSupplyLocation.EP_Ship_To__c=vmiShipToAccount.Id;
        Database.insert(lSupplyLocation); 
        system.assertNotEquals(null, lSupplyLocation.Id);
         
        //Create Supply option
        Id DeliverySupplyLocationRT = Schema.SObjectType.EP_Stock_Holding_Location__c.getRecordTypeInfosByName().get(DELIVERYTYPE_SUPPLY_LOCATION_RT).getRecordTypeId();
        lSupplyLocation1 = EP_TestDataUtility.createShipToStockLocation( vmiShipToAccount.id, True, lStogrageLocation.id, DeliverySupplyLocationRT );
        lSupplyLocation1.EP_Ship_To__c=vmiShipToAccount.Id;
        Database.insert(lSupplyLocation1); 
        system.assertNotEquals(null, lSupplyLocation1.Id);
         
        prod = EP_TestDataUtility.createTestRecordsForProduct();
        Id pricebookId = Test.getStandardPricebookId();
        String CurrencyIsoCode = [Select Id, CurrencyIsoCode from Account where Id =: lCustomerAccounts.Id LIMIT 1 ].CurrencyIsoCode;
        system.assertNotEquals(null, prod.Id);
         
        list<PriceBookEntry> pbeList = [Select Id from PriceBookEntry where Product2Id =: prod.Id AND Pricebook2Id =: pricebookId AND CurrencyIsoCode =: CurrencyIsoCode  LIMIT 1];
        PriceBookEntry pbe = new PriceBookEntry();
        if( pbeList.isEmpty() )
        {
            pbe = new PriceBookEntry( Product2Id = prod.Id, PriceBook2Id = pricebookId, CurrencyIsoCode = CurrencyIsoCode );
            Database.insert( pbe );
        }
        else
        {
            pbe = pbeList[0];
        } 
        
        
        
     }
    
     private static testMethod void unitTest1(){
         Test.startTest();
         EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
         ordrImportPageCtrl.blbCSVFile = Blob.valueOf('"testValue"\n,\n"t","e","s","t","V","a","l","u","e","r","t","e","s","t","V","a","l","u","e","r"\n"testValue"\n"testValue"\n"testValue"\n"testValue"');
         ordrImportPageCtrl.parseCSVFile();
         ordrImportPageCtrl.upsertOrders();
         ordrImportPageCtrl.validateDate(1998, 13, 11);
         ordrImportPageCtrl.validateDate(1998, 4, 31);
         ordrImportPageCtrl.validateDate(2004, 2, 30);
         ordrImportPageCtrl.validateDate(2005, 2, 31);
         ordrImportPageCtrl.validateDate(2005, 2, 11);
         String TEST_VAL = 'test value'; 
         EP_OrderImportPageController.OrderImportLineClass obj = new EP_OrderImportPageController.OrderImportLineClass(
                                                           true, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, 
                                                           1, 1, 
                                                           system.today(), system.today(), system.today(), 
                                                            1, 1, 1, 1, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL);
         Test.stopTest();
     }
     private static testMethod void unitTest2(){
         Test.startTest();
         String TEST_VAL = 'test value';
         
         Company__c company = EP_TestDataUtility.createCompany('e-testVal');
         insert company;
         
         Account billToAccount = EP_TestDataUtility.createBillToAccount();
         billToAccount.EP_Legacy_Id__c = 'testVal';
         billToAccount.EP_Puma_Company__c = company.Id;
         insert billToAccount;
         
         EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
         ordrImportPageCtrl.blbCSVFile = Blob.valueOf('"t","testVal","testVal","testVal","testVal","10","9","12/11/2012","12/11/2012","12/11/2012","22","23","24","11","Val","a","l","u","e","r"\n"t","testVal","testVal","testVal","testVal","10","9","12/11/2012","12/11/2012","12/11/2012","22","23","24","11","Val","a","l","u","e","r"\n"t","testVal","testVal","testVal","testVal","10","9","12/11/2012","12/11/2012","12/11/2012","22","23","24","11","Val","a","l","u","e","r"\n"t","testVal","testVal","testVal","testVal","10","9","12/11/2012","12/11/2012","12/11/2012","22","23","24","11","Val","a","l","u","e","r"');
         ordrImportPageCtrl.parseCSVFile();
         ordrImportPageCtrl.upsertOrders();
         ordrImportPageCtrl.validateDate(1998, 13, 11);
         ordrImportPageCtrl.validateDate(1998, 4, 31);
         ordrImportPageCtrl.validateDate(2004, 2, 30);
         ordrImportPageCtrl.validateDate(2005, 2, 31);
         ordrImportPageCtrl.validateDate(2005, 2, 11);
         
         
         
         billToAccount = [Select EP_Composite_Id__c from account];
         //system.assert(false, billToAccount);
         EP_OrderImportPageController.OrderImportLineClass obj = new EP_OrderImportPageController.OrderImportLineClass(
                                                           true, 
                                                           TEST_VAL, TEST_VAL, 'testVal', TEST_VAL, TEST_VAL, 
                                                           1, 1, 
                                                           system.today(), system.today(), system.today(), 
                                                            1, 1, 1, 1, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, 'e', TEST_VAL);
         Test.stopTest();
     }
     private static testMethod void unitTest3(){
         Test.startTest();
         EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
         ordrImportPageCtrl.blbCSVFile = Blob.valueOf('"testValue"\n,\n"t",,,,,"10","9","12/11/2012","12/11/2012","12/11/2012","22","23","24","11","Val","a","l","u","e","r"\n"testValue"\n"testValue"\n"testValue"\n"testValue"');
         ordrImportPageCtrl.parseCSVFile();
         ordrImportPageCtrl.upsertOrders();
         ordrImportPageCtrl.validateDate(1998, 13, 11);
         ordrImportPageCtrl.validateDate(1998, 4, 31);
         ordrImportPageCtrl.validateDate(2004, 2, 30);
         ordrImportPageCtrl.validateDate(2005, 2, 31);
         ordrImportPageCtrl.validateDate(2005, 2, 11);
         String TEST_VAL = 'test value';
         EP_OrderImportPageController.OrderImportLineClass obj = new EP_OrderImportPageController.OrderImportLineClass(
                                                           true, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, 
                                                           1, 1, 
                                                           system.today(), system.today(), system.today(), 
                                                            1, 1, 1, 1, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL);
         Test.stopTest();
     }
     private static testMethod void unitTest4(){
         Test.startTest();
         EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
         ordrImportPageCtrl.blbCSVFile = Blob.valueOf('"Order Number","Supply Location","Sell-To Number","Ship-To Number","Product Number","Quantity","BOL Number","Loading Date","Delivered Date","Ordered Date","Ambient Loaded Quantity","Standard Loaded Quantity","Ambient Delivered Quantity","Standard Delivered Quantity","Supplier Number","Contract Number","Payment Term","Customer PO Number","Company Code","Transporter Code"\n,"41001","1000","100","7116","1002","124","25-10-2016","25-10-2016","24-10-2016","1002","1002","1002","1002",,,"30D",,"AUN",\n,"41001","1000","AU00114946","7116","1002","567","25-10-2016","25-10-2016","24-10-2016","1002","1002","1002","1002",,,"30D",,"AUN",');
         ordrImportPageCtrl.parseCSVFile();
         ordrImportPageCtrl.upsertOrders();
         ordrImportPageCtrl.validateDate(1998, 13, 11);
         ordrImportPageCtrl.validateDate(1998, 4, 31);
         ordrImportPageCtrl.validateDate(2004, 2, 30);
         ordrImportPageCtrl.validateDate(2005, 2, 31);
         ordrImportPageCtrl.validateDate(2005, 2, 11);
         String TEST_VAL = 'test value';
         EP_OrderImportPageController.OrderImportLineClass obj = new EP_OrderImportPageController.OrderImportLineClass(
                                                           true, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, 
                                                           1, 1, 
                                                           system.today(), system.today(), system.today(), 
                                                            1, 1, 1, 1, 
                                                           TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL, TEST_VAL);
         Test.stopTest();
     }
     
     private static testMethod void unitTest5(){
        init();
          String strStLocNum = [Select Id,EP_Nav_Stock_Location_Id__c from Account Where Id =:lStogrageLocation.Id ].EP_Nav_Stock_Location_Id__c;
          String strSellTo = [Select Id,AccountNumber from Account Where Id =:lCustomerAccounts.Id ].AccountNumber;
          String strShupTo = [Select Id,AccountNumber from Account Where Id =:vmiShipToAccount.Id ].AccountNumber;
          String strProdNum = [Select Id,ProductCode from Product2 Where Id =:prod.Id ].ProductCode;
          //String strSupprNum = '';
          //String sLocNum = '';
        test.startTest();
            EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
            String csvStr = 'Order Number,Supply Location,Sell-To Number,Ship-To Number,Product Number,Quantity,BOL Number,Loading Date,Delivered Date,Ordered Date,Ambient Loaded Quantity,Standard Loaded Quantity,Ambient Delivered Quantity,Standard Delivered Quantity,Supplier Number,Contract Number,Payment Term,Customer PO Number,Company Code,Transporter Code\n'+
                            ','+strStLocNum+','+strSellTo+','+strShupTo+','+strProdNum+',1002,124,25/10/2016,25/10/2016,24/10/2016,1002,1002,1002,1002,,,30D,,AUN1,';
            system.debug('csvStr'+csvStr);
            ordrImportPageCtrl.blbCSVFile = Blob.valueOf(csvStr);
            ordrImportPageCtrl.parseCSVFile();
            ordrImportPageCtrl.upsertOrders();
        test.stopTest();
     }
     
      private static testMethod void unitTest6(){
        init();
       
        test.startTest();
            EP_OrderImportPageController ordrImportPageCtrl = new EP_OrderImportPageController();
            String csvStr = 'Order Number,Supply Location,Sell-To Number,Ship-To Number,Product Number,Quantity,BOL Number,Loading Date,Delivered Date,Ordered Date,Ambient Loaded Quantity,Standard Loaded Quantity,Ambient Delivered Quantity,Standard Delivered Quantity,Supplier Number,Contract Number,Payment Term,Customer PO Number,Company Code,Transporter Code\n'+
                            ','+','+','+','+','+','+','+','+',,,,,,,,,,,,,,,';
            
            ordrImportPageCtrl.blbCSVFile = Blob.valueOf(csvStr);
            ordrImportPageCtrl.parseCSVFile();
            ordrImportPageCtrl.upsertOrders();
        test.stopTest();
     }
     
     
}