/*
 *  @Author <Accenture>
 *  @Name <EP_StorageShipToASActive >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  05-Active Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageShipToASActive extends EP_AccountState{
     
    public EP_StorageShipToASActive() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASActive','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASActive','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASActive','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageShipToASActive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}