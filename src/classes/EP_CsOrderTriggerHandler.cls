public with sharing class EP_CsOrderTriggerHandler {

	public static void doAfterUpdate(Map<Id,csord__Order__c> orderMap){
		CSPOFA.Events.emit('update', orderMap.keySet());
	}
}