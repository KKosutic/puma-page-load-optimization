@isTest
private class EP_OrderSyncWithNAVXML_UT {
    
    private static final string MESSAGE_TYPE = 'SFDC_TO_NAV_ORDER_SYNC'; 
    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    	Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
     
   static testMethod void createXML_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       localObj.OrderObj = EP_TestDataUtility.getCSOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

  static testMethod void createPayload_Test() {
      cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
      insert basketInsert;

      EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
      localObj.orderObj = EP_TestDataUtility.getCSOrder();
      localObj.recordid = localObj.OrderObj.id;
      localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
      localObj.messageType = MESSAGE_TYPE;
      localObj.orderObj.csord__Identification__c = String.valueOf(basketInsert.Id);
      upsert localObj.orderObj;

      csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
      orderItemObj.OrderId__c = localObj.orderObj.Id;
      orderItemObj.csord__Order__c = localObj.orderObj.Id;
      orderItemObj.csord__Identification__c = localObj.orderObj.csord__Identification__c;
      orderItemObj.UnitPrice__c = 0;
      orderItemObj.Quantity__c = 1;
      orderItemObj.EP_Is_Standard__c = true;
      insert orderItemObj;

      cscfga__Product_Configuration__c prodConfig = new cscfga__Product_Configuration__c();
      prodConfig.cscfga__Product_Basket__c = basketInsert.Id;
      prodConfig.Name = EP_OrderConstant.PUMAORDERLINEDEFNAME;
      prodConfig.pricingResponseJson__c = '{"Payload" : {"any0" : {"pricingResponse" : {"priceDetails" : {"priceDetails" : {"seqId" : "8010k0000004CZdAAM-19082017015917-0019","companyCode" : "PIY","priceType" : "Fuel Price","priceRequestSource" : "","currencyCode" : "XOF","deliveryType" : "Delivery","customerId" : "00081930","shipToId" : "00081931","supplyLocationId" : "10000000","transporterId" : "","supplierId" : "","priceDate" : "2018-03-07","onRun" : "","orderId" : "","applyOrderQuantity" : "No","totalOrderQuantity" : "5","priceConsolidationBasis" : "Summarised price","versionNr" : ""},"totalAmt" : {"totalAmount" : "2","totalAmountLCY" : "2","totalTaxAmount" : "0","totalTaxAmountLCY" : "0","totalDiscountAmount" : "0","totalDiscountAmountLCY" : "0"},"priceLineItems" : {"lineItem":[ {"lineItemInfo" : {"seqId" : "8010k0000004CZdAAM-19082017015917-0019","orderId" : "","lineItemId" : "4116","itemId" : "45001","quantity" : "5","unitPrice" : "0.79949","unitPriceLCY" : "0.79949","invoiceDetails" : {"invoiceComponent":[ {"seqId" : "8010k0000004CZdAAM-19082017015917-0019","type" : "Fuel Price","name" : "Total Price","amount" : "0.77620","amountLCY" : "0.77620","taxPercentage" : "0", "taxAmount" : "0.02329","taxAmountLCY" : "0.02329","totalAmount" : "0.79949","totalAmountLCY" : "0.79949"} ]},"acctComponents" : {"acctComponent":[ {"seqId" : "8010k0000004CZdAAM-19082017015917-0019","componentCode" : "VAT","glAccountNr" : "","baseAmount" : "0.02329", "baseAmountLCY" : "0.02329","taxRate" : "3","taxAmount" : "0.02329","taxAmountLCY" : "0.02329","totalAmount" : "0.02329","totalAmountLCY" : "0.02329","isVAT" : "No"},{"seqId" : "8010k0000004CZdAAM-19082017015917-0019","componentCode" : "CCT","glAccountNr" : "","baseAmount" : "0.77620","baseAmountLCY" : "0.77620","taxRate" : "0","taxAmount" : "0.00000","taxAmountLCY" : "0.00000","totalAmount" : "0.77620","totalAmountLCY" : "0.77620","isVAT" : "Yes"} ]}},"error" : {"ErrorCode" : "","ErrorDescription" : ""}} ]},"Error" : {"ErrorCode" : "","ErrorDescription" : ""}}}}}}';
      insert prodConfig;
      Test.startTest();
      localObj.init();
      localObj.createPayload();  
      Test.stopTest();
       //no assert needed. Value are only being set
   }   
}