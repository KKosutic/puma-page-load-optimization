/* 
   @Author Accenture
   @name <EP_ExceptionLog_BatchClass_Schedule>
   @CreateDate <20/11/2015>
   @Description <this is the scheduler for batch class for deleting EP_Exception_Log__c records older than 30 days>
   @Version <1.0>
 
*/
global class EP_ExceptionLog_BatchClass_Schedule implements Schedulable{
    
    /** 
    Scheduling method
    **/
    global void execute(SchedulableContext sc) {

        EP_ExceptionLog_BatchClass log = new EP_ExceptionLog_BatchClass();
        database.executebatch(log);
       //set CRON for scheduling 
       String CRON_EXP = EP_Common_Constant.CRONexp;
       EP_ExceptionLog_BatchClass_Schedule sch = new EP_ExceptionLog_BatchClass_Schedule();
       system.schedule('Schedule Delete every Sunday at 2AM', CRON_EXP, sch);
       }
}