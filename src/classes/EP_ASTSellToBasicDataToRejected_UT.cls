@isTest
public class EP_ASTSellToBasicDataToRejected_UT
{

    static final string EVENT_NAME = '02-BasicDataSetupTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo02-Basic Data Setup';
    
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }

    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToBasicDataToRejected ast = new EP_ASTSellToBasicDataToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isTransitionPossible_Positive_test() {
        Account acct = EP_TestDataUtility.getSellToASBasicDataSetUpWithKYCReviewRequired();
        EP_AccountDomainObject obj = new EP_AccountDomainObject(new EP_AccountMapper().getAccountRecord(acct.Id));
        EP_ActionMapper mapperObj = new EP_ActionMapper();
        List<EP_Action__c> kycAction  = mapperObj.getActionsByActionNameAndParentId(acct.Id, new Set<String>{EP_Common_constant.ACT_KYC_REVIEW_RT});
        kycAction[0].OwnerId = UserInfo.getUserId();
        
        database.update(kycAction[0]);
        kycAction[0].EP_Status__c = EP_Common_Constant.ACT_REJECTED_STATUS;
        kycAction[0].EP_Reason__c = 'Test';
        database.update(kycAction[0]);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToBasicDataToRejected ast = new EP_ASTSellToBasicDataToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToBasicDataToRejected ast = new EP_ASTSellToBasicDataToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToBasicDataToRejected ast = new EP_ASTSellToBasicDataToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void isGuardCondition_positive_test() {
        Account acct = EP_TestDataUtility.getSellToASBasicDataSetUpWithKYCReviewRequired();
        EP_ActionMapper mapperObj = new EP_ActionMapper();
        EP_AccountDomainObject obj = new EP_AccountDomainObject(new EP_AccountMapper().getAccountRecord(acct.Id));
        List<EP_Action__c> kycAction  = mapperObj.getActionsByActionNameAndParentId(acct.Id, new Set<String>{EP_Common_constant.ACT_KYC_REVIEW_RT});
        kycAction[0].OwnerId = UserInfo.getUserId();
        database.update(kycAction[0]);
        kycAction[0].EP_Status__c = EP_Common_Constant.ACT_REJECTED_STATUS;
        kycAction[0].EP_Reason__c = 'Test';
        database.update(kycAction[0]);
        
        
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToBasicDataToRejected ast = new EP_ASTSellToBasicDataToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }


}