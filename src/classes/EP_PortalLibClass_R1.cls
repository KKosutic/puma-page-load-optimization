/**
 * @author <Accenture>
 * @name <EP_PortalLibClass_R1>
 * @description <This class contains utility methods> 
 * @version <1.0>
 */
global without sharing class EP_PortalLibClass_R1 {
    private static final String PLACEHOLDER = 'Placeholder';
    private static final String ZERO = '0';
    private static final String TIMEZONE = 'TIMEZONE: ';
    private static final String T_STRING = 'T';
    private static final String PLUS_STRING = '+';
    private static final string EPUMA = 'ePuma';
    private static final string EP_PORTALLIBCLASS_R1 = 'EP_PortalLibClass_R1';
    private static final string RETRIEVEUSEROFFSET = 'retrieveUserOffset';
    private static final string RETRIEVESITEOFFSET = 'retrieveSiteOffset';
    private static final string CONVERTDATETIMETODATE = 'convertDateTimeToDate';
    private static final string CALCULATELOCALDATETIME = 'calculateLocalDateTime';
    private static final string ADDOFFSETTODATETIME = 'addOffsetToDateTime';
    private static final string REMOVEOFFSETTODATETIME = 'removeOffsetToDateTime';
    private static final string ADJUSTUSERDATETIMETOSITE ='adjustUserDateTimeToSite';
    private static final string CALCULATESITEUSERUSERUTC ='calculateSiteUserUTCOffset';
    private static final string RETURNSITEUSERUTCOFFSET ='returnSiteUserUTCOffset';
    private static final string RETURNSITERUNNINGUSERUTC ='returnSiteRunningUserUTCOffset';
    private static final string RETURNLOCATIONDATETIME ='returnLocationDateTimeFromInteger';
    private static final string RETURNLOCALDATETIME ='returnLocalDateTime';
    private static final string RETURNLOCALDATETIMEFROMFULL ='returnLocalDateTimeFromFull';
    private static final string RETURNLOCALDATE ='returnLocalDate';
    private static final string CALCULATEUTCOFFSETOFRUNNINGUSER ='calculateUTCOffsetOfRunningUser';
    private static final string RETURNTIMEFROMSTRING ='returnTimeFromString';
    private static final string CREATEPLACEHOLDERTANKDIPRECORD ='createPlaceholderTankDipRecord';
    /*
        This method is used to retrieve User Offset
    */
    public static Double retrieveUserOffset(String strUserID) {
        Double dblOffset = 0;
        try{
        List<User> users = [SELECT ID, EP_User_UTC_Offset__c FROM User WHERE ID = :strUserID limit :EP_COMMON_CONSTANT.ONE];
        if (!users.isEmpty()) {
            dblOffset = users[0].EP_User_UTC_Offset__c;

        } // End check
        }catch(Exception e){
          //  apexPages.addMessages(e);
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
         /* EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETRIEVEUSEROFFSET,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR); */
            throw e;                                         
        }
        return dblOffset;
    }
    
    /*
        This method is used to retrieve Site Offset
    */
    public static Double retrieveSiteOffset(String strSiteID) {
        Double dblOffset = 0;
        try{
        List<Account> shipTos = [SELECT ID, EP_Ship_To_UTC_Timezone__c FROM Account 
                                    WHERE ID = :strSiteID limit :EP_COMMON_CONSTANT.ONE];
        if (!shipTos.isEmpty()) {
            dblOffset = calcualteUTCOffsetFromTimezone(shipTos[0].EP_Ship_To_UTC_Timezone__c);
            system.debug('dblOffset :'+dblOffset );
        } // End check
        }catch(Exception e){
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Tank_Dip_Error_Message));
          /* EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETRIEVESITEOFFSET,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dblOffset;
    }
    
     /*
        This method is used to convert dateTime to Date
    */
    public static Date convertDateTimeToDate(DateTime dt) {
        Date d = null;
        try{
        if (dt != NULL) {
            d = date.newinstance(dt.year(), dt.month(), dt.day());
        } // End datetime check
        }catch(Exception e){
           // apexPages.addMessages(e);
         //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   CONVERTDATETIMETODATE,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return d;
    }
    
     /*
        This method is used to convert UTC To Local DateTime
    */
    public static DateTime convertUTCToLocalDateTime(Double dblSiteOffset, DateTime dtDate) {
        return calculateLocalDateTime(-dblSiteOffset, dtDate);
    }
    
    /*
        This method is used to calculate Local DateTime
    */
    public static DateTime calculateLocalDateTime(Double dblSiteOffset, DateTime dtDate) {
        DateTime dtConvertedDate = dtDate;
        Integer intMinutes = null;
        try{
        if (dblSiteOffset != NULL) {      
            intMinutes = -Integer.ValueOf(dblSiteOffset * 60);
            dtConvertedDate = dtConvertedDate.addMinutes(intMinutes);
        } // End site timezone check
        }catch(Exception e){
          //  apexPages.addMessages(e);
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   CALCULATELOCALDATETIME,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dtConvertedDate;
    }
    
    /*
        This method is used to add Offset To DateTime
    */
    public static DateTime addOffsetToDateTime(DateTime dt, Double dblOffset) {
        try{
        if (dblOffset != NULL) {
            Integer intMinuteOffset = Integer.valueOf(dblOffset*60);
            if (dblOffset > 0){
                 intMinuteOffset = -Integer.valueOf(dblOffset*60);
            }
            dt = dt.addMinutes(intMinuteOffset);
        }
        }catch(Exception e){
          //  apexPages.addMessages(e);
         // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
        /* EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   ADDOFFSETTODATETIME,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dt;
    }
    
    /*
        This method is used to remove Offset To DateTime
    */
    public static DateTime removeOffsetToDateTime(DateTime dt, Double dblOffset) {
        try{
        if (dblOffset != NULL) {
            Integer intMinuteOffset = -Integer.valueOf(dblOffset*60);
            if (dblOffset > 0) {
                intMinuteOffset = Integer.valueOf(dblOffset*60);
            }
            dt = dt.addMinutes(intMinuteOffset);
        }
        }catch(Exception e){
           // apexPages.addMessages(e);
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   REMOVEOFFSETTODATETIME,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dt;
    }
    
    /*
        This method is used to adjust User DateTime To Site
    */
    public static DateTime adjustUserDateTimeToSite(DateTime dt, Double dblUserOffset, Double dblSiteOffset) {
        Double dblOffset = calculateSiteUserUTCOffset(dblUserOffset, dblSiteOffset);
        try{
        if (dblSiteOffset <= 0 && dblUserOffset <= 0) {
            dt = removeOffsetToDateTime(dt, dblOffset);
        }
        if (dblSiteOffset > 0 && dblUserOffset > 0) {
            dt = removeOffsetToDateTime(dt, dblOffset);
        }
        if (dblSiteOffset > 0 && dblUserOffset < 0) {
            dt = removeOffsetToDateTime(dt, dblOffset);
        }
        if (dblSiteOffset < 0 && dblUserOffset > 0) {
            dt = addOffsetToDateTime(dt, dblOffset);
        }
        }catch(Exception e){
           // apexPages.addMessages(e);
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
         /* EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   ADJUSTUSERDATETIMETOSITE,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dt;
    }
    
    /*
        This method is used to calculate Site User UTC Offset
    */
    public static Double calculateSiteUserUTCOffset(Double dblUserOffset, Double dblSiteOffset) {
        Double dblOffset = 0;
        try{
        if (dblSiteOffset <= 0 && dblUserOffset <= 0) {
            if (dblSiteOffset > dblUserOffset){
                 dblOffset = -dblUserOffset + dblSiteOffset;
            }
            if (dblSiteOffset < dblUserOffset){
                 dblOffset = -dblSiteOffset + dblUserOffset;
            }
        }
        if (dblSiteOffset > 0 && dblUserOffset > 0) {
            if (dblSiteOffset > dblUserOffset){
                 dblOffset = dblSiteOffset - dblUserOffset;
            }
            if (dblSiteOffset < dblUserOffset) {
                dblOffset = dblUserOffset - dblSiteOffset;
            }
        }
        
        if (dblSiteOffset > 0 && dblUserOffset < 0) {
            dblOffset = -dblSiteOffset + dblUserOffset;
        }
        
        if (dblSiteOffset < 0 && dblUserOffset > 0) {
            dblOffset = dblSiteOffset - dblUserOffset;
        }
        }catch(Exception e){
           // apexPages.addMessages(e);
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   CALCULATESITEUSERUSERUTC,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return dblOffset;
        
    }
    
    /*
        This method is used to return Site User UTC Offset
    */
    public static Double returnSiteUserUTCOffset(String strShipToID, Decimal decUserOffset) {
        Double dblOffset = null;
        try{
        Double dblSiteOffset = retrieveSiteOffset(strShipToID);
        Double dblUserOffset = Double.valueOf(decUserOffset);
        dblOffset = calculateSiteUserUTCOffset(dblUserOffset, dblSiteOffset);
        }catch(Exception e){
          //  apexPages.addMessages(e);
         // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
         /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNSITEUSERUTCOFFSET,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                          
        }
        return dblOffset;    
    }
    
    /*
        This method is used to return Site Running User UTC Offset
    */
    public static Double returnSiteRunningUserUTCOffset(String strShipToID) {
        Double dblUserOffset = null;
        try{
        dblUserOffset = calculateUTCOffsetOfRunningUser();
        }catch(Exception e){
           // apexPages.addMessages(e);
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
           /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNSITERUNNINGUSERUTC,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return returnSiteUserUTCOffset(strShipToID, dblUserOffset);
      }
    
    /*
        This method is used to calcualte UTC Offset From Timezone
    */
    public static Double calcualteUTCOffsetFromTimezone(String strTimeZone) {
        Double dblOffset = NULL;
        Double dblMinutes = 0;
        Double dblHours = 0;
        Boolean blnIsNegative = FALSE;
        strTimeZone = strTimeZone.replace(EP_Common_Constant.GMT_TIMEZONE_NAME, EP_Common_Constant.BLANK);
        strTimeZone = strTimeZone.replace(EP_Common_Constant.UTC_TIMEZONE_NAME, EP_Common_Constant.BLANK);
        strTimeZone = strTimeZone.replace(EP_Common_Constant.TIME_SEPARATOR_SIGN, EP_Common_Constant.UNDERSCORE_STRING);
        
        try {
            if (strTimeZone.indexOf(EP_Common_Constant.UNDERSCORE_STRING) > -1) {
                List<String> sValues = strTimeZone.split(EP_Common_Constant.UNDERSCORE_STRING);
                blnIsNegative = (sValues[0].indexOf(EP_Common_Constant.DASH) > -1);//30124
                sValues[0] = sValues[0].replace(EP_Common_Constant.DASH, EP_Common_Constant.BLANK);//30124
                dblHours = Double.valueOf(sValues[0]);
                dblMinutes = Double.valueOf(sValues[1]);
                dblMinutes = dblMinutes / 60;
            }
            dblOffset = dblHours + dblMinutes;
            if (blnIsNegative){
                dblOffset = -dblOffset;
            }
        } catch(exception e) {
            dblOffset = 0;
            throw e;
        }
        return dblOffset;
    }
    /*
        This method is used to return Location DateTime From Integer
    */
    public static DateTime returnLocationDateTimeFromInteger(Integer intYear, Integer intMonth, Integer intDay,
                                                                Integer intHours, Integer intMinutes, Integer intSeconds) {
        DateTime dtSelectedDateTime = null;
        try{
        dtSelectedDateTime = (DateTime)JSON.deserialize(EP_Common_Constant.DOUBLE_QUOTES_STRING + 
            intYear + EP_Common_Constant.hyphen + intMonth + EP_Common_Constant.hyphen + intDay + T_STRING +
            intHours + EP_Common_Constant.TIME_SEPARATOR_SIGN + intMinutes + EP_Common_Constant.TIME_SEPARATOR_SIGN + 
            intSeconds + EP_Common_Constant.DOUBLE_QUOTES_STRING, DateTime.class);
        }catch(Exception e){
            // apexPages.addMessages(e);
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
           /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNLOCATIONDATETIME,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
             throw e;                                          
         }
        return dtSelectedDateTime;
    }
     /*
        This method is used to return Local DateTime
    */
    public static DateTime returnLocalDateTime(DateTime dt) {
        DateTime dtSelectedDateTime = null;
        try{
        if (dt != NULL) {
            dtSelectedDateTime = (DateTime)JSON.deserialize(EP_Common_Constant.DOUBLE_QUOTES_STRING + dt.Year() +
                 EP_Common_Constant.hyphen + dt.Month() + EP_Common_Constant.hyphen + dt.Day() +
                 T_STRING + dt.Hour() + EP_Common_Constant.TIME_SEPARATOR_SIGN + dt.Minute() + 
                 EP_Common_Constant.TIME_SEPARATOR_SIGN + dt.Second() + 
                 EP_Common_Constant.DOUBLE_QUOTES_STRING, DateTime.class);
        } // End dt check
        }catch(Exception e){
            // apexPages.addMessages(e);
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
           /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNLOCALDATETIME,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
             throw e;                                          
         }
        return dtSelectedDateTime;
    }
    /*
        This method is used to return Local DateTime
    */
    public static DateTime returnLocalDateTimeFromFull(Integer intYear, 
    		Integer intMonth, Integer intDay, Integer intHour, Integer intMinute, Integer intSecond) {
        DateTime dtSelectedDateTime = null;
        try{
        if (intYear != NULL && intMonth != NULL && intDay != NULL 
                                && intHour != NULL && intMinute != NULL & intSecond != NULL) {
                                dtSelectedDateTime = (DateTime)JSON.deserialize(EP_Common_Constant.DOUBLE_QUOTES_STRING + intYear 
                                + EP_Common_Constant.hyphen + intMonth + EP_Common_Constant.hyphen + intDay + T_STRING + intHour 
                                + EP_Common_Constant.TIME_SEPARATOR_SIGN + intMinute + EP_Common_Constant.TIME_SEPARATOR_SIGN 
                                + intSecond + EP_Common_Constant.DOUBLE_QUOTES_STRING, DateTime.class);
        } // End dt check
        }catch(Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNLOCALDATETIMEFROMFULL,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
             throw e;                                          
         }
      
        return dtSelectedDateTime;
    }
    /*
        This method is used to return Local Date
    */    
    public static Date returnLocalDate(DateTime dt) {
        Date dtSelectedDate = null;
        try{    
        if (dt != NULL) {
            dtSelectedDate = (Date)JSON.deserialize(EP_Common_Constant.DOUBLE_QUOTES_STRING + dt.Year() 
                                + EP_Common_Constant.hyphen + dt.Month() + EP_Common_Constant.hyphen + dt.Day() 
                                + EP_Common_Constant.DOUBLE_QUOTES_STRING, Date.class);
        } // End dt check
        }catch(Exception e){
           // apexPages.addMessages(e);
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
           /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNLOCALDATE,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }    
        return dtSelectedDate;
    } 
    /*
        This method is used to return UTC Offset of current user
    */
    public static Double calculateUTCOffsetOfRunningUser() {
        Double dblOffset = null;
        try{
        String strOffset = System.now().format(EP_Common_Constant.Z_STRING);
        String strOffsetHours = strOffset.substring(0, 3);
        if (strOffsetHours.startsWith(PLUS_STRING)) {
          strOffsetHours = strOffsetHours.substring(1);
        }
        
        Integer intMinutes = 100 * Integer.valueOf(strOffset.substring(3));
        dblOffset = Double.valueOf(strOffsetHours + EP_Common_Constant.DOT + ((intMinutes) / 60));
        }catch(Exception e){
          //  apexPages.addMessages(e);
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            throw e;
        }
        return dblOffset;
    }
    /*
        This method is used to return combined date and time
    */    
    public Static Datetime combineDateAndTime(Date dDate, Time tTime) {
        Datetime dtDateTime = NULL;
        try {
            dtDateTime = DateTime.newInstance(dDate.Year(), 
                                                dDate.Month(), 
                                                    dDate.Day(), 
                                                        tTime.Hour(), 
                                                            tTime.Minute(), 0);
        } catch (exception e) {
            dtDateTime = NULL;
            throw e;
        }
        return dtDateTime;
    }
    /*
        This method is used to convert string into time
    */
    public static Time returnTimeFromString(String strTime) {
        Time t = NULL;
        try{
        List<String> timeComponents = new List<String>();
        
        // Initialise time component array
        timeComponents.add(ZERO);
        timeComponents.add(ZERO);   
        
        if(strTime != NULL) {
            timeComponents = strTime.split(EP_Common_Constant.TIME_SEPARATOR_SIGN);
        }
        t = Time.newInstance(Integer.valueOf(timeComponents[0]), Integer.valueOf(timeComponents[1]), 0, 0);
        }catch(Exception e){
          //  apexPages.addMessages(e);
         // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
         /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   RETURNTIMEFROMSTRING,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return t;
    }
       
    /*
        This method is used to create placeholder tank dip record
    */
    public static EP_Tank_Dip__c createPlaceholderTankDipRecord(EP_Tank__c tank){
        EP_Tank_Dip__c placeholderTankDip = null;
        try{
        String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER).getRecordTypeId();
        placeholderTankDip = new EP_Tank_Dip__c();
        placeholderTankDip.EP_Tank_Dip_Entered_In_Last_14_Days__c = true;
        placeholderTankDip.EP_Ambient_Quantity__c = 0.00;
        placeholderTankDip.EP_Unit_Of_Measure__c = tank.EP_Unit_Of_Measure__c;
        placeholderTankDip.EP_Tank__c = tank.Id;
        placeholderTankDip.EP_Reading_Date_Time__c = tank.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c;
        placeholderTankDip.RecordTypeId = recType;
        }catch(Exception e){
           // apexPages.addMessages(e);
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Data_missing_error));
           /*EP_LoggingService.logHandledException(e,
                                                   EPUMA,
                                                   CREATEPLACEHOLDERTANKDIPRECORD,
                                                   EP_PORTALLIBCLASS_R1,
                                                   ApexPages.Severity.ERROR);*/
            throw e;                                           
        }
        return placeholderTankDip; 
    }
     /*
    // ########### PLACEHOLDER ###############
    // THIS METHODS MIGHT NOT BE IN USE ANYMORE
    // IF AN ERROR OCCURES PLEASE NOTIFY SPIROS
    

    
    //Method used to calculate the time of current use based on the config of account
    public static Integer calculateCurrentUserTime(Account acc){
        Integer expectedHour;
        Boolean isPositive = FALSE;
        DateTime expectedDatetime = System.Now();
        
        if(acc.EP_Ship_To_UTC_Timezone__c != NULL) {
        
        Double UtcOffset = EP_PortalLibClass.calcualteUTCOffsetFromTimezone(acc.EP_Ship_To_UTC_Timezone__c);
        List<String> UTCHourValue = new List<String>();
        
        if(UtcOffset > 0) {
            isPositive =true;
            UTCHourValue = String.valueof(UtcOffset).remove('-').replace('.','@').split('@');
        } else {
            UTCHourValue = String.valueof(UtcOffset).remove('-').replace('.','@').split('@');
        }
        
        if(isPositive){
            expectedDatetime = expectedDatetime.addHours(integer.valueof(UTCHourValue.get(0)));  
            expectedDatetime = expectedDatetime.addMinutes(integer.valueof(UTCHourValue.get(1)));  
        }else{
            expectedDatetime = expectedDatetime.addHours(-integer.valueof(UTCHourValue.get(0))); 
            expectedDatetime = expectedDatetime.addMinutes(-integer.valueof(UTCHourValue.get(1)));       
        }
            expectedHour = expectedDatetime.hourGMT();
        }
        
        return expectedHour;
    }
    
    */
    
}