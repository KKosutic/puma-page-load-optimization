/**
   @Author Seema Bisht
   @name <EP_OrderTestDataUtility>
   @CreateDate 20/01/2107
   @Description This is a test class which will be use as helper to create unit test data for Order Object
   @Version <1.0>
   @reference <Referenced program names>
*/
  
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
*/
@isTest 
public class EP_OrderTestDataUtility {
	private static final Id orderVMIRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
	
	public static List<Order> orderList = new List<Order>();	
	public static Order createOrder(ID accountId,String status, Id rectypeId,Id pricebookId,Boolean doOrderInsert, Integer nuofOrderRecords){
	 	//for(Integer i=0; i<nuofOrderRecords; i++){
	        Order orderObj = new Order();
	        orderObj.recordtypeId = rectypeId;
	        orderObj.AccountId = accountId;
	        orderObj.Status = status; 
	        orderObj.EP_Reason_for_Change__c= 'Other';
	        orderObj.EP_Other_Reason__c = 'Other';
	        orderObj.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;   
	        //orderObj.CurrencyIsoCode = 'British Pound'; 
	        orderObj.EP_Requested_Delivery_Date__c = System.today()+5; 
	        orderObj.PriceBook2Id = pricebookId;       
	        orderObj.EffectiveDate = system.today(); 
	        //orderList.add(orderObj); 
	 	//}
	    if(doOrderInsert){
	    	Database.insert(orderObj);
	    }
	    return orderObj;
	}    

}