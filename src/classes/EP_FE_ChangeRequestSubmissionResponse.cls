/* 
  @Author <Jyutsna >
   @name <EP_FE_ChangeRequestSubmissionResponse >
   @CreateDate <20/04/2016>
   @Description < >  
   @Version <1.0>
*/
global with sharing class EP_FE_ChangeRequestSubmissionResponse extends EP_FE_Response {

    /*
    *   List of possible errors
    */
    global static final Integer ERROR_CHANGE_REQUEST_CREATION_FAILED = -1;
    global static final Integer ERROR_CHANGE_REQUEST_LINEITEM_CREATION_FAILED = -2;

    global static final String CLASSNAME = 'EP_FE_ChangeRequestEndpoint';
    global static final String METHOD = 'submitChangeRequest';
    global final static String SEVERITY = 'ERROR';
    global final static String TRANSACTION_ID = 'Change Request';
    global final static String DESCRIPTION1 = ' Change Request Creation Failed';
    global final static String DESCRIPTION2 = 'Change Request Line Item Creation Failed';
    
    
    public String changeRequestId;

}