@isTest
public class EP_AddressController_UT{
	private static Account customer = new Account();
	
	static void init() {	
		customer = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
		string address = customer.BillingStreet + EP_Common_Constant.Space + customer.BillingCity + EP_Common_Constant.Space + 
		customer.BillingState + EP_Common_Constant.Space + customer.BillingCountry + EP_Common_Constant.Space + customer.BillingPostalCode;
		String ADRESS_STR = 'address';
    	PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID,customer.id);
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.CNTRY_CODE,EP_Common_Constant.EPUMA);
		ApexPages.currentPage().getParameters().put(ADRESS_STR,address);
        EP_AddressController obj = new EP_AddressController();
        system.assert(ApexPages.currentPage().getParameters().size() > 0);
	}
	    
    static testMethod void fetchKey_test(){
    	init();
        EP_AddressController localObj = new EP_AddressController();
        Test.startTest();
        localObj.fetchKey();
        Test.stopTest();
        System.assertNotEquals(null,localObj.addressContext.googleMapKey);
    }
    
    static testMethod void cancel_test(){
    	init();
    	EP_AddressController localObj = new EP_AddressController();
    	Test.startTest();
        	Pagereference page = localObj.cancel();
        Test.stopTest();
        System.assertNotEquals(null,page);
    }
    
	static testMethod void removeNullString_test(){
		init();
		string value = null;
    	EP_AddressController localObj = new EP_AddressController();
    	Test.startTest();
        	string result = localObj.removeNullString(value);
        Test.stopTest();
        System.assertNotEquals(null,result);
    }    
    
	static testMethod void populateAddressFieldOnRecord_test(){
		init();
    	EP_AddressController localObj = new EP_AddressController();
        localObj.addressContext.record =  new Account();
    	localObj.addressContext.address = 'street_number:1,route:testRoute,locality:testlocatilty,administrative_area_level_1:testadministrativearealevel1,Country Code:AU,postal_code:12345678,Country:TestCountry,lat:1.0,lng:3.0';
        localObj.addressContext.isChangeRequest = true;
        localObj.addressContext.mTypeField.put(EP_Common_Constant.STREET_STR,'BillingStreet');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.CITY_STR,'BillingCity');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.POST_CODE,'BillingPostalCode');
        localObj.addressContext.mTypeField.put(EP_COMMON_CONSTANT.Country.toUpperCase(),'BillingCountry');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.STATE_STR,'BillingState');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.LATLANG_STR,'EP_Position__c');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.LATITUDE_STR,'BillingLatitude');
        localObj.addressContext.mTypeField.put(EP_Common_Constant.LONGITUDE_STR,'BillingLongitude');
    	Test.startTest();
        	localObj.populateAddressFieldOnRecord();
        Test.stopTest();
        //Method is delegating to other method hence a dummy assert is added
        system.assert(true); 
    }
    
	static testMethod void processAddress_validCountry_test(){
		init();
    	EP_AddressController localObj = new EP_AddressController();
		localObj.addressContext.address = 'street_number:1,route:testRoute,locality:testlocatilty,administrative_area_level_1:testadministrativearealevel1,Country Code:AU,postal_code:12345678,Country:Australia,lat:1.0,lng:3.0';
        localObj.addressContext.isChangeRequest = true;
        localObj.updateAddress();
    	Test.startTest();
        	localObj.processAddress();
        Test.stopTest();
        system.assertEquals('AU',localObj.addressContext.enteredCntryCode); 
        system.assertEquals('Australia',localObj.addressContext.addressObj.country );
        system.assertNotEquals(null,localObj.addressContext.addressObj.countryRecord );
        system.assertEquals('AU',localObj.addressContext.addressObj.cntryCode);      
    }
    
 	static testMethod void processAddress_InvalidCountry_test(){
		init();
    	EP_AddressController localObj = new EP_AddressController();
		localObj.addressContext.address = 'street_number:1,route:testRoute,locality:testlocatilty,administrative_area_level_1:testadministrativearealevel1,Country Code:AU,postal_code:12345678,Country:TestCountry,lat:1.0,lng:3.0';
        localObj.addressContext.enteredCntryCode = 'AA';
        localObj.addressContext.isChangeRequest = true;
        localObj.updateAddress();
    	Test.startTest();
        	localObj.processAddress();
        Test.stopTest();
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.EP_Invalid_Country)) b = true;
		}
		system.assert(b);   
    }
    
	static testMethod void processAddress_InvalidAddressForCountry_test(){
		init();
    	EP_AddressController localObj = new EP_AddressController();
		localObj.addressContext.address = 'street_number:1,route:testRoute,locality:testlocatilty,administrative_area_level_1:testadministrativearealevel1,Country Code:AU,postal_code:12345678,Country:Australia,lat:1.0,lng:3.0';
        localObj.addressContext.countryCode = 'AU';
        localObj.addressContext.enteredCntryCode = 'AA';
        localObj.addressContext.isChangeRequest = false;
       	Test.startTest();
        	localObj.processAddress();
        Test.stopTest();
		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains(Label.EP_Address_SelectMsg)) b = true;
		}
		system.assert(b);   
    }
}