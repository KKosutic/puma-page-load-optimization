public with sharing class EP_OrderPageContext {
    
    public csord__Order__c newOrderRecord {get;set;}   
    public Account orderAccount {get;set;} 
    public Account invoicedAccount;
    //public EP_ProfileMapper profileMapper;
    public EP_OrderMapper orderMapper;
    public EP_OrderDomainObject orderDomainObj;
    public EP_OrderService orderService;
    public EP_AccountDomainObject accountDomainObj;
    public EP_AccountService accountService; //only used once
    
    public Boolean isAccountFound;//once in controller and NOT in VF Page
    public Boolean isRoutePresent = false; //once in Controller and NOT in VF Page
    public Boolean inventoryAbsent = false; // once in Controller, not used in VF Page
    public Boolean isSlottingEnabled{get;set;}//used in VF Page
    public Boolean isOprtnlTankAvail {get;set;} // used in VF Page
    public Boolean hasAccountSetupError {get;set;} //used in VF Page
    public Boolean isPricingRequired{get;set;}//used in modification vf page
    public Boolean isPrepaymentRequired {get;set;}//Jyotsna March 16, 2017: Obsolete functionality
    public Boolean hasCreditIssues {get;set;}//used once
    public Boolean isConsignmentOrder {get;set;} //used once
    public Boolean isDummy {get;set;} //Used
    public Boolean showCustomerPoField { get;set;}//Used
    //public boolean isRoOrder{get;set;}//Used
    public boolean isConsumptionOrder{get;set;}//Used
    //Defect:-57816--Start 
    public boolean isPrepayBillToOverdue {get;set;}
    //Defect:-57816--End 
    public Boolean hasOverdueInvoices {get;set;}//Used    
    public Boolean isPricingRecieved{get;set;}//used
    public boolean isPackagedOrder{get;set;}//used
    public Boolean isPriceCalculated;//used
    public Boolean prodNotAvailAtSuppLoc{get;set;}
    public Boolean isModification = false;
            
    public Integer intOrderLineIndex;//used
    public Integer intAccountProductsFound; //used   
    public Integer pricingPollerCount{get;set;}//used
    //public integer invCheck = -1; //not used
    public Integer intOrderCreationStepIndex {get;set;}//
    public Integer intOliIndex{ get;set;}//
    public Double totalCostPE;
      

    public List < SelectOption > listOfShipToOptions {get;set;}
    public List < SelectOption > listOfSupplyLocationOptions {get;set;}
    public List < SelectOption > availableRoutes{get;set;}//used in VF Page only
    public List< SelectOption > availableRuns{get;set;}
    public List<String> availableDatesList{get;set;}
    public List<OrderWrapper> listofOrderWrapper {get;set;}
    public List <SelectOption> supplierOptions {get;set;}
    public List<SelectOption> transportAccount{get;set;}
    //public List < Contact > aContacts;//Not used anywhere
    //public List < Account > shipToAccts; //Not used anywhere
    //public List < EP_Stock_Holding_Location__c > supplyLocList; //Not used anywhere
    public List < String > setProductIDs;

    //public Map<Id,String> transporterMap{get;set;} // not used anywhere
    public Map<ID, EP_Tank__c> mapShipToOperationalTanks;
    public Map<ID, EP_Tank__c> mapShipToTanks;
    public Set<Id> shipToOperationalTankIds = new Set<Id>();
    public Map< String,List<SelectOption> > timeSlots{get;set;}
    public Map < Id, Account > mapOfShipTo;
    public Map < String, EP_Stock_Holding_Location__c > mapOfSupplyLocation;
    public Map<Id,List<Contract>> supplierContractMap;
    public Map<String,csord__Order__c> bulkRelatedOrderMap; 
    //public Map<String,Order> orgPckgdOrderMap;//not used anywhere 

    public Set<Id> orderItemToRemoveSet;//Defect 58835 

    public String profileName {get;set;}
    //public String storageLocation; //Not used anywhere
    //public String availableDates; //Not use anywhere
    public String strSelectedOrderID;
    public String availableSlots { get; set; }
    public String deliveryType {get;set;}
    public String strTransporterName{get;set;}//Defect 57621
    public string startDate{get;set;}
    public string strCustomerCurrencyCode{get;set;} 
    public String strSelectedRoute {get;set;}
    public String strSelectedRun{get;set;}
    //public String strSelectedProductID {get;set;}//not used anywhere. only local variables found
    public String strSelectedDeliveryType {get;set;}
    public String strSelectedPricebookID {get;set;}
    public String strSelectedShipToID {get;set;}
    public String strSelectedPickupLocationID {get;set;}
    public String strSelectedPaymentTerm {get;set;}
    public String strSelectedOrderLineItem {get;set;}
    public String strSelectedDate {get;set;}
    public String strLoadedPickupLocationID {get;set;}
    public String strLoadedDeliveryType {get;set;}//not sure why we need this when it's not used
    public String strRelatedBulkOrder{get;set;}
    public String strSelectedAccountID {get;set;}
    
    public static final String CLASSNAME = 'EP_OrderPageContext';
    
    
    public Map < String, Decimal > productPriceMap;
    public Map < String, String > productNameMap;
    public Map < String, String > productPriceBookEntryMap;
    public Map < String, String > PriceBookEntryproductMap;
    public Map < Id, String > productInventoryMap;
    public Map < String, String > productUnitOfMeasureMap;
    public Map < String, String > productRangeMap;
    public Map < String, List < String >> availableProductQuantitiesMap;
    public Map<Id,String> runMap;
    public map<Id,list<csord__Order_Line_Item__c>> childItemsMap;
    public Map<Id,Double> itemIdTotalPriceMap;//Defect 57495
    public Set<Id> existingOrderProductSet;

    public csord__Order__c oldOrderRecord;
    public EP_Stock_Holding_Location__c selectedPickupDetail;
    
    public EP_OrderPageContext(String strSelectedID){

        orderMapper = new EP_OrderMapper();
        if(strSelectedID.substring(0,3).equals('001')){
            accountDomainObj = new EP_AccountDomainObject(strSelectedID);
            accountService = new EP_AccountService(accountDomainObj);
            orderAccount = accountDomainObj.localAccount;
            strSelectedAccountID = orderAccount.Id;
            newOrderRecord = new csord__Order__c(EP_Order_Epoch__c = EP_Common_Constant.EPOC_CURRENT, EP_Order_Category__c = EP_Common_Constant.SALES,EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK, EP_Delivery_Type__c = EP_Common_Constant.DELIVERY);  
            newOrderRecord.AccountId__c = strSelectedAccountID;
            if (orderAccount.EP_Is_Dummy__c) {
                newOrderRecord.EP_Order_Category__c = EP_Common_Constant.ORD_CAT_TRANSFER_ORD;
            }
            orderDomainObj = new EP_OrderDomainObject(newOrderRecord);
        }else{
            orderDomainObj = new EP_OrderDomainObject(strSelectedID);
            newOrderRecord  = orderDomainObj.getOrder();  
            accountDomainObj = new EP_AccountDomainObject(newOrderRecord.AccountId__c);
            accountService = new EP_AccountService(accountDomainObj);
            orderAccount = accountDomainObj.localAccount;
            strSelectedOrderID = newOrderRecord.Id;
            strSelectedAccountID = orderAccount.Id;
        }
        //system.debug('Order domain is ' + orderDomainObj);
        //Initialize the account object pertaining to the selected Account on screen
        
        isDummy = orderAccount.EP_Is_Dummy__c;
        
        //Initialize the Order object for the page
        orderService = new EP_OrderService(orderDomainObj);
        isPackagedOrder = orderDomainObj.isPackaged();
               
        bulkRelatedOrderMap = EP_PortalOrderUtil.relatedBulkOrderMap(strSelectedID);
        //orgPckgdOrderMap = EP_PortalOrderUtil.orgPckgdOrderMap(strSelectedID); 
        
        isConsumptionOrder = false;         
        isConsignmentOrder = false;
        isPriceCalculated = false;
        showCustomerPoField = false;
        isOprtnlTankAvail = false;
        isSlottingEnabled = false;
        hasCreditIssues= false; 
        isAccountFound = true;
        hasAccountSetupError = false;
        IsPrepaymentRequired = false;
        isPricingRequired = false;
        
        
        totalCostPE = 0;        
        pricingPollerCount= 0;
        intOrderCreationStepIndex = 1;
        intOrderLineIndex = 1;
        intAccountProductsFound = 0;
        
        //availableDates = EP_Common_Constant.BLANK;
        profileName = new EP_ProfileMapper().getCurrentUserProfile().Name;
    
        //shipToAccts = new List<Account>();
        //supplyLocList = new List<EP_Stock_Holding_Location__c>();
        availableDatesList = new List<String>();
        listofOrderWrapper = new List <orderWrapper>();   
        transportAccount = new List<SelectOption>();
        //uncommenting start
        productPriceMap = new Map < String, Decimal > ();
        productNameMap = new Map < String, String > ();
        productPriceBookEntryMap = new Map < String, String > ();
        priceBookEntryproductMap = new Map < String, String > ();
        productInventoryMap = new Map < Id, String > ();
        productUnitOfMeasureMap = new Map < String, String > ();
        productRangeMap = new Map < String, String > (); // This map is used handle the product restrictions
        availableProductQuantitiesMap = new Map < String, List < String >> ();
        //uncommenting end
        runMap = new Map<Id,String>();
        setProductIDs = new List < String > ();
        mapShipToOperationalTanks = new Map<ID, EP_Tank__c>();
        mapShipToTanks = new Map<ID, EP_Tank__c>();
        existingOrderProductSet = new Set<Id>();
        orderItemToRemoveSet = new Set<Id>(); // Defect #58835

    }
    
    public boolean isRoOrder{
       get{
            return (orderDomainObj.isRetrospective());
       }
       set;

    }
       
    public boolean isBulkOrder{
        get{
            return (orderDomainObj.isbulk());
        }
        set;
    }
    
    public boolean isExRack{
        get{
            return(EP_Common_Constant.EX_RACK.equalsIgnoreCase(strSelecteddeliveryType)); 
        }
    }    
    
    public Boolean isPrepaymentCustomer {
        get {
            return (EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(strSelectedPaymentTerm));
        }
    }
    
    public Boolean showdeliveryTypeSelector {
        get {
            return (intOrderCreationStepIndex == 1);
        }
        set;
    }

    public Boolean showShipToSelector {
        get {
            return intOrderCreationStepIndex == 2;
        }
        set;
    }

    public Boolean showSupplyLocationSelector {
        get {
            return (intOrderCreationStepIndex == 3);
        }
        set;
    }
    public Boolean isDeliveryOrder {
        get {
            //system.debug('isDummy****'+ isDummy);
            if (isDummy) {
                return false;
            } 
            else {
                return (!EP_Common_Constant.EX_RACK.equalsIgnoreCase(strSelecteddeliveryType)); 
            }
        }
    }
    public Boolean showOrderEntryLocationSelector {
        get {
            return (intOrderCreationStepIndex == 4);
        }
        set;
    }

    public Boolean showOrderSummarySelector {
        get {
            return (intOrderCreationStepIndex == 5);
        }
        set;
    } 
    
    public Boolean showSubmitButton {
        get {
            return (intOrderCreationStepIndex == 5);
        }
    }
    
    public Boolean isOrderEntryValid {
        get {
            Boolean isValid = orderService.isOrderEntryValid(transportAccount.size());
            if (isValid) {
                for (EP_OrderPageContext.OrderWrapper orderWrapperObj: listofOrderWrapper) {
                    if (!orderWrapperObj.oliIsValid) {
                        return false;
                    }
                    
                } 
            }
            return isValid;
        }
    }
    
    public String userCurrencyFormat {
        get {
           return EP_GeneralUtility.getNumericDisplayFormat();
        }
    }
    public List < SelectOption > listPaymentTermsOptions {        
        get {
            listPaymentTermsOptions = new List <SelectOption> ();
            for(String strPaymentTerm : orderService.getApplicablePaymentTerms(invoicedAccount)) {
                listPaymentTermsOptions.add(new SelectOption(strPaymentTerm, strPaymentTerm));
            }            
            return listPaymentTermsOptions;
        }
        set;
    }
    public List < SelectOption > listDeliveryTypeSelectOptions {
        get {
            listDeliveryTypeSelectOptions = new List < SelectOption > ();
            for (String deliveryType : orderDomainObj.getDeliveryTypes()) {
                listDeliveryTypeSelectOptions.add(new SelectOption(deliveryType, deliveryType));
            }  
            return listDeliveryTypeSelectOptions;
        }
        set;
    }
    
    public Boolean isErrorInPage {
        get {
            Boolean hasError = FALSE;
            if (ApexPages.getMessages() != NULL) {
                hasError = ApexPages.hasMessages(ApexPages.severity.ERROR);
            } // End page error NULL check
            return hasError;
        }
    }
    
     public Double totalOrderCost {
        get {
            Double dblTotalCost = 0;
            if(newOrderRecord.Id != NULL ) {
                dblTotalCost = totalCostPE;        
            }
            return dblTotalCost;
        }
    }

    public boolean isPrepaymentOrder{
        get{
            return (newOrderRecord.EP_Payment_Term__c != NULL && newOrderRecord.EP_Payment_Term__c.equalsIgnoreCase(EP_Common_Constant.PREPAYMENT));
        }
    }

    public List<SelectOption> getSupplyLocPricing(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getSupplyLocPricing');
        List<SelectOption> listSupplyLocPricingOptions = new List<SelectOption>();                   
        for(Account supplylocPricingObj : accountDomainObj.getsupplyLocationPricingList()) {  
            listSupplyLocPricingOptions.add(EP_Common_Util.createOption(supplylocPricingObj.id,supplylocPricingObj.name));                   
        }
        return listSupplyLocPricingOptions;
    
    }
    public List<SelectOption> getTransporterPricingList(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getTransporterPricingList');
        List<SelectOption> listTransportPricingOptions = new List<SelectOption>();
        for(Account transportPricingAccount : accountDomainObj.getTransporterPricingList()) {
            listTransportPricingOptions.add(EP_Common_Util.createOption(transportPricingAccount.id,transportPricingAccount.name)); 
        }
        return listTransportPricingOptions;
    }
    
    public void onLoadActionForNewOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'onLoadActionForNewOrder');
        isAccountFound = false;
        EP_AccountDomainobject invoicingCustomer = new EP_AccountDomainObject(accountService.getBillTo());
        if(!invoicingCustomer.localaccount.EP_IsActive__c){
            hasAccountSetupError = true;
            String strErrorMsg = (Label.EP_Blocked_Order_Error).replace(EP_Common_Constant.GENERIC_MSG_REPLACE_STRING,invoicingCustomer.localAccount.EP_Account_Type__c);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,strErrorMsg));
        }
        else{
            invoicedAccount = invoicingCustomer.localaccount;
            hasAccountSetupError = false;
            isAccountFound = true;
        }     
        strSelectedDeliveryType = orderAccount.EP_Delivery_Type__c;
        newOrderRecord.CurrencyIsoCode = orderAccount.CurrencyIsoCode;
        newOrderRecord.EP_Credit_Status__c =  EP_Common_Constant.OK;
        newOrderRecord.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);         
        listofOrderWrapper = new List < EP_OrderPageContext.OrderWrapper > ();
    }
    
    public void setWizardIndexonBack(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setWizardIndexonBack');
        if(intOrderCreationStepIndex  == 4 && newOrderRecord.id != null) {
            orderDomainObj.deleteOrderLineItems();
        }
        else if(intOrderCreationStepIndex  == 3) {
            isOprtnlTankAvail = false;
            // Move to step 1 from step 3 for ex rack orders 
            //if (!isDeliveryOrder && !isDummy && !isConsumptionOrder) { // defect fix 26051 // #defectFixReOpen 29827 added !isDummy
            if(strSelectedDeliveryType.equalsIgnoreCase(EP_Common_Constant.EX_RACK)){   
                intOrderCreationStepIndex = 2;
            }          
        }
        intOrderCreationStepIndex--;
        //Changes for #60147
        pricingPollerCount = 0;
        //Changes for #60147 End  
        
    }

    public boolean isShowExportedCheckBoxAllowed(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'isShowExportedCheckBoxAllowed');
        return ((newOrderRecord.EP_Use_Managed_Transport_Services__c.equalsIgnoreCase(EP_Common_Constant.STRING_TRUE) && newOrderRecord.EP_3rd_Party_Managed_Exported__c && (EP_Common_Constant.CSC_PROFILES.contains(profileName) || EP_Common_Constant.ADMIN_PROFILES.contains(profileName)))?true:false);
    }
    
    public void setRun(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setRun');
        EP_RunMapper runMapper = new EP_RunMapper();
        List<SelectOption> runList = new List<SelectOption>();
        runList.add(new SelectOption(EP_Common_Constant.BLANK,EP_Common_Constant.PICKLIST_NONE));
        //Defect Fix Start #57583
        for(EP_Run__c objRun : runMapper.getActiveRunsByRoute(strSelectedRoute)) {
            runList.add(EP_Common_Util.createOption(objRun.id,objRun.name));
            runMap.put(objRun.id,objRun.EP_Run_Number__c);
        }
        
        if(strSelectedRoute != newOrderRecord.EP_Route__c) {
            isPriceCalculated = false;
            if(strSelectedRoute == null)
                strSelectedRun = null;
        }
        //Defect Fix End #57583
        availableRuns = runList;
    }
    
    public void setshiptodetailsonOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setshiptodetailsonOrder');
        if (isDeliveryOrder || isDummy) {
            if(!isModification){
                newOrderRecord.EP_ShipTo__c = strSelectedShipToID;
                newOrderRecord.EP_ShipTo__r = mapOfShipTo.get(strSelectedShipToID);  
            }
            mapShipToTanks = orderService.getTanks( strSelectedShipToID );
            mapShipToOperationalTanks = EP_PortalOrderUtil.getOperationalTanks( mapShipToTanks.values() );

        }
    }

    public void setDataForConsumptionOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setDataForConsumptionOrder');
        String orderCategory = System.currentPagereference().getParameters().get(EP_Common_Constant.STR_SELECTED_ORDERCATEGORY);
        //system.debug('***********:-'+newOrderRecord.EP_Order_Category__c);
        if(EP_Common_Constant.CONSUMPTION_ORDER.equalsIgnoreCase(newOrderRecord.EP_Order_Category__c)) {
            strSelectedDeliveryType = EP_Common_Constant.CONSUMPTION;
            newOrderRecord.EP_Delivery_Type__c = EP_Common_Constant.CONSUMPTION;
            newOrderRecord.EP_Order_Epoch__c = EP_Common_Constant.EPOC_RETROSPECTIVE;
            isConsumptionOrder = true;
        }else {
            strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
            newOrderRecord.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
            newOrderRecord.EP_Order_Epoch__c = EP_Common_Constant.EPOC_CURRENT;
            isConsumptionOrder = false;
        }
      //  orderDomainObj = new EP_OrderDomainObject(newOrderRecord);
        orderService = new EP_OrderService(orderDomainObj);
    }
    
    public void settankdetailsonOrder(OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Public',CLASSNAME,'settankdetailsonOrder');
        if(orderWrapperObj.oliTanksID != null){
            if(orderWrapperObj.oliTanksID <> orderWrapperObj.orderLineItem.EP_Tank__c){
                orderWrapperObj.oliQuantity = null;
            }
            orderWrapperObj.oliPricebookEntryID =  PriceBookEntryproductMap.get(mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Product__C);   
            orderWrapperObj.orderLineItem.EP_Tank__c = orderWrapperObj.oliTanksID; // tank work
            
        }else{
            orderWrapperObj.oliTankProductName = EP_Common_Constant.BLANK;
            orderWrapperObj.oliTankSafeFillLvl = null;
            orderWrapperObj.oliQuantity = null;
        }
    
    }
    
    public void setOperationTankdetails(OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOperationTankdetails');
        if(shipToOperationalTankIds.contains( orderWrapperObj.oliTanksID )){
            System.debug('****orderWrapperObj**'+orderWrapperObj);
            orderWrapperObj.oliTankProductName = mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Product__r.Name;
            orderWrapperObj.orderLineItem.EP_Ambient_Loaded_Quantity__c = Null;
            orderWrapperObj.orderLineItem.EP_Standard_Loaded_Quantity__c = Null;
            orderWrapperObj.orderLineItem.EP_Ambient_Delivered_Quantity__c = Null;
            orderWrapperObj.orderLineItem.EP_Standard_Delivered_Quantity__c = Null;
            if(mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Safe_Fill_Level__c != null)
                orderWrapperObj.oliTankSafeFillLvl = Integer.valueOf(mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Safe_Fill_Level__c);
            if(mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Tank_Alias__c != NULL)
                orderWrapperObj.oliTanksName =  mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Tank_Code__c+EP_Common_Constant.SLASH+ mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Tank_Alias__c;
            else
                orderWrapperObj.oliTanksName = mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID).EP_Tank_Code__c;
            
            
        }

    }
    
    public void setProductNameandTankDetails(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setProductNameandTankDetails');
        OrderItem orderItemObj;
        isPriceCalculated = false; 
        for (EP_OrderPageContext.OrderWrapper orderWrapperObj: listofOrderWrapper) {
          
            if( orderWrapperObj.oliPricebookEntryID != orderWrapperObj.orderLineItem.PricebookEntryId__c && orderWrapperObj.orderLineItem.PricebookEntryId__c != null){
                orderItemObj = new orderItem();   
            }else{
               // orderItemObj = orderWrapperObj.orderLineItem;
               settankdetailsonOrder(orderWrapperObj);
                
            }
            setOperationTankdetails(orderWrapperObj);
            orderWrapperObj.oliProductName = productNameMap.get(orderWrapperObj.oliPricebookEntryID);
                          
        } 
    }

    public boolean isOrderEditable(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'isOrderEditable');
        set<String> CancellableStatus = new set<String>(); 
        if(EP_Common_Constant.EPOC_RETROSPECTIVE.equalsIgnoreCase(newOrderRecord.EP_Order_Epoch__c)){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.SEVERITY.ERROR,LABEL.EP_Retro_Consumption_ConsignmentError) ); 
        }
        if( EP_Common_Constant.EX_RACK.equalsIgnoreCase(strSelectedDeliveryType) )
        {
            CancellableStatus.add(EP_Common_Constant.planned);
        }
        if( EP_Common_Constant.CUSTOMER_PROFILES.contains(profileName ) )
        {
            CancellableStatus.addAll(EP_Common_Constant.CANCELLABLE_ORDER_STATUS_FOR_CUSTOMER); 
        }
        else
        {
            CancellableStatus.addAll(EP_Common_Constant.CANCELLABLE_ORDER_STATUS_FOR_CSC);
        }
        if( !CancellableStatus.contains(newOrderRecord.csord__Status__c) )
        {
           String statuses = String.valueof( CancellableStatus );
            if( String.isNotBlank( statuses ) )
            {
                statuses = statuses.remove(EP_Common_Constant.LEFT_CURLY_BRACE).remove(EP_Common_Constant.RIGHT_CURLY_BRACE);
                ApexPages.addMessage(new ApexPages.Message( ApexPages.SEVERITY.ERROR,LABEL.EP_UnableToModifyOrder) ); 
            }
            return false;
        }
        else
        {
            return true;
        }
    }
                  
    // For exiting order, loads order line items in orderwrapper and collects product list
    public void loadExistingOrderItems(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'loadExistingOrderItems');
        childItemsMap = new map<Id,list<csord__Order_Line_Item__c>>();
        List<csord__Order_Line_Item__c> lineItemList = [SELECT Id, EP_Is_Standard__c, EP_Unit_Of_Measure__c, EP_Product__c, EP_Parent_Order_Line_Item__c, EP_Product__r.EP_Unit_Of_Measure__c, PricebookEntryId__c, EP_Product__r.Name, Quantity__c, EP_Tank__r.Id, EP_Tank__r.EP_Tank_Alias__c FROM csord__Order_Line_Item__c WHERE OrderId__c = :newOrderRecord.Id];
        System.debug('********** newOrderRecord: ' + newOrderRecord);
        System.debug('********** lineItemList: ' + lineItemList);
       // orderItemDeleteSet = new Set<Id>();
        for( csord__Order_Line_Item__c orderItemObj : lineItemList)
        {
            if(!orderItemToRemoveSet.contains(orderItemObj.Id)){ //Defect #58835
                if( orderItemObj.EP_Is_Standard__c ){ 
                    listofOrderWrapper.add( setExistingOrderLineItemsInWrapper(orderItemObj) );
                    existingOrderProductSet.add(orderItemObj.EP_Product__c);
                    intOrderLineIndex++;
                }
                else{
                    if( !childItemsMap.containsKey( orderItemObj.EP_Parent_Order_Line_Item__c )){
                        childItemsMap.put( orderItemObj.EP_Parent_Order_Line_Item__c, new list<csord__Order_Line_Item__c>());
                    }
                    childItemsMap.get( orderItemObj.EP_Parent_Order_Line_Item__c ).add( orderItemObj );
                }
            }//Defect #58835
        }
       // calculateTotalPrice(childItemsMap);
    }

    @TestVisible
    private EP_OrderPageContext.OrderWrapper setExistingOrderLineItemsInWrapper(csord__Order_Line_Item__c orderItemObj){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setExistingOrderLineItemsInWrapper');
        String UoM = String.isNotBlank(orderItemObj.EP_Unit_of_Measure__c) ? orderItemObj.EP_Unit_of_Measure__c : orderItemObj.EP_Product__r.EP_Unit_Of_Measure__c;
        String strKey = orderItemObj.PricebookEntryId__c+UoM;
        EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
        orderWrapperObj.isnew = false;
        orderWrapperObj.oliIsValid = true;
        orderWrapperObj.orderLineItem = orderItemObj;
        orderWrapperObj.orderLineItem.EP_Prior_Quantity__c = orderItemObj.Quantity__c;
        System.debug('********** itemIdTotalPriceMap: ' + itemIdTotalPriceMap);
        orderWrapperObj.oliTotalPrice = itemIdTotalPriceMap.get(orderItemObj.Id);//Defect 57495
        orderWrapperObj.oliIndex = intOrderLineIndex;
        orderWrapperObj.oliPricebookEntryID = orderItemObj.PricebookEntryId__c;
        orderWrapperObj.oliProductName = orderItemObj.EP_Product__r.Name;
        orderWrapperObj.oliQuantity = Integer.Valueof(orderItemObj.Quantity__c);
        orderWrapperObj.oliProductUoM = UoM;
        orderWrapperObj.oliTanksID = orderItemObj.EP_Tank__r.id;                                    
        orderWrapperObj.oliTanksName = orderItemObj.EP_Tank_Code__c;
        if(orderItemObj.EP_Tank__r.EP_Tank_Alias__c != NULL){
            orderWrapperObj.oliTanksName =  orderWrapperObj.oliTanksName + EP_Common_Constant.SLASH + orderItemObj.EP_Tank__r.EP_Tank_Alias__c;
        }

        return orderWrapperObj;
    }

    public list<csord__Order_Line_Item__c> getOrderLineItemsFromOrderWrapper(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderLineItemsFromOrderWrapper');
        List<csord__Order_Line_Item__c> orderItemList = new List<csord__Order_Line_Item__c>();
        for (EP_OrderPageContext.OrderWrapper orWrapper: listofOrderWrapper) {
            csord__Order_Line_Item__c ot = orWrapper.orderLineItem;  
            orderItemList.add(ot);
        } 
        return orderItemList;
    }

    public void loadAccountOrderDetails(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'loadAccountOrderDetails');
        intOrderCreationStepIndex = 2;
        oldOrderRecord = orderMapper.getCSRecordById(strSelectedOrderID);                
        if (newOrderRecord <> null) {  
            //showExportedCheckBox = isShowExportedCheckBoxAllowed();                                       
            setPageVariablesforModificationController();
            isOrderEditable();
            validateCustomer();
        }
    }

    public void setPageVariablesforModificationController(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setPageVariablesforModificationController');
        showCustomerPoField = orderDomainObj.showCustomerPOAllowed();
        strSelectedDate = String.valueOf(newOrderRecord.EP_Requested_Delivery_Date__c);
        if (String.isNotBlank(strSelectedDate)) {
            String strDay = strSelectedDate.subString(8, 10);
            String strMonth = strSelectedDate.subString(5, 7);
            String strYear = strSelectedDate.subString(0, 4);
            strSelectedDate = strDay +EP_Common_Constant.STRING_HYPHEN+strMonth+EP_Common_Constant.STRING_HYPHEN+strYear;
        }
        strSelectedPickupLocationID = newOrderRecord.Stock_Holding_Location__c;
        strSelectedShipToID = newOrderRecord.EP_ShipTo__c;
        //Defect Fix Start #57583
        strSelectedRoute = newOrderRecord.EP_Route__c;
        //Defect Fix End #57583
        strSelectedRun = newOrderRecord.EP_Run__c;
        strSelecteddeliveryType = newOrderRecord.EP_Delivery_Type__c; 
        strCustomerCurrencyCode = orderAccount.CurrencyIsoCode;
        if(newOrderRecord.EP_Related_Bulk_Order__c != NULL){
            strRelatedBulkOrder = newOrderRecord.EP_Related_Bulk_Order__r.OrderNumber__c;
            newOrderRecord.EP_Bulk_Order_Number__c = strRelatedBulkOrder;
        }    
        
    }
    @TestVisible
    private void validateCustomer(){
        EP_GeneralUtility.Log('Private',CLASSNAME,'validateCustomer');
        if(!accountDomainObj.isValidCustomer()){
            hasAccountSetupError = TRUE;
            isAccountFound = FALSE;
            if(accountDomainObj.isSellToBlocked){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Sell_To_Blocked_Order_Error));
            }
            else if(accountDomainObj.isBillToBlocked){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.EP_Bill_To_Blocked_Order_Error));
            }
        }   
    }

    public List < csord__Order_Line_Item__c > getOrderItemsIdListFromWrapper(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderItemsIdListFromWrapper');
        List < csord__Order_Line_Item__c > listOfOrderItems = new List < csord__Order_Line_Item__c > ();
        for (EP_OrderPageContext.OrderWrapper orWrapper: listofOrderWrapper) {
            csord__Order_Line_Item__c ot = orWrapper.orderLineItem;
            if (ot.orderId__c == NULL) {
                ot.orderId__c = newOrderRecord.Id;
            }
            listOfOrderItems.add(ot);
        }
        return listOfOrderItems;
    }

        //Jyotsna: March 16, 2017: Get rid of this class as it is redundant
    public without sharing class OrderSummaryWrapper {       
        public String oswProductName {get;set;}
        public String oswDescription {get;set;}
        public String oswQuantityUoM {get;set;}
        public String oswCurrencyCode {get;set;}
        
        
        public Double oswUnitPrice {get;set;}
        public Double oswBasePrice {get;set;}
        public Double oswCost {get;set;}
        public double oswTaxPercent {get;set;}
        public double oswTaxAmount {get;set;}

        public Integer oswQuantity {get;set;}
        public Integer oswItemIndex {get;set;}
        
        public Boolean oswIsAdditionalItem {get;set;}
    }
    
    public without sharing class OrderWrapper {

        public csord__Order_Line_Item__c orderLineItem {get;set;}
        public List < csord__Order_Line_Item__c > listChildOrderLineItems {get;set;}
        public List < SelectOption > oliAvailableProducts {get;set;}
        public List < SelectOption > oliAvailableQuantities {get;set;}
        public List <SelectOption> oliAvailableTanks {get;set;}
        public List < SelectOption > contractOptions{get;set;}

        public Integer oliIndex {get;set;}
        public Integer oliQuantity {get;set;}
        public Integer oliTankSafeFillLvl {get;set;}

        public Double oliTotalPrice {get;set;}
        public Decimal oliOtherCosts {get;set;}
        
        public Boolean oliIsValid {get;set;}
        
        public String oliPricebookEntryID {get;set;}
        public String oliProductName {get;set;}
        public String oliProductError {get;set;}
        public String oliQuantityRange {get;set;}
        public String oliQuantityError {get;set;}
        public String oliProductUoM {get;set;}
        public String oliTanksID {get;set;}
        public String oliTanksName {get;set;}
        public String oliTankProductName {get;set;}
        public String supplierId{get;set;}            
        public String contractId{get;set;}            
        public Boolean isNew {get;set;}//for modification
        public OrderWrapper() {
            EP_GeneralUtility.Log('Public','OrderWrapper','OrderWrapper');
            orderLineItem = new csord__Order_Line_Item__c();
            listChildOrderLineItems = new List < csord__Order_Line_Item__c > ();
            oliAvailableProducts = new List<SelectOption>();
            oliIsValid = false;
            isNew = true;
        }
    }
}