/*
 *  @Author <Accenture>
 *  @Name <EP_ASTVendorActiveToBlocked>
 *  @CreateDate <6/2/2017>
 *  @Description <Handles Vendor Account status change from Active to Blocked>
 *  @Version <1.0>
 */
 public with sharing class EP_ASTVendorActiveToBlocked  extends EP_AccountStateTransition{
    public EP_ASTVendorActiveToBlocked() {
        finalState = EP_AccountConstant.Blocked;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isGuardCondition');
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.isAccountBlockable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage('Cannot block the Transporter or 3rd Party Stock Supplier as there is one or more open Orders assigned to this record');
            return false;
        }           
        return true;
    }
    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','doOnExit');

    }
}