/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateCreditReview.cls>     
     @Description <Order State for Awaiting credit review status, common for all order types. Create a seperate classes if logic differs based on order type>   
     @Version <1.1> 
     */

    public class EP_OrderStateCreditReview extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCreditReview','doTransition');
            return super.doTransition();
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCreditReview','doOnEntry');
            
        }
        public override boolean isInboundTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCreditReview','isInboundTransitionPossible');
            //Override this with all possible guard conditions that permits the state transition possible
            //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
            return super.isInboundTransitionPossible();
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Awaiting_Credit_Review;
        }
    }