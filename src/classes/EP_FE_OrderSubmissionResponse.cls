/* 
  @Author <Nicola Tassini>
   @name <EP_FE_OrderSubmissionResponse>
   @CreateDate <20/04/2016>
   @Description <  >  
   @Version <1.0>
*/
global with sharing class EP_FE_OrderSubmissionResponse extends EP_FE_Response {
    // FROM EP_Response next data will be transferred 
    // public List<String> errors {get;set;} - list of errors why order cannot be created
    
    global static final Integer ERROR_ORDER_NOT_FOUND = -1;
    global static final Integer ERROR_UPDATING_ORDER = -2;
    global static final Integer ERROR_ORDER_STATUS_SHOULD_BE_DRAFT_FOR_UPDATING_ORDER = -3;

    global static final String CLASSNAME = 'EP_FE_OrderSubmissionEndpoint';
    global static final String METHOD = 'updateOrder';
    global final static String SEVERITY = 'PE-192';
    global final static String TRANSACTION_ID = 'ERROR';
    global final static String DESCRIPTION1 = 'Error occured while updating Order';
    global final static String DESCRIPTION2 = 'Order Status should be DRAFT for updating the Order';
    global final static String DESCRIPTION3 = 'Order not found';
        
    public Boolean isSuccess;
    public Id newOrderId; 
    public String newOrderNumber;
    public Boolean pastDueInvoiceFlag {get; set;}
    //In case when we expect to create multiple this should be replaced with list of Ids and Strings;
}