/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OSTDraftToDraft>
 *  @CreateDate <03/02/2017>
 *  @Description <Handles order status change from draft to draft>
 *  @Version <1.0>
 */

 public class EP_OSTDraftToDraft extends EP_OrderStateTransition{
    
    public EP_OSTDraftToDraft(){
        finalState = EP_OrderConstant.OrderState_Draft;
    } 
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTDraftToDraft','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTDraftToDraft','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_OrderService orderService = new EP_OrderService(order);
        EP_GeneralUtility.Log('Public','EP_OSTDraftToDraft','isGuardCondition');
        system.debug('In isGuardCondition of EP_OSTDraftToDraft' + order.isOrderSellToInvoiceOverdue() + orderService.hasCreditIssue());
        //Defect 57798 - START - If pricing status is Failure, set Order status back to Draft.
        if(EP_Common_constant.PRICING_STATUS_FAILURE.equalsIgnoreCase(this.order.getOrder().EP_Pricing_Status__c)){
            orderEvent.setEventMessage(EP_OrderConstant.PRICING_REQUEST_FAILED);
           return true;
        }
        //Defect 57798 - END        
        if (this.order.isOrderSellToInvoiceOverdue()){
           if(this.order.isRetrospective()){
                EP_PortalOrderUtil.createCreditExcRequest(order.getOrder().id,order.getOrder().accountid__c);
            }
            orderEvent.setEventMessage(EP_OrderConstant.OVERDUE_MESSAGE);
           return true;
        }
        
        //Defect:-57816--Start
        if(orderService.hasCreditIssue()){
        //Defect:-57816--End
            if(this.order.isRetrospective()){
                EP_PortalOrderUtil.createCreditExcRequest(order.getOrder().id,order.getOrder().accountid__c);
            }
            orderEvent.setEventMessage(EP_OrderConstant.CREDITISSUE_MESSAGE);
            return true;
        }
        
        return false;               
    }
  }