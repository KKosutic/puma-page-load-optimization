public with sharing class EP_OrderControllerHelper{
    public EP_OrderPageContext ctx ;
    public static final String CLASSNAME = 'EP_OrderControllerHelper';
    public EP_OrderControllerHelper(EP_OrderPageContext ctx){
        this.ctx = ctx;
            
    }
    
    public List < EP_OrderPageContext.OrderSummaryWrapper > getOrderSummaryItems() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderSummaryItems');
        List < EP_OrderPageContext.OrderSummaryWrapper > orderItems = new List < EP_OrderPageContext.OrderSummaryWrapper > ();
        if( ctx.newOrderRecord.Id == NULL || !ctx.isPriceCalculated) {         
            createSummaryfromLocalWrapper(orderItems);
        }
        else{
            fetchOrderLineItemSummary(orderItems);
        }
        return orderItems;
    }
    @TestVisible
    private void createSummaryfromLocalWrapper(List < EP_OrderPageContext.OrderSummaryWrapper > orderItems){
        double dblTotalCost;
        String strCurrencyCode;
        EP_OrderPageContext.OrderSummaryWrapper orderItem;
        Integer intIndex = 1;
        for (EP_OrderPageContext.OrderWrapper orderWrapper: ctx.listofOrderWrapper) {
            dblTotalCost = orderWrapper.oliTotalPrice;
            strCurrencyCode = ctx.newOrderRecord.CurrencyIsoCode;
            orderItem = populateOrderSummaryItem(orderWrapper.oliProductName,orderWrapper.oliQuantity,orderWrapper.orderLineItem.EP_Quantity_UOM__c,
            0,0,dblTotalCost,strCurrencyCode,intIndex,FALSE,EP_Common_Constant.BLANK,0,0);
            orderItems.add(orderItem);                    
            intIndex++;
        }
    }   
    
    public void setShipToDetails() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setShipToDetails');
        //ctx.orderDomainObj.getShipTos(ctx.strSelectedAccountID);
        List<Account> listOfShipTos = ctx.orderDomainObj.getShipToAccounts();
        ctx.listOfShipToOptions = new List <SelectOption> (); 
        ctx.mapOfShipTo = new Map <Id, Account>();
        for (Account shipToAcc : listOfShipTos) {
            ctx.mapOfShipTo.put(shipToAcc.Id, shipToAcc);
            ctx.listOfShipToOptions.add(EP_Common_Util.createOption(shipToAcc.ID, shipToAcc.Name));
        }
        if (listOfShipTos.isEmpty()) {
            ctx.hasAccountSetupError = true;
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Info, Label.EP_No_SupplyLocation_At_SellTo);
            ApexPages.addMessage(msg);
        }                                                  
    }
    
    
    
    public pagereference loadStep1() {
        EP_GeneralUtility.Log('public',CLASSNAME,'loadStep1');
        ctx.newOrderRecord.EP_Delivery_Type__c = ctx.strSelectedDeliveryType;
        ctx.newOrderRecord.csord__Account__r = ctx.orderAccount;
        ctx.isPackagedOrder = ctx.orderDomainObj.isPackaged();
        ctx.isBulkOrder = ctx.orderDomainObj.isBulk(); 
        if(!ctx.isDummy && ctx.orderAccount.EP_Is_Customer_Reference_Visible__c) {
            ctx.showCustomerPoField = true;
        }
        // Packaged to be Removed      
        if(hasNoPackageGoodsPaymentTerm()) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, LABEL.EP_No_Package_Goods_Pymnt_Term));
            return null;
        }
        // For Ex-Rack orders, skip the ship to selection step (requirement HLBR-E2E 001.50)
        //if (!isDeliveryOrder && !ctx.isDummy && !ctx.isConsumptionOrder) { // CCO Work don't skip the ship to selection in case of CCO Work 
         if(ctx.isExrack){
            ctx.intOrderCreationStepIndex++;
        }else{
            setShipToDetails(); 
        }
        return null;
    }
    public PageReference loadStep2() {
        EP_GeneralUtility.Log('public',CLASSNAME,'loadStep2');
        ctx.listOfSupplyLocationOptions = new List < SelectOption > ();
        ctx.isRoOrder = ctx.orderDomainObj.isRetrospective();
        ctx.setshiptodetailsonOrder();
        System.debug(ctx.mapShipToTanks+'******'+ctx.mapShipToOperationalTanks+'***'+ctx.isDeliveryOrder);
        if(!ctx.mapShipToTanks.isEmpty() && ctx.mapShipToOperationalTanks.isEmpty() && ctx.isDeliveryOrder) { 
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_No_Operational_Tanks)); 
            ctx.intOrderCreationStepIndex = 2;
            return null; 
        }
        if(!ctx.mapShipToOperationalTanks.isEmpty() && ctx.isBulkOrder && ctx.isDeliveryOrder) {
            ctx.isOprtnlTankAvail = true;
        }
        Account applicableAccount = ((ctx.isDeliveryOrder || ctx.isDummy) ? ctx.newOrderRecord.EP_ShipTo__r : ctx.orderAccount);
        EP_AccountService shipToAccountService = new EP_AccountService(new EP_AccountDomainObject(applicableAccount));
        List <EP_Stock_Holding_Location__c> listOfStockHoldingLocations = shipToAccountService.getSupplyLocations();
        populatesupplylocations(listOfStockHoldingLocations); 
        // If there are no storage locations on selected ship to, then raise an error
        doActionValidateStockHolidingLocations();
         // End list check
        return null;
    }
    /*
    Show Summary from PE
    */
    @TestVisible
    private void fetchOrderLineItemSummary(List < EP_OrderPageContext.OrderSummaryWrapper > orderItems) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'showSummaryFromPE');
        EP_OrderPageContext.OrderSummaryWrapper orderSummaryObj;
        Integer intIndex = 1;
        ctx.totalCostPE = 0;
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        List<csord__Order_Line_Item__c> listOrderItem = orderItemMapper.getCSNoParentRecsByOrderIds(new Set<Id>{ctx.newOrderRecord.Id});  
        for(csord__Order_Line_Item__c orderItemObj : listOrderItem) {
            ctx.totalCostPE = ctx.totalCostPE+Double.valueOf(orderItemObj.EP_Total_Price__c); 
            orderSummaryObj = populateOrderSummaryItem(orderItemObj.EP_Product__r.Name ,Integer.valueOf(orderItemObj.Quantity__c),orderItemObj.EP_Quantity_UOM__c,
            Double.valueOf(orderItemObj.UnitPrice__c),Double.valueOf(orderItemObj.EP_Pricing_Total_Amount__c),
            Double.valueOf(orderItemObj.EP_Total_Price__c),orderItemObj.CurrencyIsoCode,intIndex,FALSE,orderItemObj.EP_Invoice_Name__c,
            Double.valueOf(orderItemObj.EP_Tax_Percentage__c),Double.valueOf(orderItemObj.EP_Tax_Amount__c));
            orderItems.add(orderSummaryObj);
            //populateChildrenLineItems(orderItemObj.order_Products__r,orderSummaryObj,orderItems);

            intIndex++;
        }  
    }

    @TestVisible
    private void populateChildrenLineItems(list<OrderItem> OrderItemList,EP_OrderPageContext.OrderSummaryWrapper orderSummaryObj,List < EP_OrderPageContext.OrderSummaryWrapper > orderItems){
        for( OrderItem orderItem : OrderItemList ) {
            ctx.totalCostPE = ctx.totalCostPE+Double.valueOf(orderItem.EP_Total_Price__c);
            orderSummaryObj = populateOrderSummaryItem(orderItem.pricebookentry.product2.name,Integer.valueOf(orderItem.Quantity),NULL,
            Double.valueOf(orderItem.EP_Pricing_Total_Amount__c),Double.valueOf(orderItem.UnitPrice),
            Double.valueOf(orderItem.EP_Total_Price__c),orderItem.CurrencyIsoCode,NULL,TRUE,
            orderItem.EP_Invoice_Name__c,Double.valueOf(orderItem.EP_Tax_Percentage__c),
            Double.valueOf(orderItem.EP_Tax_Amount__c));
            orderItems.add(orderSummaryObj);
        }
    }   
     
    /*
    Get All Available dates
    */
    
    /*
    This method is used to get contracts based on the supplier RO Work
    */ 
    public List<SelectOption> supplierContracts(String supplierId){
        EP_GeneralUtility.Log('Public',CLASSNAME,'supplierContracts');
        List<SelectOption> contractList = new List<SelectOption>();
        try{
            contractList.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
            Date loadingDate = ctx.newOrderRecord.EP_Loading_Date__c.date();
            if(!ctx.supplierContractMap.containsKey(supplierId)){
                return contractList;
            }
            for(Contract con : ctx.supplierContractMap.get(supplierId)){
                if(isValidLoadingDate(con,loadingDate)){
                    contractList.add(EP_Common_Util.createOption(con.Id,con.ContractNumber));
                }
            }
        }catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.SupplierContracts_Method, CLASSNAME, ApexPages.Severity.ERROR);
        }    
        return contractList;
    } 

    @TestVisible
    private EP_OrderPageContext.OrderSummaryWrapper populateOrderSummaryItem(String strProductName,Integer intQuantity,
    String strQuantityUoM,Double basePrice,
    Double unitPrice,Double dblCost,
    String strCurrencyCode,Integer intIndex,
    Boolean isAdditionalItem,String description,
    Double taxPercentage,Double taxAmount) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'populateOrderSummaryItem');
        EP_OrderPageContext.OrderSummaryWrapper orderItem;
        orderItem = new EP_OrderPageContext.OrderSummaryWrapper();
        orderItem.oswProductName = strProductName;
        
        if (!isAdditionalItem) {
            orderItem.oswQuantity = intQuantity;
            orderItem.oswItemIndex = intIndex;
        }
        orderItem.oswBasePrice = basePrice;
        orderItem.oswUnitPrice = unitPrice;
        orderItem.oswCost = dblCost;
        orderItem.oswCurrencyCode = strCurrencyCode;
        orderItem.oswQuantityUoM = strQuantityUoM;
        orderItem.oswIsAdditionalItem = isAdditionalItem;
        orderItem.oswDescription= description;
        orderItem.oswTaxPercent = taxPercentage;
        orderItem.oswTaxAmount = taxAmount;
        return orderItem;
    }

    @TestVisible
    private boolean isValidLoadingDate(Contract con, date loadingDate){
        return (con.startDate != NULL && con.EndDate != NULL && loadingDate >= con.startDate && loadingDate <= con.EndDate);
    }
    
    /*
    Set Order values
    */
    public void setOrderDetails(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderDetails');
        ctx.newOrderRecord.AccountId__c = ctx.strSelectedAccountID;
        ctx.newOrderRecord.EffectiveDate__c = System.Today();
        if (ctx.newOrderRecord.Status__c == '' || ctx.newOrderRecord.Status__c == NULL) {
            ctx.newOrderRecord.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        }
        ctx.newOrderRecord.PriceBook2Id__c = ctx.strSelectedPricebookID;
        ctx.newOrderRecord.EP_Route__c = ctx.strSelectedRoute;
        ctx.newOrderRecord.EP_Run__c = ctx.strSelectedRun;
        ctx.newOrderRecord.EP_Run_Id__c = String.ValueOf(ctx.runMap.get(ctx.strSelectedRun)); 
        ctx.newOrderRecord.EP_Payment_Term__c = ctx.strSelectedPaymentTerm;//Defect 57275 
    }
    
    
    
    public EP_Stock_Holding_Location__c getStockHolderLocation(Id stockHoldingLocationId){
            EP_GeneralUtility.Log('Public',CLASSNAME,'getStockHolderLocation');
            EP_Stock_Holding_Location__c selectedPickupDetail;
            set<Id> setOfSupplyOptionId = new set<Id>();
            setOfSupplyOptionId.add((id)stockHoldingLocationId);
            EP_StockHoldingLocationMapper stockLocationMapperObject = new EP_StockHoldingLocationMapper();
            list<EP_Stock_Holding_Location__c> listOfStockLocations = stockLocationMapperObject.getRecordsByIds(setOfSupplyOptionId, EP_Common_Constant.ONE);
            if(!listOfStockLocations.isEmpty()){
                selectedPickupDetail = listOfStockLocations[0];
            }
            return selectedPickupDetail;
        }
    
    
    
    
    public boolean isOrderPricingInvokable(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'isOrderPricingInvokable');
        return (ctx.pricingPollerCount==1 || ctx.pricingPollerCount==10 || (ctx.pricingPollerCount > 10 && (math.mod(ctx.pricingPollerCount,2) == 0)));
    }
    
    public void populatesupplylocations(list<EP_Stock_Holding_Location__c> listOfStockHoldingLocations){
        ctx.mapOfSupplyLocation = new Map < String, EP_Stock_Holding_Location__c > ();
        //ctx.listOfSupplyLocationOptions.add(new SelectOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        for (EP_Stock_Holding_Location__c supplyLocationObject: listOfStockHoldingLocations) {
            if(ctx.newOrderRecord.Stock_Holding_Location__c == supplyLocationObject.ID && ctx.listOfSupplyLocationOptions.size() > 0){
                ctx.listOfSupplyLocationOptions.add(0,new SelectOption(supplyLocationObject.ID, supplyLocationObject.Stock_Holding_Location__r.Name));
            }
            else{
                ctx.listOfSupplyLocationOptions.add(new SelectOption(supplyLocationObject.ID, supplyLocationObject.Stock_Holding_Location__r.Name));
            }
            ctx.mapOfSupplyLocation.put(supplyLocationObject.id, supplyLocationObject);
            
        }
    }
    public void doActionValidateStockHolidingLocations(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doActionValidateStockHolidingLocations');
        if (ctx.listOfSupplyLocationOptions.isEmpty()){
            ctx.hasAccountSetupError = true;
            ApexPages.Message msg;
            if(ctx.isDeliveryOrder || ctx.isDummy || ctx.isConsumptionOrder){
                msg = EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_SupplyLocation_At_ShipTo);
            }
            else{ // CCO Work
                msg = EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_SupplyLocation_At_SellTo); 
            }
            ApexPages.addMessage(msg);
        }
    }
    public void removeOrderLineItem(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'removeOrderLineItem');
        set<EP_OrderPageContext.OrderWrapper> updateOrderWrappers = new set<EP_OrderPageContext.OrderWrapper>(ctx.listofOrderWrapper);
        EP_OrderPageContext.OrderWrapper Ordertoremove = getOrderWapperforIndex(ctx.strSelectedOrderLineItem);
        updateOrderWrappers.remove(Ordertoremove);
        ctx.orderItemToRemoveSet.add(Ordertoremove.orderLineItem.Id);//Defect #58835
        ctx.listofOrderWrapper = new list<EP_OrderPageContext.OrderWrapper>(updateOrderWrappers);
        if(ctx.newOrderRecord.ID != null && Ordertoremove.orderLineItem.Id != null){
            delete Ordertoremove.orderLineItem;
        }
        else{
            if( ctx.newOrderRecord.ID != Null ){
                ctx.isPriceCalculated = true;
            }
        }
    }
    
    
    public boolean hasNoPackageGoodsPaymentTerm(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'hasNoPackageGoodsPaymentTerm');
        return (EP_Common_Constant.PRODUCT_PACKAGED.equalsIgnoreCase(ctx.newOrderRecord.EP_Order_Product_Category__c) && !ctx.isDummy && ctx.invoicedAccount != NULL && ctx.invoicedAccount.EP_Package_Payment_Term__c == NULL);
    }
    
    
    public void doSubmitActionforNewOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doSubmitActionforNewOrder');
        if( ctx.newOrderRecord.Id == NULL ){
            ctx.orderService.doSubmitActions();
            insert ctx.newOrderRecord;
        }
        //Defect 57453 Start    
        ctx.orderDomainObj = new EP_orderDomainObject(ctx.newOrderRecord.Id);
        ctx.orderService = new EP_orderService(ctx.orderDomainObj);  
        //Defect 57453 End           
    
    }
    
    public void setOrderItemsinDomain(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderItemsinDomain');
        List <csord__Order_Line_Item__c> orderItems = new List <csord__Order_Line_Item__c> ();
        for (EP_OrderPageContext.OrderWrapper orderWrapperObj: ctx.listofOrderWrapper) {
            csord__Order_Line_Item__c orderItemObj = orderWrapperObj.orderLineItem;
            if (orderItemObj.orderId__c == NULL && orderItemObj.id == null) {
                orderItemObj.orderId__c = ctx.newOrderRecord.Id;
                                  
            }
            orderItems.add(orderItemObj);  
        }
        if (!orderItems.isEmpty()) {
            upsert orderItems; // Move to domain setOrderItems
            ctx.orderDomainObj.setOrderItems(orderItems);
        }
    }
    
    
    public pagereference updateOrder(EP_OrderEvent orderEvent){
        EP_GeneralUtility.Log('Public',CLASSNAME,'updateOrder');
        PageReference ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);           
        if(ctx.newOrderRecord.Status__c == EP_Common_Constant.ORDER_DRAFT_STATUS) {
            ctx.hasOverdueInvoices = orderEvent.getEventMessage() == EP_OrderConstant.OVERDUE_MESSAGE;
            ctx.hasCreditIssues = orderEvent.getEventMessage() == EP_OrderConstant.CREDITISSUE_MESSAGE;
            if((ctx.hasCreditIssues || ctx.hasOverdueInvoices) && !ctx.isRoOrder) {                    
                ref = setPageReferenece();
            }
        }
        else if (ctx.isPrepaymentCustomer && !ctx.isConsignmentOrder){
            ctx.newOrderRecord.EP_Payment_Term__c = EP_Common_Constant.PREPAYMENT; 
        }
        update ctx.newOrderRecord;
        //Defect:-57816--Start 
        if (ctx.newOrderRecord.EP_Overdue_Amount__c > 0 && (EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(ctx.newOrderRecord.EP_Payment_Term__c))) { 
            ctx.IsPrepayBillToOVerdue = true;
            ref = setPageReferenece();
        }
        //Defect:-57816--End 
        return ref;    
    }
    
     private pagereference setPageReferenece(){
         PageReference ref = new PageReference(EP_Common_Constant.PORTAL_ORDER_PAGE  + ctx.newOrderRecord.Id);
         if(ctx.isModification){
             ref = null;    
         }
         return ref;
     }
     
     
   public EP_OrderPageContext.OrderWrapper getOrderWapperforIndex(string orderLineItemIndex){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderWapperforIndex');
        for(EP_OrderPageContext.OrderWrapper orderWrapperObj :  ctx.listofOrderWrapper) {
            if(string.valueof(orderWrapperObj.oliIndex) ==  orderLineItemIndex) {
                return orderWrapperObj;
            }
        }
        return null;
    }
    
    public void setRelatedBulkOrderDetailsonPackagedOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderWapperforIndex');
        if(ctx.isPackagedOrder && !String.isBlank(ctx.strRelatedBulkOrder)) {
            ctx.newOrderRecord.EP_Related_Bulk_Order__c = ctx.bulkRelatedOrderMap.get(ctx.strRelatedBulkOrder).id;
            ctx.newOrderRecord.EP_Bulk_Order_Number__c = ctx.strRelatedBulkOrder;                          
        }
        else{
            ctx.newOrderRecord.EP_Related_Bulk_Order__c= NULL;
            ctx.newOrderRecord.EP_Bulk_Order_Number__c = EP_Common_Constant.BLANK;
        }
    }
    
    public boolean hasInValidDatesOnROOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'hasInValidDatesOnROOrder');
        return (!ctx.isConsumptionOrder && ctx.isRoOrder && (ctx.newOrderRecord.EP_Loading_Date__c == NULL || ctx.newOrderRecord.EP_Order_Date__c == NULL || ctx.newOrderRecord.EP_Expected_Delivery_Date__c == NULL ));
    }
    
    
    public void setPageVariablesforModificationController(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setPageVariablesforModificationController');
        ctx.strSelectedPickupLocationID = ctx.newOrderRecord.Stock_Holding_Location__c;
        ctx.strSelectedShipToID = ctx.newOrderRecord.EP_ShipTo__c;
        //Defect Fix Start #57583
        ctx.strSelectedRoute = ctx.newOrderRecord.EP_Route__c;
        //Defect Fix End #57583
        ctx.strSelectedRun = ctx.newOrderRecord.EP_Run__c;
        ctx.strSelecteddeliveryType = ctx.newOrderRecord.EP_Delivery_Type__c; 
        ctx.strCustomerCurrencyCode = ctx.orderAccount.CurrencyIsoCode;
        if(ctx.newOrderRecord.EP_Related_Bulk_Order__c != NULL){
            ctx.strRelatedBulkOrder = ctx.newOrderRecord.EP_Related_Bulk_Order__r.OrderNumber__c;
            ctx.newOrderRecord.EP_Bulk_Order_Number__c = ctx.strRelatedBulkOrder;
        }    
        
    }
    public PageReference setOrderStatustoCancelled(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderStatustoCancelled');
        PageReference ref = NULL;
        if (ctx.newOrderRecord.Id == NULL) {
            return ref;
        }
        EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        boolean isStatusChanged = ctx.orderService.setOrderStatus(orderEvent);
        if(isStatusChanged){
            Database.Update(ctx.newOrderRecord);
            ctx.orderService.doPostStatusChangeActions();
        }
        ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);
        return ref;
    }
    
    public pagereference saveOrderAsDraft(){
        EP_GeneralUtility.Log('public',CLASSNAME,'saveOrderAsDraft');
        PageReference ref; 
        ctx.newOrderRecord.EP_Credit_Status__c = null;
        ctx.newOrderRecord.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        ctx.newOrderRecord.EP_Payment_Term__c = EP_Common_Constant.Blank;
        //ctx.newOrderRecord.RecordTypeId = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_DRAFT_ORDER_RECORD_TYPE_DEV_NAME).id;
        update ctx.newOrderRecord;
        ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);
        return ref.setRedirect(false);
        
    }
    
    public void getpricing(){
        EP_GeneralUtility.Log('public',CLASSNAME,'getpricing');
        if(!isOrderPricingInvokable()) {
            ctx.pricingPollerCount++;  
            return;
        }
        csord__Order__c orderObj = ctx.orderMapper.getCSOrderPricing(ctx.newOrderRecord.Id);            
        if(EP_Common_Constant.PRICED.equalsIgnoreCase(orderObj.EP_Pricing_Status__c)) {    
            ctx.pricingPollerCount = 120;
            ctx.isPricingRecieved = true;                
        }
        else if(EP_Common_Constant.FAILURE.equalsIgnoreCase(orderObj.EP_Pricing_Status__c)){
            //if( orderObj.EP_Error_Description__c <> Null &&  orderObj.EP_Error_Description__c.contains(EP_Common_Constant.DOT)){
            ctx.newOrderRecord.EP_Error_Description__c  = orderObj.EP_Error_Description__c.split(EP_Common_Constant.SPLIT)[0];       
            //}             
            ctx.pricingPollerCount = 120;
        }
                                     
            
        ctx.pricingPollerCount++;  
    }
    
    public void doUpdateSupplierContracts(){
        EP_GeneralUtility.Log('public',CLASSNAME,'doUpdateSupplierContracts');
        EP_OrderPageContext.OrderWrapper orderWrapperObj = getOrderWapperforIndex(String.valueOf(ctx.intOliIndex)); // #defectFix 57556        
        ValidateLoadingdate(orderWrapperObj);
        setTankDetailsonWrapper(orderWrapperObj);        
    }
    
    @TestVisible
    private void ValidateLoadingdate(EP_OrderPageContext.OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Private',CLASSNAME,'ValidateLoadingdate');
        if(ctx.newOrderRecord.EP_Loading_Date__c != NULL) {
            orderWrapperObj.contractOptions = supplierContracts(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c);
            orderWrapperObj.oliProductName = ctx.productNameMap.get(orderWrapperObj.oliPricebookEntryID);
        }else{
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, LABEL.EP_Select_Loading_Date_first));
        }
    }
    @TestVisible
    private void setTankDetailsonWrapper(EP_OrderPageContext.OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Private',CLASSNAME,'setTankDetailsonWrapper');
        if(!ctx.isOprtnlTankAvail || orderWrapperObj.oliTanksID == NULL){
            return;
        }
        EP_Tank__c tankobj = ctx.mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID);
        orderWrapperObj.oliTankProductName = tankobj.EP_Product__r.Name;
        if(tankobj.EP_Safe_Fill_Level__c != null) {
            orderWrapperObj.oliTankSafeFillLvl = Integer.valueOf(tankobj.EP_Safe_Fill_Level__c);
        }
        orderWrapperObj.oliTanksName = tankobj.EP_Tank_Code__c;
        if(tankobj.EP_Tank_Alias__c != NULL) {
            orderWrapperObj.oliTanksName =  orderWrapperObj.oliTanksName+EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
        }
       
    }
    
    
    public pagereference doCancel(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doCancel');
        if(ctx.newOrderRecord.Id != NULL){
            delete ctx.newOrderRecord;
        }
        if (ctx.strSelectedAccountID != NULL) {
            return (new PageReference(EP_Common_Constant.SLASH + ctx.strSelectedAccountID));
        }
        return (new PageReference(EP_Common_Constant.HOME_PAGE_URL));
    }
    
    
    public void doCalculatePrice(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doCalculatePrice');
        setOrderDetails();
        setRelatedBulkOrderDetailsonPackagedOrder();
        if(hasInValidDatesOnROOrder()){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, LABEL.EP_Select_Order_Date_First));
        }
        //Changes for #60147
        ctx.pricingPollerCount = 0;
        //Changes for #60147 End
        ctx.orderDomainObj.setRetroOrderDetails();
        ctx.orderService.setConsumptionOrderDetails();       
        ctx.orderService.doSubmitActions();
        System.debug('********* newOrderRecord' + ctx.newOrderRecord);
        upsert ctx.newOrderRecord;
        setOrderItemsinDomain();
        ctx.orderService.calculatePrice(); 
        ctx.isPriceCalculated = true;
        ctx.isPricingRecieved = false;
    }
    
}