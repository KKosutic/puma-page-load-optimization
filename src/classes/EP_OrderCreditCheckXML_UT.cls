@isTest
private class EP_OrderCreditCheckXML_UT {
   private static final string MESSAGE_TYPE = 'SEND_ORDER_CREDIT_CHECK_IN_PROGRESS';

    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    }

   static testMethod void createXML_Test() {
       EP_OrderCreditCheckXML localObj = new EP_OrderCreditCheckXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void createPayload_Test() {
       EP_OrderCreditCheckXML localObj = new EP_OrderCreditCheckXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }

   static testMethod void createStatusPayLoad_Test() {
       EP_OrderCreditCheckXML localObj = new EP_OrderCreditCheckXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createStatusPayLoad();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }

   static testMethod void setCreditStatus_Test() {
       EP_OrderCreditCheckXML localObj = new EP_OrderCreditCheckXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.setCreditStatus();  
       localObj.messageType = EP_Common_Constant.Blank;
       //to cover else part
       localObj.setCreditStatus();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }
}