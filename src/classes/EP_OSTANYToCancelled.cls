/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToCancelled>
*  @CreateDate <03/02/2017>
*  @Description <Handles order status change from any status to cancelled>
*  @Version <1.0>
*/

public class EP_OSTANYToCancelled extends EP_OrderStateTransition{
  
      public EP_OSTANYToCancelled(){
        finalState = EP_OrderConstant.OrderState_Cancelled;
      }
      
      public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToCancelled','isTransitionPossible');
        return super.isTransitionPossible();
      }
      
      public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToCancelled','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTANYToCancelled');
        return super.isRegisteredForEvent(); 
      }

      public override boolean isGuardCondition(){
          EP_GeneralUtility.Log('Public','EP_OSTANYToCancelled','isGuardCondition');
          //system.debug('In isGuardCondition of EP_OSTANYToCancelled');
          // Check if either its user cancel or exception rejected or cancel update from WINDMS
          
          if(EP_Common_Constant.CREDIT_FAIL.equalsIgnoreCase(this.order.getOrder().EP_Credit_Status__c) 
                || EP_Common_Constant.OVERDUE_INVOICE.equalsIgnoreCase(this.order.getOrder().EP_Credit_Status__c)){
              this.order.getOrder().EP_Sync_With_LS__c = true;
              return true;
          }

          if(!String.isBlank(this.order.getOrder().EP_WinDMS_Status__c) && this.order.getOrder().EP_WinDMS_Status__c.equalsIgnoreCase(EP_Common_Constant.STATUS_CANCELLED)){
            return true;
          }
          
          if(this.orderEvent.getEventName().equalsIgnoreCase(EP_OrderConstant.USER_CANCEL)){
            return true;
          }
          
          if(this.orderEvent.getEventName().equalsIgnoreCase(EP_OrderConstant.UserExceptionRejected)){
            orderEvent.setEventMessage(((this.order).getOrder()).EP_WinDMS_Status__c);
            return true;               
          }
          
          return false;
      }

  }