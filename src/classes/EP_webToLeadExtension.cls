/* 
   @Author <Brajesh Tiwary>
   @name <EP_webToLeadExtension>
   @CreateDate <09/10/2015>
   @Description <This class will handle the incoming Web To Lead page from Puma web site>
   @Version <1.0>
 
   */
   public without sharing class EP_webToLeadExtension{
    private static final String CREATE_CASE_METHOD = 'createCase';
    private static final String SAVE_LEAD = 'saveLead';
    private static final String VALIDATE_MTD = 'validateReqd';
    private static final String MAP_REGION = 'mapRegion';
    private static final String CREATE_CNTRY_PCKLST = 'createCountryPicklist';
    /*PRIVATE MAPS FOR COUNTRY/REGION AND COUNTRY/CURRENCY*/
    private map<String,list<SelectOption>> mapOfCountryIdWithRegionList; //mCntryRegionOption 
    private map<String,String> mapOfCountryIdWithCurrencyISOCode; //mCntryCrncyCode
    /* Lead record */
    public Lead leadObj{get; set;}
    /* Page success flag */
    public boolean isShowConfirmation {get;set;} 
    public Boolean isRegionErr{get;set;}
    public string caseNumber {get; set;}
    public list<SelectOption>listOfCountryNameId{get;set;} 
    public list<selectOption>listOfRegionNameId{get;set;} 
    public String country{get;set;}
    public String deliveryCountry{get;set;} 
    public String currencyCode{get;set;} 
    public string region{get;set;}
    public string fuel{get;set;}
    public string lubes{get;set;}

    /**
     * @author <Ashok Arora>
     * @date  <11/03/2015>
     * @description < Constructor for this class>
     * @param none
     * @return None
     */ 
     public EP_webToLeadExtension(){
      leadObj = new Lead();
      listOfCountryNameId = new list<SelectOption>();
      mapOfCountryIdWithCurrencyISOCode = new map<String,String>();
      mapOfCountryIdWithRegionList = new map<String,list<SelectOption>>();
      listOfRegionNameId = new list<SelectOption>();
      listOfRegionNameId.add(new selectOption(EP_Common_Constant.BLANK
        ,EP_Common_Constant.NONE));
      fuel = EP_Common_Constant.LEAD_PROD_INTERESTED_FUEL;
      lubes = EP_Common_Constant.LEAD_PROD_INTERESTED_LUBES;
      isRegionErr = false; 
    }
    
    /**
     * @author <Ashok Arora>
     * @date  <21/06/2016>
     * @description <Method to create Country and delivery country picklist>
     * @param none
     * @return None
     */
     public void createCountryPicklist(){
      EP_GeneralUtility.Log('Public','EP_webToLeadExtension','createCountryPicklist');
      Integer nRowsInnerQuery = EP_Common_Util.getQueryLimit();
      Integer nRows;
      try{ 
        nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        list<selectOption>listOfRegionNameId;
      //Query country records and check country currency and regions associated 
      for(EP_Country__c oCountry : [SELECT Id, Name, CurrencyIsoCode
        ,(SELECT Id, Name FROM Regions__r Order By Name LIMIT : nRowsInnerQuery)  
        FROM EP_Country__c ORDER BY Name LIMIT : nRows]){
        listOfCountryNameId.add(EP_Common_Util.createOption(oCountry.id,oCountry.name)); //commented to fix novasuite issue : new selectOption(oCountry.id,oCountry.name));
        mapOfCountryIdWithCurrencyISOCode.put(oCountry.id,oCountry.currencyISOCode);
        if(string.isBlank(currencyCode)){
          currencyCode = oCountry.currencyISOCode;
        }
        for(EP_Region__c region : oCountry.regions__r){
          if(mapOfCountryIdWithRegionList.containsKey(oCountry.id)){
            mapOfCountryIdWithRegionList.get(oCountry.id).add(EP_Common_Util.createOption(region.id,region.name)); // commented to fix novasuite issue : new selectOption(region.id,region.name)); 
            }else{
              listOfRegionNameId = new list<SelectOption>();
            listOfRegionNameId.add(EP_Common_Util.createOption(region.id,region.name)); //commented to fix novasuite issue : new selectOption(region.id,region.name));
            mapOfCountryIdWithRegionList.put(oCountry.id,listOfRegionNameId);
          }
        }
      }
      }catch(Exception handledException){
        EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, CREATE_CNTRY_PCKLST, EP_webToLeadExtension.class.getName(), ApexPages.Severity.ERROR);
      }
    }
    /**
     * @author <Ashok Arora>
     * @date  <21/06/2016>
     * @description < Method to map dlvry country and currency based on selected country>
     * @param none
     * @return None
     */
     public void mapCrncyCountry(){
      EP_GeneralUtility.Log('Public','EP_webToLeadExtension','mapCrncyCountry');
      deliveryCountry = country;
      currencyCode = mapOfCountryIdWithCurrencyISOCode.get(country);
      mapRegion();
    }
    
    /**
     * @author <Ashok Arora>
     * @date  <21/06/2016>
     * @description < Method to get regions based on selected country>
     * @param none
     * @return None
     */ 
     public void mapRegion(){
      try{
        EP_GeneralUtility.Log('Public','EP_webToLeadExtension','mapRegion');
          //To get regions based on selected country
          if(mapOfCountryIdWithRegionList.containsKey(deliveryCountry )){
            listOfRegionNameId = mapOfCountryIdWithRegionList.get(deliveryCountry);
          }
          else{
            listOfRegionNameId = new list<SelectOption>();
            listOfRegionNameId.add(new selectOption(EP_Common_Constant.BLANK
              ,EP_Common_Constant.NONE));
          }
        }
        catch(Exception handledException){
          EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, MAP_REGION, EP_webToLeadExtension.class.getName(), ApexPages.Severity.ERROR);
        }
      }
    /**
     * @author <Brajesh Tiwary>
     * @date  <11/03/2015>
     * @description < Method to save lead records>
     * @param none
     * @return None
     */
     public PageReference saveLead(){
      EP_GeneralUtility.Log('Public','EP_webToLeadExtension','saveLead');
        //Database.saveResult svr;
        List<AssignmentRule> aRules;
        Database.DMLOptions dmlOpts;
        //Create Case and insert
        try{
          if(validateReqd()){
                  //Case incase = createCase();
             /* List<AssignmentRule> aRules = EP_Common_Util.getAssignmentRules(EP_Common_Constant.CASE_OBJ); 
              //Creating the DMLOptions for "Assign using active assignment rules" checkbox
              dmlOpts = new Database.DMLOptions();
              if (aRules != null && !aRules.isEmpty()){
                dmlOpts.assignmentRuleHeader.assignmentRuleId= aRules[0].id;
              }
              dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
              incase.setOptions(dmlOpts);
              Database.saveResult svr = Database.insert(incase);*/
              //system.debug('---svr----'+svr);
              /*if(svr.isSuccess()){
                  Query case Number to show in confirmation message.
                  List<case> tempCase = new List<case>([Select CaseNumber from case where id =: svr.Id limit :EP_Common_Constant.ONE]);
                  system.debug('---tempCase----'+tempCase);
                  if(!tempCase.isEmpty()){
                    caseNumber = tempCase[0].CaseNumber ;
                  }
                  //Assign created case Id to Lead Object
              }else{
                system.debug('======='+svr.getErrors());
                }*/
              //leadObj.Case__c = incase.Id;
              leadObj.LeadSource = EP_Common_Constant.LEAD_ORIGIN_WEB;
              if(String.isNotBlank(region)){
                leadObj.EP_Region__c = region;
              }
              if(String.isNotBlank(country)){
                leadObj.EP_CountryLookUp__c= country;
              }
              if(String.isNotBlank(deliveryCountry)){
                leadObj.EP_Delivery_Country__c = deliveryCountry;
              }
              aRules = aRules = EP_Common_Util.getAssignmentRules(EP_Common_Constant.LEAD_OBJ);
              //Creating the DMLOptions for "Assign using active assignment rules" checkbox
              dmlOpts = new Database.DMLOptions();
              if (aRules != null && !aRules.isEmpty()){
                dmlOpts.assignmentRuleHeader.assignmentRuleId= aRules[0].id;
              }
              dmlOpts.EmailHeader.triggerUserEmail = true;
              leadObj.setOptions(dmlOpts);
              // Insert Lead Records
              Database.insert(leadObj);
              List<Lead> tempLead= new List<Lead>([Select Case__r.CaseNumber FROM lead WHERE id =: leadObj.id LIMIT :EP_Common_Constant.ONE]);
              caseNumber = tempLead[0].case__r.CaseNumber;
              isShowConfirmation = true;
            }
            }catch(Exception handledException){  
             EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, SAVE_LEAD, EP_webToLeadExtension.class.getName(), ApexPages.Severity.ERROR);
             ApexPages.addMessages(handledException);
             // blank case Number
             caseNumber = EP_Common_Constant.BLANK;
             isShowConfirmation = false;
           }
           return null;
         }
    /**
     * @author <Ashok Arora>
     * @date  <19/04/2016>
     * @description < Method to validate required fields >
     * @param none
     * @return None
     */
     public Boolean validateReqd(){
      EP_GeneralUtility.Log('Public','EP_webToLeadExtension','validateReqd');
      Boolean isValid = true;
      isRegionErr = false;
      try{
          //Validating required fields
          if(String.isBlank(leadObj.LastName)){
            leadObj.LastName.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.FirstName)){
            leadObj.FirstName.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.Title)){
            leadObj.Title.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }     
          if(String.isBlank(leadObj.EP_Preferred_Mode_of_Communication__c)){
            leadObj.EP_Preferred_Mode_of_Communication__c.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.EP_Preferred_Time_of_Communication__c)){
            leadObj.EP_Preferred_Time_of_Communication__c.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.Street)){
            leadObj.Street.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.City)){
            leadObj.City.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.State)){
            leadObj.State.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.PostalCode)){
            leadObj.PostalCode.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }
          if(String.isBlank(leadObj.Email)){
            leadObj.Email.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          } 
          if(String.isBlank(leadObj.Company)){
            leadObj.Company.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          } 
          if(String.isBlank(leadObj.Phone)){
            leadObj.Phone.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          }      
          if(String.isBlank(leadObj.EP_Requested_Payment_Method__c)){
            leadObj.EP_Requested_Payment_Method__c.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          } 
          if(String.isBlank(leadObj.EP_Requested_Payment_Terms__c)){
            leadObj.EP_Requested_Payment_Terms__c.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          } 
          if(String.isBlank(leadObj.EP_Products_Interested_In__c)){
            leadObj.EP_Products_Interested_In__c.adderror(System.label.EP_Reqd_Field_Err);
            isValid = false;
          } 
            /*if(String.isNotBlank(leadObj.EP_Products_Interested_In__c)
                && leadObj.EP_Products_Interested_In__c.contains(EP_Common_Constant.LEAD_PROD_INTERESTED_LUBES)
                && leadObj.EP_Indicative_Lubes_Volume__c == null){
                  leadObj.EP_Indicative_Lubes_Volume__c.adderror(System.label.EP_Reqd_Field_Err);
                  isValid = false;         
            } 
            if(String.isNotBlank(leadObj.EP_Products_Interested_In__c)
                && leadObj.EP_Products_Interested_In__c.contains(EP_Common_Constant.LEAD_PROD_INTERESTED_LUBES)
                && String.isBlank(leadObj.EP_Indicative_Order_Frequency_Lubes__c)){
                  leadObj.EP_Indicative_Order_Frequency_Lubes__c.adderror(System.label.EP_Reqd_Field_Err);
                  isValid = false;
            } 
            if(String.isNotBlank(leadObj.EP_Products_Interested_In__c)
                && leadObj.EP_Products_Interested_In__c.contains(EP_Common_Constant.LEAD_PROD_INTERESTED_FUEL)
                && leadObj.EP_Indicative_Fuel_Volume__c == null){    
                  leadObj.EP_Indicative_Fuel_Volume__c.adderror(System.label.EP_Reqd_Field_Err);
                  isValid = false;                
            } 
            if(String.isNotBlank(leadObj.EP_Products_Interested_In__c)
                && leadObj.EP_Products_Interested_In__c.contains(EP_Common_Constant.LEAD_PROD_INTERESTED_FUEL)
                && String.isBlank(leadObj.EP_Indicative_Order_Frequency__c) ){   
                  leadObj.EP_Indicative_Order_Frequency__c.adderror(System.label.EP_Reqd_Field_Err);
                  isValid = false;     
                  } */
                  if( String.isBlank(leadObj.EP_Delivery_Type__c)){             
                    leadObj.EP_Delivery_Type__c.adderror(System.label.EP_Reqd_Field_Err);
                    isValid = false;                
                  } 
                  if(String.isBlank(leadObj.EP_Indicative_Total_Pur_Value_Per_Yr__c)){
                    leadObj.EP_Indicative_Total_Pur_Value_Per_Yr__c.adderror(System.label.EP_Reqd_Field_Err);
                    isValid = false;
                  }
                  if(String.isBlank(region)){
                    isValid = false;
                    isRegionErr = true;
                  }    
                  }catch(Exception handledException){
                    EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, VALIDATE_MTD , EP_webToLeadExtension.class.getName(), ApexPages.Severity.ERROR);
                  }       
                  return isValid;
                }
              }