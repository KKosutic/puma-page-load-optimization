@isTest
public class EP_TankSyncCtrlExtn_UT
{
	@testSetup static void init() {
      List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    }
    
    static testMethod void setHeader_test() {
        Account accountObj = EP_TestDataUtility.getShipToPositiveScenario();
        EP_tank__c tank = [SELECT Id FROM EP_tank__c LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(tank);
        EP_TankSyncCtrlExtn localObj = new EP_TankSyncCtrlExtn(sc);
        Test.startTest();
        localObj.setHeader();
        Test.stopTest();
        System.AssertEquals(true,localObj.headerObj!=null);
    }
    static testMethod void setPayload_test() {
        Account accountObj = EP_TestDataUtility.getShipToPositiveScenario();
        EP_tank__c tank = [SELECT Id FROM EP_tank__c where EP_Ship_To__c =: accountObj.Id LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(tank);
        EP_TankSyncCtrlExtn localObj = new EP_TankSyncCtrlExtn(sc);
        Test.startTest();
        localObj.setPayload();
        Test.stopTest();
        System.AssertEquals(true,localObj.encodedPayload!=null);
    }
    static testMethod void checkPageAccess_test() {
        Account accountObj = EP_TestDataUtility.getShipToPositiveScenario();
        EP_tank__c tank = [SELECT Id FROM EP_tank__c where EP_Ship_To__c =: accountObj.Id LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(tank);
        EP_TankSyncCtrlExtn localObj = new EP_TankSyncCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result != NULL);
	    System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
}