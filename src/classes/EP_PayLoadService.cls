/**
   @Author          CR Team
   @name            EP_PayLoadService  
   @CreateDate      01/18/2016
   @Description     This class contains methods which are generic to payload classes
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_PayLoadService {
        
        EP_PayLoadHelper payLoadHelper = null;
        
        public EP_PayLoadService() {
            payLoadHelper = new EP_PayLoadHelper();
        }
        
    /**
    *  This method creates Payload for a given sObject record for NAVISION 
    *  @name    createPayload
    *  @param   sObj, lFieldAPINames, tagName, additionTags
    *  @return  void
    *  @throws  Generic Exception
    */
    public String createPayload(sObject sObj, List<String> lFieldAPINames, String tagName, String additionTags){
        EP_GeneralUtility.Log('Public','EP_PayLoadService','createPayload');
        String navXML = EP_Common_Constant.BLANK;
        System.debug('&&&&&&&&'+sObj);
        System.debug('&&&&&&&&'+lFieldAPINames);
        System.debug('&&&&&&&&'+tagName+'&&&&&&'+additionTags);
        try{
            if(additionTags == null) {
              additionTags = EP_Common_Constant.BLANK;
          }
          String payloadWithIdentifier_str = payLoadHelper.createPayLoadWithIdentifier(sObj, lFieldAPINames);
          System.debug('&&&&&&&&'+payloadWithIdentifier_str);
          navXML =  createPayload(payloadWithIdentifier_str, tagName, additionTags);
      }
      catch(Exception handledException){
        throw handledException;
    }
    return navXML; 
}

    /**
    *  This method creates Payload for a given sObject record for NAVISION 
    *  @name    createPayload
    *  @param   payloadWithIdentifier_str, tagName, additionTags
    *  @return  void
    *  @throws  Generic Exception
    */
    public String createPayload(String payloadWithIdentifier_str, String tagName, String additionTags){
        EP_GeneralUtility.Log('Public','EP_PayLoadService','createPayload');
        String navXML = EP_Common_Constant.BLANK;
        try{
            if(additionTags == null) {
              additionTags = EP_Common_Constant.BLANK;
          }
            //CREATE BANK ACCOUNT PAYLOAD FOR CUSTOMER
            if(String.isBlank(payloadWithIdentifier_str)) {
                navXML = payLoadHelper.openingTag(tagName);           
                } else {
                    navXML =  payLoadHelper.startTag(tagName)
                    + payloadWithIdentifier_str
                    + payLoadHelper.endTag(tagName + additionTags);
                }
            }
            catch(Exception handledException){
                throw handledException;
            }
            return navXML; 
        }    

    /**
    *  This method returns value of field in string
        Special Case. if field is OrderItemNumber, its modified according to its allowed length
    *  @name    getFieldValue
    *  @param   obj, fieldName
    *  @return  String
    *  @throws  Generic Exception
    */
    public String getFieldValue(sObject obj, String fieldName) {
        EP_GeneralUtility.Log('Public','EP_PayLoadService','getFieldValue');
        String fieldVal = null;
        try{
            if(String.IsBlank(fieldName)) {
                return EP_Common_Constant.BLANK;
            }
            fieldVal = (String)obj.get(fieldName);
            if(fieldName.equalsIgnoreCase(EP_Common_Constant.ORDERITEMNUMBER)) {            
                if(String.isNotBlank( fieldVal ) && fieldVal.length() > EP_PayLoadConstants.ORDERITEMNUMBER_LENGTH ) {
                    fieldVal = fieldVal.subString( fieldVal.length()- EP_PayLoadConstants.ORDERITEMNUMBER_LENGTH );                
                }
            } 
            } catch(Exception handledException) {
                throw handledException;
            }
            return fieldVal;
        }
    }