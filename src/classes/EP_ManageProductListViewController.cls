/**
 * @author <Spiros Markantonatos>
 * @name EP_ManageProductListViewController 
 * @description <This class is used to implement a custom button agains the Account list view to allow users to mass update customer product lists> 
 * @version <1.0>
*/
public with sharing class EP_ManageProductListViewController {
  
  // Const
  private static final Integer ACCOUNT_ROWS = 10;
  private static final Integer PRODUCT_ROW_LIMIT = 10000;
  private static final Integer PRODUCT_LIST_ROW_LIMIT = 50;
  private static final String PRODUCT_LIST_NAME_SUFFIX = Label.EP_New_Product_List_Suffix;
  /**NOVASUITE CHANGES START**/
  private static final String QUERY_STR = 'SELECT Id, Name, ProductCode, Family, EP_Company_Lookup__c, EP_Company__c, EP_Unit_of_Measure__c';
  private static final String FRM_PRD_STR = ' FROM Product2';
  private static final String CMPNY_CLAUSE = 'EP_Company_Lookup__c IN :setSelectedAccountCompanyCodes AND ';
  private static final String PRD_CODE = 'ProductCode = \'';
  private static final String FAMILY_STR = 'Family = \'';
  private static final String AND_STR = '\' AND ';
  private static final String NAME_STR = '(Name LIKE \'%';
  private static final String OR_NAME = '%\' OR Name LIKE \'';
  private static final String AND_LIKE = '%\') AND ';
  private static final String PRODUCT_SOLD = 'EP_Product_Sold_As__c = \'';
  private static final String LIMIT_STR = ' LIMIT ';
  private static final String PRD_EXCEPTION = 'The system was not able to find a standard product list. Please contact your system administrator';
  private static final String PRICEBOOK_QUERY = 'SELECT ID, Name FROM Pricebook2 ';
  private static final String PRICEBOOK_WHERE = 'WHERE IsActive = TRUE AND (Name LIKE \'';
  private static final String PB_NAME_LIKE = '%\' OR Name LIKE \'%';
  private static final String LIKE_END = '%\') ';
  private static final String TRUE_STR = 'true';
  private static final String FALSE_STR = 'false';
  private static final String RECS_STR = 'recs';
  /**NOVASUITE CHANGES END**/
  
  private static final String CLASS_NAME = 'EP_ManageProductListViewController';
    private static final String SET_STANDARD_PRICE_METHOD = 'setProductStandardPrices';
    private static final String UPDATE_PRODUCT_LIST_METHOD = 'updateAccountProductLists';
   private static final string METHOD_RETRIEVE_ACC_PRODUCT_INFO = 'retrieveAccountProductListInformation';
    private static final string METHOD_SEARCH_PRODUCTS = 'searchProducts';
    private static final string METHOD_REMOVE_SEL_PROD = 'removeSelectedProductsToList';
    private static final string METHOD_ADD_SEL_PROD = 'addSelectedProductsToList';
  private String filterMethod;
  
  // Properties
  public List<Product2> lSelectedProducts {get;set;}
  public String strSelectedProductListName {get;set;}
  public String strSelectedProductListID {get;set;}
  
  public Boolean blnSearchByProduct {
    get {
      blnSearchByProduct = Boolean.valueOf(filterMethod);
      return blnSearchByProduct;
    }
    set;
  }
  
  public List<SelectOption> getavailableFilterOptions {
    get {
      List<SelectOption> lOptions = new List<SelectOption>();
      lOptions.add(new SelectOption(TRUE_STR, Label.EP_By_Product_Filter_Label));
      lOptions.add(new SelectOption(FALSE_STR, Label.EP_By_Product_List_Filter_Label));
      
      return lOptions;
    }
    set;
  }
  
  /*
    For getters and setters
  */
  public String getfilterMethod() {
        return filterMethod;
    }
    
  /*
    For Currency code of selected account
  */
  private Set<String> setSelectedAccountCurrencyCodes {
    get {
      if (setSelectedAccountCurrencyCodes == NULL)
      {
        setSelectedAccountCurrencyCodes = new Set<String>();
      }
      return setSelectedAccountCurrencyCodes;
    }
    set;
  }
  
  /*
    For company code of selected account
  */
  private Set<String> setSelectedAccountCompanyCodes {
    get {
      if (setSelectedAccountCompanyCodes == NULL)
      {
        setSelectedAccountCompanyCodes = new Set<String>();
      }
      return setSelectedAccountCompanyCodes;
    }
    set;
  }
  
  /*
    For standard product list record
  */
  public Pricebook2 standardPricebookRecord {
    get {
      if (standardPricebookRecord == NULL)
      {
        
        List<Pricebook2> lStandardPricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1];
        
        if (!lStandardPricebooks.isEmpty())
        {
          standardPricebookRecord = lStandardPricebooks[0];
        }
      }
      return standardPricebookRecord;
      
    }
    set;
  }
  
  /*
    For selected accounts
  */
  public List<Account> lSelectedAccounts {
    get {
      if (lSelectedAccounts == NULL)
      {
        lSelectedAccounts = new List<Account>();
      }
      return lSelectedAccounts;
    }
    set;
  }
  
  /*
    For filtering out the product
  */
  public Product2 filterProduct {
    get {
      if (filterProduct == NULL) filterProduct = new Product2();
      return filterProduct;
    }
    set;
  }
  
  /*
    For account product list entry 
  */
  public Map<Id, List<PricebookEntry>> mapAccountProductListEntry {
    get {
      if (mapAccountProductListEntry == NULL) mapAccountProductListEntry = new Map<Id, List<PricebookEntry>>();
      return mapAccountProductListEntry;
    }
    set;
  }
  
  /*
    For account product 
  */
  public Map<String, PricebookEntry> mapAccountProductPBE {
    get {
      if (mapAccountProductPBE == NULL) mapAccountProductPBE = new Map<String, PricebookEntry>();
      return mapAccountProductPBE;
    }
    set;
  }
  
  /*
    For account product records
  */
  public List<AccountProductClass> lAccountProductRecords {
    get {
      if (lAccountProductRecords == NULL) lAccountProductRecords = new List<AccountProductClass>();
      return lAccountProductRecords;
    }
    set;
  }
  
  // Constructor
  /*
    For account product records
  */ 
  public EP_ManageProductListViewController(ApexPages.StandardSetController stdSetController) {
    
    // Retrieve the account details
    List<Account> lAccounts = new List<Account>();
    
    if (ApexPages.currentPage().getParameters().get(RECS_STR) != NULL)
    {
      String strParam = ApexPages.currentPage().getParameters().get(RECS_STR).Trim();
      List<String> lAccountIds = strParam.split(EP_Common_Constant.COMMA, -2);
      for (String s : lAccountIds)
      {
        if (s != EP_Common_Constant.BLANK)
          lAccounts.add(new Account(Id = s));
      }
    } 
    
    if (!lAccounts.isEmpty())
    {
      
      stdSetController = new ApexPages.StandardSetController(Database.getQueryLocator(
            [SELECT Id, Name, AccountNumber, EP_PriceBook__c, EP_PriceBook__r.Name, CurrencyIsoCode, EP_Puma_Company_Code__c, EP_Puma_Company__c 
                FROM Account 
                  WHERE Id IN :lAccounts
                    AND (RecordTypeId = :EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.RT_BILL_TO_DEV_NAME) OR 
                    RecordTypeId = :EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.SELL_TO_DEV_NAME))
                    LIMIT :ACCOUNT_ROWS]));
      lSelectedAccounts = (List<Account>)stdSetController.getRecords();
      
      // Store the company codes of the selected accounts in a Set variable, which is then used to filter the selected products
      // Store the currency codes of the selected accounts, which is then used to create the default pricebook items
      for (Account a : lSelectedAccounts)
      {
        setSelectedAccountCompanyCodes.add(a.EP_Puma_Company__c);
        setSelectedAccountCurrencyCodes.add(a.CurrencyIsoCode);
      }
    }
    
    // Initiate the search mode
    filterMethod = TRUE_STR;
  }
  
  /*
    * @ Description: This method is a setter for the filter input property
    */
  public void setfilterMethod(String strFilterMethod) { 
    this.filterMethod = strFilterMethod; 
  }
  
  /*
    * @ Description: This method is used to refresh the page once the user has selected one of the available autocomplete options. The method clears the autocomplete dropdown
    */
  public PageReference refresh(){
        return null;
    }
    
    /*
    * @ Description: This method is used to retrieve the product list records based on the value that the user has entered in the auto-complete input box
    */
    @RemoteAction
    public static List<Pricebook2> getProductLists(String strProductListName) {
        List<Pricebook2> lProductLists = new List<Pricebook2>();
        if(strProductListName != EP_Common_Constant.BLANK)
        {
            String strSOQL = PRICEBOOK_QUERY;
            strSOQL += PRICEBOOK_WHERE + strProductListName + PB_NAME_LIKE + strProductListName + LIKE_END;
            strSOQL += LIMIT_STR + PRODUCT_LIST_ROW_LIMIT;
            
            lProductLists = database.query(strSOQL);
        }    
        return lProductLists;
    }
  
  /*
    * @ Description: This method is used select all filtered products
    */
  public PageReference selectAllFilteredProducts() {
    toggleFilteredProductsSelection(TRUE);
    return NULL;
  }
  
  /*
    * @ Description: This method is used de-select all filtered products
    */
  public PageReference deselectAllFilteredProducts() {
    toggleFilteredProductsSelection(FALSE);
    return NULL;
  }
  
  /*
    * @ Description: This method is used to toggle filtered products selection
    */
  private void toggleFilteredProductsSelection(Boolean blnSelect) {
    for (Product2 p : lSelectedProducts)
    {
      p.Eligible_for_Rebate__c = blnSelect;
    }
  }
  
	/*
	* @ Description: This method is used to add remove products from selection page
	*/
	private void addRemoveSelectedProductsFromList(Boolean blnAdd) {
    
		String strPBEKey;
		PricebookEntry pbe;
		Boolean blnPBEUpdated;
		Boolean blnReversed;
      
		for (Product2 p : lSelectedProducts)
		{
			if (p.Eligible_for_Rebate__c) // This flag is used to allow the user to select the products that he wants to add/remove
			{
			    for (Account a : lSelectedAccounts)
			    {
					if (a.EP_Puma_Company__c == p.EP_Company_Lookup__c)
					{
				        // Check the existing product against the list and add the product to the summary wrapper
				        strPBEKey = generateAccountProductKey(a.Id, p.Id, a.CurrencyIsoCode);
			        
				        // Initialise the variables that defines if the product is actually updated for this account
				        blnPBEUpdated = TRUE;
				        blnReversed = FALSE;
			        
				        pbe = new PricebookEntry(IsActive = blnAdd, Product2Id = p.Id, CurrencyIsoCode = a.CurrencyIsoCode, 
				                      Pricebook2Id = a.EP_PriceBook__c, UnitPrice = 1, UseStandardPrice = FALSE,
				                        EP_Is_Sell_To_Assigned__c = TRUE);
			        
				        if (mapAccountProductPBE.containsKey(strPBEKey))
				        {
							pbe = mapAccountProductPBE.get(strPBEKey);
							// If the PBE is already active and user added the product again, then the user did not add anything new to the account
							// If the PBE is already inactive and user removed the product again, then the user did not remove anything new to the account
							blnPBEUpdated = ((blnAdd && !(pbe.IsActive)) || (!blnAdd && pbe.IsActive)); 
							  
							// If the user removes a PBE that existed before then he reversed a previous change
							if (!blnAdd && pbe.Id == NULL && pbe.IsActive)
							{
								blnReversed = TRUE;
							}
							 
							// If the user adds a new PBE that existed before then he reversed a previous change
							if (blnAdd && pbe.Id == NULL && !pbe.IsActive)
								blnReversed = TRUE;
								pbe.IsActive = blnAdd;
								pbe.EP_Is_Sell_To_Assigned__c = TRUE; // Set this flag to TRUE to bypass the CR validation rule
							} else {
								blnPBEUpdated = FALSE;
							if (blnAdd)
							{
								// In this scenario there was no PBE. Therefore insert is always required
								// If the user is removing a product that does not existing anyway, then no additional action is required
								blnPBEUpdated = TRUE;
							}
							
						}
			        	
				        // Add the PBE from the map
				        if (blnPBEUpdated && !blnReversed)
							mapAccountProductPBE.put(strPBEKey, pbe);
			        
				        // Remove PBE from the map
				        if (blnPBEUpdated && blnReversed)
							mapAccountProductPBE.remove(strPBEKey);
			        
						for (AccountProductClass apr : lAccountProductRecords)
						{
							
							if (apr.accountItem.Id == a.Id)
							{
								if (blnPBEUpdated) 
							    {
									if (blnAdd && !blnReversed)
									{
										apr.intNumberOfProductsAdded = apr.intNumberOfProductsAdded + 1;
							      	}
									if (!blnAdd && !blnReversed) 
									{
										apr.intNumberOfProductsRemoved = apr.intNumberOfProductsRemoved + 1;
									}
									if (blnAdd && blnReversed) 
									{
										apr.intNumberOfProductsRemoved = apr.intNumberOfProductsRemoved - 1;
							      	}
							      
									if (!blnAdd && blnReversed)
									{
										apr.intNumberOfProductsAdded = apr.intNumberOfProductsAdded - 1;
									}
							      
									// Add the updated product in the relevant wrapper list. This list is used to control which PBE records get updated during save
									apr.lUpdateProductIDs.add(p.Id);
							    }
							    apr.intUpdatedNumberOfProducts = apr.intNumberOfExistingProducts 
													                    + apr.intNumberOfProductsAdded 
													                      - apr.intNumberOfProductsRemoved;
							    
							    break;
							}
						} // End product list for
					} // End pricebook check
				}
			}
		} // End selected product for
	}
  
	/*
    * @ Description: This method is used to add the selected products to the product list
    */
    public PageReference addSelectedProductsToList() {
      try{
        addRemoveSelectedProductsFromList(TRUE);
      }
      catch(Exception ex){
            EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, METHOD_ADD_SEL_PROD , CLASS_NAME, ApexPages.Severity.ERROR);
      }
      return NULL;
    }
    
    /*
    * @ Description: This method is used to remove the selected products to the product list
    */
    public PageReference removeSelectedProductsToList() {
		try{
			addRemoveSelectedProductsFromList(FALSE);
		}
		catch(Exception ex){
			EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, METHOD_REMOVE_SEL_PROD , CLASS_NAME, ApexPages.Severity.ERROR);
		}
  		return NULL;
    }
    
    /*
    * @ Description: This method is used to retrieve the product records that match the selected produce list
    */
    private void searchProductsByProductList() {
      Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
    if (strSelectedProductListID != NULL)
    {
      for (PricebookEntry pbe : [SELECT Product2Id, Product2.Name, Product2.ProductCode, Product2.Family, 
                      Product2.EP_Company_Lookup__c, Product2.EP_Unit_of_Measure__c
                          FROM PricebookEntry
                            WHERE Pricebook2Id = :strSelectedProductListID
                            limit :nRows])
      {
        lSelectedProducts.add(new Product2(Id = pbe.Product2Id, 
                          Name = pbe.Product2.Name,
                            ProductCode = pbe.Product2.ProductCode,
                              Family = pbe.Product2.Family,
                                EP_Company_Lookup__c = pbe.Product2.EP_Company_Lookup__c,
                                    EP_Unit_of_Measure__c = pbe.Product2.EP_Unit_of_Measure__c));
      }
    }
    }
    
	/*
	* @ Description: This method is used to retrieve the product records that match the selected product filter criteria
	*/
	private void searchProductsByAtributes() {
      
	    String strSOQL = QUERY_STR;
	    strSOQL += FRM_PRD_STR;
	    
	    String strWhereClause = EP_COMMON_CONSTANT.BLANK;
	    
	    if (!setSelectedAccountCompanyCodes.isEmpty())
	    	strWhereClause += CMPNY_CLAUSE;
	    
	    if (filterProduct.ProductCode != NULL)
	    	strWhereClause += PRD_CODE + filterProduct.ProductCode + AND_STR;
	    
	    if (filterProduct.Family != NULL)
	    	strWhereClause += FAMILY_STR + filterProduct.Family + AND_STR;
	    
	    if (filterProduct.Name != NULL)
	    	strWhereClause += NAME_STR + filterProduct.Name + OR_NAME + filterProduct.Name + AND_LIKE;
	    
	    if (filterProduct.EP_Product_Sold_As__c != NULL)
	    	strWhereClause += PRODUCT_SOLD + filterProduct.EP_Product_Sold_As__c + AND_STR;
	    
	    if (strWhereClause != EP_COMMON_CONSTANT.BLANK)
	    {
	      // Remove the extra comma (if required)
	      strWhereClause = strWhereClause.Left(strWhereClause.Length() - 5);
	      strSOQL += EP_COMMON_CONSTANT.WHERE_STRING + strWhereClause;
	    }
	    
	    strSOQL += LIMIT_STR + PRODUCT_ROW_LIMIT;
	    
	    lSelectedProducts = Database.query(strSOQL);
  }
    
	/*
	* @ Description: This method is used to retrieve the product records that match the selected user criteria
	*/
	public PageReference searchProducts() {
		// Reset the product list
		lSelectedProducts = new List<Product2>();
		try{
			if (blnSearchByProduct)
			searchProductsByAtributes();
			        
			if (!blnSearchByProduct)
				searchProductsByProductList();
			        
			// Use the "Eligible_for_Rebate__c" flag to allow the user to select the products that he wants to add
			toggleFilteredProductsSelection(TRUE);
		}
		catch(Exception ex){
			EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, METHOD_SEARCH_PRODUCTS , CLASS_NAME, ApexPages.Severity.ERROR);
		}
	    
		return NULL;
	}
  
	/*
	* @ Description: This method is used to generate a unique Account and Product key
	*/
	private String generateAccountProductKey(String strAccountId, String strProductId, String strCurrencyCode) {
		return strAccountId + EP_COMMON_CONSTANT.UNDERSCORE_STRING + strProductId + EP_COMMON_CONSTANT.UNDERSCORE_STRING + strCurrencyCode;
	}
  
	/*
	* @ Description: This method is used to retrieve the product list information for the selected account records
	*/
	public PageReference retrieveAccountProductListInformation() {
	    try{  
	        Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();  
	        if (!lSelectedAccounts.isEmpty())
	        {
	          Map<Id, Id> mapAccountProductListIDs = new Map<Id, Id>();
	          String strAccountID;
	          Integer intNumberOfProducts;
	          String strPricebookURL;
	          
	          for (Account a : lSelectedAccounts)
	          {
	            mapAccountProductListIDs.put(a.EP_PriceBook__c, a.Id);
	          }
	          
	          for (PricebookEntry pbe : [SELECT Id, Product2Id, Product2.Name, IsActive, CurrencyIsoCode, 
	          							Pricebook2Id, Pricebook2.EP_Company__r.EP_Company_Code__c 
											FROM PricebookEntry 
											WHERE Pricebook2Id IN :mapAccountProductListIDs.keySet()
											limit :nRows])
	          {
	            strAccountID = mapAccountProductListIDs.get(pbe.Pricebook2Id);
	            
	            // Update the Account + Product and PBE map
	            mapAccountProductPBE.put(generateAccountProductKey(strAccountID, pbe.Product2Id, pbe.CurrencyIsoCode), pbe);
	            
	            // Create map entry, if there isn't one
	            if (!mapAccountProductListEntry.containsKey(strAccountID))
	            {
	              mapAccountProductListEntry.put(strAccountID, new List<PricebookEntry>());
	            }
	            mapAccountProductListEntry.get(strAccountID).add(pbe);
	          }
	          
	          Pricebook2 accountProductList;
	          String strAccountProductListName;
	          
	          // Build the account product list variable used to construct the change table
	          for (Account a : lSelectedAccounts)
	          {
	            intNumberOfProducts = 0;
	            strPricebookURL = NULL;
	            
	            if (mapAccountProductListEntry.containsKey(a.Id))
	            {
              		for (PricebookEntry pbe : mapAccountProductListEntry.get(a.Id))
              		{
              			if (pbe.IsActive)
              			{
              				intNumberOfProducts++;
              			}
              		}
	            }
	            
	            if (a.EP_PriceBook__c != NULL)
	            {
	              strPricebookURL = EP_COMMON_CONSTANT.SLASH + a.EP_PriceBook__c;
	            }
	            
	            strAccountProductListName = EP_COMMON_CONSTANT.BLANK;
	            
	            if (a.Name != NULL)
				{
					strAccountProductListName = a.Name.left(37) + EP_COMMON_CONSTANT.SPACE;
				}
	            strAccountProductListName += PRODUCT_LIST_NAME_SUFFIX;
	            accountProductList = new Pricebook2(Name = strAccountProductListName, EP_Company__c = a.EP_Puma_Company__c);
	            
	            // If the account has a product list assigned already, then add the existing product list details to the wrapper class
	            if (a.EP_PriceBook__c != NULL)
	            {
	              accountProductList = new Pricebook2(Id = a.EP_PriceBook__c, Name = a.EP_PriceBook__r.Name);
	            }
	            
	            lAccountProductRecords.add(new AccountProductClass(a, accountProductList, new List<Id>(), intNumberOfProducts, 0, 0, 
	                                intNumberOfProducts, strPricebookURL, a.EP_PriceBook__r.Name));
	        	}
        	} 
	    } catch(Exception ex) {
		    EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, METHOD_RETRIEVE_ACC_PRODUCT_INFO , CLASS_NAME, ApexPages.Severity.ERROR);
		}
		return NULL;
	}
  
	/*
    * @ Description: This method is used to define the key used to populate the map that drives which PBE records are missing
    */
	private String generateStandardPricebookEntryKey(String strProductID, String strProductCurrencyCode) {
		return strProductID + EP_COMMON_CONSTANT.UNDERSCORE_STRING + strProductCurrencyCode;
	}
  
   /*
    * @ Description: This method is used to set the standard product price (if required). This is mandated by Salesforce before a product is added to a pricebook
    */
  private Boolean setProductStandardPrices() {
    
    Boolean blnResult = FALSE;
    Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
    if (standardPricebookRecord != NULL)
    {
      Set<ID> setUpdatedProductIDs = new Set<ID>();
      
      for (AccountProductClass apc : lAccountProductRecords)
      {
        for (Id productRecordId : apc.lUpdateProductIDs)
        {
          setUpdatedProductIDs.add(productRecordId);
        }
      }
      
      //List<PricebookEntry> lExistingStandardPricebookEntries = [SELECT ID, Product2Id, CurrencyIsoCode FROM PricebookEntry WHERE Pricebook2Id = :standardPricebookRecord.Id AND Product2Id IN :setUpdatedProductIDs limit :nRows];
      
      // If the products do not match the records already added to the standard pricebook, then some records are missing
      // The method will add the extra products in the standard pricebook
      Integer intNumberOfExpectedPBERecords = setUpdatedProductIDs.size() * setSelectedAccountCurrencyCodes.size();
      
      List<PricebookEntry> lPricebookEntryForInsert = new List<PricebookEntry>();
      Map<String, String> mapStandardPricebookEntryProduct = new Map<String, String>();
      String strKey;
      
      for (PricebookEntry pbe : [SELECT ID, Product2Id, CurrencyIsoCode FROM PricebookEntry WHERE Pricebook2Id = :standardPricebookRecord.Id AND Product2Id IN :setUpdatedProductIDs limit :nRows])
      { 
        strKey = generateStandardPricebookEntryKey(pbe.Product2Id, pbe.CurrencyIsoCode);
        mapStandardPricebookEntryProduct.put(strKey, NULL);
      }
      for (Id productRecordId : setUpdatedProductIDs)
      {
        for (String strCurrencyCodes : setSelectedAccountCurrencyCodes)
        {
          strKey = generateStandardPricebookEntryKey(productRecordId, strCurrencyCodes);
          if (!mapStandardPricebookEntryProduct.containsKey(strKey))
          {
            lPricebookEntryForInsert.add(new PricebookEntry(Product2Id = productRecordId, 
                                    Pricebook2Id = standardPricebookRecord.Id, 
                                      CurrencyIsoCode = strCurrencyCodes, 
                                        IsActive = TRUE,
                                          UnitPrice = 1,
                                            EP_Is_Sell_To_Assigned__c = TRUE,
                                              UseStandardPrice = FALSE));
          }
        }
      }
      
      if (!lPricebookEntryForInsert.isEmpty())
        Database.insert(lPricebookEntryForInsert);
      
      blnResult = TRUE;
      
    } else {
      EP_LoggingService.logHandledError(EP_Common_Constant.NO_DATA_FOUND, PRD_EXCEPTION, EP_Common_Constant.EPUMA, 
                    SET_STANDARD_PRICE_METHOD,  EP_Common_Constant.EPUMA, ApexPages.Severity.ERROR);
      
        ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 
                    Label.EP_No_Standard_Product_List_Error + EP_COMMON_CONSTANT.SPACE 
                    + Label.EP_Contact_Admin_Generic_Error);
		ApexPages.addMessage(errorMessage);
		blnResult = FALSE;
    }
      
    return blnResult;
  }
  
	/*
	* @ Description: This method is used to save the changes that the user did to the product lists of the selected accounts
	*/
	public PageReference updateAccountProductLists() {
    
	    // Variables used to display confirmation message to the user
	    Integer intTotalProductsAdded = 0;
	    Integer intTotalProductsRemoved = 0;
	    Integer intVariance;
    
	    // 1. Set the standard price for the selected products
	    Boolean blnResult = setProductStandardPrices();
    
	    if (blnResult)
	    {
			Savepoint sp = Database.setSavepoint();
	      
			try {
		        // 2. Create new product lists (if required)
		        List<Pricebook2> lProductListsForInsert = new List<Pricebook2>();
		        List<PricebookEntry> lPricebookEntryForUpsert = new List<PricebookEntry>();
		        List<Account> lAccountsForUpdate = new List<Account>();
		        Map<String, Integer> mapAccountNewProductListRecord = new Map<String, Integer>();
		        Integer intIndex = 0;
		        String strPBEKey;
	        
				for (AccountProductClass apc : lAccountProductRecords)
				{
					if (apc.productListItem.Id == NULL)
					{
						lProductListsForInsert.add(apc.productListItem);
						mapAccountNewProductListRecord.put(apc.accountItem.Id, intIndex);
						intIndex++;
					}
				}
	        
		        if (!lProductListsForInsert.isEmpty())
		        {
		          Database.insert(lProductListsForInsert);
		        }
	        
		        // 3. Upsert the pricebook entry records
		        Map<String, String> mapPricebookEntriesForUpsert = new Map<String, String>();
	        
		        for (AccountProductClass apc : lAccountProductRecords)
		        {
					for (Id productID : apc.lUpdateProductIDs)
					{
					    strPBEKey = generateAccountProductKey(apc.accountItem.Id, productID, apc.accountItem.CurrencyIsoCode);
					    
					    // Check if the map contains the Account/Product combinations
					    // The user might choose to reverse a change
					    // If the change is reverse the product might stay in the update product list, however it might not be the map
					    if (mapAccountProductPBE.containsKey(strPBEKey))
					    {
							// Update the PBE with the pricebook ID if required
							if (mapAccountProductPBE.get(strPBEKey).Pricebook2Id == NULL)
							{
								mapAccountProductPBE.get(strPBEKey).Pricebook2Id = 
								lProductListsForInsert[mapAccountNewProductListRecord.get(apc.accountItem.Id)].Id;
							}
							  
							// Check to ensure that the product is added in the list for a second time.
							// This is required in case the user adds a product, presses save, then removes the product, presses save
							// and tries to add the product back on
							if (!mapPricebookEntriesForUpsert.containsKey(strPBEKey))
							{
								lPricebookEntryForUpsert.add(mapAccountProductPBE.get(strPBEKey));
								mapPricebookEntriesForUpsert.put(strPBEKey, NULL);
							}
						}
					}
	          
		          	// Calculate how many products were added/removed in total
		          	intVariance = apc.intUpdatedNumberOfProducts - apc.intNumberOfExistingProducts;
		          	if (intVariance > 0)
		          	{
		            	intTotalProductsAdded = intTotalProductsAdded + intVariance;
		          	}
		          	
					if (intVariance < 0)
					{
						intTotalProductsRemoved = intTotalProductsRemoved + intVariance;
					}
				}
		        
		        if (!lPricebookEntryForUpsert.isEmpty())
		        {
		          Database.upsert(lPricebookEntryForUpsert);
		        }
        		
        		// Reset the change counter after PBE upsert
        		for (AccountProductClass apr : lAccountProductRecords)
				{
					apr.intNumberOfProductsAdded = 0;
					apr.intNumberOfProductsRemoved = 0;
				}
				
		        // 4. Update the account with product list changes
		        for (AccountProductClass apc : lAccountProductRecords)
		        {
					if (apc.accountItem.EP_PriceBook__c == NULL)
					{
						lAccountsForUpdate.add(new Account(Id = apc.accountItem.Id, EP_PriceBook__c = apc.productListItem.Id));
					}
		        }
        
		        if (!lAccountsForUpdate.isEmpty())
		        {
		          Database.update(lAccountsForUpdate);
		        }
        
		        // 5. Display confirmation message (if there is no error)
		        String strConfirmation = Label.EP_Product_List_Save_Operation_Confirmation;
		        strConfirmation = strConfirmation.replace('[PARAM1]', String.valueOf(intTotalProductsAdded));
		        strConfirmation = strConfirmation.replace('[PARAM2]', String.valueOf(Math.ABS(intTotalProductsRemoved)));
	        	
		        ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.Severity.INFO, strConfirmation);
	            ApexPages.addMessage(infoMessage);
	      
			} catch(Exception ex){
				Database.rollback(sp);
				EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, CLASS_NAME, 
				                                  UPDATE_PRODUCT_LIST_METHOD, ApexPages.Severity.ERROR);
				ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, Label.EP_Product_List_Save_Operation_Error);
				ApexPages.addMessage(infoMessage);
			}
    	}
    
		return NULL;
	}
  
  // Inner Classes
  /*
    Method for Account Produt List
  */
  public with sharing class AccountProductClass {
    public Account accountItem {get;set;}
    public Pricebook2 productListItem {get;set;}
    public Integer intNumberOfExistingProducts {get;set;}  
    public Integer intNumberOfProductsAdded {get;set;}
    public Integer intNumberOfProductsRemoved {get;set;}
    public Integer intUpdatedNumberOfProducts {get;set;}
    public String strProductListURL {get;set;}
    public String strProductListName {get;set;}
    public List<Id> lUpdateProductIDs {get;set;}
    
    /*
    Method for Account Produt List
	*/
    public AccountProductClass(Account accountItem, Pricebook2 productListItem, List<Id> lUpdateProductIDs, Integer intNumberOfExistingProducts, Integer intNumberOfProductsAdded,
                    Integer intNumberOfProductsRemoved, Integer intUpdatedNumberOfProducts, String strProductListURL,
                    String strProductListName)
    {
      this.accountItem = accountItem;
      this.productListItem = productListItem;
      this.lUpdateProductIDs = lUpdateProductIDs;
      this.intNumberOfExistingProducts = intNumberOfExistingProducts;
      this.intNumberOfProductsAdded = intNumberOfProductsAdded;
      this.intNumberOfProductsRemoved = intNumberOfProductsRemoved;
      this.intUpdatedNumberOfProducts = intUpdatedNumberOfProducts;
      this.strProductListURL = strProductListURL;
      this.strProductListName = strProductListName;
    }
  }
  
}