@isTest
public class EP_OrderStateMachine_test{
/**
   @Author : AC Ramalingam 
   @name : <EP_OrderStateMachine_test>
   @CreateDate : 2/10/2017
   @Description : Test class for EP_OrderStateMachine
   @Version <1.0>
    /* 
       @Description : Method to create setup data
       @name : < setupData >
    
    @testSetup static void setupData() {
    
        //Get Custom setting data from static resource
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        
        //Created Custom setting EP_Debug__c record
        EP_Debug__c debugCSObj = new EP_Debug__c(Name = 'Enable All' , access_modifier__c = 'public::private' , Enable__c = true);
        insert debugCSObj; 
                
        //Getting all record type id's
        Id supplyLocRecordtypeid = Schema.SObjectType.EP_Stock_Holding_Location__c.getRecordTypeInfosByName().get('Delivery Supply Location').getRecordTypeId();
        Id shpToRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-VMI Ship To').getRecordTypeId();
        Id orderNonVMIRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        //Create Company__c record
        Company__c comp = EP_TestDataUtility.createCompany('AUN');
        insert comp;
        
        //Create Product
        Product2  taxProd = EP_TestDataUtility.createProduct2(false);
        taxProd.EP_Company_Lookup__c = comp.id;
        insert taxProd;
        
        //Create PriceBook2
        Pricebook2 customPB1 = EP_TestDataUtility.createPricebook2(false);
        customPB1.EP_Company__c =comp.id;
        insert customPB1 ;
        
        //Create EP_Payment_Term__c record
        EP_Payment_Term__c payTrm = EP_TestDataUtility.createPaymentTerm();
        insert payTrm;
        
        //Create EP_Payment_Method__c record
        EP_Payment_Method__c payMthd = EP_TestDataUtility.createPaymentMethod();
        insert payMthd ;
        
        //Create Freight record
        EP_Freight_Matrix__c  freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        insert freightMatrix;
                        
        //Create PriceBookEntry        
        PricebookEntry taxPrice = EP_TestDataUtility.createPricebookEntry(taxProd.Id,customPB1.Id);
        taxPrice.EP_Is_Sell_To_Assigned__c = true;
        insert taxPrice;
              
        //Create sell to Account
        Account  sellToAccountAsBillTo = EP_TestDataUtility.createSellToAccount(Null, freightMatrix.id);
        sellToAccountAsBillTo.EP_Package_Payment_Term__c = payTrm.id;
        sellToAccountAsBillTo.EP_Puma_Company__c = comp.id;
        /* CAM 2.7 start 
        sellToAccountAsBillTo.EP_AvailableFunds__c = 1200.0;
        /* CAM 2.7 end 
        sellToAccountAsBillTo.EP_Integration_Status__c = 'SYNC ';
        insert sellToAccountAsBillTo;
        
        //Create Stock Holding Location
        Account storageLocAccPrimary1 = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAccPrimary1.EP_Nav_Stock_Location_Id__c = '155454';
        insert storageLocAccPrimary1;

        
        //Update BillTo with Setup Status
        sellToAccountAsBillTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccountAsBillTo; 
        
        //Update BillTo with Active Status
        sellToAccountAsBillTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccountAsBillTo;    
        
        //Create sell to Account
        Account  sellToAccount = EP_TestDataUtility.createSellToAccount(sellToAccountAsBillTo.id, freightMatrix.id);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = sellToAccountAsBillTo.id;
        sellToAccount.AccountNumber = '202';
        sellToAccount.EP_PriceBook__c = customPB1.Id;
        sellToAccount.EP_Puma_Company__c = comp.id;
        //sellToAccount.EP_Available_Funds__c = 100.00;
        sellToAccount.EP_Integration_Status__c = 'SYNC ';
        insert sellToAccount;
        
        //Create NON-VMI ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        nonVmiShipToAccount.AccountNumber = '41001';
        nonVmiShipToAccount.EP_PriceBook__c = customPB1.Id;
        nonVmiShipToAccount.EP_Ship_To_Type__c = 'Non-Consignment';
        nonVmiShipToAccount.EP_Package_Payment_Term__c = payTrm.id;
        nonVmiShipToAccount.EP_Payment_Method__c = payMthd.id;
        nonVmiShipToAccount.EP_Bill_To_Account__c = sellToAccountAsBillTo.id;
        insert nonVmiShipToAccount;

        //Create Stock Holding Location
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAccPrimary.EP_Nav_Stock_Location_Id__c = '155454';
        insert storageLocAccPrimary;
        
        //Update Sell-To Account with Setup status
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount; 
        
        //Update Sell-To Account with Active Status
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
                
        //Craete Vendor Account
        Account VendrAcc = EP_TestDataUtility.createTestVendorWithoutCompany('Transporter','strNAVVendorID',comp.id);
        insert VendrAcc;
        
        //Create Stock Holding Location
        EP_Stock_holding_location__c primaryStockHolding = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,storageLocAccPrimary.Id,supplyLocRecordtypeid); //Add one SHL
        primaryStockHolding.EP_Trip_Duration__c = 0;
        primaryStockHolding.EP_Transporter__c = VendrAcc.id;
        Database.insert(primaryStockHolding,true);
        
        //Create Tank Record
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,taxProd.Id);      
        insert tankObj;
        
        //Update NON-VMI Account with Setup Status
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update nonVmiShipToAccount;
        
        //Update Tank with Operational Status
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        update tankObj;
        
        //Update NON-VMI Account with Active Status
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVmiShipToAccount;
        
        //Create VMI Order
        Order  ordObj = EP_TestDataUtility.createOrder(sellToAccount.Id, orderNonVMIRecordTypeId , customPB1.Id);
        ordObj.EP_ShipTo__c = nonVmiShipToAccount.Id;
        ordObj.EP_Bill_To__c = sellToAccountAsBillTo.Id;
        ordObj.EP_Order_Epoch__c = 'Current';
        ordObj.EP_Order_Category__c = 'Sales Order';
        ordObj.EP_Order_Product_Category__c = 'Bulk';
        ordObj.type = 'Non-Consignment';
        ordObj.EP_Payment_Term__c = 'Cash on Delivery';
        ordObj.EP_Credit_Status__c = 'Overdue Invoice';
        //ordObj.EP_Credit_Status__c = 'Credit Fail';
        insert ordObj;
        
        OrderItem ordItemObj1 = EP_TestDataUtility.createOrderItem(ordObj.id,customPB1.id);
        ordItemObj1.EP_Prior_Quantity__c = Null;
        ordItemObj1.Quantity = 3;
        ordItemObj1.UnitPrice= 100;
        ordItemObj1.EP_Is_Standard__c = false;
        insert ordItemObj1;
        
        OrderItem ordItemObj2= EP_TestDataUtility.createOrderItem(ordObj.id,customPB1.id);
        ordItemObj2.EP_Prior_Quantity__c = Null;
        ordItemObj2.UnitPrice= 1000;
        ordItemObj2.Quantity = 1;
        ordItemObj2.EP_Is_Standard__c = false;
        insert ordItemObj2;        
    }
   
    static EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
     
    /* 
       @Description : Unit testing for getting instance of Order State -  getorderstate(EP_OrderEvent)
       @name : < instanceCreationbyPassingEvent>
    
    static testmethod void instanceCreationbyPassingEvent(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        // Variable for State Machines
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderStateMachine stateMachine = new EP_OrderStateMachine();
        stateMachine.setOrderDomainObject(orderdomain);
        EP_OrderEvent orderEvent = new EP_OrderEvent('testing');
        EP_OrderState orderState = stateMachine.getOrderState(orderEvent);
        system.assert(orderState != null, true);
        Test.StopTest();
    }
    
    /* 
       @Description : Unit teting for getting instance with empty constructor
       @name : < instanceCreationbyEmptyConstructor>
    
    static testmethod void instanceCreationbyEmptyConstructor(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderStateMachine stateMachine = new EP_OrderStateMachine();
        stateMachine.setOrderDomainObject(orderdomain);
        EP_OrderEvent orderEvent = new EP_OrderEvent('testing');
        EP_OrderState orderState = stateMachine.getOrderState(orderEvent);
        system.assert(stateMachine.getOrderState() != null, true);
        Test.StopTest();
    }
    
    /* 
       @Description : unit testing for order instance with Order domain object and order event
       @name : < testOrderstateInstance>
    
    static testmethod void testOrderstateInstance(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderEvent orderEvent = new EP_OrderEvent('testing');
        EP_OrderState orderState = EP_OrderStateMachine.getOrderStateInstance(orderdomain,orderevent);
        system.assert(orderState!=null,true);
        Test.StopTest();
    }
    
    //SG
    /* 
       @Description : Negative Test - Unit testing for order instance with Order domain object and order event is Null
       @name : < testOrderstateInstance_Neg>
    
    static testmethod void testOrderstateInstance_Neg(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderEvent orderEvent = new EP_OrderEvent('testing');
        try{
            EP_OrderState orderState = EP_OrderStateMachine.getOrderStateInstance(orderdomain,Null);
        }
        Catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Order State Machine not available for') ? true : false;
            System.debug('@@ e.getMessage() '+ e.getMessage());
            System.AssertEquals(expectedExceptionThrown, true);
            System.debug('@@ Done');
        }
        Test.StopTest();
    }
    
    /* 
       @Description : testing set method of Order domain object
       @name : < testSetOrderDomainObject>
    
    static testmethod void testSetOrderDomainObject(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderStateMachine sm = new EP_OrderStateMachine();
        sm.setOrderDomainObject(orderdomain);
        system.assert(orderdomain == EP_OrderStateMachine.order, true);
        Test.StopTest();
    }
    //SG
    /* 
       @Description : Unit test to get current Order state
       @name : < testGetOrderState>
    
    static testmethod void testGetOrderState(){
        Test.StartTest();
        Order ord = [SELECT Id,status FROM Order LIMIT 1];
        EP_OrderDomainObject orderdomain = new EP_OrderDomainObject(ord.id);
        EP_OrderStateMachine stateMachine = new EP_OrderStateMachine();
        stateMachine.setOrderDomainObject(orderdomain);
        EP_OrderEvent orderEvent = new EP_OrderEvent('testing');
        EP_OrderState orderState = EP_OrderStateMachine.getOrderState(orderdomain,orderEvent);
        system.assert(orderState != null, true); 
        Test.StopTest();
    }*/
}