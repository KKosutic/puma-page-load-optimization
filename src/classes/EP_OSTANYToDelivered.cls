/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToDelivered >
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to Delivered>
*  @Version <1.0>
*/

public class EP_OSTANYToDelivered extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToDelivered';
    public EP_OSTANYToDelivered(){
        finalState = EP_OrderConstant.OrderState_Delivered;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToDelivered','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToDelivered','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToDelivered','isGuardCondition');
        //Defect #57297 START
        //Getting null pointer exception so reversed the condition
        //if (((this.order.getOrder()).EP_WinDMS_Status__c).equalsIgnoreCase(EP_Common_Constant.delivered)) {
        if (EP_Common_Constant.delivered.equalsIgnoreCase(((this.order.getOrder()).EP_WinDMS_Status__c))) {
            orderEvent.setEventMessage(this.order.getOrder().EP_WinDMS_Status__c);
            (this.order.getOrder()).EP_Actual_Delivery_Date_Time__c = (this.order.getOrder()).EP_Estimated_Delivery_Date_Time__c;
            setETAonOrder();
            return true;
        }
        //Defect #57297 START
        if(this.order.isPackaged()){
        	return true;
        }
        //Defect #57297 END                   
        return false;
    }

    private void setETAonOrder(){
        EP_GeneralUtility.Log('private','EP_OSTANYToDelivered','setETAonOrder');
        Account selltoAcc = this.order.getSellToAccountDomain().getAccount();
        // Calculating ETA Range, calls General utility method.
        if(EP_GeneralUtility.isDateTimeValid(string.valueOf(this.order.getOrder().EP_Actual_Delivery_Date_Time__c))){
            this.order.getOrder().EP_Estimated_Time_Range__c = EP_GeneralUtility.getETARangeOnOrder(string.valueOf(this.order.getOrder().EP_Actual_Delivery_Date_Time__c), 
             0,0,this.order.getOrder().Status__c);
        }
    }
    
}