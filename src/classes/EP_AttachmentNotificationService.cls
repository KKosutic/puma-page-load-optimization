/**
*  @Author <Accenture>
*  @Name <EP_AttachmentNotificationService>
*  @CreateDate <18/01/2018>
*  @Description <This is the apex class containing utility methods used to send Attachment Notification>
*  @Version <1.0>
*/
public with sharing class EP_AttachmentNotificationService {
	
	/**
    *  @author <Accenture>
    *  @name <AttachmentWrapper>
    *  @createDate <18/01/2018>
    *  @description <This is class to send Attachment notification>
    *  @version <1.0>
    */
	public with sharing class AttachmentWrapper{
        public Attachment attachmentObject;
        public Id sellToId;
        public string notificationClass;
        public attachmentWrapper(Attachment attachmentObj, Id sellToAccountId, string notificationClassName){
            this.attachmentObject = attachmentObj;
            this.sellToId = sellToAccountId;
            this.notificationClass = notificationClassName;
        }
    }
    
    /**
    *  @author <Accenture>
    *  @name <sendAttachmentNotification>
    *  @createDate <18/01/2018>
    *  @description <This is class to send Attachment notification>
    *  @version <1.0>
    */
    public static void sendAttachmentNotification(List<attachmentWrapper> attachmentWrapList) {
		EP_GeneralUtility.Log('public','EP_AttachmentNotificationService','sendOrderAttachmentNotification');
        String className = null;
        try {
	        for (AttachmentWrapper attWrap : attachmentWrapList) {
	            if (attWrap.attachmentObject.Id != null) {
	            	className = EP_Common_Constant.CLASS_NAME_PREFIX+attWrap.notificationClass+EP_Common_Constant.EVENT_CLASS_NAME_SUFFIX;
	                sendNotification(attWrap.attachmentObject,attWrap.sellToId, className);
	            }
	        }
        } catch(exception exp) {
        	EP_loggingService.loghandledException(exp,EP_Common_Constant.EPUMA, 'EP_AttachmentNotificationService' , EP_AttachmentNotificationService.Class.getName(), apexPages.severity.ERROR);
        }
    }

	/**
    *  @author <Accenture>
    *  @name <sendNotification>
    *  @createDate <18/01/2018>
    *  @description <This is class to subscribe notification event>
    *  @version <1.0>
    */
    @TestVisible
    private static EP_Event_Subscription sendNotification(Attachment attachObject,Id SellToId, string className) {
		EP_GeneralUtility.Log('private','EP_AttachmentNotificationService','sendNotification');
        System.debug('****className'+className);
        Type classType = Type.forName(className);
        EP_Event_Subscription eventSub = (EP_Event_Subscription) classType.newInstance();
        eventSub.subscriptionEventObj.attachmentId = attachObject.Id;
        eventSub.subscriptionEventObj.sellToId = sellToId;
        eventSub.subscriptionEventObj.whatId = attachObject.ParentId;
        eventSub.subscribeEvent();
        System.debug('***********saveResultobj*'+eventSub.saveResultobj);
        System.debug('***********eventSub.subscriptionEventObj.attachmentId*'+eventSub.subscriptionEventObj.attachmentId);
        System.debug('***********subscriptionEventObj.sellToId*'+eventSub.subscriptionEventObj.sellToId);
        System.debug('***********subscriptionEventObj.eventType*'+eventSub.subscriptionEventObj.eventType);
        System.debug('***********subscriptionEventObj.whatId*'+eventSub.subscriptionEventObj.whatId);
        return eventSub;
    }
}