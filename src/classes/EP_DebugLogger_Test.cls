/**
 * @author <Sandeep Kumar>
 * @name <EP_DebugLogger_Test>
 * @createDate <08/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_DebugLogger_Test {
    
    /* 
     * @Description : When DEBUG_LOG record is not in custom setting EP_Debug_Log_Switch_Setting__c, 
     *    debug log should not be printed
     */
    private static testMethod void noDebugLogCase1(){
        //custom setting record other than DEBUG_LOG record 
        EP_Debug_Log_Switch_Setting__c switchSetting = new EP_Debug_Log_Switch_Setting__c(Name = 'testRecord', Enable__c = true);
        Database.insert(switchSetting);
        Test.startTest();
        String message = 'Test Message';
        EP_DebugLogger.printDebug('Default Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        EP_DebugLogger.printDebug(LoggingLevel.INFO, 'Info Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        Test.stoptest();
    }
    
    /* 
     * @Description : When there is no record in custom setting EP_Debug_Log_Switch_Setting__c,
     *    debug log should not be printed
     */
    private static testMethod void noDebugLogCase2(){
        Test.startTest();
        String message = 'Test Message';
        EP_DebugLogger.printDebug('Default Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        EP_DebugLogger.printDebug(LoggingLevel.INFO, 'Info Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        Test.stoptest();
    }
    
    /* 
     * @Description : When DEBUG_LOG record is in custom setting EP_Debug_Log_Switch_Setting__c,
     *     but its disabled, debug log should not be printed
     */
    private static testMethod void noDebugLogCase3(){
        //disabled DEBUG_LOG record 
        EP_Debug_Log_Switch_Setting__c switchSetting = new EP_Debug_Log_Switch_Setting__c(Name = EP_Common_Constant.DEBUG_LOG, Enable__c = false);
        Database.insert(switchSetting);
        Test.startTest();
        String message = 'Test Message';
        EP_DebugLogger.printDebug('Default Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        EP_DebugLogger.printDebug(LoggingLevel.INFO, 'Info Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, null);
        Test.stoptest();
    }

    /* 
     * @Description : When DEBUG_LOG record is in custom setting EP_Debug_Log_Switch_Setting__c,
     *     but its enabled, debug log will be printed
     */
    private static testMethod void debugLogCase1(){
        //enabled DEBUG_LOG record 
        EP_Debug_Log_Switch_Setting__c switchSetting = new EP_Debug_Log_Switch_Setting__c(Name = EP_Common_Constant.DEBUG_LOG, Enable__c = true);
        Database.insert(switchSetting);
        Test.startTest();
        String message = 'Test Message';
        EP_DebugLogger.printDebug('Default Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, 'Default Log' + message);
        EP_DebugLogger.printDebug(LoggingLevel.INFO, 'Info Log' + message);
        system.assertEquals(EP_DebugLogger.DEBUG_LOG_MESSAGE, 'Info Log' + message);
        Test.stoptest();
    }

}