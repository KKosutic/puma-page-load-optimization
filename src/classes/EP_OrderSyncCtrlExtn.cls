/* 
  @Author : Accenture
  @Name : EP_OrderSyncCtrlExtn
  @CreateDate  : 02/08/2017
  @Description : This is Controller Extension to generate XML for Order Sync with NAV
  @Version 1.0
*/
public with sharing class EP_OrderSyncCtrlExtn {  
    private string messageId;
    private string messageType;
    private string companyCode;
    private EP_CS_OutboundMessageSetting__c msgSetting;
    private string secretCode;
    private csord__Order__c ord;
    
    public EP_OutboundMessageHeader headerObj {get;set;}
    public String encodedPayload {get;set;}
    //L4 #45467 Start
    public string docType{get;set;}
    //L4 #45467 End
    /**
    * @Author : Accenture
    * @Name : EP_OrderSyncCtrlExtn
    * @Date : 02/08/2017
    * @Description : The extension constructor initializes the members variable orderObject by using the getRecord method from the standard controller.
    * @Param : ApexPages.StandardController
    * @return :
    */
    public EP_OrderSyncCtrlExtn(ApexPages.StandardController stdController) {
        this.secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        Id orderId = stdController.getRecord().Id;
        this.ord = [SELECT Id, EP_Puma_Company_Code__c FROM csord__Order__c WHERE Id = :orderId];
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        this.companyCode =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.SOURCE_COMPANY);
        this.msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(this.messageType);
        this.headerObj = new EP_OutboundMessageHeader();
        //L4 #45467 Start
        this.setDocumentType();
        //L4 #45467 End
        this.setHeader();
        System.debug('****** headerObj.ObjectName: ' + headerObj.ObjectName);
        this.setPayload();
        
    }
    
    /**
    * @Author : Accenture
    * @Name : setHeader
    * @Date : 02/08/2017
    * @Description: This method will be use setup the valuse for Header in XML message
    * @Param :
    * @return :
    */
    @testVisible
    private void setHeader() {
        EP_GeneralUtility.Log('Private','EP_OrderSyncCtrlExtn','setHeader');
        this.headerObj = EP_OutboundMessageUtil.setMessageHeader(msgSetting, this.messageId, this.companyCode);
    }

    /**
    * @Author : Accenture
    * @Name : setPayload
    * @Date : 02/08/2017
    * @Description: This method will be use to load the payload page and setup the values for Payload in XML message
    * @Param :
    * @return :
    */
    @testVisible
    private void setPayload() {
        EP_GeneralUtility.Log('Private','EP_OrderSyncCtrlExtn','setPayload');
        this.encodedPayload = EP_OutboundMessageUtil.getPayloadXML(msgSetting,this.ord.Id, this.messageId, this.companyCode);
    }
    
   /**
    * @Author : Accenture
    * @Name : checkPageAccess
    * @Date : 02/08/2017
    * @Description: This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
    * @Param :
    * @return : PageReference
    */
    @testVisible
    public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_OrderSyncCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        //if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            //pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        //}
        return pageRef;
    }
    //L4 #45467 Start
    @testVisible
    private void setDocumentType() {    
        docType = this.messageType.equalsIgnorecase('SEND_PICKUPADVICE_DOCUMENT_REQUEST')? 'pickupAdvice' : 'orderConfirmation';
    }
    //L4 #45467 End
}