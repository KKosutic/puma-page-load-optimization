@isTest
private class EP_LookUp_Helper_UT {
	

	@isTest static void hideClearSearchButton_Test() {
		EP_AccountNotificationTestDataUtility.createLookUPData('select id,name from Contact ','Contact','Contact','where accountid= ');
		Account acc = EP_TestDataUtility.getSellTo();
		PageReference pageRef = Page.EP_LookUP;
        Test.setCurrentPage(pageRef);
        apexpages.CurrentPage().getParameters().put(EP_Common_Constant.OBJECTNAME, 'Contact');
        apexpages.CurrentPage().getParameters().put(EP_Common_Constant.ID, acc.id);
        Test.startTest();
        SObject obj;
		EP_LookUp_Context ctx = new EP_LookUp_Context(obj);
		ctx.IdVariable = acc.id;
		ctx.objectVariable = 'Contact';
		EP_LookUp_Helper lookUpObj = new EP_LookUp_Helper(ctx);
        lookUpObj.hideClearSearchButton();
        Test.stopTest();
        system.assert(true);
	}
	
	@isTest static void lookUpRecords_Test() {
		EP_AccountNotificationTestDataUtility.createLookUPData('select id,name from Contact ','Contact','Contact','where accountid= ');
		Account acc = EP_TestDataUtility.getSellTo();
		Contact con = EP_TestDataUtility.createTestRecordsForContact(acc);
		PageReference pageRef = Page.EP_LookUP;
        Test.setCurrentPage(pageRef);
        SObject obj;
        apexpages.CurrentPage().getParameters().put(EP_Common_Constant.OBJECTNAME, 'Contact');
        apexpages.CurrentPage().getParameters().put(EP_Common_Constant.ID, acc.id);
        apexpages.CurrentPage().getParameters().put(EP_Common_Constant.SEARCHTEXT, 'DOE');
        Test.startTest();
        EP_LookUp_Context ctx = new EP_LookUp_Context(obj);
		ctx.IdVariable = acc.id;
		ctx.objectVariable = 'Contact';
		ctx.searchText = 'DOE';
		EP_LookUp_Helper lookUpObj = new EP_LookUp_Helper(ctx);
        lookUpObj.lookUpRecords();
        Test.stopTest();
        system.assert(true);
	}
	
}