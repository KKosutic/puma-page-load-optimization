@isTest
public class EP_WINDMSOrderNewHandler_UT
{
    public static final String TEXT = 'Test';
    public static final String ERRORCD = '12345';
    public static final String ACC_NAME = 'Account1';
    
    static testMethod void processRequest_PositiveTest() {
        EP_WINDMSOrderNewHandler localObj = new EP_WINDMSOrderNewHandler();
        EP_WINDMSOrderNewHelper localHelperObj = new EP_WINDMSOrderNewHelper();
       
        EP_WINDMSOrderNewStub.OrderWrapper orderWrapper = EP_WINDMSOrderNewHelper_UT.getOrderWrapper();
        EP_WINDMSOrderNewStub  stub = EP_TestDataUtility.createWINDMSOrderNewStub();
        
        stub.MSG.Payload.any0.Orders.order = new list<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        String jsonBody = System.JSON.serialize(stub);
        
        Test.startTest();
        	String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
        System.AssertNotEquals(Null,result);
    }
    static testMethod void processRequest_NegativeTest() {
        EP_WINDMSOrderNewHandler localObj = new EP_WINDMSOrderNewHandler();
       
        EP_WINDMSOrderNewStub  stub = EP_TestDataUtility.createWINDMSOrderNewStub();
        
        String jsonBody = System.JSON.serialize(stub);
        
        Test.startTest();
        	String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
        System.AssertNotEquals(Null,result);
    }
    static testMethod void createOrders_PositiveTest() {
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new List<EP_WINDMSOrderNewStub.orderWrapper>{EP_TestDataUtility.createWINDMSOrderWrapper()};
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderObj.EP_Order_Comments__c = TEXT;
        orderObj.EP_SeqId__c = 'SEQ-123';
        orderWrapperList[0].sfOrder = orderObj ;
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.createOrders(orderWrapperList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c,EP_SeqId__c FROM csord__Order__c WHERE Id =:orderWrapperList[0].sfOrder.id ];
        System.AssertEquals(TEXT,ordObj.EP_Order_Comments__c);
        System.AssertEquals(orderObj.EP_SeqId__c,ordObj.EP_SeqId__c);
    }
    static testMethod void createOrders_NegativeTest() {
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new List<EP_WINDMSOrderNewStub.orderWrapper>{EP_TestDataUtility.createWINDMSOrderWrapper()};
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderObj.EP_Order_Comments__c = TEXT;
        orderWrapperList[0].sfOrder = orderObj ;
        orderWrapperList[0].seqId = 'SEQ-123';
        orderWrapperList[0].errorDescription = 'Error';
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.createOrders(orderWrapperList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderWrapperList[0].sfOrder.id ];
        System.AssertNotEquals(TEXT, ordObj.EP_Order_Comments__c);
        System.AssertNotEquals(0, EP_WINDMSOrderNewHandler.ackResponseList.size());
    }
    static testMethod void doOrderUpsert_test() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].EP_Order_Comments__c = TEXT;
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.doOrderUpsert(orderList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderList[0].id ];
        System.AssertEquals(TEXT,ordObj.EP_Order_Comments__c);
    }
    static testMethod void processUpsertErrors_test() { 
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new  List<EP_WINDMSOrderNewStub.orderWrapper>{EP_TestDataUtility.createWINDMSOrderWrapper()};
        List<Database.Error> errorList = new List<Database.Error>();
        List<Account> listAccounts = new List<Account>{new Account(Name=ACC_NAME),new Account()};
        Database.SaveResult[] srList = Database.insert(listAccounts, false);
        errorList.add(srList[0].getErrors()[0]);
        String seqId =  orderWrapperList[0].seqId;
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.processUpsertErrors(errorList,seqId);
        Test.stopTest();
        
        System.AssertEquals(true,EP_WINDMSOrderNewHandler.ackResponseList.size() > 0);
    }
    static testMethod void createOrdersItems_PositiveTest() {
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new  List<EP_WINDMSOrderNewStub.orderWrapper>{EP_TestDataUtility.createWINDMSOrderWrapper()};
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = EP_WINDMSOrderNewHelper_UT.getOrderWrapper();
        orderWrapperList.add(orderWrapper);
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.createOrdersItems(orderWrapperList);
        Test.stopTest();
        
        list<OrderItem> ordItemObj = [SELECT Id FROM OrderItem WHERE OrderId =:orderWrapperList[0].sfOrder.id];
        System.AssertEquals(true, ordItemObj.size() > 0 );
    }
    
    static testMethod void createOrdersItems_NegativeTest() {
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new  List<EP_WINDMSOrderNewStub.orderWrapper>{EP_TestDataUtility.createWINDMSOrderWrapper()};
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = EP_WINDMSOrderNewHelper_UT.getOrderWrapper();
        orderWrapperList.add(orderWrapper);
        for(EP_WINDMSOrderNewStub.orderWrapper orderWrapperObj : orderWrapperList) {
        	for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapperObj.OrderLines.orderLine){
                ordLine.errorDescription = 'Error';
            }
        }
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.createOrdersItems(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(true,EP_WINDMSOrderNewHandler.ackResponseList.size() > 0);
    }
    
    static testMethod void processOrderItemsError_test() {
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = EP_WINDMSOrderNewHelper_UT.getOrderWrapper();
        orderWrapper.seqId = 'SEQ123';
        orderWrapper.isExistingOrder = false;

        Test.startTest();
        	EP_WINDMSOrderNewHandler.processOrderItemsError(orderWrapper, orderWrapper.seqId,'errorDescription');
        Test.stopTest();
        
        List<csord__Order__c> orderObj = [SELECT Id FROM csord__Order__c WHERE Id=:orderWrapper.sfOrder.id];
        System.AssertEquals(0,orderObj.size());
    }
    
    static testMethod void doOrderItemUpsert_test() {
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createWINDMSOrderWrapper();
        LIST<csord__Order_Line_Item__c> orderItemsToBeProcess = [SELECT Id, OrderId__c  FROM csord__Order_Line_Item__c WHERE OrderId__c =: orderWrapper.SFOrder.id];
        //orderItemsToBeProcess[0].EP_Eligible_for_Rebate__c = True;
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.doOrderItemUpsert(orderItemsToBeProcess);
        Test.stopTest();
        
        csord__Order_Line_Item__c orderItemObj = [SELECT Id
                                                    //, EP_Eligible_for_Rebate__c 
                                                    FROM csord__Order_Line_Item__c WHERE Id =:orderItemsToBeProcess[0].id];
        //System.AssertEquals(true,orderItemObj.EP_Eligible_for_Rebate__c);
    }
    static testMethod void deleteOrder_test() {
        List<csord__Order__c> ordersObj = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.deleteOrder(ordersObj);
        Test.stopTest();
        
        List<csord__Order__c> orderObj = [SELECT Id , OrderNumber__c FROM csord__Order__c WHERE Id =:ordersObj[0].id];
        System.AssertEquals(true,orderObj.isEmpty());
    }
    static testMethod void createResponse_test() {
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createWINDMSOrderWrapper();
        String seqId = orderWrapper.seqId;

        Test.startTest();
        	EP_WINDMSOrderNewHandler.createResponse(seqId,'errorCode','errorDescription');
        Test.stopTest();
        
        System.AssertEquals(true,EP_WINDMSOrderNewHandler.ackResponseList.size() > 0);
    }
    static testMethod void updateOrders_test() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].EP_Order_Comments__c = TEXT;
        
        Test.startTest();
        	EP_WINDMSOrderNewHandler.updateOrders(orderList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderList[0].id ];
        System.AssertEquals(TEXT,ordObj.EP_Order_Comments__c);
    }
    
    static testMethod void updateOrders_NegativeTest() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].EP_Order_Comments__c = TEXT;
        orderList[0].Status__c = '';
        Test.startTest();
        	EP_WINDMSOrderNewHandler.updateOrders(orderList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderList[0].id ];
        System.AssertNotEquals(TEXT,ordObj.EP_Order_Comments__c);
    }
}