public with sharing class EP_OrderLinesAndSummaryHelper_UX{
    public EP_OrderPageContext_UX ctx ;
    public static final String CLASSNAME = 'EP_OrderLinesAndSummaryHelper_UX';

    public EP_OrderLinesAndSummaryHelper_UX(EP_OrderPageContext_UX ctx){
     this.ctx = ctx;
    } 

    public void prepareProductDetails() {
        EP_GeneralUtility.Log('public',CLASSNAME,'prepareProductDetails');
        // Store the Ship-To and Pickup Location ID used to produce the data in step 3       
        setProductDetails();
        setQuantityRestrictions();
    }
    public void setProductDetails() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setProductDetails');
        EP_PriceBookEntryMapper pbEntryMapperObj = new EP_PriceBookEntryMapper();
        List < PriceBookEntry > priceBookEntries;
        ctx.strSelectedPricebookID = NULL;
        ctx.intAccountProductsFound = 0;
        ctx.productPriceMap = new Map < String, Decimal > ();
        ctx.productNameMap = new Map < String, String > ();
        ctx.productPriceBookEntryMap = new Map < String, String > ();
        ctx.productUnitOfMeasureMap = new Map < String, String > ();
        ctx.setProductIDs = new List < String > ();
        ctx.strSelectedPricebookID = ctx.accountDomainObj.getPriceBook();
        system.debug('ctx.strSelectedPricebookID ' + ctx.strSelectedPricebookID);
        Set<Id> availableProductIds = new Set<Id>();
        if(String.isNotBlank(ctx.strSelectedPickupLocationID)&& ctx.isExrackOrder ){
            availableProductIds = getAvailableProducts(ctx.strSelectedPickupLocationID);
        }
        if (ctx.strSelectedPricebookID == NULL) {
            return;
        }
        priceBookEntries = ctx.orderService.findPriceBookEntries(ctx.strSelectedPricebookID,ctx.newOrderRecord);
        system.debug('priceBookEntries ' + priceBookEntries);
        if (priceBookEntries.isEmpty()) {
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, Label.EP_No_Product_To_Choose);
            return;
        }
        for (PricebookEntry pbe: priceBookEntries) {
            // Update the memory maps
            ctx.productPriceMap.put(pbe.Id, pbe.UnitPrice);
            ctx.productPriceBookEntryMap.put(pbe.Id, pbe.Product2Id);
            ctx.PriceBookEntryproductMap.put(pbe.Product2Id, pbe.Id); // tank work
            ctx.productUnitOfMeasureMap.put(pbe.Id, pbe.EP_Unit_Of_Measure__c);
            ctx.productNameMap.put(pbe.Id, pbe.Product2.Name);
            ctx.setProductIDs.add(pbe.Product2Id);
            ctx.intAccountProductsFound++;
            // If the PriceBookEntry does not have a UoM then add the product default one
            if (pbe.EP_Unit_Of_Measure__c == NULL) {
                ctx.productUnitOfMeasureMap.put(pbe.Id, pbe.Product2.EP_Unit_Of_Measure__c);
            }

            if(!pbe.Product2.EP_Is_System_Product__c){
                ctx.numberOfAvailableProducts++;
            }

            ctx.pricebookEntryMap.put(pbe.id,pbe);
        }
         // End for
        
    }
    /*
    Get Available Products
    */
    public Set<Id> getAvailableProducts(Id supplyLocationId) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'getAvailableProducts');
        Set<Id> productIds = new Set<Id>();
        system.debug('ctx.mapOfSupplyLocation ' + ctx.mapOfSupplyLocation);
        system.debug('supplyLocationId ' + supplyLocationId);

        Id storageLocId = ctx.mapOfSupplyLocation.get(supplyLocationId).Stock_Holding_Location__c;
        for(Ep_Inventory__c inventory : [ Select EP_Product__c,EP_Product__r.EP_Product_Sold_As__c from  Ep_Inventory__c where EP_Storage_Location__c =: storageLocId AND EP_Product__r.EP_Product_Sold_As__c =: ctx.newOrderRecord.EP_Order_Product_Category__c ]) { 
            productIds.add(inventory.EP_Product__c);
        }
        return productIds;
    }
    
    public void setQuantityRestrictions() {
        EP_GeneralUtility.Log('public',CLASSNAME,'setQuantityRestrictions');
        String country;
        if (!ctx.isExrackOrder){ 
            system.debug('ctx.strSelectedShipToID ' + ctx.strSelectedShipToID);
            system.debug('ctx.mapOfShipTo ' + ctx.mapOfShipTo);
            country = ctx.mapOfShipTo.get(ctx.strSelectedShipToID).EP_Country__c; 
        }   
        else{
            system.debug('ctx.strSelectedPickupLocationID ' + ctx.strSelectedPickupLocationID);
            country = ctx.mapOfSupplyLocation.get(ctx.strSelectedPickupLocationID).Stock_Holding_Location__r.EP_Country__c;
        }
        system.debug('Country is ' + country);
        system.debug('ctx.setProductIDs ' + ctx.setProductIDs);
        List < EP_Order_Configuration__c > listOrderConfigurations = new EP_OrderConfigMapper().getRecordsByIdsCountry(ctx.setProductIDs,country);
        ctx.availableProductQuantitiesMap = new Map < String, List < String >> ();
        String strKey;
        List<String> productQty;
        if (!listOrderConfigurations.isEmpty()) {
            ctx.availableProductQuantitiesMap =  EP_PortalOrderUtil.getavailableProductQuantitiesMap(listOrderConfigurations);
            ctx.productRangeMap = EP_PortalOrderUtil.getproductRangeMap(listOrderConfigurations);
        } 
        else {
            String strOrderConfigurationError = Label.EP_System_Setup_For_Market_Incomplete;
            strOrderConfigurationError = strOrderConfigurationError.replace(EP_Common_Constant.GENERIC_MSG_REPLACE_STRING, ctx.orderAccount.EP_Country__c);
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, strOrderConfigurationError));
            ctx.hasAccountSetupError = true;
        }
    }
    public void addNewLine(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'addNewLine'); 

        if(ctx.isBulkOrder){
            return;
        }
        // Create new wrapper item
        EP_OrderPageContext_UX.OrderWrapper orderWrapper = new EP_OrderPageContext_UX.OrderWrapper();
        orderWrapper.oliAvailableProducts = getProductOptions();
        orderWrapper.oliAvailableTanks = getListShipToTanks(); // tank work   
        //ctx.listofOrderWrapper.add(orderWrapper);
        Integer currentSizeofMap = ctx.orderWrapperMap != null ? ctx.orderWrapperMap.size(): 0;
        ctx.orderWrapperMap.put(currentSizeofMap, orderWrapper);
        system.debug('Size comparision ' + ctx.orderWrapperMap.size() + '---' + orderWrapper.oliAvailableProducts.size() );
        ctx.showAddProducts = true;
        if(ctx.orderWrapperMap.size() == orderWrapper.oliAvailableProducts.size() - 1){
            ctx.showAddProducts = false;
        }
    }

    public void removeNewLine(Integer index){
        EP_OrderPageContext_UX.OrderWrapper orderWrapper = ctx.orderWrapperMap.get(index);
        List<csord__Order_Line_Item__c> deleteOrderItemslst = new List<csord__Order_Line_Item__c>();
        // reinitializing, set when the refetch order lines is completed
        ctx.isPricingAvailable = false;
        if(orderWrapper.orderLineItem.id != null){
            deleteOrderItemslst.add(orderWrapper.orderLineItem);
            deleteOrderItemslst.addAll(orderWrapper.listChildOrderLineItems);
            delete deleteOrderItemslst;
            refetchOrderLines();
        }

        if(orderWrapper.orderLineItem.id == null){
            ctx.orderWrapperMap.remove(index);
        }
        ctx.showAddProducts = false;
        if(ctx.orderWrapperMap.size() < orderWrapper.oliAvailableProducts.size() - 1){
            ctx.showAddProducts = true;
        }
    }    
    
    public void setAndValidateOrderLines() {
        EP_GeneralUtility.Log('public',CLASSNAME,'setAndValidateOrderLines');
        csord__Order_Line_Item__c ordItem;
        csord__Order_Line_Item__c wrapperordItem;
        Integer counter = 0;
        boolean isQuantitySet =  false;
        ctx.hasinvalidorderlines = false;
        system.debug('ctx.listofNewOrderWrapper '+ ctx.listofNewOrderWrapper);
        system.debug('ctx.orderWrapperMap '+ ctx.orderWrapperMap);

        removeAdditionalOrderLines();
        addFilledBulkOrderLines();

        // avoid validation of bulk order lines and packaged order lines
        if(ctx.hasinvalidorderlines){
            return;
        }
        system.debug('ctx.orderWrapperMap before populating order item in map  '+ ctx.orderWrapperMap);
        for(EP_OrderPageContext_UX.OrderWrapper orderWrapper :ctx.orderWrapperMap.values()){
            if(String.isBlank(orderWrapper.oliQuantityStr)){
                continue;
            }

            orderWrapper.oliQuantity = Integer.valueOf(orderWrapper.oliQuantityStr.trim());

            if(hasInvalidateROFields(orderWrapper)){
                ctx.hasinvalidorderlines = true;
                return;
            }

            if(isBolValid(orderWrapper)){
                ctx.hasinvalidorderlines = true;
                ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter BOL Number');
                ApexPages.addMessage(msg);
                return;
            }

            system.debug('Valid order line **** ' + orderWrapper);
            validateEnteredQuantity(orderWrapper);
            isQuantitySet =true;

            if(orderWrapper.oliIsValid){
                //TODO: do object initialization is required 
                ordItem = new csord__Order_Line_Item__c();
                wrapperordItem = orderWrapper.orderlineitem;

                // if the orderitem is inserted, avoid overriding. Else creates duplicate order item
                if(wrapperordItem.Id != null){
                    wrapperordItem.Quantity__c = orderWrapper.oliQuantity;  
                    continue;
                }
                orderWrapper.orderlineitem = ordItem;
                orderWrapper.oliIndex = counter;
                
                ordItem.EP_Quantity_UOM__c = orderWrapper.oliProductUoM;
                ordItem.Quantity__c = orderWrapper.oliQuantity;
                ordItem.PriceBookEntryId__c = orderWrapper.oliPricebookEntryID;
                ordItem.OrderId__c = ctx.newOrderRecord.Id;
                ordItem.UnitPrice__c = ctx.productPriceMap.get(ordItem.PriceBookEntryId__c);
                ordItem.EP_Order_Line_Relationship_Index__c = orderWrapper.oliIndex;
                ordItem.EP_Is_Standard__c = true;
                ordItem.EP_Tank__c = orderWrapper.oliTanksID;

                if(ctx.isROorder){

                    ordItem.EP_Ambient_Loaded_Quantity__c = wrapperordItem.EP_Ambient_Loaded_Quantity__c;
                    ordItem.EP_Standard_Loaded_Quantity__c = wrapperordItem.EP_Standard_Loaded_Quantity__c;
                    ordItem.EP_Ambient_Delivered_Quantity__c = wrapperordItem.EP_Ambient_Delivered_Quantity__c;
                    ordItem.EP_Standard_Delivered_Quantity__c = wrapperordItem.EP_Standard_Delivered_Quantity__c;

                    if(!ctx.isConsumptionOrder){
                        ordItem.EP_BOL_Number__c = wrapperordItem.EP_BOL_Number__c;
                        ordItem.EP_3rd_Party_Stock_Supplier__c = wrapperordItem.EP_3rd_Party_Stock_Supplier__c;    
                        ordItem.EP_Contract__c = wrapperordItem.EP_Contract__c;
                    }

                }
                
                validateInventory(orderWrapper);
            }
            counter++;
        }

        if(!isQuantitySet){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter Quantity for atleast one product');
            ApexPages.addMessage(msg);
            ctx.hasinvalidorderlines = true;
            return;
        }

        ctx.hasinvalidorderlines = false;

        system.debug('ctx.orderWrapperMap after populating orderitem in map '+ ctx.orderWrapperMap);
       
    }

    @TestVisible
    private boolean hasInvalidateROFields(EP_OrderPageContext_UX.OrderWrapper orderWrapper){
        EP_GeneralUtility.Log('private',CLASSNAME,'hasInvalidateROFields');
        boolean invalidQuantity = false;

        if(!ctx.isROorder){
            return false;
        }

        system.debug('orderWrapper is ' + orderWrapper);
        if(orderWrapper.orderlineitem.EP_Ambient_Loaded_Quantity__c == null){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter Amibient Loaded Quantity');
            ApexPages.addMessage(msg);
            invalidQuantity = true; 
        }
        if(orderWrapper.orderlineitem.EP_Ambient_Delivered_Quantity__c ==null){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter Ambient Delivered Quantity');
            ApexPages.addMessage(msg);
            invalidQuantity = true;
        }
        if(orderWrapper.orderlineitem.EP_Standard_Loaded_Quantity__c == null){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter Standard Loaded Quantity');
            ApexPages.addMessage(msg);
            invalidQuantity = true;
        } 

        if(orderWrapper.orderlineitem.EP_Standard_Delivered_Quantity__c == null){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter Standard Delivered Quantity');
            ApexPages.addMessage(msg);
            invalidQuantity = true;
        }

        return invalidQuantity;
    }

    @TestVisible
    private boolean isBolValid(EP_OrderPageContext_UX.OrderWrapper orderWrapper){
        EP_GeneralUtility.Log('private',CLASSNAME,'isBolValid');
        system.debug('isBolValid' + orderWrapper);
        system.debug(!ctx.isConsumptionOrder + '---' + ctx.isROorder + '----' +String.isBlank(orderWrapper.orderlineitem.EP_BOL_Number__c) +'---');
        return (!ctx.isConsumptionOrder && ctx.isROorder && String.isBlank(orderWrapper.orderlineitem.EP_BOL_Number__c));
    }

    @TestVisible
    private void addFilledBulkOrderLines(){
        EP_GeneralUtility.Log('public',CLASSNAME,'addFilledBulkOrderLines');

        if(!ctx.isBulkOrder){
            return;
        }

        map<id,EP_OrderPageContext_UX.OrderWrapper> updatedOrderitems = new map<id,EP_OrderPageContext_UX.OrderWrapper>();
        List<EP_OrderPageContext_UX.OrderWrapper> newOrderLinesList = new List<EP_OrderPageContext_UX.OrderWrapper>();
        //adding inline bulk order list to the map of order wrapper
        Integer cnt = 0;
        for(EP_OrderPageContext_UX.OrderWrapper oWrap: ctx.listofNewOrderWrapper.clone()){
            system.debug('---'+oWrap.oliQuantityStr+'---');

            if(String.isBlank(oWrap.oliQuantityStr)){
                cnt++;
                continue;
            }

            if(hasInvalidateROFields(oWrap)){
                ctx.hasinvalidorderlines = true;
                return;
            }

            if(isBolValid(oWrap)){
                ctx.hasinvalidorderlines = true;
                ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Enter BOL Number');
                ApexPages.addMessage(msg);
                return;
            }

            if(Integer.valueOf(oWrap.oliQuantityStr.trim()) > 0){ 
                oWrap.oliQuantity = Integer.valueOf(oWrap.oliQuantityStr.trim()); 
                updatedOrderitems.put(oWrap.oliPricebookEntryID, oWrap);
                system.debug('cnt is ' + cnt);
                system.debug('listofNewOrderWrapper size is ' + ctx.listofNewOrderWrapper.size());
                ctx.listofNewOrderWrapper.remove(cnt);
                cnt--;
            }
            cnt++;
        }


        system.debug('updatedOrderitems ' + updatedOrderitems);
        system.debug('ctx.orderWrapperMap in addFilledBulkOrderLines ' + ctx.orderWrapperMap);
        system.debug('ctx.listofNewOrderWrapper in addFilledBulkOrderLines ' + ctx.listofNewOrderWrapper);

        //Checking if the products is already added in the pricing. Id exisits, simply updates quantity in the orderwrapper map
        for(EP_OrderPageContext_UX.OrderWrapper orderWrapper :ctx.orderWrapperMap.values()){
            if(!updatedOrderitems.containsKey(orderWrapper.oliPricebookEntryID)){
                newOrderLinesList.add(orderWrapper);
                continue;
            }
            orderWrapper.oliQuantity = updatedOrderitems.get(orderWrapper.oliPricebookEntryID).oliQuantity;
        }

        // adding bulk order line to orderwrapper map for further processign
        Integer indexval = ctx.orderWrapperMap.size();
        for(EP_OrderPageContext_UX.OrderWrapper oWrapper: updatedOrderitems.values()){
            ctx.orderWrapperMap.put(indexval, oWrapper);    
            indexval++;
        }

        system.debug('After adding bulk, ctx.orderWrapperMap '+ ctx.orderWrapperMap);
    }


    @TestVisible
    private void removeAdditionalOrderLines(){
        EP_GeneralUtility.Log('public',CLASSNAME,'removeAdditionalOrderLines');
        List<csord__Order_Line_Item__c> deleteOrderItems = new List<csord__Order_Line_Item__c>();
        Integer index = 0;
        /*for(EP_OrderPageContext_UX.OrderWrapper orderWrapper :ctx.orderWrapperMap.values()){
            if(orderWrapper.oliIsAdditionalItem!= null && orderWrapper.oliIsAdditionalItem){
                deleteOrderItems.add(OrderWrapper.orderLineItem);
            }
            index++;
        }*/

        // Removing previous prices
        EP_OrderPageContext_UX.OrderWrapper orderWrapper;
        for(Integer indexKey: ctx.orderWrapperMap.keySet().clone()){
            orderWrapper = new EP_OrderPageContext_UX.OrderWrapper();
            orderWrapper = ctx.orderWrapperMap.get(indexKey);
            if(orderWrapper.oliIsAdditionalItem!= null && orderWrapper.oliIsAdditionalItem){
                deleteOrderItems.add(OrderWrapper.orderLineItem);
                ctx.orderWrapperMap.remove(indexKey);
            }

        }
        system.debug('deleteOrderItems ' + deleteOrderItems);
        if(!deleteOrderItems.isEmpty())
            delete deleteOrderItems;
    }

    @TestVisible
    private void validateEnteredQuantity(EP_OrderPageContext_UX.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'validateEnteredQuantity');
        String strSelectedProductID;
        Boolean isValid = TRUE;
        orderWrapper.oliProductError = EP_Common_Constant.BLANK;
        orderWrapper.oliQuantityError = EP_Common_Constant.BLANK;
        // Ensure that the user has selected products for all order line items
        if (string.isblank(orderWrapper.oliPricebookEntryID)) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Select_product));
            orderWrapper.oliProductError = EP_Common_Constant.ERROR;
            ctx.hasinvalidorderlines = true;
            isValid = FALSE;
        }
        if (orderWrapper.oliQuantity == NULL) {
            isValid = FALSE;
            setQuantityError(orderWrapper);
            ctx.hasinvalidorderlines = true;
            return;
            //orderWrapper.oliQuantity = 0;
        }
        // Ensure that quantity is > 0
        if (orderWrapper.oliQuantity <= 0) {
            setQuantityError(orderWrapper);
            isValid = FALSE;
            ctx.hasinvalidorderlines = true;
            // 1. Validation for non mixing countries
            strSelectedProductID = ctx.productPriceBookEntryMap.get(orderWrapper.oliPricebookEntryID);
        }
        
        if(orderWrapper.oliTanksID != null && isQuantiyHigherthanSafefill(orderWrapper)){
            ApexPages.Message msg = EP_Common_Util.createApexMessage(ApexPages.Severity.Warning, LABEL.EP_Qty_entered_higher_than_safe_fill_level);
            ApexPages.addMessage(msg);
            ctx.hasinvalidorderlines = true;
        }
        orderWrapper.oliIsValid = isValid;
    }

    @TestVisible
    private boolean isQuantiyHigherthanSafefill(EP_OrderPageContext_UX.OrderWrapper orderWrapper){
        system.debug('--'+ctx.mapShipToOperationalTanks+'****'+orderWrapper.oliTanksID);
        //System.debug(ctx.isOprtnlTankAvail+'****'+ctx.mapShipToOperationalTanks.containsKey(orderWrapper.oliTanksID)+'********'+(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c != null)+'******'+orderWrapper.oliQuantity+'***'+Integer.valueOf(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c));
        return (ctx.isOprtnlTankAvail && ctx.mapShipToOperationalTanks.containsKey(orderWrapper.oliTanksID) && 
        (ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c != null) &&
        orderWrapper.oliQuantity > Integer.valueOf(ctx.mapShipToOperationalTanks.get(orderWrapper.oliTanksID).EP_Safe_Fill_Level__c));
        
    }

    @TestVisible
    private void setQuantityError(EP_OrderPageContext_UX.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'setQuantityError');
        ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Invalid_Quantity));
        orderWrapper.oliQuantityError = EP_Common_Constant.ERROR;
        
    }  

    public void validateInventory(EP_OrderPageContext_UX.OrderWrapper orderWrapper){
        EP_GeneralUtility.Log('public',CLASSNAME,'validateInventory');
        csord__Order_Line_Item__c orderItemObj = orderWrapper.orderLineItem;

        //TODO: revisit the inventory logic


        String productId= ctx.productPriceBookEntryMap.get(orderWrapper.oliPricebookEntryID);
        system.debug('ctx.productPriceBookEntryMap '+ ctx.productPriceBookEntryMap);
        orderItemObj.EP_Inventory_Availability__c = ctx.productInventoryMap.get(productId);
        system.debug('ctx.productInventoryMap '+ ctx.productInventoryMap);
        if(orderItemObj.EP_Inventory_Availability__c == EP_Common_Constant.LOW_INV) {
            apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_Low_Inventory_Error));
        }
        else if(orderItemObj.EP_Inventory_Availability__c == EP_Common_Constant.NO_INV) {
            apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_Inventory_Error));
        }
        else if(orderItemObj.EP_Inventory_Availability__c != EP_Common_Constant.GOOD_INV) {
            apexpages.addmessage(EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_Inventory_Not_Available));
            orderItemObj.EP_Inventory_Availability__c = EP_Common_Constant.INV_UNAVAILABLE;
            ctx.isSlottingEnabled = true;
        }
    }

    public void loadExisitingOrderItems(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'loadExisitingOrderItems'); 
        Integer index = 0;
        ctx.totalCostPE = 0.0;
        EP_OrderPageContext_UX.OrderWrapper oWrapper = new EP_OrderPageContext_UX.OrderWrapper();
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        List<csord__Order_Line_Item__c> listOrderItem = orderItemMapper.getCSNoParentRecsByOrderIds(new Set<Id>{ctx.newOrderRecord.Id}); 
        ctx.numberOfProductsInOrders = listOrderItem.size();
        system.debug('listOrderItem ' + listOrderItem);
        for(csord__Order_Line_Item__c orderItemObj : listOrderItem) {
            ctx.totalCostPE = ctx.totalCostPE + Double.valueOf(orderItemObj.EP_Total_Price__c); 
            /*oWrapper = populateOrderSummaryItem(orderItemObj.PricebookEntry__r.product2.name ,Integer.valueOf(orderItemObj.Quantity__c),orderItemObj.EP_Quantity_UOM__c,
            Double.valueOf(orderItemObj.UnitPrice__c),Double.valueOf(orderItemObj.EP_Pricing_Total_Amount__c),
            Double.valueOf(orderItemObj.EP_Total_Price__c),orderItemObj.CurrencyIsoCode,FALSE,orderItemObj.EP_Invoice_Name__c,
            Double.valueOf(orderItemObj.EP_Tax_Percentage__c),Double.valueOf(orderItemObj.EP_Tax_Amount__c));*/
            // as this is not applicable to child order items, have not included in populate order summary method
            oWrapper.oliPricebookEntryID = orderItemObj.PricebookEntryId__c;
            oWrapper.oliAvailableProducts = getProductOptions();
            oWrapper.oliAvailableTanks = getListShipToTanks();
            oWrapper.orderLineItem = orderItemObj;
            //oWrapper.listChildOrderLineItems = orderItemObj.order_Products__r;
            ctx.orderWrapperMap.put(index,oWrapper);
            index++;
            //index = populateChildrenLineItems(orderItemObj.order_Products__r,index);
            
        }  
        system.debug('ctx.orderWrapperMap in loadExisitingOrderItems ' + ctx.orderWrapperMap);
    }

    @TestVisible
    private Integer populateChildrenLineItems(list<csord__Order_Line_Item__c> OrderItemList,Integer counter){
        EP_GeneralUtility.Log('Public',CLASSNAME,'populateChildrenLineItems'); 
        system.debug('order item list size ' + OrderItemList.size());
        system.debug('order itemlist' + OrderItemList);
        system.debug('Counter is ' + counter);
        for( csord__Order_Line_Item__c orderItem : OrderItemList ) {
            ctx.isPricingAvailable = true;
            ctx.totalCostPE = ctx.totalCostPE+Double.valueOf(orderItem.EP_Total_Price__c);
            EP_OrderPageContext_UX.OrderWrapper orderWrapperObj;
            /*orderWrapperObj = populateOrderSummaryItem(orderItem.pricebookentry.product2.name,Integer.valueOf(orderItem.Quantity),NULL,
            Double.valueOf(orderItem.EP_Pricing_Total_Amount__c),Double.valueOf(orderItem.UnitPrice),
            Double.valueOf(orderItem.EP_Total_Price__c),orderItem.CurrencyIsoCode,TRUE,
            orderItem.EP_Invoice_Name__c,Double.valueOf(orderItem.EP_Tax_Percentage__c),
            Double.valueOf(orderItem.EP_Tax_Amount__c));*/
            orderWrapperObj.oliIsAdditionalItem = true;
            orderWrapperObj.orderLineItem = orderItem;
            ctx.orderWrapperMap.put(counter,orderWrapperObj);
            counter++;
            system.debug('ctx.orderWrapperMap ' + ctx.orderWrapperMap);
        }
        return counter;
    }   

    // methods popuplates inline bulk order lines 
    public void newOrderLines(){
        EP_GeneralUtility.Log('public',CLASSNAME,'newOrderLines'); 
        system.debug('ctx.numberOfAvailableProducts ' + ctx.numberOfAvailableProducts);
        system.debug('ctx.numberOfProductsInOrders ' + ctx.numberOfProductsInOrders);
        if(!ctx.isBulkOrder || ctx.numberOfAvailableProducts == ctx.numberOfProductsInOrders){
            return;
        }

        ctx.listofNewOrderWrapper = new List<EP_OrderPageContext_UX.OrderWrapper>();
        EP_OrderPageContext_UX.OrderWrapper oWrapper;
        system.debug('ctx.pricebookEntryMap size is' + ctx.pricebookEntryMap.size());
        system.debug('ctx.pricebookEntryMap ' + ctx.pricebookEntryMap);

        if(ctx.isOprtnlTankAvail){
            setTankDetailsofOrderWrapper();
            return;
        }

        for(PricebookEntry pbe: ctx.pricebookEntryMap.values()){
            if(pbe.Product2.EP_Is_System_Product__c){
                continue;
            }
            oWrapper = new EP_OrderPageContext_UX.OrderWrapper();
            oWrapper.oliPricebookEntryID = pbe.id;
            oWrapper.oliProductName = pbe.Product2.Name;
            oWrapper.oliProductUoM = pbe.EP_Unit_Of_Measure__c;
            if (pbe.EP_Unit_Of_Measure__c == NULL) {
                oWrapper.oliProductUoM = pbe.Product2.EP_Unit_Of_Measure__c;
            }
            ctx.listofNewOrderWrapper.add(oWrapper);
        }

    }

    //used for Modification flow bulk order
    public void removeDuplicateOrderLines(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'removeDuplicateOrderLines');
        Integer counter = 0;
        if(ctx.listofNewOrderWrapper == null || ctx.listofNewOrderWrapper.isEmpty()){
            return;
        }
        system.debug('ctx.listofNewOrderWrapper ' + ctx.listofNewOrderWrapper);

        for(EP_OrderPageContext_UX.OrderWrapper oWrap: ctx.listofNewOrderWrapper.clone()){
            if(String.isNotBlank(oWrap.oliPricebookEntryID) && isOrderlineExistInOrderMap(oWrap.oliPricebookEntryID)){
                ctx.listofNewOrderWrapper.remove(counter);
            }
            counter++;
        }
    }

    private boolean isOrderlineExistInOrderMap(String pricebookentryid){
        EP_GeneralUtility.Log('Public',CLASSNAME,'isOrderlineExistInOrderMap');
        system.debug('ctx.orderWrapperMap ' +ctx.orderWrapperMap);
        for(EP_OrderPageContext_UX.OrderWrapper oWrap: ctx.orderWrapperMap.values()){
            if(pricebookentryid.equalsignorecase(oWrap.oliPricebookEntryID)){
                return true;
            }
        }
        return false;
    }

    public List<SelectOption> getProductOptions() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'getProductOptions');
        List<SelectOption> listProductOptions = new List <SelectOption> ();
        Map <String, String> selectedProductMap = new Map <String, String> ();
        listProductOptions.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE)); // Hardcoding removal

        String strProductSetupKey;
        String strUoM;
        
        // TODO: needs revist
        if(ctx.listofOrderWrapper != null && !ctx.listofOrderWrapper.isEmpty()){
            // Build list of products that are already selected by the user (product and UoM)
            for (EP_OrderPageContext_UX.OrderWrapper orderWrapper: ctx.listofOrderWrapper) {
                selectedProductMap.put(orderWrapper.oliPricebookEntryID + orderWrapper.oliProductUoM, orderWrapper.oliProductUoM);
            } // End for
        }
        // Dynamicaly build the list to ensure that products already selected are not re-selected
        for(PricebookEntry pbe: ctx.pricebookEntryMap.values()){
            strUoM = String.isBlank(pbe.EP_Unit_Of_Measure__c)? pbe.Product2.EP_Unit_Of_Measure__c: pbe.EP_Unit_Of_Measure__c;
            strProductSetupKey = EP_PortalOrderUtil.constructProductQuantityLimitKey(pbe.Product2.id, strUoM);
            // Only include products that have range or allowable quantity setup // defect_anu
            if ((ctx.availableProductQuantitiesMap.containsKey(strProductSetupKey) || ctx.productRangeMap.containsKey(strProductSetupKey)) && !selectedProductMap.containsKey(pbe.id + strUoM)) {
                listProductOptions.add(EP_Common_Util.createOption(pbe.id,pbe.Product2.Name + EP_Common_Constant.LEFT_BRACKET + strUoM + EP_Common_Constant.RIGHT_BRACKET)); // 
            } // End setup check
        } //
        return listProductOptions;
    }
    public List<SelectOption> getListShipToTanks() {
        EP_GeneralUtility.Log('Public',CLASSNAME,'getListShipToTanks');
        List<SelectOption> shipToTanks = new list<SelectOption>();
        set<Id> selectedTankIds = new set<Id>();
        shipToTanks.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        if(ctx.listofOrderWrapper != null && !ctx.listofOrderWrapper.isEmpty()){
            for (EP_OrderPageContext_UX.OrderWrapper o: ctx.listofOrderWrapper) {
                selectedTankIds.add(o.oliTanksID);
            }
        }
        for (Id tankId :  ctx.mapShipToOperationalTanks.keySet()) {
            ctx.shipToOperationalTankIds.add(tankId);
        }
        ctx.shipToOperationalTankIds.removeall(selectedTankIds);
        for(Id tankId : ctx.shipToOperationalTankIds) {
            EP_Tank__c tankobj = ctx.mapShipToOperationalTanks.get(tankId);
            String tankCodeAlias = tankobj.EP_Tank_Code__c;               
            if(tankobj.EP_Tank_Alias__c != NULL){
                tankCodeAlias = tankCodeAlias + EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
            }
            shipToTanks.add(EP_Common_Util.createOption(tankId,tankCodeAlias)); // #novaSuiteFixes
            
        }
    
        return shipToTanks;
    }


    /*public void setOrderLineitemsForUpdate(EP_OrderPageContext_UX.OrderWrapper orderWrapper)
    {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderLineitemsForUpdate');
        
        

        ctx.intOrderLineIndex++;
        setOrderLineItemValues(orderWrapperObj.orderLineItem,orderWrapperObj);
        orderItemObj = orderWrapperObj.orderLineItem;
        orderItemObj.EP_Quantity_UOM__c = EP_Common_Constant.BLANK;
       
        setProductQuantity(orderWrapperObj.orderLineItem,orderWrapperObj);
        setOrderWrapperAndValidateOrderLines(orderWrapperObj.orderLineItem,newOrderWrapperobj,orderWrapperObj,listValidatedOrderLines);
        orderItemObj.Quantity = orderWrapperObj.oliQuantity;
        system.debug('===orderItemObj==='+orderItemObj);    
    }*/

    public void populatesupplierOptions(){
        if(!ctx.isRoOrder){
            return;
        }
        ctx.supplierOptions = new List < SelectOption > ();
        ctx.supplierOptions.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        if(ctx.strSelectedPickupLocationID == NULL) {
            return;
        }
        List<Account> supplierAccount = new EP_AccountMapper().getSuppliersOfSupplyLocation(ctx.strSelectedPickupLocationID );
        for(Account acc : supplierAccount) {
            ctx.supplierOptions.add(EP_Common_Util.createOption(acc.id, acc.name));
        }                     
        ctx.supplierContractMap = EP_PortalOrderUtil.fetchContracts(supplierAccount);
        system.debug('ctx.supplierContractMap ' + ctx.supplierContractMap);
    }


    public void setTankDetailsofOrderWrapper() {
        EP_GeneralUtility.Log('public',CLASSNAME,'setTankDetailsofOrderWrapper');
        EP_OrderPageContext_UX.OrderWrapper orderWrapperObj;
        for(EP_Tank__c tankobj : ctx.mapShipToOperationalTanks.values()){
            orderWrapperObj = new EP_OrderPageContext_UX.OrderWrapper();
            orderWrapperObj.oliPricebookEntryID =  ctx.PriceBookEntryproductMap.get(tankobj.EP_Product__C);   
            orderWrapperObj.orderLineItem.EP_Tank__c = orderWrapperObj.oliTanksID; // tank work
            orderWrapperObj.oliTankProductName = tankobj.EP_Product__r.Name;
            orderWrapperObj.oliTanksID = tankobj.id;
            //TODO check if it needs to be removed
            orderWrapperObj.oliProductName = tankobj.EP_Product__r.Name;

            if(tankobj.EP_Safe_Fill_Level__c != null){
                orderWrapperObj.oliTankSafeFillLvl = Integer.valueOf(tankobj.EP_Safe_Fill_Level__c);
            }
            orderWrapperObj.oliTanksName = tankobj.EP_Tank_Code__c;
            if(tankobj.EP_Tank_Alias__c != NULL){
                orderWrapperObj.oliTanksName =  orderWrapperObj.oliTanksName+EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
            }
            orderWrapperObj.oliProductUoM = ctx.productUnitOfMeasureMap.get(orderWrapperObj.oliPricebookEntryID);

            ctx.listofNewOrderWrapper.add(orderWrapperObj);
        }
    }

    public void refetchOrderLines(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'refetchOrderLines'); 
        ctx.orderWrapperMap = new map<Integer,EP_OrderPageContext_UX.OrderWrapper>();
        loadExisitingOrderItems();
    }

    @TestVisible
    private EP_OrderPageContext_UX.OrderWrapper populateOrderSummaryItem(String strProductName,Integer intQuantity,
    String strQuantityUoM,Double basePrice,
    Double unitPrice,Double dblCost,
    String strCurrencyCode,
    Boolean isAdditionalItem,String description,
    Double taxPercentage,Double taxAmount) {
        EP_GeneralUtility.Log('Private',CLASSNAME,'populateOrderSummaryItem');
        EP_OrderPageContext_UX.OrderWrapper orderItem = new EP_OrderPageContext_UX.OrderWrapper();
        orderItem.oliProductName = strProductName;
        
        if (!isAdditionalItem) {
            orderItem.oliQuantity = intQuantity;
            orderItem.oliQuantityStr = String.valueOf(intQuantity);
        }
        orderItem.oliBasePrice = basePrice;
        orderItem.oliUnitPrice = unitPrice;
        orderItem.oliCost = dblCost;
        orderItem.oliCurrencyCode = strCurrencyCode;
        orderItem.oliProductUoM = strQuantityUoM;
        orderItem.oliIsAdditionalItem = isAdditionalItem;
        orderItem.oliDescription= description;
        orderItem.oliTaxPercent = taxPercentage;
        orderItem.oliTaxAmount = taxAmount;
        return orderItem;
    }

    public void validateQuantity(String index,String ordLinesType){
        EP_GeneralUtility.Log('Public',CLASSNAME,'validateQuantity');
        EP_OrderPageContext_UX.OrderWrapper oWrapper;
        Integer idx = Integer.valueOf(index);

        if(ordLinesType.equalsIgnoreCase('bulk')){
            oWrapper = ctx.listofNewOrderWrapper[idx];
        }

        if(ordLinesType.equalsIgnoreCase('packaged')){
            oWrapper = ctx.orderWrapperMap.get(idx);
        }

        system.debug('order wrapper is @@ ' + oWrapper);
        if(String.isNotBlank(oWrapper.oliQuantityStr)){
            if( oWrapper.oliQuantity != Integer.valueOf(oWrapper.oliQuantityStr) && !(ctx.isConsignment || ctx.isTransfer)){
                ctx.isPriceCalculated = false;
                ctx.showSubmitButton = false;
                ctx.showCalculatePriceButton =true;
            }
            oWrapper.oliQuantity = Integer.valueOf(oWrapper.oliQuantityStr);
        }
        validateEnteredQuantity(oWrapper);

        if(ctx.hasinvalidorderlines){
            return;
        }
        validateInventory(oWrapper);

    }

    public void prepareInventoryMap(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'prepareInventoryMap');
        if(ctx.setProductIDs.isEmpty() || ctx.newOrderRecord.EP_Stock_Holding_Location__c == null){
            return;
        }

        ctx.productInventoryMap = new map<id,String>();
        set<id> prdidset = new set<id>();
        for(String idval : ctx.setProductIDs){
            prdidset.add(Id.valueOf(idval));    
        }
        
        List<EP_Inventory__c> inventoryList = new EP_InventoryMapper().getInventoryByProdId(prdidset, ctx.newOrderRecord.EP_Stock_Holding_Location__c);
        // Get the Inventory records and looping through
        if(!inventoryList.isEmpty()){
            for(EP_Inventory__c inv : inventoryList){
                if(inv.EP_Inventory_Availability__c == EP_Common_Constant.LOW_INV){
                    ctx.productInventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                } 
                else if ( inv.EP_Inventory_Availability__c == EP_Common_Constant.NO_INV){
                    ctx.productInventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                }
                else if(inv.EP_Inventory_Availability__c == EP_Common_Constant.GOOD_INV){
                    ctx.productInventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                }
                
            }
        }      
    }
    public void updateUOMinOrderLines(String index){
        EP_GeneralUtility.Log('Public',CLASSNAME,'updateUOMinOrderLines');
        Integer indexVal = Integer.valueOf(index);
        EP_OrderPageContext_UX.OrderWrapper orderLine = ctx.orderWrapperMap.get(indexVal);
        orderLine.oliProductUoM='';
        if(ctx.productUnitOfMeasureMap.containsKey(orderLine.oliPricebookEntryID))
            orderLine.oliProductUoM = ctx.productUnitOfMeasureMap.get(orderLine.oliPricebookEntryID);
    }
}