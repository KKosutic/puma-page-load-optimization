/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_CreditHoldingExtensionHelper>                     *
*  @CreateDate <3/11/2017>                                     *
*  @Description <Helpert class for implementation>             *
*  @Version <1.0>                                              *
****************************************************************/
public with sharing class EP_CreditHoldingExtensionHelper {
	private EP_CreditHoldingExtensionContext ctx;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_CreditHoldingExtensionHelper                 *
* @description  constructor of class                            *
* @param        EP_CreditHoldingExtensionContext                *
* @return       NA                                              *
****************************************************************/
	public EP_CreditHoldingExtensionHelper(EP_CreditHoldingExtensionContext ctx) {
		this.ctx = ctx;
		associatedCustomer();
	}

/****************************************************************
* @author       Accenture                                       *
* @name         searchCreditCustomer                            *
* @description  method to get all credit customer               *
* @param        NA 					                            *
* @return       pageReference                                   *
****************************************************************/
	public pageReference searchCreditCustomer(){
		EP_GeneralUtility.Log('public','EP_CreditHoldingExtensionHelper','searchCreditCustomer');
		ctx.searchInputBlank = false;
		ctx.searchCustomerWrapper = new List<EP_CreditHoldingExtensionContext.creditCustomerWrapper>();
		ctx.custSearch = false;
		if(String.isBlank(ctx.searchName)){
			ctx.searchInputBlank = true;
			ctx.searchInputBlankError = 'Name or Number is required to search Credit Customers';
			return null;
		}
		EP_CreditHoldingExtensionContext.creditCustomerWrapper wrapObj;
		ctx.searchResult = EP_AccountMapper.getSellToAccount(listAccFields(),accWhereFilter(ctx.searchCriteria));
		for(Account acc : ctx.searchResult){
			wrapObj = new EP_CreditHoldingExtensionContext.creditCustomerWrapper(acc,false);
			ctx.searchCustomerWrapper.add(wrapObj);
			ctx.custSearch = true;
		}
		system.debug('Search Result = '+ctx.searchCustomerWrapper);
		return null;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         listAccFields                                   *
* @description  method to create list of fields to query        *
* @param        NA 					                            *
* @return       List string                                     *
****************************************************************/
	private List<string> listAccFields(){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','listAccFields');
		List<string> accFields = new List<string>();
		accFields.add('id');
		accFields.add('name');
		return accFields;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         accWhereFilter                                  *
* @description  method to create filter for account query       *
* @param        NA 					                            *
* @return       string                                          *
****************************************************************/
	private string accWhereFilter(String searchCriteria){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','accWhereFilter');
		return('where recordType.developerName = '+ '\'' + 'EP_Sell_To'+ '\'' + ' and EP_Puma_Company__c = ' 
									+ '\'' +ctx.holdingObj.EP_company__c + '\''+ 
									' and EP_Package_Payment_Term__r.Name != ' + '\''+ 'Cash on Delivery' + '\'' + 
						            ' and EP_Package_Payment_Term__r.Name != ' + '\''+ 'Prepayment' + '\''+ ' and ' 
						            + searchCriteria + '= ' + '\''+ ctx.searchName + '\'' + 
						            ' and EP_Holding__c!= '+ '\'' + ctx.holdingObj.id + '\'');
	} 

/****************************************************************
* @author       Accenture                                       *
* @name         associatedCustomerWhereFilter                   *
* @description  method to create filter for account query       *
* @param        NA 					                            *
* @return       string                                          *
****************************************************************/
	private string associatedCustomerWhereFilter(){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','accWhereFilter');
		return('where EP_Holding__c= '+ '\'' + ctx.holdingObj.id + '\'');
	} 

/****************************************************************
* @author       Accenture                                       *
* @name         associatedCustomerWhereFilter                   *
* @description  method to create filter for account query       *
* @param        NA 					                            *
* @return       string                                          *
****************************************************************/
	private void associatedCustomer(){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','associatedCustomer');
		EP_CreditHoldingExtensionContext.creditCustomerWrapper wrapObj;
		for(Account acc : EP_AccountMapper.getSellToAccount(listAccFields(),associatedCustomerWhereFilter())){
			wrapObj = new EP_CreditHoldingExtensionContext.creditCustomerWrapper(acc,true);
			ctx.associatedCustomer.add(wrapObj);
		}
	}

/****************************************************************
* @author       Accenture                                       *
* @name         save                                            *
* @description  method to do DML on account and holding object  *
* @param        NA 					                            *
* @return       pagereference                                   *
****************************************************************/
	public pageReference save(){
		EP_GeneralUtility.Log('public','EP_CreditHoldingExtensionHelper','save');
		pagereference page = new pagereference(ctx.returnURL);
		Account acc;
		List<Account> accList = new List<Account>();
		for(EP_CreditHoldingExtensionContext.creditCustomerWrapper creditCust : ctx.searchCustomerWrapper){
			system.debug('creditCust.customerSelected = '+creditCust.customerSelected);
			if(creditCust.customerSelected){
				accList.add(associateCustHolding(creditCust.customerAcc));
			}
		}
		accList.addAll(dissociateCreditCustomers());
		system.debug('accList = '+accList);
		Database.update(accList,true);
		rollupCreditLimit();
		Database.update(ctx.holdingObj,true);
		return page;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         cancel                                          *
* @description  method to return to holding record              *
* @param        NA 					                            *
* @return       pagereference                                   *
****************************************************************/
	private void rollupCreditLimit(){
        EP_CreditHoldingDomainObject holdingDomainObj = new EP_CreditHoldingDomainObject(ctx.holdingObj);
        holdingDomainObj.rollupCreditLimit();
        system.debug(holdingDomainObj.rollupCreditLimit());
    }

/****************************************************************
* @author       Accenture                                       *
* @name         cancel                                          *
* @description  method to return to holding record              *
* @param        NA 					                            *
* @return       pagereference                                   *
****************************************************************/
	public pageReference cancel(){
		EP_GeneralUtility.Log('public','EP_CreditHoldingExtensionHelper','cancel');
		pagereference page = new pagereference(ctx.returnURL);
		return page;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         dissociateCreditCustomers                       *
* @description  method to dissociate account with holding       *
* @param        NA          		                            *
* @return       List account                                    *
****************************************************************/
	private List<account> dissociateCreditCustomers(){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','dissociateCreditCustomers');
		List<Account> acList = new List<Account>();
		for(EP_CreditHoldingExtensionContext.creditCustomerWrapper creditCust : ctx.associatedCustomer){
			system.debug('creditCust.customerSelected = '+creditCust.customerSelected);
			if(!creditCust.customerSelected){
				acList.add(dissociateCustHolding(creditCust.customerAcc));
			}
		}
		return acList;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         dissociateCustHolding                           *
* @description  method to dissociate account with holding       *
* @param        Account     		                            *
* @return       account                                         *
****************************************************************/
	private account dissociateCustHolding(Account acc){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','dissociateCustHolding');
		Account accObj = new Account(id = acc.id,EP_Holding__c = null);
		return accObj;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         associateCustHolding                            *
* @description  method to associate account with holding        *
* @param        Account     		                            *
* @return       account                                         *
****************************************************************/
	private account associateCustHolding(Account acc){
		EP_GeneralUtility.Log('private','EP_CreditHoldingExtensionHelper','associateCustHolding');
		Account accObj = new Account(id = acc.id,EP_Holding__c = ctx.holdingObj.id);
		return accObj;
	}
}