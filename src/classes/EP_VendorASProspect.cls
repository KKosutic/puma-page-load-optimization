/*
 *  @Author <Accenture>
 *  @Name <EP_VendorASProspect>
 *  @CreateDate <>
 *  @Description <Vendor Account State for 01-Prospect Status>
 *  @Version <1.0>
 */
 public with sharing class EP_VendorASProspect extends EP_AccountState{
     
    public EP_VendorASProspect() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VendorASProspect','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VendorASProspect','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VendorASProspect','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VendorASProspect','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VendorASProspect','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}