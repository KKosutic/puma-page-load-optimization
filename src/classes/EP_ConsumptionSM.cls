/*   
     @Author Aravindhan Ramalingam
     @name <EP_ConsumptionSM.cls>     
     @Description <Consumption Statemachine >   
     @Version <1.1> 
*/

public class EP_ConsumptionSM extends EP_OrderStateMachine {
    
    public EP_ConsumptionSM(){

    }
    
public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
		EP_GeneralUtility.Log('Public','EP_ConsumptionSM','getOrderState');
        return super.getOrderState(currentEvent);
    }
}