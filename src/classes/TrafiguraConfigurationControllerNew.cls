public with sharing class TrafiguraConfigurationControllerNew {
	private static final String CONFIG_ID_PARAM = 'configId';
	public String configId {get; set;}
	public String productConfiguration;
	public cscfga__Product_Definition__c editingCfgDefinitionSObject { get; private set; }
	public cscfga__Product_Configuration__c editingConfigSObject { get; private set; }
	
	public Object getProductConfigurationString() {
		return cscfga.UISupport.getProductConfiguration(configId);
	}
	
	public Object getProductModelString() {
		return cscfga.UISupport.getProductModel(editingCfgDefinitionSObject.Id);
	}
	
	public String getProductTemplateString() {
		return cscfga.UISupport.getProductTemplate(editingCfgDefinitionSObject.Id, '', 'CustomOnlineTemplate').escapeEcmaScript();
	}
	
	@RemoteAction
	public static Map<String, Object> getAllConfiguratorDetails(String configId, String definitionId) {
		Map<String, Object> retMap = new Map<String, Object>();
		retMap.put('config', cscfga.UISupport.getProductConfiguration(configId));
		retMap.put('model', cscfga.UISupport.getProductModel(definitionId));
		retMap.put('template', cscfga.UISupport.getProductTemplate(definitionId, '', 'CustomOnlineTemplate').escapeEcmaScript());
		return retMap;
	}
	
	public TrafiguraConfigurationControllerNew(ApexPages.StandardController standardController) {
		Map<String, String> currentPageParameters = ApexPages.currentPage().getParameters();
		configId = currentPageParameters.get(CONFIG_ID_PARAM);
		this.editingConfigSObject = (cscfga__Product_Configuration__c) SObjectUtils.loadSObject(configId);
		this.editingCfgDefinitionSObject = (cscfga__Product_Definition__c) SObjectUtils.loadSObject(this.editingConfigSObject.cscfga__product_definition__c);
	}
	
	public TrafiguraConfigurationControllerNew() {
		Map<String, String> currentPageParameters = ApexPages.currentPage().getParameters();
		configId = currentPageParameters.get(CONFIG_ID_PARAM);
		this.editingConfigSObject = (cscfga__Product_Configuration__c) SObjectUtils.loadSObject(configId);
		this.editingCfgDefinitionSObject = (cscfga__Product_Definition__c) SObjectUtils.loadSObject(this.editingConfigSObject.cscfga__product_definition__c);
	}
	
	public TrafiguraConfigurationControllerNew(cscfga.UISupport uiController) {
		Map<String, String> currentPageParameters = ApexPages.currentPage().getParameters();
		configId = currentPageParameters.get(CONFIG_ID_PARAM);
		this.editingConfigSObject = (cscfga__Product_Configuration__c) SObjectUtils.loadSObject(configId);
		this.editingCfgDefinitionSObject = (cscfga__Product_Definition__c) SObjectUtils.loadSObject(this.editingConfigSObject.cscfga__product_definition__c);
	}
}