/**
 * @author <Kamal Garg>
 * @name <EP_IntegrationAcknowledgementHandlerTest>
 * @createDate <27/11/2015>
 * @description <Test class for EP_IntegrationAcknowledgementHandler> 
 * @version <1.0>
 */
@isTest
private class EP_IntegrationAcknowledgementHandlerTest {
    
    private static final String NAME = 'System Administrator';
    private static String TESTREC ='Test Record';
    private static String TEST_TARGET ='Test Target';
    /**
     * @author Kamal Garg
     * @date 27/11/2015
     * @description Test Method to cover parse API defined in EP_IntegrationAcknowledgementHandler apex class.
     */
    static testMethod void testParse() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            String jsonString =        '{'+
	        '    "Acknowledgment": ['+
	        '    {'+
	        '        "HeaderCommon": {'+
	        '            "SourceApplication": "",'+
	        '            "DestinationApplication": "",'+
	        '            "SourceCompany": "",'+
	        '            "DestinationCompany": "",'+
	        '            "CorrelationID": "",'+
	        '            "SourceUpdateStatusAddress": "",'+
	        '            "DestinationUpdateStatusAddress": "",'+
	        '            "MiddlewareUrlForPush": "",'+
	        '            "EmailNotification": "",'+
	        '            "TransportStatus": "",'+
	        '            "ProcessStatus": "",'+
	        '            "CommunicationType": "",'+
	        '            "MethodForReceiveAckn": "",'+
	        '            "FormatofAckn": ""'+
	        '        },'+
	        '        "Response": {'+
	        '            "Result": ['+
	        '                {'+
	        '                    "Type": "Customer",'+
	        '                    "Seq": "1",'+
	        '                    "MsgID": "1111",'+
	        '                    "ObjectID": "",'+
	        '                    "TransactionId": "",'+
	        '                    "ProcessedStatus": "",'+
	        '                    "ProcessedDateTime": "",'+
	        '                    "ErrorDetail": {'+
	        '                        "ErrorCode": "",'+
	        '                        "ErrorDescription": ""'+
	        '                    }'+
	        '                }'+
	        '            ]'+
	        '        },'+
	        '        "StatusResponse": ""'+
	        '    }'+
	        '    ]'+
	        '}';
	        EP_IntegrationAcknowledgementHandler obj = EP_IntegrationAcknowledgementHandler.parse(jsonString);
	        System.assertNotEquals(obj, null);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 30/11/2015
     * @description Test Method to cover handleAcknowledgement API defined in EP_IntegrationAcknowledgementHandler apex class.
     * This test method covers scenario for a single EP_IntegrationRecord__c record with status 'Success'.
     */
    static testMethod void testhandleAcknowledgementSuccess() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            // Create test Account record
            List<Account> acctsToBeInserted = new List<Account>();
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            database.insert(acctsToBeInserted);
            Account accObj = acctsToBeInserted.get(0);
            // Create test IntegrationRecord record
            List<EP_IntegrationRecord__c> intRecToBeInserted = new List<EP_IntegrationRecord__c>();
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts','', EP_IntegrationUtil.getMessageId(TESTREC), TEST_TARGET, DateTime.now()));
            database.insert(intRecToBeInserted); 
            
            EP_IntegrationRecord__c intRec = [SELECT Id, EP_Message_ID__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_Target__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c=:accObj.Id LIMIT: EP_Common_Constant.ONE];
            
            String jsonString =        '{'+
            '    "Acknowledgment": ['+
            '    {'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec.EP_Object_ID__c+'",'+
            '                    "TransactionId": "",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.SUCCESS+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    }'+
            '    ]'+
            '}';
            
            EP_IntegrationAcknowledgementHandler.handleAcknowledgement(jsonString);
            List<EP_IntegrationRecord__c> ackRecList = [SELECT Id, EP_Error_Description__c, EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_Type__c=:EP_Common_Constant.ACKNOWLEDGEMENT LIMIT: EP_Common_Constant.ONE];
            system.assertNotEquals(ackRecList.size(), 0);
            system.assert(String.isBlank(ackRecList.get(0).EP_Error_Description__c));
            system.assertEquals(ackRecList.get(0).EP_Status__c, EP_Common_Constant.RECEIVED_STATUS.toUpperCase());
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 01/12/2015
     * @description Test Method to cover scenario when multiple EP_IntegrationRecord__c records sent to same target system.
     */
    static testMethod void testhandleAcknowledgementSuccess1() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            // Create test Account record
            List<Account> acctsToBeInserted = new List<Account>();
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            database.insert(acctsToBeInserted);
            Account accObj1 = acctsToBeInserted.get(0);
            Account accObj2 = acctsToBeInserted.get(1);
            // Create test IntegrationRecord record
            List<EP_IntegrationRecord__c> intRecToBeInserted = new List<EP_IntegrationRecord__c>();
            Datetime dtSent = DateTime.now();
            String msgId = EP_IntegrationUtil.getMessageId(TESTREC);
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj1.Id,'Accounts','', msgId, TEST_TARGET, dtSent));
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj2.Id,'Accounts','', msgId, TEST_TARGET, dtSent));
            database.insert(intRecToBeInserted); 
            
            List<EP_IntegrationRecord__c> intRecList = [SELECT Id, EP_Message_ID__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_Target__c FROM EP_IntegrationRecord__c WHERE EP_Message_ID__c=:msgId LIMIT:2];
            EP_IntegrationRecord__c intRec1 = intRecList.get(0);
            EP_IntegrationRecord__c intRec2 = intRecList.get(1);
            
            String jsonString =        '{'+
            '    "Acknowledgment": ['+
            '    {'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec1.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec1.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec1.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec1.EP_Object_ID__c+'",'+
            '                    "TransactionId": "",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.SUCCESS+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                },'+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "2",'+
            '                    "MsgID": "'+intRec2.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec2.EP_Object_ID__c+'",'+
            '                    "TransactionId": "",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.SUCCESS+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    }'+
            '    ]'+
            '}';
            
            EP_IntegrationAcknowledgementHandler.handleAcknowledgement(jsonString);
            List<EP_IntegrationRecord__c> ackRecList = [SELECT Id, EP_Error_Description__c, EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_Type__c=:EP_Common_Constant.ACKNOWLEDGEMENT LIMIT: EP_Common_Constant.ONE];
            system.assertNotEquals(ackRecList.size(), 0);
            system.assert(String.isBlank(ackRecList.get(0).EP_Error_Description__c));
            system.assertEquals(ackRecList.get(0).EP_Status__c, EP_Common_Constant.RECEIVED_STATUS.toUpperCase());
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 01/12/2015
     * @description Test Method to cover scenario when same EP_IntegrationRecord__c sent to different target systems.
     */
    static testMethod void testhandleAcknowledgementSuccess2() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            // Create test Account record
            List<Account> acctsToBeInserted = new List<Account>();
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            database.insert(acctsToBeInserted);
            Account accObj = acctsToBeInserted.get(0);
            // Create test IntegrationRecord record
            String txnId = EP_IntegrationUtil.getTransactionID(TESTREC, TESTREC);
            List<EP_IntegrationRecord__c> intRecToBeInserted = new List<EP_IntegrationRecord__c>();
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts', txnId, EP_IntegrationUtil.getMessageId(TESTREC+1), TEST_TARGET+1, DateTime.now()));
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts', txnId, EP_IntegrationUtil.getMessageId(TESTREC+1), TEST_TARGET+1, DateTime.now()));
            database.insert(intRecToBeInserted); 
            
            List<EP_IntegrationRecord__c> intRecList = [SELECT Id, EP_Message_ID__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_Target__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c=:accObj.Id LIMIT: EP_Common_Constant.ONE];
            EP_IntegrationRecord__c intRec1 = intRecList.get(0);
            EP_IntegrationRecord__c intRec2 = intRecList.get(0);
            
            String jsonString =        '{'+
            '    "Acknowledgment": ['+
            '        {'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec1.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec1.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec1.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec1.EP_Object_ID__c+'",'+
            '                    "TransactionId": "'+txnId+'",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.SUCCESS+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    },'+
            '        {'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec2.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec2.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec2.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec2.EP_Object_ID__c+'",'+
            '                    "TransactionId": "'+txnId+'",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.SUCCESS+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    }'+
            ']'+
            '}';
            
            EP_IntegrationAcknowledgementHandler.handleAcknowledgement(jsonString);
            List<EP_IntegrationRecord__c> ackRecList = [SELECT Id, EP_Error_Description__c, EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_Type__c=:EP_Common_Constant.ACKNOWLEDGEMENT LIMIT: EP_Common_Constant.ONE];
            system.assertNotEquals(ackRecList.size(), 0);
            system.assert(String.isBlank(ackRecList.get(0).EP_Error_Description__c));
            system.assertEquals(ackRecList.get(0).EP_Status__c, EP_Common_Constant.RECEIVED_STATUS.toUpperCase());
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 01/12/2015
     * @description Test Method to cover handleAcknowledgement API defined in EP_IntegrationAcknowledgementHandler apex class.
     * This test method covers scenario for a single EP_IntegrationRecord__c record with status 'Failure'.
     */
    static testMethod void testhandleAcknowledgementFailure() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            // Create test Account record
            List<Account> acctsToBeInserted = new List<Account>();
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            database.insert(acctsToBeInserted);
            Account accObj = acctsToBeInserted.get(0);
            // Create test IntegrationRecord record
            List<EP_IntegrationRecord__c> intRecToBeInserted = new List<EP_IntegrationRecord__c>();
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts','', EP_IntegrationUtil.getMessageId(TESTREC), TEST_TARGET, DateTime.now()));
            database.insert(intRecToBeInserted); 
            
            EP_IntegrationRecord__c intRec = [SELECT Id, EP_Message_ID__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_Target__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c=:accObj.Id LIMIT: EP_Common_Constant.ONE];
            
            String jsonString =        '{'+
            '    "Acknowledgment": ['+
            '    {'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec.EP_Object_ID__c+'",'+
            '                    "TransactionId": "",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.FAILURE+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    }'+
            ']'+
            '}';
            
            EP_IntegrationAcknowledgementHandler.handleAcknowledgement(jsonString);
            List<EP_IntegrationRecord__c> ackRecList = [SELECT Id, EP_Error_Description__c, EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_Type__c=:EP_Common_Constant.ACKNOWLEDGEMENT LIMIT: EP_Common_Constant.ONE];
            system.assertNotEquals(ackRecList.size(), 0);
            system.assert(String.isBlank(ackRecList.get(0).EP_Error_Description__c));
            system.assertEquals(ackRecList.get(0).EP_Status__c, EP_Common_Constant.RECEIVED_STATUS.toUpperCase());
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 01/12/2015
     * @description Test Method to cover scenario for a single EP_IntegrationRecord__c which is not parsed by handleAcknowledgement API defined in EP_IntegrationAcknowledgementHandler apex class.
     */
    static testMethod void testhandleAcknowledgementFailure1() {
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name=:NAME LIMIT: EP_Common_Constant.ONE];
        User u = EP_TestDataUtility.createUser(sysAdmin.Id);
        System.runAs(u) {
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            // Create test Account record
            List<Account> acctsToBeInserted = new List<Account>();
            acctsToBeInserted.add(EP_TestDataUtility.createBillToAccount());
            database.insert(acctsToBeInserted);
            Account accObj = acctsToBeInserted.get(0);
            // Create test IntegrationRecord record
            List<EP_IntegrationRecord__c> intRecToBeInserted = new List<EP_IntegrationRecord__c>();
            intRecToBeInserted.add(EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts','', EP_IntegrationUtil.getMessageId(TESTREC), TEST_TARGET, DateTime.now()));
            database.insert(intRecToBeInserted); 
            
            EP_IntegrationRecord__c intRec = [SELECT Id, EP_Message_ID__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_Target__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c=:accObj.Id LIMIT: EP_Common_Constant.ONE];
            
            String jsonString =        '{'+
            '    "Acknowledgment": {{'+
            '        "HeaderCommon": {'+
            '            "SourceApplication": "'+intRec.EP_Target__c+'",'+
            '            "DestinationApplication": "",'+
            '            "SourceCompany": "",'+
            '            "DestinationCompany": "",'+
            '            "CorrelationID": "'+intRec.EP_Message_ID__c+'",'+
            '            "SourceUpdateStatusAddress": "",'+
            '            "DestinationUpdateStatusAddress": "",'+
            '            "MiddlewareUrlForPush": "",'+
            '            "EmailNotification": "",'+
            '            "TransportStatus": "",'+
            '            "ProcessStatus": "",'+
            '            "CommunicationType": "",'+
            '            "MethodForReceiveAckn": "",'+
            '            "FormatofAckn": ""'+
            '        },'+
            '        "Response": {'+
            '            "Result": ['+
            '                {'+
            '                    "Type": "Account",'+
            '                    "Seq": "1",'+
            '                    "MsgID": "'+intRec.EP_Message_ID__c+'",'+
            '                    "ObjectID": "'+intRec.EP_Object_ID__c+'",'+
            '                    "TransactionId": "",'+
            '                    "ProcessedStatus": "'+EP_Common_Constant.FAILURE+'",'+
            '                    "ProcessedDateTime": "",'+
            '                    "ErrorDetail": {'+
            '                        "ErrorCode": "",'+
            '                        "ErrorDescription": ""'+
            '                    }'+
            '                }'+
            '            ]'+
            '        },'+
            '        "StatusResponse": ""'+
            '    }'+
            '}';
            
            EP_IntegrationAcknowledgementHandler.handleAcknowledgement(jsonString);
            List<EP_IntegrationRecord__c> ackRecList = [SELECT Id, EP_Error_Description__c, EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_Type__c=:EP_Common_Constant.ACKNOWLEDGEMENT LIMIT: EP_Common_Constant.ONE];
            system.assertNotEquals(ackRecList.size(), 0);
            system.assert(String.isNotBlank(ackRecList.get(0).EP_Error_Description__c));
            system.assertEquals(ackRecList.get(0).EP_Status__c, EP_Common_Constant.ERROR_RECEIVED_STATUS.toUpperCase());
        }
    }
    
}