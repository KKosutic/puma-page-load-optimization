@isTest
public class EP_OSTANYToAwaitingInventoryReview_UT
{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
static testMethod void isTransitionPossible_positive_test() {
   // EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderDomainObject obj = getOrderDomainObj();
    EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.UserExceptionApproved);
    EP_OSTANYToAwaitingInventoryReview ost = new EP_OSTANYToAwaitingInventoryReview();
    csord__Order__c ord = obj.getOrder();
    ord.EP_Order_Epoch__c = EP_OrderConstant.Order_Epoch_Current;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isTransitionPossible_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObjectPositiveScenario();
    EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
    EP_OSTANYToAwaitingInventoryReview ost = new EP_OSTANYToAwaitingInventoryReview();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(false,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
   // EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderDomainObject obj = getOrderDomainObj();
    EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.UserExceptionApproved);
    EP_OSTANYToAwaitingInventoryReview ost = new EP_OSTANYToAwaitingInventoryReview();
    csord__Order__c ord = obj.getOrder();
    ord.EP_Order_Epoch__c = EP_OrderConstant.Order_Epoch_Current;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isRegisteredForEvent();
    Test.stopTest();
    System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_positive_test() {
   // EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderDomainObject obj = getOrderDomainObj();
    EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
    EP_OSTANYToAwaitingInventoryReview ost = new EP_OSTANYToAwaitingInventoryReview();
   
        //return odo;
    //Order ord = obj.getOrder();
   // ord.EP_Order_Epoch__c = EP_OrderConstant.Order_Epoch_Current;
    ost.setOrderContext(obj,oe);
    csord__Order__c ord = obj.getOrder();
    System.debug('Test##'+obj.isInventoryLow());
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isGuardCondition_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObjectPositiveScenario();
    EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
    EP_OSTANYToAwaitingInventoryReview ost = new EP_OSTANYToAwaitingInventoryReview();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(false,result);
}

static EP_OrderDomainObject getOrderDomainObj(){
 EP_OrderDataWrapper odw = new EP_OrderDataWrapper();
        odw.orderType = EP_Common_Constant.SALES_ORDER; 
        odw.vmiType = EP_Common_Constant.NONVMI;       
        odw.deliveryType = EP_Common_Constant.DELIVERY;
        odw.productSoldAsType = EP_Common_Constant.BULK_ORDER_PRODUCT_CAT;
        odw.epochType = EP_Common_Constant.EPOC_CURRENT;  
        odw.consignmentType = EP_Common_Constant.NON_CONSIGNMENT;               
        odw.status = EP_OrderConstant.OrderState_Awaiting_Inventory_Review;
        odw.scenarioType = EP_Common_Constant.NEGATIVE;

        
        csord__Order__c orderObj = EP_OrderTestDataUtility_Temp.getOrder(odw);
        EP_OrderDomainObject obj = new EP_OrderDomainObject(orderObj);
        //assert not needed
        system.assert(true);
        return obj;
}
}