/* 
   @Author 			Accenture
   @name 			EP_TankCreationPayloadCtrlExtn
   @CreateDate 		02/08/2017
   @Description		Controller Extension to generate XML for newly created Tanks
   @Version 		1.0
*/
public with sharing class EP_TankCreationPayloadCtrlExtn { 
    private string messageId;
    private string messageType;
    public String shipToSeqId {get;set;}  
    public String tankSeqId {get;set;}  
    private string secretCode;
    public EP_Tank__c TankObject {get;set;}
    
    /**
	* @author 			Accenture
	* @name				shipToStatus
	* @date 			02/08/2017
	* @description 		Sets the shipToStatus based on the status of account
	* @param 			NA
	* @return 			string
	*/
    public string shipToStatus {
    get {
    	//Defect fix for #57771 start
      //To set the value of shipToStatus for WINDMS
      string shipToStatusValue = ((EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(String.valueOf(this.TankObject.EP_Ship_To__r.EP_Status__c))
                 || EP_Common_Constant.STATUS_INACTIVE.equalsIgnoreCase(String.valueOf(this.TankObject.EP_Ship_To__r.EP_Status__c))
                ) ? EP_Common_Constant.STRING_NO_LS :  EP_Common_Constant.STRING_YES_LS);
      return shipToStatusValue ; } set;
      //Defect fix for #57771 end 
    }
    
    /**
	* @author 			Accenture
	* @name				EP_TankCreationPayloadCtrlExtn
	* @date 			02/08/2017
	* @description 		The extension constructor initializes the members variable AccountObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
	* @param 			ApexPages.StandardController
	* @return 			NA
	*/
    public EP_TankCreationPayloadCtrlExtn  (ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{EP_Common_Constant.SHIP_TO_STATUS,EP_Common_Constant.TANKSHIPTOLASTMOIDIFIEDBYALIAS}); 
        this.TankObject = (EP_Tank__c) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        shipToSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.TankObject.EP_Ship_To__r.Id);
        tankSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.TankObject.Id);
    }
    
    /**
	* @author 			Accenture
	* @name				checkPageAccess
	* @date 			02/08/2017
	* @description 		This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
	* @param 			NA
	* @return 			PageReference
	*/
	public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_TankCreationPayloadCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}