/* 
  @Author <Ashok Arora>
  @name <EP_MockDispatcher>
  @CreateDate <21/10/2015>
  @Description <This is class is used for generating Mock Responses for Web Services>
  @Version <1.0>
 
*/
@isTest
global class EP_MockDispatcher {
    /*public void doInvoke(Object stub
                       ,Object request
                       ,Map<String, Object> response
                       ,String endpoint
                       ,String soapAction
                       ,String requestName
                       ,String responseNS
                       ,String responseName
                       ,String responseType){
        //CHECK IF CALLOUT FOR NAV CUSTOMER SERVICE
        if(stub instanceof EP_Trafigura_WS.BasicHttpBinding_ITwoWayAsync){
            new EP_NavCustmrWebServiceMock().doInvoke( stub
                                                       ,request
                                                       ,response
                                                       ,endpoint
                                                       ,soapAction
                                                       ,requestName
                                                       ,responseNS
                                                       ,responseName
                                                       ,responseType);
        }
        //CHECK IF CALLOUT FOR LOMOSOFT CUSTOMER IMPORT SERVICE
        else if(stub instanceof EP_Trafigura_Lomo_WS.BasicHttpBinding_ITwoWayAsync){
            new EP_LomoCstmrWebServiceMock().doInvoke( stub
                                                       ,request
                                                       ,response
                                                       ,endpoint
                                                       ,soapAction
                                                       ,requestName
                                                       ,responseNS
                                                       ,responseName
                                                       ,responseType);
        }
        else if(stub instanceof  EP_TrafiguraCstmrNAVEditWS.BasicHttpBinding_ITwoWayAsync){
            new EP_NAVCstmrEditWebServiceMock().doInvoke(stub
                                                        ,request
                                                        ,response
                                                        ,endpoint
                                                        ,soapAction
                                                        ,requestName
                                                        ,responseNS
                                                        ,responseName
                                                        ,responseType);
        }
        //CHECK IF CALLOUT FOR ORDER CUSTOMER SERVICE
        if(stub instanceof EP_TrafiguraOrderCreditCheckWS.BasicHttpBinding_ITwoWayAsync){
            new EP_OrderWebServiceMock().doInvoke( stub
                                                       ,request
                                                       ,response
                                                       ,endpoint
                                                       ,soapAction
                                                       ,requestName
                                                       ,responseNS
                                                       ,responseName
                                                       ,responseType);
        }
        
   }*/

  /**
    This class generates mock response for SFDC TO NAV (Customer Import) WebService
  **/ 
/*   public class EP_NavCustmrWebServiceMock{
        public void doInvoke(Object stub
                            ,Object request
                            ,Map<String, Object> response
                            ,String endpoint
                            ,String soapAction
                            ,String requestName
                            ,String responseNS
                            ,String responseName
                            ,String responseType){
       
            EP_SFDC_Customer_NAV_Intg_WebService.MSG_element msgElement = new EP_SFDC_Customer_NAV_Intg_WebService.MSG_element();
            msgElement.StatusPayload = 'Success_Mock_NAV';
            response.put('response_x', msgElement);
        
        }
   }*/ 
   /**
    This class generates mock response for SFDC TO NAV(Customer Edit) WebService
  **/
   /*public class EP_NAVCstmrEditWebServiceMock implements WebServiceMock{
    public void doInvoke(Object stub
                         ,Object request
                         ,Map<String, Object> response
                         ,String endpoint
                         ,String soapAction
                         ,String requestName
                         ,String responseNS
                         ,String responseName
                         ,String responseType){
       
            EP_CstmrNAVEditWebService.MSG_element MsgElement = new EP_CstmrNAVEditWebService.MSG_element();
            msgElement.StatusPayload = 'Success_Mock_Nav_Cstmr_Edit';
            response.put('response_x', msgElement);
            
       }
   }*/
   
   /**
    This class generates mock response for SFDC TO Lomosoft(Customer Import) WebService
  **/
   /*public class EP_LomoCstmrWebServiceMock implements WebServiceMock{
    public void doInvoke(Object stub
                         ,Object request
                         ,Map<String, Object> response
                         ,String endpoint
                         ,String soapAction
                         ,String requestName
                         ,String responseNS
                         ,String responseName
                         ,String responseType){
       
            EP_SFDC_Cstmr_Lomo_Intg_WebService.MSG_element msgElement = new EP_SFDC_Cstmr_Lomo_Intg_WebService.MSG_element();
            msgElement.StatusPayload = 'Success_Mock_Lomo';
            response.put('response_x', msgElement);
            
       }
   }*/
   /**
     This class generates mock response for SFDC To Lomosoft (VMI Orders) WebService
   **/
  /* public class EP_OrderWebServiceMock implements WebServiceMock{  
       public void doinvoke(Object stub
                         ,Object request
                         ,Map<String, Object> response
                         ,String endpoint
                         ,String soapAction
                         ,String requestName
                         ,String responseNS
                         ,String responseName
                         ,String responseType){
            
            EP_TrafiguraOrderCreditCheckWebService.MSG_element msgElement = new EP_TrafiguraOrderCreditCheckWebService.MSG_element();
            msgElement.StatusPayload = 'Success_Mock_Lomo';
            response.put('response_x', msgElement); 
            }             
   }*/
   /*
       common method for creating success response
   */
   public static HttpResponse WebserviceExpectedResponse()
    {
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setStatusCode(200);
        httpResponse.setBody('{"message":"success"}');
        return httpResponse;
    }
    
    global HttpResponse respond(HTTPRequest req)
    {
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setStatusCode(200);
        httpResponse.setBody('{"message":"success"}');
        return httpResponse;
    }
   
}