/*   
     @Author Aravindhan Ramalingam
     @name <EP_NonVMINonConsignmentSM.cls>     
     @Description <Non VMI Non Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_NonVMINonConsignmentSM extends EP_OrderStateMachine {
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_NonVMINonConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }