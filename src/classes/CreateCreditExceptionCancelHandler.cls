/*
   @Author          CloudSense
   @Name            CreateCreditExceptionCancelHandler
   @CreateDate      02/05/2018
   @Description     This class is responsible for Updating the Credit Exception status to cancelled
   @Version         1.1
 
*/

global class CreateCreditExceptionCancelHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/09/2017
    * @Description  Method to process the step and update Credit Exception record
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        //Variable declaration to store data
        List<sObject> result = new List<sObject>();
        List<EP_Credit_Exception_Request__c> crList = new List<EP_Credit_Exception_Request__c>();
        List<EP_Credit_Exception_Request__c> crUpdatedList = new List<EP_Credit_Exception_Request__c>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        Map<Id,EP_Credit_Exception_Request__c> creditExceptionMap = new Map<Id,EP_Credit_Exception_Request__c>();
        system.debug('mategr orch 2');
        system.debug('StepList is :' +stepList);
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_AvailableFunds__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.TotalAmount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.CurrencyIsoCode, CSPOFA__Orchestration_Process__r.Order__r.CreatedBy.FederationIdentifier,
                                                                CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Overdue_Amount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Balance__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.EP_Order_Comments__c,CSPOFA__Orchestration_Process__r.Order__r.EP_Is_Credit_Exception_Cancelled_Sync__c,CSPOFA__Orchestration_Process__r.Order__r.Cancellation_Check_Done__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        system.debug('extended list is :' +extendedList);
        
        if (!extendedList.isEmpty()) {
            for(CSPOFA__Orchestration_Step__c step:extendedList){
                orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
            }   
        }
        
        if(orderIdList != null){
           crList =[SELECT 
                        id,EP_Bill_To__c,EP_Current_Order_Value__c,EP_CS_Order__c,EP_Status__c
                    FROM
                        EP_Credit_Exception_Request__c
                    WHERE
                        Order__c in : orderIdList and EP_Status__c = :system.label.CE_Pending_Approval
                                                    
                  ];
        }
        if(crList.size() >0){
            for(EP_Credit_Exception_Request__c ce :crList){
                orderIdCrIdMap.put(ce.EP_CS_Order__c,ce.id);
                creditExceptionMap.put(ce.id,ce);
            }
        }
        
        for(CSPOFA__Orchestration_Step__c step:extendedList){
           
            Id OrderId = step.CSPOFA__Orchestration_Process__r.Order__c;
            Id crId = orderIdCrIdMap.get(OrderId);
            if(crId != null){
                 EP_Credit_Exception_Request__c crRequest = creditExceptionMap.get(crId);
                 crRequest.EP_Status__c=system.label.DocStatus_Cancel;
                 crUpdatedList.add(crRequest);
            }
           
            step.CSPOFA__Status__c ='Complete';
            step.CSPOFA__Completed_Date__c=Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
       
      }
      try{
           
            if(!crList.isEmpty()){
                database.update(crUpdatedList);
            }
        }Catch(Exception exp){
         EP_loggingService.loghandledException(exp, EP_Common_Constant.EPUMA, 'process', 'CreateCreditExceptionCancelHandler',apexPages.severity.ERROR);
        }
      return result; 
   }
}