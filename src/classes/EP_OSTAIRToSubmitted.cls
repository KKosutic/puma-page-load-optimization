/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToSubmitted>
*  @CreateDate <03/02/2017>
*  @Description <handles order status change from any status to submitted>
*  @Version <1.0>
*/

public class EP_OSTAIRToSubmitted extends EP_OrderStateTransition{
    static string classname = 'EP_OSTAIRToSubmitted';
    public EP_OSTAIRToSubmitted(){
        finalState = EP_OrderConstant.OrderState_Submitted;
        //system.debug('In Constructor: EP_OSTANYToSubmitted');
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToSubmitted','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToSubmitted','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTANYToSubmitted');
        
        return super.isRegisteredForEvent();      
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToSubmitted','isGuardCondition');
        if( this.order.getOrder().EP_Reason_For_Change__c.equalsIgnoreCase(EP_OrderConstant.INVENTORYAPPROVED)){
            return true;
        }
        return false;               
    }
    
}