/* 
   @author <Kamal Garg>
   @name <EP_IntegrationAcknowledgementHandler>
   @createDate <27/11/2015>
   @description <This class handles response received middleware> 
   @version <1.0>
*/
public with sharing class EP_IntegrationAcknowledgementHandler {
    
    private static final String HANDLEACKNOWLEDGEMENT = 'handleAcknowledgement';
    private static final String INTEGRATIONACKNOWLEDGEMENTHANDLER = 'EP_IntegrationAcknowledgementHandler';
    /**
     * @author Kamal Garg
     * @date 27/11/2015
     * @description handles response sent by sendAcknowledgement API defined in EP_IntegrationAcknowledgement_WebService apex class.
     * @param jsonResponse : response received
     */
    public static void handleAcknowledgement(String jsonResponse) {
        system.debug('[EP_IntegrationAcknowledgementHandler:handleAcknowledgement] Response : '+jsonResponse);
        EP_IntegrationAcknowledgementHandler obj;
        try{
            // Parse JSON response
            obj = parse(jsonResponse);
            if(obj != NULL && !obj.Acknowledgment.isEmpty()){
                // Parse each Result received in JSON response.
                for(EP_IntegrationAcknowledgementHandler.cls_Acknowledgment ack : obj.Acknowledgment){
                    system.debug('[EP_IntegrationAcknowledgementHandler:handleAcknowledgement] Acknowledgment : '+ack);
                    for(EP_IntegrationAcknowledgementHandler.cls_Result result : ack.Response.Result){
                        system.debug('[EP_IntegrationAcknowledgementHandler:handleAcknowledgement] Result : '+result);
                        String processedStatus = result.ProcessedStatus;
                        // Depending on the ProcessedStatus for each record, we call 'syncOk' or 'syncError' API defined in EP_IntegrationUtil apex class.
                        // If status is 'Success' then we call 'syncOk' API.
                        // If Staus is 'Failure' then we call 'syncError' API.
                        if(processedStatus.equals(EP_Common_Constant.SUCCESS)){
                            EP_IntegrationUtil.syncOk(result.ObjectID, result.MsgID);
                        }else if(processedStatus.equals(EP_Common_Constant.FAILURE)){
                            EP_IntegrationUtil.syncError(result.ObjectID, result.MsgID, result.ErrorDetail.ErrorDescription);
                        }
                    }
                }
                // If all Results received in JSON response parsed successfully then we will need to call the receivedOK or receivedError methods at the end of this http call – to record 
                // a successful or failed processing of the overall response. The methods will required the following inputs:
                //   Object ID = null 
                //   Object Type = “Acknowledgement”
                //   Message ID From Header(MsgID or CorrelationID)
                //   DT_RECEIVED SystemTime-Now
                //   ERROR_DESCRIPTION System exception captured in the class
                //   SOURCE From Header(SourceApplication)
                //   EXTERNAL ID From Header(MsgID or CorrelationID)
                String sourceApplication = obj.Acknowledgment.get(0).HeaderCommon.SourceApplication;
                String correlationID = obj.Acknowledgment.get(0).HeaderCommon.CorrelationID;
                EP_IntegrationUtil.receivedOK(null, EP_Common_Constant.ACKNOWLEDGEMENT, correlationID, Datetime.now(), sourceApplication, correlationID);
            }
        }catch(Exception ex){
            if(obj != NULL && !obj.Acknowledgment.isEmpty()){
                EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, HANDLEACKNOWLEDGEMENT,INTEGRATIONACKNOWLEDGEMENTHANDLER, ApexPages.Severity.ERROR);
                String sourceApplication = obj.Acknowledgment.get(0).HeaderCommon.SourceApplication;
                String correlationID = obj.Acknowledgment.get(0).HeaderCommon.CorrelationID;
                EP_IntegrationUtil.receivedError(null, EP_Common_Constant.ACKNOWLEDGEMENT, correlationID, ex.getMessage(), Datetime.now(), sourceApplication, correlationID);
            } else {
                 EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, HANDLEACKNOWLEDGEMENT,INTEGRATIONACKNOWLEDGEMENTHANDLER, ApexPages.Severity.ERROR);
                // Create an EP_IntegrationRecord__c record with following inputs
                // Object ID = null
                // Object Type = “Acknowledgement”
                // Message ID = Datetime.now().format()
                // ERROR_DESCRIPTION System exception captured in the class
                // DT_RECEIVED SystemTime-Now
                // SOURCE = "SF"
                // EXTERNAL ID = Datetime.now().format()
                EP_IntegrationUtil.receivedError(null, EP_Common_Constant.ACKNOWLEDGEMENT, Datetime.now().format(), ex.getMessage(), Datetime.now(), EP_Common_Constant.TARGET_SF, Datetime.now().format());
            }
        }
    }
    
    private List<cls_Acknowledgment> Acknowledgment;
    /**
     * @author <Kamal Garg>
     * @name <cls_Acknowledgment>
     * @createDate <27/11/2015>
     * @description wrapper class to capture acknowledgement response sent by middleware. 
     * @version <1.0>
     */
    public with sharing class cls_Acknowledgment {
        public cls_HeaderCommon HeaderCommon;
        public cls_Response Response;
        public String StatusResponse;
    }
    /**
     * @author <Kamal Garg>
     * @name <cls_HeaderCommon>
     * @createDate <27/11/2015>
     * @description wrapper class to capture header information from acknowledgement response sent by middleware. 
     * @version <1.0>
     */
    public with sharing class cls_HeaderCommon {
        public String SourceApplication;
        public String DestinationApplication;
        public String SourceCompany;
        public String DestinationCompany;
        public String CorrelationID;
        public String SourceUpdateStatusAddress;
        public String DestinationUpdateStatusAddress;
        public String MiddlewareUrlForPush;
        public String EmailNotification;
        public String TransportStatus;
        public String ProcessStatus;
        public String CommunicationType;
        public String MethodForReceiveAckn;
        public String FormatofAckn;
    }
    /**
     * @author <Kamal Garg>
     * @name <cls_Response>
     * @createDate <27/11/2015>
     * @description wrapper class to capture response information from acknowledgement response sent by middleware. 
     * @version <1.0>
     */
    public with sharing class cls_Response {
        public cls_Result[] Result;
    }
    /**
     * @author <Kamal Garg>
     * @name <cls_Result>
     * @createDate <27/11/2015>
     * @description wrapper class to capture result object information from acknowledgement response sent by middleware. 
     * @version <1.0>
     */
    public with sharing class cls_Result {
        public String Type;
        public String Seq;
        public String MsgID;
        public String ObjectID;
        public String TransactionId;
        public String ProcessedStatus;
        public String ProcessedDateTime;
        public cls_ErrorDetail ErrorDetail;
    }
    /**
     * @author <Kamal Garg>
     * @name <cls_Result>
     * @createDate <27/11/2015>
     * @description wrapper class to capture error information from acknowledgement response sent by middleware if any. 
     * @version <1.0>
     */
    public with sharing class cls_ErrorDetail {
        public String ErrorCode;
        public String ErrorDescription;
    }
    
    /**
     * @author Kamal Garg
     * @date 27/11/2015
     * @description Parse JSON response.
     * @param jsonResponse : response received
     * @return Return EP_IntegrationAcknowledgementHandler apex class refernce
     */
    public static EP_IntegrationAcknowledgementHandler parse(String jsonResponse){
        try{
        return (EP_IntegrationAcknowledgementHandler) System.JSON.deserialize(jsonResponse, EP_IntegrationAcknowledgementHandler.class);
        }
        catch(Exception e){
              System.debug('Exception : '+e);
              return null;
        }
    }   
}