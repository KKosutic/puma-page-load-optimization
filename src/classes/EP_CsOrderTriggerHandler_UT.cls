@isTest
public class EP_CsOrderTriggerHandler_UT {

    @isTest
    static void orderTriggerHandler_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
        
    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
        
        Map<Id,csord__Order__c> orderMap = new Map<Id,csord__Order__c>{newOrder.Id => newOrder};
        
        Test.startTest();
            EP_CsOrderTriggerHandler.doAfterUpdate(orderMap);
        Test.stopTest();
    }
}