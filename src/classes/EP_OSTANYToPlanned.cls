/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToPlanned>
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to Planned>
*  @Version <1.0>
*/

public class EP_OSTANYToPlanned extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToPlanned';
    public EP_OSTANYToPlanned(){
        finalState = EP_OrderConstant.OrderState_Planned;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanned','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanned','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToPlanned','isGuardCondition');
        
        if(!String.isBlank(this.order.getOrder().EP_WinDMS_Status__c)){
            if(this.order.getOrder().EP_WinDMS_Status__c.equalsIgnoreCase(EP_Common_Constant.planned) 
            && this.order.getOrder().EP_Order_Credit_Status__c.equalsIgnoreCase(EP_Common_Constant.Credit_Okay)){
 
                this.order.getOrder().EP_Planned_Delivery_Date_Time__c = this.order.getOrder().EP_Estimated_Delivery_Date_Time__c;
                // Defect Start # 57216 : set ETA
                setETAonOrder();
                // Defect End # 57216
                orderEvent.setEventMessage(this.order.getOrder().EP_WinDMS_Status__c);
                system.debug('Order status is updated as planned *****');
                return true;
            }else if(this.order.getOrder().EP_WinDMS_Status__c.equalsIgnoreCase(EP_Common_Constant.LOCKED_STATUS)){
                orderEvent.setEventMessage(this.order.getOrder().EP_WinDMS_Status__c);
                return true;
            }
        }
        return false;                            
    }        

    private void setETAonOrder(){
        EP_GeneralUtility.Log('private','EP_OSTANYToPlanned','setETAonOrder');
        Account selltoAcc = this.order.getSellToAccountDomain().getAccount();
        // Calculating ETA Range, calls General utility method.
        if(EP_GeneralUtility.isDateTimeValid(string.valueOf(this.order.getOrder().EP_Planned_Delivery_Date_Time__c))){
            this.order.getOrder().EP_Estimated_Time_Range__c = EP_GeneralUtility.getETARangeOnOrder(string.valueOf(this.order.getOrder().EP_Planned_Delivery_Date_Time__c), 
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_Start_Hours__c),
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_End_Hours__c),
             this.order.getOrder().Status__c);
        }
    }
}