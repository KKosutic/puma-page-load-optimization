@isTest
private class EP_CreditExceptionUpdateStub_UT {
	
	static testMethod void unit_test() {
    	string jsonBody =  '{"MSG":{"HeaderCommon":{"InterfaceType":"SFDC_NAV","SourceCompany":"AAF","SourceResponseAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","SourceUpdateStatusAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"New","UpdateSourceOnReceive":"false","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"false","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"CreditException","CommunicationType":"Async","MsgID":"788d09f3-eba2-45ff-bbd1-cb29c44a7f57","SourceGroupCompany":"null","DestinationGroupCompany":"null","DestinationCompany":"null","CorrelationID":"null","DestinationAddress":"","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"null","EmailNotification":"null","ErrorCode":"null","ErrorDescription":"null","ProcessStatus":"null","ProcessingErrorDescription":"ProcessingErrorDescription"},"Payload":{"any0":{"creditExceptionStatus":{"status":[{"seqId":"seqId-000362","identifier":{"exceptionNo":"CER-000026","billTo":"00006683","clientId":"PA22"},"approvalStatus":"Approved","reasonForRejection":"XXX","approverId":"Tester.Te","modifiedDt":"2017-12-21T12:45:00","modifiedBy":"Tester.Test","versionNr":"20171219T234609.000"}]}}}}}';
        Test.startTest();
            EP_CreditExceptionUpdateStub stub = (EP_CreditExceptionUpdateStub) System.JSON.deserialize(jsonBody, EP_CreditExceptionUpdateStub.class);
        Test.stopTest();
        system.assertNotEquals(stub.MSG, null);
    }
}