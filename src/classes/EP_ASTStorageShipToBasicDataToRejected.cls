/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToBasicDataToRejected>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 08-Rejected>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToBasicDataToRejected extends EP_AccountStateTransition {

    public EP_ASTStorageShipToBasicDataToRejected () {
        finalState = EP_AccountConstant.REJECTED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToRejected', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToRejected',' isGuardCondition');        
        return true;
    }
}