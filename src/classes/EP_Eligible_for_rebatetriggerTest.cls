/**
@Author <Pravindra Khatri>
@name <EP_Eligible_for_rebatetriggerTest>
@CreateDate <05/01/2016>
@Description <This test class is used to cover Trigger 'EP_Eligible_for_rebatetrigger' as well as Automation Scenarios for Thread 'E2E024_002g'>
@Version <1.0>
*/
@isTest
public class EP_Eligible_for_rebatetriggerTest{
    private static Account accobj,accObj1,accobj2; 
    private static EP_Freight_Matrix__c freightMatrix;
    
// This method validates the 'EP_Eligible_for_Rebate__c' field of OrderItem to TRUE if ('Eligible_for_Rebate__c' field of Product is TRUE AND 'EP_Eligible_for_Rebate__c' field of Order account is TRUE AND 'EP_Eligible_for_Rebate__c' field of Order Ship-to account is TRUE)
    private static testMethod void orderitem_eligible_for_rebate(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Id selltostrRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Id pricebookId = Test.getStandardPricebookId();                         

        Test.startTest();
        
        System.runAs(cscUser){
            
            // Inserting a EP_Freight_Matrix__c record      
            freightMatrix = EP_TestDataUtility.createFreightMatrix();
            database.insert(freightMatrix,false);
            
            //create a bill-to account record 
            accobj2 = EP_TestDataUtility.createBillToAccount();
           // accobj2.EP_Eligible_for_Rebate__c=TRUE;
            database.insert(accobj2,false);
            
            // create a sell-to account record where sell-to <> bill-to
            accObj1 = EP_TestDataUtility.createSellToAccount(accObj2.id,freightMatrix.id);
            accobj1.EP_Eligible_for_Rebate__c=TRUE;
            database.insert(accObj1,false);
            
            //Create a ship-to account record       
            accobj = EP_TestDataUtility.createShipToAccount(accObj1.Id, strRecordTypeId);
            insert accObj;
            accobj.EP_Eligible_for_Rebate__c = TRUE;
            update accobj;
            
            Product2 prod = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1', isActive = true,Eligible_for_Rebate__c =True);
            insert prod;
/*
            PricebookEntry price =new PricebookEntry(UnitPrice = 1,Product2Id=prod.id,Pricebook2Id=pricebookId,isActive=true);
            insert price;

            Order ord = new Order(name='Test1',Accountid = accobj1.id,EffectiveDate=system.today(),status='draft', PriceBook2Id=price.id ,EP_ShipTo__c = accobj.id);
            database.insert(ord,false);

            OrderItem item = new OrderItem(OrderId=ord.id,Quantity=1,PricebookEntryId=price.id, unitPrice=1,EP_Eligible_for_Rebate__c = true);
            database.insert(item,false);
            
            system.assertequals(item.EP_Eligible_for_Rebate__c,true);*/
        }
        test.stoptest();
    } 
// This method validates the 'EP_Eligible_for_Rebate__c' field of OrderItem to TRUE if ('Eligible_for_Rebate__c' field of Product is TRUE AND 'EP_Eligible_for_Rebate__c' field of Order account is TRUE AND 'EP_Eligible_for_Rebate__c' field of Order Ship-to account is TRUE)
    private static testMethod void orderitem_eligible_for_rebate1(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Id selltostrRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Id pricebookId = Test.getStandardPricebookId();                         

        Test.startTest();
        
        System.runAs(cscUser){
                       
            // create a sell-to account record where sell-to == bill-to
            accObj1 = EP_TestDataUtility.createSellToAccount(NULL,NULL);
            database.insert(accObj1,false);
            
            //Create a ship-to account record       
            accobj = EP_TestDataUtility.createShipToAccount(accObj1.Id, strRecordTypeId);
            database.insert(accObj,false);   
            accobj.EP_Eligible_for_Rebate__c = TRUE;
            update accobj;
            Product2 prod = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1', isActive = true,Eligible_for_Rebate__c =True);
            insert prod;

            //PricebookEntry price =new PricebookEntry(UnitPrice = 1,Product2Id=prod.id,Pricebook2Id=pricebookId,isActive=true);
            //insert price;

            //Order ord = new Order(name='Test1',Accountid = accobj1.id,EffectiveDate=system.today(),status='draft', PriceBook2Id=pricebookId,EP_ShipTo__c = accobj.id);
            //database.insert(ord,false);

            //OrderItem item = new OrderItem(OrderId=ord.id,Quantity=1,PricebookEntryId=price.id, unitPrice=1,EP_Eligible_for_Rebate__c = true);
            //database.insert(item,false);
            
            //system.assertequals(item.EP_Eligible_for_Rebate__c,true);
        }
        test.stoptest();
    } 
    
// This method validates the 'EP_Eligible_for_Rebate__c' field of OrderItem to FALSE if ('Eligible_for_Rebate__c' field of Product is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order account is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order Ship-to account is FALSE)
    private static testMethod void orderitem_eligible_for_rebate2(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Id selltostrRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Id pricebookId = Test.getStandardPricebookId();                               

        Test.startTest();
        
        System.runAs(cscUser){
            
            // Inserting a EP_Freight_Matrix__c record      
            freightMatrix = EP_TestDataUtility.createFreightMatrix();
            database.insert(freightMatrix,false);
            
            //create a bill-to account record 
            accobj2 = EP_TestDataUtility.createBillToAccount();
            database.insert(accobj2,false);
            
            // create a sell-to account record where sell-to <> bill-to
            accObj1 = EP_TestDataUtility.createSellToAccount(accObj2.id,freightMatrix.id);
            database.insert(accObj1,false);
            
            //Create a ship-to account record       
            accobj = EP_TestDataUtility.createShipToAccount(accObj1.Id, strRecordTypeId);
            database.insert(accObj,false);   
             
            
            Product2 prod = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1', isActive = true, Eligible_for_Rebate__c = false);
            insert prod;
/*
            PricebookEntry price =new PricebookEntry(UnitPrice = 1,Product2Id=prod.id,Pricebook2Id=pricebookId,isActive=true);
            insert price;

            Order ord = new Order(name='Test1',Accountid = accobj1.id,EffectiveDate=system.today(),status='draft', PriceBook2Id=pricebookId,EP_ShipTo__c = accobj.id);
            database.insert(ord,false);

            OrderItem item = new OrderItem(OrderId=ord.id,Quantity=1,PricebookEntryId=price.id, unitPrice=1,EP_Eligible_for_Rebate__c = false);
            database.insert(item,false);
            
            system.assertequals(item.EP_Eligible_for_Rebate__c,false);*/
        }
        test.stoptest();
    }
    // This method validates the 'EP_Eligible_for_Rebate__c' field of OrderItem to FALSE if ('Eligible_for_Rebate__c' field of Product is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order account is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order Ship-to account is FALSE)
    private static testMethod void orderitem_eligible_for_rebate3(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Id selltostrRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Id pricebookId = Test.getStandardPricebookId();                               

        Test.startTest();
        
        System.runAs(cscUser){
            
            // create a sell-to account record where sell-to == bill-to
            accObj1 = EP_TestDataUtility.createSellToAccount(NULL,NULL);
            database.insert(accObj1,false);
            
            //Create a ship-to account record       
            accobj = EP_TestDataUtility.createShipToAccount(accObj1.Id, strRecordTypeId);
            database.insert(accObj,false);   
             
            /*
            Product2 prod = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1', isActive = true, Eligible_for_Rebate__c = false);
            insert prod;

            PricebookEntry price =new PricebookEntry(UnitPrice = 1,Product2Id=prod.id,Pricebook2Id=pricebookId,isActive=true);
            insert price;

            Order ord = new Order(name='Test1',Accountid = accobj1.id,EffectiveDate=system.today(),status='draft', PriceBook2Id=pricebookId,EP_ShipTo__c = accobj.id);
            database.insert(ord,false);

            OrderItem item = new OrderItem(OrderId=ord.id,Quantity=1,PricebookEntryId=price.id, unitPrice=1,EP_Eligible_for_Rebate__c = false);
            database.insert(item,false);
            
            system.assertequals(item.EP_Eligible_for_Rebate__c,false);*/
        }
        test.stoptest();
    } 
  // This method validates the 'EP_Eligible_for_Rebate__c' field of OrderItem to FALSE if ('Eligible_for_Rebate__c' field of Product is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order account is FALSE OR 'EP_Eligible_for_Rebate__c' field of Order Ship-to account is TRUE)
    private static testMethod void orderitem_eligible_for_rebate4(){
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        Id selltostrRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Id pricebookId = Test.getStandardPricebookId();                               

        Test.startTest();
        
        System.runAs(cscUser){
            
            // create a sell-to account record where sell-to == bill-to
            accObj1 = EP_TestDataUtility.createSellToAccount(NULL,NULL);
            database.insert(accObj1,false);
            
            accobj1.EP_Eligible_for_Rebate__c = false;
            database.update(accobj1,false);
            
            //Create a ship-to account record       
            accobj = EP_TestDataUtility.createShipToAccount(accObj1.Id, strRecordTypeId);
            database.insert(accObj,false);   
             
            /*
            Product2 prod = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1', isActive = true, Eligible_for_Rebate__c = false);
            insert prod;

            PricebookEntry price =new PricebookEntry(UnitPrice = 1,Product2Id=prod.id,Pricebook2Id=pricebookId,isActive=true);
            insert price;

            Order ord = new Order(name='Test1',Accountid = accobj1.id,EffectiveDate=system.today(),status='draft', PriceBook2Id=pricebookId,EP_ShipTo__c = accobj.id);
            database.insert(ord,false);

            OrderItem item = new OrderItem(OrderId=ord.id,Quantity=1,PricebookEntryId=price.id, unitPrice=1,EP_Eligible_for_Rebate__c = false);
            database.insert(item,false);
            
            system.assertequals(item.EP_Eligible_for_Rebate__c,false);*/
        }
        test.stoptest();
    } 
 
}