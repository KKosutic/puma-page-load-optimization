/*
	@Author <Jai Singh>
   	@name <EP_Manual_RetryController>
   	@Description <Page controller for page <EP_Manual_RetryPage> to submit ERROR_SENT Integration Records for retry >
   	@Version <1.0>
*/ 
public with sharing class EP_Manual_RetryController {
        
    public Boolean showRecords{get;set;}
    public Integer Maximux_Record_Count{get;set;}
    public String Id_separater{get;set;}
    private static final string QUERY1 = 'Select Id, Name, EP_Resend__c, EP_Status__c, EP_DT_SENT__c, EP_Error_Description__c,';
    private static final string QUERY2 = 'EP_Message_ID__c, EP_Company__c, EP_Object_Type__c, EP_Source__c, EP_Object_Record_Name__c, ';
    private static final string QUERY3 = 'EP_Target__c, EP_Transaction_ID__c, EP_Attempt__c, EP_IsLatest__c, EP_Object_ID__c ';
    private static final string QUERY4 = 'FROM EP_IntegrationRecord__c ';
    private static final string QUERY5 = 'WHERE EP_IsLatest__c = TRUE ';
    private static final string QUERY6 = 'AND EP_Integration_Failed__c = TRUE ';
    private static final string QUERY7 = 'AND (EP_Object_ID__c = :strObjectID OR EP_Query_Object_ID__c = :strObjectID) ';
    private static final string QUERY8 = 'ORDER BY CreatedDate DESC ';
    private static final string QUERY9 = 'LIMIT 1000 ';
    private List<EP_IntegrationRecord__c> integrationRecords;
    /*
    @Description <Initialize setController and return a list of records>
    */
    public List<EP_IntegrationRecord__c> getIntRecords() {
    	integrationRecords = new List<EP_IntegrationRecord__c>();
    	integrationRecords = (List<EP_IntegrationRecord__c>) IntSetController.getRecords();
        return integrationRecords;
    }
    
    private String strObjectID {
    	get {
    		strObjectID = NULL;
    		
    		if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID) != NULL)
    			strObjectID = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
    		
    		return strObjectID;
    	}
    	set;
    }
    
    /*
    @Description <StandardSetController property>
    */
    public Apexpages.StandardSetController IntSetController {
        get {
	        if(IntSetController == NULL)
	        {
	        	String strQuery = QUERY1;
				strQuery += QUERY2;
				strQuery += QUERY3;
				strQuery += QUERY4;
				strQuery += QUERY5;
				strQuery += QUERY6;
				
				if (strObjectID != NULL)
					strQuery += QUERY7;
				
				strQuery += QUERY8;
				strQuery += QUERY9;
				
				strQuery = String.escapeSingleQuotes(strQuery);
				
				IntSetController = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
				
	            // Set no of records on each page
	            IntSetController.setPageSize(Maximux_Record_Count);
	        } 
	        return IntSetController;
        }
        set;
    
    }
    
    /*
    @Description <Constructor Definition>
    */
    public EP_Manual_RetryController(){
        showRecords = TRUE;
        Id_separater = EP_Common_Constant.TIME_SEPARATOR_SIGN;
        if(EP_INTEGRATION_CUSTOM_SETTING__c.getValues(EP_Common_Constant.RETRY_MAX_RECORD_COUNT) != NULL)
        {
            String value = EP_INTEGRATION_CUSTOM_SETTING__c.getValues(EP_Common_Constant.RETRY_MAX_RECORD_COUNT).EP_Value__c;
            if( String.isNotBlank( value ) && value.isNumeric())
            {
            	Maximux_Record_Count = Integer.valueof(value);
            }
            else
            {
            	Maximux_Record_Count = 0;
            }
        }
    }
     
    /*
    @Description <Method to reTry selected records>
    */
    public void resubmit() {
        //get Selected Integration records from page parameters
        String selectedIds = ApexPages.currentPage().getParameters().get(EP_Common_Constant.SELECTED_PARAMETER_NAME);
        
        if( String.isNotBlank( selectedIds ) )
        {
                //System.debug( 'selectedIds=='+selectedIds );
                try
                {
                        list<Id> selectedIntRecordIds = selectedIds.split(EP_Common_Constant.TIME_SEPARATOR_SIGN);
                        if( !selectedIntRecordIds.isEmpty() )
                        {
                                //create EP_IntegrationRecord__c list to update
                                list<EP_IntegrationRecord__c> selectedIntRecords = new list<EP_IntegrationRecord__c>();
                                
                                for( Id selectedId : selectedIntRecordIds )
                                {
                                	//Start Changes for #59187 
                                	EP_IntegrationRecord__c intRec = new EP_IntegrationRecord__c( Id = selectedId ) ;
                                	intRec.EP_Resend__c = true;
                                  	selectedIntRecords.add(intRec);
                                  	//End Changes for #59187
                                }
                                Database.update(selectedIntRecords);
                                showRecords = False;
                                Apexpages.addMessage( new Apexpages.Message( Apexpages.Severity.CONFIRM, EP_Common_Constant.SUCCESS_MESSAGE));
                        }
                }
                catch( Exception Ex)
                {
                        //show Error on page
                        //system.debug( Ex.getMessage());
                        Apexpages.addMessages(Ex);
                }
                 
        }
    }
}