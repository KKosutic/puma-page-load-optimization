/***************************************************************
*  @Author              Accenture                                      *
*  @Name                EP_HoldingAssociationController                        *
*  @CreateDate  9-Nov-2017                                     *
*  @Description This class is use as controller  for 
                                EP_Holding VF page                                         *
*  @Version     1.0                                            *
****************************************************************/
public with sharing class EP_HoldingAssociationController {
    public EP_HoldingAssociationContext holdingAssociationCtx {get;set;}
    public list<string> lststr{get;set;}
    public EP_HoldingAssociationController(){
            holdingAssociationCtx = new EP_HoldingAssociationContext();
            getResult();
            lststr = new list<string>();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         getResult                                                           *
* @description  This Method is used to retrive reocords
                                to show on page                                 *
* @param        NA                                                                              *
* @return       pageReference                                   *
****************************************************************/        
    public pageReference getResult(){
            holdingAssociationCtx.getReport(); 
            return null; 
    }
}