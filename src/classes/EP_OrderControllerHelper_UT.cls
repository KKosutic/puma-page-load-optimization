@isTest
public class EP_OrderControllerHelper_UT
{
    //For state machine to give proper status
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

    static testMethod void getOrderSummaryItemsNewOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        csord__Order__c newOrd = ord.clone(false,false,false,false);
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.accountid__c);
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx); 
        ctx.newOrderRecord = newOrd;
        //Test Class Fix Start
        ctx.itemIdTotalPriceMap = new Map<Id,Double>();
        for(csord__Order_Line_Item__c orderItemObj : ctx.newOrderRecord.csord__Order_Line_Items__r){
       		ctx.itemIdTotalPriceMap.put(orderItemObj.id, 1.1);
        }
		//Test Class Fix End
        ctx.loadExistingOrderItems();
        List<EP_OrderPageContext.OrderSummaryWrapper> orderItems = new List<EP_OrderPageContext.OrderSummaryWrapper>();
        Test.startTest();
        LIST< EP_OrderPageContext.OrderSummaryWrapper > result = localObj.getOrderSummaryItems();
        Test.stopTest();
        System.AssertEquals(true,(result.size() == ctx.listofOrderWrapper.size()));
    }

    static testMethod void getOrderSummaryItemsPricingCalculated_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        //Test Class Fix Start
        ctx.itemIdTotalPriceMap = new Map<Id,Double>();
        for(csord__Order_Line_Item__c orderItemObj : ctx.newOrderRecord.csord__Order_Line_Items__r){
       		ctx.itemIdTotalPriceMap.put(orderItemObj.id, 1.1);
        }
		//Test Class Fix End
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx); 
        ctx.isPriceCalculated =false;
        ctx.loadExistingOrderItems();
        List<EP_OrderPageContext.OrderSummaryWrapper> orderItems = new List<EP_OrderPageContext.OrderSummaryWrapper>();
        Test.startTest();
        LIST< EP_OrderPageContext.OrderSummaryWrapper > result = localObj.getOrderSummaryItems();
        Test.stopTest();
        System.AssertEquals(true,(result.size() == ctx.listofOrderWrapper.size()));
    }

    static testMethod void getOrderSummaryItemsPricingNotCalculated_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        //Test Class Fix Start
        ctx.itemIdTotalPriceMap = new Map<Id,Double>();
        for(csord__Order_Line_Item__c orderItemObj : ctx.newOrderRecord.csord__Order_Line_Items__r){
       		ctx.itemIdTotalPriceMap.put(orderItemObj.id, 1.1);
        }
		//Test Class Fix End
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx); 
        ctx.isPriceCalculated =true;
        ctx.loadExistingOrderItems();
        List<EP_OrderPageContext.OrderSummaryWrapper> orderItems = new List<EP_OrderPageContext.OrderSummaryWrapper>();
        Test.startTest();
        LIST< EP_OrderPageContext.OrderSummaryWrapper > result = localObj.getOrderSummaryItems();
        Test.stopTest();
        System.AssertEquals(true,(result.size() == ctx.listofOrderWrapper.size()));
    }
    static testMethod void createSummaryfromLocalWrapper_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
       	//Test Class Fix Start
        ctx.itemIdTotalPriceMap = new Map<Id,Double>();
        for(csord__Order_Line_Item__c orderItemObj : ctx.newOrderRecord.csord__Order_Line_Items__r){
       		ctx.itemIdTotalPriceMap.put(orderItemObj.id, 1.1);
        }
		//Test Class Fix End
        ctx.loadExistingOrderItems();
        
        List<EP_OrderPageContext.OrderSummaryWrapper> orderItems = new List<EP_OrderPageContext.OrderSummaryWrapper>();
        Test.startTest();
        localObj.createSummaryfromLocalWrapper(orderItems);
        Test.stopTest();
        system.assertEquals(true, ctx.listofOrderWrapper[0].oliQuantity == orderItems[0].oswQuantity);
    }
    static testMethod void setShipToDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
        Test.startTest();
        localObj.setShipToDetails();
        Test.stopTest();
        System.AssertEquals(true,ctx.listOfShipToOptions.size() == ctx.orderDomainObj.getShipToAccounts().size());
    }
    static testMethod void loadStep1_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        csord__Order__c newOrd = ord.clone(false,false,false,false);
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.accountid__c);
        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
        ctx.strSelectedDeliveryType = EP_Common_Constant.EX_RACK;
        // getting shipto id
        ctx.strSelectedShipToID = ord.EP_ShipTo__c;

        Test.startTest();
        PageReference result = localObj.loadStep1();
        Test.stopTest();
        // Method always return the null
        System.AssertEquals(true,result == null);
    }

    static testMethod void loadStep2DeliveryOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.strSelecteddeliveryType = EP_Common_Constant.DELIVERY;
        ctx.strSelectedShipToID = ord.EP_ShipTo__c;
        system.debug('strSelectedShipToID is  ' + ctx.strSelectedShipToID);

        map<id,account> shiptoacc = new map<id, account>();
        shiptoacc.put(ord.EP_ShipTo__C, ord.EP_ShipTo__r);
        ctx.mapofShipTo = shiptoacc;

        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
        Test.startTest();
        PageReference result = localObj.loadStep2();
        Test.stopTest();

        system.debug('ctx.intOrderCreationStepIndex ' + ctx.intOrderCreationStepIndex);
        //method returns always returns null
        system.debug('result');
        system.assertEquals(true, result == null);
        system.assertEquals(true, ctx.intOrderCreationStepIndex == 1);

    }

    static testMethod void loadStep2BulkOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        
        EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
        ctx.strSelectedShipToID = ord.EP_ShipTo__c;
        ctx.strSelecteddeliveryType = EP_Common_Constant.DELIVERY;
        ctx.isBulkOrder = true;

        map<id,account> shiptoacc = new map<id, account>();
        shiptoacc.put(ord.EP_ShipTo__C, ord.EP_ShipTo__r);
        ctx.mapofShipTo = shiptoacc;

        EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
        Test.startTest();
        PageReference result = localObj.loadStep2();
        Test.stopTest();
        
        system.debug('result');
        //method returns always returns null
        system.assertEquals(true, result == null);
        system.assertEquals(true, ctx.isOprtnlTankAvail);
    }

static testMethod void fetchOrderLineItemSummary_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    LIST< EP_OrderPageContext.OrderSummaryWrapper > orderItems = new LIST< EP_OrderPageContext.OrderSummaryWrapper >();
    Test.startTest();
    localObj.fetchOrderLineItemSummary(orderItems);
    Test.stopTest();

    System.AssertEquals(true,orderItems[0].oswCost != null);
}

static testMethod void populateChildrenLineItems_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    LIST<OrderItem> OrderItemList = [SELECT AvailableQuantity,CurrencyIsoCode,Description,EndDate,pricebookentry.product2.name, EP_3rd_Party_Stock_Supplier__c,EP_Accounting_Details__c,EP_Ambient_Delivered_Quantity__c,EP_Ambient_Loaded_Quantity__c,EP_BOL_Number__c,EP_ChargeType__c,EP_Contract_Nav_Id__c,EP_Contract__c,EP_Eligible_for_Rebate__c,EP_Integration_Status__c,EP_InventoryErrorMessage__c,EP_Inventory_Availability__c,EP_Invoice_Name__c,EP_Is_Freight_Price__c,EP_Is_Standard__c,EP_Is_Tank_Fill__c,EP_Is_Taxes__c,EP_Line_Id_NAV_XML__c,EP_Line_Id_WINDMS_XML__c,EP_Location_ID_NAV_XML__c,EP_OrderItem_Number__c,EP_Order_Item_Key__c,EP_Order_Line_Number__c,EP_Order_Line_Relationship_Index__c,EP_Order_Number__c,EP_Parent_Order_Line_item__c,EP_Pricing_Error__c,EP_Pricing_Response_Unit_Price__c,EP_Pricing_Total_Amount__c,EP_Prior_Quantity__c,EP_Product_Code__c,EP_Product_Name__c,EP_Product__c,EP_Quantity_UOM__c,EP_SeqId__c,EP_Standard_Delivered_Quantity__c,EP_Standard_Loaded_Quantity__c,EP_Stock_Holding_Location__c,EP_Stock_Location_Id__c,EP_Stock_Location_Pricing__c,EP_Supplier_Nav_Vendor_Id__c,EP_Tank_Code__c,EP_Tank_Number__c,EP_Tank__c,EP_Tax_Amount_XML__c,EP_Tax_Amount__c,EP_Tax_Percentage__c,EP_Total_Price__c,EP_Unit_of_Measure__c,EP_Unit__c,EP_Updated_Amount__c,EP_WinDMS_Line_ItemId__c,EP_WinDMS_Line_Item_Reference_Number__c,EP_WinDMS_StockHldngLocId__c,Id,ListPrice,OrderId,OrderItemNumber,OriginalOrderItemId,PricebookEntryId,Product2Id,Quantity,ServiceDate,UnitPrice FROM OrderItem where OrderId=:ord.id];

    EP_OrderPageContext.OrderSummaryWrapper orderSummaryObj = new EP_OrderPageContext.OrderSummaryWrapper();
    LIST< EP_OrderPageContext.OrderSummaryWrapper > orderItems =  new LIST< EP_OrderPageContext.OrderSummaryWrapper >();
    

    Test.startTest();
    localObj.populateChildrenLineItems(OrderItemList,orderSummaryObj,orderItems);
    Test.stopTest();
    system.debug('orderItems ' + orderItems.size());

    System.AssertEquals(true,orderItems.size() > 0);
}
static testMethod void supplierContracts_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    List<csord__Order_Line_Item__c> oitems = ord.csord__Order_Line_Items__r;
    OrderItem oiq = [select id,EP_3rd_Party_Stock_Supplier__c from orderitem where OrderId= :ord.id limit 1];
    String supplierId = oiq.EP_3rd_Party_Stock_Supplier__c;
    
    Test.startTest();
    LIST<SelectOption> result = localObj.supplierContracts(supplierId);
    Test.stopTest();
    system.debug('Result is ' + result.size());
    System.AssertEquals(true,result.size() > 0);
}
static testMethod void populateOrderSummaryItem_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    List<csord__Order_Line_Item__c> oItemList = ord.csord__Order_Line_Items__r;
    csord__Order_Line_Item__c oitem = oItemList[0];
    String strProductName = oitem.EP_Product__c;

    Integer intQuantity = 10;
    String strQuantityUoM = oitem.EP_Quantity_UOM__c ;
    Double basePrice = 1.0;
    Double unitPrice = 2.0;
    Double dblCost = 3.0;
    String strCurrencyCode = 'USD';
    Integer intIndex = 0;
    Boolean isAdditionalItem =false;
    String description = 'Product';
    Double taxPercentage = 0.2;
    Double taxAmount = 12;

    Test.startTest();
    EP_OrderPageContext.OrderSummaryWrapper result = localObj.populateOrderSummaryItem(strProductName,intQuantity,strQuantityUoM,basePrice,unitPrice,dblCost,strCurrencyCode,intIndex,isAdditionalItem,description,taxPercentage,taxAmount);
    Test.stopTest();
    
    System.AssertEquals(true,result.oswProductName == strProductName);
    System.AssertEquals(true,result.oswQuantityUoM == strQuantityUoM);
    System.AssertEquals(true,result.oswQuantity == intQuantity);
    
}
static testMethod void isValidLoadingDate_PositiveScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);

    Contract contract1 = new Contract();
    contract1.accountid = ord.accountid__c;
    contract1.startdate = Date.valueOf('2017-04-11');
    contract1.enddate = Date.valueOf('2017-04-15');

    Date loadingDate = Date.valueOf('2017-04-13');

    Test.startTest();
    Boolean result = localObj.isValidLoadingDate(contract1,loadingDate);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isValidLoadingDate_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);

    Contract contract1 = new Contract();
    contract1.accountid = ord.accountid__c;
    contract1.startdate = Date.valueOf('2017-04-11');
    contract1.enddate = Date.valueOf('2017-04-15');
    
    Date loadingDate = Date.valueOf('2017-05-13');
    Test.startTest();
    Boolean result = localObj.isValidLoadingDate(contract1,loadingDate);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void setOrderDetails_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.strSelectedAccountID = ord.accountid__c;
    ctx.strSelectedPricebookID = ord.Pricebook2Id__c;
    ctx.strSelectedRoute = ord.EP_Route__c;
    ctx.strSelectedRun = ord.EP_Run__c;
    Test.startTest();
    localObj.setOrderDetails();
    Test.stopTest();
    // sets order details and will not return anything
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void getStockHolderLocation_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    EP_Stock_Holding_Location__c stocklocation = [select id from EP_Stock_Holding_Location__c limit 1];
    
    Test.startTest();
    EP_Stock_Holding_Location__c result = localObj.getStockHolderLocation(stocklocation.id);
    Test.stopTest();
    
    System.AssertEquals(true,result.id == stocklocation.id);

}
static testMethod void isOrderPricingInvokable_PositiveScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.pricingPollerCount = 1;
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.isOrderPricingInvokable();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isOrderPricingInvokable_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.pricingPollerCount = 0;
    Test.startTest();
    Boolean result = localObj.isOrderPricingInvokable();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void populatesupplylocations_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.listOfSupplyLocationOptions = new List < SelectOption >();
    LIST<EP_Stock_Holding_Location__c> listOfStockHoldingLocations = [select id,Stock_Holding_Location__r.Name from EP_Stock_Holding_Location__c];
    
    Test.startTest();
    localObj.populatesupplylocations(listOfStockHoldingLocations);
    Test.stopTest();
    
    System.AssertEquals(true,ctx.mapOfSupplyLocation.size() > 0);
    
}
static testMethod void doActionValidateStockHolidingLocations_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.strSelecteddeliveryType = EP_Common_Constant.DELIVERY;
    system.debug('Order is ' + ord);
    EP_Stock_Holding_Location__c stocLocs = [select id,name,Stock_Holding_Location__r.Name from EP_Stock_Holding_Location__c where id = :ord.Stock_Holding_Location__c limit 1];
    system.debug('Stock loc list ' + stocLocs);
    ctx.listOfSupplyLocationOptions = new List<SelectOption>();
    ctx.listOfSupplyLocationOptions.add(new SelectOption(stocLocs.ID, stocLocs.Name));
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    
    Test.startTest();
    localObj.doActionValidateStockHolidingLocations();
    Test.stopTest();
    // method doesnt return
    //System.AssertEquals(true,<asset conditions>); 
}

static testMethod void doActionValidateStockHolidingLocations_test1() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.strSelecteddeliveryType = EP_Common_Constant.EX_RACK;
    system.debug('Order is ' + ord);
    EP_Stock_Holding_Location__c stocLocs = [select id,name,Stock_Holding_Location__r.Name from EP_Stock_Holding_Location__c where id = :ord.Stock_Holding_Location__c limit 1];
    system.debug('Stock loc list ' + stocLocs);
    ctx.listOfSupplyLocationOptions = new List<SelectOption>();
    ctx.listOfSupplyLocationOptions.add(new SelectOption(stocLocs.ID, stocLocs.Name));

    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    //localobj.populatesupplylocations(stocLocs);

    Test.startTest();
    localObj.doActionValidateStockHolidingLocations();
    Test.stopTest();

    // method doesnt return
    //System.AssertEquals(true,<asset conditions>);
}


static testMethod void removeOrderLineItem_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.strSelectedOrderLineItem = '0';
    csord__Order_Line_Item__c oitem = (ord.csord__Order_Line_Items__r)[0];
    system.debug('Order items is ' + oitem);
    EP_OrderPageContext.OrderWrapper orWrapper = new EP_OrderPageContext.OrderWrapper();
    orwrapper.oliIndex = 0;
    orwrapper.orderLineItem = oitem;


    List<EP_OrderPageContext.OrderWrapper> oWrapperList = new List<EP_OrderPageContext.OrderWrapper>();
    oWrapperList.add(orwrapper);
    ctx.listofOrderWrapper = oWrapperList;

    system.debug('ctx.listofOrderWrapper ' + ctx.listofOrderWrapper);


    Test.startTest();
    localObj.removeOrderLineItem();
    Test.stopTest();

    List<orderitem> oitems = [select id from Orderitem where id=:oitem.id];

    System.AssertEquals(true,oitems.size() == 0);
    
}
static testMethod void hasNoPackageGoodsPaymentTerm_PositiveScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getConsignmentPackagedOrder();
    ord.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_PACKAGED;
    update ord;
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.onLoadActionForNewOrder();
    ctx.invoicedAccount.EP_Package_Payment_Term__c = null;
    Test.startTest();
    Boolean result = localObj.hasNoPackageGoodsPaymentTerm();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void hasNoPackageGoodsPaymentTerm_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ord.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK;
    update ord;
    Test.startTest();
    Boolean result = localObj.hasNoPackageGoodsPaymentTerm();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void doSubmitActionforNewOrder_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    localObj.doSubmitActionforNewOrder();
    Test.stopTest();
    
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void setOrderItemsinDomain_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    localObj.setOrderItemsinDomain();
    Test.stopTest();

    System.AssertEquals(true,ctx.orderDomainObj.getOrderItems().size() == ord.csord__Order_Line_Items__r.size());
    
}
static testMethod void updateOrder_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ord.EP_Payment_Term__c = EP_Common_Constant.PREPAYMENT;
    update ord;
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
    orderEvent.setEventMessage(EP_OrderConstant.OVERDUE_MESSAGE);
    
    Test.startTest();
    PageReference result = localObj.updateOrder(orderEvent);
    Test.stopTest();
    
    System.AssertEquals(true,ctx.newOrderRecord.EP_Payment_Term__c == EP_Common_Constant.PREPAYMENT);
    
}
static testMethod void getOrderWapperforIndex_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    csord__Order_Line_Item__c oitem = (ord.csord__Order_Line_Items__r)[0];
    EP_OrderPageContext.OrderWrapper orWrapper = new EP_OrderPageContext.OrderWrapper();
    orwrapper.oliIndex = 0;
    orwrapper.orderLineItem = oitem;
    List<EP_OrderPageContext.OrderWrapper> oWrapperList = new List<EP_OrderPageContext.OrderWrapper>();
    oWrapperList.add(orwrapper);
    ctx.listofOrderWrapper = oWrapperList;

    String orderLineItemIndex = '0';

    Test.startTest();
    EP_OrderPageContext.OrderWrapper result = localObj.getOrderWapperforIndex(orderLineItemIndex);
    Test.stopTest();
    
    System.AssertEquals(orwrapper.oliIndex,result.oliIndex);

}
static testMethod void setRelatedBulkOrderDetailsonPackagedOrder_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    localObj.setRelatedBulkOrderDetailsonPackagedOrder();
    Test.stopTest();
    
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void hasInValidDatesOnROOrder_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.hasInValidDatesOnROOrder();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void hasInValidDatesOnROOrder_RetroPositiveScenariotest() {
    //give retrospective order but not consumption order
    csord__Order__c ord = EP_TestDataUtility.getRetrospectiveNegativeScenario();
    ord.Status__c =EP_OrderConstant.OrderState_Draft;
    ord.EP_Order_Date__c = null;
    ord.EP_Loading_Date__c = null;
    ord.EP_Expected_Delivery_Date__c = null;
    update ord;
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.hasInValidDatesOnROOrder();
    Test.stopTest();
    System.AssertEquals(true,result);
}

static testMethod void hasInValidDatesOnROOrder_ConsumptionNegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.hasInValidDatesOnROOrder();
    Test.stopTest();
    System.AssertEquals(false,result);
}

static testMethod void setPageVariablesforModificationController_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    localObj.setPageVariablesforModificationController();
    Test.stopTest();
    
    System.AssertEquals(true,ctx.strSelectedPickupLocationID == ord.Stock_Holding_Location__c);
    
}
static testMethod void setOrderStatustoCancelled_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    PageReference result = localObj.setOrderStatustoCancelled();
    Test.stopTest();

    system.assertEquals(true,ctx.newOrderRecord.Status__c == EP_Common_Constant.CANCELLED_STATUS);
}
static testMethod void saveOrderAsDraft_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    PageReference result = localObj.saveOrderAsDraft();
    Test.stopTest();

    System.AssertEquals(true,ctx.newOrderRecord.Status__c == EP_Common_Constant.ORDER_DRAFT_STATUS);
    system.assertEquals(true, result != null);
}
static testMethod void getpricing_Positivetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ord.EP_Pricing_Status__c = 'PRICED';
    ctx.pricingPollerCount = 1;
    update ord;
    Test.startTest();
    localObj.getpricing();
    Test.stopTest();

    System.AssertEquals(true,ctx.isPricingRecieved);

}

static testMethod void getpricing_NegativePollerCounttest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ord.EP_Pricing_Status__c = 'PRICED';
    ctx.pricingPollerCount = 0;
    update ord;

    Test.startTest();
    localObj.getpricing();
    Test.stopTest();
    
    System.AssertEquals(true,ctx.pricingPollerCount == 1);
}

static testMethod void getpricing_Negativetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    ctx.pricingPollerCount = 1;
    ord.EP_Pricing_Status__c = 'FAILURE';
    ord.EP_Error_Description__c = 'error';
    update ord;

    Test.startTest();
    localObj.getpricing();
    Test.stopTest();

    System.AssertEquals(true,ctx.newOrderRecord.EP_Error_Description__c == 'error');

}

static testMethod void doUpdateSupplierContracts_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    PageReference pagRef = Page.EP_PortalOrderPage;
    Test.startTest();
    Test.setCurrentPage(pagRef);
    pagRef.getParameters().put(EP_Common_Constant.INT_OLIINDEX,'3');
    localObj.doUpdateSupplierContracts();
    Test.stopTest();

    // Void method does not return anything
    //System.AssertEquals(true,<asset conditions>);

}

static testMethod void ValidateLoadingdate_positivetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    csord__Order_Line_Item__c oitem = (ord.csord__Order_Line_Items__r)[0];
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);

    Orderitem orderItem = [select id,PricebookEntryId,EP_3rd_Party_Stock_Supplier__c from Orderitem where id = :oitem.id limit 1];
    EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
    orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c = orderitem.EP_3rd_Party_Stock_Supplier__c;
    orderWrapperObj.oliPricebookEntryID = orderitem.PricebookEntryId;

    Test.startTest();
    localObj.ValidateLoadingdate(orderWrapperObj);
    Test.stopTest();
    
    // method not returning, hence validating contract options in a different test method.
   // System.AssertEquals(true,orderWrapperObj.contractOptions != null);
    //System.AssertEquals(true,orderWrapperObj.oliProductName != null);
}

static testMethod void ValidateLoadingdate_negativetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.ValidateLoadingdate(orderWrapperObj);
    Test.stopTest();
    
    System.AssertEquals(true,orderWrapperObj.contractOptions == null);
    
}
static testMethod void setTankDetailsonWrapper_positivetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    csord__Order_Line_Item__c orditem = ord.csord__Order_Line_Items__r[0];
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();

    Account acc = [select id from Account where id=:ord.EP_ShipTo__c];
    EP_Tank__c tankobj = EP_TestDataUtility.createTestEP_Tank(acc);
    //tankObj.EP_Tank_Status__c='Operational';
    tankObj.EP_Tank_Alias__c = 'Diesel tank';
    update tankobj;
    EP_Tank__c tank = [select id,EP_Product__r.Name,EP_Safe_Fill_Level__c,EP_Tank_Code__c,EP_Tank_Alias__c from EP_Tank__c where id=:tankobj.id];
    orderWrapperObj.oliTanksID = tankobj.id;


    map<id,EP_Tank__c> maptanks = new map<id,EP_Tank__c>();
    maptanks.put(tank.id, tank);
    ctx.mapShipToOperationalTanks = maptanks;
    ctx.isOprtnlTankAvail = true;
    Test.startTest();
    localObj.setTankDetailsonWrapper(orderWrapperObj);
    Test.stopTest();
    
    system.assertEquals(orderWrapperObj.oliTankProductName, tank.EP_Product__r.Name);
    system.assertEquals(orderWrapperObj.oliTankSafeFillLvl, Integer.valueOf(tank.EP_Safe_Fill_Level__c));
}

static testMethod void setTankDetailsonWrapper_negativetest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    csord__Order_Line_Item__c orditem = ord.csord__Order_Line_Items__r[0];
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    EP_OrderPageContext.OrderWrapper orderWrapperObj = new EP_OrderPageContext.OrderWrapper();
    
    Account acc = [select id from Account where id=:ord.EP_ShipTo__c];
    EP_Tank__c tankobj = EP_TestDataUtility.createTestEP_Tank(acc);
    
    Test.startTest();
    localObj.setTankDetailsonWrapper(orderWrapperObj);
    Test.stopTest();
    system.debug(orderWrapperObj.oliTanksName);
    // Returns null when tank id is null
    // System.AssertEquals(true,orderWrapperObj.oliTanksName == tankobj.EP_Tank_Code__c);
    // **** TILL HERE ~@~ *****
}
static testMethod void doCancel_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    PageReference result = localObj.doCancel();
    List<csord__Order__c> ordList = [select Id from csord__Order__c where id=:ord.id];
    Test.stopTest();
    
    System.AssertEquals(true,ordList.size() == 0);
    
}
static testMethod void doCalculatePrice_test() {
    Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
    Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.strSelectedAccountID = ord.AccountId__c;
    ctx.strSelectedPricebookID = ord.Pricebook2Id__c;
    ctx.strSelectedRoute = ord.EP_Route__c;
    ctx.strSelectedRun = ord.EP_Run__c;

    EP_OrderControllerHelper localObj = new EP_OrderControllerHelper(ctx);
    Test.startTest();
    localObj.doCalculatePrice();
    Test.stopTest();
    // the flag is set true after pricing response
     System.AssertEquals(true,ctx.isPriceCalculated);
}
}