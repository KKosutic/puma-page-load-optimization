/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_ProductListTest {

    private static string PBID = 'pbid';
    private static string COMPID = 'cmpnyId';
    private static string ACCID = 'accId';
    private static string TYPE = 'type';
    private static string RETURN_TYPE = 'returl';
    private static string OPERATION = 'op';
    private static string UPDATE_OPERATION = 'UPDATE';
    private static string NEW_OPERATION = 'NEW';
    private static string RECORD_TYPE = 'rtype'; 
    private static string RECORD_TYPE_NAME = 'recTypeName';
    private static final string CRNCY_ID = 'crncyId';

    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    private static  PricebookEntry pbEntryPetrol;
    private static Product2 productPetrolObj;
    
    private static string TANKID = 'tankId';
    private static string TANKLABEL = 'tankLabel';
    
    
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static set<String> profileName = new set<String>{EP_Common_Constant.SM_AGENT_PROFILE,EP_Common_Constant.ADMIN_PROFILE
                                                            ,EP_Common_Constant.TM_AGENT_PROFILE
                                                            ,EP_Common_Constant.CSC_AGENT_PROFILE, EP_Common_Constant.LOGISTICS_PROFILE
                                                            ,EP_Common_Constant.PRICING_PROFILE
                                                            ,EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE
                                                            ,EP_Common_Constant.CREDIT_PROFILE};
                                                    
    private static set<String> userRole = new  set<String>{'EP_SM_TM_Team_AU', 
                                                           'EP_Internal_Review_Team_AU'};   

    private static Map<String, Profile> profileMap = new Map<String, Profile>();
    private static Map<String, UserRole> userRoleMap = new Map<String, UserRole>();
    
    private static Map<String, User> userMap ;
    Id pricebookId;    
    
    @testSetUp
    static void dataSetup(){
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        userMap  = new Map<String, User>();
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        insert country;
        Company__c comp = EP_TestDataUtility.createCompany('EPUMA');
        comp.EP_BSM_GM_Review_Required__C = true;
        insert comp;
        profileMap = new Map<String, Profile>();
        for(Profile p : [Select id , Name from Profile
                            Where Name in:profileName ]){
            profileMap.put(p.Name,p);
        }
        List<database.SaveResult> accSaveResults  = new List<database.SaveResult>();
        List<account> accounts = new List<account>();
        User SMTMuser = EP_TestDataUtility.createUser(profileMap.get('Puma SM Agent_R1').id);
        User adminUser = EP_TestDataUtility.createUser(profileMap.get('System Administrator').id);
        User cscUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.CSC_AGENT_PROFILE).id); 
        User logisticsUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.LOGISTICS_PROFILE).id);
        User pricingUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.PRICING_PROFILE).id);
        userMap.put('Puma SM Agent_R1', SMTMuser );
        userMap.put('System Administrator', adminUser );
        userMap.put(EP_Common_Constant.CSC_AGENT_PROFILE, cscUser );
        userMap.put(EP_Common_Constant.PRICING_PROFILE, pricingUser );
        userMap.put(EP_Common_Constant.LOGISTICS_PROFILE, logisticsUser );
        insert userMap.values();
        
        //Insert Custom PriceBook and Product
        Pricebook2 customPB = new Pricebook2(Name=EP_Common_Constant.STANDARD_PRICE_BOOK_NAME, isActive=true);
        customPB.EP_Company__c = comp.id;
        insert customPB;
            
        productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = TRUE,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,
                                                EP_Company_Lookup__c = comp.id);                           
        insert productPetrolObj;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        
        PricebookEntry pbEntryPetrol11 = new PricebookEntry(
        Pricebook2Id = customPB.Id,EP_Is_Sell_To_Assigned__c = true, Product2Id = productPetrolObj.Id,
        UnitPrice = 12000, IsActive = true);
        //database.insert(pbEntryPetrol,false);
         
        List<PricebookEntry> pbeList1 = new List<PricebookEntry>();
        pbeList1.add(pbEntryPetrol11);
        product2 productPetrolObj1 = new product2(Name = 'petrol1', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = true,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,
                                                EP_Company_Lookup__c = comp.id,EP_Unit_of_Measure__c = 'HL'                                                );                           
        insert productPetrolObj1;
        
        
        PricebookEntry pbEntry1= new PricebookEntry(
        Pricebook2Id = customPB.Id,EP_Is_Sell_To_Assigned__c = true, Product2Id = productPetrolObj1.Id,
        UnitPrice = 12000, IsActive = true);
        pbeList1.add(pbEntry1);
        
        product2 productPetrolObj2 = new product2(Name = 'petrol2', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = true,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,EP_Company_Lookup__c = comp.id
                                                ,EP_Unit_of_Measure__c = 'LT');                           
        insert productPetrolObj2;
       
        
        //database.insert(pbeList);
        PricebookEntry pbEntry2= new PricebookEntry(
        Pricebook2Id = customPB.Id, EP_Is_Sell_To_Assigned__c = true, 
        Product2Id = productPetrolObj2.Id,
        UnitPrice = 12000, IsActive = true);
        //pbeList1.add(pbEntry2);
        insert pbeList1;
        EP_Region__c region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        insert region;
        
        Account  sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        sellToAccount.EP_Puma_Company__c = comp.id;
        sellToAccount.ownerID = userMap.get('Puma SM Agent_R1').id;
        sellToAccount.EP_Requested_Payment_Terms__c='PrePayment';
        sellToAccount.EP_Requested_Packaged_Payment_Term__c='PrePayment';
        sellToAccount.EP_Alternative_Payment_Method__c  = 'Cash Payment';
        insert sellToAccount;
        
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true];
 
        Account storageLoc = EP_TestDataUtility.createStorageLocAccount(country.Id,bhrs.Id);
        Account TransporterRec = EP_TestDataUtility.createTestVendor('Transporter','testNAV','AUN1');
        Account SupplierRec = EP_TestDataUtility.createTestVendor('3rd Party Stock Supplier','testNAV','AUN');
        
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;

        Contact  contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);  
        Contract contractRec = EP_TestDataUtility.createContract(storageLoc.Id,comp.Id,paymentTerm.Id,storageLoc);
        
        ID sellToSHLRTID = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Stock_Holding_Location__c', 'Ex_Rack_Supply_Location');
        //ID shipToToSHLRTID= EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Stock_Holding_Location__c', 'Delivery_Supply_Location');
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,sellToSHLRTID); //Add one SHL
        SHL1.EP_Supplier__c = SupplierRec.id;
        SHL1.EP_Purchase_Contract__c = contractRec.Id;
        SHL1.EP_Is_Pickup_Enabled__c= true;
        SHL1.EP_Supplier_Contract_Advice__c='None';
        SHL1.EP_Use_Managed_Transport_Services__c = 'N/A';
        SHL1.EP_Transporter__c = TransporterRec.Id ;
        SHL1.EP_Duty__c = 'Excise Paid';
        //system.debug('--SHL1.EP_Transporter__c--'+ SHL1.EP_Transporter__c);
        Database.saveResult s12 = Database.insert(SHL1);
       // system.assertEquals('', s12.getErrors()[0].getMessage());
        system.debug('--s12--'+ s12);
        
        system.assert(s12.isSuccess());
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status to basic data setup
        sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
        sellToAccount.EP_PriceBook__c = customPB.id;
       
        Database.saveResult sv = Database.update(sellToAccount);
        //Create Bank Acc
        system.debug(sv+'selllllllllllllllll'+sellToAccount.EP_PriceBook__c);
        EP_Bank_Account__c bankAcc  = EP_TestDataUtility.createBankAccount(sellToAccount.id);
        for(UserRole r : [Select id,developerName  from UserRole where developerName in: userRole]){
            userRoleMap.put(r.developerName,r);
        }
        
        //create ship to
        Account nonVMIshipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,shpToRecordTypeId);
        nonVMIshipTo.EP_Country__c = country.id;
        nonVMIshipTo.ownerID = userMap.get('Puma SM Agent_R1').id;
        nonVMIshipTo.EP_Transportation_Management__c = 'Puma Planned';
        nonVMIshipTo.EP_Eligible_for_Rebate__c = true;
        nonVMIshipTo.EP_Ship_To_Type__c = 'Consignment';
        insert nonVMIshipTo;
        //create SHL
        EP_Stock_holding_location__c SHL2 = EP_TestDataUtility.createShipToStockLocation(nonVMIshipTo.id,true,null,sellToSHLRTID); //Add one SHL
        insert SHL2;
        //create tank
        EP_Tank__c tank1 = EP_TestDataUtility.createTestEP_Tank(nonVMIshipTo.id, productPetrolObj.id);
        
        Account vmiShipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
        vmiShipTo.RecordTypeId =  EP_Common_Util.fetchRecordTypeId('Account','VMI Ship To');
        vmiShipTo.EP_Country__c = country.id;
        vmiShipTo.ownerID = userMap.get('Puma SM Agent_R1').id;
        vmiShipTo.EP_Transportation_Management__c = 'Puma Planned';
        vmiShipTo.EP_Eligible_for_Rebate__c = false;
        vmiShipTo.EP_Puma_company__c =  comp.id;
        vmiShipTo.EP_Ship_To_Type__c = 'Non-Consignment';
        insert vmiShipTo;
        //Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        //create tank
        EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(vmiShipTo.id, productPetrolObj.id);
        database.insert(new List<EP_Tank__C>{tank1,tank});
        //Update ship to stats to basic data setup
        vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        nonVMIshipTo.EP_Status__c  =    EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        vmiShipTo.EP_Status__c  =   EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        
        sellToAccount.EP_Status__c  =   EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        accounts = new List<account>{sellToAccount};//,nonVMIshipTo, vmiShipTo
        update accounts;
    }
    
    static testMethod void addProductListTest(){
        userMap = new Map<String, User>();
        
       
        for(User u : [Select id, Name, Profile.Name from User Where Profile.Name in :profileName]){
          userMap.put(u.Profile.Name, u);
        }
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        Map<String, Account> accountMap = New Map<String, Account>();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c 
                                  from Account order by CreatedDate ASC limit 3 ]){
            
            acc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
        update accountMap.values();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c 
                                  from Account order by CreatedDate ASC limit 3 ]){
            
            
            
            acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
        
        EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;

        update accountMap.values();
        List<EP_Action__c> actions ;
        EP_ChangeRequest__c Reqsts;
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        system.runAs(userMap.get(EP_Common_Constant.SM_AGENT_PROFILE)){
        Test.startTest();
            PageReference pageRef = Page.EP_AddProductPage;
            Test.setCurrentPage(pageRef);
            //Set Sell To apage Variables 
            ApexPages.currentPage().getParameters().put(PBID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_PriceBook__c);
            ApexPages.currentPage().getParameters().put(COMPID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_Puma_Company__c);
            ApexPages.currentPage().getParameters().put(ACCID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id);
            ApexPages.currentPage().getParameters().put(CRNCY_ID , 'GBP');
            EP_AddProductController controller = new EP_AddProductController();
            
            Integer count;
            set<id> productSet = new set<id>(); 
            system.debug('====='+controller.wrapperList);
            system.assert(controller.wrapperList.size()>0);
            for(EP_AddProductController.WrapperProduct wr: controller.wrapperList){
                if(wr.iSSelect){
                     wr.isSelect = false;
                }else{
                    wr.isSelect = true;
                }
            }
            controller.addProduct();
            Id productID = [Select id, Name from product2 where name='petrol1' limit 1].id;
                
            LIST<EP_ChangeRequest__c> crList = [Select id, Name , EP_Account__c, EP_Request_Status__c, 
                                                    (Select Id, EP_New_Value__c, EP_Review_Step__c,
                                                        EP_Action__c, EP_Original_Value__c,EP_Source_Field_API_Name__c,
                                                        EP_Request_Status__c
                                                    from  Change_Request_Lines__r),
                                                    (Select id, Name, EP_Action_Name__c, EP_Request_Id__c,EP_Status__c,
                                                             OwnerID, EP_Record_Type_Name__c
                                                     from Actions__r) 
                                                From EP_ChangeRequest__c 
                                                where EP_Account__c=:accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id];    
       
           for(EP_AddProductController.WrapperProduct wr: controller.wrapperList){
                    if(productID == wr.product.id){
                         wr.isSelect = false;
                         //productSet.add(productID);
                    }else{
                        wr.isSelect = true;
                    }
                    
            }
            controller.addProduct();
            test.stopTest();
            
            LIST<EP_ChangeRequest__c> crList1 = [Select id, Name , EP_Account__c, EP_Request_Status__c, 
                                                    (Select Id, EP_New_Value__c, EP_Review_Step__c,
                                                        EP_Action__c, EP_Original_Value__c,EP_Source_Field_API_Name__c,
                                                        EP_Request_Status__c
                                                    from  Change_Request_Lines__r),
                                                    (Select id, Name, EP_Action_Name__c, EP_Request_Id__c,EP_Status__c,
                                                             OwnerID, EP_Record_Type_Name__c
                                                     from Actions__r) 
                                                From EP_ChangeRequest__c 
                                                where EP_Account__c=:accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id];    
       
        
           system.assert(crList.isEmpty());
           
           system.assert(!crList1.isEmpty());
           system.assert(!crList1[0].Actions__r.isEmpty());
           
           system.assert('Pricing Review'.equalsIgnoreCase(crList1[0].Actions__r[0].EP_Action_Name__c));
           for(EP_ChangeRequestLine__c crl: crList1[0].Change_Request_Lines__r){
                
                if('Remove'.equalsIgnoreCase(crl.EP_New_Value__c)){
                    system.assertEquals(null,crl.EP_Action__c );
                }else if('Add'.equalsIgnoreCase(crl.EP_New_Value__c)){
                    system.assertEquals(crList1[0].Actions__r[0].id,crl.EP_Action__c );
                }
            }
       }
   }  
    
   static testmethod void updatePricebookByPricingUser(){
        userMap = new Map<String, User>();
        
        for(User u : [Select id, Name, Profile.Name from User Where Profile.Name in :profileName]){
          userMap.put(u.Profile.Name, u);
        }
        Map<String, Account> accountMap = New Map<String, Account>();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c 
                                  from Account limit 3]){
            acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
        system.runAs(userMap.get(EP_Common_Constant.PRICING_PROFILE)){
            Test.startTest();
            PageReference pageRef = Page.EP_AddProductPage;
            Test.setCurrentPage(pageRef);
            //Set Sell To apage Variables 
            ApexPages.currentPage().getParameters().put(PBID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_PriceBook__c);
            ApexPages.currentPage().getParameters().put(COMPID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_Puma_Company__c);
            ApexPages.currentPage().getParameters().put(ACCID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id);
            ApexPages.currentPage().getParameters().put(CRNCY_ID , 'GBP');
            EP_AddProductController controller = new EP_AddProductController();
            system.assertEquals(false, controller.noProductListFlag);
            //controller.wrapperList();
            controller.next();
            
            controller.first();
            controller.last();
            controller.Previous();
            controller.refreshPageSize();
            controller.getallowedPBModification();
            controller.getcurentAccount();
            controller.doCancel();
            test.stopTest();
            
            //system.assertEquals(false, controller.getallowedPBModification());    
        }
    }
    
    static testmethod void removeProductOnInactiveAccTest(){
        userMap = new Map<String, User>();
        
        for(User u : [Select id, Name, Profile.Name from User Where Profile.Name in :profileName]){
          userMap.put(u.Profile.Name, u);
        }
        Map<String, Account> accountMap = New Map<String, Account>();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c 
                                  from Account limit 3]){
            //acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
        system.runAs(userMap.get(EP_Common_Constant.CSC_AGENT_PROFILE)){
            Test.startTest();
            PageReference pageRef = Page.EP_AddProductPage;
            Test.setCurrentPage(pageRef);
            //Set Sell To apage Variables 
            ApexPages.currentPage().getParameters().put(PBID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_PriceBook__c);
            ApexPages.currentPage().getParameters().put(COMPID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_Puma_Company__c);
            ApexPages.currentPage().getParameters().put(ACCID, accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id);
            ApexPages.currentPage().getParameters().put(CRNCY_ID , 'GBP');
            EP_AddProductController controller = new EP_AddProductController();
            Id productID = [Select id, Name from product2 where name='petrol1' limit 1].id;
            set<id> productSet = new set<id>();
            for(EP_AddProductController.WrapperProduct wr: controller.wrapperList){
                if(productID == wr.product.id){
                         wr.isSelect = false;
                    }else{
                        wr.isSelect = true;
                    }
                    productSet.add(wr.product.id);
            }
            controller.addProduct();
            test.stopTest();
            LIST<EP_ChangeRequest__c> crList1 = [Select id, Name , EP_Account__c, EP_Request_Status__c, 
                                                    (Select Id, EP_New_Value__c, EP_Review_Step__c,
                                                        EP_Action__c, EP_Original_Value__c,EP_Source_Field_API_Name__c,
                                                        EP_Request_Status__c
                                                    from  Change_Request_Lines__r)
                                                    From EP_ChangeRequest__c 
                                                    where EP_Account__c=:accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).id];    
            
            PriceBookEntry pb = [select id,isActive from PriceBookEntry 
                                    where PriceBook2Id =:accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_PriceBook__c 
                                    AND Product2Id =:productID];
                                    
            system.assertEquals(false, pb.isActive); 
            Id productID2 = [Select id, Name from product2 where name='petrol2' limit 1].id;
            system.assert(productSet.contains(productID2));   
            /*PriceBookEntry pb1 = [select id,isActive from PriceBookEntry 
                                    where PriceBook2Id =: accountMap.get(EP_Common_Constant.SELL_TO_DEV_NAME ).EP_PriceBook__c 
                                    AND Product2.Name =: 'petrol2'];
            system.assertEquals(true,pb1.isActive);   */                  
        }
    }
    
    static testMethod void tankProductUomTest(){
        userMap = new Map<String, User>();
        
        for(User u : [Select id, Name, Profile.Name from User Where Profile.Name in :profileName]){
          userMap.put(u.Profile.Name, u);
        }
        Map<String, Account> accountMap = New Map<String, Account>();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c, 
                                  (Select id,Name From Tank__r)from Account limit 10]){
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
        system.debug('===accountMap======11111111111111111=');
        system.runAs(userMap.get(EP_Common_Constant.CSC_AGENT_PROFILE)){system.debug('===accountMap======22222222221=');
            Test.startTest();
            PageReference pageRef = Page.EP_Tank_Product_Page;
            Test.setCurrentPage(pageRef);
            //Set Sell To apage Variables
            //system.assertEquals('--', string.valueOf(accountMap));
            //system.debug('===accountMap======='+ accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME)); 
            ApexPages.currentPage().getParameters().put('Id', accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME).Tank__r[0].id);
            EP_TankDetailPageController controller = new EP_TankDetailPageController(new ApexPages.StandardController(accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME).Tank__r[0]));
            controller.fetchProductValues();
            //system.assert(!controller.ProductValues.isEmpty());
        }
    } 
    
    
    static testmethod void testUniquePriceBookNameValidation(){
    	List<PriceBook2> pricebookList = new List<PriceBook2>([Select id, Name from Pricebook2 Limit 1]);
    	
    	Pricebook2 clonedPricebook = pricebookList[0].clone();
    	// Insert Test with same name 
    	database.saveResult saveResult = database.insert(clonedPricebook, false);
    	system.assert(!saveResult.isSuccess());
    	//Change Name and again try to insert
    	clonedPricebook.Name = pricebookList[0].Name +'Cloned';
    	// pricebook should insert successfully
    	saveResult = database.insert(clonedPricebook, false);
    	system.assert(saveResult.isSuccess());
    	
    	// Update Operation test
    	
    	clonedPricebook.Name = pricebookList[0].Name;
    	saveResult = database.update(clonedPricebook, false);
    	system.assert(!saveResult.isSuccess());
    	
    }
}