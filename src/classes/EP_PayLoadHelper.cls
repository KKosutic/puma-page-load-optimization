/**
   @Author          CR Team
   @name            EP_PayLoadHelper 
   @CreateDate      01/13/2016
   @Description     This class contains methods which are generic to payload classes
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_PayLoadHelper {

        EP_PaymentTermMapper paymentTermMapper = null;
        EP_ObjectFieldNodeSettingMapper objectFieldNodeSettingMapper = null;
        private static map<String,map<String,map<String,String>>> mObjectFieldNode;
        private static map<String,map<String,map<String,set<String>>>> mParentNodeField;
    //String externalSystem;
    private static string EXTERNAL_SYSTEM_NAME = null;
    
    /**
    *  Constructor. 
    *  @name            EP_PayLoadHelper 
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_PayLoadHelper () {
        paymentTermMapper = new EP_PaymentTermMapper();
        objectFieldNodeSettingMapper = new EP_ObjectFieldNodeSettingMapper();
    }
    
    /**
    *  This method creates query string and list of fields' API names based on Object API name and fieldset provided as input
    *  @name createFieldsWrapper
    *  @param String ObjectAPIName,String fieldSetName
    *  @return WrapperFields
    *  @throws Generic Exception
    */ 
    public WrapperFields createFieldsWrapper(String ObjectAPIName,String fieldSetName){
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','createFieldsWrapper');
        List<String> lFieldAPINames = new List<String>();
        String query;
        String fieldString = EP_Common_Constant.BLANK;
        try{
            lFieldAPINames = new List<String>();
            query= EP_Common_Constant.SELECT_STRING;
            //create query adding all fields in the specified fieldset for the specified object
            for(FieldSetMember fsm : EP_Common_Util.getFieldSetMember(ObjectAPIName, fieldSetName)){
                lFieldAPINames.add(fsm.getFieldPath());
                fieldString += EP_Common_Constant.SPACE + fsm.getFieldPath() + EP_Common_Constant.COMMA_STRING;
                
            }
            fieldString = fieldString.removeEnd(EP_Common_Constant.COMMA_STRING);
            query += fieldString
            + EP_Common_Constant.FROM_STRING
            + ObjectAPIName ;
            
        }
        catch(Exception handledException){
            throw handledException;
        }
        System.debug('$$$$$lFieldAPINames$ = '+lFieldAPINames);
        System.debug('$$$$$lFieldAPINames$ = '+query);
        System.debug('$$$$$lFieldAPINames$ = '+fieldString);
        return new WrapperFields(lFieldAPINames
            ,query
            ,fieldString);

    }

    /**
    *  TEMPORARY FIX TO SEND PAYMENT TERM CODE AGAINST  PAYMENT TERM DESCRIPTION
    *  @name createPaymentTermCodeMap
    *  @param NA
    *  @return String
    *  @throws NA
    */ 
    public map<String,String> createPaymentTermCodeMap(){
        map<String,String>mPaymentTerms = new map<String,String>();
        try {
            Integer nRows = EP_Common_Util.getQueryLimit();
            for(EP_Payment_Term__c paymentTerm : paymentTermMapper.getAllPaymentTerms(nRows)){
                mPaymentTerms.put(paymentTerm.name.toUpperCase(), paymentTerm.EP_Payment_Term_Code__c);
            }
            } catch (Exception e) {
                throw e;
            }  
            return mPaymentTerms;
        }

    /**
    *  TEMPORARY FIX TO SEND PAYMENT TERM CODE AGAINST  PAYMENT TERM DESCRIPTION
    *  @name mapPaymentTermCode
    *  @param Account customer, map<String,String> mPaymentTerms
    *  @return void
    *  @throws NA
    */ 
    public void mapPaymentTermCode(Account customer, map<String,String> mPaymentTerms){

        customer.EP_Requested_Payment_Terms__c = String.isNotBlank(customer.EP_Requested_Payment_Terms__c) && mPaymentTerms.containsKey(customer.EP_Requested_Payment_Terms__c.toUpperCase())
        ?mPaymentTerms.get(customer.EP_Requested_Payment_Terms__c.toUpperCase()):customer.EP_Requested_Payment_Terms__c;
        
        
        customer.EP_Requested_Packaged_Payment_Term__c = String.isNotBlank(customer.EP_Requested_Packaged_Payment_Term__c) && mPaymentTerms.containsKey(customer.EP_Requested_Packaged_Payment_Term__c.toUpperCase())
        ?mPaymentTerms.get(customer.EP_Requested_Packaged_Payment_Term__c.toUpperCase()):customer.EP_Requested_Packaged_Payment_Term__c;                                                    

    }

    /**
       @Author          Sandeep Kumar
       @name            WrapperFields
       @CreateDate      01/17/2017
       @Description     This wrapper binds together the list of fields' API names and query string
       @Version         1.0
       @reference       NA
       */
       public with sharing class WrapperFields{
        public List<String>lFieldAPINames{get;set;}
        public String query{get;set;}
        public String fieldString{get;set;}
        /*
         * Initializing fields
         */
         Public WrapperFields(List<String>lFieldAPINames
            ,String query
            ,String fieldString){

            this.lFieldAPINames = lFieldAPINames;
            this.query = query;
            this.fieldString = fieldString;
        }                   
    }
    
    
    /**
    *  Query custom metadata for creating tags for request. Populating mObjectFieldNode and mParentNodeField map only.
    *  @name createObjectFieldNodeMap
    *  @param String externalSystemName
    *  @return void
    *  @throws Generic Exception
    */ 
    public void createObjectFieldNodeMap(String externalSystemName){
        EP_PayloadHelper.mObjectFieldNode = new map<String,map<String,map<String,String>>>();
        EP_PayloadHelper.mParentNodeField = new map<String,map<String,map<String,Set<String>>>>();
        map<String,map<String,String>> mRecordTypeFieldNode;
        map<String,String>mFieldNode;
        Set<String>sFieldName; 
        map<String,Set<String>>mIdentifierFields;
        map<String,map<String,Set<String>>>mRecordTypeParentNode;
        Integer nRows = EP_Common_Util.getQueryLimit();
        try{

          for(EP_ObjectFieldNodeSetting__mdt fieldNodeObj : objectFieldNodeSettingMapper.getRecordsByExternalSystem(externalSystemName, nRows)){            
            if(EP_PayloadHelper.mObjectFieldNode.containsKey(fieldNodeObj.EP_Object_API_NAME__c)){
              if(EP_PayloadHelper.mObjectFieldNode.get(fieldNodeObj.EP_Object_API_NAME__c).containsKey(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase())){
                EP_PayloadHelper.mObjectFieldNode.get(fieldNodeObj.EP_Object_API_NAME__c).get(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()).put(fieldNodeObj.EP_FIELD_API_NAME__c.toUpperCase()
                    ,fieldNodeObj.EP_NODE_NAME__c);
            } 
            else{
                mFieldNode = new map<String,String>();
                mFieldNode.put(fieldNodeObj.EP_FIELD_API_NAME__c.toUpperCase()
                    ,fieldNodeObj.EP_NODE_NAME__c);
                
                EP_PayloadHelper.mObjectFieldNode.get(fieldNodeObj.EP_Object_API_NAME__c).put(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()
                    ,mFieldNode);
            }
        }
        else{
          mFieldNode = new map<String,String>();
          mFieldNode.put(fieldNodeObj.EP_FIELD_API_NAME__c.toUpperCase()
            ,fieldNodeObj.EP_NODE_NAME__c);

          mRecordTypeFieldNode = new map<String,map<String,String>>();
          mRecordTypeFieldNode.put(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()
            ,mFieldNode);
          System.debug('#############'+fieldNodeObj.EP_Object_API_NAME__c);
          EP_PayloadHelper.mObjectFieldNode.put(fieldNodeObj.EP_Object_API_NAME__c
            ,mRecordTypeFieldNode);
      }

      if(String.isNotBlank(fieldNodeObj.EP_Parent_Node__c)){
        if(EP_PayloadHelper.mParentNodeField.containsKey(fieldNodeObj.EP_Object_API_NAME__c)){
            if(EP_PayloadHelper.mParentNodeField.get(fieldNodeObj.EP_Object_API_NAME__c).containsKey(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase())){
                if(EP_PayloadHelper.mParentNodeField.get(fieldNodeObj.EP_Object_API_NAME__c).get(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()).containsKey(fieldNodeObj.EP_Parent_Node__c)){
                    EP_PayloadHelper.mParentNodeField.get(fieldNodeObj.EP_Object_API_NAME__c).get(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()).get(fieldNodeObj.EP_Parent_Node__c).add(fieldNodeObj.EP_FIELD_API_NAME__c);
                }
                else{
                    sFieldName = new Set<String>();
                    sFieldName.add(fieldNodeObj.EP_FIELD_API_NAME__c);
                    EP_PayloadHelper.mParentNodeField.get(fieldNodeObj.EP_Object_API_NAME__c).get(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()).put(fieldNodeObj.EP_Parent_Node__c,sFieldName);
                }
            }
            else{
                sFieldName = new Set<String>();
                mIdentifierFields = new map<String,Set<String>>();
                sFieldName.add(fieldNodeObj.EP_FIELD_API_NAME__c);
                mIdentifierFields.put(fieldNodeObj.EP_Parent_Node__c,sFieldName);
                EP_PayloadHelper.mParentNodeField.get(fieldNodeObj.EP_Object_API_NAME__c).put(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase(),mIdentifierFields);
            }
        }
        else{
           sFieldName = new Set<String>();
           mIdentifierFields = new map<String,Set<String>>();
           mRecordTypeParentNode = new map<String,map<String,Set<String>>>();
           sFieldName.add(fieldNodeObj.EP_FIELD_API_NAME__c);
           mIdentifierFields.put(fieldNodeObj.EP_Parent_Node__c,sFieldName);
           mRecordTypeParentNode .put(fieldNodeObj.EP_RECORD_TYPE_NAME__c.toUpperCase()
               ,mIdentifierFields);
           EP_PayloadHelper.mParentNodeField.put(fieldNodeObj.EP_Object_API_NAME__c,mRecordTypeParentNode );
       }
   }
}
}
catch(Exception handledException){
    throw handledException;
}

}

    /**
    *  This method groups object and its set of ids
    *  @name populateObjectRecordMap
    *  @param String objectName, Id recordId
    *  @return void
    *  @throws Generic Exception
    */ 
    public void populateObjectRecordMap(String objectName, Id recordId){                                            
        try{
            if(EP_PayLoadHandler.mObjectRecordId.containsKey(ObjectName)){
                EP_PayLoadHandler.mObjectRecordId.get(ObjectName).add(recordId);
            }
            else{
             EP_PayLoadHandler.mObjectRecordId.put(objectName, new Set<Id>{recordId});
         }
         }catch(Exception handledException){
            throw handledException;
        }
    }
    
    /**
    *  populate PARENT CHILD MAP
    *  @name populateChildParentMap
    *  @param String recId,String parentId
    *  @return void
    *  @throws NA
    */ 
    public void populateChildParentMap(String recId,String parentId){
        if(EP_PayLoadHandler.mChildParent != null && String.isNotBlank(parentId)){
            EP_PayLoadHandler.mChildParent.put(recId, parentId);
        }        
    }


    /**
    *  METHOD FOR LOMOSOFT TANK EDIT PAYLOAD
    *  @name createNode
    *  @param XMLStreamWriter xmlSWriter, SObject sfObject, String fieldName, map<String,String>mFieldNode, String externalSystem
    *  @return void
    *  @throws Generic Exception
    */ 
    public void createNode(XMLStreamWriter xmlSWriter
        ,SObject sfObject
        ,String fieldName
        ,map<String,String>mFieldNode){
        try{
           System.debug(mfieldNode.get(fieldName.toUpperCase()));
           if(mfieldNode.containsKey(fieldName.toUpperCase())
            && mfieldNode.get(fieldName.toUpperCase()) != null){

               if(String.isBlank(String.ValueOf(sfObject.get(fieldName)))
                /**Uncommentng Code to send empty tag for distance**/
                || (EP_Common_Constant.LS_CUSTOMER.equalsIgnoreCase(EP_PayLoadHandler.EXTERNAL_SYSTEM)
                    && ( EP_Common_Constant.LS_NODE_DISTANCE.equalsIgnoreCase(mFieldNode.get(fieldName.toUpperCase()))
                        )

                    )
                ){
                System.debug(fieldName);
                xmlSWriter.writeEmptyElement(null
                    ,mfieldNode.get(fieldName.toUpperCase())
                    ,null);

            }
            else {
                        //ELSE CREATE AN ELEMENT WITH TEXT NODE AS VALUE OF THIS FIELD                    
                        xmlSWriter.writeStartElement(null
                            ,mfieldNode.get(fieldName.toUpperCase())
                            ,null);
                        if(EP_Common_Constant.DATE_TIME.equalsIgnoreCase(sfObject.getsobjectType()
                            .getDescribe().fields.getMap().get(fieldName).getDescribe().getType().name())){
                            xmlSWriter.writeCharacters(String.valueOf(((DateTime)sfObject.get(fieldName)).format(EP_PayLoadConstants.DATE_TIME_FORMAT)));
                            
                        }                    
                        
                        else if(EP_Common_Constant.EP_STATUS.equalsIgnoreCase(fieldName)
                            && EP_Common_Constant.LS_CUSTOMER.equalsIgnoreCase(EP_PayLoadHelper.EXTERNAL_SYSTEM_NAME)){
                            xmlSWriter.writeCharacters(EP_Common_Constant.STRING_NO_LS);
                        }
                        else {
                            xmlSWriter.writeCharacters(String.valueOf(sfObject.get(fieldName)));
                        }
                        XmlSWriter.writeEndElement();
                    }
                } 
            }

            catch(Exception handledException){
                throw handledException;
            }        
        }

    /**
    *  This method returns the XML payload with IDENTIFIER TAG
    *  @name createPayLoadWithIdentifier
    *  @param SObject sfObject, List<String>lFieldAPIName
    *  @return String
    *  @throws Generic Exception
    */ 
    public String createPayLoadWithIdentifier(SObject sfObject, List<String> lFieldAPIName){
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','createPayLoadWithIdentifier');
        if(EP_PayloadHelper.EXTERNAL_SYSTEM_NAME != EP_PayLoadHandler.EXTERNAL_SYSTEM) {
            createObjectFieldNodeMap(EP_PayLoadHandler.EXTERNAL_SYSTEM);
        }
        String xmlString = EP_Common_Constant.BLANK;
        String recordTypeName;
        String objectAPIName;
        Integer count;
        integer indexcount ;
        Boolean isRecordTypeDefined = false;
        XMLStreamWriter xmlSWriter = new XMLStreamWriter();
        map<String,String>mFieldNode;
        set<String>sParentNodeField = new set<String>();
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','createPayLoadWithIdentifier');
        try{
            objectAPIName = sfObject.getSObjectType().getDescribe().getName();
            isRecordTypeDefined = EP_Common_Util.checkIsRecordsDefined(objectAPIName);
            populateObjectRecordMap(objectAPIName, (Id)sfObject.get(EP_PayLoadConstants.IDFIELD));
            
            if(isRecordTypeDefined){
                recordTypeName = EP_Common_Util.fetchRecordTypeName(objectAPIName
                    ,(Id)sfObject.get(EP_PayLoadConstants.RECORDTYPEID));
                if(recordTypeName.equalsIgnoreCase(EP_Common_Constant.STORAGE_SHIP_TO)){
                    recordTypename = EP_Common_Constant.NON_VMI_SHIP_TO;
                }                                                    
                //REMOVE RECORD TYPE FROM LIST OF FIELDS
                if((new Set<String>(lFieldAPIName)).contains(EP_PayLoadConstants.RECORDTYPEID)){

                    for(string s : lFieldAPIName){

                        if(indexcount == null ){
                           indexcount = 0;
                       }
                       else{
                        indexcount ++;
                    }
                    if(EP_PayLoadConstants.RECORDTYPEID.equals(s)){
                        break;
                    }
                }
                if(indexcount != null){
                    lFieldAPIName.remove(indexcount);
                }
            }
        }
        System.debug(' --- Debug Map ----- ' +  mObjectFieldNode + ' --- ' + objectAPIName);
        mFieldNode = new map<String,String>();
        for(String fieldName :lFieldAPIName){
            if(isRecordTypeDefined
                && EP_PayloadHelper.mObjectFieldNode.get(objectAPIName).containsKey(recordTypeName.toUpperCase())
                && EP_PayloadHelper.mObjectFieldNode.get(objectAPIName).get(recordTypeName.toUpperCase()).containsKey(fieldName.toUpperCase())){
                mFieldNode.put( fieldName.toUpperCase()
                    ,EP_PayloadHelper.mObjectFieldNode.get(objectAPIName).get(recordTypeName.toUpperCase()).get(fieldName.toUpperCase()));
            }
            else{
                mFieldNode.put( fieldName.toUpperCase()
                    ,EP_PayloadHelper.mObjectFieldNode.get(objectAPIName).get(EP_Common_Constant.ALL).get(fieldName.toUpperCase()));
            }
        }

        sParentNodeField = new set<String>();
        if(!EP_PayloadHelper.mParentNodeField.isEmpty()
            && EP_PayloadHelper.mParentNodeField.containsKey(objectAPIName)){

            if(isRecordTypeDefined
                && EP_PayloadHelper.mParentNodeField.get(objectAPIName).containsKey(recordTypeName.toUpperCase())
                && EP_PayloadHelper.mParentNodeField.get(objectAPIName).get(recordTypeName.toUpperCase()).containsKey(EP_Common_Constant.IDENTIFIER)){
                sParentNodeField = EP_PayloadHelper.mParentNodeField.get(objectAPIName).get(recordTypeName.toUpperCase()).get(EP_Common_Constant.IDENTIFIER);

            }
            else if(EP_PayloadHelper.mParentNodeField.get(objectAPIName).containsKey(EP_Common_Constant.ALL)
                && EP_PayloadHelper.mParentNodeField.get(objectAPIName).get(EP_Common_Constant.ALL).containsKey(EP_Common_Constant.IDENTIFIER)){
                sParentNodeField = EP_PayloadHelper.mParentNodeField.get(objectAPIName).get(EP_Common_Constant.ALL).get(EP_Common_Constant.IDENTIFIER);
                }else{

                }
            }
            indexcount = 0;
            for(count = 0; count < lFieldAPIName.size(); count++){
             if(sParentNodeField.contains(lFieldAPIName[count])){

                 xmlString = XMLSWriter.getXMLString();
                 if(String.isNotBlank(xmlString)
                    && !xmlString.endsWith(EP_Common_Constant.ANG_RIGHT)){
                    xmlString += EP_Common_Constant.ANG_RIGHT_CLOSE;
                }
                xmlString += startTag(EP_Common_Constant.IDENTIFIER);

                XMLSWriter.close();
                XMLSWriter = new XMLStreamWriter();
                indexcount = count;
                break;
            }
            else{
                      // System.debug(lFieldAPIName[count]);
                      createNode(xmlSWriter
                         , sfObject
                         , lFieldAPIName[count]
                         , mFieldNode);

                  }
              }

              while(count < indexcount + sParentNodeField.size()
                 && count < lFieldAPIName.size()){
                 createNode(xmlSWriter
                     , sfObject
                     , lFieldAPIName[count]
                     , mFieldNode
                     );

                 count++;

             }
             if(xmlString.contains(startTag(EP_Common_Constant.IDENTIFIER))) {
                 xmlString += xmlSWriter.getXMLString();

                 if(!xmlString.endsWith(EP_Common_Constant.ANG_RIGHT)){
                    xmlString += EP_Common_Constant.ANG_RIGHT_CLOSE;
                }
                xmlString +=  endTag(EP_Common_Constant.IDENTIFIER);
                xmlSWriter.close();
                xmlSWriter = new XMLStreamWriter();
            }               
            while(count < lFieldAPIName.size()){
                createNode(xmlSWriter
                 , sfObject
                 , lFieldAPIName[count]
                 , mFieldNode
                 );
                count++;
            }
            if(String.isNotBlank(xmlSWriter.getXMLString() )){
             xmlString += xmlSWriter.getXMLString();

         }
         xmlSWriter.close();

         if(!xmlString.endsWith(EP_Common_Constant.ANG_RIGHT)){
            xmlString += EP_Common_Constant.ANG_RIGHT_CLOSE;
        }

    }
    catch(Exception handledException){
        throw handledException;
    }
    return xmlString;
}

    /**
    *  This method is used to create node, value element. Used by addElement method. 
    *  @name writeElement
    *  @param XmlStreamWriter xmlSWriter, String nodeName, String value
    *  @return void
    *  @throws NA
    */ 
    private void writeElement(XmlStreamWriter xmlSWriter, String nodeName, String value) {
        EP_GeneralUtility.Log('Private','EP_PayLoadHelper','writeElement');
        if(xmlSWriter != null) {
            if(String.isNotBlank(value)) {
                xmlSwriter.writeStartElement(null, nodeName, null);
                xmlSwriter.writeCharacters(value);
                xmlSwriter.writeEndElement();
                } else {
                    XmlSwriter.writeEmptyElement(null, nodeName, null);
                }
            }
        }

    /**
    *  This method creates node, value element for XML string
    *  @name addElement
    *  @param XmlStreamWriter xmlSWriter, Map<String, String> nodeValueMap, String nodeName
    *  @return void
    *  @throws NA
    */ 
    public void addElement(XmlStreamWriter xmlSWriter, Map<String, String> nodeValueMap, String nodeName) {
        if(xmlSWriter != null) {
            //nodeName = nodeName.toUpperCase();
            writeElement(xmlSWriter, nodeName, nodeValueMap.get(nodeName.toUpperCase()));
        }
    }

    /**
    *  METHOD to put value in nodeValueMap map. Objective of this method is to enter key in uppercase. 
    *  @name putValue
    *  @param Map<String, String> nodeValueMap, String key, String value
    *  @return void
    *  @throws NA
    */ 
    public void putValue(Map<String, String> nodeValueMap, String key, String value) {
        if(nodeValueMap != null && key != null) {
            nodeValueMap.put(key.toUpperCase(), value);
        }
    }

    /**
    *  returns <tagName>
    *  @name startTag
    *  @param String tagName
    *  @return String
    *  @throws NA
    */ 
    public String startTag(String tagName) {
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','startTag');
        return EP_Common_Constant.ANG_LEFT_OPEN + tagName + EP_Common_Constant.ANG_RIGHT;
    }

    /**
    *  returns </tagName>
    *  @name endTag
    *  @param map <Id, Id> mUpdateTanksShipTo
    *  @return String
    *  @throws NA
    */ 
    public String endTag(String tagName) {
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','endTag');
        return EP_Common_Constant.ANG_LEFT_CLOSE + tagName + EP_Common_Constant.ANG_RIGHT;
    }

    /**
    *  returns <tagName/>
    *  @name openingTag
    *  @param String tagName
    *  @return String
    *  @throws NA
    */ 
    public String openingTag(String tagName) {
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','openingTag');
        return EP_common_Constant.ANG_LEFT_OPEN+
        + tagName+
        + EP_common_Constant.ANG_RIGHT_CLOSE;
    }

    /**
    *  METHOD to populate objectChildTagMap, objectFields, windmsIntgRecList from EP_Order_Fieldset_Sfdc_Nav_Intg__c custom setting
    *  @name processIntegrationWindmsSetting
    *  @param String objectName, 
        Map < string, List < EP_Order_Fieldset_Sfdc_Windms_Intg__c >> objectChildTagMap, 
        set < String > objectFields, List < EP_Order_Fieldset_Sfdc_Windms_Intg__c > windmsIntgRecList
    *  @return void
    *  @throws Generic Exception
    */ 
    public void processIntegrationWindmsSetting(String objectName, 
        Map < string, List < EP_Order_Fieldset_Sfdc_Windms_Intg__c >> objectChildTagMap, 
        set < String > objectFields, List < EP_Order_Fieldset_Sfdc_Windms_Intg__c > windmsIntgRecList) {
        String fieldApiName = null;
        String thisObjName = null;
        String parentTag = null;
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','processIntegrationWindmsSetting');
        try {
            List < EP_Order_Fieldset_Sfdc_Windms_Intg__c > sfdcWindmsIntg = new List < EP_Order_Fieldset_Sfdc_Windms_Intg__c > ();
            EP_OrderFieldsetSfdcWindmsMapper orderFieldSetMapper = new EP_OrderFieldsetSfdcWindmsMapper();
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            for (EP_Order_Fieldset_Sfdc_Windms_Intg__c obj : orderFieldSetMapper.getOrderFieldsetSfdcWindmsRec(nRows)) {
                thisObjName = obj.Object_Name__c;
                if (thisObjName != Null && thisObjName.equalsIgnoreCase(objectName)) {
                    fieldApiName = obj.Field_API_Name__c;
                    parentTag = obj.Parent_Tag__c;

                    if (objectFields != null && fieldApiName != null) {
                        objectFields.add(fieldApiName.toLowerCase());
                    }
                    if(windmsIntgRecList != null) {
                        windmsIntgRecList.add(obj);
                    }
                    if (objectChildTagMap != null && string.isNotBlank(parentTag)) {
                        objectChildTagMap.put(parentTag, sfdcWindmsIntg);
                        if (!objectChildTagMap.containsKey(parentTag)) {
                            objectChildTagMap.put(parentTag.toLowerCase(), sfdcWindmsIntg);
                        }
                        objectChildTagMap.get(parentTag).add(obj);
                    }
                }
            }
            } catch (Exception e) {
                throw e;
            } 
        }

    /**
    *  METHOD to populate objectChildTagMap, objectFields, windmsIntgRecList from EP_Order_Fieldset_Sfdc_Nav_Intg__c custom setting
    *  @name processIntegrationNAVSetting
    *  @param String objectName, Map < string, List < EP_Order_Fieldset_Sfdc_Nav_Intg__c >> objectChildTagMap, 
              set < String > objectFields, List < EP_Order_Fieldset_Sfdc_Nav_Intg__c > windmsIntgRecList
    *  @return Void
    *  @throws Generic Exception
    */ 
    public void processIntegrationNAVSetting(String objectName, 
        Map < string, List < EP_Order_Fieldset_Sfdc_Nav_Intg__c >> objectChildTagMap, 
        set < String > objectFields, List < EP_Order_Fieldset_Sfdc_Nav_Intg__c > windmsIntgRecList) {
        String fieldApiName = null;
        String thisObjName = null;
        String parentTag = null;
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','processIntegrationNAVSetting');
        try {
            List < EP_Order_Fieldset_Sfdc_Nav_Intg__c > sfdcWindmsIntg = new List < EP_Order_Fieldset_Sfdc_Nav_Intg__c > ();
            EP_OrderFieldsetSfdcNavMapper orderFieldSetMapper = new EP_OrderFieldsetSfdcNavMapper();
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            for (EP_Order_Fieldset_Sfdc_Nav_Intg__c obj : orderFieldSetMapper.getOrderFieldsetSfdcNavRec(nRows)) {
                thisObjName = obj.Object_Name__c;
                if (thisObjName != Null && thisObjName.equalsIgnoreCase(objectName)) {
                    fieldApiName = obj.Field_API_Name__c;
                    parentTag = obj.Parent_Tag__c;

                    if (objectFields != null && fieldApiName != null) {
                        objectFields.add(fieldApiName.toLowerCase());
                    }
                    if(windmsIntgRecList != null) {
                        windmsIntgRecList.add(obj);
                    }
                    if (objectChildTagMap != null && string.isNotBlank(parentTag)) {
                        objectChildTagMap.put(parentTag, sfdcWindmsIntg);
                        if (!objectChildTagMap.containsKey(parentTag)) {
                            objectChildTagMap.put(parentTag.toLowerCase(), sfdcWindmsIntg);
                        }
                        objectChildTagMap.get(parentTag).add(obj);
                    }
                }
            }
            } catch(Exception e) {
                throw e;
            } 
        }

    /**
    *  Method to create generic soql query string
    *  @name getQuery
    *  @param String objectName, Set<String> fieldNames, String stringOfExtraFields, String whereCondition
    *  @return String
    *  @throws Generic Exception
    */ 
    public String getQuery(String objectName, Set<String> fieldNames, String stringOfExtraFields, String whereCondition) {
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','getQuery');
        String query = EP_Common_Constant.BLANK;
        try {
            string orditemQuery = EP_Common_Constant.BLANK;
            for (String fieldName: fieldNames) {
                query += EP_PayLoadConstants.COMMA + fieldName;
            }
            if(!String.IsBlank(stringOfExtraFields)) {
                query += stringOfExtraFields;
            }
            if (query != EP_Common_Constant.BLANK) {
                query = query.substring(1);
            }
            query = EP_PayLoadConstants.SELECT_QUERY + query + EP_Common_Constant.FROM_STRING + objectName;
            if(!String.IsBlank(whereCondition)) {
                query += EP_Common_Constant.WHERE_STRING + whereCondition;
            }
            } catch (Exception e) {
                throw e;
            }
            return query;
        }

    /**
    *  This method returns '' if object is null 
    *  @name getXMLValue
    *  @param Object value
    *  @return Object
    *  @throws NA
    */ 
    public Object getXMLValue(Object value) {
        EP_GeneralUtility.Log('Public','EP_PayLoadHelper','getXMLValue');
        return value != null? value: EP_Common_Constant.BLANK;
    }
}