public with sharing class EP_ASTSellToActiveToInActive  extends EP_AccountStateTransition {
    public EP_ASTSellToActiveToInActive() {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToInActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToInActive','isGuardCondition');
        //No guard conditions for Active to Inactive
        return true;
    }
}