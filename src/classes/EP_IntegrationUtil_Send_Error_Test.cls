/*   
     @Author <Sanchit Dua>
     @name <EP_IntegrationUtil_Test.cls>   
     @CreateDate <19/11/2015>   
     @Description <This class is used to cover the class EP_IntegrationUtil.>   
     @Version <1.1> 
     Revision updates:
     Added 3 methods for ok by jyotsna yadav
     Added 3 methods for error handling by <<Sanchit Dua>>
*/  
@isTest
public class EP_IntegrationUtil_Send_Error_Test {
    //static member variable to be used as test data
    private static Account acc;
    private static String txId;
    private static String msgId;
    private static String company;
    private static DateTime dt_sent; 
    private static final String TESTREC = 'test';
    private static final String SENTERROR = 'ERROR-SENT';
    private static final String SYNCERROR = 'ERROR-SYNC';
    private static final String INITIAL_VAL = '';
    
    private static final Integer LMT = Limits.getLimitQueries();
    
    /*
     * Description: creating the sample test data
     * @author: sanchit dua
    */
    private static void createSampleData(){
        EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
        insert country;
        acc = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
        BillingCountry='in',
        ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
        insert acc; 
        txId = EP_IntegrationUtil.getTransactionID(TESTREC, TESTREC);    
        msgId = EP_IntegrationUtil.getMessageId(TESTREC); 
        company = TESTREC;   
        dt_sent = DateTime.now();  
    }
     
       
    
    
    @isTest  
    /*
     * Description: Test method for testing the sendBulkError method defined in class EP_IntegrationUtil
     * @author: sanchit dua
    */ 
    static void sendBulkError_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            List<Id> objIdList = new List<Id>();
            EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
            insert country;
            Account acc1 = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
            BillingCountry='in', ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
            insert acc1; 
            objIdList.add(acc.Id); 
            objIdList.add(acc1.Id); 
            List<String> objTypeList = new List<String>{'Accounts', 'Accounts'};
            test.startTest();
            system.debug('the bulk records created are: '+EP_IntegrationUtil.sendBulkError(objIdList, objTypeList, String.valueOf(txId), String.valueOf(msgId), TESTREC, company, TESTREC, dt_sent));
            test.stopTest();
           
            List<EP_IntegrationRecord__c> intRecList = [Select EP_Status__c from EP_IntegrationRecord__c where EP_Object_ID__c IN :objIdList LIMIT: LMT];
            system.assertEquals(2,intRecList.size());
            system.assertEquals(SENTERROR,intRecList[0].EP_Status__c);
            system.assertEquals(SENTERROR,intRecList[1].EP_Status__c); 
        }        
    }
     
     
     /*
     * Description: Test method for testing the syncError method defined in class EP_IntegrationUtil
     * @author: sanchit dua
    */
    static testMethod void syncError_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = new EP_IntegrationRecord__c();
            intRecord.EP_Object_ID__c = acc.Id;         
            intRecord.EP_Message_ID__c = msgId;         
            intRecord.EP_DT_SENT__c = dt_Sent;  
            intRecord.EP_Target__c = TESTREC;   
            intRecord.EP_Object_Type__c = 'Accounts';     
            intRecord.EP_Status__c = 'SENT';
            intRecord.EP_Source__c = TESTREC;
            insert intRecord;
            test.startTest();
            EP_IntegrationUtil.syncError(acc.Id,msgId, TESTREC);
            test.stopTest();
            List<EP_IntegrationRecord__c> intRecList = [Select EP_Status__c from EP_IntegrationRecord__c where EP_Object_ID__c=:acc.Id LIMIT: LMT];
            system.assertEquals(1,intRecList.size());
            system.assertEquals(SYNCERROR, intRecList[0].EP_Status__c);  
        }       
     }
     
    /**
       Method to test the functionality of unique message Id.
      **/     
     static testMethod void getMessageId_Test(){
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             String interfaceId = 'Customersync';
             test.startTest();
             msgId = EP_IntegrationUtil.getMessageId(interfaceId);
             test.stopTest();
             String expectedId = 'Customersync_'+DateTime.now();
             
         }
     }
     
    /**
       Method to test the functionality of unique message Id.
      **/     
     static testMethod void getMessageId2_Test(){
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             String interfaceId = 'Customersync';
             test.startTest();
             msgId = EP_IntegrationUtil.getMessageId('source'
                                        ,'local'
                                        ,'processName'
                                        ,DateTime.now()
                                        ,'uniqueId');
             test.stopTest();

         }
     }
    
    /**
       Method to test the functionality of unique transaction Id.
      **/         
    static testMethod void getTransactionId_Test(){
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            String interfaceId = 'Customersync';
            String transactionType = 'Create';
            test.startTest();
            txId = EP_IntegrationUtil.getTransactionID(interfaceId,transactionType);
            test.stopTest();
            String expectedId = 'Create_Customersync_'+DateTime.now();
           
        }
     }
 }