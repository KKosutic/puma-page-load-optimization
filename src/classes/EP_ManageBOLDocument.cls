/**
 *  @Author <Accenture>
 *  @Name <EP_ManageBOLDocument>
 *  @CreateDate <12/08/2016>
 *  @Description <This is the apex class used to generate PDF or Save the Info for BOL Record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageBOLDocument {

    private static final string CLASS_NAME = 'EP_ManageBOLDocument';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForBOL';
    
    /**
     * @author <Accenture>
     * @name <generateAttachmentForBOL>
     * @date <12/08/2016>
     * @description <This method is used to generate document for BOL>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForBOL(EP_ManageDocument.Documents documents) {
    
        List<EP_BOL_Document__c> bolDocuments = new List<EP_BOL_Document__c>();
        Map<String, List<Attachment>> bolKeyAttachListMap = new Map<String, List<Attachment>>();
        Map<String, Id> bolKeyIdMap = new Map<String, Id>();
        Integer count;
        try
        {   
            List<Attachment> boKeyAttachment = null;
            for(EP_ManageDocument.Document doc : documents.document) {
                bolDocWrapper bolDocWrap = EP_DocumentUtil.fillbolDocWrapper(doc.documentMetaData.metaDatas);
                EP_BOL_Document__c bolDoc = generateBOLDocument(bolDocWrap);
                bolDocuments.add(bolDoc);
                
                String bolDocUnqId = bolDoc.EP_BOL_Document_Key__c;
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                
                if(!(bolKeyAttachListMap.containsKey(bolDocUnqId))) {
                    boKeyAttachment = new List<Attachment>();
                    bolKeyAttachListMap.put(bolDocUnqId, boKeyAttachment);
                }
                bolKeyAttachListMap.get(bolDocUnqId).add(att);            
            }
            
            count=0;
            Schema.SObjectField keyField = EP_BOL_Document__c.Fields.EP_BOL_Document_Key__c;
            Database.upsertResult[] upsrtResults = Database.upsert(bolDocuments, keyField, false);
            for(Database.upsertResult result : upsrtResults) {
                if(result.isSuccess())
                    bolKeyIdMap.put(bolDocuments[count].EP_BOL_Document_Key__c, result.getId());
                count++;
            }
            
            List<Attachment> attachments = new List<Attachment>();
            for(String key : bolKeyAttachListMap.keySet()) {
                List<Attachment> attachList = bolKeyAttachListMap.get(key);
                for(Attachment att : attachList) {                
                    att.ParentId = bolKeyIdMap.get(key);
                    attachments.add(att);
                }
            }
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }
    
    /**
     * @author <Accenture>
     * @name <generateBOLDocument>
     * @date <12/08/2016>
     * @description <This method is used to generate 'EP_BOL_Document__c' custom object record>
     * @version <1.0>
     * @param bolDocWrapper
     * @return EP_BOL_Document__c
     */    
    private static EP_BOL_Document__c generateBOLDocument(bolDocWrapper bolDoc_Wrap) {
        EP_BOL_Document__c bolDoc = new EP_BOL_Document__c();
        
        bolDoc.EP_BOL_Document_Number__c = bolDoc_Wrap.bolDocNo;
        bolDoc.EP_Trip_ID__c = bolDoc_Wrap.tripId;
        bolDoc.EP_BOL_Document_Date__c = Date.valueOf(bolDoc_Wrap.bolDOcDate);
        bolDoc.EP_BOL_Document_Key__c = bolDoc_Wrap.bolDocKey;
        bolDoc.EP_Loading_Order_Number__c = bolDoc_Wrap.bolLoadingOrder;
        //bolDoc.EP_Invoice_Key__c = EP_DocumentUtil.generateCustomerInvoiceKey(custInvWrap.BillTo, inv.Name, inv.EP_Invoice_Issue_Date__c, inv.EP_Invoice_Due_Date__c);
        
        return bolDoc;
    }
    
    /**
     *  @Author <Accenture>
     *  @Name <bolDocWrapper>
     *  @CreateDate <12/08/2016>
     *  @Description <This is the wrapper class containing 'BOL' record information>
     *  @Version <1.0>
     */
    public with sharing class bolDocWrapper {
        public String bolDocNo;
        public String bolDocKey;
        public String tripId;        
        public String bolDOcDate;
        public String bolLoadingOrder;
        public String issuedFromId;
        //public String InvoiceDueDate;
    }
}