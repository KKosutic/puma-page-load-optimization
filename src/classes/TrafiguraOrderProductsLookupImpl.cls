global with sharing class TrafiguraOrderProductsLookupImpl extends cscfga.ALookupSearch {

	private static final String ACCOUNT_ID_FIELD = 'Account ID';
	private static final String PRODUCT_TYPE_FIELD = 'ProductType';

	//global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
	public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
		String accid = searchFields.get(ACCOUNT_ID_FIELD);
		String productType = searchFields.get(PRODUCT_TYPE_FIELD);
		//accid = '0010k00000OhssjAAB';

		system.debug('TrafiguraOrderProductsLookupImpl searchFields = ' + searchFields);
		system.debug('TrafiguraOrderProductsLookupImpl accid = ' + accid);

		List<EP_Product_Option__c> productOptions = [select Price_List__c from EP_Product_Option__c where Sell_To__c = :accid];

		system.debug('TrafiguraOrderProductsLookupImpl productOptions = ' + productOptions);

		List<Id> pricebooks = new List<Id>(); //[SELECT id from Pricebook2 where ];

		for (EP_Product_Option__c prodOp : productOptions) {
			pricebooks.add(prodOp.Price_List__c);
		}

		system.debug('TrafiguraOrderProductsLookupImpl pricebooks = ' + pricebooks);

		String productQuery = 'SELECT name, ProductCode, Product2.id, Product2.name, Product2.EP_Max_Order_Qty__c, Product2.EP_Min_Order_Qty__c, Product2.EP_Company__c, Product2.EP_Unit_of_Measure__c from PricebookEntry where Pricebook2.Id IN :pricebooks';

		//List<PricebookEntry> products = [SELECT name, ProductCode, Product2.id, Product2.name, Product2.EP_Company__c, Product2.EP_Unit_of_Measure__c from PricebookEntry where 
											//Pricebook2.Id IN :pricebooks AND Product2.EP_Product_Sold_As__c = :ProductType];
		String productTypeQuery = '';

		if (productType != null) {
			if (productType.equals(EP_OrderConstant.productTypeBulk)) {
				//productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = :csOrderSetting.ProductTypeBulk__c OR Product2.EP_Product_Sold_As__c = :csOrderSetting.ProductTypeService__c)';
				productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
			} else if(productType.equals(EP_OrderConstant.productTypeService)) {
				productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\'';
				//productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = :csOrderSetting.ProductTypeService__c';
			} else if (productType.equals(EP_OrderConstant.productTypePackaged)) {
				//productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = :csOrderSetting.ProductTypePackaged__c OR Product2.EP_Product_Sold_As__c = :csOrderSetting.ProductTypeService__c)';
				productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
			}
		}
		
		system.debug('TrafiguraOrderProductsLookupImpl full query = ' + productQuery + productTypeQuery);

		List<PricebookEntry> products = Database.query(productQuery + productTypeQuery);

		system.debug('TrafiguraOrderProductsLookupImpl products = ' + products);
		
		return products;
	}

	public override String getRequiredAttributes() {
    	return '["' + ACCOUNT_ID_FIELD + ',' + PRODUCT_TYPE_FIELD + '"]';
    	//return '["Account ID","Product Basket ID","Customer"]';
  	}
}