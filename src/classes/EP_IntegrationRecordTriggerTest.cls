/* 
  @Author <Jyotsna Yadav>
   @name <EP_IntegrationRecordTriggerTest>
   @CreateDate <27/10/2015>
   @Description <This is the test class for EP_IntegrationRecordTrigger> 
   @Version <1.0>
*/
@isTest
private class EP_IntegrationRecordTriggerTest {
    
    //Static Member Variable declaration
    private static Account acc;
    private static EP_Tank_Dip__c tankDip;
    private static String TESTREC ='Test Record';
    private static String company;
    private static DateTime dt_sent; 
    private static final String NAME = 'System Administrator';
    private static final Integer LMT = Limits.getLimitQueries();
    private static Account accObj; 
    private static EP_Tank__c TankInstance;
    
   /* private static void init(){
     EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
     accObj=customHierarchy.lShipToAccounts[0];
     TankInstance=customHierarchy.lTanks[0];     
    }*/
    
    /*
        This method is used to create test data.
    */
    @testSetup static void createTestData() {
        // init();
        Map<String,Trigger_Settings__c> trigerObj = Trigger_Settings__c.getAll();
        if(trigerObj.size() == 0){
            Trigger_Settings__c accountTriggerSetting = new Trigger_Settings__c(Name='EP_Account_Trigger', IsActive__c = false);
            database.insert(accountTriggerSetting); 
        }
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
        List<EP_PROCESS_NAME_CS__c> lProcessNameCustomSetting = Test.loadData(EP_PROCESS_NAME_CS__c.sObjectType, 'EP_PROCESS_NAME_CS_TestData');
        //Instantiating the Utility class
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(1);
        //Updating the Account record with Active Status
        accObj=customHierarchy.lShipToAccounts[0];
        //accObj.recordType = [Select id,name from RecordType where sObjectType='Account' and RecordType.developerName= 'EP_VMI_SHIP_TO' limit 1];
        //accObj.EP_Status__c = 'Active';
        accObj.EP_Tank_Dips_Schedule_Time__c = '01:00';
        accObj.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
        accObj.EP_Status__c = '02-Basic Data Setup';
        update accObj;
        
        //Updating the tank record to be 'Operational'
        TankInstance=customHierarchy.lTanks[0];
        TankInstance.EP_Tank_Status__c= 'Basic Data Setup';
        update TankInstance;
        TankInstance.EP_Tank_Status__c= 'Operational';
        update TankInstance;
        //Creating a Tank Dip record in association with above Tank record
        tankDip= new EP_Tank_Dip__c(EP_Unit_Of_Measure__c = 'LT',EP_Ambient_Quantity__c = 5,EP_Tank__c=TankInstance.id,EP_Reading_Date_Time__c=System.today() - 2,EP_Reading_Date_Time_Local__c = System.today()-2);
        insert tankDip;
        //Loading the test data of custom settings.
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Pricebook2 customPB = new Pricebook2(Name='Custom Price Book', isActive=true);
        insert customPB;
        Order ord = EP_TestDataUtility.createOrder(accObj.Id, EP_TestDataUtility.NONVMI_ORDER_RT, customPB.Id); 
        insert ord;
    }
    
    /*
        This method is used to test the scenario with transaction Ids 
        different txids..different system...all are not sync...so no update
    */
    private static testMethod void checkUpdatemethod(){
        
        //createTestdata();
        
        test.startTest();   
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'company','Sent');
             insert intRec;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now().addSeconds(10),'companyNew','Sent');
             intRecNew.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_IsLatest__c = true];
             epList[0].EP_Status__c = 'SYNC';
             
             update epList[0];              
        test.stopTest();
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             
             system.assertEquals('SYNC',testDataAcc[0].EP_Integration_Status__c);   
                              
    }   
    
    //Scenario 3 record sent to multiple system in one transaction    ..both records got synced...object record is sync 
    private static testMethod void checkUpdatemethod1(){
        
        //createTestdata();
        
        test.startTest();
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'company','Sent');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',dt,'companyNew','Sent');
             intRecNew.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c];
             epList[0].EP_Status__c = 'SYNC';
             epList[1].EP_Status__c = 'SYNC';
             update epList;              
        test.stopTest();
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             
             system.assertEquals('SYNC',testDataAcc[0].EP_Integration_Status__c);   
                              
    }
    
    //Scenario 3 record sent to multiple system in one transaction    ..old record sync...data object will not update to sync..need confirmation
    private static testMethod void checkUpdatemethod2(){
        
        //createTestdata();
        
        test.startTest();
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'company','Sent');
             intRec.EP_IsLatest__c = true;
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',dt,'companyNew','Sent');
             intRec.EP_IsLatest__c = false;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = 'SYNC';
             update epList[0];              
        test.stopTest();
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             
             system.assertEquals('SENT',testDataAcc[0].EP_Integration_Status__c);   
                              
    }   
    private static testMethod void checkUpdatemethod3(){
        
        //createTestdata();
        test.startTest();
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = 'SYNC'; 
             epList[0].EP_IsLatest__c = true;
             update epList[0];      
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             
             system.assertEquals('SYNC',testDataAcc[0].EP_Integration_Status__c);      
        test.stopTest();            
                              
    }
    
    private static testMethod void checkUpdatemethod3_1(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS; 
             epList[0].EP_IsLatest__c = true;
             update epList[0];      
        
        test.stopTest();
               
                              
    }
    
    //set EP_Error_Description__c as Label.EP_Credit_Fail_Overdue_Invoice_Msg
    private static testMethod void checkUpdatemethod3_2(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS; 
             epList[0].EP_Error_Description__c = Label.EP_Credit_Fail_Overdue_Invoice_Msg;
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('FAILURE', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }

    //set EP_Error_Description__c as 'Other'
    private static testMethod void checkUpdatemethod3_3(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS; 
             epList[0].EP_Error_Description__c = 'Other';
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('FAILURE', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }

    //set EP_Status__c as EP_Common_Constant.ERROR_RECEIVED_STATUS
    private static testMethod void checkUpdatemethod3_3_1(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_RECEIVED_STATUS;
             epList[0].EP_Error_Description__c = 'Other';
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('SENT', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }

    //set EP_Status__c as EP_Common_Constant.ACKNWLDGD
    private static testMethod void checkUpdatemethod3_3_2(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ACKNWLDGD; 
             epList[0].EP_Error_Description__c = 'Other';
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('ACKNOWLEDGED', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }

    //set EP_Status__c as Other
    private static testMethod void checkUpdatemethod3_3_3(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = 'Other'; 
             epList[0].EP_Error_Description__c = 'Other';
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('SENT', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }

    //set EP_Error_Description__c as Label.EP_Overdue_Invoice_Msg
    private static testMethod void checkUpdatemethod3_4(){
        
        //createTestdata();
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company','ERROR-SENT');
             insert intRec;
             DateTime dt = [Select id,EP_Status__c,EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',dt,'companyNew','SENT');
             intRec.EP_IsLatest__c = true;
             insert intRecNew;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c,EP_IsLatest__c,EP_DT_SENT__c from EP_IntegrationRecord__c];
             system.debug('***************'+epList );   
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS; 
             epList[0].EP_Error_Description__c = Label.EP_Overdue_Invoice_Msg;
             epList[0].EP_IsLatest__c = true;
             update epList[0];    
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('FAILURE', testDataOrd[0].EP_Integration_Status__c);
        test.stopTest();      
    }
    
    
    private static testMethod void checkUpdatemethod4(){
        
        //createTestdata();
        test.startTest();
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'company', EP_Common_Constant.SYNC_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(accObj.Id,'Accounts',DateTime.now(),'companyNew','SENT');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SENT_STATUS; 
             epList[0].EP_Resend__c = true;
             epList[0].EP_IsLatest__c = true;
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             //system.assert(false, epList + '***' + epList1);
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             //system.assertEquals(EP_Common_Constant.SYNC_STATUS,testDataAcc[0].EP_Integration_Status__c); 
             system.assertEquals(EP_Common_Constant.ERROR_SENT_STATUS,testDataAcc[0].EP_Integration_Status__c); 
        test.stopTest();   
    }
    
    //set EP_Message_ID__c as EP_Common_Constant.OCN
    private static testMethod void checkUpdatemethod5(){
        
        //createTestdata();
        
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company', EP_Common_Constant.SYNC_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'companyNew', 'sent');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.SYNC_STATUS;
             epList[0].EP_IsLatest__c = true;
             epList[0].EP_Message_ID__c = EP_Common_Constant.OCN;
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             
             //system.assertEquals('SYNC', testDataOrd[0].EP_Integration_Status__c);
             system.assertEquals('SENT', testDataOrd[0].EP_Integration_Status__c);  
        test.stopTest();
               
                              
    }
    
    //set EP_Message_ID__c as OSN
    private static testMethod void checkUpdatemethod5_1(){
        
        //createTestdata();
        
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company', EP_Common_Constant.SYNC_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'companyNew', 'sent');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.SYNC_STATUS;
             epList[0].EP_IsLatest__c = true;
             epList[0].EP_Message_ID__c = 'OSN';
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             system.assertEquals('SENT', testDataOrd[0].EP_Integration_Status__c);  
             //system.assertEquals('SYNC', testDataOrd[0].EP_Integration_Status__c); 
        test.stopTest();
               
                              
    }

    private static testMethod void checkUpdatemethod5_2(){
        
        //createTestdata();
        
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company', EP_Common_Constant.SYNC_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'companyNew', 'sent');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.ACKNWLDGD;
             epList[0].EP_IsLatest__c = true;
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             
             system.assertEquals('ACKNOWLEDGED', testDataOrd[0].EP_Integration_Status__c); 
        test.stopTest();
               
                              
    }
    
    private static testMethod void checkUpdatemethod6(){
        
        //createTestdata();
        
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company', EP_Common_Constant.ERROR_SYNC_SENT_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'companyNew', 'sent');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.SYNC_STATUS;
             epList[0].EP_IsLatest__c = true;
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             
             system.assertEquals('FAILURE', testDataOrd[0].EP_Integration_Status__c); 
        test.stopTest();
               
                              
    }
    
    private static testMethod void checkUpdatemethod7(){
        
        //createTestdata();
        
        test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company', EP_Common_Constant.ERROR_SYNC_SENT_STATUS);
             intRec.EP_IsLatest__c = true;
             //insert intRec;
             //DateTime dt = [Select id,EP_Status__c, EP_DT_SENT__c from EP_IntegrationRecord__c where id =: intRec.Id].EP_DT_SENT__c;
             EP_IntegrationRecord__c intRecNew = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'companyNew', 'sent');
             insert new List<EP_IntegrationRecord__c>{intRec, intRecNew}; 
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             epList[0].EP_Status__c = EP_Common_Constant.SYNC_STATUS;
             epList[0].EP_IsLatest__c = true;
             update epList[0];   
             epList = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'CompanyNew'];
             List<EP_IntegrationRecord__c> epList1 = [Select id, EP_Object_ID__c, EP_Transaction_ID__c, EP_Status__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where EP_Company__c =: 'company'];   
             List<Order> testDataOrd = [Select id, EP_Integration_Status__c from Order where id =: ordrObj.Id];
             
             system.assertEquals('FAILURE', testDataOrd[0].EP_Integration_Status__c); 
        test.stopTest();
               
                              
    }
    
    private static testMethod void checkStatusUpdatWOTxError(){
        
        //createTestdata();
        
        test.startTest();
             accObj = [select Id from Account where recordTypeId = :EP_TestDataUtility.VMI_SHIP_RT];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRecWoTX(accObj.Id,'Accounts',DateTime.now(),'company','target 1','Sent');
             
             insert intRec;
             List<EP_IntegrationRecord__c> epList = new List<EP_IntegrationRecord__c>();
             epList = [Select id,EP_Status__c from EP_IntegrationRecord__c where EP_Company__c =: 'Company'];
             epList[0].EP_Status__c = 'ERROR-SYNC';
             epList[0].EP_IsLatest__c = true;
             update epList[0];               
        test.stopTest();
             List<Account> testDataAcc = new List<Account>();
             testDataAcc = [Select id, EP_Integration_Status__c from ACCOUNT where id =: accObj.Id];
             
             system.assertEquals('FAILURE',testDataAcc[0].EP_Integration_Status__c);   
                              
    }
    
    private static testMethod void getRecordsToUpdate_Test() {
    	test.startTest();
             Order ordrObj = [Select Id from order limit 1];
             EP_IntegrationRecord__c intRec = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company',EP_Common_Constant.SYNC_STATUS);
             intRec.EP_Message_ID__c = EP_Common_Constant.OCN;
             intRec.EP_Transaction_ID__c = 'TRA';
             intRec.EP_IsLatest__c = true;
             
             
             EP_IntegrationRecord__c intRec1 = EP_TestDataUtility.createIntegrationRec(ordrObj.Id,'Orders',DateTime.now(),'company',EP_Common_Constant.SYNC_STATUS);
             intRec1.EP_Message_ID__c = EP_Common_Constant.OCN;
             intRec1.EP_Transaction_ID__c = 'TRA-1';
            
             insert intRec1;
             insert intRec;
             
             List<EP_IntegrationRecord__c> epList = [Select id, EP_Status__c,EP_Message_ID__c, EP_Object_Type__c,EP_Error_Description__c, EP_Object_ID__c, EP_Transaction_ID__c, EP_IsLatest__c, EP_Resend__c from EP_IntegrationRecord__c where Id =: intRec1.Id];
             epList[0].EP_Status__c = EP_Common_Constant.ERROR_SYNC_SENT_STATUS;
             
             update epList[0];   
             epList[0].EP_Transaction_ID__c = 'TRA';
             
             map<id, EP_IntegrationRecord__c> intMap = new map<id, EP_IntegrationRecord__c>();
             intMap.put(epList[0].id, epList[0]);
             Map<Id,String> IntRecMap = EP_IntegrationRecordTriggerHelper.getRecordsToUpdate(intMap);
        test.stopTest();   
        system.assertNotEquals(null, IntRecMap);  
    }
    private static testMethod void getStatus_Test() {
    	test.startTest();
    		string statusFinal = EP_IntegrationRecordTriggerHelper.getStatus(null);
    	test.stopTest();   
        system.assertEquals(null, statusFinal);
    }
}