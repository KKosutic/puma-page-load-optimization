/* 
   @Author          Accenture
   @name            EP_CustomerCreationPayloadCtrlExtn
   @CreateDate      02/08/2017
   @Description     Controller extension of outbound interface used to generate XML for Customer Creation  
   @Version         1.0
*/
public with sharing class EP_CustomerCreationPayloadCtrlExtn { 
    private string messageId;
    private string messageType;
    public String custSeqId {get;set;}  
    public Account AccountObject {get;set;}
    public list<shipToAddressWrapper> shipToAddresses {get;set;}
    public list<bankAccountWrapper> bankAccounts {get;set;}
    private string secretCode;
    public List<shipToAddressWrapperForLS> shipToAddressesLS {get;set;}
    public string paymentTermCode {get;set;}
    public string packagedPaymentTermCode {get;set;}
      
    /* 
       @Author          Accenture
       @name            shipToAddressWrapperForLS
       @CreateDate      02/08/2017
       @Description     Wrapper class to hold shipToSeqId with its ship to and related records
       @Version         1.0
    */   
    public class shipToAddressWrapperForLS {
        public Account shipToAddress {get;set;}
        public string shipToSeqId {get;set;}
        public List<tanksWrapper> tanks {get;set;}
        public List<supplyLocationsWrapper> supplyLocations {get;set;}
        
        /**
        * @author           Accenture
        * @name             shipToStatus
        * @date             02/08/2017
        * @description      Sets the shipToStatus based on the account status
        * @param            NA
        * @return           string
        */ 
        public string shipToStatus {
            get {
              //To set the value of shipToStatus for WINDMS
              //Defect fix for #57771 start
              string shipToStatusValue = ((EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(String.valueOf(this.shipToAddress.EP_Status__c))
               || EP_Common_Constant.STATUS_INACTIVE.equalsIgnoreCase(String.valueOf(this.shipToAddress.EP_Status__c))
               ) ? EP_Common_Constant.STRING_NO_LS :  EP_Common_Constant.STRING_YES_LS );
              return shipToStatusValue ; } set;
              //Defect fix for #57771 end 
        }
        /**
        * @author           Accenture
        * @name             shipToAddressWrapperForLS
        * @date             02/08/2017
        * @description      Constructor for shipToAddressWrapperForLS.
        * @param            NA
        * @return           NA
        */     
         public shipToAddressWrapperForLS (){
            EP_GeneralUtility.Log('Public','EP_CustomerCreationPayloadCtrlExtn','');
            tanks = new List<tanksWrapper>();
            supplyLocations = new List<supplyLocationsWrapper>();
        }
    }
    
    /* 
       @Author          Accenture
       @name            tanksWrapper
       @CreateDate      02/08/2017
       @Description     Wrapper class to hold tankSeqId with its tank records
       @Version         1.0
    */  
    public class tanksWrapper {
        public EP_Tank__c Tank {get;set;}
        public string tankSeqId {get;set;}
    }
    
    /* 
       @Author          Accenture
       @name            supplyLocationsWrapper
       @CreateDate      02/08/2017
       @Description     Wrapper class to hold supplyLocationSeqId with its supply location records
       @Version         1.0
    */   
    public class supplyLocationsWrapper {
        public EP_Stock_Holding_Location__c supplyLocation {get;set;}
        public string supplyLocationSeqId {get;set;}
    }
    
    /**
    * @author           Accenture
    * @name             deferUpperLimit
    * @date             02/08/2017
    * @description      To hold the value of deferUpperLimit as the XML only takes the numberic value and not null
    * @param            NA
    * @return           Integer
    */
    public Integer deferUpperLimit {
        get {
          Integer deferUpperLimitValue = Integer.ValueOf(string.isblank(this.AccountObject.EP_Defer_Upper_Limit__c) ? '0' : this.AccountObject.EP_Defer_Upper_Limit__c);
          return deferUpperLimitValue;
          } set;
    }    
    
    /* 
       @Author          Accenture
       @name            bankAccountWrapper
       @CreateDate      02/08/2017
       @Description     Wrapper class to hold bankAccountSeqId with its bank account records
       @Version         1.0
    */
    public class bankAccountWrapper {
        public EP_Bank_Account__c bankAcc {get;set;}
        public string bankAccountSeqId {get;set;}
    }
    
    /* 
       @Author          Accenture
       @name            shipToAddressWrapper
       @CreateDate      02/08/2017
       @Description     Wrapper class to hold shipToSeqId with its ship to  records
       @Version         1.0
    */
    public class shipToAddressWrapper {
        public Account shipToAddress {get;set;}
        public string shipToSeqId {get;set;}
    }
    
    /**
    * @author           Accenture
    * @name             setOrderDetails
    * @date             02/08/2017
    * @description      The extension constructor initializes the members variable AccountObject by using the getRecord method from the standard controller.This method will be use setup the values for Header in XML message
    * @param            ApexPages.StandardController
    * @return           NA
    */               
    public EP_CustomerCreationPayloadCtrlExtn(ApexPages.StandardController stdController) {
    	secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{EP_Common_Constant.COMPANY_CODE_FIELD ,EP_Common_Constant.DEFER_UPPER_LIMIT_FIELD,EP_Common_Constant.ACCOUNT_EP_REQUESTED_PAYMENT_TERMS,EP_Common_Constant.ACCOUNT_EP_REQUESTED_PACKAGED_PAYMENT_TERM, EP_Common_Constant.ACCOUNT_DUMMY_FIELD}); 
        this.AccountObject = (Account) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        this.shipToAddresses = new list<shipToAddressWrapper>();
        this.bankAccounts = new list<bankAccountWrapper>();
        this.setShipToAddresses();
        this.setBankAccounts();
        custSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.AccountObject.Id);
        
        this.shipToAddressesLS  = new List<shipToAddressWrapperForLS>();
        this.setShipToAddressesForLS();
        EP_PaymentTermMapper paymentMapper = new EP_PaymentTermMapper();
        if(string.isNotBlank(this.AccountObject.EP_Requested_Payment_Terms__c)){
        	EP_Payment_Term__c paymentTerm = paymentMapper.getRecordsByName(this.AccountObject.EP_Requested_Payment_Terms__c) ;
        	this.paymentTermCode = paymentTerm == null ? EP_Common_Constant.BLANK : paymentTerm.EP_Payment_Term_Code__c;
        }
    	if(string.isNotBlank(this.AccountObject.EP_Requested_Packaged_Payment_Term__c)){
    		EP_Payment_Term__c packagedPaymentTerm = paymentMapper.getRecordsByName(this.AccountObject.EP_Requested_Payment_Terms__c) ;
        	this.packagedPaymentTermCode = packagedPaymentTerm == null ? EP_Common_Constant.BLANK : packagedPaymentTerm.EP_Payment_Term_Code__c; 
    	} 
    }   
        
    /**
    * @author           Accenture
    * @name             setShipToAddressesForLS
    * @date             02/08/2017
    * @description      This will get the shipToaddresses along with Tanks and SupplyLocations and will setup the wrappers for WINDMS request
    * @param            NA
    * @return           NA
    */                 
    public void setShipToAddressesForLS() {
        EP_GeneralUtility.Log('Public','EP_CustomerCreationPayloadCtrlExtn','setShipToAddressesForLS');
        this.shipToAddressesLS = new list<shipToAddressWrapperForLS>();
        //List <String>shipTOAdressTypes = new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME,EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME}; 
        List <String>shipTOAdressTypes = new List<String>();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        List<Account> shipTos = new List<Account>();
        if(this.AccountObject.EP_Is_Dummy__c){
        	shipTOAdressTypes.add(EP_Common_Constant.STORAGE_SHIP_TO_DEV_NAME);
        	shipTos= accMapper.getDummyAccountWithShipToTanksById(AccountObject.Id,shipTOAdressTypes,EP_Common_Constant.STATUS_SET_UP);
        }else {
        	shipTOAdressTypes.add(EP_Common_Constant.VMI_SHIP_TO_DEV_NAME);
        	shipTOAdressTypes.add(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME);
        	shipTos = accMapper.getAccountWithShipToTanksById(AccountObject.Id,shipTOAdressTypes,EP_Common_Constant.STATUS_SET_UP);
        }
        for(Account shipToAdd : shipTos) {
            shipToAddressWrapperForLS shipToWrap = new shipToAddressWrapperForLS();
            shipToWrap.shipToAddress = shipToAdd;
            shipToWrap.shipToSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, shipToAdd.Id);
            
            for(EP_Tank__c epTank : shipToAdd.Tank__r){
                tanksWrapper tankWrap = new tanksWrapper();
                tankWrap.tankSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, epTank.Id);
                tankWrap.Tank = epTank;
                shipToWrap.tanks.add(tankWrap);
            }
            for(EP_Stock_Holding_Location__c  epSupplyLocation : shipToAdd.Stock_Holding_Locations1__r){
                supplyLocationsWrapper supplyWrap = new supplyLocationsWrapper();
                supplyWrap.supplyLocationSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, epSupplyLocation.Id);
                supplyWrap.supplyLocation  = epSupplyLocation;
                shipToWrap.supplyLocations.add(supplyWrap);
            }
            
            this.shipToAddressesLS.add(shipToWrap);
        }
    }
    
    /**
    * @author           Accenture
    * @name             setOrderDetails
    * @date             02/08/2017
    * @description      This methods sets the data in the bank account wrappers for the respective account
    * @param            NA
    * @return           NA
    */           
    public void setBankAccounts() {
        EP_GeneralUtility.Log('Public','EP_CustomerCreationPayloadCtrlExtn','setBankAccounts');
        this.bankAccounts = new list<bankAccountWrapper>();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        for(EP_Bank_Account__c bank : accMapper.getBankAccountsByAccountId(AccountObject.Id)) {
            bankAccountWrapper bankWrap = new bankAccountWrapper();
            bankWrap.bankAcc = bank;
            bankWrap.bankAccountSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, bank.Id);
            this.bankAccounts.add(bankWrap);
        }
    }
     
    /**
    * @author           Accenture
    * @name             setShipToAddresses
    * @date             02/08/2017
    * @description      This methods sets the data in the ship to account wrappers for the respective account
    * @param            NA
    * @return           NA
    */          
    public void setShipToAddresses() {
        EP_GeneralUtility.Log('Public','EP_CustomerCreationPayloadCtrlExtn','setShipToAddresses');
        this.shipToAddresses = new list<shipToAddressWrapper>();
        List <String>shipTOAdressTypes = new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME,EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME}; 
        EP_AccountMapper accMapper = new EP_AccountMapper();       
        for(Account shipToAdd : accMapper.getShipToAccountsByAccountId(AccountObject.Id,shipTOAdressTypes,EP_Common_Constant.STATUS_SET_UP)) {
            shipToAddressWrapper shipToWrap = new shipToAddressWrapper();
            shipToWrap.shipToAddress = shipToAdd;
            shipToWrap.shipToSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, shipToAdd.Id);
            this.shipToAddresses.add(shipToWrap);
        }
    }
      
    /**
    * @author           Accenture
    * @name             checkPageAccess
    * @date             02/08/2017
    * @description      This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
    * @param            NA
    * @return           NA
    */             
    public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_CustomerCreationPayloadCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}