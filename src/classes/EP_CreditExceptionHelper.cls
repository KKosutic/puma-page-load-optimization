/*
@Author <Accenture>
@name <EP_CreditExceptionHelper>
@CreateDate <01/06/2016>
@Description <This class will generate Payload for SObjects> 
@Version <1.0> 
*/
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    1/19/2017           Sandeep Kumar           Commented out createNavCreditExcpPayLoad method and 
                                                replaced its caller with payLoadService class method
    *************************************************************************************************************
    */

    public without sharing class EP_CreditExceptionHelper {

    private static final String NAV = 'NAV';
    private static final String CREDEXCEPTION_CREATE = 'CREDEXCEPTION_CREATE';
    private static final String CLASSNAME = 'EP_CreditExceptionHelper'; 
    //VARAIBLES TO CAPTURE ERROR FOR CALLOUTS
    public static String errorDescription_NAV;
    public static String errorDescription_LS;
    public static List<Exception> listOfCallOutExceptions;
    // Variables from PayLoad    
    public static Integer queryRowLimit;
    static String externalSystem;
    private static final String NAV_CREDEXCP_FIELDS = 'EP_NAV_Credit_Exception';
    private static final String FROM_CREDIT_EXCEPTION = ',EP_Customer_Id__c from EP_Credit_Exception_Request__c where ID IN: sCredExcpId  limit: queryRowLimit';    // VARIABLES TO TRACK EXCEPTION DETAILS
    // VARIABLES TO TRACK EXCEPTION DETAILS
    private static final String EPUMA = 'ePuma';
    private static final String EP_CREDEXCP_HANDLER = 'EP_CreditExceptionHelper ';
    private static final String CREATE_NAV_BULK_CREDEXCEPTION_PAYLOAD_MTD = 'createNavBulkCreditExcpPayLoad';
    private static final String CREATE_NAV_CREDEXCEPTION_PAYLOAD_MTD = 'createNavCreditExcpPayLoad';
    
    /**
    * @author <Accenture>
    * @name <callSfdcToNavFuture>
    * @date <01/06/2016>
    * @description <This is the future method that makes callout to SFDC to Nav>
    * @version <1.0>
    * @param set<id>, List<Id>, string
    * @return void
    */
    @future(callout=true)
    public static void callSfdcToNavFuture(set<id>mCreditExcp, List<Id> listOfMessageIdGeneratorId, String CompanyCode){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionHelper','callSfdcToNavFuture');
        String payload;
        String transactionId;
        String messageId;
        String requestXML;        
        DateTime dtSent;        
        
        List<String> listOfNAVObjectType = new List<String>();
        List<Id> listOfNAVObjectID = new List<Id>();
        
        Map<String,String> mapOfCreateIntegrationStatus = new Map<String,String>();
        List<EP_Message_Id_Generator__c> listOfMessageIdGenerator = new List<EP_Message_Id_Generator__c>();
        mapOfCreateIntegrationStatus = EP_Common_Util.createIntegrationStatusByObjAPIName();
        
        try{
            listOfCallOutExceptions = new List<Exception>();
            queryRowLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
            //CREATE MSG ID FOR NAV
            listOfMessageIdGenerator = [Select Name 
            from EP_Message_Id_Generator__c 
            where Id IN :listOfMessageIdGeneratorId
            Limit : queryRowLimit];
            
            messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
                ,EP_Common_Constant.GBL
                ,EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.CREATE_CREDIT_EXCEPTION_NAV).EP_Process_Name__c
                ,DateTime.now()
                ,listOfMessageIdGenerator[0].name);
            
            payload = EncodingUtil.base64Encode
            (Blob.valueOf(createNavBulkCreditExcpPayLoad(mCreditExcp)));
            
            requestXML = EP_PayloadHandler.createCustomerRequest( messageId//MESSAGE ID
            ,EP_Common_Constant.SFDC_NAV//INTERFACE NAME
            ,CompanyCode//COMPANY BLANK
            ,payload
            ,EP_Common_Constant.CREDIT_EXCEPTION_CREATE //OBJECT NAME
            );        

            for(String objectType : EP_PayLoadHandler.mObjectRecordId.keySet()){
                listOfNAVObjectID.addAll(EP_PayLoadHandler.mObjectRecordId.get(objectType));
            }

            for(Id objectId : listOfNAVObjectID){
                listOfNAVObjectType.add(mapOfCreateIntegrationStatus.get(objectId.getSObjectType().getDescribe().getName().toUpperCase()));
            }

            dtSent = System.now();
            transactionId = EP_IntegrationUtil.getTransactionID(NAV,CREDEXCEPTION_CREATE);
            
            EP_IntegrationUtil.callSfdcToIPASS(EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(EP_Common_Constant.IPASS_NAV_CREDEXCEPTION).EP_Value__c
                ,EP_COMMON_CONSTANT.POST
                ,requestXML
                ,listOfNAVObjectID 
                ,listOfNAVObjectType 
                ,transactionId 
                ,messageId
                ,NAV
                ,EP_Common_Constant.BLANK 
                ,dtSent
                ,new Map<String, String>()
                ,CLASSNAME
                ,true);
        }
        catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();            
            listOfCallOutExceptions.add(handledException);            
        }  
    } 
    
    /*** Defect #28272 ***/
    /**
    * @author <Accenture>
    * @name <callSfdcToNavFuture>
    * @date <01/06/2016>
    * @description <This is the future method that makes callout to SFDC to Nav>
    * @version <1.0>
    * @param String, String, List<Id>, String 
    * @return void
    */
    @future(callout=true)
    public static void callSfdcToNavFuture( String endpoint, String requestXML, List<Id> lIntegRecIds, String msg ){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionHelper','callSfdcToNavFuture');
        
        try{    
            listOfCallOutExceptions = new List<Exception>();
            EP_IntegrationUtil.callSfdcToIPASS( endpoint
                ,EP_COMMON_CONSTANT.POST
                ,requestXML
                ,lIntegRecIds
                ,CLASSNAME
                ,msg
                ,true);
            
            }catch(Exception handledException){
                errorDescription_NAV = handledException.getMessage();            
                listOfCallOutExceptions.add(handledException);            
            }
        }
        
    /**
    * @author <Accenture>
    * @name <createNavBulkCreditExcpPayLoad>
    * @date <01/06/2016>
    * @description <This method creates Payload for bulk Credit Exception record for NAVISION>
    * @version <1.0>
    * @param set<Id>
    * @return String
    */
    public static String createNavBulkCreditExcpPayLoad(set<Id> sCredExcpId){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionHelper','createNavBulkCreditExcpPayLoad');
        
        String navBulkcredExcepPayLoad = EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.CREDIT_EXCEPTIONS + EP_Common_Constant.ANG_RIGHT ;
        String query = EP_Common_Constant.SELECT_STRING;        
        queryRowLimit = EP_Common_Util.getQueryLimit();
        try{
            //line added by Sandeep, dated 2/1/2017//External System name modified by GAUTAM on 10th Feb
            EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.NAV_CREDIT_EXCEPTION;
            //Commented out by Sandeep, dated 2/1/2017
            //externalSystem = EP_Common_Constant.NAV_CUSTOMER;
            //EP_PayLoadHandler.createObjectFieldNodeMap(EP_Common_Constant.NAV_CREDIT_EXCEPTION);
            EP_PayloadHelper.WrapperFields credExcepWrapperFields = EP_PayLoadHandler.createFieldsWrapper(EP_Common_Constant.CREDITEXCEPTION_OBJ, NAV_CREDEXCP_FIELDS);
            query += credExcepWrapperFields.fieldString;
            query += FROM_CREDIT_EXCEPTION;
            
            for(EP_Credit_Exception_Request__c creditExcp : DataBase.Query(query) ){
                //modified by Sandeep, dated 1/19/2017
                //navBulkcredExcepPayLoad += createNavCreditExcpPayLoad(creditExcp, credExcepWrapperFields.lFieldAPINames);     
                EP_PayLoadService payLoadService = new EP_PayLoadService();
                navBulkcredExcepPayLoad += payLoadService.createPayload(creditExcp, credExcepWrapperFields.lFieldAPINames, 
                    EP_Common_Constant.CREDIT_EXCEPTION, null);   
            }        
            navBulkcredExcepPayLoad +=  EP_Common_Constant.ANG_LEFT_CLOSE 
            + EP_Common_Constant.CREDIT_EXCEPTIONS 
            + EP_Common_Constant.ANG_RIGHT ;
            
            //TEMP SOLUTION
            navBulkcredExcepPayLoad =   navBulkcredExcepPayLoad.replace(EP_Common_Constant.Identifier
                ,EP_Common_Constant.Identifier.toLowerCase());        
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EPUMA,
                CREATE_NAV_BULK_CREDEXCEPTION_PAYLOAD_MTD, 
                EP_CREDEXCP_HANDLER, ApexPages.Severity.ERROR);
        }        
        return navBulkcredExcepPayLoad;
    }    
    
    /**
    * @author <Accenture>
    * @name <createNavCreditExcpPayLoad>
    * @date <01/06/2016>
    * @description <This method creates Credit Exception payload for NAV>
    * @version <1.0>
    * @param EP_Credit_Exception_Request__c,List<String>
    * @return String
    */
    /* Commented out by Sandeep. Dated 1/19/2017. This method has no use, can be removed.
    
    public static String createNavCreditExcpPayLoad(EP_Credit_Exception_Request__c credExcep, List<String> lFielsAPINames){        
        String navCredExcepPayLoad;
        try{
            navCredExcepPayLoad = EP_Common_Constant.ANG_LEFT_OPEN 
            + EP_Common_Constant.CREDIT_EXCEPTION 
            + EP_Common_Constant.ANG_RIGHT 
            + EP_PayLoadHandler.createPayLoadWithIdentifier(credExcep,lFielsAPINames)                                
            + EP_Common_Constant.ANG_LEFT_CLOSE 
            + EP_Common_Constant.CREDIT_EXCEPTION 
            + EP_Common_Constant.ANG_RIGHT;
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, 
            EPUMA, 
            CREATE_NAV_CREDEXCEPTION_PAYLOAD_MTD, 
            EP_CREDEXCP_HANDLER, 
            ApexPages.Severity.ERROR);
        }                               
        return navCredExcepPayLoad;                      
        }*/
    }