@isTest
private class EP_InvoiceMapper_UT {    
    private static testMethod void getOverdueInvoices_test() {
        EP_InvoiceMapper localObj = new EP_InvoiceMapper();
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderWithInvoice();
        EP_Invoice__c invoiceObj = [SELECT id, Name, EP_Outstanding_Amount__c, EP_Invoice_Due_Date__c , EP_Overdue__c ,EP_Bill_To__c  FROM EP_Invoice__c LIMIT 1];
        System.debug('@@ invoiceObj +'+invoiceObj);
        set<id> idSet = new Set<id>{invoiceObj.EP_Bill_To__c };
        Test.StartTest();
        list<EP_Invoice__c> result= localObj.getOverdueInvoices(idSet);
        Test.StopTest();
        System.AssertEquals(True,result.size() > 0);
    }
}