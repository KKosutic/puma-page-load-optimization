/**
* @Author <Ashok Arora>
* @Name <EP_AddProductController>
* @CreateDate <07/06/2016>
* @Description <This apex class is used to show list of products associated with a Pricebook. 
                This also handles addition/removal of products from associated Pricebook>
* @Version <1.0>
*/
public without sharing class EP_AddProductController{
    
    private boolean packagedProductFound{
        get{ return packagedProductFound == NULL? false : packagedProductFound; }
        set{ packagedProductFound = value; }
    }
    private String pbId = EP_Common_Constant.BLANK;
    private String cmpnyId = EP_Common_Constant.BLANK;
    private String accId = EP_Common_Constant.BLANK;
    private String currencyCode = EP_Common_Constant.BLANK;
    private Map<Id, PriceBookEntry> prodPBEMap = null;
    
    /**DEFECT 47511 FIX START**/ 
    map<Id,WrapperProduct>productStateMap;
    map<Id,Boolean>productOriginalStateMap;
    list<WrapperProduct>lUpdatedWrapperProduct;
    public Integer numberOfpackagedProductsSelected{get;set;}
    /**DEFECT 47511 FIX END**/
    
    private static final string PBID_STR = 'pbid';
    private static final string COMPNYID_STR = 'cmpnyId';
    private static final string ACCID_STR = 'accId';
    private static final string CRNCY_ID = 'crncyId';
    private static final String PRICEBOOK = 'PriceBook';
    private static final String COUNTERVAR = 'counter';
    private static final String PROLIST = 'EP_Product_List__c';
    private static final String REMOVE = 'Remove';
    private static final String UPDATEVAR = 'Update';
    private static final String PRODUCTVAR = 'Product';
    private static final String ADD = 'Add';
    
    public list<WrapperProduct> wrapperList{get; set;}
    /********Pagination Methods & Variables Start****************/
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public List<SelectOption> pageSizeOptions{get; set;}
    public ApexPages.StandardSetController setCon{
        get{
            if(setCon == null){
                Integer nrow = EP_Common_Util.getQueryLimit();
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select id
                ,IsActive
                ,Name
                From Product2
                Where  EP_Company_Lookup__c=:cmpnyId 
                order by Name ASC
                Limit :nrow]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }
        set;
    }
    /***********Pagination Methods & Variables End***********/
    /**
    * Flag to check if there is no product list id
    */
    public  boolean noProductListFlag{
        get{
            boolean pbCheck = false;
            if(string.isBlank(pbId)){
                pbCheck = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,system.label.EP_Product_Not_Available_Error_Message));
            }
            return pbCheck;
        } 
        set;
    }
    
    /**
     * @author <Ashok Arora>
     * @description <Default Constructor>
     * @name <EP_AddProductController>
     * @date <07/06/2016>
     * @param none
     * @return none
     */
    public EP_AddProductController(){
        size = EP_Common_Constant.TEN;
        packagedProductFound = false;
        pbId = ApexPages.currentpage().getParameters().get(PBID_STR);
        cmpnyId = ApexPages.currentpage().getParameters().get(COMPNYID_STR);
        accId = ApexPages.currentpage().getParameters().get(ACCID_STR);
        currencyCode = ApexPages.currentpage().getParameters().get(CRNCY_ID);
       	wrapperList = new list<WrapperProduct>();
       	/**DEFECT 47511 FIX START**/ 
        productStateMap = new map<Id,WrapperProduct>();
        productOriginalStateMap = new map<Id,Boolean>();
        lUpdatedWrapperProduct = new list<WrapperProduct>();
        numberOfpackagedProductsSelected = 0;
        /**DEFECT 47511 FIX END**/
        pageSizeOptions = paginationSizeOptions();
        queryRecord();
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to add/remove product(s) from page>
     * @name <addProduct>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public PageReference addProduct(){
        PageReference page;
        Integer nRows;
        list<PriceBookEntry>lPriceBookEntry = new list<PriceBookEntry>(); 
        
        PriceBookEntry pbe ;
        






        //EP_Tank__c tank;
        set<id> productTankSet = new set<Id>();
        List<EP_ChangeRequestLine__c> crlIterate = new List<EP_ChangeRequestLine__c>();
        

        Id reqliItemRtType = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.CHANGE_REQUEST_LINE_OBJ,EP_Common_Constant.CHANGEProduct_REQ_LINE_RT_DEV);
        Integer counter = 0;
        
        try{




			/**DEFECT 47511 FIX START**/
            for(WrapperProduct wrapProduct : wrapperList){

                productStateMap.put(wrapProduct.product.Id,wrapProduct);
            }
			for(Id ProductId : productStateMap.keyset()){	
                if(productStateMap.get(productId).isSelect != productOriginalStateMap.get(productId)){
                    lUpdatedWrapperProduct.add(productStateMap.get(productId));
                    
                }
            }
            
            for(WrapperProduct wrapProduct : lUpdatedWrapperProduct){	
             /**DEFECT 47511 FIX END**/
                if(wrapProduct.isSelect){
                    
                    if(prodPBEMap.containsKey(wrapProduct.Product.Id)){
                        pbe = prodPBEMap.get(wrapProduct.Product.Id);
                        if(!prodPBEMap.get(wrapProduct.Product.Id).isActive){
                            
                            crlIterate.add(assignValues(wrapProduct,reqliItemRtType,ADD,PRODUCTVAR));
                            
                        }
                    }else{
                        pbe = new PriceBookEntry(Pricebook2Id = pbId
                        ,Product2Id = wrapProduct.Product.Id
                        );
                        
                        crlIterate.add(assignValues(wrapProduct,reqliItemRtType,ADD,PRODUCTVAR));  
                        
                    }
                    
                    pbe.EP_Is_Sell_To_Assigned__c = true;   
                    pbe.isActive = true;
                    pbe.UnitPrice = 0;
                    pbe.UseStandardPrice = false;
                    if(String.isBlank(pbe.id)){
                        pbe.currencyISOCode = currencyCode;
                    }
                    lPriceBookEntry.add(pbe);
                    
                }else if(!wrapProduct.isSelect && prodPBEMap.containsKey(wrapProduct.Product.Id)){
                    productTankSet.add(wrapProduct.Product.Id);
                    pbe = prodPBEMap.get(wrapProduct.Product.Id);
                    pbe.isActive = false;
                    pbe.EP_Is_Sell_To_Assigned__c = true;
                    pbe.UseStandardPrice = false;
                    pbe.UnitPrice = 0;
                    EP_ChangeRequestLine__c requestLine = assignValues(wrapProduct,reqliItemRtType,REMOVE,PRODUCTVAR);
                    requestLine.EP_List_Price__c = wrapProduct.standardPrice;
                    crlIterate.add(requestLine);
                    lPriceBookEntry.add(pbe);
                }else{}

            }
            
            
            Boolean isErrorFound = false;
            
            Map<id,Integer> countTankProduct = new map<id,Integer>();
            nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            for(AggregateResult aggTank : [SELECT count (id) counter,EP_Product__c 
            FROM EP_Tank__c 
            WHERE (EP_Tank_Status__c!= :EP_Common_Constant.TANK_DECOMISSIONED_STATUS 
            AND EP_Ship_To__r.ParentId =:accId) 
            GROUP BY EP_Product__c 
            limit :nRows]){
                countTankProduct.put((ID)aggTank.get(EP_Common_Constant.PRODUCT_OBJ),Integer.valueOf(aggTank.get(COUNTERVAR)));





            }
            


            
            /**DEFECT 47511 FIX START**/
            
			for(WrapperProduct wrapProduct : lUpdatedWrapperProduct){	
			/**DEFECT 47511 FIX END**/
                
                if(!wrapProduct.isSelect && prodPBEMap.containsKey(wrapProduct.Product.Id) && countTankProduct.containsKey(wrapProduct.Product.Id)){
                    ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, 
                    wrapProduct.Product.Name+system.label.EP_Error_Message_on_Product_Removal_If_Tank_Associated));
                    isErrorFound = true;
                }
            }

            if(isErrorFound){

                return null;
            }

            Boolean isSuccess = true;
            if(lPriceBookEntry.isEmpty()){
                //add page message
                isSuccess= false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.label.EP_Product_Not_Selected_Error_Message));
            }
            else{


                Account ac = [select id,Name,EP_Status__c from Account WHERE id =:accId limit:EP_Common_Constant.ONE];
                // Created a set to contain status values together
                set<String> sellToaccStatus = new set<String>{EP_Common_Constant.STATUS_ACTIVE,EP_Common_Constant.STATUS_BLOCKED
                    ,EP_Common_Constant.STATUS_REJECTED ,EP_Common_Constant.STATUS_ARCHIVED};
                if(sellToaccStatus.Contains(ac.EP_Status__c)){
                    EP_ChangeRequest__c request = new EP_ChangeRequest__c();
                    request = EP_ChangeRequestService.createChangeRequestRecord(pbId, PRICEBOOK , EP_Common_Constant.CHANGEProduct_REQ_RT_DEV, UPDATEVAR, PROLIST);
                    request.EP_Account__c = ac.id;
                    request.RecordTypeID = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.CHANGE_REQUEST_OBJ, 
                    EP_Common_Constant.CHANGEProduct_REQ_RT_DEV);
                    

                    Database.saveResult sv = Database.insert(request);
                    

                    if(!sv.isSuccess()){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,sv.getErrors()[0].getMessage()));
                        isSuccess = false;
                        
                    }else{


                        List<EP_ChangeRequestLine__c> requestLines = new List<EP_ChangeRequestLine__c>();
                        for(EP_ChangeRequestLine__c crl :crlIterate){
                            crl.EP_Change_Request__c = request.id;
                            crl.EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_OPEN;
                        } 

                        List<Database.saveResult> srList = Database.insert(crlIterate,false);
                        

                        for(Database.saveResult s : srList){
                            if(!s.isSuccess()){
                                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR,sv.getErrors()[0].getMessage()));
                                isSuccess = false;
                            }
                        }



                        if(isSuccess){
                            page = new PageReference(EP_Common_Constant.SLASH+accId).setRedirect(true);
                            nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();

                            List<EP_ChangeRequest__c> crList =new List<EP_ChangeRequest__c>([Select Id,EP_Object_Type__c, EP_Record_Type__c, EP_Process_Step__c
                            ,EP_Requestor__c
                            ,EP_Requestor__r.Profile.Name
                            ,EP_Region__c

                            ,EP_Delivery_Country__c
                            ,EP_Product_List__c
                            ,(select Id
                            ,EP_Action__c


                            ,EP_Request_Status__c
                            ,EP_Review_Step__c
                            ,EP_New_Value__c
                            ,EP_Product_Name__c
                            ,EP_Request_Type__c
                            ,EP_Original_Value__c
                            ,EP_Change_Request__c
                            ,EP_Source_Field_API_Name__c
                            from Change_Request_Lines__r limit : nRows)
                            From EP_ChangeRequest__c    
                            Where ID=: request.id limit 1]);
                            if(!crList.isEmpty()){
                                EP_ChangeRequestService.findNextReviewActionforCRGeneric(crList);
                            }
                        }
                    }
                }
                else{





                    List<Database.upsertResult> acc = database.upsert(lPriceBookEntry, false);
                    

                    Boolean success = true;
                    for(Database.upsertResult s : acc){

                        if(!s.isSuccess()){
                            

                            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR,s.getErrors()[0].getMessage()));
                            success = false;
                        }
                    }



                    if(success){
                        page = new PageReference(EP_Common_Constant.SLASH+accId).setRedirect(true);
                    }
                }
            }



        }Catch(Exception e){

            ApexPages.addMessages(e);
            page = null;
        }   

        return page;
    }

    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to perform Cancel operation from page>
     * @name <doCancel>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public pageReference doCancel(){
        PageReference ref ;
        try{
            ref = new PageReference(EP_Common_constant.SLASH+accId);
        }catch(Exception e){
            Apexpages.addmessages(e);
        }
        return ref;
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to move to Next page>
     * @name <next>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public void next(){
        try{
            if(setCon != null){
                setCon.next();
                queryRecord();
            }
        }catch(Exception e){
            apexpages.addMessages(e);
        }   
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to move to Previous page>
     * @name <previous>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public void previous(){
        try{
            if(setCon != null){
                setCon.previous();
                queryRecord();
            }
        }catch(Exception e){
            apexpages.addMessages(e);
        }       
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to move to First page>
     * @name <first>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public void first(){
        try{    
            if(setCon != null){
                setCon.first();
                queryRecord();
            }
        }catch(Exception e){
            apexpages.addMessages(e);
        }   
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to move to Last page>
     * @name <last>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public void last(){
        try{
            if(setCon != null){
                setCon.last();
                queryRecord();
            }
        }catch(Exception e){
            apexpages.addMessages(e);
        }   
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to change pagination size>
     * @name <refreshPageSize>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    public void refreshPageSize(){
        try{ 
            setCon.setPageSize(size);
            queryRecord();
        }catch(Exception e){
            apexpages.addMessages(e);
        }
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to verify that whether current user is allowed to modify Pricebook or not based on profile>
     * @name <getallowedPBModification>
     * @date <07/06/2016>
     * @param none
     * @return Boolean
     */
    public boolean getallowedPBModification(){
        
        boolean check = false;
        noProductListFlag = false;
        try{
            Profile p = [Select id, Name from Profile where id=: UserInfo.getProfileId() limit 1];
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            List<EP_CR_Profile_Mapping__mdt> profMapping = new List<EP_CR_Profile_Mapping__mdt>([Select id, EP_Modify_Product_List__c from 
            EP_CR_Profile_Mapping__mdt where MasterLabel =: p.Name
            limit :nRows]);
            if(!profMapping.isEmpty() && profMapping[0].EP_Modify_Product_List__c){
                check = true;
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,system.label.EP_Product_List_Modify_Access_Error_Message));
            }
        }catch(Exception e){
            apexpages.addMessages(e);
        }   
        return check;
    }
    
    /**
     * @author <Ashok Arora>
     * @description <To get account details of selected account>
     * @name <getCurentAccount>
     * @date <07/06/2016>
     * @param none
     * @return Account
     */
    public Account getCurentAccount(){
        return [Select id, Name, EP_PriceBook__r.Name,currencyISOCode  from Account where id=: accId limit 1];
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to retreive 'Product2' records associated with a particular company>
     * @name <queryRecord>
     * @date <07/06/2016>
     * @param none
     * @return void
     */
    private void queryRecord(){

		/**DEFECT 47511 FIX START**/ 
        if(wrapperList != null & !wrapperList.isEmpty()){
           for(WrapperProduct wrapProduct: wrapperList){
             productStateMap.put(wrapProduct.product.id,wrapProduct); 
           } 
        }
        /**DEFECT 47511 FIX END**/


        wrapperList = new list<WrapperProduct>();
        Set<Id> product2Ids = new Set<Id>();
        if(setCon != null){
            for(Product2 p: (List<Product2>)setCon.getRecords()){
                product2Ids.add(p.id);

            }
            prodPBEMap = new Map<Id, PriceBookEntry>();
            if(!product2Ids.isEmpty()){
                Integer nRows = EP_Common_Util.getQueryLimit();
                for(PriceBookEntry pbe : [Select Product2Id, isActive
                ,UseStandardPrice,UnitPrice,
                Pricebook2Id
                from priceBookEntry 
                Where PriceBook2Id =:pbId and Product2ID IN : product2Ids
                and currencyISOCode = :currencyCode
                limit :nRows]){

                    prodPBEMap.put(pbe.Product2Id, pbe);



                    
                }
                
                Id stdpricebookID = Test.isRunningTest() ?
                Test.getStandardPricebookId() :
                [SELECT Id From Pricebook2 
                WHERE IsStandard = true limit 1].Id;
                nRows = EP_Common_Util.getQueryLimit();
                WrapperProduct wrObj;
                for(Product2 product : [Select id
                ,IsActive

                ,Name
                ,EP_Product_Sold_As__c
                ,productCode 
                ,(select UnitPrice
                from PriceBookEntries
                Where priceBook2id =: stdpricebookID
                AND currencyISOCode = :currencyCode
                AND isActive = true
                limit 1)
                From Product2
                Where  id in:product2Ids  
                order by Name ASC
                Limit :nRows 

                ]){
                    if(EP_Common_Constant.PRODUCT_PACKAGED.equalsIgnoreCase(product.EP_Product_Sold_As__c)){
                        packagedProductFound = true;


                    }
                    
                    if(!product.PricebookEntries.isEmpty()){
                        wrObj = createWrapperInstance(product, 
                        (!product.PricebookEntries.isEmpty() ? product.PricebookEntries[0].UnitPrice: 0 ), 
                        wrapperList.size());


                        
                        
                        wrObj.stdPriceBookEntryNotDefined = true;


                        
                        
                        if(prodPBEMap.containsKey(product.id)){


                            /**DEFECT 47511 FIX START**/
                            wrObj.isSelect= productStateMap.containsKey(product.id)?productStatemap.get(product.id).isSelect:prodPBEMap.get(product.id).IsActive;
							wrObj.isActive= prodPBEMap.get(product.id).IsActive;
                            /**DEFECT 47511 FIX END**/

                            wrObj.UseStandard = prodPBEMap.get(product.id).UseStandardPrice;

							/**DEFECT 47511 FIX START**/
							productOriginalStateMap.put(product.id,prodPBEMap.get(product.id).IsActive);
							/**DEFECT 47511 FIX END**/

                            if(wrObj.UseStandard){
                                wrObj.listPrice = wrObj.standardPrice;

                            }
                        }
						/**DEFECT 47511 FIX START**/
                        else{
                           wrObj.isSelect= productStateMap.containsKey(product.id)?productStatemap.get(product.id).isSelect:false; 
                           productOriginalStateMap.put(product.id,false); 

                        }
                        /**DEFECT 47511 FIX END**/
                        wrapperList.add(wrObj);

                    }   
                } 
            }
            
        }
        
    }
    /**
     * @author <Ashok Arora>
     * @description <This method is used to instantiate 'WrapperProduct' inner class>
     * @name <createWrapperInstance>
     * @date <07/06/2016>
     * @param Product2, Id, Decimal, Integer
     * @return WrapperProduct
     */
    private WrapperProduct createWrapperInstance(Product2 pr, decimal unitprice, integer index){
        return new WrapperProduct(pr,unitprice, index);
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to instantiate 'EP_ChangeRequestLine' custom object with pre-populated details>
     * @name <assignValues>
     * @date <07/06/2016>
     * @param WrapperProduct, Id, String, String
     * @return EP_ChangeRequestLine
     */
    private EP_ChangeRequestLine__c assignValues(WrapperProduct wrapProduct,Id reqliItemRtType,String rType,String fieldApiName){
        EP_ChangeRequestLine__c requestLine = new EP_ChangeRequestLine__c();
        requestLine.EP_Product_Name__c = wrapProduct.Product.Id;
        requestLine.RecordTypeId = reqliItemRtType;
        requestLine.EP_Request_Type__c = rType;
        requestLine.EP_Source_Field_API_Name__c = fieldApiName;
        requestLine.EP_New_Value__c = rType;
        requestLine.EP_List_Price__c = wrapProduct.standardPrice;
        requestLine.currencyISOCode = currencyCode;
        return requestLine;
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to populate options value for Page Size dropdown available on the page>
     * @name <paginationSizeOptions>
     * @date <07/06/2016>
     * @param none
     * @return List<SelectOption>
     */
    private List<SelectOption> paginationSizeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(EP_Common_Constant.NUM_5,EP_Common_Constant.NUM_5));
        options.add(new SelectOption(EP_Common_Constant.NUM_10,EP_Common_Constant.NUM_10));
        options.add(new SelectOption(EP_Common_Constant.NUM_20,EP_Common_Constant.NUM_20));
        options.add(new SelectOption(EP_Common_Constant.NUM_50,EP_Common_Constant.NUM_50));
        options.add(new SelectOption(EP_Common_Constant.NUM_100,EP_Common_Constant.NUM_100));
        return options;
    }
    
    /**
     * @Author <Ashok Arora>
     * @Name <WrapperProduct>
     * @CreateDate <07/06/2016>
     * @Description <This is the inner class used as Wrapper for Pricebook details>
     * @Version <1.0>
     */
    public without sharing class WrapperProduct{
        public Boolean isSelect {get;set;}
        public Boolean IsActive {get;set;}
        public Product2 product{get;set;}
        public Boolean UseStandard{get;set;}
        public boolean packagedProduct {get; set;}
        public Decimal standardPrice{get;set;}
        public Decimal listPrice{get;set;}
        public Integer rowIndex{get;set;} 
        
        public boolean stdPriceBookEntryNotDefined {get; set;}
        
        /**
         * @author <Ashok Arora>
         * @description <Default Constructor for Inner Class>
         * @name <WrapperProduct>
         * @date <07/06/2016>
         * @param Product2, Decimal, Integer
         * @return none
         */
        public WrapperProduct(Product2 product, Decimal standardPrice, Integer rIndex){
            IsActive = false;
            isSelect = false;
            UseStandard = false;
            stdPriceBookEntryNotDefined = false;
            this.product = product; 
            this.standardPrice = standardPrice;
            this.listPrice = listPrice;
            this.rowIndex = rIndex;
            if(product != null && EP_Common_Constant.PRODUCT_PACKAGED.equalsIgnoreCase(product.EP_Product_Sold_As__c)){
                this.packagedProduct = true;
            }else{
                this.packagedProduct = false;
            }
        }
    }
    
}