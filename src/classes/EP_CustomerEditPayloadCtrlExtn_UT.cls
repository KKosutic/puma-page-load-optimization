@isTest
public class EP_CustomerEditPayloadCtrlExtn_UT
{
    static testMethod void setShipToAddressesForLS_WithSellTo() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario() ;
        Account sellTo = [select id from Account where Id =: acc.ParentId];
        sellTo.EP_Requested_Payment_Terms__c=EP_Common_Constant.PREPAYMENT;
        sellTo.EP_Requested_Packaged_Payment_Term__c=EP_Common_Constant.PREPAYMENT;
        update sellTo;
        
        PageReference pageRef = Page.EP_CustomerEditWithWINDMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new account(Id= acc.ParentId));
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.ParentId);
        EP_CustomerEditPayloadCtrlExtn localObj = new EP_CustomerEditPayloadCtrlExtn(sc);
        
        //remove unused classes
        EP_CustomerEditPayloadCtrlExtn.bankAccountWrapper bankWrapper = new EP_CustomerEditPayloadCtrlExtn.bankAccountWrapper();
        bankWrapper.bankAcc = new EP_Bank_Account__c();
        bankWrapper.bankAccountSeqId = '';
        EP_CustomerEditPayloadCtrlExtn.shipToAddressWrapper shipWrapper = new EP_CustomerEditPayloadCtrlExtn.shipToAddressWrapper();
        shipWrapper.shipToAddress = new Account();
        shipWrapper.shipToSeqId = '';
        
        Test.startTest();
        Integer deferUpperLimit = localObj.deferUpperLimit;
        string shipToStatus = localObj.shipToAddressesLS[0].shipToStatus;
        localObj.setShipToAddressesForLS();
        Test.stopTest();     
        System.AssertEquals(true,!localObj.shipToAddressesLS.isEmpty());
    }
    
    static testMethod void setShipToAddressesForLS_WithShipTo() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario() ;
        Account sellTo = [select id from Account where Id =: acc.ParentId];
        sellTo.EP_Requested_Payment_Terms__c=EP_Common_Constant.PREPAYMENT;
        sellTo.EP_Requested_Packaged_Payment_Term__c=EP_Common_Constant.PREPAYMENT;
        update sellTo;
        
        acc = [select id,Parent.EP_Requested_Payment_Terms__c,Parent.EP_Requested_Packaged_Payment_Term__c,EP_Defer_Upper_Limit__c,EP_Requested_Payment_Terms__c,EP_Requested_Packaged_Payment_Term__c from Account where id=: acc.Id];
        
        PageReference pageRef = Page.EP_CustomerEditWithWINDMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.Id);
        EP_CustomerEditPayloadCtrlExtn localObj = new EP_CustomerEditPayloadCtrlExtn(sc);
        
        Test.startTest();
        Integer deferUpperLimit = localObj.deferUpperLimit;
        string shipToStatus = localObj.shipToAddressesLS[0].shipToStatus;
        localObj.setShipToAddressesForLS();
        Test.stopTest();     
        System.AssertEquals(true,!localObj.shipToAddressesLS.isEmpty());
    }
    
    static testMethod void checkPageAccess_test() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario() ;
        ApexPages.StandardController sc = new ApexPages.StandardController(new account(Id=acc.Id));
        EP_CustomerEditPayloadCtrlExtn localObj = new EP_CustomerEditPayloadCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true, result!= null);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
}