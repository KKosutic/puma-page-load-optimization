@isTest
public class EP_PricngResponse_Test{
    private static Account sellToAccount;
    private static Account shipToAccount;
    private static Account storageLocation;
    @testSetup static void init() {
         EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true; 
         EP_TestDummyData.intializePricingHandlerData();
    }
    public static testMethod void errorInPricingTest(){
        List<StaticResource> sr = [Select body, name from StaticResource where Name = 'EP_Error_Pricing_Response'];
        String json = sr[0].body.toString();
        System.debug('$$$$$$$$$$$$JSON STRING:' + json);
        Order o = [Select id, OrderNumber from Order where RecordType.DeveloperName = 'EP_Non_VMI_Orders'];
        for(Account acc : [Select id, RecordType.DeveloperName, AccountNumber, EP_Nav_Stock_Location_Id__c from Account]){
            System.debug('------------'+acc);
            if(acc.RecordType.DeveloperName == 'EP_Sell_To'){
                sellToAccount = acc;
            }
            else if(acc.RecordType.DeveloperName == 'EP_Non_VMI_Ship_To'){
                shipToAccount = acc;
            }
        }
        json = json.replace('<custId>',sellToAccount.AccountNumber);
        json = json.replace('<ShipToId>',shipToAccount.AccountNumber);
        json = json.replace('<OrderId>',o.OrderNumber);
        json = json.replace('<seqId>',o.id);
        test.StartTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = 'https://cs81.salesforce.com/v1/PricingResponse/';  
        
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        EP_PricingHandler.UpdatePRICING(EP_PricingHandler.parse(json)); 
        test.StopTest();
    }
    public static testMethod void errorInPricingIncorrectJsonTest(){
        List<StaticResource> sr = [Select body, name from StaticResource where Name = 'EP_Incorrect_JSON'];
        String json = sr[0].body.toString();
        System.debug('$$$$$$$$$$$$JSON STRING:' + json);
        Order o = [Select id, OrderNumber from Order where RecordType.DeveloperName = 'EP_Non_VMI_Orders'];
        for(Account acc : [Select id, RecordType.DeveloperName, AccountNumber, EP_Nav_Stock_Location_Id__c from Account]){
            System.debug('------------'+acc);
            if(acc.RecordType.DeveloperName == 'EP_Sell_To'){
                sellToAccount = acc;
            }
            else if(acc.RecordType.DeveloperName == 'EP_Non_VMI_Ship_To'){
                shipToAccount = acc;
            }
        }
        json = json.replace('<custId>',sellToAccount.AccountNumber);
        json = json.replace('<ShipToId>',shipToAccount.AccountNumber);
        json = json.replace('<OrderId>',o.OrderNumber);
        json = json.replace('<seqId>',o.id);
        test.StartTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = 'https://cs81.salesforce.com/v1/PricingResponse/';  
        
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        EP_PricingHandler.UpdatePRICING(EP_PricingHandler.parse(json)); 
        test.StopTest();
    }
    public static testMethod void testPricingResponse(){
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;     
        List<StaticResource> sr = [Select body, name from StaticResource where Name = 'EP_PricingResponse'];
        String json = sr[0].body.toString();
        System.debug('$$$$$$$$$$$$JSON STRING:' + json);
        Order o = [Select id, OrderNumber from Order where RecordType.DeveloperName = 'EP_Non_VMI_Orders'];
        List<OrderItem> oi = [Select id, EP_OrderItem_Number__c, PriceBookEntryId from OrderItem where OrderId =: o.id];
        PriceBookEntry pe = [Select id, Product2Id,Product2.ProductCode, PriceBook2.Id from PriceBookEntry where id =: oi[0].PriceBookEntryId];
        for(Account acc : [Select id, RecordType.DeveloperName, AccountNumber, EP_Nav_Stock_Location_Id__c from Account]){
            System.debug('------------'+acc);
            if(acc.RecordType.DeveloperName == 'EP_Sell_To'){
                sellToAccount = acc;
            }
            else if(acc.RecordType.DeveloperName == 'EP_Non_VMI_Ship_To'){
                shipToAccount = acc;
            }
        }
        System.debug('#################'+pe);
        json = json.replace('<custId>',sellToAccount.AccountNumber);
        json = json.replace('<ShipToId>',shipToAccount.AccountNumber);
        json = json.replace('<OrderId>',o.OrderNumber);
        json = json.replace('<lineId>',String.valueOf(oi[0].EP_OrderItem_Number__c));
        json = json.replace('<itemId>',pe.Product2.ProductCode);
        json = json.replace('<seqId>',o.id);
        System.debug('########'+json);
        test.StartTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = 'https://cs81.salesforce.com/v1/PricingResponse/';  
        
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        EP_PricingHandler.UpdatePRICING(EP_PricingHandler.parse(json)); 
        test.StopTest();
                
    }
}