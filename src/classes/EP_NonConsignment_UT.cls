/**
  * @Author      : Accenture
  * @name        : EP_NonConsignment_UT
  * @CreateDate  : 3/17/2017
  * @Description : Test class for EP_NonConsignment
  * @Version     : <1.0>
  * @reference   : N/A
*/
@isTest
private class EP_NonConsignment_UT {
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void hasCreditIssue_positivetest() {
        EP_NonConsignment localObj = new EP_NonConsignment();       
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Test.startTest();
        Boolean result = localObj.hasCreditIssue(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasCreditIssue_negativetest() {
        EP_NonConsignment localObj = new EP_NonConsignment();
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
        Test.startTest();
        Boolean result = localObj.hasCreditIssue(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void findRecordType_test() {
        EP_NonConsignment localObj = new EP_NonConsignment();
        EP_VendorManagement vmType = new EP_VendorManagement();
        Test.startTest();
        Id result = localObj.findRecordType(vmType);
        Test.stopTest();
        System.AssertEquals(null,result);
    }

    static testMethod void calculatePrice_test() {
        EP_NonConsignment localObj = new EP_NonConsignment();
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Test.startTest();
        localObj.calculatePrice(ord);
        Test.stopTest();
        List<EP_IntegrationRecord__c> integRecords = [select id,EP_Error_Description__c,EP_Status__c,EP_SeqId__c from EP_IntegrationRecord__c];
        system.assertNotEquals(0, integRecords.size());
    }
}