/* 
   @Author          Accenture
   @name            EP_CustomerOtherAdjustmentMapper
   @CreateDate      05/05/2017
   @Description     Mapper class of Customer Othe Adjustment object
   @Version         1.0
*/
public with sharing class EP_CustomerOtherAdjustmentMapper {
    
    /**
    * @author           Accenture
    * @name             getCustomerOtherAdjustmentRecordsByUniqueId
    * @date             05/05/2017
    * @description      Method used to get the list of Customer Other Adjustment records by unique Id
    * @param            set<string>
    * @return           List<EP_Customer_Other_Adjustment__c>
    */
    public List<EP_Customer_Other_Adjustment__c> getCustomerOtherAdjustmentRecordsByUniqueId(set<string> setOfCustomerAdjustments){
        EP_GeneralUtility.Log('Public','EP_CustomerOtherAdjustmentMapper','getCustomerOtherAdjustmentRecordsByUniqueId');
        list<EP_Customer_Other_Adjustment__c> custAdjustList = new list<EP_Customer_Other_Adjustment__c>();
        for (List<EP_Customer_Other_Adjustment__c> custAdjObjList: [select Id, EP_Adjustment_Unique_Key__c,Name from 
                                                                    EP_Customer_Other_Adjustment__c where 
                                                                    EP_Adjustment_Unique_Key__c in : setOfCustomerAdjustments]){
            custAdjustList.addAll(custAdjObjList); 
        }      
        return custAdjustList;
    }
    
    /**
    * @author           Accenture
    * @name             getCustomerOtherAdjustmentMapByUniqueId
    * @date             05/05/2017
    * @description      Method used to get the map of Customer Other Adjustment records by unique Id
    * @param            set<string>
    * @return           Map<String,EP_Customer_Other_Adjustment__c>
    */
    public Map<String,EP_Customer_Other_Adjustment__c> getCustomerOtherAdjustmentMapByUniqueId(set<string> setOfCustomerAdjustments){
        EP_GeneralUtility.Log('Public','EP_CustomerOtherAdjustmentMapper','getCustomerOtherAdjustmentMapByUniqueId');
        Map<String,EP_Customer_Other_Adjustment__c> mapCustomerAdjustments = new Map<String,EP_Customer_Other_Adjustment__c>();
        for(EP_Customer_Other_Adjustment__c custAdjust :  getCustomerOtherAdjustmentRecordsByUniqueId(setOfCustomerAdjustments)){
            mapCustomerAdjustments.put(custAdjust.EP_Adjustment_Unique_Key__c, custAdjust);
        }
        return mapCustomerAdjustments;
    }
}