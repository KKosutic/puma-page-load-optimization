/**
  * @Author <Accenture>
  * @Name setAccountDomainObject
  **/ 
public  class EP_ASTSellToBasicDataToAccountSetUp  extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTSellToBasicDataToAccountSetUp
    */
    public EP_ASTSellToBasicDataToAccountSetUp() {
        finalState = EP_AccountConstant.ACCOUNTSETUP;   
    }
    /**
    * @Author <Accenture>
    * @Name isTransitionPossible
    **/
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToAccountSetUp','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @Author <Accenture>
    * @Name isRegisteredForEvent
    **/
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToAccountSetUp','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    * @Author <Accenture>
    * @Name isGuardCondition
    **/
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToAccountSetUp','isGuardCondition');
        //L4_45352_START
        /*
        system.debug('Checking Guard conditions for Basic Data Setup to Account Set-up.......');
        EP_AccountService service = new EP_AccountService(this.account);
        
        system.debug('## 1. Checking if all the review actions are completed for sell to and its related ship to if any......');
        if(!service.isReviewActionsCompleted()){
            accountEvent.isError = true;
            accountEvent.setEventMessage('Please complete all review actions');
            return false;
        }
        system.debug('## 2. Checking if  if Delivery type is Delivery, then one Ship To with  Account set setup status is required.....');
        if(EP_AccountConstant.DELIVERY.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isSetUpShipToAssociated()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(System.Label.EP_No_Set_Up_Ship_To);
                return false;
            }    
        }
        
        system.debug('## 3. Checking if Bank Account Required .....');
        if(service.isBankAccountRequired()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.Label.EP_NOActiveBankAcc);
            return false;   
        }
        */
        //L4_45352_End        
        return true;
    }
    /**
    * @Author <Accenture>
    * @Name doOnExit
    **/    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToAccountSetUp','doOnExit');
        EP_AccountService service = new EP_AccountService(this.account);
        service.setCustomerActivationDate();
        //service.setSentNAVWINDMFlag();        
    }
}