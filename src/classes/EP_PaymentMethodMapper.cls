/**
   @Author          CR Team
   @name            EP_PaymentMethodMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to EP_PaymentMetho__c Object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_PaymentMethodMapper {
        
        
    /**
    *  Constructor. 
    *  @name            EP_PaymentMethodMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */
    public EP_PaymentMethodMapper() {
        
    }
    
    /* *  This method returns Direct Debit record counts
    *  @name             getDirectDebitCountById
    *  @param            Set<id>
    *  @return           list<EP_Payment_Method__c>
    *  @throws           NA
    */ 
    public Integer getDirectDebitCountById(Set<Id> recIds) {
        EP_GeneralUtility.Log('Public','EP_PaymentMethodMapper','getDirectDebitCountById');
        return [SELECT count() FROM EP_Payment_Method__c 
        WHERE id IN : recIds and Name = :EP_Common_Constant.DIRECT_DEBIT];
    }
    
    /* *  This method returns Payment Method Records by Name.
    *  @name             getRecordsByName
    *  @param            list<id>, Integer
    *  @return           list<EP_Payment_Method__c>
    *  @throws           NA
    */ 
    public list<EP_Payment_Method__c> getRecordsByName(string methodName) {
        EP_GeneralUtility.Log('Public','EP_PaymentMethodMapper','getRecordsByName');
        list<EP_Payment_Method__c> payMethObjList = new list<EP_Payment_Method__c>();
        for(list<EP_Payment_Method__c> payMethList : [   SELECT 
            id
            , Name 
            , EP_Payment_Method_Code__c
            FROM EP_Payment_Method__c 
            WHERE name =: methodName
            ]){
            payMethObjList.addAll(payMethList );                                   
        }                               
        return payMethObjList ;
    }
    
    public List<EP_Payment_Method__c> getRecordsByPaymentMethodCode(Set<string> setCustomerPaymentMtds){
    	return [SELECT Id, EP_Payment_Method_Code__c FROM EP_Payment_Method__c WHERE EP_Payment_Method_Code__c IN:setCustomerPaymentMtds];
    }
    
    /**
	* @author 			Accenture
	* @name				getPaymentMethodsMapByCodes
	* @date 			03/13/2017
	* @description 		Will return the map of payment method records of codes and Id.
	* @param 			Set<String>
	* @return 			Map<String,Id>
	*/
    public Map<String,Id> getPaymentMethodsMapByCodes(Set<String> setCustomerPaymentMtds){
        EP_GeneralUtility.Log('Private','EP_PaymentMethodMapper','getPaymentMethodsMapByCodes');
        Map<String,Id> mapPaymentMtdPaymentIDs = new Map<String,Id>();
        if(!setCustomerPaymentMtds.isEmpty()){
        	for(EP_Payment_Method__c pymntMtd : getRecordsByPaymentMethodCode(setCustomerPaymentMtds)){
                mapPaymentMtdPaymentIDs.put(pymntMtd.EP_Payment_Method_Code__c.toUpperCase(),pymntMtd.id);
            } 
        }
        return mapPaymentMtdPaymentIDs;
    }
    /**Defect fix 71439**/
	/**
	* @author 			Accenture
	* @name				getPaymentMethodCodeById
	* @date 			12/18/2017
	* @description 		Will return the map of payment method code
	* @param 			id
	* @return 			string
	*/
	public static string getPaymentMethodCodeById(string id){
		EP_GeneralUtility.Log('public','EP_PaymentMethodMapper','getPaymentMethodCodeById');
        return [SELECT Id, EP_Payment_Method_Code__c FROM EP_Payment_Method__c WHERE Id=: id limit 1 ].EP_Payment_Method_Code__c;    
	} 
	/**Defect fix 71439**/
	
	/**BugFix-71967**/
	/**
	* @author 			Accenture
	* @name				getPaymentMethodMapForCompany
	* @date 			01/05/2018
	* @description 		To get Map of payment method for a company
	* @param 			id
	* @return 			Map<Id,string>
	*/
	public static Map<Id,string> getPaymentMethodMapForCompany (Id companyId){
		Map<Id,string> paymentMethodMap = new Map<Id,string>();
		for(EP_Payment_Method__c paymtdh : [SELECT Id, EP_Payment_Method_Code__c,EP_Company__c FROM EP_Payment_Method__c WHERE EP_Company__c =: companyId]){
			paymentMethodMap.put(paymtdh.id,paymtdh.EP_Payment_Method_Code__c);
		}
		return paymentMethodMap;
	}
	/**BugFix-71967**/
}