@isTest
public class EP_OrderModificationPageController_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

    static testMethod void isOrderEditable_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        Boolean result = localObj.isOrderEditable(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderEditable_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ord.Status__c = EP_OrderConstant.OrderState_Delivered;
        Update ord;
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        Boolean result = localObj.isOrderEditable(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void loadAccountOrderDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder(); 
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.loadAccountOrderDetails();
        Test.stopTest();
        System.AssertEquals(2,localObj.ctx.intOrderCreationStepIndex);       
    }
    
    static testMethod void loadAccountOrderDetails_Negative_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.strSelectedOrderID = EP_Common_Constant.BLANK;
        Test.startTest();
        localObj.loadAccountOrderDetails();
        Test.stopTest();
        System.AssertEquals(1,localObj.ctx.intOrderCreationStepIndex);       
    }
    
    static testMethod void loadAccountOrderDetails_ExRack_test() {
        csord__Order__c ord = EP_TestDataUtility.getExRackOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.loadAccountOrderDetails();
        Test.stopTest();
        System.AssertEquals(3,localObj.ctx.intOrderCreationStepIndex);       
    }
    
    static testMethod void loadExistingOrderItems_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.loadExistingOrderItems();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void back_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.intOrderCreationStepIndex = 1;
        Test.startTest();
        localObj.back();
        Test.stopTest();
        System.AssertEquals(0,localObj.ctx.intOrderCreationStepIndex);
        System.AssertEquals(2,localObj.ctx.pricingPollerCount );
        System.AssertEquals(false,localObj.ctx.hasAccountSetupError );
    }
    static testMethod void next_Step1Test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord );
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.intOrderCreationStepIndex = 1;
        Test.startTest();
        localObj.next();
        Test.stopTest();
        System.AssertEquals(2,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void next_Step2Test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord );
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.intOrderCreationStepIndex = 2;
        Test.startTest();
        localObj.next();
        Test.stopTest();
        System.AssertEquals(3,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void next_Step3Test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord );
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.intOrderCreationStepIndex = 3;
        localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
        Test.startTest();
        localObj.next();
        Test.stopTest();
        //Delegates to Modification helper, different scenarios will be covered there
        System.AssertEquals(3,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void next_Step4Test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord );
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.intOrderCreationStepIndex = 4;
        Test.startTest();
        localObj.next();
        Test.stopTest();
        System.AssertEquals(5,localObj.ctx.intOrderCreationStepIndex);
    }
    static testMethod void setRun_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.setRun();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void removeOrderLineItem_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.removeOrderLineItem();
        Test.stopTest();
        System.AssertEquals(True,localObj.ctx.isPriceCalculated);
    }
    static testMethod void addNewLine_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.addNewLine();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void cancel_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        //EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
       // localObj.ctx.isPricingRequired;
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.next();
        localObj.next();
        localObj.next();
        localObj.calculatePrice();
        Test.startTest();
        PageReference result = localObj.cancel();
        Test.stopTest();
        system.debug('Result is ' + result.getURL());
        system.debug('ord id is ' + result.getURL());
        System.AssertEquals(true,result.getURL().contains(ord.id));
    }
    
     static testMethod void cancel_NewPageRef_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.next();
        localObj.next();
        localObj.next();
        localObj.calculatePrice();
        localObj.ctx.isPricingRequired = true;
        Test.startTest();
        PageReference result = localObj.cancel();
        Test.stopTest();
        System.AssertEquals(true,result.getURL().contains(ord.id));
    }
    
    static testMethod void cancelOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.newOrderRecord = ord;
        Test.startTest();
        PageReference result = localObj.CancelOrder();
        Test.stopTest();    
        csord__Order__c modOrder = [SELECT Id, Status__c FROM csord__Order__c WHERE Id =: ord.Id];
        //System.AssertEquals(EP_OrderConstant.CANCELLED_STATUS, modOrder.status); // need to ask what will be the status of order after cancelling the order
        System.AssertEquals(EP_OrderConstant.OrderState_Draft, modOrder.Status__c);

    }
    static testMethod void openInvoicesOverduePage_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        PageReference result = localObj.openInvoicesOverduePage();
        Test.stopTest();
        System.AssertEquals(true,result.getURL().contains(localObj.ctx.orderAccount.Id));
    }
    static testMethod void submit_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.newOrderRecord = ord;
        Test.startTest();
        PageReference result = localObj.submit();
        Test.stopTest();
        System.AssertEquals(true,result.getURL().contains(localObj.ctx.newOrderRecord.Id));
        System.AssertEquals(ord.Status__c,localObj.ctx.newOrderRecord.Status__c);
    }
    static testMethod void saveOrderAsDraft_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.hasOverdueInvoices = true;
        Test.startTest();
        PageReference result = localObj.saveOrderAsDraft();
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.ORDER_DRAFT_STATUS,ord.Status__c);       
    }
    static testMethod void isProductAvailable_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        //OrderItem oi = EP_TestDataUtility.createOrderItem(ord.id,Test.getStandardPricebookId());
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        //localObj.ctx.newOrderRecord = ord;
        //intentionally giving wrong id
        localObj.ctx.strSelectedPickupLocationID = ord.Stock_Holding_Location__c;
        localobj.ctx.listOfSupplyLocationOptions = new List < SelectOption >();
        LIST<EP_Stock_Holding_Location__c> listOfStockHoldingLocations = [select id,Stock_Holding_Location__r.Name from EP_Stock_Holding_Location__c];
        localobj.hlp.populatesupplylocations(listOfStockHoldingLocations);

        Test.startTest();
        localObj.isProductAvailable();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.prodNotAvailAtSuppLoc);
    }
    
    static testMethod void isProductAvailable_NoInventory_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        localObj.ctx.strSelectedPickupLocationID = ord.Stock_Holding_Location__c;
        localobj.ctx.listOfSupplyLocationOptions = new List < SelectOption >();
        LIST<EP_Stock_Holding_Location__c> listOfStockHoldingLocations = [select id,Stock_Holding_Location__r.Name from EP_Stock_Holding_Location__c];
        localobj.hlp.populatesupplylocations(listOfStockHoldingLocations);
        delete [Select Id FROM EP_Inventory__c];
        Test.startTest();
        localObj.isProductAvailable();
        Test.stopTest();
        System.AssertEquals(Label.EP_ProductNotAvail_At_New_SL,localObj.productNotAvailableMsg);
    }
    
    static testMethod void getPricing_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.getPricing();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true); 
    }
    static testMethod void setPricingVariable_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.setPricingVariable();
        Test.stopTest();
        System.AssertEquals(false,localObj.ctx.isPriceCalculated);
    }
    static testMethod void doUpdateSupplierContracts_test() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ord.EP_Loading_Date__c = null;
        update ord;

        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        PageReference pagRef = Page.EP_OrderModificationPage;       
        OrderItem oiq = [select id,EP_3rd_Party_Stock_Supplier__c,quantity from orderitem where OrderId= :ord.id limit 1];
        localObj.loadExistingOrderItems();
        
        Test.startTest();
        localObj.doUpdateSupplierContracts();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void calculatePrice_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.calculatePrice();
        Test.stopTest();
        System.AssertEquals(true,localObj.ctx.isPriceCalculated );
        System.AssertEquals(false,localObj.ctx.isPricingRequired);
    }
    static testMethod void displayProductNameAndSafeFillLevel_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.displayProductNameAndSafeFillLevel();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void updateOrderItemLines_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Test.startTest();
        localObj.updateOrderItemLines();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void isErrorInPage_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Boolean blnExport = localObj.showExportedCheckBox;
        Test.startTest();
        Boolean result = localObj.isErrorInPage;
        Test.stopTest();
        System.AssertEquals(false,result);
        }
        
     static testMethod void isPrepaymentCustomer_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Boolean blnExport = localObj.showExportedCheckBox;
        Test.startTest();
        Boolean result = localObj.isPrepaymentCustomer;
        Test.stopTest();
        System.AssertEquals(false,result);
        }
        
     static testMethod void showAddNewLineButton_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Boolean blnExport = localObj.showExportedCheckBox;
        Test.startTest();
        Boolean result = localObj.showAddNewLineButton;
        Test.stopTest();
        System.AssertEquals(false,result);
        }
        
      static testMethod void showNextButton_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
        Boolean blnExport = localObj.showExportedCheckBox;
        Test.startTest();
        Boolean result = localObj.showNextButton;
        Test.stopTest();
        System.AssertEquals(true,result);
        }
        
        static testMethod void showSpinButton_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            Test.startTest();
            Boolean result = localObj.showSpinButton;
            Test.stopTest();
            System.AssertEquals(false,result);
        }
        
        static testMethod void showBackButton_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            localObj.next();
            localObj.next();
            Test.startTest();
            Boolean result = localObj.showBackButton;
            Test.stopTest();
            System.AssertEquals(true,result);
        }
        
        static testMethod void showBackButton_ExRack_test() {
            csord__Order__c ord = EP_TestDataUtility.getExRackOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            localObj.ctx.intOrderCreationStepIndex = 3;
            System.debug('ctx.isDeliveryOrder##'+localObj.ctx.isDeliveryOrder);
            localObj.next();
            localObj.next();
            System.debug('test##'+localObj.ctx.intOrderCreationStepIndex);
            Test.startTest();
            Boolean result = localObj.showBackButton;
            Test.stopTest();
            System.AssertEquals(true,result);
        }
        
        static testMethod void showMessageSupplyLocationMissing_test() {
            PageReference pageRef = Page.EP_OrderModificationPage;
            Test.setCurrentPage(pageRef);
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, ord.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);           
            Test.startTest();
            localObj.showMessageSupplyLocationMissing();
            Test.stopTest();
            // assertion not required since method is void , hence adding dummy assert
            System.Assert(true);
        }
        
        static testMethod void userCurrencyFormat_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            Test.startTest();
            String result = localObj.userCurrencyFormat;
            Test.stopTest();
            System.Assert(result != NULL);
        }
        
        static testMethod void supplyLocTransportEditable_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            Test.startTest();
            Boolean result = localObj.supplyLocTransportEditable;
            Test.stopTest();
            System.AssertEquals(true,result);
        }
       
       static testMethod void showCancelButton_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            localObj.next();
            Test.startTest();
            Boolean result = localObj.showCancelButton;
            Test.stopTest();
            System.AssertEquals(true,result);
        }
        
        static testMethod void calculatePrice_cover_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);
            System.debug('pricebook1## ' + ord.csord__Account__r.EP_PriceBook__c);
            localObj.next();
            localObj.next();
            localObj.next();
            System.debug('step##'+localObj.ctx.intOrderCreationStepIndex);
            Test.startTest();
            localObj.calculatePrice();
            System.debug('pricebook2## ' + ord.csord__Account__r.EP_PriceBook__c);
            Test.stopTest();            
            System.AssertEquals(true,localObj.ctx.isPriceCalculated);
        }
        
        static testMethod void orgPckgdOrderMap_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);           
            Test.startTest();
            Map<String,csord__Order__c> result = localObj.orgPckgdOrderMap;
            Test.stopTest();            
            System.Assert(result.size() > 0);
        }
        
        static testMethod void showOrderConfirmationTab_test() {
            csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);
            EP_OrderModificationPageController localObj = new EP_OrderModificationPageController(sc);           
            Test.startTest();
            Boolean result = localObj.showOrderConfirmationTab;
            Test.stopTest();            
            System.AssertEquals(true,result);
        }     
}