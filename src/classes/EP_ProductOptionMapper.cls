/*
   @Author          Accenture
   @Name            EP_ProductOptionMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to Product Option Object
   @Version         1.0
   @Reference       NA
*/
/***L4-45352 start****/
public class EP_ProductOptionMapper {
    /****************************************************************
    * @author       Accenture                                       *
    * @Description  Get product Options associated active or bsic 
    * data set up accounts and to pricebook                         *
    * @Name         getProductOptionLinkedToPriceBook 
    * @param        Price Book Id                                   *
    * @return       List                                            *
    ****************************************************************/
    public static list<EP_Product_Option__c> getProductOptionLinkedToPriceBook(Id ProcebookId){
        list<EP_Product_Option__c> ProductOptnList = new list<EP_Product_Option__c>();
        for(EP_Product_Option__c VarPO : [select id,Price_List__c,Sell_To__c,Sell_To__r.EP_Status__c from EP_Product_Option__c where Price_List__c = : ProcebookId and  (Sell_To__r.EP_Status__c = : EP_Common_Constant.STATUS_ACTIVE OR Sell_To__r.EP_Status__c = : EP_Common_Constant.STATUS_BASIC_DATA_SETUP)]){
            ProductOptnList.add(VarPO);
            
        }
        return ProductOptnList;
    }
    
    /****************************************************************
    * @author       Accenture                                       *
    * @Description  Get product Options associated to pricebook     *
    * @Name         getProductOptionLinkedToPriceBook 
    * @param        set of Price Book Ids                           *
    * @return       map Price Book Id To list of Product Option.
    *
    ****************************************************************/
    public static map<Id,list<EP_Product_Option__c>> getProductOptionLinkedToPriceBook(set<Id> ProcebookIds){
        map<Id,list<EP_Product_Option__c>> mapIdTolstProdOption = new map<Id,list<EP_Product_Option__c>>();
        for(EP_Product_Option__c productOption : [select id,Price_List__c,Sell_To__c,Sell_To__r.EP_Status__c from EP_Product_Option__c where Price_List__c IN : ProcebookIds]){
            if(!mapIdTolstProdOption.containskey(productOption.Price_List__c)){
                mapIdTolstProdOption.put(productOption.Price_List__c,new list<EP_Product_Option__c>());
            }
            mapIdTolstProdOption.get(productOption.Price_List__c).add(productOption);     
        }
        return mapIdTolstProdOption;
    }
    
    /****************************************************************
    * @author       Accenture                                       *
    * @Description  Get product Options associated to sellTo        *
    * @Name         getProductOptionLinkedToSellTo 
    * @param        Price Book Id                                   *
    * @return       List                                            *
    ****************************************************************/
    public static list<EP_Product_Option__c> getProductOptionLinkedToSellTo(Id sellToId){
        list<EP_Product_Option__c> ProductOptnList = new list<EP_Product_Option__c>();
        for(EP_Product_Option__c VarPO : [select id,Price_List__c,Sell_To__c,Sell_To__r.EP_Status__c from EP_Product_Option__c where Sell_To__c = : sellToId]){
            ProductOptnList.add(VarPO);            
        }
        return ProductOptnList;
    }
      /***L4-45352 end****/
}