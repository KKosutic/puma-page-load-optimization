/* 
  @Author : Accenture
  @name : EP_GenerateCreditHoldingXMLWithNav
  @CreateDate :10/Nov/2017
  @Description :This class is use to created the payload XML for credit Holding interface 
  @Version : 1.0
*/

public with sharing class EP_GenerateCreditHoldingXMLWithNav extends EP_GenerateCreditHoldingRequestXML{
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Credit Holding to Sync with NAV
    * @param            NA
    * @return           NA
    */      
    public override void createPayload(){ 
        EP_GeneralUtility.Log('Public','EP_GenerateCreditHoldingRequestXML','createPayload');
        Dom.XMLNode PayloadNode = MSGNode.addChildElement('Payload',null,null);
        Dom.XmlNode AnyNode = PayloadNode.addChildElement('any0',null, null);
        Dom.XMLNode CreditHoldingNodes = AnyNode.addChildElement('customerHoldingEntity',null, null);
        Dom.XMLNode CreditHoldingNode = CreditHoldingNodes.addChildElement('customerHoldingEntity',null, null);
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objCreditHolding.Id); 
        CreditHoldingNode.addChildElement('seqId',null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        
        Dom.XMLNode IdentifierNode = CreditHoldingNode.addChildElement('Identifier',null,null);
        IdentifierNode.addChildElement('custHoldingId',null,null).addTextNode(getValueforNode(objCreditHolding.EP_holding_number__c));
        IdentifierNode.addChildElement('entrprsId',null,null).addTextNode(getValueforNode(objCreditHolding.EP_enterprise_id__c));
        IdentifierNode.addChildElement('clientId',null,null).addTextNode(getValueforNode(objCreditHolding.EP_company__r.EP_Company_Code__c)); 
        
        CreditHoldingNode.addChildElement('name',null,null).addTextNode(getValueforNode(objCreditHolding.Name));
        CreditHoldingNode.addChildElement('name2',null,null).addTextNode(getValueforNode(objCreditHolding.EP_Holding_Name_2__c));
        CreditHoldingNode.addChildElement('address',null,null).addTextNode(getValueforNode(objCreditHolding.EP_address_line_1__c));
        CreditHoldingNode.addChildElement('address2',null,null).addTextNode(getValueforNode(objCreditHolding.EP_address_line_2__c));
        CreditHoldingNode.addChildElement('city',null,null).addTextNode(getValueforNode(objCreditHolding.EP_city__c));
        CreditHoldingNode.addChildElement('phone',null,null).addTextNode(getValueforNode(objCreditHolding.EP_phone__c));
        CreditHoldingNode.addChildElement('mobilePhone',null,null).addTextNode(getValueforNode(objCreditHolding.EP_mobile_phone__c));
        CreditHoldingNode.addChildElement('currencyId',null,null).addTextNode(getValueforNode(objCreditHolding.EP_Seq_id__c)); //Todo
        CreditHoldingNode.addChildElement('cntryCode',null,null).addTextNode(getValueforNode(objCreditHolding.EP_country_code__c));
        CreditHoldingNode.addChildElement('fax',null,null).addTextNode(getValueforNode(objCreditHolding.EP_fax__c));
        CreditHoldingNode.addChildElement('postCode',null,null).addTextNode(getValueforNode(objCreditHolding.EP_postal_code__c));
        CreditHoldingNode.addChildElement('county',null,null).addTextNode(getValueforNode(objCreditHolding.EP_state__c));
        CreditHoldingNode.addChildElement('email',null,null).addTextNode(getValueforNode(objCreditHolding.EP_email__c));
        CreditHoldingNode.addChildElement('status',null,null).addTextNode(getValueforNode(objCreditHolding.EP_Status__c));
        Dom.XMLNode CustomerNode =CreditHoldingNode.addChildElement('customers',null,null);
        for(Account objAccount :lstAccount){
            CustomerNode.addChildElement('custId',null,null).addTextNode(getValueforNode(objAccount.AccountNumber));
        } 
    }
}