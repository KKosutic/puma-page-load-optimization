/**
   @Author          CR Team
   @name            EP_BankAccountMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to EP_BankAccount__c Object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_BankAccountMapper {

    /**
    *  Constructor. 
    *  @name            EP_BankAccountMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_BankAccountMapper() {
      
    } 
    
    /** This method returns Customer Bank Account Records.
    *  @name            getRecordsByAccountIds
    *  @param           list<Id>, Integer 
    *  @return          list<EP_Bank_Account__c>
    *  @throws          NA
    */
    public list<EP_Bank_Account__c> getRecordsByAccountIds(list<Id> idList) {
      EP_GeneralUtility.Log('Public','EP_BankAccountMapper','getRecordsByAccountIds');
      list<EP_Bank_Account__c> bnkAccObjList =  new list<EP_Bank_Account__c>();
      for(list<EP_Bank_Account__c> bnkAccList : [ SELECT Id, EP_Account__c
        FROM EP_Bank_Account__c
        WHERE EP_Account__c IN :idList
        ]){
        bnkAccObjList.addAll(bnkAccList);
      }       
      return bnkAccObjList; 
    }
    
    /** This method returns Customer Bank Account Record by account code
    *  @name            getRecordsByBankAccountCode
    *  @param           Set<string> 
    *  @return          list<EP_Bank_Account__c>
    *  @throws          NA
    */
    public list<EP_Bank_Account__c> getRecordsByBankAccountCode(Set<string> setOfBankAccountIDs){
    	return [SELECT Id, EP_NAV_Bank_Account_Code__c FROM EP_Bank_Account__c WHERE EP_NAV_Bank_Account_Code__c IN:setOfBankAccountIDs];
    }
    
    /** This method returns count of Customer Bank Account Records.
    *  @name            getRecordsByAccountIds
    *  @param           list<Id>, Integer 
    *  @return          list<EP_Bank_Account__c>
    *  @throws          NA
    */
    public Integer getBankAccountCount(Id AccountId) {
      EP_GeneralUtility.Log('Public','EP_BankAccountMapper','getBankAccountCount');
      return [Select count() from EP_Bank_Account__c Where EP_Account__c=:AccountId]; 
    }
	
	/**
	* @author 			Accenture
	* @name				getBankAccountsMapByCode
	* @date 			03/13/2017
	* @description 		Will return the map of bank account records for the set of bank accounts
	* @param 			Set<String>
	* @return 			Map<String,Id>
	*/
    public Map<String,EP_Bank_Account__c> getBankAccountsMapByCode(Set<String> setOfBankAccountIDs){
        EP_GeneralUtility.Log('Private','EP_CustomerUpdateHelper','getBankAccountsMap');
        Map<String,EP_Bank_Account__c> mapBankAccounts = new Map<String,EP_Bank_Account__c>();
        if(!setOfBankAccountIDs.isEmpty()){
        	for(EP_Bank_Account__c bank : getRecordsByBankAccountCode(setOfBankAccountIDs)){
                mapBankAccounts.put(bank.EP_NAV_Bank_Account_Code__c.toUpperCase(),bank);
            }
        }
        return mapBankAccounts;
    }
    
  }