public with sharing class EP_OrderControllerHelper_UX{
    public EP_OrderPageContext_UX ctx ;
    public EP_OrderLinesAndSummaryHelper_UX ordLinehlp;
    public static final String CLASSNAME = 'EP_OrderControllerHelper_UX';
    public EP_OrderControllerHelper_UX(EP_OrderPageContext_UX ctx){
        this.ctx = ctx;
        ordLinehlp = new EP_OrderLinesAndSummaryHelper_UX(ctx);
    }

    // previously loadstep2
    public void loadSupplyOptionsAndTanks() {
        EP_GeneralUtility.Log('public',CLASSNAME,'loadSupplyLocationOptions');
        system.debug('*******ctx.newOrderRecord.EP_ShipTo__r ' + ctx.newOrderRecord.EP_ShipTo__r);
        ctx.listOfSupplyLocationOptions = new List < SelectOption > ();
        ctx.isRoOrder = ctx.orderDomainObj.isRetrospective();
        ctx.setshiptodetailsonOrder();
        system.debug('*******ctx.newOrderRecord.EP_ShipTo__r ' + ctx.newOrderRecord.EP_ShipTo__r);
        System.debug(ctx.mapShipToTanks+'******'+ctx.mapShipToOperationalTanks+'***'+ctx.isDeliveryOrder);
        if(!ctx.mapShipToTanks.isEmpty() && ctx.mapShipToOperationalTanks.isEmpty() && ctx.isDeliveryOrder) { 
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_No_Operational_Tanks)); 
            return;
        }
        if(!ctx.mapShipToOperationalTanks.isEmpty() && ctx.isBulkOrder && ctx.isDeliveryOrder) {
            ctx.isOprtnlTankAvail = true;
        }
        system.debug('*******ctx.newOrderRecord.EP_ShipTo__r ' + ctx.newOrderRecord.EP_ShipTo__r + '*****' + (ctx.isDeliveryOrder || ctx.isDummy));
        Account applicableAccount = ((ctx.isDeliveryOrder || ctx.isDummy) ? ctx.newOrderRecord.EP_ShipTo__r : ctx.orderAccount);
        EP_AccountService shipToAccountService = new EP_AccountService(new EP_AccountDomainObject(applicableAccount));
        List <EP_Stock_Holding_Location__c> listOfStockHoldingLocations = shipToAccountService.getSupplyLocations();
        system.debug('*******listOfStockHoldingLocations '+ listOfStockHoldingLocations);
        populatesupplylocations(listOfStockHoldingLocations); 
        // If there are no storage locations on selected ship to, then raise an error
        doActionValidateStockHolidingLocations();
         // End list check
    }
    
    public void populatesupplylocations(list<EP_Stock_Holding_Location__c> listOfStockHoldingLocations){
        ctx.mapOfSupplyLocation = new Map < String, EP_Stock_Holding_Location__c > ();
        //ctx.listOfSupplyLocationOptions.add(new SelectOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
        for (EP_Stock_Holding_Location__c supplyLocationObject: listOfStockHoldingLocations) {
            if(ctx.newOrderRecord.Stock_Holding_Location__c == supplyLocationObject.ID && ctx.listOfSupplyLocationOptions.size() > 0){
                ctx.listOfSupplyLocationOptions.add(0,new SelectOption(supplyLocationObject.ID, supplyLocationObject.Stock_Holding_Location__r.Name));
            }
            else{
                ctx.listOfSupplyLocationOptions.add(new SelectOption(supplyLocationObject.ID, supplyLocationObject.Stock_Holding_Location__r.Name));
            }
            ctx.mapOfSupplyLocation.put(supplyLocationObject.id, supplyLocationObject);
            
        }

        //For firt time load
        if(String.isBlank(ctx.strSelectedPickupLocationID) && !listOfStockHoldingLocations.isempty()){
            ctx.strSelectedPickupLocationID = listOfStockHoldingLocations[0].id;
        }
    }
    
    public void doActionValidateStockHolidingLocations(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doActionValidateStockHolidingLocations');
        if (ctx.listOfSupplyLocationOptions.isEmpty()){
            ctx.hasAccountSetupError = true;
            ApexPages.Message msg;
            if(ctx.isDeliveryOrder || ctx.isDummy || ctx.isConsumptionOrder){
                msg = EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_SupplyLocation_At_ShipTo);
                return;
            }
            else{ // CCO Work
                msg = EP_Common_Util.createApexMessage(ApexPages.Severity.INFO, Label.EP_No_SupplyLocation_At_SellTo); 
                return;
            }
            ApexPages.addMessage(msg);
        }
    }

     
    public void setPickupDetails(){
        EP_GeneralUtility.Log('Private',CLASSNAME,'SetPickupDetails');
        system.debug('ctx.strSelectedPickupLocationID' + ctx.strSelectedPickupLocationID);
        EP_Stock_Holding_Location__c selectedPickupDetail = new EP_StockHoldingLocationMapper().getRecordsById(ctx.strSelectedPickupLocationID);
        system.debug('selectedPickupDetail' + selectedPickupDetail );
        ctx.isSlottingEnabled = selectedPickupDetail.Stock_Holding_Location__r.EP_Slotting_Enabled__c;
        ctx.newOrderRecord.EP_Stock_Holding_Location__c = selectedPickupDetail.Stock_Holding_Location__c;
        ctx.newOrderRecord.EP_Supply_Location_Name__c =  selectedPickupDetail.Stock_Holding_Location__r.Name; 
        if(!ctx.isExrackOrder){//Defect 57558
            if(selectedPickupDetail.EP_Transporter__r.Id != null){
                ctx.transportAccount = new List<SelectOption>();
                ctx.transportAccount.add(EP_Common_Util.createOption(selectedPickupDetail.EP_Transporter__r.Id,selectedPickupDetail.EP_Transporter__r.Name));
                ctx.orderService.checkForNoTransporter(selectedPickupDetail.EP_Transporter__r.Name);  
            }                 
            ctx.startDate = EP_PortalOrderUtil.getDeliveryStartDate(selectedPickupDetail);
            ctx.availableRoutes = setRunAndRoute(ctx.strSelectedShipToID,selectedPickupDetail.Stock_Holding_Location__c);
            ctx.newOrderRecord.EP_ShipTo__c = ctx.strSelectedShipToID;
            ctx.newOrderRecord.EP_Price_Type__c = EP_Common_Constant.FUEL_FREIGHT_PRICE;
        }
        else{
            getAllAvailableDates(selectedPickupDetail);
            ctx.newOrderRecord.EP_Price_Type__c = EP_Common_Constant.FUEL_PRICE;//PE  
        }
    }

    public void setOrderRecordType(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderRecordType');
        ctx.orderDomainObj = new EP_orderDomainObject(ctx.newOrderRecord);   
        ctx.orderService = new EP_orderService(ctx.orderDomainObj);
        Id recordTypeId  =  ctx.orderDomainObj.findRecordType();
        if (recordTypeId != NULL) {
            ctx.newOrderRecord.RecordTypeId = recordTypeId;
        } 
    }
    
    /*
    This method is used to create picklist for route and run
    */
    public List<SelectOption> setRunAndRoute(String shipToId, String stockholdinglocationId) {
        EP_GeneralUtility.Log('Public',CLASSNAME,'setRunAndRoute');
        List<SelectOption> listRouteOption = new List<SelectOption>();
        listRouteOption.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK,EP_Common_Constant.PICKLIST_NONE));
        for(EP_Route__c route : ctx.orderService.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId)) {
           listRouteOption.add(new SelectOption(route.id,route.name));
        }
        ctx.isRoutePresent = listRouteOption.size() > 1;
        return listRouteOption;
    }  

    public void getAllAvailableDates(EP_Stock_Holding_Location__c selectedPickupDetail){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getAllAvailableDates');
        ctx.timeSlots = new Map<String, List<SelectOption>>();
        EP_FE_NewOrderInfoResponse feResponse = new EP_FE_NewOrderInfoResponse();
        EP_PortalOrderUtil.getAvailableDates(feResponse,selectedPickupDetail);
        for( EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo term : feResponse.terminalInfoList){
            filterTerminalDateTime(term.availableDateTimeList,selectedPickupDetail);               
        }
        
    }
    @TestVisible
    private void filterTerminalDateTime(list<EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime> availableDateTimeList,EP_Stock_Holding_Location__c selectedPickupDetail){
        for(EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpen : availableDateTimeList  ){
            ctx.availableDatesList.add(String.valueOf(dateOpen.workingDate));
            if(selectedPickupDetail.Stock_Holding_Location__r.EP_Slotting_Enabled__c){
                ctx.timeSlots.put(null,new List<SelectOption>{EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE)}); // #novaSuiteFixes
                setTimeframeForOpenDate(dateOpen);
            }
        }
        
    }
    @TestVisible
    private void setTimeframeForOpenDate(EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpen ){
        List<String> dates = new List<String>();
        String workingDate = EP_Common_Constant.BLANK;
        List<SelectOption> newList = new List<SelectOption>();
        for(EP_FE_NewOrderInfoResponse.EP_FE_TimePeriod timeSlot : dateOpen.timePeriods  ){
            dates = String.valueOf(dateOpen.workingDate).split(EP_Common_Constant.hyphen);
            workingDate = dates[2]+EP_Common_Constant.hyphen+dates[1]+EP_Common_Constant.hyphen+dates[0];
            if(ctx.timeSlots.containsKey(workingDate)){
                newList = ctx.timeSlots.get(workingDate);
                newList.add(EP_Common_Util.createOption(String.valueOf(timeSlot.endTime), String.valueOf(timeSlot.startTime).substring(0,5)+EP_Common_Constant.HRS+EP_Common_Constant.hyphen+String.valueOf(timeSlot.endTime).substring(0,5)+EP_Common_Constant.HRS));  // #novaSuiteFixes
                ctx.timeSlots.put(workingDate,newList);
            }else{
                ctx.timeSlots.put(workingDate,new List<SelectOption>{EP_Common_Util.createOption(String.valueOf(timeSlot.endTime), String.valueOf(timeSlot.startTime).substring(0,5)+EP_Common_Constant.HRS+EP_Common_Constant.hyphen+String.valueOf(timeSlot.endTime).substring(0,5)+EP_Common_Constant.HRS)});
            }
        }
    }

    public void doCalculatePrice(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doCalculatePrice');
        system.debug('ctx.listofNewOrderWrapper '+ ctx.listofNewOrderWrapper);
        system.debug('ctx.orderWrapperMap '+ ctx.orderWrapperMap);
        setOrderDetails();
        setRelatedBulkOrderDetailsonPackagedOrder();
        ctx.orderDomainObj.setRetroOrderDetails();
        /*if(hasInValidDatesOnROOrder()){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, LABEL.EP_Select_Order_Date_First));
            ctx.startPolling = false;
            return;
        }*/

        if(!isvalidOrder()){
            system.debug('Validation error');
            ctx.startPolling = false;
            return;
        }

        if(hasInValidDates() ){
            system.debug('hasInValidDates ');
            String errMsg = ctx.isExrackOrder ? 'Request Preffered Date is required' : 'Preffered Delivery date is manadatory';
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, errMsg));
            ctx.startPolling = false;
            return;
        }
        ctx.orderService.setConsumptionOrderDetails();       
        ctx.orderService.doSubmitActions();
        ordLinehlp.setAndValidateOrderLines();
        system.debug('Validate order lines');
        if(ctx.hasinvalidorderlines){
            ctx.startPolling = false;
            return;
        }
        system.debug('All good, upserting order line');
        upsert ctx.newOrderRecord;

        setOrderItemsinDomain();
        ctx.orderService.calculatePrice(); 
        ctx.isPriceCalculated = true;
        ctx.isPricingRecieved = false;
        ctx.startPolling = true;
    }

    /*
    * Set Order values
    */
    public void setOrderDetails(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderDetails');
        ctx.newOrderRecord.Stock_Holding_Location__c = ctx.strSelectedPickupLocationID;
        ctx.newOrderRecord.AccountId__c = ctx.strSelectedAccountID;
        ctx.newOrderRecord.EffectiveDate__c = System.Today();
        ctx.newOrderRecord.EP_Requested_Delivery_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.strSelectedDate);
        //ctx.newOrderRecord.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(ctx.availableSlots)).subString(0,8);
        if (ctx.newOrderRecord.Status__c == '' || ctx.newOrderRecord.Status__c == NULL) {
            ctx.newOrderRecord.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        }
        ctx.newOrderRecord.Pricebook2Id__c = ctx.strSelectedPricebookID;
        ctx.newOrderRecord.EP_Route__c = ctx.strSelectedRoute;
        ctx.newOrderRecord.EP_Run__c = ctx.strSelectedRun;
        ctx.newOrderRecord.EP_Run_Id__c = String.ValueOf(ctx.runMap.get(ctx.strSelectedRun));
        ctx.newOrderRecord.EP_Payment_Term__c = ctx.strSelectedPaymentTerm;//Defect 57275 

        if(ctx.isConsumptionOrder){
            ctx.newOrderRecord.EP_Order_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.orderDateStr);
        }

        if(ctx.isRoOrder && !ctx.isConsumptionOrder){
            ctx.newOrderRecord.EP_Order_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.orderDateStr);
            ctx.newOrderRecord.EP_Loading_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.loadingDateStr);
            ctx.newOrderRecord.EP_Expected_Delivery_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.deliveredDateStr);
        }
        system.debug('setOrderDetails ' + ctx.newOrderRecord);
    }

    public void setRelatedBulkOrderDetailsonPackagedOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'getOrderWapperforIndex');
        if(!ctx.isBulkOrder && !String.isBlank(ctx.strRelatedBulkOrder)) {
            ctx.newOrderRecord.EP_Related_Bulk_Order__c = ctx.bulkRelatedOrderMap.get(ctx.strRelatedBulkOrder).id;
            ctx.newOrderRecord.EP_Bulk_Order_Number__c = ctx.strRelatedBulkOrder;                          
        }
        else{
            ctx.newOrderRecord.EP_Related_Bulk_Order__c= NULL;
            ctx.newOrderRecord.EP_Bulk_Order_Number__c = EP_Common_Constant.BLANK;
        }
    }

    public void setOrderItemsinDomain(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'setOrderItemsinDomain');
        List <csord__Order_Line_Item__c> orderItems = new List <csord__Order_Line_Item__c> ();

        for (EP_OrderPageContext_UX.OrderWrapper orderWrapperObj: ctx.orderWrapperMap.values()) {
            csord__Order_Line_Item__c orderItemObj = orderWrapperObj.orderLineItem;
            if (orderItemObj.OrderId__c == NULL && orderItemObj.id == null) {
                orderItemObj.OrderId__c = ctx.newOrderRecord.Id;
            }
            orderItems.add(orderItemObj);  
        }
        system.debug('orderItems to be upserted ' + orderItems);
        if (!orderItems.isEmpty()) {
            upsert orderItems; // Move to domain setOrderItems
            ctx.orderDomainObj.setOrderItems(orderItems);
        }
    }

    public boolean hasInValidDatesOnROOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'hasInValidDatesOnROOrder');
        system.debug('ctx.isConsumptionOrder ' + ctx.isConsumptionOrder);
        system.debug('ctx.isRoOrder ' + ctx.isRoOrder);
        system.debug('ctx.orderDomainObj.isValidLoadingDate ' + ctx.orderDomainObj.isValidLoadingDate());
        system.debug('ctx.orderDomainObj.isValidOrderDate ' + ctx.orderDomainObj.isValidOrderDate());
        system.debug('ctx.orderDomainObj.isValidExpectedDate ' + ctx.orderDomainObj.isValidOrderDate());

        return (!ctx.isConsumptionOrder && ctx.isRoOrder && (ctx.orderDomainObj.isValidLoadingDate() || ctx.orderDomainObj.isValidOrderDate() || ctx.orderDomainObj.isValidExpectedDate()));
    }

    public boolean hasInValidDates(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'hasInValidDates');
        return ((ctx.isDeliveryOrder || ctx.isExrackOrder || ctx.isConsignment) && !ctx.isRoOrder && (String.isBlank(ctx.strSelectedDate)));
    }

    public boolean IsValidOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'IsValidOrder');
        boolean isvalidOrder = true;
        if(ctx.isPrepaymentCustomer && ctx.showCustomerPoField){
            Boolean showPrepayCustomerPOBlankError = EP_PortalOrderUtil.checkPrepayForPoNumber(ctx.newOrderRecord,ctx.isPrepaymentCustomer,ctx.isConsignmentOrder,ctx.isConsumptionOrder,ctx.isDummy,ctx.orderAccount); // #customerPoWork
            if(showPrepayCustomerPOBlankError) {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, LABEL.EP_ProvideCustomerPurchaseOrderNumber));
                isvalidOrder = false;
                system.debug('PO error');
            }
        }
        if(ctx.orderDomainObj.isValidOrderDate()){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Order_Date_Error));
            isvalidOrder = false;
            system.debug('Order date error');
        }
        /*Defect 53419 Start*/
        if (ctx.orderDomainObj.isValidLoadingDate()) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Loading_Date_Error));
            isvalidOrder = false;
            system.debug('Loading date error');
        }
        
        if (ctx.orderDomainObj.isValidExpectedDate()) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_Delivery_Date_Error));
            isvalidOrder = false;
            system.debug('Expected date error');
        }
        /*Defect 53419 End*/
        return isvalidOrder;
    }


    @TestVisible
    private void validateLoadingdate(EP_OrderPageContext_UX.OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Private',CLASSNAME,'ValidateLoadingdate');
        
            System.debug('orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c##'+orderWrapperObj.orderLineItem);
            orderWrapperObj.contractOptions = supplierContracts(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c);
            
    }
    @TestVisible
    private void setTankDetailsonWrapper(EP_OrderPageContext_UX.OrderWrapper orderWrapperObj){
        EP_GeneralUtility.Log('Private',CLASSNAME,'setTankDetailsonWrapper');
        if(!ctx.isOprtnlTankAvail || orderWrapperObj.oliTanksID == NULL){
            return;
        }
        EP_Tank__c tankobj = ctx.mapShipToOperationalTanks.get(orderWrapperObj.oliTanksID);
        orderWrapperObj.oliTankProductName = tankobj.EP_Product__r.Name;
        if(tankobj.EP_Safe_Fill_Level__c != null) {
            orderWrapperObj.oliTankSafeFillLvl = Integer.valueOf(tankobj.EP_Safe_Fill_Level__c);
        }
        orderWrapperObj.oliTanksName = tankobj.EP_Tank_Code__c;
        if(tankobj.EP_Tank_Alias__c != NULL) {
            orderWrapperObj.oliTanksName =  orderWrapperObj.oliTanksName+EP_Common_Constant.SLASH+ tankobj.EP_Tank_Alias__c;
        }
       
    }

    public List<SelectOption> supplierContracts(String supplierId){
        EP_GeneralUtility.Log('Public',CLASSNAME,'supplierContracts');
        List<SelectOption> contractList = new List<SelectOption>();
        try{
            contractList.add(EP_Common_Util.createOption(EP_Common_Constant.BLANK, EP_Common_Constant.NONE));
            Date loadingDate = ctx.newOrderRecord.EP_Loading_Date__c.date();
            if(!ctx.supplierContractMap.containsKey(supplierId)){
                return contractList;
            }
            for(Contract con : ctx.supplierContractMap.get(supplierId)){
                if(isValidLoadingDate(con,loadingDate)){
                    contractList.add(EP_Common_Util.createOption(con.Id,con.ContractNumber));
                }
            }
        }catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, EP_Common_Constant.SupplierContracts_Method, CLASSNAME, ApexPages.Severity.ERROR);
        }    
        return contractList;
    } 

     @TestVisible
    private boolean isValidLoadingDate(Contract con, date loadingDate){
        return (con.startDate != NULL && con.EndDate != NULL && loadingDate >= con.startDate && loadingDate <= con.EndDate);
    }


     public void doSubmitActionforNewOrder(){
        EP_GeneralUtility.Log('Public',CLASSNAME,'doSubmitActionforNewOrder');
        System.debug('doSubmitActionforNewOrder---');
        if( ctx.newOrderRecord.Id == NULL ){
            ctx.orderService.doSubmitActions();
            
            ordLinehlp.setAndValidateOrderLines();
            if(ctx.hasinvalidorderlines){
                return;
            }
            system.debug('All good, inserting order line');
            insert ctx.newOrderRecord;
            
            setOrderItemsinDomain();
        }
        //Defect 57453 Start    
        ctx.orderDomainObj = new EP_orderDomainObject(ctx.newOrderRecord.Id);
        ctx.orderService = new EP_orderService(ctx.orderDomainObj);  
        //Defect 57453 End           
    
    }
    
    public pagereference saveOrderAsDraft(){
        EP_GeneralUtility.Log('public',CLASSNAME,'saveOrderAsDraft');
        PageReference ref; 
        //Defect 57277 START
        //ctx.newOrderRecord.EP_Credit_Status__c = null;
        if((ctx.hasCreditIssues || ctx.hasOverdueInvoices) && !ctx.isRoOrder) {         
                ctx.newOrderRecord.EP_Credit_Status__c = EP_OrderConstant.CREDIT_FAIL;
        }
        //Defect 57277 END
        ctx.newOrderRecord.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        ctx.newOrderRecord.EP_Payment_Term__c = EP_Common_Constant.Blank;
        ctx.newOrderRecord.EffectiveDate__c = System.Today();
        ctx.newOrderRecord.RecordTypeId = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_DRAFT_ORDER_RECORD_TYPE_DEV_NAME).id;
        upsert ctx.newOrderRecord;
        ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);
        return ref.setRedirect(false);        
    }

    public void doUpdateSupplierContracts(){
        EP_GeneralUtility.Log('public',CLASSNAME,'doUpdateSupplierContracts');
        
        if(String.isBlank(ctx.loadingDateStr)) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.ERROR, LABEL.EP_Select_Loading_Date_first));
            return;
        }

        ctx.newOrderRecord.EP_Loading_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.loadingDateStr);

        // for packaged lines
        for(EP_OrderPageContext_UX.OrderWrapper orderWrapperObj : ctx.orderWrapperMap.values()){
            if(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c != null){
                system.debug('orderWrapperObj ' + orderWrapperObj);
                orderWrapperObj.contractOptions = supplierContracts(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c);
            }
        }

        //for bulk lines
        for(EP_OrderPageContext_UX.OrderWrapper orderWrapperObj : ctx.listofNewOrderWrapper){
            if(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c != null){
                system.debug('orderWrapperObj ' + orderWrapperObj);
                orderWrapperObj.contractOptions = supplierContracts(orderWrapperObj.orderLineItem.EP_3rd_Party_Stock_Supplier__c);
            }
        }
    }

    public PageReference setOrderStatustoCancelled(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderStatustoCancelled');
        PageReference ref = NULL;
        if (ctx.newOrderRecord.Id == NULL) {
            system.debug('New record');
            ref = new PageReference('/apex/EP_PortalOrderNew?id='+ctx.orderAccount.id);
            return ref;
        }
        EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        boolean isStatusChanged = ctx.orderService.setOrderStatus(orderEvent);
        System.debug('OrderStatus1'+ctx.newOrderRecord.Status__c);
        if(isStatusChanged){
            Database.Update(ctx.newOrderRecord);
            ctx.orderService.doPostStatusChangeActions();
        }
        System.debug('OrderStatus2'+ctx.newOrderRecord.Status__c);
        ref = new PageReference('/apex/EP_PortalOrderSummaryNew?id='+ ctx.newOrderRecord.Id);
        return ref;
    }

    public PageReference doCancelAndClose(){
        EP_GeneralUtility.Log('public',CLASSNAME,'setOrderStatustoCancelled');
        PageReference ref = new PageReference(EP_Common_Constant.SLASH + ctx.orderAccount.id);

        List<csord__Order_Line_Item__c> recToDelete = ctx.orderDomainObj.getOrderItems();
        List<csord__Order_Line_Item__c> recToUpsert = new List<csord__Order_Line_Item__c>();
        system.debug('recToDelete ' + recToDelete);

        if(!recToDelete.isEmpty()){
            delete recToDelete;
        }

        for(csord__Order_Line_Item__c oItem: ctx.oldOrderItems){
            if(oItem.EP_Is_Standard__c){
                oItem.Id = null;
                recToUpsert.add(oItem);
            }
        }

        system.debug('ctx.oldOrderItems ' + ctx.oldOrderItems);

        if(!recToUpsert.isEmpty()){
            upsert recToUpsert;
        }

        return ref;
    }
    
}