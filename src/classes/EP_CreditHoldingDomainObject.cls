/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_CreditHoldingDomainObject>                        *
*  @CreateDate <1/11/2017>                                     *
*  @Description <Domain object to store Credit Holding and 
                    Credit Holding related status>             *
*  @Version <1.0>                                              *
****************************************************************/
 public class EP_CreditHoldingDomainObject{
    private EP_Credit_Holding__c localCreditHolding;
    EP_CreditHoldingService service;
    EP_CreditHoldingMapper creditHoldingMapper;
    private List<EP_Credit_Holding__c> localCeditHoldingList;
    private map<id,EP_Credit_Holding__c> mapOldCreditHolding;



/****************************************************************
* @author       Accenture                                       *
* @name         EP_CreditHoldingDomainObject                    *
* @description  Constructor of class to set new credit Holding  *
* @param        EP_Credit_Holding__c                               *
* @return       NA                                              *
****************************************************************/ 
    public EP_CreditHoldingDomainObject(EP_Credit_Holding__c crHoldingObj){
        this.localCreditHolding = crHoldingObj;
        this.service = new EP_CreditHoldingService(this);
        this.creditHoldingMapper = new EP_CreditHoldingMapper(this);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         EP_CreditHoldingDomainObject                    *
* @description  Constructor of class to set new credit holding 
                list and old credit holdig map                  *
* @param        List<EP_Credit_Holding__c>,
                map<id,EP_Credit_Holding__c>                       *
* @return       NA                                              *
****************************************************************/ 
    public EP_CreditHoldingDomainObject(List<EP_Credit_Holding__c> ListcrHolding, map<id,EP_Credit_Holding__c> mapOldCH){
        this.localCeditHoldingList = ListcrHolding;
        this.mapOldCreditHolding = mapOldCH;
        this.service = new EP_CreditHoldingService(this);
        this.creditHoldingMapper = new EP_CreditHoldingMapper(this);
    }
    
/****************************************************************
* @author       Accenture                                       *
* @name         doActionAfterInsert                             *
* @description  This method is to be called thorugh trigger to 
                execute all after Insert event logic/actions    *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void doActionAfterInsert(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','doActionAfterUpdate');
        service.doAfterInsertHandle(); 
    }    

/****************************************************************
* @author       Accenture                                       *
* @name         doActionBeforeInsert                            * 
* @description  This method is to be called thorugh trigger to 
                execute all after insert event logic/actions    *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/    
    public void doActionBeforeInsert(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','doActionAfterInsert');
        service.doBeforeInsertHandle(); 
    }
        

/****************************************************************
* @author       Accenture                                       *
* @name         doActionAfterUpdate                             *
* @description  This method is to be called thorugh trigger to 
                execute all after update event logic/actions    *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void doActionAfterUpdate(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','doActionAfterUpdate');
        //service.doAfterUpdateHandle(); 
    }

/****************************************************************
* @author       Accenture                                       *
* @name         doActionBeforeUpdate                            *
* @description  This method is to be called thorugh trigger to 
                execute all before update event logic/actions   *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void doActionBeforeUpdate(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','doActionBeforeUpdate');
        service.doBeforeUpdateHandler();
    }


/****************************************************************
* @author       Accenture                                       *
* @name         getCreditHolding                                *
* @description  This method is used to get current credit 
                                Holding List                    *
* @param        NA                                              *
* @return       List Credit Holding                             *
****************************************************************/     
    public List<EP_Credit_Holding__c> getCreditHolding(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','getCreditHolding');
        return localCeditHoldingList; 
    } 

/****************************************************************
* @author       Accenture                                       *
* @name         getCreditHoldingObject                          *
* @description  This method is used to get current credit 
                                Holding object                  *
* @param        NA                                              *
* @return       List Credit Holding                             *
****************************************************************/     
    public EP_Credit_Holding__c getCreditHoldingObject(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','getCreditHoldingObject');
        return localCreditHolding;
    }

/****************************************************************
* @author       Accenture                                       *
* @name         getOldRecord                                    *
* @description  This method is used to get old Credit Holding 
                    record object                               *
* @param        NA                                              *
* @return       map                                             *
****************************************************************/
    public map<id,EP_Credit_Holding__c> getOldRecord(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','getOldRecord');
        return mapOldCreditHolding;
    }

/****************************************************************
* @author       Accenture                                       *
* @name         getOldRecord                                    *
* @description  This method is used to get old Credit Holding 
                    record object                               *
* @param        NA                                              *
* @return       map                                             *
****************************************************************/
    public EP_Credit_Holding__c rollupCreditLimit(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingDomainObject','getOldRecord');
        return(service.rollupCreditLimit());
    }

/****************************************************************
* @author       Accenture                                       *
* @name         isStatusChanged                                 * 
* @description  to check if status changed                      *
* @param        new Credit Hoding, Old Credit Holding           *
* @return       boolean                                         *
****************************************************************/
    public boolean isStatusChanged(EP_Credit_Holding__c holdingObj){
        EP_GeneralUtility.Log('public','EP_CreditHoldingService','isStatusChanged');
        return(mapOldCreditHolding.get(holdingObj.id).EP_Status__c != holdingObj.EP_Status__c );
    }

}