/*
 *  @Author <Accenture>
 *  @Name <EP_StorageLocationASInactive >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  07-Inactive Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageLocationASInactive extends EP_AccountState{
     
    public EP_StorageLocationASInactive() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASInactive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASInactive','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASInactive','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASInactive','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASInactive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}