/**
* @Author <Vikas Malik>
* @Name <EP_CaseHelper>
* @CreateDate <09/10/2015>
* @Description <This class is the helper class for Case object>
* @Version <1.0>
*/
public with sharing class EP_CaseHelper{
    
    /* Lead Required Field Lis*/
    private static List<String> listOfRequiredLeadFieldNames = new List<String>{EP_Common_Constant.NAME
        ,EP_Common_Constant.COMPANY
        ,EP_Common_Constant.COMPANY_TAX_NUMBER
        ,EP_Common_Constant.STREET
        ,EP_Common_Constant.CITY
        ,EP_Common_Constant.STATE
        ,EP_Common_Constant.COUNTRY
        ,EP_Common_Constant.POSTALCODE
        ,EP_Common_Constant.LEAD_MODE_COMM_EMAIL
        ,EP_Common_Constant.LEAD_MODE_COMM_PHONE
        ,EP_Common_Constant.LEAD_PROD_INTERESTED_IN
        ,EP_Common_Constant.LEAD_INDICATIVE_FUEL_VOLUME
        ,EP_Common_Constant.LEAD_INDICATIVE_LUBES_VOLUME
        ,EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY
        ,EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY_LUBES
        ,EP_Common_Constant.LEAD_REQUESTED_PAYMENT_TERM
        ,EP_Common_Constant.LEAD_REQUESTED_PAYMENT_METHOD
        ,EP_Common_Constant.LEAD_BANK_NAME
        ,EP_Common_Constant.LEAD_IBAN
        ,EP_Common_Constant.LEAD_PREFERRED_MODE_OF_COMMUNICATION
        ,EP_Common_Constant.LEAD_PREFERRED_TIME_OF_COMMUNICATION};
    /* Map to hold field Name Label Map */
    private static Map<String,String> mapOfFieldLabelName = new Map<String,String>();
    /*Skipped FieldNames Set*/
    private static Set<String> setOfSkippedFieldNames = new Set<String>{EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY_LUBES
        ,EP_Common_Constant.LEAD_INDICATIVE_FUEL_VOLUME
        ,EP_Common_Constant.LEAD_INDICATIVE_LUBES_VOLUME
        ,EP_Common_Constant.LEAD_BANK_NAME 
        ,EP_Common_Constant.LEAD_IBAN 
        ,EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY};
    /*
    @Author <Kamendra Singh>
    @CreateDate <08/06/2016>
    @Description <get Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).> 
    */
    private static RecordType applicationCaseRecordType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.CASE_OBJ_REC, EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
    private static final String PORTAL_CASE_ORIGIN = 'Portal';
    private static final String CLASSNAME = 'EP_CaseHelper';
    private static final String ASSRULEMTD = 'triggerAssignmentRulesForPortalCases';
    private static final String DOBEFOREUPDATE_MTD = 'doBeforeUpdate';
    private static final String DOBEFOREINSERT_MTD = 'doBeforeInsert';
    private static final String DOAFTERINSERT_MTD = 'doAfterInsert';
    /**
     * @author <Vikas Malik>
     * @description <This method will handle the after insert event for Case object>
     * @name <doBeforeInsert>
     * @date <09/10/2015>
     * @param List<Case>
     * @return void
     */
    public static void doBeforeInsert(List<Case> lCases){
        try{
            calculateCaseSLA(lCases);
        }catch(exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, DOBEFOREINSERT_MTD, CLASSNAME , ApexPages.Severity.ERROR);
        }
    }
    
    /**
     * @author <Vikas Malik>
     * @description <This method will handle the after insert event for Case object>
     * @name <doAfterInsert>
     * @date <09/10/2015>
     * @param List<Case>
     * @return void
     */
    public static void doAfterInsert(List<Case> lCases){
        try{
            triggerAssignmentRulesForPortalCases(lCases);
        }catch(exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, DOAFTERINSERT_MTD, CLASSNAME , ApexPages.Severity.ERROR);
        }
    }
    
    /**
     * @author <Vikas Malik>
     * @description <This method will trigger the default Case assignment rule for new cases submited via the customer portal>
     * @name <triggerAssignmentRulesForPortalCases>
     * @date <09/10/2015>
     * @param List<Case>
     * @return void
     */
    public static void triggerAssignmentRulesForPortalCases(List<Case> lCases){
        List<Case> listOfUpdatedCases = new List<Case>();
        Case caseobj ;
        try{
            for(Case caseObject : lCases){
                if (caseObject.Origin == PORTAL_CASE_ORIGIN){
                    caseobj  = new case(Id=caseObject.Id);
                    listOfUpdatedCases.add(caseobj);
                }
            } // End for
            if(!listOfUpdatedCases.isEmpty()){
                // Create the DMLOptions object for "Assign using active assignment rules" checkbox
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.useDefaultRule = TRUE;
                Database.update(listOfUpdatedCases, dmlOpts);
            }
        }catch(exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, ASSRULEMTD, CLASSNAME , ApexPages.Severity.ERROR);
        }
    }
    
    /**
     * @author <Vikas Malik>
     * @description <This method will handle the before update event for Case object>
     * @name <doBeforeUpdate>
     * @date <09/10/2015>
     * @param Map<Id, Case>, Map<Id, Case>
     * @return void
     */
    public static void doBeforeUpdate(Map<Id, Case> mapofnewCaseIdCase, Map<Id, Case> mapOfOldCaseIdCase){
        List<Case> listOfCaseProcess = new List<Case>();
        List<Case> listOfcaseSLAProcess = new List<Case>();
        
        for(Case newCase : mapofnewCaseIdCase.values()){
            /* @Author <Kamendra Singh>
             * @Description <add filter on Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).
             * @CreateDate <08/06/2016>
             */
            if(newCase.Status != mapOfOldCaseIdCase.get(newCase.Id).Status && EP_Common_Constant.CASE_STATUS_UNDER_KYC_REVIEW.equals(newCase.Status) 
               && newCase.RecordTypeId == applicationCaseRecordType.Id){
                listOfCaseProcess.add(newCase);
            }
            /* @Author <Spiros Markantonatos>
             * @Description <Recalculate Case SLA when type or account is changed>.
             * @CreateDate <28/06/2016>
             */
            if (newCase.AccountId != mapOfOldCaseIdCase.get(newCase.Id).AccountId || newCase.Type != mapOfOldCaseIdCase.get(newCase.Id).Type 
                || newCase.Origin != mapOfOldCaseIdCase.get(newCase.Id).Origin){
                listOfcaseSLAProcess.add(newCase);
            }
        }
        try{
            if(!listOfcaseSLAProcess.isEmpty()){
                calculateCaseSLA(listOfcaseSLAProcess);
            }
            if(listOfCaseProcess != null && listOfCaseProcess.size() > 0){
                mapOfFieldLabelName = EP_Common_Util.getFieldsLabels(EP_Common_Constant.LEAD_OBJ);
                for (Lead caseLead : [SELECT Id,Case__c,Name,Street,City
                ,State,PostalCode,Country,Company
                ,EP_Company_Tax_Number__c,
                 Email,Phone,MobilePhone,Fax
                ,EP_Products_Interested_In__c
                ,EP_Indicative_Order_Frequency_Lubes__c
                ,EP_Indicative_Fuel_Volume__c
                ,EP_Indicative_Lubes_Volume__c
                ,EP_Indicative_Order_Frequency__c
                ,EP_Requested_Payment_Terms__c
                ,EP_Requested_Payment_Method__c
                ,EP_Bank_Name__c,EP_IBAN__c
                ,EP_Preferred_Mode_of_Communication__c
                ,EP_Preferred_Time_of_Communication__c 
                FROM Lead WHERE Case__c IN :listOfCaseProcess limit :EP_Common_Constant.ONE])
                {
                    String errorMessage = validateLeadData(caseLead);
                    if (!string.isBlank(errorMessage)){
                        mapofnewCaseIdCase.get(caseLead.Case__c).
                        addError(Label.EP_Review_Required_Lead_Fields_Error+EP_Common_Constant.TIME_SEPARATOR_SIGN+errorMessage.left(errorMessage.length()-1));
                    }
                }
            }
        }catch(Exception e){
            //Exception Handling Code to be written
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, DOBEFOREUPDATE_MTD, CLASSNAME , ApexPages.Severity.ERROR);
        }
    }
    
    /**
     * @author <Spiros Markantonatos>
     * @description <Calculate the target resolution date of the case based on the country code, local support business hours and case type>
     * @name <calculateCaseSLA>
     * @date <28/06/2016>
     * @param List<Case>
     * @return void
     */
    private static void calculateCaseSLA(List<Case> lCasesToProcess){
        // Retrieve SLA map from custom setting object
        Map<String, EP_Case_SLA_Map__c> mapOfIdCaseSla = EP_Case_SLA_Map__c.getAll();
        DateTime dtCaseOpenDateTime = null;
        Integer millisecond = EP_Common_Constant.ZERO;
        Integer nRows = EP_Common_Util.getQueryLimit();
        // Retrieve the Record Type Developer names for the cases
        Map<String, String> mapOfCaseRecordTypeIdName = new Map<String, String>();
        Set<ID> setCaseRecordTypeDeveloperName = new Set<ID>();
        for (Case caseObject : lCasesToProcess){
            setCaseRecordTypeDeveloperName.add(caseObject.RecordTypeId);
        } // End Case Record Type ID set for
        
        if (!setCaseRecordTypeDeveloperName.isEmpty()){
            for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE Id IN :setCaseRecordTypeDeveloperName limit :nRows]){
                mapOfCaseRecordTypeIdName.put(rt.Id, rt.DeveloperName);
            } // End record type for
        } // End Case Record Type ID set check
        
        // Find the country code and type for all inserted/updated cases
        for (Case caseObject : lCasesToProcess) {
            // Reset the target resolution date/time
            caseObject.EP_FE_Target_Resolution_Date_Time__c= NULL;
            if (caseObject.Type <> NULL && caseObject.EP_Case_Country_Code__c <> NULL  && caseObject.Origin <> NULL) {
                for (EP_Case_SLA_Map__c m : mapOfIdCaseSla.values()) {
                    if (mapOfCaseRecordTypeIdName.containsKey(caseObject.RecordTypeId)){
                        if (m.EP_Country_Code__c == caseObject.EP_Case_Country_Code__c 
                                && m.EP_Type__c == caseObject.Type 
                                && m.EP_Case_Origin__c == caseObject.Origin
                                && m.EP_Case_Record_Type_Developer_Name__c == mapOfCaseRecordTypeIdName.get(caseObject.RecordTypeId)) {
                            
                            // Update the target resolution of the Case
                            caseObject.EP_Target_Resolution_Hours__c = m.EP_Target_Resolution_Time_Hours__c;
                            
                            // Update the priority of the Case
                            caseObject.Priority = m.EP_Priority__c;
                            
                            // Calculate the case open date/time based on the opening hours of the local call centre
                            dtCaseOpenDateTime = System.Now();
                            if (caseObject.CreatedDate != NULL)
                            dtCaseOpenDateTime = caseObject.CreatedDate;
                            
                            dtCaseOpenDateTime = BusinessHours.nextStartDate(m.EP_Business_Hours_ID__c, dtCaseOpenDateTime);
                            
                            if (caseObject.EP_Target_Resolution_Hours__c != NULL) {
                                // Convert hours to milliseconds
                                millisecond = Integer.valueOf(caseObject.EP_Target_Resolution_Hours__c) * 60 * 60000;
                                
                                // Calculate the target close date/time based on the case open date/time and the SLA for the type of case and customer tier and country code
                                caseObject.EP_FE_Target_Resolution_Date_Time__c = BusinessHours.add(m.EP_Business_Hours_ID__c, dtCaseOpenDateTime, millisecond);
                                
                            } // End resolution time check
                            
                            break;
                        }
                    } // End Case record type check
                } // End for
            } // End Type and Country Code check
        } // End for
        
    }
    
    /**
     * @author <Vikas Malik>
     * @description <This method will validate the associated Lead record required values before it can be marked as Ready for KYC Review>
     * @name <validateLeadData>
     * @date <09/10/2015>
     * @param Lead
     * @return String
     */
    private static String validateLeadData(Lead caseLead){
        String errorMessage = EP_Common_Constant.BLANK;
        for (String fieldName : listOfRequiredLeadFieldNames){
            if (fieldName == EP_Common_Constant.LEAD_PROD_INTERESTED_IN && caseLead.EP_Products_Interested_In__c != null){
                String[] interestedValues = caseLead.EP_Products_Interested_In__c.split(EP_Common_Constant.SEMICOLON);
                for(String intVal : interestedValues){
                    if(EP_Common_Constant.LEAD_PROD_INTERESTED_FUEL.equals(intVal) && caseLead.EP_Indicative_Fuel_Volume__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_INDICATIVE_FUEL_VOLUME.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                    if(EP_Common_Constant.LEAD_PROD_INTERESTED_FUEL.equals(intVal) && caseLead.EP_Indicative_Fuel_Volume__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                    if(EP_Common_Constant.LEAD_PROD_INTERESTED_LUBES.equals(intVal) && caseLead.EP_Indicative_Lubes_Volume__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_INDICATIVE_LUBES_VOLUME.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                    if(EP_Common_Constant.LEAD_PROD_INTERESTED_LUBES.equals(intVal) && caseLead.EP_Indicative_Order_Frequency_Lubes__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_INDICATIVE_ORDER_FREQUENCY_LUBES.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                }
            }else if(fieldName == EP_Common_Constant.LEAD_REQUESTED_PAYMENT_METHOD && caseLead.EP_Requested_Payment_Method__c != null){
                if(EP_Common_Constant.LEAD_DD_PAYMENT_METHOD.equals(caseLead.EP_Requested_Payment_Method__c)){
                    if(caseLead.EP_Bank_Name__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_BANK_NAME.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                    if(caseLead.EP_IBAN__c == null){
                        errorMessage += mapOfFieldLabelName.get(EP_Common_Constant.LEAD_IBAN.toLowerCase())+EP_Common_Constant.COMMA;
                    }
                }
            }else{
                if (!setOfSkippedFieldNames.contains(fieldName) && caseLead.get(fieldName) == null){
                    errorMessage += mapOfFieldLabelName.get(fieldName.toLowerCase())+EP_Common_Constant.COMMA; 
                }
            }
        }
        return errorMessage;
    }
}